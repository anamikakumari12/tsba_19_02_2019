﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;
using System.IO.Compression;
using System.IO;
using System.Globalization;
using System.Web.Services;
using Ionic.Zip;
using System.Drawing;

namespace TaegutecSalesBudget
{
    public partial class WebForm1 : System.Web.UI.Page
    {

        #region GlobalDeclareation
        Budget objBudget = new Budget();
        Reports objReports = new Reports();
        AdminConfiguration objConfig = new AdminConfiguration();
        List<string> cssList = new List<string>();
        List<string> familylist = new List<string>();
        List<string> subfamilylist = new List<string>();
        public static DataTable dtTotals, dtfamilyname, dtfamilytotals, dtvalue;
        public static string customernumber, customertype, RoleID, strUserId, salesengineernumber;
        public static float byValueIn, budgetval, ytdval, arate;
        public static int actual_mnth;
        public static int tablesLoadedStatus;
        Review objRSum = new Review();
        public string UserId, UserRoleId;
        #endregion


        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (Session["RoleId"].ToString() == "BM") { Response.Redirect("BMReports.aspx?Reports"); return; }
                else if (Session["RoleId"].ToString() == "HO" || Session["RoleId"].ToString() == "TM")
                {
                    Response.Redirect("HOReports.aspx?Reports"); return;
                }
                else if (Session["RoleId"].ToString() == "SE")
                {

                    strUserId = Session["UserId"].ToString();
                    string roleId = Session["RoleId"].ToString();
                    // int BudgetYear = objConfig.getBudgetYear();
                    exportbtn.Visible = false;
                    // lblBdgtYr.InnerText = BudgetYear + " " + "BUDGET";
                    //reportdrpdwns.Visible = false;
                    //reportsgrid.Visible = false;
                    actual_mnth = 12 - (objConfig.getActualMonth());

                    byValueIn = 1000;

                    LoadCustomerDetails(strUserId, roleId);
                    LoadCSS();

                }
            }
        }
        //protected void detailreports_Click1(object sender, EventArgs e)
        //{
        //    exportbtn.Visible = false;
        //    dfltreportform.Visible = false;
        //    reportdrpdwns.Visible = true;
        //    reportsgrid.Visible = true;
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        //}
        protected void ddlcustomertype_SelectedIndexChanged(object sender, EventArgs e)
        {
            exportbtn.Visible = false;
            string strUserId = Session["UserId"].ToString();
            string roleId = Session["RoleId"].ToString();
            string customernumber = ddlcustomertype.SelectedItem.Value;
            if (customernumber == "A")
            {
                LoadCustomerDetails(strUserId, roleId);
            }
            else
            {
                DataTable dtCutomerDetails = new DataTable();
                if (Session["UserId"] != null)
                {


                    dtCutomerDetails = objReports.LoadCustomerDetailstype(strUserId, roleId, customernumber);


                    if (dtCutomerDetails.Rows.Count > 0)
                    {
                        DataTable dtDeatils = new DataTable();
                        dtDeatils.Columns.Add("customer_number", typeof(string));
                        dtDeatils.Columns.Add("customer_short_name", typeof(string));


                        for (int i = 0; i < dtCutomerDetails.Rows.Count; i++)
                        {
                            dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[1].ToString(), dtCutomerDetails.Rows[i].ItemArray[2].ToString() + "(" + dtCutomerDetails.Rows[i].ItemArray[1].ToString() + ")");
                        }
                        ddlCustomerList.DataSource = dtDeatils;
                        ddlCustomerList.DataTextField = "customer_short_name";
                        ddlCustomerList.DataValueField = "customer_number";
                        ddlCustomerList.DataBind();
                        //ddlCustomerList.Items.Insert(0, "-- SELECT CUSTOMER --");
                        ddlCustomerList.Items.Insert(0, "ALL");

                        ddlCustomerNumber.DataSource = dtCutomerDetails;
                        ddlCustomerNumber.DataTextField = "customer_number";
                        ddlCustomerNumber.DataValueField = "customer_number";
                        ddlCustomerNumber.DataBind();
                        //ddlCustomerNumber.Items.Insert(0, "-- SELECT CUSTOMER NUMBER --");
                        ddlCustomerNumber.Items.Insert(0, "ALL");
                    }
                    else
                    {
                        ddlCustomerList.DataSource = dtCutomerDetails;
                        ddlCustomerList.DataTextField = "customer_short_name";
                        ddlCustomerList.DataValueField = "customer_number";
                        ddlCustomerList.DataBind();
                        ddlCustomerList.Items.Insert(0, "NO CUSTOMERS EXIST");

                    }
                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        protected void reports_Click(object sender, EventArgs e)
        {
            LoadProductGroups();
            List<string> PgList = new List<string>();
            UserId = Session["UserId"].ToString();
            UserRoleId = Session["RoleId"].ToString();
            if (ValInThsnd.Checked)
            {
                Rdbtnvalue.Checked = true;
                Rdbtnlacks.Checked = false;
                Rdbtnunits.Checked = false;
            }
            else if (ValInLakhs.Checked)
            {
                Rdbtnvalue.Checked = false;
                Rdbtnlacks.Checked = true;
                Rdbtnunits.Checked = false;
            }

            if (ValInThsnd.Checked)
            {
                byValueIn = 1000;
            }
            else
            {
                byValueIn = 100000;
            }
            tablesLoadedStatus = 1;
            bindgoldproductsGrid();
            bind5yrsproductsGrid();
            bindspecialsproductsGrid();
            bindtopproductsGrid();
            bindbbproductsGrid();
            bindtenyrsprodutcsgrid();
            PgList = Session["PgList"] as System.Collections.Generic.List<string>;
            int count = 0;
            for (int i = 0; i < PgList.Count; i++)
            {
                switch (PgList[i])
                {
                    case "GOLD":
                        goldproducts.Visible = true;
                        break;
                    case "TOP":
                        topproducts.Visible = true;

                        break;
                    case "BB":

                        bbproducts.Visible = true;

                        break;
                    case "FIVE YEARS":

                        fiveyrsproducts.Visible = true;

                        break;
                    case "TEN YEARS":
                        tenyrsproducts.Visible = true;
                        break;
                    case "SPC":
                        spcproducts.Visible = true;
                        break;
                }
            }
            //  tablesLoadedStatus = 1;
            // bindgoldproductsGrid();
            // bind5yrsproductsGrid();
            //  bindspecialsproductsGrid();
            // bindtopproductsGrid();
            // bindbbproductsGrid();
            //  bindtenyrsprodutcsgrid();
            bindgridsalesbyline();
            bindgridsalesbyfamily();
            bindgridsalesbyapp();
            bindgridsalesbyappQty();
            bindgridsalesbycustomer();
            LoadCSS();
            bindgridColor();
            exportbtn.Visible = true;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "<script type='text/javascript' src='js/Reports.js'></script>", false);

        }
        protected void byValueIn_CheckedChanged(Object sender, EventArgs e)
        {
            if (ValInThsnd.Checked)
            {
                byValueIn = 1000;

                if (tablesLoadedStatus == 1)
                {
                    reports_Click(null, null);
                }
            }

            if (ValInLakhs.Checked)
            {
                byValueIn = 100000;

                if (tablesLoadedStatus == 1)
                {
                    reports_Click(null, null);
                }
            }
        }

        protected void byValueorunitsorlacks_CheckedChanged(Object sender, EventArgs e)
        {
            if (Rdbtnunits.Checked)
            {
                byValueIn = 1;
            }

            if (Rdbtnvalue.Checked)
            {
                byValueIn = 1000;
            }

            if (Rdbtnlacks.Checked)
            {
                byValueIn = 100000;
            }

            tablesLoadedStatus = 1;

            bindgridsalesbyapp();
            bindgridsalesbyappQty();

            LoadCSS();
            bindgridColor();

            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "byValueorunitsorlacks_CheckedChanged", "<script>byValueorunitsorlacks_CheckedChanged()</script>", false);
        }
        protected void goldproducts_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.Header)
            {
                //ActualValue
                int BudgetYear = objConfig.getBudgetYear();
                int ActualYear = objConfig.getActualYear() - 1;
                int ActualMonth = objConfig.getActualMonth();
                string Year = "";
                if (ActualMonth == 12) { Year = ActualYear + ""; }
                else { Year = Year = ActualYear + "P"; }// ActualYear + "P"; }


                string ActualValueYear2 = (ActualYear - 2).ToString();
                string ActualValueyear1 = (ActualYear - 1).ToString();
                string ActualValueNextYear = (ActualYear + 1).ToString();
                string valueIn = " VAL(000)";
                if (byValueIn == 100000)
                {
                    valueIn = " VAL(00,000)";
                }
                e.Row.Cells[1].Text = ActualValueyear1 + "<br/> " + valueIn; //2013
                // e.Row.Cells[2].Text = ActualValueyear1 + "<br/> Rupee(000)";
                e.Row.Cells[2].Text = Year + "<br/> " + valueIn; //2014
                e.Row.Cells[3].Text = BudgetYear + "B" + "<br/>" + valueIn; //2015
                e.Row.Cells[4].Text = BudgetYear + "B" + "/" + Year + "<br/> % CHANGE"; //2015/2014
                e.Row.Cells[5].Text = BudgetYear + " " + "YTD " + "<br/> " + valueIn;



                e.Row.Cells[6].Text = "YTD" + "/" + BudgetYear + "B" + "<br/> % ACH";
                e.Row.Cells[7].Text = BudgetYear + "B" + " " + " Ask.Rate" + " <br/>" + valueIn;

            }
        }
        protected void salesbyapp_cust_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.Header)
            {
                //ActualValue
                int BudgetYear = objConfig.getBudgetYear();
                int ActualYear = objConfig.getActualYear() - 1;
                int ActualMonth = objConfig.getActualMonth();
                string Year = "";
                if (ActualMonth == 12) { Year = ActualYear + ""; }
                else { Year = Year = ActualYear + "P"; }// ActualYear + "P"; }

                string ActualValueYear2 = (ActualYear - 2).ToString();
                string ActualValueyear1 = (ActualYear - 1).ToString();
                string ActualValueNextYear = (ActualYear + 1).ToString();
                string valueIn = " VAL(000)";
                if (byValueIn == 100000)
                {
                    valueIn = " VAL(00,000)";
                }
                e.Row.Cells[2].Text = ActualValueyear1 + "<br/>" + valueIn;
                e.Row.Cells[3].Text = Year + "<br/> " + valueIn;
                e.Row.Cells[4].Text = BudgetYear + "B" + "<br/>" + valueIn;
                e.Row.Cells[5].Text = BudgetYear + "B" + "/" + Year + "<br/> % CHANGE";
                e.Row.Cells[6].Text = BudgetYear + " " + "YTD" + "<br/> " + valueIn;
                e.Row.Cells[7].Text = "YTD" + "/" + BudgetYear + "B" + "<br/> % ACH";
                e.Row.Cells[8].Text = BudgetYear + "B" + " " + "Ask.Rate " + " <br/>" + valueIn;

            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                var lblYtdPlan = e.Row.FindControl("lbl_Budget") as Label;
                var lblYTDSale = e.Row.FindControl("lbl_YtdSale") as Label;
                var lbl_customername = e.Row.FindControl("Labelcust_name") as Label;
                if (lbl_customername != null)
                    if (!lbl_customername.Text.ToString().Contains("TOTAL"))
                        if (lblYtdPlan != null)
                        {
                            if (lblYtdPlan.Text == "0" && (lblYTDSale.Text != "0"))
                            {
                                e.Row.Cells[6].CssClass = "tdHighlight";
                            }
                        }


            }

        }

        protected void salesbyapp_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.Header)
            {
                //ActualValue
                int BudgetYear = objConfig.getBudgetYear();
                int ActualYear = objConfig.getActualYear() - 1;
                int ActualMonth = objConfig.getActualMonth();
                string Year = "";
                if (ActualMonth == 12) { Year = ActualYear + ""; }
                else { Year = Year = ActualYear + "P"; }// ActualYear + "P"; }

                string ActualValueYear2 = (ActualYear - 2).ToString();
                string ActualValueyear1 = (ActualYear - 1).ToString();
                string ActualValueNextYear = (ActualYear + 1).ToString();
                string valueIn = " VAL(000)";
                if (byValueIn == 100000)
                {
                    valueIn = " VAL(00,000)";
                }
                else if (byValueIn == 1)
                {
                    valueIn = " VAL";
                }

                e.Row.Cells[2].Text = ActualValueyear1 + "<br/> " + valueIn;
                e.Row.Cells[3].Text = Year + "<br/>" + valueIn;
                e.Row.Cells[4].Text = BudgetYear + "B" + "<br/>" + valueIn;
                e.Row.Cells[5].Text = BudgetYear + "B" + "/" + Year + "<br/> % CHANGE";
                e.Row.Cells[6].Text = BudgetYear + " " + "YTD" + "<br/> " + valueIn;
                e.Row.Cells[7].Text = "YTD" + "/" + BudgetYear + "B" + "<br/> % ACH";
                e.Row.Cells[8].Text = BudgetYear + "B" + " " + "Ask.Rate" + "<br/> " + valueIn;
            }

        }

        /// <summary>
        /// Modified By : Anamika
        /// Date : 9th Feb 2017
        /// Desc : Loaded data without comma from session to excel
        /// Modified By: Neha
        /// Date: 18th Dec 2018
        /// Desc :Commented some extra line of code and Description ddded 10 yrs flag  and added contion for handling null values and disable special group visibility.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void export_Click(object sender, EventArgs e)
        {
            Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "Reports.xls"));
            Response.ContentType = "application/ms-excel";
            //Response.ContentType = "application/vnd.ms-powerpoint";
            StringWriter sw = new StringWriter();
            HtmlTextWriter ht = new HtmlTextWriter(sw);
            string custtype = ddlcustomertype.SelectedItem.Text;
            string custname = ddlCustomerList.SelectedItem.Text;
            string custno = ddlCustomerNumber.SelectedItem.Text;
            DataTable dtexport = new DataTable();
            LoadProductGroups();
            List<string> PgList = new List<string>();
            PgList = Session["PgList"] as System.Collections.Generic.List<string>;
            for (int i = 0; i < PgList.Count; i++)
            {
                switch (PgList[i])
                {
                    case "GOLD":
                        goldproducts.Visible = true;
                        break;
                    case "TOP":
                        topproducts.Visible = true;

                        break;
                    case "BB":

                        bbproducts.Visible = true;

                        break;
                    case "FIVE YEARS":

                        fiveyrsproducts.Visible = true;

                        break;
                    case "TEN YEARS":
                        tenyrsproducts.Visible = true;
                        break;
                    case "SPC":
                        spcproducts.Visible = true;
                        break;
                }
            }
            sw.WriteLine("<table><tr><td></td><td></td><td></td><td colspan=3 style='font-weight: bold; font-size:20px'>BUDGET REPORT</td></table>");
            sw.WriteLine("<table style='margin-left: 200px;'>");
            //sw.WriteLine("CUSTOMER TYPE :" + "" + custtype + "<br/>");
            sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>CUSTOMER TYPE : " + "</td><td colspan=8 style='font-style: italic;'>" + custtype + "</td></tr>");
            // sw.WriteLine("CUSTOMER NAME :" + "" + custname + "<br/>");
            sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>CUSTOMER NAME :" + "</td><td colspan=8 style='font-style: italic; '>" + custname + "</td></tr>");
            //sw.WriteLine("CUSTOMER NUMBER :" + "" + custno + "<br/>");
            sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>CUSTOMER NUMBER :" + "</td><td colspan=8 style='font-style: italic; text-align: left; '>" + custno + "</td></tr>");
            dtexport = (DataTable)Session["goldproducts"];
            GridView dtgoldproducts = new GridView();
            //dtgoldproducts.DataSource = (DataTable)Session["goldproducts"];
            //dtgoldproducts.DataBind();
            //GridView dtfiveyrsproducts = new GridView();
            //dtfiveyrsproducts.DataSource = (DataTable)Session["fiveyrsproducts"];
            //dtfiveyrsproducts.DataBind();
            //GridView dtspcproducts = new GridView();
            //dtspcproducts.DataSource = (DataTable)Session["spcproducts"];
            //dtspcproducts.DataBind();
            //GridView dttopproducts = new GridView();
            //dttopproducts.DataSource = (DataTable)Session["topproducts"];
            //dttopproducts.DataBind();
            //GridView dtbbproducts = new GridView();
            //dtbbproducts.DataSource = (DataTable)Session["bbproducts"];
            //dtbbproducts.DataBind();
            //GridView dtsalesbylinegrid = new GridView();
            //dtsalesbylinegrid.DataSource = (DataTable)Session["salesbylinegrid"];
            //dtsalesbylinegrid.DataBind();
            //GridView dtsalesbyfamilygrid = new GridView();
            //dtsalesbyfamilygrid.DataSource = (DataTable)Session["salesbyfamilygrid"];
            //dtsalesbyfamilygrid.DataBind();
            //GridView dtsalesbyapp = new GridView();
            //if (rbtn_Quantity.Checked)
            //{
            //    dtsalesbyapp.DataSource = (DataTable)Session["salesbyapp_qty"];
            //}
            //else
            //    dtsalesbyapp.DataSource = (DataTable)Session["salesbyapp"];
            //dtsalesbyapp.DataBind();
            //GridView dtsalesbycustomer = new GridView();
            //dtsalesbycustomer.DataSource = (DataTable)Session["salesbycustomer"];
            //dtsalesbycustomer.DataBind();



            if (dtexport != null)
            {
                if (dtexport.Rows.Count > 0)
                {
                    dtgoldproducts.DataSource = (DataTable)Session["goldproducts"];
                }
                else
                {
                    dtgoldproducts.DataSource = null;
                }
            }
            else
            {
                dtgoldproducts.DataSource = null;
            }
            dtgoldproducts.DataBind();
            dtexport = new DataTable();
            dtexport = (DataTable)Session["fiveyrsproducts"];
            GridView dtfiveyrsproducts = new GridView();
            if (dtexport != null)
            {
                if (dtexport.Rows.Count > 0)
                {
                    dtfiveyrsproducts.DataSource = (DataTable)Session["fiveyrsproducts"];
                }
                else
                {
                    dtfiveyrsproducts.DataSource = null;
                }
            }
            else
            {
                dtfiveyrsproducts.DataSource = null;
            }
            //dtfiveyrsproducts.DataSource= (DataTable)Session["fiveyrsproducts"];
            dtfiveyrsproducts.DataBind();

            dtexport = new DataTable();
            dtexport = (DataTable)Session["spcproducts"];
            GridView dtspcproducts = new GridView();
            if (dtexport != null)
            {
                if (dtexport.Rows.Count > 0)
                {
                    dtspcproducts.DataSource = (DataTable)Session["spcproducts"];
                }
                else
                {
                    dtspcproducts.DataSource = null;
                }
            }
            else
            {
                dtspcproducts.DataSource = null;
            }
            // dtspcproducts.DataSource =   (DataTable)Session["spcproducts"];
            dtspcproducts.DataBind();

            dtexport = new DataTable();
            dtexport = (DataTable)Session["topproducts"];
            GridView dttopproducts = new GridView();
            if (dtexport != null)
            {
                if (dtexport.Rows.Count > 0)
                {
                    dttopproducts.DataSource = (DataTable)Session["topproducts"];
                }
                else
                {
                    dttopproducts.DataSource = null;
                }
            }
            else
            {
                dttopproducts.DataSource = null;
            }
            //  dttopproducts.DataSource=(DataTable)Session["topproducts"];
            dttopproducts.DataBind();

            dtexport = new DataTable();
            dtexport = (DataTable)Session["bbproducts"];
            GridView dtbbproducts = new GridView();
            if (dtexport != null)
            {
                if (dtexport.Rows.Count > 0)
                {
                    dtbbproducts.DataSource = (DataTable)Session["bbproducts"];
                }
                else
                {
                    dtbbproducts.DataSource = null;
                }
            }
            else
            {
                dtbbproducts.DataSource = null;
            }
            // dtbbproducts.DataSource=(DataTable)Session["bbproducts"];
            dtbbproducts.DataBind();

            dtexport = new DataTable();
            dtexport = (DataTable)Session["tenyrsproducts"];
            GridView dttenyrsproducts = new GridView();
            if (dtexport != null)
            {
                if (dtexport.Rows.Count > 0)
                {
                    dttenyrsproducts.DataSource = (DataTable)Session["tenyrsproducts"];
                }
                else
                {
                    dttenyrsproducts.DataSource = null;
                }
            }
            else
            {
                dttenyrsproducts.DataSource = null;
            }
            // dttenyrsproducts.DataSource = (DataTable)Session["tenyrsproducts"];
            dttenyrsproducts.DataBind();

            dtexport = new DataTable();
            dtexport = (DataTable)Session["salesbylinegrid"];
            GridView dtsalesbylinegrid = new GridView();
            if (dtexport != null)
            {
                if (dtexport.Rows.Count > 0)
                {
                    dtsalesbylinegrid.DataSource = (DataTable)Session["salesbylinegrid"];
                }
                else
                {
                    dtsalesbylinegrid.DataSource = null;
                }
            }
            else
            {
                dtsalesbylinegrid.DataSource = null;
            }
            //dtsalesbylinegrid.DataSource = (DataTable)Session["salesbylinegrid"];
            dtsalesbylinegrid.DataBind();

            dtexport = new DataTable();
            dtexport = (DataTable)Session["salesbyfamilygrid"];
            GridView dtsalesbyfamilygrid = new GridView();
            if (dtexport != null)
            {
                if (dtexport.Rows.Count > 0)
                {
                    dtsalesbyfamilygrid.DataSource = (DataTable)Session["salesbyfamilygrid"];
                }
                else
                {
                    dtsalesbyfamilygrid.DataSource = null;
                }
            }
            else
            {
                dtsalesbyfamilygrid.DataSource = null;
            }
            // dtsalesbyfamilygrid.DataSource = (DataTable)Session["salesbyfamilygrid"];
            dtsalesbyfamilygrid.DataBind();

            dtexport = new DataTable();

            GridView dtsalesbyapp = new GridView();
            if (rbtn_Quantity.Checked)
            {
                dtexport = (DataTable)Session["salesbyapp_qty"];
                if (dtexport != null)
                {
                    if (dtexport.Rows.Count > 0)
                    {
                        dtsalesbyapp.DataSource = (DataTable)Session["salesbyapp_qty"];
                    }

                    else
                    {
                        dtsalesbyapp.DataSource = null;
                    }
                }
                else
                {
                    dtsalesbyapp.DataSource = null;
                }
                //dtsalesbyapp.DataSource = (DataTable)Session["salesbyapp_qty"];
            }
            else
            {
                dtexport = new DataTable();
                dtexport = (DataTable)Session["salesbyapp"];
                dtsalesbyapp = new GridView();
                if (dtexport != null)
                {
                    if (dtexport.Rows.Count > 0)
                    {
                        dtsalesbyapp.DataSource = (DataTable)Session["salesbyapp"];
                    }

                    else
                    {
                        dtsalesbyapp.DataSource = null;
                    }
                }
                else
                {
                    dtsalesbyapp.DataSource = null;
                }
                //  dtsalesbyapp.DataSource = (DataTable)Session["salesbyapp"];
                dtsalesbyapp.DataBind();
            }
            dtexport = new DataTable();
            dtexport = (DataTable)Session["salesbycustomer"];
            GridView dtsalesbycustomer = new GridView();
            if (dtexport != null)
            {
                if (dtexport.Rows.Count > 0)
                {
                    dtsalesbycustomer.DataSource = (DataTable)Session["salesbycustomer"];
                }
                else
                {
                    dtsalesbycustomer.DataSource = null;
                }
            }
            else
            {
                dtsalesbycustomer.DataSource = null;
            }
            ///dtsalesbycustomer.DataSource = (DataTable)Session["salesbycustomer"];
            dtsalesbycustomer.DataBind();
            for (int i = 0; i < PgList.Count; i++)
            {
                switch (PgList[i])
                {
                    case "GOLD":
                        if (dtgoldproducts.Rows.Count > 0)
                        {
                            dtgoldproducts.HeaderRow.Cells[0].Text = "GOLD PRODUCTS";
                            dtgoldproducts.HeaderRow.Cells[1].Text = goldproducts.HeaderRow.Cells[1].Text;
                            dtgoldproducts.HeaderRow.Cells[2].Text = goldproducts.HeaderRow.Cells[2].Text;
                            dtgoldproducts.HeaderRow.Cells[3].Text = goldproducts.HeaderRow.Cells[3].Text;
                            dtgoldproducts.HeaderRow.Cells[4].Text = goldproducts.HeaderRow.Cells[4].Text;
                            dtgoldproducts.HeaderRow.Cells[5].Text = goldproducts.HeaderRow.Cells[5].Text;
                            dtgoldproducts.HeaderRow.Cells[6].Text = goldproducts.HeaderRow.Cells[6].Text;
                            dtgoldproducts.HeaderRow.Cells[7].Text = goldproducts.HeaderRow.Cells[7].Text;
                            dtgoldproducts.RenderControl(ht);
                        }
                        break;
                    case "FIVE YEARS":
                        if (dtfiveyrsproducts.Rows.Count > 0)
                        {
                            dtfiveyrsproducts.HeaderRow.Cells[0].Text = "5 YEARS PRODUCTS";
                            dtfiveyrsproducts.HeaderRow.Cells[2].Text = fiveyrsproducts.HeaderRow.Cells[2].Text;
                            dtfiveyrsproducts.HeaderRow.Cells[1].Text = fiveyrsproducts.HeaderRow.Cells[1].Text;
                            dtfiveyrsproducts.HeaderRow.Cells[3].Text = fiveyrsproducts.HeaderRow.Cells[3].Text;
                            dtfiveyrsproducts.HeaderRow.Cells[4].Text = fiveyrsproducts.HeaderRow.Cells[4].Text;
                            dtfiveyrsproducts.HeaderRow.Cells[5].Text = fiveyrsproducts.HeaderRow.Cells[5].Text;
                            dtfiveyrsproducts.HeaderRow.Cells[6].Text = fiveyrsproducts.HeaderRow.Cells[6].Text;
                            dtfiveyrsproducts.HeaderRow.Cells[7].Text = fiveyrsproducts.HeaderRow.Cells[7].Text;
                            dtfiveyrsproducts.RenderControl(ht);
                        }
                        break;
                    case "SPC":
                        if (dtspcproducts.Rows.Count > 0)
                        {
                            dtspcproducts.HeaderRow.Cells[0].Text = "SPECIALS";
                            dtspcproducts.HeaderRow.Cells[1].Text = spcproducts.HeaderRow.Cells[1].Text;
                            dtspcproducts.HeaderRow.Cells[2].Text = spcproducts.HeaderRow.Cells[2].Text;
                            dtspcproducts.HeaderRow.Cells[3].Text = spcproducts.HeaderRow.Cells[3].Text;
                            dtspcproducts.HeaderRow.Cells[4].Text = spcproducts.HeaderRow.Cells[4].Text;
                            dtspcproducts.HeaderRow.Cells[5].Text = spcproducts.HeaderRow.Cells[5].Text;
                            dtspcproducts.HeaderRow.Cells[6].Text = spcproducts.HeaderRow.Cells[6].Text;
                            dtspcproducts.HeaderRow.Cells[7].Text = spcproducts.HeaderRow.Cells[7].Text;
                            dtspcproducts.RenderControl(ht);
                        }
                        break;
                    case "TOP":
                        if (dttopproducts.Rows.Count > 0)
                        {
                            dttopproducts.HeaderRow.Cells[0].Text = "TOP PRODUCTS";
                            dttopproducts.HeaderRow.Cells[1].Text = topproducts.HeaderRow.Cells[1].Text;
                            dttopproducts.HeaderRow.Cells[2].Text = topproducts.HeaderRow.Cells[2].Text;
                            dttopproducts.HeaderRow.Cells[3].Text = topproducts.HeaderRow.Cells[3].Text;
                            dttopproducts.HeaderRow.Cells[4].Text = topproducts.HeaderRow.Cells[4].Text;
                            dttopproducts.HeaderRow.Cells[5].Text = topproducts.HeaderRow.Cells[5].Text;
                            dttopproducts.HeaderRow.Cells[6].Text = topproducts.HeaderRow.Cells[6].Text;
                            dttopproducts.HeaderRow.Cells[7].Text = topproducts.HeaderRow.Cells[7].Text;
                            dttopproducts.RenderControl(ht);
                        }
                        break;
                    case "BB":
                        if (dtbbproducts.Rows.Count > 0)
                        {
                            dtbbproducts.HeaderRow.Cells[0].Text = "BB PRODUCTS";
                            dtbbproducts.HeaderRow.Cells[1].Text = bbproducts.HeaderRow.Cells[1].Text;
                            dtbbproducts.HeaderRow.Cells[2].Text = bbproducts.HeaderRow.Cells[2].Text;
                            dtbbproducts.HeaderRow.Cells[3].Text = bbproducts.HeaderRow.Cells[3].Text;
                            dtbbproducts.HeaderRow.Cells[4].Text = bbproducts.HeaderRow.Cells[4].Text;
                            dtbbproducts.HeaderRow.Cells[5].Text = bbproducts.HeaderRow.Cells[5].Text;
                            dtbbproducts.HeaderRow.Cells[6].Text = bbproducts.HeaderRow.Cells[6].Text;
                            dtbbproducts.HeaderRow.Cells[7].Text = bbproducts.HeaderRow.Cells[7].Text;
                            dtbbproducts.RenderControl(ht);
                        }
                        break;
                    case "TEN YEARS":
                        if (dttenyrsproducts.Rows.Count > 0)
                        {

                            dttenyrsproducts.HeaderRow.Cells[0].Text = "10 YEARS  PRODUCTS";
                            dttenyrsproducts.HeaderRow.Cells[1].Text = tenyrsproducts.HeaderRow.Cells[1].Text;
                            dttenyrsproducts.HeaderRow.Cells[2].Text = tenyrsproducts.HeaderRow.Cells[2].Text;
                            dttenyrsproducts.HeaderRow.Cells[3].Text = tenyrsproducts.HeaderRow.Cells[3].Text;
                            dttenyrsproducts.HeaderRow.Cells[4].Text = tenyrsproducts.HeaderRow.Cells[4].Text;
                            dttenyrsproducts.HeaderRow.Cells[5].Text = tenyrsproducts.HeaderRow.Cells[5].Text;
                            dttenyrsproducts.HeaderRow.Cells[6].Text = tenyrsproducts.HeaderRow.Cells[6].Text;
                            dttenyrsproducts.HeaderRow.Cells[7].Text = tenyrsproducts.HeaderRow.Cells[7].Text;
                            dttenyrsproducts.RenderControl(ht);

                        }
                        break;
                }
            }
            if (dtsalesbylinegrid.Rows.Count > 0)
            {
                dtsalesbylinegrid.HeaderRow.Cells[0].Text = " SALES BY LINE";
                dtsalesbylinegrid.HeaderRow.Cells[1].Text = salesbylinegrid.HeaderRow.Cells[1].Text;
                dtsalesbylinegrid.HeaderRow.Cells[2].Text = salesbylinegrid.HeaderRow.Cells[2].Text;
                dtsalesbylinegrid.HeaderRow.Cells[3].Text = salesbylinegrid.HeaderRow.Cells[3].Text;
                dtsalesbylinegrid.HeaderRow.Cells[4].Text = salesbylinegrid.HeaderRow.Cells[4].Text;
                dtsalesbylinegrid.HeaderRow.Cells[5].Text = salesbylinegrid.HeaderRow.Cells[5].Text;
                dtsalesbylinegrid.HeaderRow.Cells[6].Text = salesbylinegrid.HeaderRow.Cells[6].Text;
                dtsalesbylinegrid.HeaderRow.Cells[7].Text = salesbylinegrid.HeaderRow.Cells[7].Text;
                dtsalesbylinegrid.RenderControl(ht);
            }
            if (dtsalesbyfamilygrid.Rows.Count > 0)
            {
                dtsalesbyfamilygrid.HeaderRow.Cells[0].Text = " SALES BY FAMILY";
                dtsalesbyfamilygrid.HeaderRow.Cells[1].Text = salesbyfamilygrid.HeaderRow.Cells[1].Text;
                dtsalesbyfamilygrid.HeaderRow.Cells[2].Text = salesbyfamilygrid.HeaderRow.Cells[2].Text;
                dtsalesbyfamilygrid.HeaderRow.Cells[3].Text = salesbyfamilygrid.HeaderRow.Cells[3].Text;
                dtsalesbyfamilygrid.HeaderRow.Cells[4].Text = salesbyfamilygrid.HeaderRow.Cells[4].Text;
                dtsalesbyfamilygrid.HeaderRow.Cells[5].Text = salesbyfamilygrid.HeaderRow.Cells[5].Text;
                dtsalesbyfamilygrid.HeaderRow.Cells[6].Text = salesbyfamilygrid.HeaderRow.Cells[6].Text;
                dtsalesbyfamilygrid.HeaderRow.Cells[7].Text = salesbyfamilygrid.HeaderRow.Cells[7].Text;
                dtsalesbyfamilygrid.RenderControl(ht);
            }
            if (rbtn_Quantity.Checked)
            {
                if (dtsalesbyapp.Rows.Count > 0)
                {
                    dtsalesbyapp.HeaderRow.Cells[0].Text = salesbyapp_qty.HeaderRow.Cells[1].Text;
                    dtsalesbyapp.HeaderRow.Cells[1].Text = salesbyapp_qty.HeaderRow.Cells[0].Text;
                    dtsalesbyapp.HeaderRow.Cells[2].Text = salesbyapp_qty.HeaderRow.Cells[2].Text;
                    dtsalesbyapp.HeaderRow.Cells[3].Text = salesbyapp_qty.HeaderRow.Cells[3].Text;
                    dtsalesbyapp.HeaderRow.Cells[4].Text = salesbyapp_qty.HeaderRow.Cells[4].Text;
                    dtsalesbyapp.HeaderRow.Cells[5].Text = salesbyapp_qty.HeaderRow.Cells[5].Text;
                    dtsalesbyapp.HeaderRow.Cells[6].Text = salesbyapp_qty.HeaderRow.Cells[6].Text;
                    dtsalesbyapp.HeaderRow.Cells[7].Text = salesbyapp_qty.HeaderRow.Cells[7].Text;
                    dtsalesbyapp.HeaderRow.Cells[8].Text = salesbyapp_qty.HeaderRow.Cells[8].Text;
                    dtsalesbyapp.RenderControl(ht);
                }
            }
            else
            {
                if (dtsalesbyapp.Rows.Count > 0)
                {
                    dtsalesbyapp.HeaderRow.Cells[0].Text = salesbyapp.HeaderRow.Cells[1].Text;
                    dtsalesbyapp.HeaderRow.Cells[1].Text = salesbyapp.HeaderRow.Cells[0].Text;
                    dtsalesbyapp.HeaderRow.Cells[2].Text = salesbyapp.HeaderRow.Cells[2].Text;
                    dtsalesbyapp.HeaderRow.Cells[3].Text = salesbyapp.HeaderRow.Cells[3].Text;
                    dtsalesbyapp.HeaderRow.Cells[4].Text = salesbyapp.HeaderRow.Cells[4].Text;
                    dtsalesbyapp.HeaderRow.Cells[5].Text = salesbyapp.HeaderRow.Cells[5].Text;
                    dtsalesbyapp.HeaderRow.Cells[6].Text = salesbyapp.HeaderRow.Cells[6].Text;
                    dtsalesbyapp.HeaderRow.Cells[7].Text = salesbyapp.HeaderRow.Cells[7].Text;
                    dtsalesbyapp.HeaderRow.Cells[8].Text = salesbyapp.HeaderRow.Cells[8].Text;
                    dtsalesbyapp.RenderControl(ht);
                }
            }

            if (dtsalesbycustomer.Rows.Count > 0)
            {
                dtsalesbycustomer.HeaderRow.Cells[0].Text = salesbycustomer.HeaderRow.Cells[1].Text;
                dtsalesbycustomer.HeaderRow.Cells[1].Text = salesbycustomer.HeaderRow.Cells[0].Text;
                dtsalesbycustomer.HeaderRow.Cells[2].Text = salesbycustomer.HeaderRow.Cells[2].Text;
                dtsalesbycustomer.HeaderRow.Cells[3].Text = salesbycustomer.HeaderRow.Cells[3].Text;
                dtsalesbycustomer.HeaderRow.Cells[4].Text = salesbycustomer.HeaderRow.Cells[4].Text;
                dtsalesbycustomer.HeaderRow.Cells[5].Text = salesbycustomer.HeaderRow.Cells[5].Text;
                dtsalesbycustomer.HeaderRow.Cells[6].Text = salesbycustomer.HeaderRow.Cells[6].Text;
                dtsalesbycustomer.HeaderRow.Cells[7].Text = salesbycustomer.HeaderRow.Cells[7].Text;
                dtsalesbycustomer.HeaderRow.Cells[8].Text = salesbycustomer.HeaderRow.Cells[8].Text;
                dtsalesbycustomer.RenderControl(ht);
            }
            Response.Write(sw.ToString());
            Response.End();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }
        /// <summary>
        /// Author: Neha
        /// Date: 18th Dec, 2018
        /// Desc: Added method for export header and colour
        /// </summary>
        /// <param name="control"></param>
        private static void PrepareControlForExport(Control control)
        {
            for (int i = 0; i < control.Controls.Count; i++)
            {
                Control current = control.Controls[i];
                if (current is LinkButton)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as LinkButton).Text));
                }
                else if (current is ImageButton)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as ImageButton).AlternateText));
                }
                else if (current is HyperLink)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as HyperLink).Text));
                }
                else if (current is DropDownList)
                {
                    control.Controls.Remove(current);
                    //control.Controls.AddAt(i, new LiteralControl((current as DropDownList).SelectedItem.Text));
                }
                else if (current is HiddenField)
                {
                    control.Controls.Remove(current);
                }
                else if (current is CheckBox)
                {
                    control.Controls.Remove(current);
                    // control.Controls.AddAt(i, new LiteralControl((current as CheckBox).Checked ? "True" : "False"));
                }
                if (current.HasControls())
                {
                    PrepareControlForExport(current);
                }
            }
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
        }
        protected void ddlCustomerNumber_SelectedIndexChanged(object sender, EventArgs e)
        {


        }
        #endregion


        #region Methods
        public void LoadProductGroups()
        {
            int B_Budgetyear = 0;
            DataTable ProductGroups = null;
            try
            {
                B_Budgetyear = Convert.ToInt32(objConfig.GetProfile("BUDGET_YEAR"));

                ProductGroups = objRSum.GetProductGroups(B_Budgetyear);

                List<string> PgList = new List<string>();

                for (int i = 0; i < ProductGroups.Rows.Count; i++)
                {
                    PgList.Add(Convert.ToString(ProductGroups.Rows[i][1]));
                    if (Convert.ToString(ProductGroups.Rows[i][1]) == "FIVE YEARS")
                    {
                        ProductGroups.Rows[i][1] = "5YRS";
                    }
                    else if (Convert.ToString(ProductGroups.Rows[i][1]) == "TEN YEARS")
                    {
                        ProductGroups.Rows[i][1] = "10YRS";
                    }
                }
                Session["PgList"] = PgList;

            }
            catch (Exception ex)
            {
                // objCom.LogError(ex);
            }
        }
        protected void LoadCustomerDetails(string strUserId, string roleId)
        {
            DataTable dtCutomerDetails = new DataTable();
            if (Session["UserId"] != null)
            {

                Budget objBudgets = new Budget();
                dtCutomerDetails = objBudget.LoadCustomerDetails(strUserId, roleId);
                if (dtCutomerDetails != null)
                {

                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("customer_number", typeof(string));
                    dtDeatils.Columns.Add("customer_short_name", typeof(string));


                    for (int i = 0; i < dtCutomerDetails.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[1].ToString(), dtCutomerDetails.Rows[i].ItemArray[2].ToString() + "(" + dtCutomerDetails.Rows[i].ItemArray[1].ToString() + ")");
                    }
                    ddlCustomerList.DataSource = dtDeatils;
                    ddlCustomerList.DataTextField = "customer_short_name";
                    ddlCustomerList.DataValueField = "customer_number";
                    ddlCustomerList.DataBind();
                    //ddlCustomerList.Items.Insert(0, "-- SELECT CUSTOMER --");
                    ddlCustomerList.Items.Insert(0, "ALL");

                    ddlCustomerNumber.DataSource = dtCutomerDetails;
                    ddlCustomerNumber.DataTextField = "customer_number";
                    ddlCustomerNumber.DataValueField = "customer_number";
                    ddlCustomerNumber.DataBind();
                    // ddlCustomerNumber.Items.Insert(0, "-- SELECT CUSTOMER NUMBER --");
                    ddlCustomerNumber.Items.Insert(0, "ALL");
                }
            }

        }
        protected void bindgoldproductsGrid()
        {
            salesengineernumber = Session["UserId"].ToString();
            customernumber = ddlCustomerList.SelectedItem.Value == "ALL" ? null : ddlCustomerList.SelectedItem.Value;

            customertype = ddlcustomertype.SelectedItem.Value == "A" ? null : ddlcustomertype.SelectedItem.Value;


            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };
            DataTable dtTopsum = objReports.getSpecialGroupValueSum(null, null, salesengineernumber, customernumber, "GOLD", null, customertype);
            dtTotals = objReports.getValueSum(null, null, salesengineernumber, customernumber, null, null, customertype);
            // actual_mnth = 12 - (objConfig.getActualMonth());
            DataTable dtGold = new DataTable();
            dtGold.Columns.Add("FixedRow", typeof(string));
            dtGold.Columns.Add("sales_value_year_1", typeof(string));
            dtGold.Columns.Add("sales_value_year_0", typeof(string));
            dtGold.Columns.Add("estimate_value_next_year", typeof(string));
            dtGold.Columns.Add("p_sales_value_year_1", typeof(string));
            dtGold.Columns.Add("ytd", typeof(string));
            dtGold.Columns.Add("ach", typeof(string));
            dtGold.Columns.Add("arate", typeof(string));

            DataTable dtGoldReport = new DataTable();
            dtGoldReport.Columns.Add("FixedRow", typeof(string));
            dtGoldReport.Columns.Add("sales_value_year_1", typeof(string));
            dtGoldReport.Columns.Add("sales_value_year_0", typeof(string));
            dtGoldReport.Columns.Add("estimate_value_next_year", typeof(string));
            dtGoldReport.Columns.Add("p_sales_value_year_1", typeof(string));
            dtGoldReport.Columns.Add("ytd", typeof(string));
            dtGoldReport.Columns.Add("ach", typeof(string));
            dtGoldReport.Columns.Add("arate", typeof(string));

            for (int i = 0; i < dtTopsum.Rows.Count; i++)
            {
                for (int j = 0; j < dtTotals.Rows.Count; j++)
                {
                    if (i == j)
                    {
                        float top0 = dtTopsum.Rows[i].ItemArray[0].ToString() == "" || dtTopsum.Rows[i].ItemArray[0].ToString() == null ? 0    //2013
                             : Convert.ToInt64(dtTopsum.Rows[i].ItemArray[0].ToString());

                        float sum0 = dtTotals.Rows[i].ItemArray[0].ToString() == "" || dtTotals.Rows[i].ItemArray[0].ToString() == null ? 0
                            : Convert.ToInt64(dtTotals.Rows[i].ItemArray[0].ToString());
                        float below0 = sum0 == 0 ? 0 : (top0 / sum0) * 100;

                        float top1 = dtTopsum.Rows[i].ItemArray[1].ToString() == "" || dtTopsum.Rows[i].ItemArray[1].ToString() == null ? 0    //2014
                            : Convert.ToInt64(dtTopsum.Rows[i].ItemArray[1].ToString());
                        float sum1 = dtTotals.Rows[i].ItemArray[1].ToString() == "" || dtTotals.Rows[i].ItemArray[1].ToString() == null ? 0
                            : Convert.ToInt64(dtTotals.Rows[i].ItemArray[1].ToString());
                        float below1 = sum1 == 0 ? 0 : (top1 / sum1) * 100;

                        float top2 = dtTopsum.Rows[i].ItemArray[2].ToString() == "" || dtTopsum.Rows[i].ItemArray[2].ToString() == null ? 0   //2015
                           : Convert.ToInt64(dtTopsum.Rows[i].ItemArray[2].ToString());
                        float sum2 = dtTotals.Rows[i].ItemArray[2].ToString() == "" || dtTotals.Rows[i].ItemArray[2].ToString() == null ? 0
                            : Convert.ToInt64(dtTotals.Rows[i].ItemArray[2].ToString());
                        float below2 = sum2 == 0 ? 0 : (top2 / sum2) * 100;

                        float top3 = top1 == 0 ? 0 : (top2 / top1 - 1) * 100; //15/14

                        float top4 = dtTopsum.Rows[i].ItemArray[3].ToString() == "" || dtTopsum.Rows[i].ItemArray[3].ToString() == null ? 0   //ytd
                            : Convert.ToInt64(dtTopsum.Rows[i].ItemArray[3].ToString());
                        float sum4 = dtTotals.Rows[i].ItemArray[3].ToString() == "" || dtTotals.Rows[i].ItemArray[3].ToString() == null ? 0
                            : Convert.ToInt64(dtTotals.Rows[i].ItemArray[3].ToString());
                        float below4 = sum4 == 0 ? 0 : (top4 / sum4) * 100;

                        float top5 = top2 == 0 ? 0 : (top4 / top2) * 100;
                        float top6 = actual_mnth == 0 ? 0 : (top2 - top4) / actual_mnth;

                        top0 = top0 == 0 ? 0 : (top0 / byValueIn);
                        top1 = top1 == 0 ? 0 : (top1 / byValueIn);
                        top2 = top2 == 0 ? 0 : (top2 / byValueIn);
                        top4 = top4 == 0 ? 0 : (top4 / byValueIn);
                        top6 = top6 == 0 ? 0 : (top6 / byValueIn);
                        dtGold.Rows.Add("GOLD PRODUCTS", Math.Round(top0, 0).ToString("N0", culture), (Math.Round(top1, 0)).ToString("N0", culture), (Math.Round(top2, 0)).ToString("N0", culture), Convert.ToString(Math.Round(top3, 0)) + '%', (Math.Round(top4, 0)).ToString("N0", culture), Convert.ToString(Math.Round(top5, 0)) + '%', (Math.Round(top6, 0)).ToString("N0", culture));
                        dtGold.Rows.Add("CONTRIBUTION TO SALES", Convert.ToString(Math.Round(below0)) + '%', Convert.ToString(Math.Round(below1)) + '%', Convert.ToString(Math.Round(below2)) + '%', "", Convert.ToString(Math.Round(below4)) + '%');
                        dtGold.Rows.Add("CONTRIBUTION TO GROWTH");

                        dtGoldReport.Rows.Add("GOLD PRODUCTS", Math.Round(top0, 0).ToString(), (Math.Round(top1, 0)).ToString(), (Math.Round(top2, 0)).ToString(), Convert.ToString(Math.Round(top3, 0)) + '%', (Math.Round(top4, 0)).ToString(), Convert.ToString(Math.Round(top5, 0)) + '%', (Math.Round(top6, 0)).ToString());
                        dtGoldReport.Rows.Add("CONTRIBUTION TO SALES", Convert.ToString(Math.Round(below0)) + '%', Convert.ToString(Math.Round(below1)) + '%', Convert.ToString(Math.Round(below2)) + '%', "", Convert.ToString(Math.Round(below4)) + '%');
                        dtGoldReport.Rows.Add("CONTRIBUTION TO GROWTH");
                    }
                }
            }
            Session["goldproducts"] = dtGoldReport;
            goldproducts.DataSource = dtGold;
            goldproducts.DataBind();
        }
        //5yrs region
        protected void bind5yrsproductsGrid()
        {
            dtfamilyname = objBudget.LoadFamilyId();
            for (int k = 0; k < dtfamilyname.Rows.Count; k++)
            {
                familylist.Add(dtfamilyname.Rows[k].ItemArray[1].ToString());
            }
            string branchcode = Session["BranchCode"].ToString();
            string seId = Session["UserId"].ToString();
            string CustType = ddlcustomertype.SelectedItem.Value == "A" ? "ALL" : ddlcustomertype.SelectedItem.Value;
            string custNum = ddlCustomerNumber.SelectedItem.Value;
            dtfamilytotals = objReports.salesbyfamily(branchcode, seId, CustType, custNum);
            DataTable dt5yrs = productgroup("5yrs", "5 YEARS PRODUCTS");
            fiveyrsproducts.DataSource = dt5yrs;
            fiveyrsproducts.DataBind();
        }
        //specials region
        protected void bindspecialsproductsGrid()
        {


            DataTable dtspc = productgroup("SPC", "SPECIALS");

            spcproducts.DataSource = dtspc;
            spcproducts.DataBind();
        }
        //region  top products
        protected void bindtopproductsGrid()
        {


            DataTable dttop = productgroup("TOP", "TOP PRODUCTS");


            topproducts.DataSource = dttop;
            topproducts.DataBind();
        }

        //region BBproducts
        protected void bindbbproductsGrid()
        {
            DataTable dtbb = productgroup("BB", "BB PRODUCTS");

            bbproducts.DataSource = dtbb;
            bbproducts.DataBind();
        }
        /// <summary>
        /// Author      :Neha
        /// Date        :11th,dec 2018
        /// Description :Loading 10 yrs product
        /// </summary>
        protected void bindtenyrsprodutcsgrid()
        {
            DataTable dt10yrs = productgroup("10YRS", "10 YRS PRODUCTS");

            tenyrsproducts.DataSource = dt10yrs;
            tenyrsproducts.DataBind();
        }
        /// <summary>
        /// Modified By : Anamika
        /// Date : 9th Feb 2017
        /// Desc : Loaded data without comma in sessions
        /// </summary>
        /// <param name="flag_short_name"></param>
        /// <param name="flag_full_name"></param>
        /// <returns></returns>
        public DataTable productgroup(string flag_short_name, string flag_full_name)
        {

            DataTable dtTopsum = objReports.getSpecialGroupValueSum(null, null, salesengineernumber, customernumber, flag_short_name, null, customertype);
            DataTable dtproduct = new DataTable();
            dtproduct.Columns.Add("FixedRow", typeof(string));
            dtproduct.Columns.Add("sales_value_year_1", typeof(string));
            dtproduct.Columns.Add("sales_value_year_0", typeof(string));
            dtproduct.Columns.Add("estimate_value_next_year", typeof(string));
            dtproduct.Columns.Add("p_sales_value_year_1", typeof(string));
            dtproduct.Columns.Add("ytd", typeof(string));
            dtproduct.Columns.Add("ach", typeof(string));
            dtproduct.Columns.Add("arate", typeof(string));

            DataTable dtproductReport = new DataTable();
            dtproductReport.Columns.Add("FixedRow", typeof(string));
            dtproductReport.Columns.Add("sales_value_year_1", typeof(string));
            dtproductReport.Columns.Add("sales_value_year_0", typeof(string));
            dtproductReport.Columns.Add("estimate_value_next_year", typeof(string));
            dtproductReport.Columns.Add("p_sales_value_year_1", typeof(string));
            dtproductReport.Columns.Add("ytd", typeof(string));
            dtproductReport.Columns.Add("ach", typeof(string));
            dtproductReport.Columns.Add("arate", typeof(string));
            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };
            for (int i = 0; i < dtTopsum.Rows.Count; i++)
            {
                for (int j = 0; j < dtTotals.Rows.Count; j++)
                {
                    if (i == j)
                    {
                        float top0 = dtTopsum.Rows[i].ItemArray[0].ToString() == "" || dtTopsum.Rows[i].ItemArray[0].ToString() == null ? 0    //2013
                            : Convert.ToInt64(dtTopsum.Rows[i].ItemArray[0].ToString());

                        float sum0 = dtTotals.Rows[i].ItemArray[0].ToString() == "" || dtTotals.Rows[i].ItemArray[0].ToString() == null ? 0
                            : Convert.ToInt64(dtTotals.Rows[i].ItemArray[0].ToString());
                        float below0 = sum0 == 0 ? 0 : (top0 / sum0) * 100;

                        float top1 = dtTopsum.Rows[i].ItemArray[1].ToString() == "" || dtTopsum.Rows[i].ItemArray[1].ToString() == null ? 0    //2014
                            : Convert.ToInt64(dtTopsum.Rows[i].ItemArray[1].ToString());
                        float sum1 = dtTotals.Rows[i].ItemArray[1].ToString() == "" || dtTotals.Rows[i].ItemArray[1].ToString() == null ? 0
                            : Convert.ToInt64(dtTotals.Rows[i].ItemArray[1].ToString());
                        float below1 = sum1 == 0 ? 0 : (top1 / sum1) * 100;

                        float top2 = dtTopsum.Rows[i].ItemArray[2].ToString() == "" || dtTopsum.Rows[i].ItemArray[2].ToString() == null ? 0   //2015
                           : Convert.ToInt64(dtTopsum.Rows[i].ItemArray[2].ToString());
                        float sum2 = dtTotals.Rows[i].ItemArray[2].ToString() == "" || dtTotals.Rows[i].ItemArray[2].ToString() == null ? 0
                            : Convert.ToInt64(dtTotals.Rows[i].ItemArray[2].ToString());
                        float below2 = sum2 == 0 ? 0 : (top2 / sum2) * 100;

                        float top3 = top1 == 0 ? 0 : (top2 / top1 - 1) * 100; //15/14

                        float top4 = dtTopsum.Rows[i].ItemArray[3].ToString() == "" || dtTopsum.Rows[i].ItemArray[3].ToString() == null ? 0   //ytd
                            : Convert.ToInt64(dtTopsum.Rows[i].ItemArray[3].ToString());
                        float sum4 = dtTotals.Rows[i].ItemArray[3].ToString() == "" || dtTotals.Rows[i].ItemArray[3].ToString() == null ? 0
                            : Convert.ToInt64(dtTotals.Rows[i].ItemArray[3].ToString());
                        float below4 = sum4 == 0 ? 0 : (top4 / sum4) * 100;

                        //   //ytd/2015
                        //float top5 = top2 == 0 ? 0 : (top4 / top2 - 1) * 100;
                        float top5 = top2 == 0 ? 0 : (top4 / top2) * 100;
                        float top6 = actual_mnth == 0 ? 0 : (top2 - top4) / actual_mnth;


                        top0 = top0 == 0 ? 0 : (top0 / byValueIn);
                        top1 = top1 == 0 ? 0 : (top1 / byValueIn);
                        top2 = top2 == 0 ? 0 : (top2 / byValueIn);
                        top4 = top4 == 0 ? 0 : (top4 / byValueIn);
                        top6 = top6 == 0 ? 0 : (top6 / byValueIn);
                        dtproduct.Rows.Add(flag_full_name, (Math.Round(top0, 0)).ToString("N0", culture), (Math.Round(top1, 0)).ToString("N0", culture), (Math.Round(top2, 0)).ToString("N0", culture), Convert.ToString(Math.Round(top3, 0)) + '%', (Math.Round(top4, 0)).ToString("N0", culture), Convert.ToString(Math.Round(top5, 0)) + '%', (Math.Round(top6, 0)).ToString("N0", culture));
                        dtproduct.Rows.Add("CONTRIBUTION TO SALES", Convert.ToString(Math.Round(below0)) + '%', Convert.ToString(Math.Round(below1)) + '%', Convert.ToString(Math.Round(below2)) + '%', "", Convert.ToString(Math.Round(below4)) + '%');
                        dtproduct.Rows.Add("CONTRIBUTION TO GROWTH");

                        dtproductReport.Rows.Add(flag_full_name, (Math.Round(top0, 0)).ToString(), (Math.Round(top1, 0)).ToString(), (Math.Round(top2, 0)).ToString(), Convert.ToString(Math.Round(top3, 0)) + '%', (Math.Round(top4, 0)).ToString(), Convert.ToString(Math.Round(top5, 0)) + '%', (Math.Round(top6, 0)).ToString());
                        dtproductReport.Rows.Add("CONTRIBUTION TO SALES", Convert.ToString(Math.Round(below0)) + '%', Convert.ToString(Math.Round(below1)) + '%', Convert.ToString(Math.Round(below2)) + '%', "", Convert.ToString(Math.Round(below4)) + '%');
                        dtproductReport.Rows.Add("CONTRIBUTION TO GROWTH");
                    }
                }
            }
            //byfamily
            DataTable dtfamilyBelowSum = dtfamilytotals;
            for (int j = 0; j < dtfamilyBelowSum.Rows.Count; j++)
            {
                string fmname = dtfamilyBelowSum.Rows[j].ItemArray[1].ToString();
                DataTable dtfamilyTopsum = objReports.getSpecialGroupValueSum(null, null, salesengineernumber, customernumber, flag_short_name, fmname, customertype);

                for (int i = 0; i < dtfamilyTopsum.Rows.Count; i++)
                {
                    float top0 = dtfamilyTopsum.Rows[i].ItemArray[0].ToString() == "" || dtfamilyTopsum.Rows[i].ItemArray[0].ToString() == null ? 0
                                 : Convert.ToInt64(dtfamilyTopsum.Rows[i].ItemArray[0].ToString());

                    float sum0 = dtfamilyBelowSum.Rows[j].ItemArray[2].ToString() == "" || dtfamilyBelowSum.Rows[j].ItemArray[2].ToString() == null ? 0
                        : Convert.ToInt64(dtfamilyBelowSum.Rows[j].ItemArray[2].ToString());
                    float below0 = sum0 == 0 ? 0 : (top0 / sum0) * 100;

                    float top1 = dtfamilyTopsum.Rows[i].ItemArray[1].ToString() == "" || dtfamilyTopsum.Rows[i].ItemArray[1].ToString() == null ? 0
                        : Convert.ToInt64(dtfamilyTopsum.Rows[i].ItemArray[1].ToString());
                    float sum1 = dtfamilyBelowSum.Rows[j].ItemArray[3].ToString() == "" || dtfamilyBelowSum.Rows[j].ItemArray[3].ToString() == null ? 0
                        : Convert.ToInt64(dtfamilyBelowSum.Rows[j].ItemArray[3].ToString());
                    float below1 = sum1 == 0 ? 0 : (top1 / sum1) * 100;

                    float top2 = dtfamilyTopsum.Rows[i].ItemArray[2].ToString() == "" || dtfamilyTopsum.Rows[i].ItemArray[2].ToString() == null ? 0
                       : Convert.ToInt64(dtfamilyTopsum.Rows[i].ItemArray[2].ToString());
                    float sum2 = dtfamilyBelowSum.Rows[j].ItemArray[4].ToString() == "" || dtfamilyBelowSum.Rows[j].ItemArray[4].ToString() == null ? 0
                        : Convert.ToInt64(dtfamilyBelowSum.Rows[j].ItemArray[4].ToString());
                    float below2 = sum2 == 0 ? 0 : (top2 / sum2) * 100;

                    float top3 = top1 == 0 ? 0 : (top2 / top1 - 1) * 100;

                    float top4 = dtfamilyTopsum.Rows[i].ItemArray[3].ToString() == "" || dtfamilyTopsum.Rows[i].ItemArray[3].ToString() == null ? 0
                        : Convert.ToInt64(dtfamilyTopsum.Rows[i].ItemArray[3].ToString());
                    float sum4 = dtfamilyBelowSum.Rows[j].ItemArray[5].ToString() == "" || dtfamilyBelowSum.Rows[j].ItemArray[5].ToString() == null ? 0
                        : Convert.ToInt64(dtfamilyBelowSum.Rows[j].ItemArray[5].ToString());
                    float below4 = sum4 == 0 ? 0 : (top4 / sum4) * 100;

                    //ytd/2015
                    //float top5 = top2 == 0 ? 0 : (top4 / top2 - 1) * 100;
                    float top5 = top2 == 0 ? 0 : (top4 / top2) * 100;
                    float top6 = actual_mnth == 0 ? 0 : (top2 - top4) / actual_mnth;
                    top0 = top0 == 0 ? 0 : (top0 / byValueIn);
                    top1 = top1 == 0 ? 0 : (top1 / byValueIn);
                    top2 = top2 == 0 ? 0 : (top2 / byValueIn);
                    top4 = top4 == 0 ? 0 : (top4 / byValueIn);
                    top6 = top0 == 0 ? 0 : (top6 / byValueIn);
                    bool allCoulmnszero = objReports.DeleteEmptyRows(top0, top1, top2, top3, top4, top5, top6);
                    if (allCoulmnszero == false)
                    {
                        dtproduct.Rows.Add(flag_full_name + " - " + fmname, (Math.Round(top0, 0)).ToString("N0", culture), (Math.Round(top1, 0)).ToString("N0", culture), (Math.Round(top2, 0)).ToString("N0", culture), Convert.ToString(Math.Round(top3, 0)) + '%', (Math.Round(top4, 0)).ToString("N0", culture), Convert.ToString(Math.Round(top5, 0)) + '%', (Math.Round(top6, 0)).ToString("N0", culture));
                        dtproduct.Rows.Add("as % of TOTAL" + " " + fmname, Convert.ToString(Math.Round(below0)) + '%', Convert.ToString(Math.Round(below1)) + '%', Convert.ToString(Math.Round(below2)) + '%', "", Convert.ToString(Math.Round(below4)) + '%');
                        dtproductReport.Rows.Add(flag_full_name + " - " + fmname, (Math.Round(top0, 0)).ToString(), (Math.Round(top1, 0)).ToString(), (Math.Round(top2, 0)).ToString(), Convert.ToString(Math.Round(top3, 0)) + '%', (Math.Round(top4, 0)).ToString(), Convert.ToString(Math.Round(top5, 0)) + '%', (Math.Round(top6, 0)).ToString());
                        dtproductReport.Rows.Add("as % of TOTAL" + " " + fmname, Convert.ToString(Math.Round(below0)) + '%', Convert.ToString(Math.Round(below1)) + '%', Convert.ToString(Math.Round(below2)) + '%', "", Convert.ToString(Math.Round(below4)) + '%');
                    }

                }
            }
            if (flag_short_name == "5yrs")
            {
                Session["fiveyrsproducts"] = dtproductReport;
            }
            if (flag_short_name == "SPC")
            {
                Session["spcproducts"] = dtproductReport;
            }
            if (flag_short_name == "TOP")
            {
                Session["topproducts"] = dtproductReport;
            }
            if (flag_short_name == "BB")
            {
                Session["bbproducts"] = dtproductReport;
            }
            if (flag_short_name == "10YRS")
            {
                Session["tenyrsproducts"] = dtproductReport;
            }
            return dtproduct;
        }

        protected void bindgridsalesbyline()
        {
            string branchcode = Session["BranchCode"].ToString();
            string seId = Session["UserId"].ToString();
            string CustType = ddlcustomertype.SelectedItem.Value == "A" ? "ALL" : ddlcustomertype.SelectedItem.Value;
            string custNum = ddlCustomerNumber.SelectedItem.Value;

            DataSet dsTables = (DataSet)objReports.getSalesByLineSum("SF", byValueIn, branchcode, seId, CustType, custNum);
            DataTable dtsubfamilyvalue = new DataTable();
            DataTable dtsubfamilyvalueReport = new DataTable();
            if (dsTables.Tables.Count > 0)
            {
                dtsubfamilyvalue = dsTables.Tables[0];
                dtsubfamilyvalueReport = dsTables.Tables[1];
            }
            DataTable dtsalesbytotal = dtTotals;
            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };
            for (int i = 0; i < dtsalesbytotal.Rows.Count; i++)
            {
                float top0 = dtsalesbytotal.Rows[i].ItemArray[0].ToString() == "" || dtsalesbytotal.Rows[i].ItemArray[0].ToString() == null ? 0
                           : Convert.ToInt64(dtsalesbytotal.Rows[i].ItemArray[0].ToString());
                float top1 = dtsalesbytotal.Rows[i].ItemArray[1].ToString() == "" || dtsalesbytotal.Rows[i].ItemArray[1].ToString() == null ? 0
                            : Convert.ToInt64(dtsalesbytotal.Rows[i].ItemArray[1].ToString());
                float top2 = dtsalesbytotal.Rows[i].ItemArray[2].ToString() == "" || dtsalesbytotal.Rows[i].ItemArray[2].ToString() == null ? 0
                            : Convert.ToInt64(dtsalesbytotal.Rows[i].ItemArray[2].ToString());
                float top3 = dtsalesbytotal.Rows[i].ItemArray[3].ToString() == "" || dtsalesbytotal.Rows[i].ItemArray[3].ToString() == null ? 0
                            : Convert.ToInt64(dtsalesbytotal.Rows[i].ItemArray[3].ToString());
                top0 = top0 == 0 ? 0 : (top0 / byValueIn);
                top1 = top1 == 0 ? 0 : (top1 / byValueIn);
                top2 = top2 == 0 ? 0 : (top2 / byValueIn);
                top3 = top3 == 0 ? 0 : (top3 / byValueIn);
                double top4 = 0, top5 = 0, top6 = 0;
                top4 = top1 == 0 ? 0 : (top2 / top1 - 1) * 100;
                top6 = actual_mnth == 0 ? 0 : (top2 - top3) / actual_mnth;
                top5 = top2 == 0 ? 0 : (top3 / top2) * 100;
                dtsubfamilyvalue.Rows.Add("TOTAL C/T", (Math.Round(top0, 0)).ToString("N0", culture), (Math.Round(top1, 0)).ToString("N0", culture), (Math.Round(top2, 0)).ToString("N0", culture), Convert.ToString(Math.Round(top4, 0)) + '%', (Math.Round(top3, 0)).ToString("N0", culture), Convert.ToString(Math.Round(top5, 0)) + '%', (Math.Round(top6, 0)).ToString("N0", culture));
                dtsubfamilyvalue.Rows.Add("TOTAL C/T", (Math.Round(top0, 0)).ToString(), (Math.Round(top1, 0)).ToString(), (Math.Round(top2, 0)).ToString(), Convert.ToString(Math.Round(top4, 0)) + '%', (Math.Round(top3, 0)).ToString(), Convert.ToString(Math.Round(top5, 0)) + '%', (Math.Round(top6, 0)).ToString());

            }
            Session["salesbylinegrid"] = dtsubfamilyvalueReport;
            salesbylinegrid.DataSource = dtsubfamilyvalue;
            salesbylinegrid.DataBind();


        }
        //region salesbyfamily
        protected void bindgridsalesbyfamily()
        {
            string branchcode = Session["BranchCode"].ToString();
            string seId = Session["UserId"].ToString();
            string CustType = ddlcustomertype.SelectedItem.Value == "A" ? "ALL" : ddlcustomertype.SelectedItem.Value;
            string custNum = ddlCustomerNumber.SelectedItem.Value;

            DataSet dsTables = (DataSet)objReports.getSalesByLineSum("F", byValueIn, branchcode, seId, CustType, custNum);
            DataTable dtsales = new DataTable();
            DataTable dtsalesReport = new DataTable();
            if (dsTables.Tables.Count > 0)
            {
                dtsales = dsTables.Tables[0];
                dtsalesReport = dsTables.Tables[1];
            }
            DataTable dtsalesbytotal = dtTotals;
            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };
            for (int i = 0; i < dtsalesbytotal.Rows.Count; i++)
            {
                float top0 = dtsalesbytotal.Rows[i].ItemArray[0].ToString() == "" || dtsalesbytotal.Rows[i].ItemArray[0].ToString() == null ? 0
                           : Convert.ToInt64(dtsalesbytotal.Rows[i].ItemArray[0].ToString());
                float top1 = dtsalesbytotal.Rows[i].ItemArray[1].ToString() == "" || dtsalesbytotal.Rows[i].ItemArray[1].ToString() == null ? 0
                            : Convert.ToInt64(dtsalesbytotal.Rows[i].ItemArray[1].ToString());
                float top2 = dtsalesbytotal.Rows[i].ItemArray[2].ToString() == "" || dtsalesbytotal.Rows[i].ItemArray[2].ToString() == null ? 0
                            : Convert.ToInt64(dtsalesbytotal.Rows[i].ItemArray[2].ToString());
                float top3 = dtsalesbytotal.Rows[i].ItemArray[3].ToString() == "" || dtsalesbytotal.Rows[i].ItemArray[3].ToString() == null ? 0
                            : Convert.ToInt64(dtsalesbytotal.Rows[i].ItemArray[3].ToString());
                top0 = top0 == 0 ? 0 : (top0 / byValueIn);
                top1 = top1 == 0 ? 0 : (top1 / byValueIn);
                top2 = top2 == 0 ? 0 : (top2 / byValueIn);
                top3 = top3 == 0 ? 0 : (top3 / byValueIn);
                double top4 = 0, top5 = 0, top6 = 0;
                top4 = top1 == 0 ? 0 : (top2 / top1 - 1) * 100;
                top6 = actual_mnth == 0 ? 0 : (top2 - top3) / actual_mnth;
                top5 = top2 == 0 ? 0 : (top3 / top2) * 100;
                dtsales.Rows.Add("TOTAL C/T", (Math.Round(top0, 0)).ToString("N0", culture), (Math.Round(top1, 0)).ToString("N0", culture), (Math.Round(top2, 0)).ToString("N0", culture), Convert.ToString(Math.Round(top4, 0)) + '%', (Math.Round(top3, 0)).ToString("N0", culture), Convert.ToString(Math.Round(top5, 0)) + '%', (Math.Round(top6, 0)).ToString("N0", culture));
                dtsales.Rows.Add("TOTAL C/T", (Math.Round(top0, 0)).ToString(), (Math.Round(top1, 0)).ToString(), (Math.Round(top2, 0)).ToString(), Convert.ToString(Math.Round(top4, 0)) + '%', (Math.Round(top3, 0)).ToString(), Convert.ToString(Math.Round(top5, 0)) + '%', (Math.Round(top6, 0)).ToString());

            }
            Session["salesbyfamilygrid"] = dtsalesReport;
            salesbyfamilygrid.DataSource = dtsales;
            salesbyfamilygrid.DataBind();


        }

        protected void bindgridsalesbyapp()
        {
            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };


            DataSet dsTables = (DataSet)objReports.getsalesbyapp(customernumber, salesengineernumber, customertype, null, null, byValueIn);
            DataTable dtsalesbyapp = new DataTable();
            DataTable dtsalesbyappReport = new DataTable();
            if (dsTables.Tables.Count > 0)
            {
                dtsalesbyapp = dsTables.Tables[0];
                dtsalesbyappReport = dsTables.Tables[1];
            }
            if (dtsalesbyapp.Rows.Count != 0)
            {
                decimal sv_1 = 0, sv_0 = 0, ev = 0, change = 0, ytd = 0, acmnt = 0, askrate = 0;
                for (int i = 0; i < dtsalesbyapp.Rows.Count; i++)
                {
                    sv_1 += dtsalesbyapp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbyapp.Rows[i].ItemArray[2].ToString());
                    sv_0 += dtsalesbyapp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbyapp.Rows[i].ItemArray[3].ToString());
                    ev += dtsalesbyapp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbyapp.Rows[i].ItemArray[4].ToString());
                    ytd += dtsalesbyapp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbyapp.Rows[i].ItemArray[6].ToString());
                }
                change = sv_0 == 0 ? 0 : (ev / sv_0 - 1) * 100;
                acmnt = ev == 0 ? 0 : (ytd / ev) * 100;
                askrate = actual_mnth == 0 ? 0 : (ev - ytd) / actual_mnth;
                dtsalesbyapp.Rows.Add("APPLICATION TOTAL", "", Math.Round(sv_1).ToString("N0", culture), Math.Round(sv_0).ToString("N0", culture), Math.Round(ev).ToString("N0", culture), Math.Round(change) + "%", Math.Round(ytd), Math.Round(acmnt) + "%", Math.Round(askrate).ToString("N0", culture));
                dtsalesbyapp.Rows.Add("APPLICATION TOTAL", "", Math.Round(sv_1).ToString(), Math.Round(sv_0).ToString(), Math.Round(ev).ToString(), Math.Round(change) + "%", Math.Round(ytd), Math.Round(acmnt) + "%", Math.Round(askrate).ToString());
            }
            Session["salesbyapp"] = dtsalesbyappReport;
            salesbyapp.DataSource = dtsalesbyapp;
            salesbyapp.DataBind();
        }

        #region Anantha
        protected void bindgridsalesbyappQty()
        {

            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };

            DataSet dsTables = objReports.getsalesbyappQty(customernumber, salesengineernumber, customertype, null, null, byValueIn);
            DataTable dtsalesbyapp = new DataTable();
            DataTable dtsalesbyappReport = new DataTable();
            if (dsTables.Tables.Count > 0)
            {
                dtsalesbyapp = dsTables.Tables[0];
                dtsalesbyappReport = dsTables.Tables[1];
            }
            if (dtsalesbyapp.Rows.Count != 0)
            {
                decimal sv_1 = 0, sv_0 = 0, ev = 0, change = 0, ytd = 0, acmnt = 0, askrate = 0;
                for (int i = 0; i < dtsalesbyapp.Rows.Count; i++)
                {
                    sv_1 += dtsalesbyapp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbyapp.Rows[i].ItemArray[2].ToString());
                    sv_0 += dtsalesbyapp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbyapp.Rows[i].ItemArray[3].ToString());
                    ev += dtsalesbyapp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbyapp.Rows[i].ItemArray[4].ToString());
                    ytd += dtsalesbyapp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbyapp.Rows[i].ItemArray[6].ToString());
                }
                change = sv_0 == 0 ? 0 : (ev / sv_0 - 1) * 100;
                acmnt = ev == 0 ? 0 : (ytd / ev) * 100;
                askrate = actual_mnth == 0 ? 0 : (ev - ytd) / actual_mnth;
                dtsalesbyapp.Rows.Add("APPLICATION TOTAL", "", Math.Round(sv_1).ToString("N0", culture), Math.Round(sv_0).ToString("N0", culture), Math.Round(ev).ToString("N0", culture), Math.Round(change) + "%", Math.Round(ytd).ToString("N0", culture), Math.Round(acmnt) + "%", Math.Round(askrate).ToString("N0", culture));
                dtsalesbyappReport.Rows.Add("APPLICATION TOTAL", "", Math.Round(sv_1).ToString(), Math.Round(sv_0).ToString(), Math.Round(ev).ToString(), Math.Round(change) + "%", Math.Round(ytd).ToString(), Math.Round(acmnt) + "%", Math.Round(askrate).ToString());
            }
            Session["salesbyapp_qty"] = dtsalesbyappReport;
            salesbyapp_qty.DataSource = dtsalesbyapp;
            salesbyapp_qty.DataBind();
        }
        #endregion

        protected void bindgridsalesbycustomer()
        {

            string se_id = Session["UserId"].ToString();
            string cust_type = ddlcustomertype.SelectedItem.Value == "A" ? null : ddlcustomertype.SelectedItem.Value;
            string custname = ddlCustomerList.SelectedItem.Value == "ALL" ? null : ddlCustomerList.SelectedItem.Value;
            string cust_num = ddlCustomerNumber.SelectedItem.Value == "ALL" ? null : ddlCustomerNumber.SelectedItem.Value;
            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };
            if (cust_type == null)
            {
                DataTable dtcust = new DataTable();
                DataTable dtcustReport = new DataTable();
                /*Case:Customer Toatl*/
                DataSet dsTables = (DataSet)objReports.cust_total(null, se_id, "C", cust_num, byValueIn);
                DataTable dtsalesbycust = new DataTable();
                DataTable dtsalesbycustReport = new DataTable();
                if (dsTables.Tables.Count > 0)
                {
                    dtsalesbycust = dsTables.Tables[0];
                    dtsalesbycustReport = dsTables.Tables[1];
                }
                if (dtsalesbycust.Rows.Count != 0)
                {
                    decimal sv_1 = 0, sv_0 = 0, ev = 0, change = 0, ytd = 0, acmnt = 0, askrate = 0;
                    for (int i = 0; i < dtsalesbycust.Rows.Count; i++)
                    {
                        sv_1 += dtsalesbycust.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbycust.Rows[i].ItemArray[2].ToString());
                        sv_0 += dtsalesbycust.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbycust.Rows[i].ItemArray[3].ToString());
                        ev += dtsalesbycust.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbycust.Rows[i].ItemArray[4].ToString());
                        ytd += dtsalesbycust.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbycust.Rows[i].ItemArray[6].ToString());
                    }
                    change = sv_0 == 0 ? 0 : (ev / sv_0 - 1) * 100;
                    acmnt = ev == 0 ? 0 : (ytd / ev) * 100;
                    askrate = actual_mnth == 0 ? 0 : (ev - ytd) / actual_mnth;
                    dtsalesbycust.Rows.Add("CUSTOMER TOTAL", "", Math.Round(sv_1).ToString("N0", culture), Math.Round(sv_0).ToString("N0", culture), Math.Round(ev).ToString("N0", culture), Math.Round(change) + "%", Math.Round(ytd).ToString("N0", culture), Math.Round(acmnt) + "%", Math.Round(askrate).ToString("N0", culture));
                    dtsalesbycustReport.Rows.Add("CUSTOMER TOTAL", "", Math.Round(sv_1).ToString(), Math.Round(sv_0).ToString(), Math.Round(ev).ToString(), Math.Round(change) + "%", Math.Round(ytd).ToString(), Math.Round(acmnt) + "%", Math.Round(askrate).ToString());
                }

                dtcust.Merge(dtsalesbycust);
                dtcustReport.Merge(dtsalesbycustReport);
                dsTables = (DataSet)objReports.cust_total(null, se_id, "D", cust_num, byValueIn);
                DataTable dtsalesbydlr = new DataTable();
                DataTable dtsalesbydlrReport = new DataTable();
                if (dsTables.Tables.Count > 0)
                {
                    dtsalesbydlr = dsTables.Tables[0];
                    dtsalesbydlrReport = dsTables.Tables[1];
                }
                if (dtsalesbydlr.Rows.Count != 0)
                {
                    decimal sv_1 = 0, sv_0 = 0, ev = 0, change = 0, ytd = 0, acmnt = 0, askrate = 0;
                    for (int i = 0; i < dtsalesbydlr.Rows.Count; i++)
                    {
                        sv_1 += dtsalesbydlr.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbydlr.Rows[i].ItemArray[2].ToString());
                        sv_0 += dtsalesbydlr.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbydlr.Rows[i].ItemArray[3].ToString());
                        ev += dtsalesbydlr.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbydlr.Rows[i].ItemArray[4].ToString());
                        ytd += dtsalesbydlr.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbydlr.Rows[i].ItemArray[6].ToString());
                    }
                    change = sv_0 == 0 ? 0 : (ev / sv_0 - 1) * 100;
                    acmnt = ev == 0 ? 0 : (ytd / ev) * 100;
                    askrate = actual_mnth == 0 ? 0 : (ev - ytd) / actual_mnth;
                    dtsalesbydlr.Rows.Add("CHANNEL PARTNER TOTAL", "", Math.Round(sv_1).ToString("N0", culture), Math.Round(sv_0).ToString("N0", culture), Math.Round(ev).ToString("N0", culture), Math.Round(change) + "%", Math.Round(ytd).ToString("N0", culture), Math.Round(acmnt) + "%", Math.Round(askrate).ToString("N0", culture));
                    dtsalesbydlrReport.Rows.Add("CHANNEL PARTNER TOTAL", "", Math.Round(sv_1).ToString(), Math.Round(sv_0).ToString(), Math.Round(ev).ToString(), Math.Round(change) + "%", Math.Round(ytd).ToString(), Math.Round(acmnt) + "%", Math.Round(askrate).ToString());
                }

                dtcust.Merge(dtsalesbydlr);
                dtcustReport.Merge(dtsalesbydlrReport);
                if (dtcust.Rows.Count != 0)
                {
                    decimal sv_1 = 0, sv_0 = 0, ev = 0, change = 0, ytd = 0, acmnt = 0, askrate = 0;
                    for (int i = 0; i < dtcust.Rows.Count; i++)
                    {
                        if (dtcust.Rows[i].ItemArray[0].ToString() != "CUSTOMER TOTAL" && dtcust.Rows[i].ItemArray[0].ToString() != "CHANNEL PARTNER TOTAL")
                        {
                            sv_1 += dtcust.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtcust.Rows[i].ItemArray[2].ToString());
                            sv_0 += dtcust.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtcust.Rows[i].ItemArray[3].ToString());
                            ev += dtcust.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtcust.Rows[i].ItemArray[4].ToString());
                            ytd += dtcust.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtcust.Rows[i].ItemArray[6].ToString());
                        }

                    }
                    change = sv_0 == 0 ? 0 : (ev / sv_0 - 1) * 100;
                    acmnt = ev == 0 ? 0 : (ytd / ev) * 100;
                    askrate = actual_mnth == 0 ? 0 : (ev - ytd) / actual_mnth;
                    dtcust.Rows.Add("GRAND TOTAL", "", Math.Round(sv_1).ToString("N0", culture), Math.Round(sv_0).ToString("N0", culture), Math.Round(ev).ToString("N0", culture), Math.Round(change) + "%", Math.Round(ytd).ToString("N0", culture), Math.Round(acmnt) + "%", Math.Round(askrate).ToString("N0", culture));
                    dtcust.Rows.Add("GRAND TOTAL", "", Math.Round(sv_1).ToString(), Math.Round(sv_0).ToString(), Math.Round(ev).ToString(), Math.Round(change) + "%", Math.Round(ytd).ToString(), Math.Round(acmnt) + "%", Math.Round(askrate).ToString());
                }

                Session["salesbycustomer"] = dtcustReport;
                salesbycustomer.DataSource = dtcust;
                salesbycustomer.DataBind();
            }
            else
            {
                string totaltxt = null;
                if (cust_type == "C")
                {
                    totaltxt = "CUSTOMER TOTAL";
                }
                else
                {
                    totaltxt = "CHANNEL PARTNER TOTAL";
                }
                DataTable dtcust = new DataTable();
                DataTable dtcustReport = new DataTable();
                /*Case:Customer Toatl*/
                DataSet dsTables = (DataSet)objReports.cust_total(null, se_id, cust_type, cust_num, byValueIn);
                DataTable dtsalesbycust = new DataTable();
                DataTable dtsalesbycustReport = new DataTable();
                if (dsTables.Tables.Count > 0)
                {
                    dtsalesbycust = dsTables.Tables[0];
                    dtsalesbycustReport = dsTables.Tables[1];
                }
                if (dtsalesbycust.Rows.Count != 0)
                {
                    decimal sv_1 = 0, sv_0 = 0, ev = 0, change = 0, ytd = 0, acmnt = 0, askrate = 0;
                    for (int i = 0; i < dtsalesbycust.Rows.Count; i++)
                    {
                        sv_1 += dtsalesbycust.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbycust.Rows[i].ItemArray[2].ToString());
                        sv_0 += dtsalesbycust.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbycust.Rows[i].ItemArray[3].ToString());
                        ev += dtsalesbycust.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbycust.Rows[i].ItemArray[4].ToString());
                        ytd += dtsalesbycust.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtsalesbycust.Rows[i].ItemArray[6].ToString());
                    }
                    change = sv_0 == 0 ? 0 : (ev / sv_0 - 1) * 100;
                    acmnt = ev == 0 ? 0 : (ytd / ev) * 100;
                    askrate = actual_mnth == 0 ? 0 : (ev - ytd) / actual_mnth;
                    dtsalesbycust.Rows.Add(totaltxt, "", Math.Round(sv_1).ToString("N0", culture), Math.Round(sv_0).ToString("N0", culture), Math.Round(ev).ToString("N0", culture), Math.Round(change) + "%", Math.Round(ytd).ToString("N0", culture), Math.Round(acmnt).ToString("N0", culture) + "%", Math.Round(askrate));
                }

                //DataTable dtcusttotal = objReports.custtype_total(null, "HO", "C");
                //dtsalesbycust.Merge(dtcusttotal);
                dtcust.Merge(dtsalesbycust);


                if (dtcust.Rows.Count != 0)
                {
                    decimal sv_1 = 0, sv_0 = 0, ev = 0, change = 0, ytd = 0, acmnt = 0, askrate = 0;
                    for (int i = 0; i < dtcust.Rows.Count; i++)
                    {
                        if (dtcust.Rows[i].ItemArray[0].ToString() != "CUSTOMER TOTAL" && dtcust.Rows[i].ItemArray[0].ToString() != "CHANNEL PARTNER TOTAL")
                        {
                            sv_1 += dtcust.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtcust.Rows[i].ItemArray[2].ToString());
                            sv_0 += dtcust.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtcust.Rows[i].ItemArray[3].ToString());
                            ev += dtcust.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtcust.Rows[i].ItemArray[4].ToString());
                            ytd += dtcust.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtcust.Rows[i].ItemArray[6].ToString());
                        }
                    }
                    change = sv_0 == 0 ? 0 : (ev / sv_0 - 1) * 100;
                    acmnt = ev == 0 ? 0 : (ytd / ev) * 100;
                    askrate = actual_mnth == 0 ? 0 : (ev - ytd) / actual_mnth;
                    dtcust.Rows.Add("GRAND TOTAL", "", Math.Round(sv_1).ToString("N0", culture), Math.Round(sv_0).ToString("N0", culture), Math.Round(ev).ToString("N0", culture), Math.Round(change) + "%", Math.Round(ytd).ToString("N0", culture), Math.Round(acmnt) + "%", Math.Round(askrate).ToString("N0", culture));
                    dtcust.Rows.Add("GRAND TOTAL", "", Math.Round(sv_1).ToString(), Math.Round(sv_0).ToString(), Math.Round(ev).ToString(), Math.Round(change) + "%", Math.Round(ytd).ToString(), Math.Round(acmnt) + "%", Math.Round(askrate).ToString());
                }

                Session["salesbycustomer"] = dtcustReport;
                salesbycustomer.DataSource = dtcust;
                salesbycustomer.DataBind();
            }


        }



        protected void bindgridColor()
        {
            if (goldproducts.Rows.Count != 0)
            {
                int colorIndex = 0;

                foreach (GridViewRow row in goldproducts.Rows)
                {
                    row.Cells[0].CssClass = GetCSS(colorIndex);
                    if (colorIndex < 3)
                    {
                        colorIndex++;
                    }
                    else { colorIndex = 1; }
                }
            }
            if (spcproducts.Rows.Count != 0)
            {
                int colorIndex = 0;

                foreach (GridViewRow row in spcproducts.Rows)
                {
                    row.Cells[0].CssClass = GetCSS(colorIndex);
                    if (colorIndex < 2)
                    {
                        colorIndex++;
                    }
                    else { colorIndex = 1; }
                }
            }
            if (fiveyrsproducts.Rows.Count != 0)
            {
                int colorIndex = 0;

                foreach (GridViewRow row in fiveyrsproducts.Rows)
                {
                    row.Cells[0].CssClass = GetCSS(colorIndex);
                    if (colorIndex < 2)
                    {
                        colorIndex++;
                    }
                    else { colorIndex = 1; }
                }
            }
            if (topproducts.Rows.Count != 0)
            {
                int colorIndex = 0;

                foreach (GridViewRow row in topproducts.Rows)
                {
                    row.Cells[0].CssClass = GetCSS(colorIndex);
                    if (colorIndex < 2)
                    {
                        colorIndex++;
                    }
                    else { colorIndex = 1; }
                }
            }
            if (bbproducts.Rows.Count != 0)
            {
                int colorIndex = 0;

                foreach (GridViewRow row in bbproducts.Rows)
                {
                    row.Cells[0].CssClass = GetCSS(colorIndex);
                    if (colorIndex < 2)
                    {
                        colorIndex++;
                    }
                    else { colorIndex = 1; }
                }
            }

            if (tenyrsproducts.Rows.Count != 0)
            {
                int colorIndex = 0;

                foreach (GridViewRow row in tenyrsproducts.Rows)
                {
                    row.Cells[0].CssClass = GetCSS(colorIndex);
                    if (colorIndex < 2)
                    {
                        colorIndex++;
                    }
                    else { colorIndex = 1; }
                }
            }

            if (salesbylinegrid.Rows.Count != 0)
            {
                int colorIndex = 1;

                foreach (GridViewRow row in salesbylinegrid.Rows)
                {
                    row.Cells[0].CssClass = GetCSS(colorIndex);
                    if (colorIndex < 2)
                    {
                        colorIndex++;
                    }
                    else { colorIndex = 1; }
                }
            }
            if (salesbyfamilygrid.Rows.Count != 0)
            {
                int colorIndex = 1;

                foreach (GridViewRow row in salesbyfamilygrid.Rows)
                {
                    row.Cells[0].CssClass = GetCSS(colorIndex);
                    if (colorIndex < 2)
                    {
                        colorIndex++;
                    }
                    else { colorIndex = 1; }
                }
            }
            if (salesbyapp.Rows.Count != 0)
            {
                int colorIndex = 1;

                foreach (GridViewRow row in salesbyapp.Rows)
                {
                    row.Cells[0].CssClass = GetCSS(colorIndex);
                    row.Cells[1].CssClass = GetCSS(colorIndex);
                    if (colorIndex < 2)
                    {
                        colorIndex++;
                    }
                    else { colorIndex = 1; }
                }
            }
            if (salesbyapp_qty.Rows.Count != 0)
            {
                int colorIndex = 1;

                foreach (GridViewRow row in salesbyapp_qty.Rows)
                {
                    row.Cells[0].CssClass = GetCSS(colorIndex);
                    row.Cells[1].CssClass = GetCSS(colorIndex);
                    if (colorIndex < 2)
                    {
                        colorIndex++;
                    }
                    else { colorIndex = 1; }
                }
            }
            if (salesbycustomer.Rows.Count != 0)
            {
                int colorIndex = 1;

                foreach (GridViewRow row in salesbycustomer.Rows)
                {
                    row.Cells[0].CssClass = GetCSS(colorIndex);
                    row.Cells[1].CssClass = GetCSS(colorIndex);
                    if (colorIndex < 2)
                    {
                        colorIndex++;
                    }
                    else { colorIndex = 1; }
                }
            }

        }
        protected void LoadCSS()
        {
            cssList.Add("color_3");
            cssList.Add("color_4");
            cssList.Add("color_5");
            cssList.Add("color_4");
            cssList.Add("color_2");
            cssList.Add("greendark");
        }
        protected string GetCSS(int colorIndex)
        {
            string index = Convert.ToString(colorIndex);
            string cIndex = index[index.Length - 1].ToString();

            if (cIndex.Contains("1"))
            { return cssList.ElementAt(1); }
            else if (cIndex.Contains("2"))
            { return cssList.ElementAt(2); }
            else if (cIndex.Contains("3"))
            { return cssList.ElementAt(3); }
            else if (cIndex.Contains("4"))
            { return cssList.ElementAt(4); }
            //else if (cIndex.Contains("5"))
            //{ return cssList.ElementAt(5); }
            else { return cssList.ElementAt(5); }

        }




        #region Anantha
        protected void salesbyapp_qty_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.Header)
            {
                //ActualValue
                int BudgetYear = objConfig.getBudgetYear();
                int ActualYear = objConfig.getActualYear() - 1;
                int ActualMonth = objConfig.getActualMonth();
                string Year = "";
                if (ActualMonth == 12) { Year = ActualYear + ""; }
                else { Year = Year = ActualYear + "P"; }// ActualYear + "P"; }

                string ActualValueYear2 = (ActualYear - 2).ToString();
                string ActualValueyear1 = (ActualYear - 1).ToString();
                string ActualValueNextYear = (ActualYear + 1).ToString();
                //string QtyIn = " QTY";

                string QtyIn = " QTY(000)";
                if (byValueIn == 100000)
                {
                    QtyIn = " QTY(00,000)";
                }

                else if (byValueIn == 1)
                {
                    QtyIn = " QTY";
                }



                e.Row.Cells[2].Text = ActualValueyear1 + "<br/> " + QtyIn;
                e.Row.Cells[3].Text = Year + "<br/> " + QtyIn;
                e.Row.Cells[4].Text = BudgetYear + "B" + "<br/>" + QtyIn;
                e.Row.Cells[5].Text = BudgetYear + "B" + "/" + Year + "<br/> % CHANGE";
                e.Row.Cells[6].Text = BudgetYear + " " + "YTD" + "<br/> " + QtyIn;
                e.Row.Cells[7].Text = "YTD" + "/" + BudgetYear + "B" + "<br/> % ACH";
                e.Row.Cells[8].Text = BudgetYear + "B" + " " + "Ask.Rate" + "<br/> " + QtyIn;
            }

        }
        #endregion

        private byte[] Compress(byte[] b)
        {
            MemoryStream ms = new MemoryStream();
            GZipStream zs = new GZipStream(ms, CompressionMode.Compress, true);
            zs.Write(b, 0, b.Length);
            zs.Close();
            return ms.ToArray();
        }

        /// This method takes the compressed byte stream as parameter
        /// and return a decompressed bytestream.

        private byte[] Decompress(byte[] b)
        {
            MemoryStream ms = new MemoryStream();
            GZipStream zs = new GZipStream(new MemoryStream(b),
                                           CompressionMode.Decompress, true);
            byte[] buffer = new byte[4096];
            int size;
            while (true)
            {
                size = zs.Read(buffer, 0, buffer.Length);
                if (size > 0)
                    ms.Write(buffer, 0, size);
                else break;
            }
            zs.Close();
            return ms.ToArray();
        }

        protected override object LoadPageStateFromPersistenceMedium()
        {
            System.Web.UI.PageStatePersister pageStatePersister1 = this.PageStatePersister;
            pageStatePersister1.Load();
            String vState = pageStatePersister1.ViewState.ToString();
            byte[] pBytes = System.Convert.FromBase64String(vState);
            pBytes = Decompress(pBytes);
            LosFormatter mFormat = new LosFormatter();
            Object ViewState = mFormat.Deserialize(System.Convert.ToBase64String(pBytes));
            return new Pair(pageStatePersister1.ControlState, ViewState);
        }

        protected override void SavePageStateToPersistenceMedium(Object pViewState)
        {
            Pair pair1;
            System.Web.UI.PageStatePersister pageStatePersister1 = this.PageStatePersister;
            Object ViewState;
            if (pViewState is Pair)
            {
                pair1 = ((Pair)pViewState);
                pageStatePersister1.ControlState = pair1.First;
                ViewState = pair1.Second;
            }
            else
            {
                ViewState = pViewState;
            }
            LosFormatter mFormat = new LosFormatter();
            StringWriter mWriter = new StringWriter();
            mFormat.Serialize(mWriter, ViewState);
            String mViewStateStr = mWriter.ToString();
            byte[] pBytes = System.Convert.FromBase64String(mViewStateStr);
            pBytes = Compress(pBytes);
            String vStateStr = System.Convert.ToBase64String(pBytes);
            pageStatePersister1.ViewState = vStateStr;
            pageStatePersister1.Save();
        }
        #endregion
    }
}
