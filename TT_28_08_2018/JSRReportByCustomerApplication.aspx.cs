﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TaegutecSalesBudget
{
    public partial class JSRReportByCustomerApplication : System.Web.UI.Page
    {
        #region Global Declaration
        csJSR objJSR = new csJSR();
        csJSRDAL objJSRDAL = new csJSRDAL();
        CommonFunctions objCom = new CommonFunctions();
        List<string> cssList = new List<string>();
        List<string> cssListFamilyHead = new List<string>();
          Review objRSum = new Review();
           AdminConfiguration objAdmin = new AdminConfiguration();

        #endregion

        #region Events
        /// <summary>
        /// Author:
        /// Date:
        /// Desc: we are retriving user id,value In  from session and Loading Reports
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            
            if (!IsPostBack)
            {
                if (Session["ValueIn"] == null) { Session["ValueIn"] = 1000; rbtn_Thousand.Checked = true; }
                else if (Convert.ToInt32(Session["ValueIn"]) == 100000) rbtn_Lakhs.Checked = true;
                LoadReport();
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            //else
            //{
            //    LoadReport();
            //}
        }

        /// <summary>
        /// Author: 
        /// Date:
        /// Desc:Export in Excel
        /// ModifiedBy:K.LakshmiBindu
        /// Date:13 Dec,2018
        /// Desc: product group excel export based on configurations in admin page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void exportexcel_Click(object sender, EventArgs e)
        {

            List<string> PgList = null;
            int count = 3;
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                //LoadReport(null, null);
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "JSRReportByApplication_Family.xls"));
                Response.ContentType = "application/ms-excel";
                DataTable dtExcel = new DataTable();
                dtExcel = (DataTable)Session["dtCustomerApplication"];

                DataTable dtFamilyExport;
                dtFamilyExport = dtExcel.Clone();
                DataRow drFirst = dtExcel.Rows[0];
                dtFamilyExport.Rows.Add(drFirst.ItemArray);
                DataRow[] dr = dtExcel.Select("sumFlag ='FamilySum'");
                foreach (DataRow dr1 in dr)
                {
                    dtFamilyExport.Rows.Add(dr1.ItemArray);
                }
                DataTable dtExport;
                dtExport = dtFamilyExport.Copy();

                //DataTable dtExport;
                //dtExport = dtExcel.Copy();
                dtExport.Columns.Remove("ID");
                dtExport.Columns.Remove("FAMILY_ID");
                dtExport.Columns.Remove("SUBFAMILY_ID");
                dtExport.Columns.Remove("I_T_O_FLAG");
                dtExport.Columns.Remove("sumFlag");
                dtExport.Columns.Remove("DISPLAY_VALUE");
                dtExport.Columns.Remove("Line_desc");
                if (Session["RoleId"].ToString() != "HO" || Convert.ToString(dtExport.Rows[1]["ShowGP"]) == "0")
                {
                    dtExport.Columns.Remove("GP_MONTHLY");
                    dtExport.Columns.Remove("GP_YEARLY");
                }
                dtExport.Columns.Remove("APPLICATION_CODE");
                dtExport.Columns["TEN_YEARS_FLAG"].SetOrdinal(6);
                dtExport.Columns.Remove("GOLD_FLAG");
                dtExport.Columns.Remove("TOP_FLAG");
                dtExport.Columns.Remove("TEN_YEARS_FLAG");
                dtExport.Columns.Remove("FIVE_YEARS_FLAG");
                dtExport.Columns.Remove("BB_FLAG");
                dtExport.Columns.Remove("SPC_FLAG");

                PgList = Session["PgList"] as System.Collections.Generic.List<string>;
                for (int i = 0; i < PgList.Count; i++)
                {
                    count = count + 1;
                    int rowIdx = 0;
                    switch (PgList[i])
                    {
                        case "GOLD":
                            DataColumn dg = dtExport.Columns.Add("GOLD_FLAG");
                            dg.SetOrdinal(count);
                            dtExport.AsEnumerable().All(row => { row["GOLD_FLAG"] = dtFamilyExport.Rows[rowIdx++]["GOLD_FLAG"]; return true; });
                            break;
                        case "TOP":
                            DataColumn dt = dtExport.Columns.Add("TOP_FLAG");
                            dt.SetOrdinal(count);
                            dtExport.AsEnumerable().All(row => { row["TOP_FLAG"] = dtFamilyExport.Rows[rowIdx++]["TOP_FLAG"]; return true; });
                            break;
                        case "BB":
                            DataColumn dbb = dtExport.Columns.Add("BB_FLAG");
                            dbb.SetOrdinal(count);
                            dtExport.AsEnumerable().All(row => { row["BB_FLAG"] = dtFamilyExport.Rows[rowIdx++]["BB_FLAG"]; return true; });
                            break;
                        case "FIVE YEARS":
                            DataColumn df = dtExport.Columns.Add("FIVE_YEARS_FLAG");
                            df.SetOrdinal(count);
                            dtExport.AsEnumerable().All(row => { row["FIVE_YEARS_FLAG"] = dtFamilyExport.Rows[rowIdx++]["FIVE_YEARS_FLAG"]; return true; });
                            break;
                        case "TEN YEARS":
                            DataColumn dten = dtExport.Columns.Add("TEN_YEARS_FLAG");
                            dten.SetOrdinal(count);
                            dtExport.AsEnumerable().All(row => { row["TEN_YEARS_FLAG"] = dtFamilyExport.Rows[rowIdx++]["TEN_YEARS_FLAG"]; return true; });
                            break;
                        case "SPC":
                            DataColumn dtspc = dtExport.Columns.Add("SPC_FLAG");
                            dtspc.SetOrdinal(count);
                            dtExport.AsEnumerable().All(row => { row["SPC_FLAG"] = dtFamilyExport.Rows[rowIdx++]["SPC_FLAG"]; return true; });
                            break;
                    }
                }




                objJSR.jsr_month = Convert.ToInt32(Request.QueryString["month"]);
                objJSR.jsr_year = Convert.ToInt32(Request.QueryString["year"]);
                if (Convert.ToString(Request.QueryString["branch"]) != null)
                    objJSR.branch = Convert.ToString("'" + Request.QueryString["branch"] + "'");
                else
                    objJSR.branch = Convert.ToString(Session["ExportBranchList"]);
                if (Convert.ToString(Request.QueryString["se"]) != null)
                    objJSR.salesengineer_id = Convert.ToString("'" + Request.QueryString["se"] + "'");
                else
                    objJSR.salesengineer_id = Convert.ToString(Session["ExportSalesEngineers"]);
                if (Convert.ToString(Request.QueryString["cnum"]) != null)
                    objJSR.c_num = Convert.ToString("'" + Request.QueryString["cnum"] + "'");
                else
                    objJSR.c_num = Convert.ToString(Session["ExportCustomers"]);

                objJSR.customer_type = Convert.ToString(Session["ddlcustomertype.SelectedItem.Value"]);
                objJSR.family = Convert.ToString(Session["ExportFamily"]);
                objJSR.subfamily = Convert.ToString(Session["ExportSubFamily"]);
                objJSR.application = Convert.ToString(Session["ExportApp"]);
                objJSR.p_group = Convert.ToString(Session["ExportProductGroup"]);
                objJSR.cter = Convert.ToString(Session["cter"]);

                GridView grdExportExcel = new GridView();
                grdExportExcel.DataSource = dtExport;
                grdExportExcel.DataBind();

                using (StringWriter sw = new StringWriter())
                {
                    using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                    {
                        //  Create a table to contain the grid
                        Table table = new Table();
                        table.GridLines = grdExportExcel.GridLines;

                        foreach (GridViewRow row in grdExportExcel.Rows)
                        {
                            table.Rows.Add(row);
                        }

                        table.Rows[0].Height = 30;
                        table.Rows[0].BackColor = Color.LightSeaGreen;
                        table.Rows[0].Font.Bold = true;

                        sw.WriteLine("<table><tr><td></td><td></td><td></td><td colspan=3 style='font-weight: bold; font-size:20px'>JSR Report By Application Summary</td></table>");
                        sw.WriteLine("<table style='margin-left: 200px;'>");


                        sw.WriteLine("<tr><td></td><td></td><td></td><td style='font-weight: bold;'>TERRITORY :" + "</td><td colspan=8 style='font-style: italic;'>" + objJSR.cter + "</td></tr>");

                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>MONTH :" + "</td><td colspan=8 style='font-style: italic; text-align: left;'>" + objJSR.jsr_month + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>YEAR :" + "</td><td colspan=8 style='font-style: italic; text-align: left;'>" + objJSR.jsr_year + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>BRANCH :" + "</td><td colspan=8 style='font-style: italic; '>" + objJSR.branch + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>SALES ENGINEERS :" + "</td><td colspan=8 style='font-style: italic; '>" + objJSR.salesengineer_id + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>CUSTOMER TYPE : " + "</td><td colspan=8 style='font-style: italic;'>" + objJSR.customer_type + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>CUSTOMER NAME :" + "</td><td colspan=8 style='font-style: italic; '>" + objJSR.c_num + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>PRODUCT GROUP :" + "</td><td colspan=8 style='font-style: italic;'>" + objJSR.p_group + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>PRODUCT FAMILY :" + "</td><td colspan=8 style='font-style: italic;'>" + objJSR.family + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>PRODUCT SUB-FAMILY :" + "</td><td colspan=8 style='font-style: italic; '>" + objJSR.subfamily + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>APPLICATION :" + "</td><td colspan=8 style='font-style: italic;'>" + objJSR.application + "</td></tr>");
                        sw.WriteLine("</table><br/>");
                        //sw.WriteLine("TERRITORY :" + "" + objJSR.cter + "<br/>");
                        //sw.WriteLine("MONTH :" + "" + objJSR.jsr_month + "<br/>");
                        //sw.WriteLine("YEAR :" + "" + objJSR.jsr_year + "<br/>");
                        //sw.WriteLine("BRANCH :" + "" + objJSR.branch + "<br/>");
                        //sw.WriteLine("SALES ENGINEERS :" + "" + objJSR.salesengineer_id + "<br/>");
                        //sw.WriteLine("CUSTOMER TYPE : " + "" + objJSR.customer_type + "<br/>");
                        //sw.WriteLine("CUSTOMER NAME :" + "" + objJSR.c_num + "<br/>");
                        //sw.WriteLine("PRODUCT GROUP :" + "" + objJSR.p_group + "<br/>" + "<br/>");
                        //sw.WriteLine("PRODUCT FAMILY :" + "" + objJSR.family + "<br/>" + "<br/>");
                        //sw.WriteLine("PRODUCT SUB-FAMILY :" + "" + objJSR.subfamily + "<br/>" + "<br/>");
                        //sw.WriteLine("APPLICATION :" + "" + objJSR.application + "<br/>" + "<br/>");

                        table.RenderControl(htw);
                    }

                    Response.Write(sw.ToString());
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

                    Response.End();
                }


            }
            catch (Exception ex)
            {
                // objFunc.LogError(ex);
            }

        }


        /// <summary>
        /// Author : Anamika
        /// Date : July 10, 2017
        /// Desc : Export in Excel family wise
        /// ModifiedBy:K.LakshmiBindu
        /// Date:13 Dec, 2018
        /// Desc: Displaying Product groups columns in excel based on configurations in admin page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void exportfamily_Click(object sender, EventArgs e)
        {
            List<string> PgList = null;
            int count = 3;
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "JSRReportByApplication.xls"));
                Response.ContentType = "application/ms-excel";
                DataTable dtExcel = new DataTable();
                dtExcel = (DataTable)Session["dtCustomerApplication"];

                DataTable dtExport;
                dtExport = dtExcel.Copy();
                dtExport.Columns.Remove("ID");
                dtExport.Columns.Remove("FAMILY_ID");
                dtExport.Columns.Remove("SUBFAMILY_ID");
                dtExport.Columns.Remove("I_T_O_FLAG");
                dtExport.Columns.Remove("sumFlag");
                dtExport.Columns.Remove("DISPLAY_VALUE");
                dtExport.Columns.Remove("Line_Desc");
                  dtExport.Columns.Remove("ShowGP");
                  dtExport.Columns.Add("ShowGP");
                if (Session["RoleId"].ToString() != "HO" || Convert.ToString(dtExport.Rows[1]["ShowGP"]) == "0")
                {
                    dtExport.Columns.Remove("GP_MONTHLY");
                    dtExport.Columns.Remove("GP_YEARLY");
                }
                dtExport.Columns["TEN_YEARS_FLAG"].SetOrdinal(6);
                dtExport.Columns.Remove("GOLD_FLAG");
                dtExport.Columns.Remove("TOP_FLAG");
                dtExport.Columns.Remove("TEN_YEARS_FLAG");
                dtExport.Columns.Remove("FIVE_YEARS_FLAG");
                dtExport.Columns.Remove("BB_FLAG");
                dtExport.Columns.Remove("SPC_FLAG");
                PgList = Session["PgList"] as System.Collections.Generic.List<string>;
                for (int i = 0; i < PgList.Count; i++)
                {
                    count = count + 1;
                    int rowIdx = 0;
                    switch (PgList[i])
                    {
                        case "GOLD":
                            DataColumn dg = dtExport.Columns.Add("GOLD_FLAG");
                            dg.SetOrdinal(count);
                            dtExport.AsEnumerable().All(row => { row["GOLD_FLAG"] = dtExcel.Rows[rowIdx++]["GOLD_FLAG"]; return true; });
                            break;
                        case "TOP":
                            DataColumn dt = dtExport.Columns.Add("TOP_FLAG");
                            dt.SetOrdinal(count);
                            dtExport.AsEnumerable().All(row => { row["TOP_FLAG"] = dtExcel.Rows[rowIdx++]["TOP_FLAG"]; return true; });
                            break;
                        case "BB":
                            DataColumn dbb = dtExport.Columns.Add("BB_FLAG");
                            dbb.SetOrdinal(count);
                            dtExport.AsEnumerable().All(row => { row["BB_FLAG"] = dtExcel.Rows[rowIdx++]["BB_FLAG"]; return true; });
                            break;
                        case "FIVE YEARS":
                            DataColumn df = dtExport.Columns.Add("FIVE_YEARS_FLAG");
                            df.SetOrdinal(count);
                            dtExport.AsEnumerable().All(row => { row["FIVE_YEARS_FLAG"] = dtExcel.Rows[rowIdx++]["FIVE_YEARS_FLAG"]; return true; });
                            break;
                        case "TEN YEARS":
                            DataColumn dten = dtExport.Columns.Add("TEN_YEARS_FLAG");
                            dten.SetOrdinal(count);
                            dtExport.AsEnumerable().All(row => { row["TEN_YEARS_FLAG"] = dtExcel.Rows[rowIdx++]["TEN_YEARS_FLAG"]; return true; });
                            break;
                        case "SPC":
                            DataColumn dtspc = dtExport.Columns.Add("SPC_FLAG");
                            dtspc.SetOrdinal(count);
                            dtExport.AsEnumerable().All(row => { row["SPC_FLAG"] = dtExcel.Rows[rowIdx++]["SPC_FLAG"]; return true; });
                            break;
                    }
                }
                int rowIdxs = 0;
                dtExport.AsEnumerable().All(row => { row["ShowGP"] = dtExcel.Rows[rowIdxs++]["ShowGP"]; return true; });
              
                objJSR.jsr_month = Convert.ToInt32(Request.QueryString["month"]);
                objJSR.jsr_year = Convert.ToInt32(Request.QueryString["year"]);
                if (Convert.ToString(Request.QueryString["branch"]) != null)
                    objJSR.branch = Convert.ToString("'" + Request.QueryString["branch"] + "'");
                else
                    objJSR.branch = Convert.ToString(Session["ExportBranchList"]);
                if (Convert.ToString(Request.QueryString["se"]) != null)
                    objJSR.salesengineer_id = Convert.ToString("'" + Request.QueryString["se"] + "'");
                else
                    objJSR.salesengineer_id = Convert.ToString(Session["ExportSalesEngineers"]);
                if (Convert.ToString(Request.QueryString["cnum"]) != null)
                    objJSR.c_num = Convert.ToString("'" + Request.QueryString["cnum"] + "'");
                else
                    objJSR.c_num = Convert.ToString(Session["ExportCustomers"]);

                objJSR.customer_type = Convert.ToString(Session["ddlcustomertype.SelectedItem.Value"]);
                objJSR.family = Convert.ToString(Session["ExportFamily"]);
                objJSR.subfamily = Convert.ToString(Session["ExportSubFamily"]);
                objJSR.application = Convert.ToString(Session["ExportApp"]);
                objJSR.p_group = Convert.ToString(Session["ExportProductGroup"]);
                objJSR.cter = Convert.ToString(Session["cter"]);
                GridView grdExportExcel = new GridView();
                grdExportExcel.DataSource = dtExport;
                grdExportExcel.DataBind();

                using (StringWriter sw = new StringWriter())
                {
                    using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                    {
                        //  Create a table to contain the grid
                        Table table = new Table();
                        table.GridLines = grdExportExcel.GridLines;

                        foreach (GridViewRow row in grdExportExcel.Rows)
                        {
                            row.Attributes.Add("font-weight", "bold");
                            table.Rows.Add(row);
                        }
                        table.Rows[0].Height = 30;
                        table.Rows[0].BackColor = Color.LightSeaGreen;
                        table.Rows[0].Font.Bold = true;

                        for (int i = 0; i < table.Rows.Count; i++)
                        {
                            if (Convert.ToString(table.Rows[i].Cells[3].Text).Contains("CUTTING TOOLS TOTAL"))
                            {
                                table.Rows[i].BackColor = Color.LightCyan;
                                table.Rows[i].Font.Bold = true;
                            }
                            else if (Convert.ToString(table.Rows[i].Cells[3].Text).Contains("TOTAL"))
                            {
                                if (Convert.ToString(table.Rows[i].Cells[3].Text).Contains("INSERTS") || Convert.ToString(table.Rows[i].Cells[3].Text).Contains("TOOLS"))
                                {
                                    table.Rows[i].BackColor = Color.LightGray;
                                    table.Rows[i].Font.Bold = true;
                                }
                                else if (Convert.ToString(table.Rows[i].Cells[3].Text).Contains("FAMILY"))
                                {
                                    table.Rows[i].BackColor = Color.Black;
                                    table.Rows[i].ForeColor = Color.White;
                                    table.Rows[i].Font.Bold = true;
                                }
                                else
                                {
                                    table.Rows[i].BackColor = Color.Gray;
                                    // table.Rows[i].ForeColor = Color.White;
                                    table.Rows[i].Font.Bold = true;
                                }
                            }
                        }
                        sw.WriteLine("<table><tr><td></td><td></td><td></td><td colspan=3 style='font-weight: bold; font-size:20px; '>JSR Report By Application</td></table>");
                        sw.WriteLine("<table style='margin-left: 200px;'>");

                            sw.WriteLine("<tr><td></td><td></td><td></td><td style='font-weight: bold;'>TERRITORY :" + "</td><td colspan=8 style='font-style: italic;'>" + objJSR.cter + "</td></tr>");
                        
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>MONTH :" + "</td><td colspan=8 style='font-style: italic; text-align: left;'>" + objJSR.jsr_month + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>YEAR :" + "</td><td colspan=8 style='font-style: italic; text-align: left;'>" + objJSR.jsr_year + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>BRANCH :" + "</td><td colspan=8 style='font-style: italic; '>" + objJSR.branch + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>SALES ENGINEERS :" + "</td><td colspan=8 style='font-style: italic; '>" + objJSR.salesengineer_id + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>CUSTOMER TYPE : " + "</td><td colspan=8 style='font-style: italic;'>" + objJSR.customer_type + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>CUSTOMER NAME :" + "</td><td colspan=8 style='font-style: italic; '>" + objJSR.c_num + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>PRODUCT GROUP :" + "</td><td colspan=8 style='font-style: italic;'>" + objJSR.p_group + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>PRODUCT FAMILY :" + "</td><td colspan=8 style='font-style: italic;'>" + objJSR.family + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>PRODUCT SUB-FAMILY :" + "</td><td colspan=8 style='font-style: italic; '>" + objJSR.subfamily + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>APPLICATION :" + "</td><td colspan=8 style='font-style: italic;'>" + objJSR.application + "</td></tr>");
                        sw.WriteLine("</table><br/>");
                        table.RenderControl(htw);

                        //sw.WriteLine("TERRITORY :" + "" + objJSR.cter + "<br/>");
                        //sw.WriteLine("MONTH :" + "" + objJSR.jsr_month + "<br/>");
                        //sw.WriteLine("YEAR :" + "" + objJSR.jsr_year + "<br/>");
                        //sw.WriteLine("BRANCH :" + "" + objJSR.branch + "<br/>");
                        //sw.WriteLine("SALES ENGINEERS :" + "" + objJSR.salesengineer_id + "<br/>");
                        //sw.WriteLine("CUSTOMER TYPE : " + "" + objJSR.customer_type + "<br/>");
                        //sw.WriteLine("CUSTOMER NAME :" + "" + objJSR.c_num + "<br/>");
                        //sw.WriteLine("PRODUCT GROUP :" + "" + objJSR.p_group + "<br/>" + "<br/>");
                        //sw.WriteLine("PRODUCT FAMILY :" + "" + objJSR.family + "<br/>" + "<br/>");
                        //sw.WriteLine("PRODUCT SUB-FAMILY :" + "" + objJSR.subfamily + "<br/>" + "<br/>");
                        //sw.WriteLine("APPLICATION :" + "" + objJSR.application + "<br/>" + "<br/>");

                        //table.RenderControl(htw);
                    }

                    Response.Write(sw.ToString());
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

                    Response.End();
                }


            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("Thread") == false)
                {
                    //Response.Redirect("~/Error.aspx?ErrorMsg = " + ex.Message);
                }
                // objFunc.LogError(ex);
            }

        }

        /// <summary>
        /// Author : 
        /// Date   : 
        /// Desc   : Clicking on "000" value, all the value will be displayed in thousands
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Thousand_CheckedChanged(object sender, EventArgs e)
        {
           Session["ValueIn"] = 1000;
           LoadReport();
           ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        /// <summary>
        /// Author :
        /// Date :
        /// Desc : Clicking on "lakh" value, all the value will be displayed in lakhs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Lakhs_CheckedChanged(object sender, EventArgs e)
        {
            Session["ValueIn"] = 100000;
            LoadReport();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }
        #endregion

        #region Methods
        /// <summary>
        /// Author:
        /// Date:
        /// Desc: Loading GridView Based on values that are passed  from Query String
        /// </summary>
        protected void LoadReport()
        {
            DataTable dtReport = new DataTable();
            try
            {
                objJSR.jsr_month = Convert.ToInt32(Request.QueryString["month"]);
                objJSR.jsr_year = Convert.ToInt32(Request.QueryString["year"]);
                if (Convert.ToString(Request.QueryString["branch"]) != null)
                    objJSR.branch = Convert.ToString("'" + Request.QueryString["branch"] + "'");
                else
                    objJSR.branch = Convert.ToString(Session["SelectedBranchList"]);
                if (Convert.ToString(Request.QueryString["se"]) != null)
                    objJSR.salesengineer_id = Convert.ToString("'" + Request.QueryString["se"] + "'");
                else
                    objJSR.salesengineer_id = Convert.ToString(Session["SelectedSalesEngineers"]);
                if (Convert.ToString(Request.QueryString["cnum"]) != null)
                    objJSR.c_num = Convert.ToString("'" + Request.QueryString["cnum"] + "'");
                else
                    objJSR.c_num = Convert.ToString(Session["SelectedCustomerNumbers"]);

                objJSR.customer_type = Convert.ToString(Session["ddlcustomertype.SelectedItem.Value"]);
                objJSR.family = Convert.ToString(Session["SelectedProductFamily"]);
                objJSR.subfamily = Convert.ToString(Session["SelectedSubFamily"]);
                objJSR.application = Convert.ToString(Session["SelectedApplications"]);
                objJSR.p_group = Convert.ToString(Session["SelectedProductGroup"]);
                objJSR.cter = Convert.ToString(Session["cter"]);
                objJSR.fiterby = "APPLICATION";
                objJSR.valueIn = Convert.ToInt32(Session["ValueIn"]);
                objJSR.roleId = Convert.ToString(Session["RoleId"]);
                LoadCSS();
                dtReport = objJSRDAL.getReport(objJSR);
                dtReport.Columns.Add("ShowGP");
                var col = dtReport.Columns["ShowGP"];
                if (Convert.ToBoolean(Request.QueryString["flag"]) == true)
                {
                    foreach (DataRow row in dtReport.Rows)
                        row[col] = 1;
                }
                else
                {
                    foreach (DataRow row in dtReport.Rows)
                        row[col] = 0;
                }
                if (dtReport.Rows.Count > 0)
                {
                    if (objJSR.valueIn == 100000)
                        lblResult.Text = "All vlaues are in lakhs.";
                    else
                        lblResult.Text = "All vlaues are in thousands.";
                    grdviewAllValues.DataSource = dtReport;
                    grdviewAllValues.DataBind();
                    bindgridColor();
                    Session["dtCustomerApplication"] = dtReport;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                }
                else
                {
                    lblResult.Text = "No data to display.";
                    grdviewAllValues.DataSource = null;
                    grdviewAllValues.DataBind();
                }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }
        /// <summary>
        /// Author:LakshmiBindu
        /// Date: 12 Dec,2018
        /// Desc: Loads Product Groups based on cofigurations set in AdminPage and are used for setting products groups visibility in gridview
        /// </summary>
        public void LoadProductGroups()
        {
              int B_Budgetyear =0;
             DataTable ProductGroups=null;
            try{
                B_Budgetyear = Convert.ToInt32(objAdmin.GetProfile("B_BUDGET_YEAR"));

                ProductGroups = objRSum.GetProductGroups(B_Budgetyear);

                List<string> PgList = new List<string>();

                for (int i = 0; i < ProductGroups.Rows.Count; i++)
                {
                    PgList.Add(Convert.ToString(ProductGroups.Rows[i][1]));
                    if (Convert.ToString(ProductGroups.Rows[i][1]) == "FIVE YEARS")
                    {
                        ProductGroups.Rows[i][1] = "5YRS";
                    }
                    else if (Convert.ToString(ProductGroups.Rows[i][1]) == "TEN YEARS")
                    {
                        ProductGroups.Rows[i][1] = "10YRS";
                    }
                }
                Session["PgList"] = PgList;
           
            }
            catch(Exception ex)
            {
                objCom.LogError(ex);
            }
        }
        /// <summary>
        /// Author:
        /// Date:
        /// Desc : Used for Binding colors to Gridview 
        /// </summary>
        protected void bindgridColor()
        {
            LoadProductGroups();
            if (grdviewAllValues.Rows.Count != 0)
            {
                List<string> PgList = null;
                int colorIndex = 0;
                int rowIndex = 1, subRowIndex = 0, productTypeIndex = 1; int color = 0, parent_row_index = 0;
                bool currentRowIsLast = false;
                foreach (GridViewRow row in grdviewAllValues.Rows)
                {

                    var check = row.FindControl("lblSumFlag") as Label;
                    string txt = check.Text;

                    PgList = Session["PgList"] as System.Collections.Generic.List<string>;
                    for (int i = 0; i < PgList.Count; i++)
                    {
                        switch (PgList[i])
                        {
                            case "GOLD":
                                var lblgold = row.FindControl("lblgold") as Label;
                                lblgold.Visible = true;
                                break;
                            case "TOP":
                                var lbltop = row.FindControl("lbltop") as Label;
                                lbltop.Visible = true;
                                break;
                            case "BB":
                                var lblBB = row.FindControl("lblbb") as Label;
                                lblBB.Visible = true;
                                break;
                            case "FIVE YEARS":
                                var lbl5yrs = row.FindControl("lbl5yrs") as Label;
                                lbl5yrs.Visible = true;
                                break;
                            case "TEN YEARS":
                                var lbltenyrs = row.FindControl("lbltenyrs") as Label;
                                lbltenyrs.Visible = true;
                                break;
                            case "SPC":
                                var lblspc = row.FindControl("lblspc") as Label;
                                lblspc.Visible = true;
                                break;
                        }
                    }

                        


                    if (txt == "typeSum")
                    {
                        row.CssClass = "product_row_hide subTotalRowGrid subrowindex subrowindex_" + subRowIndex + " ";

                        productTypeIndex++;
                    }
                    else if (txt == "SubFamilySum")
                    {
                        row.CssClass = "product_row_hide greendarkSubFamSum subrowindex subrowindex_" + subRowIndex;
                    }
                    else if (txt == "SubFamilyHeading")
                    {
                        row.CssClass = "product_row_hide greendark row_index row_" + rowIndex + " parent_row_index_" + parent_row_index;

                        var image = row.FindControl("Image1") as System.Web.UI.WebControls.Image;
                        image.CssClass = "row_index_image";

                        row.Attributes["data-index"] = rowIndex.ToString();
                        rowIndex++;
                        subRowIndex++;
                    }
                    else if (txt == "products")
                    {
                        color++;
                        if (color == 1) { row.CssClass = "color_Product1 "; }
                        else if (color == 2)
                        {
                            row.CssClass = "color_Product2 ";
                            color = 0;
                        }

                        row.CssClass += "product_row_hide product_type_" + productTypeIndex + " subrowindex subrowindex_" + subRowIndex;
                    }
                    else if (txt == "FamilyHeading")
                    {
                        row.CssClass = "parent_row_index";
                        row.Attributes["data-parent_row_index"] = "" + (++parent_row_index);

                        if (colorIndex <= 6)
                        {
                            for (int i = 0; i < row.Cells.Count; i++)
                            {
                                row.Cells[0].CssClass = "TotalRowGridHead1 " + GetCssFam(colorIndex); ;
                                row.Cells[i].CssClass = "TotalRowGridHead " + GetCssFam(colorIndex); ;
                            }
                            //row.CssClass = GetCssFam(colorIndex); 
                            colorIndex++;
                        }
                        else
                        {
                            colorIndex = 0;
                            for (int i = 0; i < row.Cells.Count; i++)
                            {
                                row.Cells[0].CssClass = "TotalRowGridHead1 " + GetCssFam(colorIndex); ;
                                row.Cells[i].CssClass = "TotalRowGridHead " + GetCssFam(colorIndex); ;
                            }
                        }



                    }
                    else if (txt == "BranchHeading")
                    {
                        row.CssClass = "parent_row_index";
                        row.Attributes["data-parent_row_index"] = "" + (++parent_row_index);

                        if (colorIndex <= 6)
                        {
                            for (int i = 0; i < row.Cells.Count; i++)
                            {
                                row.Cells[0].CssClass = "TotalRowGridHead1 " + GetCssFam(colorIndex); ;
                                row.Cells[i].CssClass = "TotalRowGridHead " + GetCssFam(colorIndex); ;
                            }
                            //row.CssClass = GetCssFam(colorIndex); 
                            colorIndex++;
                        }
                        else
                        {
                            colorIndex = 0;
                            for (int i = 0; i < row.Cells.Count; i++)
                            {
                                row.Cells[0].CssClass = "TotalRowGridHead1 " + GetCssFam(colorIndex); ;
                                row.Cells[i].CssClass = "TotalRowGridHead " + GetCssFam(colorIndex); ;
                            }
                        }



                    }
                    else if (txt == "SEHeading")
                    {
                        row.CssClass = "product_row_hide greendark row_index row_" + rowIndex + " parent_row_index_" + parent_row_index;

                        var image = row.FindControl("Image1") as System.Web.UI.WebControls.Image;
                        image.CssClass = "row_index_image";

                        row.Attributes["data-index"] = rowIndex.ToString();
                        rowIndex++;
                        subRowIndex++;
                    }
                    else if (txt == "FamilySum")
                    {
                        row.CssClass = "product_row_hide TotalRowGrid" + " parent_row_index_" + parent_row_index;
                        currentRowIsLast = true;

                    }
                    else if (txt == "MainSum")
                    {
                        row.CssClass = "MainTotal";
                    }

                    else if (txt == "")
                    {
                        row.CssClass = "empty_row";
                        if (!currentRowIsLast)
                        {
                            row.CssClass += " product_row_hide ";
                        }
                        currentRowIsLast = false;
                        for (int i = 0; i < row.Cells.Count; i++)
                        {
                            row.Cells[i].CssClass = "HidingHeading";
                        }
                    }
                    else if (txt == "HidingHeading")
                    {

                        for (int i = 0; i < row.Cells.Count; i++)
                        { row.Cells[i].CssClass = "greendark MainHeader"; }
                    }
                    else if (txt == "sumFlag")
                    {
                        for (int i = 0; i < row.Cells.Count; i++)
                        { row.Cells[i].CssClass = "greendark MainHeader"; }
                    }

                }
            }




        }
        /// <summary>
        /// Author:
        /// Date:
        /// Desc: Used for loading Css to the GridView
        /// </summary>
        protected void LoadCSS()
        {
            cssList.Add("color_3");
            cssList.Add("color_4");
            cssList.Add("color_3");
            //cssList.Add("color_5");
            //cssList.Add("color_4");
            //cssList.Add("color_2");
            //cssList.Add("greendark");

            cssListFamilyHead.Add("heading1");
            cssListFamilyHead.Add("heading2");
            cssListFamilyHead.Add("heading3");
            cssListFamilyHead.Add("heading4");
            cssListFamilyHead.Add("heading5");
            cssListFamilyHead.Add("heading6");
            cssListFamilyHead.Add("heading7");

        }
        /// <summary>
        /// Author:
        /// Date:
        /// Desc: add css class to the rows of gridview based on the row index we are passing
        /// </summary>
        /// <param name="colorIndex">based on the row number we are choosing which CSS class to assaign to the row</param>
        /// <returns>css class</returns>
        protected string GetCSS(int colorIndex)
        {
            string index = Convert.ToString(colorIndex);
            string cIndex = index[index.Length - 1].ToString();

            if (cIndex.Contains("1"))
            { return cssList.ElementAt(1); }
            else if (cIndex.Contains("2"))
            { return cssList.ElementAt(2); }
            //else if (cIndex.Contains("3"))
            //{ return cssList.ElementAt(3); }
            //else if (cIndex.Contains("4"))
            //{ return cssList.ElementAt(4); }
            //else if (cIndex.Contains("5"))
            //{ return cssList.ElementAt(5); }
            else { return cssList.ElementAt(2); }

        }
        /// <summary>
        /// Author : 
        /// Date : 
        /// Desc : Retriews CSS class bases on the row index that we pass from BindGridColor method for FamilyHeading
        /// </summary>
        /// <param name="colorIndex"></param>
        /// <returns>css class</returns>
        protected string GetCssFam(int colorIndex)
        {
            string index = Convert.ToString(colorIndex);
            string cIndex = index[index.Length - 1].ToString();

            if (cIndex.Contains("1"))
            { return cssListFamilyHead.ElementAt(1); }
            else if (cIndex.Contains("2"))
            { return cssListFamilyHead.ElementAt(2); }
            else if (cIndex.Contains("3"))
            { return cssListFamilyHead.ElementAt(3); }
            else if (cIndex.Contains("4"))
            { return cssListFamilyHead.ElementAt(4); }
            else if (cIndex.Contains("5"))
            { return cssListFamilyHead.ElementAt(5); }
            else if (cIndex.Contains("6"))
            { return cssListFamilyHead.ElementAt(6); }
            else { return cssListFamilyHead.ElementAt(0); }
        }

        #endregion
        
    }
}