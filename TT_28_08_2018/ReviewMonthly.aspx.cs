﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.IO.Compression;
using System.Globalization;
//using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System.Text;
using iTextSharp.tool.xml;
using System.Web.Services;
using System.Collections.Specialized;
//using iTextSharp.text.html.simpleparser;
//using iTextSharp.text.pdf;

namespace TaegutecSalesBudget
{
    public partial class ReviewMonthly : System.Web.UI.Page
    {

        #region Global Declaration

        AdminConfiguration objAdmin = new AdminConfiguration();
        Budget objBudget = new Budget();
        AdminConfiguration objConfig = new AdminConfiguration();
        Reports objReports = new Reports();
        Review objRSum = new Review();
        CommonFunctions objCom = new CommonFunctions();
        public static int gridLoadedStatus;
        public static string Sbranchlist;
        public static string cter;
        public static int tegutecheckedchanged;
        public static int BudgetYear;
        #endregion

        #region Events
        /// <summary>
        /// Author :
        /// Created date :
        /// Description :Loading dropdownlist of branch,sales engineer list, customer names, customer numbers, application list, product groups based on user roles

        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                BudgetYear = objConfig.getBudgetYear();
                hdnYear.Value = Convert.ToString(BudgetYear);

                if (!IsPostBack)
                {
                    Session["datatable"] = "1";
                    //   Session["hdnClick"] = null;
                    chartdisplay.Value = "0";
                    cter = null;
                    gridLoadedStatus = 0;
                    string name_desc = "", name_code = "";
                    int count = 0;
                    string strUserId = Session["UserId"].ToString();
                    string roleId = Session["RoleId"].ToString();
                    hdnSearch.Value = roleId;
                    //load product family details
                    if (Session["RoleId"].ToString() == "HO" || Session["RoleId"].ToString() == "TM")
                    {
                        if (Session["RoleId"].ToString() == "HO")
                        {
                            if (Session["cter"] == null && roleId == "HO")
                            {
                                Session["cter"] = "TTA";
                                cter = "TTA";

                            }

                            if (Session["cter"].ToString() == "DUR")
                            {
                                rdBtnDuraCab.Checked = true;
                                rdBtnTaegutec.Checked = false;
                                cter = "DUR";
                            }
                            else
                            {
                                rdBtnTaegutec.Checked = true;
                                rdBtnDuraCab.Checked = false;
                                cter = "TTA";
                            }
                            cterDiv.Visible = true;
                        }

                        LoadBranches();

                        BranchList_SelectedIndexChanged(null, null);
                        SalesEngList_SelectedIndexChanged(null, null);
                        ddlcustomertype_SelectedIndexChanged(null, null);
                        // ApplicationList_SelectedIndexChanged(null, null);
                        //reports_Click(null, null);

                    }

                    else if (Session["RoleId"].ToString() == "BM")
                    {
                        string username = Session["UserName"].ToString();
                        string branchcode = Session["BranchCode"].ToString();
                        string branchDec = Session["BranchDesc"].ToString();
                        Session["SelectedBranchList"] = "'" + branchcode + "'";
                       
                        //bind branch
                        BranchList.Items.Insert(0, branchcode);
                        divBranch.Visible = false;

                        // sales engineers loading
                        DataTable dtSalesEngDetails = objReports.LoadUserInfo(null, branchcode);
                        if (dtSalesEngDetails != null)
                        {
                            SalesEngList.DataSource = dtSalesEngDetails;
                            SalesEngList.DataTextField = "EngineerName";
                            SalesEngList.DataValueField = "EngineerId";
                            SalesEngList.DataBind();
                            //ddlSalesEngineerList.Items.Insert(0, "SELECT SALES ENGINEER");
                            // ChkSalesEng.Items.Insert(0, "ALL");
                        }

                        foreach (ListItem val in SalesEngList.Items)
                        {
                            val.Selected = true;
                            if (val.Selected)
                            {
                                count++;
                                name_desc += val.Text + " , ";
                                name_code += val.Value + "','";
                            }
                        }

                        SalesEngList_SelectedIndexChanged(null, null);


                    }
                    else if (Session["RoleId"].ToString() == "SE")
                    {

                        string username = Session["UserName"].ToString();
                        string branchcode = Session["BranchCode"].ToString();
                        string branchDec = Session["BranchDesc"].ToString();
                        Session["SelectedBranchList"] = "'" + branchcode + "'";
                        Session["SelectedSalesEngineers"] = "'" + strUserId + "'";

                        //bind branch
                        BranchList.Items.Insert(0, branchcode);
                        divBranch.Visible = false;
                        //bind Sales engineer
                        SalesEngList.Items.Insert(0, strUserId);
                        divSE.Visible = false;

                        // customers loading
                        DataTable dtCutomerDetails = objBudget.LoadCustomerDetails(strUserId, "SE"); ;
                        if (dtCutomerDetails != null)
                        {
                            DataTable dtDeatils = new DataTable();
                            dtDeatils.Columns.Add("customer_number", typeof(string));
                            dtDeatils.Columns.Add("customer_name", typeof(string));
                            for (int i = 0; i < dtCutomerDetails.Rows.Count; i++)
                            {
                                dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[1].ToString(), dtCutomerDetails.Rows[i].ItemArray[2].ToString() + "(" + dtCutomerDetails.Rows[i].ItemArray[1].ToString() + ")");
                            }
                            CustNameList.DataSource = dtDeatils;
                            CustNameList.DataTextField = "customer_name";
                            CustNameList.DataValueField = "customer_number";
                            CustNameList.DataBind();
                            //ddlCustomerList.Items.Insert(0, "-- SELECT CUSTOMER --");
                            //ChkCustName.Items.Insert(0, "ALL");

                            CustNumList.DataSource = dtCutomerDetails;
                            CustNumList.DataTextField = "customer_number";
                            CustNumList.DataValueField = "customer_number";
                            CustNumList.DataBind();
                            //ddlCustomerNumber.Items.Insert(0, "-- SELECT CUSTOMER NUMBER --");
                            //ChkCustNum.Items.Insert(0, "ALL");

                            foreach (ListItem val in CustNameList.Items)
                            {
                                val.Selected = true;
                                if (val.Selected)
                                {
                                    count++;
                                    name_desc += val.Text + " , ";
                                    name_code += val.Value + "','";
                                }
                            }

                            foreach (ListItem val in CustNumList.Items)
                            {
                                val.Selected = true;
                                if (val.Selected)
                                {
                                    count++;
                                    name_desc += val.Text + " , ";
                                    name_code += val.Value + "','";
                                }
                            }

                            CustNameList_SelectedIndexChanged(null, null);
                            CustNumList_SelectedIndexChanged(null, null);
                        }

                    }

                    LoadProductFamliy();
                    LoadProductGroup();
                    ProductGrpList_SelectedIndexChanged(null, null);

                }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }
        /// <summary>
        /// Author :
        /// Created date :
        /// Description : For Exporting chart and gridview to PDF
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ExportToImage(object sender, EventArgs e)
        {
            try
            {
                string filepath = string.Empty;

                if (rbtn_value.Checked)
                    filepath = Convert.ToString(HttpContext.Current.Session["PDF_file"]);

                else
                    filepath = Convert.ToString(HttpContext.Current.Session["PDF_qty_file"]);
                System.Web.HttpResponse response = System.Web.HttpContext.Current.Response;
                response.ClearContent();
                response.Clear();
                response.ContentType = "text/plain";
                response.AddHeader("Content-Disposition",
                                   "attachment; filename=" + filepath + ";");
                response.TransmitFile(filepath);
                response.Flush();
                response.End();
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }


        /// <summary>
        /// Author :
        /// Created date :
        /// Description :  click on branches dropdownlist, se should be loaded accordingly and selected value should be stored in session for further use
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void BranchList_SelectedIndexChanged(object sender, EventArgs e)
        {
            int count = 0;

            string name_desc = "", name_code = "";
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                //    int[] a = BranchList.GetSelectedIndices();
                //     int c = a.Length;


              
                foreach (ListItem val in BranchList.Items)
                {
                    if (val.Selected)
                    {
                        count++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                    }
                }


                name_code = "'" + name_code;

                string branchlist = name_code.Substring(0, Math.Max(0, name_code.Length - 2));

                if (count == BranchList.Items.Count)
                {
                    Session["SelectedBranchList"] = "ALL";
                }
                else
                {
                    Session["SelectedBranchList"] = branchlist;
                }
                string roleId = Session["RoleId"].ToString();
                string userId = Session["UserId"].ToString();
                string branchcode = Convert.ToString(Session["SelectedBranchList"]);
                objRSum.BranchCode = roleId == "TM" && branchcode == "ALL" ? userId : branchcode;
                objRSum.roleId = roleId;
                objRSum.flag = "SalesEngineer";
                objRSum.cter = cter;
                DataTable dtData = objRSum.getFilterAreaValue(objRSum);

                if (dtData.Rows.Count != 0)
                {
                    SalesEngList.DataSource = dtData;
                    SalesEngList.DataTextField = "EngineerName";
                    SalesEngList.DataValueField = "EngineerId";
                    SalesEngList.DataBind();
                    // ChkSalesEng.Items.Insert(0, "ALL");
                }
                else
                {
                    SalesEngList.DataSource = dtData;
                    SalesEngList.DataTextField = "EngineerName";
                    SalesEngList.DataValueField = "EngineerId";
                    SalesEngList.DataBind();
                    //ChkSalesEng.Items.Insert(0, "NO SALES ENGINEER");
                }
                foreach (ListItem val in SalesEngList.Items)
                {
                    val.Selected = true;

                    count++;
                    name_desc += val.Text + " , ";
                    name_code += val.Value + "','";

                }


                SalesEngList_SelectedIndexChanged(null, null);



                DataTable dt = ViewState["Chart"] as DataTable;
                Chart2.Visible = false;
                Chart3.Visible = false;


                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }

        /// <summary>
        /// Author :
        /// Created date :
        /// Description : click on se Dropdownlist, customers should be loaded accordingly and selected value should be stored in session for further use
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void SalesEngList_SelectedIndexChanged(object sender, EventArgs e)
        {

            string name_desc = "", name_code = "";
            int count = 0;
            string SalesengList = null;
            string SalesengnameList = null; ;
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
               
                foreach (ListItem val in SalesEngList.Items)
                {
                    if (val.Selected)
                    {
                        count++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                    }
                }

                name_code = "'" + name_code;

                SalesengList = name_code.Substring(0, Math.Max(0, name_code.Length - 2));
                SalesengnameList = name_desc.Substring(0, Math.Max(0, name_desc.Length - 2));

                if (count == SalesEngList.Items.Count)
                {
                    Session["SelectedSalesEngineers"] = "ALL";
                }
                else
                {
                    Session["SelectedSalesEngineers"] = SalesengList;
                }


                string roleId = Session["RoleId"].ToString();
                string userId = Session["UserId"].ToString();
                string branchcode = Convert.ToString(Session["SelectedBranchList"]);
                objRSum.BranchCode = (roleId == "TM" && BranchList.SelectedItem.Value == "ALL") ? userId : branchcode;
                objRSum.salesengineer_id = SalesengList.ToString() == "ALL" || SalesengList.ToString() == "" ? null : SalesengList;


                objRSum.customer_type = ddlcustomertype.SelectedItem.Value;
                objRSum.roleId = roleId;
                objRSum.flag = "CustomerType";
                objRSum.cter = cter;
                DataTable dtData = objRSum.getFilterAreaValue(objRSum);

                if (dtData.Rows.Count != 0)
                {
                    CustNameList.DataSource = dtData;
                    CustNameList.DataTextField = "customer_short_name";
                    CustNameList.DataValueField = "customer_number";
                    CustNameList.DataBind();
                    // ChkCustName.Items.Insert(0, "ALL");
                }
                else
                {
                    CustNameList.DataSource = dtData;
                    CustNameList.DataTextField = "customer_short_name";
                    CustNameList.DataValueField = "customer_number";
                    CustNameList.DataBind();
                    // ChkCustName.Items.Insert(0, "NO CUSTOMER");
                }
                if (dtData.Rows.Count != 0)
                {
                    CustNumList.DataSource = dtData;
                    CustNumList.DataTextField = "customer_number";
                    CustNumList.DataValueField = "customer_number";
                    CustNumList.DataBind();
                    // ChkCustNum.Items.Insert(0, "ALL");
                }
                else
                {
                    CustNumList.DataSource = dtData;
                    CustNumList.DataTextField = "customer_number";
                    CustNumList.DataValueField = "customer_number";
                    CustNumList.DataBind();
                    //  ChkCustNum.Items.Insert(0, "NO CUSTOMER");
                }

                foreach (ListItem val in CustNameList.Items)
                {
                    val.Selected = true;
                    if (val.Selected)
                    {
                        count++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                    }
                }

                foreach (ListItem val in CustNumList.Items)
                {
                    val.Selected = true;
                    if (val.Selected)
                    {
                        count++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                    }
                }

                CustNameList_SelectedIndexChanged(null, null);
                CustNumList_SelectedIndexChanged(null, null);


                DataTable dt = ViewState["Chart"] as DataTable;
                Chart2.Visible = false;
                Chart3.Visible = false;

                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        /// <summary>
        /// Author :
        /// Created date :
        /// Description : click on cust_name checkbox, cust_no should be checked accordingly and selected value should be stored in session for further use
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CustNameList_SelectedIndexChanged(object sender, EventArgs e)
        {
  

            string name_desc = "", name_code = "";
            string c_name_desc = "", c_name_code = "";
            int counter = 0, icounter = 0;
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            try
            {
                foreach (ListItem val in CustNameList.Items)
                {
                    if (val.Selected)
                    {
                        counter++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                        foreach (ListItem val1 in CustNumList.Items)
                        {
                            if (val1.Value == val.Value)
                            {
                                icounter++;
                                val1.Selected = true;
                                c_name_desc += val1.Text + " , ";
                                c_name_code += val1.Value + "','";
                            }
                        }
                    }
                    else
                    {
                        foreach (ListItem val1 in CustNumList.Items)
                        {
                            if (val1.Value == val.Value)
                            {
                                val1.Selected = false;
                            }
                        }
                    }
                }

                name_code = "'" + name_code;
                string CustomerNamelist = name_code.Substring(0, Math.Max(0, name_code.Length - 2));


                if (CustNameList.Items.Count == counter)
                {
                    Session["SelectedCustomerNames"] = "ALL";
                }
                else
                {
                    Session["SelectedCustomerNames"] = CustomerNamelist;
                }

                c_name_code = "'" + c_name_code;
                string CustomerNumlist = c_name_code.Substring(0, Math.Max(0, c_name_code.Length - 2));
                if (CustNumList.Items.Count == icounter)
                {

                    Session["SelectedCustomerNumbers"] = "ALL";
                }
                else
                {
                    Session["SelectedCustomerNumbers"] = CustomerNumlist;

                }

            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

            Chart2.Visible = false;
            Chart3.Visible = false;

            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }
        /// <summary>
        /// Author :
        /// Created date :
        /// Description : click on cust_no checkbox, cust_name should be checked accordingly and selected value should be stored in session for further use
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CustNumList_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region waste
            /*
            string name_desc = "", name_code = "";
            string c_name_desc = "", c_name_code = "";
            int counter = 0, icounter = 0;
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            try
            {
                if (flag.Value != "CustNameListChange")
                {
                    if (lcnu.Value != "")
                    {
                        string s = lcnu.Value;
                        List<string> l = s.Split(',').ToList();

                        foreach (ListItem val in CustNumList.Items)
                        {

                            val.Selected = false;
                            foreach (string k in l)
                            {
                                string sk = k;
                                string subk = sk.Substring(1, sk.Length - 2);

                                string valu = val.Value;
                                if (subk == val.Value)
                                {
                                    val.Selected = true;
                                    counter++;
                                    name_desc += val.Text + " , ";
                                    name_code += val.Value + "','";
                                }
                            }
                        }
                        foreach (ListItem val1 in CustNameList.Items)
                        {
                            foreach (ListItem val in CustNumList.Items)
                            {
                                if (val.Selected)
                                {
                                    val1.Selected = false;
                                    if (val1.Value == val.Value)
                                    {
                                        icounter++;
                                        val1.Selected = true;
                                        c_name_desc += val1.Text + " , ";
                                        c_name_code += val1.Value + "','";
                                        break;
                                    }
                                }
                            }

                        }
                    }
                    else if (lcna.Value == "")
                    {
                        foreach (ListItem val in CustNumList.Items)
                        {
                            val.Selected = true;
                            counter++;
                            name_desc += val.Text + " , ";
                            name_code += val.Value + "','";
                            foreach (ListItem val1 in CustNameList.Items)
                            {
                                val1.Selected = true;
                                if (val1.Value == val.Value)
                                {
                                    icounter++;
                                    val1.Selected = true;
                                    c_name_desc += val1.Text + " , ";
                                    c_name_code += val1.Value + "','";
                                }
                            }
                        }

                    }
                    else
                    {
                        foreach (ListItem val in CustNumList.Items)
                        {
                            if (val.Selected)
                            {
                                counter++;
                                name_desc += val.Text + " , ";
                                name_code += val.Value + "','";
                                foreach (ListItem val1 in CustNameList.Items)
                                {
                                    if (val1.Value == val.Value)
                                    {
                                        icounter++;
                                        val1.Selected = true;
                                        c_name_desc += val1.Text + " , ";
                                        c_name_code += val1.Value + "','";
                                    }
                                }
                            }
                            else
                            {
                                foreach (ListItem val1 in CustNameList.Items)
                                {
                                    if (val1.Value == val.Value)
                                    {
                                        val1.Selected = false;
                                    }
                                }
                            }
                        }
                    }


                    name_code = "'" + name_code;
                    string CustomerNumlist = name_code.Substring(0, Math.Max(0, name_code.Length - 2));


                    if (CustNameList.Items.Count == counter)
                    {
                        Session["SelectedCustomerNumbers"] = "ALL";
                    }
                    else
                    {
                        Session["SelectedCustomerNumbers"] = CustomerNumlist;
                    }

                    c_name_code = "'" + c_name_code;
                    string CustomerNamelist = c_name_code.Substring(0, Math.Max(0, c_name_code.Length - 2));
                    if (CustNumList.Items.Count == icounter)
                    {

                        Session["SelectedCustomerNames"] = "ALL";
                    }
                    else
                    {
                        Session["SelectedCustomerNames"] = CustomerNamelist;

                    }


                }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }


            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            */
            #endregion
            string name_desc = "", name_code = "";
            string c_name_desc = "", c_name_code = "";
            int counter = 0, icounter = 0;
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            try
            {
                foreach (ListItem val in CustNumList.Items)
                {
                    if (val.Selected)
                    {
                        counter++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                        foreach (ListItem val1 in CustNameList.Items)
                        {
                            if (val1.Value == val.Value)
                            {
                                icounter++;
                                val1.Selected = true;
                                c_name_desc += val1.Text + " , ";
                                c_name_code += val1.Value + "','";
                            }
                        }
                    }
                    else
                    {
                        foreach (ListItem val1 in CustNameList.Items)
                        {
                            if (val1.Value == val.Value)
                            {
                                val1.Selected = false;
                            }
                        }
                    }
                }

                name_code = "'" + name_code;
                string CustomerNumlist = name_code.Substring(0, Math.Max(0, name_code.Length - 2));


                if (CustNameList.Items.Count == counter)
                {
                    Session["SelectedCustomerNumbers"] = "ALL";
                }
                else
                {
                    Session["SelectedCustomerNumbers"] = CustomerNumlist;
                }

                c_name_code = "'" + c_name_code;
                string CustomerNamelist = c_name_code.Substring(0, Math.Max(0, c_name_code.Length - 2));
                if (CustNumList.Items.Count == icounter)
                {

                    Session["SelectedCustomerNames"] = "ALL";
                }
                else
                {
                    Session["SelectedCustomerNames"] = CustomerNamelist;

                }

                Chart2.Visible = false;
                Chart3.Visible = false;

            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

            //string name_desc = "", name_code = "";
            //int count = 0;
            //if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

            //foreach (ListItem val in CustNumList.Items)
            //{
            //    if (val.Selected)
            //    {
            //        count++;
            //        name_desc += val.Text + " , ";
            //        name_code += val.Value + "','";
            //    }
            //}

            //name_code = "'" + name_code;

            ////  TxtCustomerNum.Text = name_desc;

            //string CustomerNumlist = name_code.Substring(0, Math.Max(0, name_code.Length - 2));
            //string CustomerNumDesclist = name_desc.Substring(0, Math.Max(0, name_desc.Length - 2));

            //if (count == CustNumList.Items.Count)
            //{
            //    Session["SelectedCustomerNumbers"] = "ALL";
            //}
            //else
            //{
            //    Session["SelectedCustomerNumbers"] = CustomerNumlist;
            //}
            ////if (ChkCustNumAll.Checked)
            ////{
            ////    Session["SelectedCustomerNumbers"] = "ALL";
            ////}
            ////else
            ////{
            ////    Session["SelectedCustomerNumbers"] = CustomerNumlist;
            ////}

            ////    TxtCustomerNum.Text = CustomerNumDesclist;

            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        }
        /// <summary>
        /// Author :
        /// Created date :
        /// Description :click on customer type, customers should be loaded accordingly and selected value should be stored in session for further use
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlcustomertype_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string name_desc = "", name_code = "";
            int count = 0;
            try
            {
                string roleId = Session["RoleId"].ToString();
                string userId = Session["UserId"].ToString();
                string branchcode = Convert.ToString(Session["SelectedBranchList"]);
                string SalesengList = Convert.ToString(Session["SelectedSalesEngineers"]);
                objRSum.BranchCode = (roleId == "TM" && branchcode == "ALL") ? userId : branchcode;
                objRSum.salesengineer_id = SalesengList;
                objRSum.customer_type = ddlcustomertype.SelectedItem.Value;
                objRSum.roleId = roleId;
                objRSum.flag = "CustomerType";
                Session["ddlcustomertype"] = ddlcustomertype.SelectedItem.Value;
                DataTable dtData = objRSum.getFilterAreaValue(objRSum);

                if (dtData.Rows.Count != 0)
                {
                    CustNameList.DataSource = dtData;
                    CustNameList.DataTextField = "customer_short_name";
                    CustNameList.DataValueField = "customer_number";
                    CustNameList.DataBind();
                    // ChkCustName.Items.Insert(0, "ALL");
                }
                else
                {
                    CustNameList.DataSource = dtData;
                    CustNameList.DataTextField = "customer_short_name";
                    CustNameList.DataValueField = "customer_number";
                    CustNameList.DataBind();
                    // ChkCustName.Items.Insert(0, "NO CUSTOMER");
                }
                if (dtData.Rows.Count != 0)
                {
                    CustNumList.DataSource = dtData;
                    CustNumList.DataTextField = "customer_number";
                    CustNumList.DataValueField = "customer_number";
                    CustNumList.DataBind();
                    //ChkCustNum.Items.Insert(0, "ALL");
                }
                else
                {
                    CustNumList.DataSource = dtData;
                    CustNumList.DataTextField = "customer_number";
                    CustNumList.DataValueField = "customer_number";
                    CustNumList.DataBind();
                    // ChkCustNum.Items.Insert(0, "NO CUSTOMER");
                }

                foreach (ListItem val in CustNameList.Items)
                {
                    val.Selected = true;
                    if (val.Selected)
                    {
                        count++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                    }
                }

                foreach (ListItem val in CustNumList.Items)
                {
                    val.Selected = true;
                    if (val.Selected)
                    {
                        count++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                    }
                }

                CustNameList_SelectedIndexChanged(null, null);
                CustNumList_SelectedIndexChanged(null, null);

                Chart2.Visible = false;
                Chart3.Visible = false;


                DataTable dt = ViewState["Chart"] as DataTable;
                // if (dt != null) { if (dt.Rows.Count != 0) bindchart(dt); }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        /// <summary>
         ///Author :
        /// Created date :
        /// Description :click on family checkbox, subfamily should be loaded accordingly and selected value should be stored in session for further use
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        protected void ProductFamilyList_SelectedIndexChanged(object sender, EventArgs e)
        {


            string name_desc = "", name_code = "", chakgrp_desc = "", chkgrp_code = "";
            int count = 0, count1 = 0;
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                // For PRODUCT FAMILY
                foreach (ListItem val in ProductFamilyList.Items)
                {
                    if (val.Selected)
                    {
                        count++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + ",";
                    }
                }

                name_code = "" + name_code;

                // TxtProductfamily.Text = name_desc;

                string ProductFamilyListVal = name_code.Substring(0, Math.Max(0, name_code.Length - 1));
                string ProductFamilyNameList = name_desc.Substring(0, Math.Max(0, name_desc.Length - 2));

                if (count == ProductFamilyList.Items.Count)
                {
                    Session["SelectedProductFamily"] = "ALL";
                }
                else
                {
                    Session["SelectedProductFamily"] = ProductFamilyListVal;
                }
                //if (ChkProductFamilyAll.Checked)
                //{
                //    TxtProductfamily.Text = "ALL";
                //    Session["SelectedProductFamily"] = "ALL";
                //}
                //else
                //{
                //    Session["SelectedProductFamily"] = ProductFamilyList;
                //}

                // For PRODUCT GROUP
                foreach (ListItem val in ProductGrpList.Items)
                {
                    if (val.Selected)
                    {
                        count1++;
                        chakgrp_desc += val.Text + " , ";
                        chkgrp_code += val.Value + "','";
                    }
                }

                chkgrp_code = "'" + chkgrp_code;

                //  TxtProductGrp.Text = chakgrp_desc;

                string ProductGrpListVal = chkgrp_code.Substring(0, Math.Max(0, chkgrp_code.Length - 2));
                string ProductGrpNameList = chakgrp_desc.Substring(0, Math.Max(0, chakgrp_desc.Length - 2));

                if (count1 == ProductGrpList.Items.Count)
                {
                    Session["SelectedProductGroup"] = "ALL";
                }
                else
                {
                    Session["SelectedProductGroup"] = ProductGrpListVal;
                }
                //if (ChkProductGrpAll.Checked)
                //{
                //    TxtProductGrp.Text = "ALL";
                //    Session["SelectedProductGroup"] = "ALL";
                //}
                //else
                //{
                //    Session["SelectedProductGroup"] = ProductGrpList;
                //}


                string ProductGroup = Convert.ToString(Session["SelectedProductGroup"]);
                string ProductFamily = Convert.ToString(Session["SelectedProductFamily"]);


                string famId = ProductFamily == "ALL" || ProductFamily == "" ? "0" : ProductFamily;
                // int Id = Convert.ToInt32(famId);
                DataTable dtPL = new DataTable();
                string group = ProductGroup == "ALL" || ProductGroup == "" ? null : ProductGroup;
                dtPL = objRSum.getProducts(famId, group);
                DataTable dtTemp = dtPL.Clone();
                for (int i = 0; i < dtPL.Rows.Count; i++)
                {
                    dtTemp.Rows.Add(dtPL.Rows[i].ItemArray[0], dtPL.Rows[i].ItemArray[0].ToString() + "_" + dtPL.Rows[i].ItemArray[2].ToString(), dtPL.Rows[i].ItemArray[2]);
                }
                if (dtTemp.Rows.Count != 0)
                {
                    ApplicationList.DataSource = dtTemp;
                    ApplicationList.DataValueField = "item_code";
                    ApplicationList.DataTextField = "item_short_name";
                    ApplicationList.DataBind();
                    // ChkApplicationList.Items.Insert(0, "ALL");
                }
                else
                {

                    ApplicationList.DataSource = dtTemp;
                    ApplicationList.DataValueField = "item_code";
                    ApplicationList.DataTextField = "item_short_name";
                    ApplicationList.DataBind();
                    // ChkApplicationList.Items.Insert(0, "NO APPLICATION FOR SELECTION");
                }

                //if ((ChkProductFamilyAll.Checked == true || count >= 1) && (ChkProductGrpAll.Checked == true || count1 >= 1))
                //{
                //    ChkAppAll.Checked = true;
                //  //  ChkAppAll_CheckedChanged(null, null);
                //}

                foreach (ListItem val in ApplicationList.Items)
                {
                    val.Selected = true;
                    if (val.Selected)
                    {
                        count++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                    }
                }

                ApplicationList_SelectedIndexChanged(null, null);
                //  if (ProductGrpList == "" || ProductFamilyList == "")
                //  {
                //       TxtApplication.Text = "";
                //  }
                //   else
                //   {
                //       TxtApplication.Text = "ALL";
                //    }
                Chart2.Visible = false;
                Chart3.Visible = false;


                DataTable dt = ViewState["Chart"] as DataTable;
                //    if (dt != null) { if (dt.Rows.Count != 0) bindchart(dt); }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                //  ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }
        /// <summary>
        /// Author  : 
        /// Date    : 
        /// Desc    : click on group checkbox,  selected value should be stored in session for further use
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ProductGrpList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                ProductFamilyList_SelectedIndexChanged(null, null);
                DataTable dt = ViewState["Chart"] as DataTable;
                Chart2.Visible = false;
                Chart3.Visible = false;

                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }


        /// <summary>
        /// Author  : 
        /// Date    : 
        /// Desc    : click on application checkbox, selected value should be stored in session for further use
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ApplicationList_SelectedIndexChanged(object sender, EventArgs e)
        {
            #region waste
            /*
            string name_desc = "", name_code = "";
            int count = 0;
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

            if (apl.Value != "")
            {
                string s = apl.Value;
                List<string> l = s.Split(',').ToList();

                foreach (ListItem val in ApplicationList.Items)
                {
                    val.Selected = false;
                    foreach (string k in l)
                    {
                        string sk = k;
                        string subk = sk.Substring(1, sk.Length - 2);

                        string valu = val.Value;
                        if (subk == val.Value)
                        {
                            val.Selected = true;
                            count++;
                            name_desc += val.Text + " , ";
                            name_code += val.Value + "','";
                            break;
                        }
                    }
                }
            }
            else if (apl.Value == "")
            {
                foreach (ListItem val in ApplicationList.Items)
                {
                        val.Selected = true;
                        count++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";    
                }
            }
            else
            {

                foreach (ListItem val in ApplicationList.Items)
                {
                    if (val.Selected)
                    {
                        count++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                    }
                }
            }

            name_code = "'" + name_code;


            string ApplicationListVal = name_code.Substring(0, Math.Max(0, name_code.Length - 2));
            string ApplicationNameList = name_desc.Substring(0, Math.Max(0, name_desc.Length - 2));

            if (count == ApplicationList.Items.Count)
            {
                Session["SelectedApplications"] = "ALL";
            }
            else
            {
                Session["SelectedApplications"] = ApplicationListVal;
            }


            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
             * */
            #endregion

            string name_desc = "", name_code = "";
            int count = 0;
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

                foreach (ListItem val in ApplicationList.Items)
                {
                    if (val.Selected)
                    {
                        count++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                    }
                }

                name_code = "'" + name_code;

                // TxtApplication.Text = name_desc;

                string ApplicationListVal = name_code.Substring(0, Math.Max(0, name_code.Length - 2));
                string ApplicationNameList = name_desc.Substring(0, Math.Max(0, name_desc.Length - 2));

                if (count == ApplicationList.Items.Count)
                {
                    Session["SelectedApplications"] = "ALL";
                }
                else
                {
                    Session["SelectedApplications"] = ApplicationListVal;
                }
                //if (ChkAppAll.Checked)
                //{
                //    Session["SelectedApplications"] = "ALL";
                //}
                //else
                //{
                //    Session["SelectedApplications"] = ApplicationList;
                //}

                //  TxtApplication.Text = ApplicationNameList;
                Chart2.Visible = false;
                Chart3.Visible = false;

                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        /// <summary>
        /// Author  : 
        /// Date    : 
        /// Desc    : Select the company type and store in session
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Value_or_Qty_Change(object sender, EventArgs e)
        {

            try
            {
                chartdisplay.Value = "0";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Please click on FILTER to view results');triggerPostGridLodedActions();", true);
                // ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Please click on FILTER to view results');triggerPostGridLodedActions();", true);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();$('#alertmsg').show().delay(5000).fadeOut();", true);
                tegutecheckedchanged = 1;

                if (rdBtnTaegutec.Checked)
                {
                    Session["cter"] = "TTA";
                    cter = "TTA";
                }

                if (rdBtnDuraCab.Checked)
                {
                    Session["cter"] = "DUR";
                    cter = "DUR";
                }
                grdviewAllValues.DataSource = null;
                grdviewAllValues.DataBind();
                //     Chart2.DataSource = null;
                grdviewAllValues.DataBind();

                divgridchart.Visible = false;
                grdviewAllQuantites.DataSource = null;
                grdviewAllQuantites.DataBind();
                //     Chart3.DataSource = null;
                grdviewAllQuantites.DataBind();


                LoadBranches();

                BranchList_SelectedIndexChanged(null, null);
                SalesEngList_SelectedIndexChanged(null, null);
                LoadProductFamliy();
                LoadProductGroup();
                Chart2.Visible = false;
                Chart3.Visible = false;

            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }
        /// <summary>
        ///  Author : 
        /// Date :
        /// Desc : Fetch report from database using all the filters
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void reports_Click(object sender, EventArgs e)
        {


            try
            {
                DataTable dtYtdSales = new DataTable();
                gridLoadedStatus = 1;

                if (rbtn_Thousand.Checked) { dtYtdSales = loadGrid(); }
                if (rbtn_Units.Checked) { dtYtdSales = loadGrid_Unit(); }
                if (rbtn_Lakhs.Checked) { dtYtdSales = loadGrid_lakh(); }


                Session["dtYtdSales"] = dtYtdSales;
                if (dtYtdSales.Rows.Count != 0)
                {
                    btnExport.Visible = true;
                    grdviewAllValues.DataSource = dtYtdSales;
                    grdviewAllValues.DataBind();
                }

                //if (!dtYtdSales.Columns.Contains("Color"))
                //{
                //    DataColumn dc = new DataColumn("Color");

                //    dtYtdSales.Columns.Add(dc);

                //    dtYtdSales.Rows[5]["Color"] = "#097054";
                    
                //}




                bindgridColor();
                ForGraph();

                DataTable dtChart = GenerateTransposedTable(dtYtdSales);
                DataTable dtoriginal = dtChart.Copy();
                ViewState["dtChart"] = dtChart;
                System.Drawing.Font chtFont = new System.Drawing.Font("Arial", 42);
                List<Color> l = new List<Color>();
                foreach (DataRow row in dtChart.Rows)
                {
                    if (row.ItemArray[0].ToString() == "flag")
                    {
                        row.Delete(); dtChart.AcceptChanges();
                        #region CommentedCode
                        //LoadChartData(dtChart);
                        //string column1 = dtChart.Columns[1].ColumnName.ToString(); // Months 
                        //string column2 = dtChart.Columns[5].ColumnName.ToString(); // YTD SALE
                        //string column3 = dtChart.Columns[4].ColumnName.ToString(); // YTD PLAN
                        //string column4 = dtChart.Columns[6].ColumnName.ToString(); // YTD SALE PREVIOUS YEAR

                        //Chart2.DataSource = dtChart;
                        ////Bar chart
                        //Chart2.Series["YTD SALE"].XValueMember = column1;
                        //Chart2.Series["YTD SALE"].YValueMembers = column2;
                        ////Line chart YTD PLAN 2015
                        //Chart2.Series["YTD PLAN"].XValueMember = column1;
                        //Chart2.Series["YTD PLAN"].YValueMembers = column3;
                        //Chart2.Series["Series4"].XValueMember = column1;
                        //Chart2.Series["Series4"].YValueMembers = column3;
                        //Chart2.Series["Series4"].IsVisibleInLegend = false;
                        //// Line chaert YTD SALE 2014
                        //Chart2.Series["YTD SALE PREVIOUS YEAR"].XValueMember = column1;
                        //Chart2.Series["YTD SALE PREVIOUS YEAR"].YValueMembers = column4;
                        //Chart2.Series["Series5"].XValueMember = column1;
                        //Chart2.Series["Series5"].YValueMembers = column4;

                        //Chart2.Series["Series5"].IsVisibleInLegend = false;
                        //// Create a new legend called "Legend2".
                        ////Chart1.Legends.Add(new Legend("Legend2"));

                        ////// Set Docking of the Legend chart to the Default Chart Area.
                        ////chart1.Legends["Legend2"].DockToChartArea = "Default";

                        ////// Assign the legend to Series1.
                        ////Chart1.Series["Series1"].Legend = "Legend2";
                        ////Chart1.Series["Series1"].IsVisibleInLegend = true;

                        //Chart2.DataBind();

                        //// making user friendly

                        //Chart2.ChartAreas["ChartArea1"].AxisX.Interval = 1;

                        //Chart2.Series["YTD SALE"].ToolTip = column2 + " " + ":" + " " + "#VALY"; ;
                        //Chart2.Series["YTD PLAN"].ToolTip = column3 + " " + ":" + " " + "#VALY"; ;
                        //Chart2.Series["YTD SALE PREVIOUS YEAR"].ToolTip = column4 + " " + ":" + " " + "#VALY";
                        //Chart2.Series["YTD PLAN"].IsValueShownAsLabel = true;
                        //Chart2.Series["YTD PLAN"].Font = new System.Drawing.Font("Arial", 15);
                        //Chart2.Series["YTD SALE PREVIOUS YEAR"].IsValueShownAsLabel = true;
                        //Chart2.Series["YTD SALE PREVIOUS YEAR"].Font = new System.Drawing.Font("Arial", 15);
                        //Chart2.Visible = true;
                        //Chart2.Width = 2500;
                        //Chart2.Height = 500;


                        //Chart2.Legends["Legend2"].CellColumns.Add(new LegendCellColumn(column2, LegendCellColumnType.Text, "MTD SALE"));
                        //Random random = new Random();

                        //foreach (var item in Chart2.Series["YTD SALE"].Points)
                        //{

                        //    Color c = Color.FromArgb(random.Next(256), random.Next(256), random.Next(256));
                        //    // Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                        //    item.Color = c;
                        //    l.Add(c);
                        //}
                        #endregion

              //          Chart2.SaveImage(Convert.ToString(ConfigurationManager.AppSettings["ChartImageFile"]), ChartImageFormat.Png);
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", " AmChartDoubleGraph();", true);
                        Session["hdnClick"] = "1";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "AmChartDoubleGraph();", true);
                        break;
                    }
                }

                #region codefor adding color
                //if (!dtoriginal.Columns.Contains("Color"))
                //{
                //    DataColumn dc = new DataColumn("Color");

                //    dtoriginal.Columns.Add(dc);
                //    for (int i = 0; i < dtoriginal.Rows.Count - 1; i++)
                //    {
                //        //Color myColor = Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256));
                //        for (int j = i; j < l.Count; j++)
                //        {
                //            string hex = l[j].R.ToString("X2") + l[j].G.ToString("X2") + l[j].B.ToString("X2");
                //            DataRow dr = dtoriginal.Rows[i];
                //            dr[dc] = "#" + hex;
                //            break;
                //        }
                //        //string hex = myColor.R.ToString("X2") + myColor.G.ToString("X2") + myColor.B.ToString("X2");

                //        //dr[dc] = "#" + hex;
                //        //  dtChart.Rows.Add(dr);
                //    }
                //}


                #endregion







                for (int i = dtoriginal.Rows.Count; i > 0; i--)
                {
                    for (int j = 0; j < dtoriginal.Rows[i - 1].ItemArray.Length - 1; j++)
                    {
                        string columnname = Convert.ToString(dtoriginal.Rows[i - 1].ItemArray[j]);
                        for (int k = j; k < dtoriginal.Columns.Count; k++)
                        {
                            dtoriginal.Columns[k].ColumnName = columnname;
                            break;
                        }

                    }
                    DataRow row = dtoriginal.Rows[i - 1];


                    break;
                }

                Session["dtChart_val"] = dtoriginal;



                //   Session["dtChart"] = dtChart;

                


                if (rbtn_Thousand.Checked) { dtYtdSales = loadGridQty(); }
                if (rbtn_Lakhs.Checked) { dtYtdSales = loadGrid_lakhQty(); }
                if (rbtn_Units.Checked) { dtYtdSales = loadGrid_UnitQty(); }




                Session["dtYtdSales_qty"] = dtYtdSales;
                if (dtYtdSales.Rows.Count != 0)
                {
                    grdviewAllQuantites.DataSource = dtYtdSales;
                    grdviewAllQuantites.DataBind();
                }
                bindgridColorQty();
                //chart pre
                DataTable dtChart_qty = GenerateTransposedTable(dtYtdSales);
                DataTable dtoriginal_qty = dtChart_qty.Copy();

                ViewState["Chart"] = dtChart_qty;



                chartdisplay.Value = "1";
                // Session["hdnClick"] = "1";
                if (rbtn_quantity.Checked)
                {
                    Session["dtChart"] = dtChart_qty;

                }
                else if (rbtn_value.Checked)
                {
                    Session["dtChart"] = dtChart;

                }

                List<Color> ls = new List<Color>();
                foreach (DataRow row in dtChart_qty.Rows)
                {
                    if (row.ItemArray[0].ToString() == "flag")
                    {
                        row.Delete(); dtChart_qty.AcceptChanges();
                        //LoadChartData(dtChart_qty);
                        #region COmmentedCode
                        //string column1 = dtChart_qty.Columns[1].ColumnName.ToString(); // Months 
                        //string column2 = dtChart_qty.Columns[5].ColumnName.ToString(); // YTD SALE
                        //string column3 = dtChart_qty.Columns[4].ColumnName.ToString(); // YTD PLAN
                        //string column4 = dtChart_qty.Columns[6].ColumnName.ToString(); // YTD SALE PREVIOUS YEAR

                        //Chart3.DataSource = dtChart_qty;
                        ////Bar chart
                        //Chart3.Series["YTD SALE"].XValueMember = column1;
                        //Chart3.Series["YTD SALE"].YValueMembers = column2;
                        ////Line chart YTD PLAN 2015
                        //Chart3.Series["YTD PLAN"].XValueMember = column1;
                        //Chart3.Series["YTD PLAN"].YValueMembers = column3;
                        //Chart3.Series["Series4"].XValueMember = column1;
                        //Chart3.Series["Series4"].YValueMembers = column3;
                        //Chart3.Series["Series4"].IsVisibleInLegend = false;
                        //// Line chaert YTD SALE 2014
                        //Chart3.Series["YTD SALE PREVIOUS YEAR"].XValueMember = column1;
                        //Chart3.Series["YTD SALE PREVIOUS YEAR"].YValueMembers = column4;
                        //Chart3.Series["Series5"].XValueMember = column1;
                        //Chart3.Series["Series5"].YValueMembers = column4;

                        //Chart3.Series["Series5"].IsVisibleInLegend = false;
                        //Chart3.DataBind();

                        //// making user friendly
                        //Chart3.ChartAreas["ChartArea1"].AxisX.Interval = 1;
                        //Chart3.Series["YTD SALE"].ToolTip = column2 + " " + ":" + " " + "#VALY"; ;
                        //Chart3.Series["YTD PLAN"].ToolTip = column3 + " " + ":" + " " + "#VALY"; ;
                        //Chart3.Series["YTD SALE PREVIOUS YEAR"].ToolTip = column4 + " " + ":" + " " + "#VALY";
                        ////Chart3.Visible = true;
                        //Chart3.Series["YTD PLAN"].IsValueShownAsLabel = true;
                        //Chart3.Series["YTD PLAN"].Font = new System.Drawing.Font("Arial", 8);
                        //Chart3.Series["YTD SALE PREVIOUS YEAR"].IsValueShownAsLabel = true;
                        //Chart3.Series["YTD SALE PREVIOUS YEAR"].Font = new System.Drawing.Font("Arial", 8);

                        ////Chart2.Legends["Legend2"].CellColumns.Add(new LegendCellColumn(column2, LegendCellColumnType.Text, "MTD SALE"));
                        //Random random = new Random();
                        //foreach (var item in Chart3.Series["YTD SALE"].Points)
                        //{
                        //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                        //    item.Color = c;
                        //    ls.Add(c);
                        //}



                    //    Chart3.SaveImage(Convert.ToString(ConfigurationManager.AppSettings["ChartImageFile_qty"]), ChartImageFormat.Png);
                        // ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

                        #endregion
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                        break;
                    }


                    if (!dtoriginal_qty.Columns.Contains("Color"))
                    {
                        DataColumn dc = new DataColumn("Color");

                        dtoriginal_qty.Columns.Add(dc);
                        for (int i = 0; i < dtoriginal_qty.Rows.Count - 1; i++)
                        {
                            //Color myColor = Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256));
                            for (int j = i; j < l.Count; j++)
                            {
                                string hex = l[j].R.ToString("X2") + l[j].G.ToString("X2") + l[j].B.ToString("X2");
                                DataRow dr = dtoriginal_qty.Rows[i];
                                dr[dc] = "#" + hex;
                                break;
                            }
                            //string hex = myColor.R.ToString("X2") + myColor.G.ToString("X2") + myColor.B.ToString("X2");

                            //dr[dc] = "#" + hex;
                            //  dtChart.Rows.Add(dr);
                        }
                    }

                    for (int i = dtoriginal_qty.Rows.Count; i > 0; i--)
                    {
                        for (int j = 0; j < dtoriginal_qty.Rows[i - 1].ItemArray.Length - 1; j++)
                        {
                            string columnname = Convert.ToString(dtoriginal_qty.Rows[i - 1].ItemArray[j]);
                            for (int k = j; k < dtoriginal_qty.Columns.Count; k++)
                            {
                                dtoriginal_qty.Columns[k].ColumnName = columnname;
                                break;
                            }

                        }
                        DataRow rowS = dtoriginal_qty.Rows[i - 1];


                        break;
                    }


                    Session["dtChart_Qty"] = dtoriginal_qty;




                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

                    divgridchart.Visible = true;
                    divgridchartqty.Visible = true;
                    Chart2.Visible = false;
                    Chart3.Visible = false;
                }

               
                PDFCreationWithChart();
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        public void ForGraph()
        {
            DataTable dtYtdSales_inLakh_QTY = new DataTable();
            DataTable dtYtdSales_inLakh_Val = new DataTable();
            dtYtdSales_inLakh_QTY = loadGrid_lakhQty();
            Session["dtYtdSales_inLakh_QTY"] = dtYtdSales_inLakh_QTY;
            dtYtdSales_inLakh_Val = loadGrid_lakh();
            Session["dtYtdSales_inLakh_Val"] = dtYtdSales_inLakh_Val;
            #region graphforvalues
            DataTable dtChart = GenerateTransposedTable(dtYtdSales_inLakh_Val);
            DataTable dtoriginal = dtChart.Copy();
            ViewState["dtChart"] = dtChart;
            System.Drawing.Font chtFont = new System.Drawing.Font("Arial", 42);
            List<Color> l = new List<Color>();
            foreach (DataRow row in dtChart.Rows)
            {
                if (row.ItemArray[0].ToString() == "flag")
                {
                    row.Delete(); dtChart.AcceptChanges();
                    //LoadChartData(dtChart);
                    string column1 = dtChart.Columns[1].ColumnName.ToString(); // Months 
                    string column2 = dtChart.Columns[7].ColumnName.ToString(); // YTD SALE
                    string column3 = dtChart.Columns[5].ColumnName.ToString(); // YTD PLAN
                    string column4 = dtChart.Columns[3].ColumnName.ToString(); // YTD SALE PREVIOUS YEAR
                    
                    Chart2.DataSource = dtChart;
                    //Bar chart
                    Chart2.Series["YTD SALE"].XValueMember = column1;
                    Chart2.Series["YTD SALE"].YValueMembers = column2;
                    //Line chart YTD PLAN 2015
                    Chart2.Series["YTD PLAN"].XValueMember = column1;
                    Chart2.Series["YTD PLAN"].YValueMembers = column3;
                    Chart2.Series["Series4"].XValueMember = column1;
                    Chart2.Series["Series4"].YValueMembers = column3;
                    Chart2.Series["Series4"].IsVisibleInLegend = false;
                    // Line chaert YTD SALE 2014
                    Chart2.Series["YTD SALE PREVIOUS YEAR"].XValueMember = column1;
                    Chart2.Series["YTD SALE PREVIOUS YEAR"].YValueMembers = column4;
                    Chart2.Series["Series5"].XValueMember = column1;
                    Chart2.Series["Series5"].YValueMembers = column4;

                    Chart2.Series["Series5"].IsVisibleInLegend = false;

                    Chart2.Series["YTD SALE"].LabelForeColor = System.Drawing.Color.Black;
                    Chart2.Series["YTD SALE PREVIOUS YEAR"].LabelForeColor = System.Drawing.Color.Black;
                    Chart2.Series["YTD PLAN"].LabelForeColor = System.Drawing.Color.Black;

                    // Create a new legend called "Legend2".
                    //Chart1.Legends.Add(new Legend("Legend2"));
                    Chart2.Legends["Legend2"].Font = new System.Drawing.Font("Arial", 15);
                    //// Set Docking of the Legend chart to the Default Chart Area.
                    //chart1.Legends["Legend2"].DockToChartArea = "Default";

                    //// Assign the legend to Series1.
                    //Chart1.Series["Series1"].Legend = "Legend2";
                    //Chart1.Series["Series1"].IsVisibleInLegend = true;

                    Chart2.DataBind();

                    // making user friendly

                    Chart2.ChartAreas["ChartArea1"].AxisX.Interval = 1;

                    Chart2.Series["YTD SALE"].ToolTip = column2 + " " + ":" + " " + "#VALY"; ;
                    Chart2.Series["YTD PLAN"].ToolTip = column3 + " " + ":" + " " + "#VALY"; ;
                    Chart2.Series["YTD SALE PREVIOUS YEAR"].ToolTip = column4 + " " + ":" + " " + "#VALY";
                    Chart2.Series["YTD SALE"].IsValueShownAsLabel = true;
                    Chart2.Series["YTD PLAN"].IsValueShownAsLabel = true;
                    Chart2.Series["YTD PLAN"].Font = new System.Drawing.Font("Arial", 15);
                    Chart2.Series["YTD SALE PREVIOUS YEAR"].IsValueShownAsLabel = true;
                    Chart2.Series["YTD SALE PREVIOUS YEAR"].Font = new System.Drawing.Font("Arial", 15);
                    Chart2.Visible = true;
                    Chart2.Width = 2500;
                    Chart2.Height = 500;
            //    Series s= new Series();
                    Chart2.Series["YTD SALE"].LegendText = "YTD SALE " + Convert.ToString(BudgetYear);
                    Chart2.Series["YTD PLAN"].LegendText = "YTD PLAN " + Convert.ToString(BudgetYear);
                    Chart2.Series["YTD SALE PREVIOUS YEAR"].LegendText = "YTD SALE  " + Convert.ToString(BudgetYear-1);
                    Chart2.Series["YTD SALE"].Color = System.Drawing.Color.FromArgb(9, 112, 84);
                    Chart2.Series["YTD PLAN"].Color = System.Drawing.Color.FromArgb(103, 183, 220);
                    Chart2.Series["YTD SALE PREVIOUS YEAR"].Color = System.Drawing.Color.FromArgb(0, 103, 128);
                

                    //Chart2.Legends["Legend2"].CellColumns.Add(new LegendCellColumn(column2, LegendCellColumnType.Text, "MTD SALE"));
                //    Random random = new Random();

                 //   foreach (var item in Chart2.Series["YTD SALE"].Points)
                 //   {

                  //      Color c = Color.FromArgb(random.Next(256), random.Next(256), random.Next(256));
                        // Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                       // l.Add(c);
                   // }//
                 Chart2.SaveImage(Convert.ToString(ConfigurationManager.AppSettings["ChartImageFile"]), ChartImageFormat.Png);
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", " AmChartDoubleGraph();", true);
                    Session["hdnClick"] = "1";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "AmChartDoubleGraph();", true);
                    break;
                }
            }




            for (int i = dtoriginal.Rows.Count; i > 0; i--)
            {
                for (int j = 0; j < dtoriginal.Rows[i - 1].ItemArray.Length - 1; j++)
                {
                    string columnname = Convert.ToString(dtoriginal.Rows[i - 1].ItemArray[j]);
                    for (int k = j; k < dtoriginal.Columns.Count; k++)
                    {
                        dtoriginal.Columns[k].ColumnName = columnname;
                        break;
                    }

                }
                DataRow row = dtoriginal.Rows[i - 1];


                break;
            }

            Session["dtChart_val"] = dtoriginal;

            #endregion
            #region graphforQty
            DataTable dtChart_qty = GenerateTransposedTable(dtYtdSales_inLakh_QTY);
                DataTable dtoriginal_qty = dtChart_qty.Copy();

                ViewState["Chart"] = dtChart_qty;



                chartdisplay.Value = "1";
                // Session["hdnClick"] = "1";
                if (rbtn_quantity.Checked)
                {
                    Session["dtChart"] = dtChart_qty;

                }
                else if (rbtn_value.Checked)
                {
                    Session["dtChart"] = dtChart;

                }

                List<Color> ls = new List<Color>();
                foreach (DataRow row in dtChart_qty.Rows)
                {
                    if (row.ItemArray[0].ToString() == "flag")
                    {
                        row.Delete(); dtChart_qty.AcceptChanges();
                        //LoadChartData(dtChart_qty);
                        string column1 = dtChart_qty.Columns[1].ColumnName.ToString(); // Months 
                        string column2 = dtChart_qty.Columns[7].ColumnName.ToString(); // YTD SALE
                        string column3 = dtChart_qty.Columns[5].ColumnName.ToString(); // YTD PLAN
                        string column4 = dtChart_qty.Columns[3].ColumnName.ToString(); // YTD SALE PREVIOUS YEAR

                        Chart3.DataSource = dtChart_qty;
                        //Bar chart
                        Chart3.Series["YTD SALE"].XValueMember = column1;
                        Chart3.Series["YTD SALE"].YValueMembers = column2;
                        //Line chart YTD PLAN 2015
                        Chart3.Series["YTD PLAN"].XValueMember = column1;
                        Chart3.Series["YTD PLAN"].YValueMembers = column3;
                        Chart3.Series["Series4"].XValueMember = column1;
                        Chart3.Series["Series4"].YValueMembers = column3;
                        Chart3.Series["Series4"].IsVisibleInLegend = false;
                        // Line chaert YTD SALE 2014
                        Chart3.Series["YTD SALE PREVIOUS YEAR"].XValueMember = column1;
                        Chart3.Series["YTD SALE PREVIOUS YEAR"].YValueMembers = column4;
                     //   Chart3.Series["Series5"].XValueMember = column1;
                  //      Chart3.Series["Series5"].YValueMembers = column4;

                        Chart3.Series["Series5"].IsVisibleInLegend = false;
                        Chart3.DataBind();

                        // making user friendly
                        Chart3.ChartAreas["ChartArea1"].AxisX.Interval = 1;
                        Chart3.Series["YTD SALE"].ToolTip = column2 + " " + ":" + " " + "#VALY"; ;
                        Chart3.Series["YTD PLAN"].ToolTip = column3 + " " + ":" + " " + "#VALY"; ;
                        Chart3.Series["YTD SALE PREVIOUS YEAR"].ToolTip = column4 + " " + ":" + " " + "#VALY";
                        //Chart3.Visible = true;
                        Chart3.Series["YTD SALE"].IsValueShownAsLabel = true;
                        Chart3.Series["YTD PLAN"].IsValueShownAsLabel = true;
                        Chart3.Series["YTD PLAN"].Font = new System.Drawing.Font("Arial", 8);
                        Chart3.Series["YTD SALE PREVIOUS YEAR"].IsValueShownAsLabel = true;
                        Chart3.Series["YTD SALE PREVIOUS YEAR"].Font = new System.Drawing.Font("Arial", 8);
                        Chart3.Series["YTD SALE"].Font = new System.Drawing.Font("Arial", 8);
                        Chart3.Series["YTD SALE"].LabelForeColor = System.Drawing.Color.Black;
                        Chart3.Series["YTD SALE PREVIOUS YEAR"].LabelForeColor = System.Drawing.Color.Black;
                        Chart3.Series["YTD PLAN"].LabelForeColor = System.Drawing.Color.Black;
                        Chart3.Legends["Legend2"].Font = new System.Drawing.Font("Arial", 15);
               
                        //Chart2.Legends["Legend2"].CellColumns.Add(new LegendCellColumn(column2, LegendCellColumnType.Text, "MTD SALE"));
                     //   Random random = new Random();
                    //    foreach (var item in Chart3.Series["YTD SALE"].Points)
                     //   {
                      //      Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                      //      item.Color = c;
                      //      ls.Add(c);
                     //   }

                        Chart3.Series["YTD SALE"].LegendText = "YTD SALE " + Convert.ToString(BudgetYear);
                        Chart3.Series["YTD PLAN"].LegendText = "YTD PLAN " + Convert.ToString(BudgetYear);
                        Chart3.Series["YTD SALE PREVIOUS YEAR"].LegendText = "YTD SALE " + Convert.ToString(BudgetYear-1);
                        Chart3.Series["YTD SALE"].Color = System.Drawing.Color.FromArgb(9, 112, 84);
                        Chart3.Series["YTD PLAN"].Color = System.Drawing.Color.FromArgb(103, 183, 220);
                        Chart3.Series["YTD SALE PREVIOUS YEAR"].Color = System.Drawing.Color.FromArgb(0, 103, 128);

                          Chart3.SaveImage(Convert.ToString(ConfigurationManager.AppSettings["ChartImageFile_qty"]), ChartImageFormat.Png);
                        // ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                        break;
                    }
                    for (int i = dtoriginal_qty.Rows.Count; i > 0; i--)
                    {
                        for (int j = 0; j < dtoriginal_qty.Rows[i - 1].ItemArray.Length - 1; j++)
                        {
                            string columnname = Convert.ToString(dtoriginal_qty.Rows[i - 1].ItemArray[j]);
                            for (int k = j; k < dtoriginal_qty.Columns.Count; k++)
                            {
                                dtoriginal_qty.Columns[k].ColumnName = columnname;
                                break;
                            }

                        }
                        DataRow rowS = dtoriginal_qty.Rows[i - 1];


                        break;
                    }


                    Session["dtChart_Qty"] = dtoriginal_qty;




                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

                    divgridchart.Visible = true;
                    divgridchartqty.Visible = true;
                    Chart2.Visible = false;
                    Chart3.Visible = false;
            #endregion

                    Chart2.Visible = false;
                    Chart3.Visible = false;
                }


        }

        #region value in 000'/00000'

        protected void rbtn_Value_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                reports_Click(null, null);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                // ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }
        /// <summary>
        /// loads gridview in thousands
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Thousand_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (gridLoadedStatus == 1)
                {
                    reports_Click(null, null);
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                // ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }
        /// <summary>
        /// loads gridview in Lakhs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Lakhs_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (gridLoadedStatus == 1)
                {

                    reports_Click(null, null);
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                //ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }


        }

        /// <summary>
        /// loads gridview in units
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Units_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (gridLoadedStatus == 1)
                {

                    reports_Click(null, null);
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                // ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }


        }


        #endregion

        protected void rdBtnTaegutec_CheckedChanged(object sender, EventArgs e)
        {
            chartdisplay.Value = "0";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Please click on FILTER to view results');triggerPostGridLodedActions();", true);
            // ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Please click on FILTER to view results');triggerPostGridLodedActions();", true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();$('#alertmsg').show().delay(5000).fadeOut();", true);

            if (rdBtnTaegutec.Checked)
            {
                Session["cter"] = "TTA";
                cter = "TTA";
            }
            if (rdBtnDuraCab.Checked)
            {
                Session["cter"] = "DUR";
                cter = "DUR";
            }
            grdviewAllValues.DataSource = null;
            grdviewAllValues.DataBind();
            Chart2.DataSource = null;
            grdviewAllValues.DataBind();
            btnExport.Visible = false;
          
            divgridchart.Visible = false;


            grdviewAllQuantites.DataSource = null;
            grdviewAllQuantites.DataBind();
            Chart3.DataSource = null;
            grdviewAllQuantites.DataBind();


            LoadBranches();
            BranchList_SelectedIndexChanged(null, null);
            SalesEngList_SelectedIndexChanged(null, null);
         
        }
        #endregion

        #region Methods

        /// <summary>
        /// retrives values for filling gridview from database based on the user input from dropdown list on Value radio button checked
        /// </summary>
        /// <param name="Year"> year specifies whether it is actual year or current year</param>
        /// <param name="flag">specifies whether it is MTD SALE or MTD PLAN, Or YTD sale or YTD plan</param>
        /// <param name="valuein"> specifies whether it is units or lakshs</param>
        /// <returns></returns>
        #region load Grid By Values
       
      
        protected DataTable getMonthlyvalues(int Year, string flag, int valuein = 1000)
        {
            try
            {
                DataTable dtmonthval = new DataTable();
                string roleId = Session["RoleId"].ToString();
                string userId = Session["UserId"].ToString();
                string branchcode = Convert.ToString(Session["SelectedBranchList"]);
                string SalesengList = Convert.ToString(Session["SelectedSalesEngineers"]);
                string CustomerNamelist = Convert.ToString(Session["SelectedCustomerNames"]);
                string CustomerNumlist = Convert.ToString(Session["SelectedCustomerNumbers"]);
                string ProductFamily = Convert.ToString(Session["SelectedProductFamily"]);
                string ProductGroup = Convert.ToString(Session["SelectedProductGroup"]);
                string ApplicationListVal = Convert.ToString(Session["SelectedApplications"]);

                if (roleId == "TM")
                {
                    objRSum.BranchCode = branchcode == "ALL" ? userId : branchcode;
                }
                else if (roleId == "BM" || roleId == "SE")
                {
                    objRSum.BranchCode = branchcode == "ALL" || branchcode == "" ? Session["BranchCode"].ToString() : branchcode;
                }
                else { objRSum.BranchCode = branchcode == "ALL" ? null : branchcode; }
                if (roleId == "SE")
                {
                    objRSum.salesengineer_id = SalesengList == "ALL" || SalesengList == "" ? Session["UserId"].ToString() : SalesengList;
                }
                else { objRSum.salesengineer_id = SalesengList == "ALL" ? null : SalesengList; }
                objRSum.customer_type = ddlcustomertype.SelectedItem.Text == "ALL" ? null : ddlcustomertype.SelectedItem.Value;
                objRSum.customer_number = CustomerNumlist == "ALL" ? null : CustomerNumlist;
                objRSum.item_family_name = ProductFamily == "ALL" || ProductFamily == "" ? null : ProductFamily;
                objRSum.item_sub_family_name = null;
                objRSum.Year = Year;
                objRSum.flag = flag;
                objRSum.product_group = ProductGroup == "ALL" || ProductGroup == "" ? null : ProductGroup;
                objRSum.item_code = ApplicationListVal == "ALL" || ApplicationListVal == "" ? null : ApplicationListVal;
                objRSum.valuein = valuein;
                objRSum.cter = cter;
                dtmonthval = objRSum.getMonthlyVal(objRSum);

                return dtmonthval;
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
                return null;
            }
        }
        /// <summary>
        /// filling grid view 
        /// </summary>
        /// <returns>DATA TABLE  that contains MTDPLAN,MTDSALE for current year,YTD SALE,YTD PLAN, for current years and ytd sale for previous previous year pro rata achievement and growth percent </returns>
        protected DataTable loadGrid()
        {
            try
            {
                var culture = new CultureInfo("en-us", true)
                {
                    NumberFormat =
                    {
                        NumberGroupSizes = new int[] { 2, 2 }
                    }
                };
                DataTable dtYtdSales = new DataTable();
                dtYtdSales.Columns.Add("title", typeof(string));
                dtYtdSales.Columns.Add("jan", typeof(string));
                dtYtdSales.Columns.Add("feb", typeof(string));
                dtYtdSales.Columns.Add("mar", typeof(string));
                dtYtdSales.Columns.Add("apr", typeof(string));
                dtYtdSales.Columns.Add("may", typeof(string));
                dtYtdSales.Columns.Add("jun", typeof(string));
                dtYtdSales.Columns.Add("jul", typeof(string));
                dtYtdSales.Columns.Add("aug", typeof(string));
                dtYtdSales.Columns.Add("sep", typeof(string));
                dtYtdSales.Columns.Add("oct", typeof(string));
                dtYtdSales.Columns.Add("nov", typeof(string));
                dtYtdSales.Columns.Add("dec", typeof(string));
                dtYtdSales.Columns.Add("flag", typeof(string));
                DataTable temp = new DataTable();
                int ActualYear = objConfig.getActualYear() - 1;
                int YTDYear = ActualYear + 1;
                int MTDYear = ActualYear + 1;
                int currentmonth = System.DateTime.Now.Month;
                int Remain_months = 12 - currentmonth;

                /// Adding Header to Table
                ///  
                dtYtdSales.Rows.Add("", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "Heading");
                /// 
                /// 
                #region YTDSSALE P_act

                /// --------------------------------------------------------------------------------------------------------------------
                /// STEP: 5 YTD SALE 2014
                temp = null;
                temp = getMonthlyvalues(ActualYear - 1, "YTD Sale");

                for (int i = 0; i < temp.Rows.Count; i++)
                {
                    int loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString());
                    int loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString());
                    int loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString());
                    int loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString());
                    int loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[4].ToString());
                    int loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[5].ToString());
                    int loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[6].ToString());
                    int loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[7].ToString());
                    int loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[8].ToString());
                    int loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[9].ToString());
                    int loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[10].ToString());
                    int loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[11].ToString());
                    //pending to add all values
                    dtYtdSales.Rows.Add("YTD SALE " + (ActualYear - 1),
                                       loc1.ToString("N0", culture),
                                       loc2.ToString("N0", culture),
                                       loc3.ToString("N0", culture),
                                       loc4.ToString("N0", culture),
                                       loc5.ToString("N0", culture),
                                       loc6.ToString("N0", culture),
                                       loc7.ToString("N0", culture),
                                       loc8.ToString("N0", culture),
                                       loc9.ToString("N0", culture),
                                       loc10.ToString("N0", culture),
                                       loc11.ToString("N0", culture),
                                       loc12.ToString("N0", culture),
                                       "YTDSALE P_ActualYear"
                                       );

                }

                #endregion
                #region YTDSALE_ACT
                /// --------------------------------------------------------------------------------------------------------------------
                /// STEP: 5 YTD SALE 2014
                temp = null;
                temp = getMonthlyvalues(ActualYear, "YTD Sale");

                for (int i = 0; i < temp.Rows.Count; i++)
                {
                    int loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString());
                    int loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString());
                    int loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString());
                    int loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString());
                    int loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[4].ToString());
                    int loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[5].ToString());
                    int loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[6].ToString());
                    int loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[7].ToString());
                    int loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[8].ToString());
                    int loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[9].ToString());
                    int loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[10].ToString());
                    int loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[11].ToString());
                    //pending to add all values
                    dtYtdSales.Rows.Add("YTD SALE " + ActualYear,
                                       loc1.ToString("N0", culture),
                                       loc2.ToString("N0", culture),
                                       loc3.ToString("N0", culture),
                                       loc4.ToString("N0", culture),
                                       loc5.ToString("N0", culture),
                                       loc6.ToString("N0", culture),
                                       loc7.ToString("N0", culture),
                                       loc8.ToString("N0", culture),
                                       loc9.ToString("N0", culture),
                                       loc10.ToString("N0", culture),
                                       loc11.ToString("N0", culture),
                                       loc12.ToString("N0", culture),
                                       "YTDSALE ActualYear"
                                       );

                }
                #endregion
           
                #region MTPLAN
                /// Step:1 Mtd PLAN 2015
                /// 
                /// --------------------------------------------------------------------------------------------------------------------
                /// MTD PLAN 2015 

                temp = getMonthlyvalues(MTDYear, "MTD Plan");
                decimal YTDBudget = 0; ;
                ///
                /// MTD PLAN 2015 formula = ((BUDGET 2015)-(YTD SALE 2015))/(No. of months remaining) 
                /// Work in progress
                ///               

                DataTable dtSale = new DataTable();
                decimal val1 = 0, val2 = 0, val3 = 0, val4 = 0, val5 = 0, val6 = 0, val7 = 0, val8 = 0, val9 = 0, val10 = 0, val11 = 0, val12 = 0;
                dtSale = getMonthlyvalues(YTDYear, "YTD Sale");
                for (int i = 0; i < temp.Rows.Count; i++)
                {
                    YTDBudget = temp.Rows[i].ItemArray[0].ToString() != "" ? Convert.ToDecimal(temp.Rows[i].ItemArray[0].ToString()) : 0;
                    val1 = YTDBudget / 12; // jan
                    for (int j = 0; j < dtSale.Rows.Count; j++)
                    {
                        if (i == j)
                        {
                            val2 = currentmonth > 1 ? (YTDBudget - (dtSale.Rows[j].ItemArray[0].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[0].ToString()) : 0)) / 11 : val1; //feb
                            val3 = currentmonth > 2 ? (YTDBudget - (dtSale.Rows[j].ItemArray[1].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[1].ToString()) : 0)) / 10 : val2; // mar
                            val4 = currentmonth > 3 ? (YTDBudget - (dtSale.Rows[j].ItemArray[2].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[2].ToString()) : 0)) / 9 : val3; // apr
                            val5 = currentmonth > 4 ? (YTDBudget - (dtSale.Rows[j].ItemArray[3].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[3].ToString()) : 0)) / 8 : val4; //may

                            val6 = currentmonth > 5 ? (YTDBudget - (dtSale.Rows[j].ItemArray[4].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[4].ToString()) : 0)) / 7 : val5;
                            val7 = currentmonth > 6 ? (YTDBudget - (dtSale.Rows[j].ItemArray[5].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[5].ToString()) : 0)) / 6 : val6;
                            val8 = currentmonth > 7 ? (YTDBudget - (dtSale.Rows[j].ItemArray[6].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[6].ToString()) : 0)) / 5 : val7;
                            val9 = currentmonth > 8 ? (YTDBudget - (dtSale.Rows[j].ItemArray[7].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[7].ToString()) : 0)) / 4 : val8;
                            val10 = currentmonth > 9 ? (YTDBudget - (dtSale.Rows[j].ItemArray[8].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[8].ToString()) : 0)) / 3 : val9;
                            val11 = currentmonth > 10 ? (YTDBudget - (dtSale.Rows[j].ItemArray[9].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[9].ToString()) : 0)) / 2 : val10;
                            val12 = currentmonth > 11 ? (YTDBudget - (dtSale.Rows[j].ItemArray[10].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[10].ToString()) : 0)) / 1 : val11;

                            try
                            {
                                dtYtdSales.Rows.Add("MTD PLAN " + MTDYear,
                                                   Math.Round(val1).ToString("N0", culture), //1
                                                   Math.Round(val2).ToString("N0", culture), //2
                                                   Math.Round(val3).ToString("N0", culture), //3
                                                   Math.Round(val4).ToString("N0", culture),//4
                                                   Math.Round(val5).ToString("N0", culture),//5
                                                   Math.Round(val6).ToString("N0", culture),//6
                                                   Math.Round(val7).ToString("N0", culture),//7
                                                   Math.Round(val8).ToString("N0", culture),//8
                                                   Math.Round(val9).ToString("N0", culture),//9
                                                   Math.Round(val10).ToString("N0", culture),//10
                                                   Math.Round(val11).ToString("N0", culture),//11
                                                   Math.Round(val12).ToString("N0", culture),//12
                                                   "MTDPLAN CurrentYear"
                                                   );
                            }
                            catch (Exception ex) { }
                        }
                    }
                }

                #endregion
                #region MTDSALE
                /// Step:2 MTD SALE 2015
                ///---------------------------------------------------------------------------------------------------------------------------
                ///MTD SALE 2015 
                ///MTD SALE 2015 = Actual sale for that month in 2015 ( so if we have logged in June’15,
                ///then Jun-Dec fields would show 0 value; 
                ///only after end of month will data show for that month because we would get monthly dumps only)
                ///
                temp = null;
                temp = getMonthlyvalues(MTDYear, "MTD Sale");
                for (int i = 0; i < temp.Rows.Count; i++)
                {
                    int loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString());
                    int loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString());
                    int loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString());
                    int loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString());
                    int loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[4].ToString());
                    int loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[5].ToString());
                    int loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[6].ToString());
                    int loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[7].ToString());
                    int loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[8].ToString());
                    int loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[9].ToString());
                    int loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[10].ToString());
                    int loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[11].ToString());
                    dtYtdSales.Rows.Add("MTD SALE " + MTDYear,
                                       loc1.ToString("N0", culture),
                                       loc2.ToString("N0", culture),
                                       loc3.ToString("N0", culture),
                                       loc4.ToString("N0", culture),
                                       loc5.ToString("N0", culture),
                                       loc6.ToString("N0", culture),
                                       loc7.ToString("N0", culture),
                                       loc8.ToString("N0", culture),
                                       loc9.ToString("N0", culture),
                                       loc10.ToString("N0", culture),
                                       loc11.ToString("N0", culture),
                                       loc12.ToString("N0", culture),

                                        "MTDSALE CurrentYear");

                }



                /// STEP: 3 YTD PLAN 2015
                /// --------------------------------------------------------------------------------------------------------------------
                /// YTD PLAN 2015
                /// 
                ///If  Cell_month != Actual_month THEN   YTD_Plan(Cell_month) = YTD_Plan(Cell_month -1) + MTD_Plan(Cell_Month) 
                ///If  Cell_month = Actual_month THEN   YTD_Plan(Cell_month) = YTD_actuals(Cell_month -1) + MTD_Plan(Cell_Month) 
                ///
                ///
                temp = null;


                decimal YTD_val1 = 0, YTD_val2 = 0, YTD_val3 = 0, YTD_val4 = 0, YTD_val5 = 0, YTD_val6 = 0, YTD_val7 = 0, YTD_val8 = 0, YTD_val9 = 0, YTD_val10 = 0, YTD_val11 = 0, YTD_val12 = 0;

                for (int j = 0; j < dtSale.Rows.Count; j++)
                {
                    YTD_val1 = YTDBudget / 12; // jan
                    YTD_val2 = currentmonth != 2 ? YTD_val1 + val2 : (val2 + (dtSale.Rows[j].ItemArray[0].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[0].ToString()) : 0)); //feb
                    YTD_val3 = currentmonth != 3 ? YTD_val2 + val3 : (val3 + (dtSale.Rows[j].ItemArray[1].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[1].ToString()) : 0));  // mar
                    YTD_val4 = currentmonth != 4 ? YTD_val3 + val4 : (val4 + (dtSale.Rows[j].ItemArray[2].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[2].ToString()) : 0)); // apr
                    YTD_val5 = currentmonth != 5 ? YTD_val4 + val5 : (val5 + (dtSale.Rows[j].ItemArray[3].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[3].ToString()) : 0)); //may
                    YTD_val6 = currentmonth != 6 ? YTD_val5 + val6 : (val6 + (dtSale.Rows[j].ItemArray[4].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[4].ToString()) : 0));
                    YTD_val7 = currentmonth != 7 ? YTD_val6 + val7 : (val7 + (dtSale.Rows[j].ItemArray[5].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[5].ToString()) : 0));
                    YTD_val8 = currentmonth != 8 ? YTD_val7 + val8 : (val8 + (dtSale.Rows[j].ItemArray[6].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[6].ToString()) : 0));
                    YTD_val9 = currentmonth != 9 ? YTD_val8 + val9 : (val9 + (dtSale.Rows[j].ItemArray[7].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[7].ToString()) : 0));
                    YTD_val10 = currentmonth != 10 ? YTD_val9 + val10 : (val10 + (dtSale.Rows[j].ItemArray[8].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[8].ToString()) : 0));
                    YTD_val11 = currentmonth != 11 ? YTD_val10 + val11 : (val11 + (dtSale.Rows[j].ItemArray[9].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[9].ToString()) : 0));
                    YTD_val12 = currentmonth != 12 ? YTD_val11 + val12 : (val12 + (dtSale.Rows[j].ItemArray[10].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[10].ToString()) : 0));

                    try
                    {
                        dtYtdSales.Rows.Add("YTD PLAN " + YTDYear,
                                           Math.Round(YTD_val1).ToString("N0", culture), //1
                                           Math.Round(YTD_val2).ToString("N0", culture), //2
                                           Math.Round(YTD_val3).ToString("N0", culture), //3
                                           Math.Round(YTD_val4).ToString("N0", culture),//4
                                           Math.Round(YTD_val5).ToString("N0", culture),//5
                                           Math.Round(YTD_val6).ToString("N0", culture),//6
                                           Math.Round(YTD_val7).ToString("N0", culture),//7
                                           Math.Round(YTD_val8).ToString("N0", culture),//8
                                           Math.Round(YTD_val9).ToString("N0", culture),//9
                                           Math.Round(YTD_val10).ToString("N0", culture),//10
                                           Math.Round(YTD_val11).ToString("N0", culture),//11
                                           Math.Round(YTD_val12).ToString("N0", culture),//12
                                           "YTDPLAN CurrentYear"
                                           );
                    }
                    catch (Exception ex) { }

                }


                #endregion
                #region YTDSALE_YTDYEAR
                /// STEP: 4 YTD SALE 2015
                /// 
                ///-----------------------------------------------------------------------------------------------------------------------------------
                ///YTD SALE 2015 
                ///YTD SALE 2015 = Cumulative total of MTD sales, as in d); 
                ///however, if logged in June’15, then Jun-Dec fields would show same data as NA not applicable)
                ///
                temp = null;

                temp = getMonthlyvalues(YTDYear, "YTD Sale");
                for (int i = 0; i < temp.Rows.Count; i++)
                {

                    int loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString());
                    int loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString());
                    int loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString());
                    int loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString());
                    int loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[4].ToString());
                    int loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[5].ToString());
                    int loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[6].ToString());
                    int loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[7].ToString());
                    int loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[8].ToString());
                    int loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[9].ToString());
                    int loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[10].ToString());
                    int loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[11].ToString());

                    dtYtdSales.Rows.Add("YTD SALE " + YTDYear,
                                       currentmonth < 1 ? "0" : Convert.ToInt32(loc1).ToString("N0", culture),
                                       currentmonth < 2 ? "0" : Convert.ToInt32(loc2).ToString("N0", culture),
                                       currentmonth < 3 ? "0" : Convert.ToInt32(loc3).ToString("N0", culture),
                                       currentmonth < 4 ? "0" : Convert.ToInt32(loc4).ToString("N0", culture),
                                       currentmonth < 5 ? "0" : Convert.ToInt32(loc5).ToString("N0", culture),
                                       currentmonth < 6 ? "0" : Convert.ToInt32(loc6).ToString("N0", culture),
                                       currentmonth < 7 ? "0" : Convert.ToInt32(loc7).ToString("N0", culture),
                                       currentmonth < 8 ? "0" : Convert.ToInt32(loc8).ToString("N0", culture),
                                       currentmonth < 9 ? "0" : Convert.ToInt32(loc9).ToString("N0", culture),
                                       currentmonth < 10 ? "0" : Convert.ToInt32(loc10).ToString("N0", culture),
                                       currentmonth < 11 ? "0" : Convert.ToInt32(loc11).ToString("N0", culture),
                                       currentmonth < 12 ? "0" : Convert.ToInt32(loc12).ToString("N0", culture),
                                       "YTDSALE CurrentYear");
                }
#endregion
             

                decimal temp1 = 0, temp2 = 0, temp3 = 0, temp4 = 0, temp5 = 0, temp6 = 0, temp7 = 0, temp8 = 0, temp9 = 0, temp10 = 0, temp11 = 0, temp12 = 0;
                decimal ys1 = 0, ys2 = 0, ys3 = 0, ys4 = 0, ys5 = 0, ys6 = 0, ys7 = 0, ys8 = 0, ys9 = 0, ys10 = 0, ys11 = 0, ys12 = 0;
                decimal ys_a1 = 0, ys_a2 = 0, ys_a3 = 0, ys_a4 = 0, ys_a5 = 0, ys_a6 = 0, ys_a7 = 0, ys_a8 = 0, ys_a9 = 0, ys_a10 = 0, ys_a11 = 0, ys_a12 = 0;
                for (int i = 0; i < dtYtdSales.Rows.Count; i++)
                {
                    string flag = dtYtdSales.Rows[i].ItemArray[13].ToString();
                    //execute one time
                    if (flag == "YTDSALE ActualYear")
                    {
                        ys_a1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                        ys_a2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                        ys_a3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                        ys_a4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                        ys_a5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                        ys_a6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                        ys_a7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                        ys_a8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                        ys_a9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                        ys_a10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                        ys_a11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                        ys_a12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                    }
                    if (flag == "YTDPLAN CurrentYear")
                    {
                        temp1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                        temp2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                        temp3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                        temp4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                        temp5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                        temp6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                        temp7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                        temp8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                        temp9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                        temp10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                        temp11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                        temp12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                    }
                    if (flag == "YTDSALE CurrentYear")
                    {
                        ys1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                        ys2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                        ys3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                        ys4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                        ys5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                        ys6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                        ys7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                        ys8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                        ys9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                        ys10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                        ys11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                        ys12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                    }

                }

                ///
                ///PRO-RATE ACH% = ((YTD SALE 2015)/(YTD PLAN 2015))x100
                ///GROWTH% = ((YTD SALES 2015)/(YTD SALE 2014))x100             
                decimal result1 = 0, result2 = 0, result3 = 0, result4 = 0, result5 = 0, result6 = 0, result7 = 0, result8 = 0, result9 = 0, result10 = 0, result11 = 0, result12 = 0;
              

                ///changed Growth% = ((YTD SALE 2015)-(YTD SALE 2014))*100/YTD PLAN 2014
                result1 = 0; result2 = 0; result3 = 0; result4 = 0; result5 = 0; result6 = 0; result7 = 0; result8 = 0; result9 = 0; result10 = 0; result11 = 0; result12 = 0;
                result1 = ys_a1 != 0 ? (((decimal)ys1 - (decimal)ys_a1) * 100) / (decimal)ys_a1 : 0;
                result2 = ys_a1 != 0 ? (((decimal)ys2 - (decimal)ys_a2) * 100) / (decimal)ys_a2 : 0;
                result3 = ys_a1 != 0 ? (((decimal)ys3 - (decimal)ys_a3) * 100) / (decimal)ys_a3 : 0;
                result4 = ys_a1 != 0 ? (((decimal)ys4 - (decimal)ys_a4) * 100) / (decimal)ys_a4 : 0;
                result5 = ys_a1 != 0 ? (((decimal)ys5 - (decimal)ys_a5) * 100) / (decimal)ys_a5 : 0;
                result6 = ys_a1 != 0 ? (((decimal)ys6 - (decimal)ys_a6) * 100) / (decimal)ys_a6 : 0;
                result7 = ys_a1 != 0 ? (((decimal)ys7 - (decimal)ys_a7) * 100) / (decimal)ys_a7 : 0;
                result8 = ys_a1 != 0 ? (((decimal)ys8 - (decimal)ys_a8) * 100) / (decimal)ys_a8 : 0;
                result9 = ys_a1 != 0 ? (((decimal)ys9 - (decimal)ys_a9) * 100) / (decimal)ys_a9 : 0;
                result10 = ys_a1 != 0 ? (((decimal)ys10 - (decimal)ys_a10) * 100) / (decimal)ys_a10 : 0;
                result11 = ys_a1 != 0 ? (((decimal)ys11 - (decimal)ys_a11) * 100) / (decimal)ys_a11 : 0;
                result12 = ys_a1 != 0 ? (((decimal)ys12 - (decimal)ys_a12) * 100) / (decimal)ys_a12 : 0;
                dtYtdSales.Rows.Add("GROWTH%",
                                     Math.Round(result1),
                                     Math.Round(result2),
                                     Math.Round(result3),
                                     Math.Round(result4),
                                     Math.Round(result5),
                                     Math.Round(result6),
                                     Math.Round(result7),
                                     Math.Round(result8),
                                     Math.Round(result9),
                                     Math.Round(result10),
                                     Math.Round(result11),
                                     Math.Round(result12),
                                     "Growth"
                                    );
                result1 = temp1 != 0 ? ((decimal)ys1 / (decimal)temp1) * 100 : 0;
                result2 = temp2 != 0 ? (((decimal)ys2 / (decimal)temp2) * 100) : 0;
                result3 = temp3 != 0 ? (((decimal)ys3 / (decimal)temp3) * 100) : 0;
                result4 = temp4 != 0 ? (((decimal)ys4 / (decimal)temp4) * 100) : 0;
                result5 = temp5 != 0 ? (((decimal)ys5 / (decimal)temp5) * 100) : 0;
                result6 = temp6 != 0 ? (((decimal)ys6 / (decimal)temp6) * 100) : 0;
                result7 = temp7 != 0 ? (((decimal)ys7 / (decimal)temp7) * 100) : 0;
                result8 = temp8 != 0 ? (((decimal)ys8 / (decimal)temp8) * 100) : 0;
                result9 = temp9 != 0 ? (((decimal)ys9 / (decimal)temp9) * 100) : 0;
                result10 = temp10 != 0 ? (((decimal)ys10 / (decimal)temp10) * 100) : 0;
                result11 = temp11 != 0 ? (((decimal)ys11 / (decimal)temp11) * 100) : 0;
                result12 = temp12 != 0 ? (((decimal)ys12 / (decimal)temp12) * 100) : 0;
                dtYtdSales.Rows.Add("PRO-RATA ACH%",
                                     Math.Round(result1),
                                     Math.Round(result2),
                                     Math.Round(result3),
                                     Math.Round(result4),
                                     Math.Round(result5),
                                     Math.Round(result6),
                                     Math.Round(result7),
                                     Math.Round(result8),
                                     Math.Round(result9),
                                     Math.Round(result10),
                                     Math.Round(result11),
                                     Math.Round(result12),
                                     "ProRate"
                                    );

                return dtYtdSales;
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
                return null;
            }
        }
        /// <summary>
        /// load data into gridview in lakh if user selects units as lakh
        /// </summary>
        /// <returns></returns>
        protected DataTable loadGrid_lakh()
        {
            try
            {

                var culture = new CultureInfo("en-us", true)
                {
                    NumberFormat =
                    {
                        NumberGroupSizes = new int[] { 2, 2 }
                    }
                };
                DataTable dtYtdSales = new DataTable();
                dtYtdSales.Columns.Add("title", typeof(string));
                dtYtdSales.Columns.Add("jan", typeof(string));
                dtYtdSales.Columns.Add("feb", typeof(string));
                dtYtdSales.Columns.Add("mar", typeof(string));
                dtYtdSales.Columns.Add("apr", typeof(string));
                dtYtdSales.Columns.Add("may", typeof(string));
                dtYtdSales.Columns.Add("jun", typeof(string));
                dtYtdSales.Columns.Add("jul", typeof(string));
                dtYtdSales.Columns.Add("aug", typeof(string));
                dtYtdSales.Columns.Add("sep", typeof(string));
                dtYtdSales.Columns.Add("oct", typeof(string));
                dtYtdSales.Columns.Add("nov", typeof(string));
                dtYtdSales.Columns.Add("dec", typeof(string));
                dtYtdSales.Columns.Add("flag", typeof(string));
                DataTable temp = new DataTable();
                int ActualYear = objConfig.getActualYear() - 1;
             //   int ActualYear = objConfig.getActualYear() - 2;
                int YTDYear = ActualYear + 1;
                int MTDYear = ActualYear+ 1;
                int currentmonth = System.DateTime.Now.Month;
                int Remain_months = 12 - currentmonth;

                /// Adding Header to Table
                ///  
                dtYtdSales.Rows.Add("", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "Heading");

                #region YTDSALE_PACT

                temp = null;
                temp = getMonthlyvalues(ActualYear - 1, "YTD Sale", 100000);

                for (int i = 0; i < temp.Rows.Count; i++)
                {
                    int loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString());
                    int loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString());
                    int loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString());
                    int loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString());
                    int loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[4].ToString());
                    int loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[5].ToString());
                    int loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[6].ToString());
                    int loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[7].ToString());
                    int loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[8].ToString());
                    int loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[9].ToString());
                    int loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[10].ToString());
                    int loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[11].ToString());
                    //pending to add all values
                    dtYtdSales.Rows.Add("YTD SALE " + (ActualYear - 1),
                                       loc1.ToString("N0", culture),
                                       loc2.ToString("N0", culture),
                                       loc3.ToString("N0", culture),
                                       loc4.ToString("N0", culture),
                                       loc5.ToString("N0", culture),
                                       loc6.ToString("N0", culture),
                                       loc7.ToString("N0", culture),
                                       loc8.ToString("N0", culture),
                                       loc9.ToString("N0", culture),
                                       loc10.ToString("N0", culture),
                                       loc11.ToString("N0", culture),
                                       loc12.ToString("N0", culture),
                                       "YTDSALE  P_ActualYear"
                                       );

                }



                #endregion
                #region YTDSALE_ACT
                /// --------------------------------------------------------------------------------------------------------------------
                /// STEP: 5 YTD SALE 2014
                temp = null;
                temp = getMonthlyvalues(ActualYear, "YTD Sale", 100000);

                for (int i = 0; i < temp.Rows.Count; i++)
                {
                    int loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString());
                    int loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString());
                    int loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString());
                    int loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString());
                    int loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[4].ToString());
                    int loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[5].ToString());
                    int loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[6].ToString());
                    int loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[7].ToString());
                    int loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[8].ToString());
                    int loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[9].ToString());
                    int loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[10].ToString());
                    int loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[11].ToString());
                    //pending to add all values
                    dtYtdSales.Rows.Add("YTD SALE " + ActualYear,
                                       loc1.ToString("N0", culture),
                                       loc2.ToString("N0", culture),
                                       loc3.ToString("N0", culture),
                                       loc4.ToString("N0", culture),
                                       loc5.ToString("N0", culture),
                                       loc6.ToString("N0", culture),
                                       loc7.ToString("N0", culture),
                                       loc8.ToString("N0", culture),
                                       loc9.ToString("N0", culture),
                                       loc10.ToString("N0", culture),
                                       loc11.ToString("N0", culture),
                                       loc12.ToString("N0", culture),
                                       "YTDSALE ActualYear"
                                       );

                }

                #endregion
                #region MTDPLAN
                /// 
                /// Step:1 Mtd PLAN 2015
                /// 
                /// --------------------------------------------------------------------------------------------------------------------
                /// MTD PLAN 2015 

                temp = getMonthlyvalues(MTDYear, "MTD Plan", 100000);
                decimal YTDBudget = 0; ;
                ///
                /// MTD PLAN 2015 formula = ((BUDGET 2015)-(YTD SALE 2015))/(No. of months remaining) 
                /// Work in progress
                ///               

                DataTable dtSale = new DataTable();
                decimal val1 = 0, val2 = 0, val3 = 0, val4 = 0, val5 = 0, val6 = 0, val7 = 0, val8 = 0, val9 = 0, val10 = 0, val11 = 0, val12 = 0;
                dtSale = getMonthlyvalues(YTDYear, "YTD Sale", 100000);
                for (int i = 0; i < temp.Rows.Count; i++)
                {
                    YTDBudget = temp.Rows[i].ItemArray[0].ToString() != "" ? Convert.ToDecimal(temp.Rows[i].ItemArray[0].ToString()) : 0;
                    val1 = YTDBudget / 12; // jan
                    for (int j = 0; j < dtSale.Rows.Count; j++)
                    {
                        if (i == j)
                        {
                            val2 = currentmonth > 1 ? (YTDBudget - (dtSale.Rows[j].ItemArray[0].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[0].ToString()) : 0)) / 11 : val1; //feb
                            val3 = currentmonth > 2 ? (YTDBudget - (dtSale.Rows[j].ItemArray[1].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[1].ToString()) : 0)) / 10 : val2; // mar
                            val4 = currentmonth > 3 ? (YTDBudget - (dtSale.Rows[j].ItemArray[2].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[2].ToString()) : 0)) / 9 : val3; // apr
                            val5 = currentmonth > 4 ? (YTDBudget - (dtSale.Rows[j].ItemArray[3].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[3].ToString()) : 0)) / 8 : val4; //may

                            val6 = currentmonth > 5 ? (YTDBudget - (dtSale.Rows[j].ItemArray[4].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[4].ToString()) : 0)) / 7 : val5;
                            val7 = currentmonth > 6 ? (YTDBudget - (dtSale.Rows[j].ItemArray[5].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[5].ToString()) : 0)) / 6 : val6;
                            val8 = currentmonth > 7 ? (YTDBudget - (dtSale.Rows[j].ItemArray[6].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[6].ToString()) : 0)) / 5 : val7;
                            val9 = currentmonth > 8 ? (YTDBudget - (dtSale.Rows[j].ItemArray[7].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[7].ToString()) : 0)) / 4 : val8;
                            val10 = currentmonth > 9 ? (YTDBudget - (dtSale.Rows[j].ItemArray[8].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[8].ToString()) : 0)) / 3 : val9;
                            val11 = currentmonth > 10 ? (YTDBudget - (dtSale.Rows[j].ItemArray[9].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[9].ToString()) : 0)) / 2 : val10;
                            val12 = currentmonth > 11 ? (YTDBudget - (dtSale.Rows[j].ItemArray[10].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[10].ToString()) : 0)) / 1 : val11;

                            try
                            {
                                dtYtdSales.Rows.Add("MTD PLAN " + MTDYear,
                                                   Math.Round(val1).ToString("N0", culture), //1
                                                   Math.Round(val2).ToString("N0", culture), //2
                                                   Math.Round(val3).ToString("N0", culture), //3
                                                   Math.Round(val4).ToString("N0", culture),//4
                                                   Math.Round(val5).ToString("N0", culture),//5
                                                   Math.Round(val6).ToString("N0", culture),//6
                                                   Math.Round(val7).ToString("N0", culture),//7
                                                   Math.Round(val8).ToString("N0", culture),//8
                                                   Math.Round(val9).ToString("N0", culture),//9
                                                   Math.Round(val10).ToString("N0", culture),//10
                                                   Math.Round(val11).ToString("N0", culture),//11
                                                   Math.Round(val12).ToString("N0", culture),//12
                                                   "MTDPLAN CurrentYear"
                                                   );
                            }
                            catch (Exception ex) { }
                        }
                    }
                }

                #endregion
                #region YTDPLAN
                /// STEP: 3 YTD PLAN 2015
                /// --------------------------------------------------------------------------------------------------------------------
                /// YTD PLAN 2015
                /// 
                ///If  Cell_month != Actual_month THEN   YTD_Plan(Cell_month) = YTD_Plan(Cell_month -1) + MTD_Plan(Cell_Month) 
                ///If  Cell_month = Actual_month THEN   YTD_Plan(Cell_month) = YTD_actuals(Cell_month -1) + MTD_Plan(Cell_Month) 
                ///
                ///
                temp = null;


                decimal YTD_val1 = 0, YTD_val2 = 0, YTD_val3 = 0, YTD_val4 = 0, YTD_val5 = 0, YTD_val6 = 0, YTD_val7 = 0, YTD_val8 = 0, YTD_val9 = 0, YTD_val10 = 0, YTD_val11 = 0, YTD_val12 = 0;

                for (int j = 0; j < dtSale.Rows.Count; j++)
                {
                    YTD_val1 = YTDBudget / 12; // jan
                    YTD_val2 = currentmonth != 2 ? YTD_val1 + val2 : (val2 + (dtSale.Rows[j].ItemArray[0].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[0].ToString()) : 0)); //feb
                    YTD_val3 = currentmonth != 3 ? YTD_val2 + val3 : (val3 + (dtSale.Rows[j].ItemArray[1].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[1].ToString()) : 0));  // mar
                    YTD_val4 = currentmonth != 4 ? YTD_val3 + val4 : (val4 + (dtSale.Rows[j].ItemArray[2].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[2].ToString()) : 0)); // apr
                    YTD_val5 = currentmonth != 5 ? YTD_val4 + val5 : (val5 + (dtSale.Rows[j].ItemArray[3].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[3].ToString()) : 0)); //may
                    YTD_val6 = currentmonth != 6 ? YTD_val5 + val6 : (val6 + (dtSale.Rows[j].ItemArray[4].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[4].ToString()) : 0));
                    YTD_val7 = currentmonth != 7 ? YTD_val6 + val7 : (val7 + (dtSale.Rows[j].ItemArray[5].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[5].ToString()) : 0));
                    YTD_val8 = currentmonth != 8 ? YTD_val7 + val8 : (val8 + (dtSale.Rows[j].ItemArray[6].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[6].ToString()) : 0));
                    YTD_val9 = currentmonth != 9 ? YTD_val8 + val9 : (val9 + (dtSale.Rows[j].ItemArray[7].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[7].ToString()) : 0));
                    YTD_val10 = currentmonth != 10 ? YTD_val9 + val10 : (val10 + (dtSale.Rows[j].ItemArray[8].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[8].ToString()) : 0));
                    YTD_val11 = currentmonth != 11 ? YTD_val10 + val11 : (val11 + (dtSale.Rows[j].ItemArray[9].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[9].ToString()) : 0));
                    YTD_val12 = currentmonth != 12 ? YTD_val11 + val12 : (val12 + (dtSale.Rows[j].ItemArray[10].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[10].ToString()) : 0));

                    try
                    {
                        dtYtdSales.Rows.Add("YTD PLAN " + YTDYear,
                                           Math.Round(YTD_val1).ToString("N0", culture), //1
                                           Math.Round(YTD_val2).ToString("N0", culture), //2
                                           Math.Round(YTD_val3).ToString("N0", culture), //3
                                           Math.Round(YTD_val4).ToString("N0", culture),//4
                                           Math.Round(YTD_val5).ToString("N0", culture),//5
                                           Math.Round(YTD_val6).ToString("N0", culture),//6
                                           Math.Round(YTD_val7).ToString("N0", culture),//7
                                           Math.Round(YTD_val8).ToString("N0", culture),//8
                                           Math.Round(YTD_val9).ToString("N0", culture),//9
                                           Math.Round(YTD_val10).ToString("N0", culture),//10
                                           Math.Round(YTD_val11).ToString("N0", culture),//11
                                           Math.Round(YTD_val12).ToString("N0", culture),//12
                                           "YTDPLAN CurrentYear"
                                           );
                    }
                    catch (Exception ex) { }

                }
                #endregion
                #region MTDSALE
                /// Step:2 MTD SALE 2015
                ///---------------------------------------------------------------------------------------------------------------------------
                ///MTD SALE 2015 
                ///MTD SALE 2015 = Actual sale for that month in 2015 ( so if we have logged in June’15,
                ///then Jun-Dec fields would show 0 value; 
                ///only after end of month will data show for that month because we would get monthly dumps only)
                ///
                temp = null;
                temp = getMonthlyvalues(MTDYear, "MTD Sale", 100000);

                for (int i = 0; i < temp.Rows.Count; i++)
                {
                    int loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString());
                    int loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString());
                    int loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString());
                    int loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString());
                    int loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[4].ToString());
                    int loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[5].ToString());
                    int loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[6].ToString());
                    int loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[7].ToString());
                    int loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[8].ToString());
                    int loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[9].ToString());
                    int loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[10].ToString());
                    int loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[11].ToString());
                    dtYtdSales.Rows.Add("MTD SALE " + MTDYear,
                                       loc1.ToString("N0", culture),
                                       loc2.ToString("N0", culture),
                                       loc3.ToString("N0", culture),
                                       loc4.ToString("N0", culture),
                                       loc5.ToString("N0", culture),
                                       loc6.ToString("N0", culture),
                                       loc7.ToString("N0", culture),
                                       loc8.ToString("N0", culture),
                                       loc9.ToString("N0", culture),
                                       loc10.ToString("N0", culture),
                                       loc11.ToString("N0", culture),
                                       loc12.ToString("N0", culture),

                                        "MTDSALE CurrentYear");

                }
#endregion
            
                #region YTDSALE

                /// STEP: 4 YTD SALE 2015
                /// 
                ///-----------------------------------------------------------------------------------------------------------------------------------
                ///YTD SALE 2015 
                ///YTD SALE 2015 = Cumulative total of MTD sales, as in d); 
                ///however, if logged in June’15, then Jun-Dec fields would show same data as NA not applicable)
                ///
                temp = null;

                temp = getMonthlyvalues(YTDYear, "YTD Sale", 100000);

                for (int i = 0; i < temp.Rows.Count; i++)
                {
                    int loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString());
                    int loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString());
                    int loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString());
                    int loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString());
                    int loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[4].ToString());
                    int loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[5].ToString());
                    int loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[6].ToString());
                    int loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[7].ToString());
                    int loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[8].ToString());
                    int loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[9].ToString());
                    int loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[10].ToString());
                    int loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[11].ToString());

                    dtYtdSales.Rows.Add("YTD SALE " + YTDYear,
                                       currentmonth < 1 ? "0" : Convert.ToInt32(loc1).ToString("N0", culture),
                                       currentmonth < 2 ? "0" : Convert.ToInt32(loc2).ToString("N0", culture),
                                       currentmonth < 3 ? "0" : Convert.ToInt32(loc3).ToString("N0", culture),
                                       currentmonth < 4 ? "0" : Convert.ToInt32(loc4).ToString("N0", culture),
                                       currentmonth < 5 ? "0" : Convert.ToInt32(loc5).ToString("N0", culture),
                                       currentmonth < 6 ? "0" : Convert.ToInt32(loc6).ToString("N0", culture),
                                       currentmonth < 7 ? "0" : Convert.ToInt32(loc7).ToString("N0", culture),
                                       currentmonth < 8 ? "0" : Convert.ToInt32(loc8).ToString("N0", culture),
                                       currentmonth < 9 ? "0" : Convert.ToInt32(loc9).ToString("N0", culture),
                                       currentmonth < 10 ? "0" : Convert.ToInt32(loc10).ToString("N0", culture),
                                       currentmonth < 11 ? "0" : Convert.ToInt32(loc11).ToString("N0", culture),
                                       currentmonth < 12 ? "0" : Convert.ToInt32(loc12).ToString("N0", culture),
                                       "YTDSALE CurrentYear");
                }
                #endregion
               

              

                decimal temp1 = 0, temp2 = 0, temp3 = 0, temp4 = 0, temp5 = 0, temp6 = 0, temp7 = 0, temp8 = 0, temp9 = 0, temp10 = 0, temp11 = 0, temp12 = 0;
                decimal ys1 = 0, ys2 = 0, ys3 = 0, ys4 = 0, ys5 = 0, ys6 = 0, ys7 = 0, ys8 = 0, ys9 = 0, ys10 = 0, ys11 = 0, ys12 = 0;
                decimal ys_a1 = 0, ys_a2 = 0, ys_a3 = 0, ys_a4 = 0, ys_a5 = 0, ys_a6 = 0, ys_a7 = 0, ys_a8 = 0, ys_a9 = 0, ys_a10 = 0, ys_a11 = 0, ys_a12 = 0;
                for (int i = 0; i < dtYtdSales.Rows.Count; i++)
                {
                    string flag = dtYtdSales.Rows[i].ItemArray[13].ToString();
                    //execute one time
                    if (flag == "YTDSALE ActualYear")
                    {
                        ys_a1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                        ys_a2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                        ys_a3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                        ys_a4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                        ys_a5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                        ys_a6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                        ys_a7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                        ys_a8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                        ys_a9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                        ys_a10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                        ys_a11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                        ys_a12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                    }
                    if (flag == "YTDPLAN CurrentYear")
                    {
                        temp1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                        temp2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                        temp3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                        temp4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                        temp5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                        temp6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                        temp7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                        temp8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                        temp9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                        temp10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                        temp11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                        temp12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                    }
                    if (flag == "YTDSALE CurrentYear")
                    {
                        ys1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                        ys2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                        ys3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                        ys4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                        ys5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                        ys6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                        ys7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                        ys8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                        ys9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                        ys10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                        ys11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                        ys12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                    }


                }

                ///

                #region Growth
                decimal result1 = 0, result2 = 0, result3 = 0, result4 = 0, result5 = 0, result6 = 0, result7 = 0, result8 = 0, result9 = 0, result10 = 0, result11 = 0, result12 = 0;
               
                ///changed Growth% = ((YTD SALE 2015)-(YTD SALE 2014))*100/YTD SALE 2014
                result1 = 0; result2 = 0; result3 = 0; result4 = 0; result5 = 0; result6 = 0; result7 = 0; result8 = 0; result9 = 0; result10 = 0; result11 = 0; result12 = 0;
                result1 = ys_a1 != 0 ? (((decimal)ys1 - (decimal)ys_a1) * 100) / (decimal)ys_a1 : 0;
                result2 = ys_a1 != 0 ? (((decimal)ys2 - (decimal)ys_a2) * 100) / (decimal)ys_a2 : 0;
                result3 = ys_a1 != 0 ? (((decimal)ys3 - (decimal)ys_a3) * 100) / (decimal)ys_a3 : 0;
                result4 = ys_a1 != 0 ? (((decimal)ys4 - (decimal)ys_a4) * 100) / (decimal)ys_a4 : 0;
                result5 = ys_a1 != 0 ? (((decimal)ys5 - (decimal)ys_a5) * 100) / (decimal)ys_a5 : 0;
                result6 = ys_a1 != 0 ? (((decimal)ys6 - (decimal)ys_a6) * 100) / (decimal)ys_a6 : 0;
                result7 = ys_a1 != 0 ? (((decimal)ys7 - (decimal)ys_a7) * 100) / (decimal)ys_a7 : 0;
                result8 = ys_a1 != 0 ? (((decimal)ys8 - (decimal)ys_a8) * 100) / (decimal)ys_a8 : 0;
                result9 = ys_a1 != 0 ? (((decimal)ys9 - (decimal)ys_a9) * 100) / (decimal)ys_a9 : 0;
                result10 = ys_a1 != 0 ? (((decimal)ys10 - (decimal)ys_a10) * 100) / (decimal)ys_a10 : 0;
                result11 = ys_a1 != 0 ? (((decimal)ys11 - (decimal)ys_a11) * 100) / (decimal)ys_a11 : 0;
                result12 = ys_a1 != 0 ? (((decimal)ys12 - (decimal)ys_a12) * 100) / (decimal)ys_a12 : 0;
                dtYtdSales.Rows.Add("GROWTH%",
                                     Math.Round(result1),
                                     Math.Round(result2),
                                     Math.Round(result3),
                                     Math.Round(result4),
                                     Math.Round(result5),
                                     Math.Round(result6),
                                     Math.Round(result7),
                                     Math.Round(result8),
                                     Math.Round(result9),
                                     Math.Round(result10),
                                     Math.Round(result11),
                                     Math.Round(result12),
                                     "Growth"
                                    );
#endregion
                #region ProRata
                ///PRO-RATE ACH% = ((YTD SALE 2015)/(YTD PLAN 2015))x100
                result1 = temp1 != 0 ? ((decimal)ys1 / (decimal)temp1) * 100 : 0;
                result2 = temp2 != 0 ? (((decimal)ys2 / (decimal)temp2) * 100) : 0;
                result3 = temp3 != 0 ? (((decimal)ys3 / (decimal)temp3) * 100) : 0;
                result4 = temp4 != 0 ? (((decimal)ys4 / (decimal)temp4) * 100) : 0;
                result5 = temp5 != 0 ? (((decimal)ys5 / (decimal)temp5) * 100) : 0;
                result6 = temp6 != 0 ? (((decimal)ys6 / (decimal)temp6) * 100) : 0;
                result7 = temp7 != 0 ? (((decimal)ys7 / (decimal)temp7) * 100) : 0;
                result8 = temp8 != 0 ? (((decimal)ys8 / (decimal)temp8) * 100) : 0;
                result9 = temp9 != 0 ? (((decimal)ys9 / (decimal)temp9) * 100) : 0;
                result10 = temp10 != 0 ? (((decimal)ys10 / (decimal)temp10) * 100) : 0;
                result11 = temp11 != 0 ? (((decimal)ys11 / (decimal)temp11) * 100) : 0;
                result12 = temp12 != 0 ? (((decimal)ys12 / (decimal)temp12) * 100) : 0;
                dtYtdSales.Rows.Add("PRO-RATA ACH%",
                                     Math.Round(result1),
                                     Math.Round(result2),
                                     Math.Round(result3),
                                     Math.Round(result4),
                                     Math.Round(result5),
                                     Math.Round(result6),
                                     Math.Round(result7),
                                     Math.Round(result8),
                                     Math.Round(result9),
                                     Math.Round(result10),
                                     Math.Round(result11),
                                     Math.Round(result12),
                                     "ProRate"
                                    );

#endregion
                return dtYtdSales;
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
                return null;
            }
        }
        /// <summary>
        /// loads data into gridview in units if user selects units as units
        /// </summary>
        /// <returns></returns>
        protected DataTable loadGrid_Unit()
        {
            try
            {
                var culture = new CultureInfo("en-us", true)
                {
                    NumberFormat =
                    {
                        NumberGroupSizes = new int[] { 3, 3 }
                    }
                };
                DataTable dtYtdSales = new DataTable();
                dtYtdSales.Columns.Add("title", typeof(string));
                dtYtdSales.Columns.Add("jan", typeof(string));
                dtYtdSales.Columns.Add("feb", typeof(string));
                dtYtdSales.Columns.Add("mar", typeof(string));
                dtYtdSales.Columns.Add("apr", typeof(string));
                dtYtdSales.Columns.Add("may", typeof(string));
                dtYtdSales.Columns.Add("jun", typeof(string));
                dtYtdSales.Columns.Add("jul", typeof(string));
                dtYtdSales.Columns.Add("aug", typeof(string));
                dtYtdSales.Columns.Add("sep", typeof(string));
                dtYtdSales.Columns.Add("oct", typeof(string));
                dtYtdSales.Columns.Add("nov", typeof(string));
                dtYtdSales.Columns.Add("dec", typeof(string));
                dtYtdSales.Columns.Add("flag", typeof(string));
                DataTable temp = new DataTable();
                int ActualYear = objConfig.getActualYear() - 1;
                int YTDYear = ActualYear + 1;
                int MTDYear = ActualYear + 1;
                int currentmonth = System.DateTime.Now.Month;
                int Remain_months = 12 - currentmonth;

                /// Adding Header to Table
                ///  
                dtYtdSales.Rows.Add("", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "Heading");
                #region YTDSALE_PACT

                temp = null;
                temp = getMonthlyvalues(ActualYear - 1, "YTD Sale", 1);

                for (int i = 0; i < temp.Rows.Count; i++)
                {
                    long loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[0].ToString());
                    long loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[1].ToString());
                    long loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[2].ToString());
                    long loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[3].ToString());
                    long loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[4].ToString());
                    long loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[5].ToString());
                    long loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[6].ToString());
                    long loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[7].ToString());
                    long loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[8].ToString());
                    long loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[9].ToString());
                    long loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[10].ToString());
                    long loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[11].ToString());
                    //pending to add all values
                    dtYtdSales.Rows.Add("YTD SALE " + (ActualYear - 1),
                                       loc1.ToString("N0", culture),
                                       loc2.ToString("N0", culture),
                                       loc3.ToString("N0", culture),
                                       loc4.ToString("N0", culture),
                                       loc5.ToString("N0", culture),
                                       loc6.ToString("N0", culture),
                                       loc7.ToString("N0", culture),
                                       loc8.ToString("N0", culture),
                                       loc9.ToString("N0", culture),
                                       loc10.ToString("N0", culture),
                                       loc11.ToString("N0", culture),
                                       loc12.ToString("N0", culture),
                                       "YTDSALE ActualYear_PACT"
                                       );
                }
                #endregion
                #region YTDSALE_ACT
                /// --------------------------------------------------------------------------------------------------------------------
                /// STEP: 5 YTD SALE 2014
                temp = null;
                temp = getMonthlyvalues(ActualYear, "YTD Sale", 1);

                for (int i = 0; i < temp.Rows.Count; i++)
                {
                    long loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[0].ToString());
                    long loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[1].ToString());
                    long loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[2].ToString());
                    long loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[3].ToString());
                    long loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[4].ToString());
                    long loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[5].ToString());
                    long loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[6].ToString());
                    long loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[7].ToString());
                    long loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[8].ToString());
                    long loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[9].ToString());
                    long loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[10].ToString());
                    long loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[11].ToString());
                    //pending to add all values
                    dtYtdSales.Rows.Add("YTD SALE " + ActualYear,
                                       loc1.ToString("N0", culture),
                                       loc2.ToString("N0", culture),
                                       loc3.ToString("N0", culture),
                                       loc4.ToString("N0", culture),
                                       loc5.ToString("N0", culture),
                                       loc6.ToString("N0", culture),
                                       loc7.ToString("N0", culture),
                                       loc8.ToString("N0", culture),
                                       loc9.ToString("N0", culture),
                                       loc10.ToString("N0", culture),
                                       loc11.ToString("N0", culture),
                                       loc12.ToString("N0", culture),
                                       "YTDSALE ActualYear"
                                       );
                }
                #endregion
                #region MTDPLAN

                /// 
                /// Step:1 Mtd PLAN 2015
                /// 
                /// --------------------------------------------------------------------------------------------------------------------
                /// MTD PLAN 2015 

                temp = getMonthlyvalues(MTDYear, "MTD Plan", 1);
                decimal YTDBudget = 0; ;
                ///
                /// MTD PLAN 2015 formula = ((BUDGET 2015)-(YTD SALE 2015))/(No. of months remaining) 
                /// Work in progress
                ///               

                DataTable dtSale = new DataTable();
                decimal val1 = 0, val2 = 0, val3 = 0, val4 = 0, val5 = 0, val6 = 0, val7 = 0, val8 = 0, val9 = 0, val10 = 0, val11 = 0, val12 = 0;
                dtSale = getMonthlyvalues(YTDYear, "YTD Sale", 1);
                for (int i = 0; i < temp.Rows.Count; i++)
                {
                    YTDBudget = temp.Rows[i].ItemArray[0].ToString() != "" ? Convert.ToDecimal(temp.Rows[i].ItemArray[0].ToString()) : 0;
                    val1 = YTDBudget / 12; // jan
                    for (int j = 0; j < dtSale.Rows.Count; j++)
                    {
                        if (i == j)
                        {
                            val2 = currentmonth > 1 ? (YTDBudget - (dtSale.Rows[j].ItemArray[0].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[0].ToString()) : 0)) / 11 : val1; //feb
                            val3 = currentmonth > 2 ? (YTDBudget - (dtSale.Rows[j].ItemArray[1].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[1].ToString()) : 0)) / 10 : val2; // mar
                            val4 = currentmonth > 3 ? (YTDBudget - (dtSale.Rows[j].ItemArray[2].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[2].ToString()) : 0)) / 9 : val3; // apr
                            val5 = currentmonth > 4 ? (YTDBudget - (dtSale.Rows[j].ItemArray[3].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[3].ToString()) : 0)) / 8 : val4; //may

                            val6 = currentmonth > 5 ? (YTDBudget - (dtSale.Rows[j].ItemArray[4].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[4].ToString()) : 0)) / 7 : val5;
                            val7 = currentmonth > 6 ? (YTDBudget - (dtSale.Rows[j].ItemArray[5].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[5].ToString()) : 0)) / 6 : val6;
                            val8 = currentmonth > 7 ? (YTDBudget - (dtSale.Rows[j].ItemArray[6].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[6].ToString()) : 0)) / 5 : val7;
                            val9 = currentmonth > 8 ? (YTDBudget - (dtSale.Rows[j].ItemArray[7].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[7].ToString()) : 0)) / 4 : val8;
                            val10 = currentmonth > 9 ? (YTDBudget - (dtSale.Rows[j].ItemArray[8].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[8].ToString()) : 0)) / 3 : val9;
                            val11 = currentmonth > 10 ? (YTDBudget - (dtSale.Rows[j].ItemArray[9].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[9].ToString()) : 0)) / 2 : val10;
                            val12 = currentmonth > 11 ? (YTDBudget - (dtSale.Rows[j].ItemArray[10].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[10].ToString()) : 0)) / 1 : val11;

                            try
                            {
                                dtYtdSales.Rows.Add("MTD PLAN " + MTDYear,
                                                   Math.Round(val1).ToString("N0", culture), //1
                                                   Math.Round(val2).ToString("N0", culture), //2
                                                   Math.Round(val3).ToString("N0", culture), //3
                                                   Math.Round(val4).ToString("N0", culture),//4
                                                   Math.Round(val5).ToString("N0", culture),//5
                                                   Math.Round(val6).ToString("N0", culture),//6
                                                   Math.Round(val7).ToString("N0", culture),//7
                                                   Math.Round(val8).ToString("N0", culture),//8
                                                   Math.Round(val9).ToString("N0", culture),//9
                                                   Math.Round(val10).ToString("N0", culture),//10
                                                   Math.Round(val11).ToString("N0", culture),//11
                                                   Math.Round(val12).ToString("N0", culture),//12
                                                   "MTDPLAN CurrentYear"
                                                   );
                            }
                            catch (Exception ex) {
                                CommonFunctions.LogErrorStatic(ex);
                            }
                        }
                    }
                }
                #endregion
                #region YTDPLAN_YTDYEAR
                /// STEP: 3 YTD PLAN 2015
                /// --------------------------------------------------------------------------------------------------------------------
                /// YTD PLAN 2015
                /// 
                ///If  Cell_month != Actual_month THEN   YTD_Plan(Cell_month) = YTD_Plan(Cell_month -1) + MTD_Plan(Cell_Month) 
                ///If  Cell_month = Actual_month THEN   YTD_Plan(Cell_month) = YTD_actuals(Cell_month -1) + MTD_Plan(Cell_Month) 
                ///
                ///
                temp = null;


                decimal YTD_val1 = 0, YTD_val2 = 0, YTD_val3 = 0, YTD_val4 = 0, YTD_val5 = 0, YTD_val6 = 0, YTD_val7 = 0, YTD_val8 = 0, YTD_val9 = 0, YTD_val10 = 0, YTD_val11 = 0, YTD_val12 = 0;

                for (int j = 0; j < dtSale.Rows.Count; j++)
                {
                    YTD_val1 = YTDBudget / 12; // jan
                    YTD_val2 = currentmonth != 2 ? YTD_val1 + val2 : (val2 + (dtSale.Rows[j].ItemArray[0].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[0].ToString()) : 0)); //feb
                    YTD_val3 = currentmonth != 3 ? YTD_val2 + val3 : (val3 + (dtSale.Rows[j].ItemArray[1].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[1].ToString()) : 0));  // mar
                    YTD_val4 = currentmonth != 4 ? YTD_val3 + val4 : (val4 + (dtSale.Rows[j].ItemArray[2].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[2].ToString()) : 0)); // apr
                    YTD_val5 = currentmonth != 5 ? YTD_val4 + val5 : (val5 + (dtSale.Rows[j].ItemArray[3].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[3].ToString()) : 0)); //may
                    YTD_val6 = currentmonth != 6 ? YTD_val5 + val6 : (val6 + (dtSale.Rows[j].ItemArray[4].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[4].ToString()) : 0));
                    YTD_val7 = currentmonth != 7 ? YTD_val6 + val7 : (val7 + (dtSale.Rows[j].ItemArray[5].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[5].ToString()) : 0));
                    YTD_val8 = currentmonth != 8 ? YTD_val7 + val8 : (val8 + (dtSale.Rows[j].ItemArray[6].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[6].ToString()) : 0));
                    YTD_val9 = currentmonth != 9 ? YTD_val8 + val9 : (val9 + (dtSale.Rows[j].ItemArray[7].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[7].ToString()) : 0));
                    YTD_val10 = currentmonth != 10 ? YTD_val9 + val10 : (val10 + (dtSale.Rows[j].ItemArray[8].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[8].ToString()) : 0));
                    YTD_val11 = currentmonth != 11 ? YTD_val10 + val11 : (val11 + (dtSale.Rows[j].ItemArray[9].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[9].ToString()) : 0));
                    YTD_val12 = currentmonth != 12 ? YTD_val11 + val12 : (val12 + (dtSale.Rows[j].ItemArray[10].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[10].ToString()) : 0));

                    try
                    {
                        dtYtdSales.Rows.Add("YTD PLAN " + YTDYear,
                                           Math.Round(YTD_val1).ToString("N0", culture), //1
                                           Math.Round(YTD_val2).ToString("N0", culture), //2
                                           Math.Round(YTD_val3).ToString("N0", culture), //3
                                           Math.Round(YTD_val4).ToString("N0", culture),//4
                                           Math.Round(YTD_val5).ToString("N0", culture),//5
                                           Math.Round(YTD_val6).ToString("N0", culture),//6
                                           Math.Round(YTD_val7).ToString("N0", culture),//7
                                           Math.Round(YTD_val8).ToString("N0", culture),//8
                                           Math.Round(YTD_val9).ToString("N0", culture),//9
                                           Math.Round(YTD_val10).ToString("N0", culture),//10
                                           Math.Round(YTD_val11).ToString("N0", culture),//11
                                           Math.Round(YTD_val12).ToString("N0", culture),//12
                                           "YTDPLAN CurrentYear"
                                           );
                    }
                    catch (Exception ex) { }

                }
                #endregion
                #region MTDSALE
                /// Step:2 MTD SALE 2015
                ///---------------------------------------------------------------------------------------------------------------------------
                ///MTD SALE 2015 
                ///MTD SALE 2015 = Actual sale for that month in 2015 ( so if we have logged in June’15,
                ///then Jun-Dec fields would show 0 value; 
                ///only after end of month will data show for that month because we would get monthly dumps only)
                ///
                temp = null;
                temp = getMonthlyvalues(MTDYear, "MTD Sale", 1);

                for (int i = 0; i < temp.Rows.Count; i++)
                {
                    long loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[0].ToString());
                    long loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[1].ToString());
                    long loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[2].ToString());
                    long loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[3].ToString());
                    long loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[4].ToString());
                    long loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[5].ToString());
                    long loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[6].ToString());
                    long loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[7].ToString());
                    long loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[8].ToString());
                    long loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[9].ToString());
                    long loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[10].ToString());
                    long loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[11].ToString());
                    dtYtdSales.Rows.Add("MTD SALE " + MTDYear,
                                       loc1.ToString("N0", culture),
                                       loc2.ToString("N0", culture),
                                       loc3.ToString("N0", culture),
                                       loc4.ToString("N0", culture),
                                       loc5.ToString("N0", culture),
                                       loc6.ToString("N0", culture),
                                       loc7.ToString("N0", culture),
                                       loc8.ToString("N0", culture),
                                       loc9.ToString("N0", culture),
                                       loc10.ToString("N0", culture),
                                       loc11.ToString("N0", culture),
                                       loc12.ToString("N0", culture),

                                        "MTDSALE CurrentYear");

                }
                #endregion
                #region YTDSALE_YTDYEAR
                /// STEP: 4 YTD SALE 2015
                /// 
                ///-----------------------------------------------------------------------------------------------------------------------------------
                ///YTD SALE 2015 
                ///YTD SALE 2015 = Cumulative total of MTD sales, as in d); 
                ///however, if logged in June’15, then Jun-Dec fields would show same data as NA not applicable)
                ///
                temp = null;

                temp = getMonthlyvalues(YTDYear, "YTD Sale", 1);

                for (int i = 0; i < temp.Rows.Count; i++)
                {

                    long loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[0].ToString());
                    long loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[1].ToString());
                    long loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[2].ToString());
                    long loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[3].ToString());
                    long loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[4].ToString());
                    long loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[5].ToString());
                    long loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[6].ToString());
                    long loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[7].ToString());
                    long loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[8].ToString());
                    long loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[9].ToString());
                    long loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[10].ToString());
                    long loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[11].ToString());

                    dtYtdSales.Rows.Add("YTD SALE " + YTDYear,
                                       currentmonth < 1 ? "0" : Convert.ToInt64(loc1).ToString("N0", culture),
                                       currentmonth < 2 ? "0" : Convert.ToInt64(loc2).ToString("N0", culture),
                                       currentmonth < 3 ? "0" : Convert.ToInt64(loc3).ToString("N0", culture),
                                       currentmonth < 4 ? "0" : Convert.ToInt64(loc4).ToString("N0", culture),
                                       currentmonth < 5 ? "0" : Convert.ToInt64(loc5).ToString("N0", culture),
                                       currentmonth < 6 ? "0" : Convert.ToInt64(loc6).ToString("N0", culture),
                                       currentmonth < 7 ? "0" : Convert.ToInt64(loc7).ToString("N0", culture),
                                       currentmonth < 8 ? "0" : Convert.ToInt64(loc8).ToString("N0", culture),
                                       currentmonth < 9 ? "0" : Convert.ToInt64(loc9).ToString("N0", culture),
                                       currentmonth < 10 ? "0" : Convert.ToInt64(loc10).ToString("N0", culture),
                                       currentmonth < 11 ? "0" : Convert.ToInt64(loc11).ToString("N0", culture),
                                       currentmonth < 12 ? "0" : Convert.ToInt64(loc12).ToString("N0", culture),
                                       "YTDSALE CurrentYear");
                }
                #endregion

              
             
                decimal temp1 = 0, temp2 = 0, temp3 = 0, temp4 = 0, temp5 = 0, temp6 = 0, temp7 = 0, temp8 = 0, temp9 = 0, temp10 = 0, temp11 = 0, temp12 = 0;
                decimal ys1 = 0, ys2 = 0, ys3 = 0, ys4 = 0, ys5 = 0, ys6 = 0, ys7 = 0, ys8 = 0, ys9 = 0, ys10 = 0, ys11 = 0, ys12 = 0;
                decimal ys_a1 = 0, ys_a2 = 0, ys_a3 = 0, ys_a4 = 0, ys_a5 = 0, ys_a6 = 0, ys_a7 = 0, ys_a8 = 0, ys_a9 = 0, ys_a10 = 0, ys_a11 = 0, ys_a12 = 0;
                for (int i = 0; i < dtYtdSales.Rows.Count; i++)
                {
                    string flag = dtYtdSales.Rows[i].ItemArray[13].ToString();
                    //execute one time
                    if (flag == "YTDSALE ActualYear")
                    {
                        ys_a1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                        ys_a2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                        ys_a3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                        ys_a4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                        ys_a5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                        ys_a6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                        ys_a7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                        ys_a8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                        ys_a9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                        ys_a10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                        ys_a11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                        ys_a12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                    }
                    if (flag == "YTDPLAN CurrentYear")
                    {
                        temp1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                        temp2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                        temp3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                        temp4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                        temp5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                        temp6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                        temp7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                        temp8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                        temp9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                        temp10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                        temp11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                        temp12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                    }
                    if (flag == "YTDSALE CurrentYear")
                    {
                        ys1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                        ys2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                        ys3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                        ys4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                        ys5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                        ys6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                        ys7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                        ys8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                        ys9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                        ys10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                        ys11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                        ys12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                    }

                }

                ///
          

                decimal result1 = 0, result2 = 0, result3 = 0, result4 = 0, result5 = 0, result6 = 0, result7 = 0, result8 = 0, result9 = 0, result10 = 0, result11 = 0, result12 = 0;
               
                ///changed Growth% = ((YTD SALE 2015)-(YTD SALE 2014))*100/YTD SALE 2014
                result1 = 0; result2 = 0; result3 = 0; result4 = 0; result5 = 0; result6 = 0; result7 = 0; result8 = 0; result9 = 0; result10 = 0; result11 = 0; result12 = 0;
                result1 = ys_a1 != 0 ? (((decimal)ys1 - (decimal)ys_a1) * 100) / (decimal)ys_a1 : 0;
                result2 = ys_a1 != 0 ? (((decimal)ys2 - (decimal)ys_a2) * 100) / (decimal)ys_a2 : 0;
                result3 = ys_a1 != 0 ? (((decimal)ys3 - (decimal)ys_a3) * 100) / (decimal)ys_a3 : 0;
                result4 = ys_a1 != 0 ? (((decimal)ys4 - (decimal)ys_a4) * 100) / (decimal)ys_a4 : 0;
                result5 = ys_a1 != 0 ? (((decimal)ys5 - (decimal)ys_a5) * 100) / (decimal)ys_a5 : 0;
                result6 = ys_a1 != 0 ? (((decimal)ys6 - (decimal)ys_a6) * 100) / (decimal)ys_a6 : 0;
                result7 = ys_a1 != 0 ? (((decimal)ys7 - (decimal)ys_a7) * 100) / (decimal)ys_a7 : 0;
                result8 = ys_a1 != 0 ? (((decimal)ys8 - (decimal)ys_a8) * 100) / (decimal)ys_a8 : 0;
                result9 = ys_a1 != 0 ? (((decimal)ys9 - (decimal)ys_a9) * 100) / (decimal)ys_a9 : 0;
                result10 = ys_a1 != 0 ? (((decimal)ys10 - (decimal)ys_a10) * 100) / (decimal)ys_a10 : 0;
                result11 = ys_a1 != 0 ? (((decimal)ys11 - (decimal)ys_a11) * 100) / (decimal)ys_a11 : 0;
                result12 = ys_a1 != 0 ? (((decimal)ys12 - (decimal)ys_a12) * 100) / (decimal)ys_a12 : 0;
                dtYtdSales.Rows.Add("GROWTH%",
                                     Math.Round(result1),
                                     Math.Round(result2),
                                     Math.Round(result3),
                                     Math.Round(result4),
                                     Math.Round(result5),
                                     Math.Round(result6),
                                     Math.Round(result7),
                                     Math.Round(result8),
                                     Math.Round(result9),
                                     Math.Round(result10),
                                     Math.Round(result11),
                                     Math.Round(result12),
                                     "Growth"
                                    );
                ///PRO-RATE ACH% = ((YTD SALE 2015)/(YTD PLAN 2015))x100
                result1 = temp1 != 0 ? ((decimal)ys1 / (decimal)temp1) * 100 : 0;
                result2 = temp2 != 0 ? (((decimal)ys2 / (decimal)temp2) * 100) : 0;
                result3 = temp3 != 0 ? (((decimal)ys3 / (decimal)temp3) * 100) : 0;
                result4 = temp4 != 0 ? (((decimal)ys4 / (decimal)temp4) * 100) : 0;
                result5 = temp5 != 0 ? (((decimal)ys5 / (decimal)temp5) * 100) : 0;
                result6 = temp6 != 0 ? (((decimal)ys6 / (decimal)temp6) * 100) : 0;
                result7 = temp7 != 0 ? (((decimal)ys7 / (decimal)temp7) * 100) : 0;
                result8 = temp8 != 0 ? (((decimal)ys8 / (decimal)temp8) * 100) : 0;
                result9 = temp9 != 0 ? (((decimal)ys9 / (decimal)temp9) * 100) : 0;
                result10 = temp10 != 0 ? (((decimal)ys10 / (decimal)temp10) * 100) : 0;
                result11 = temp11 != 0 ? (((decimal)ys11 / (decimal)temp11) * 100) : 0;
                result12 = temp12 != 0 ? (((decimal)ys12 / (decimal)temp12) * 100) : 0;
                dtYtdSales.Rows.Add("PRO-RATA ACH%",
                                     Math.Round(result1),
                                     Math.Round(result2),
                                     Math.Round(result3),
                                     Math.Round(result4),
                                     Math.Round(result5),
                                     Math.Round(result6),
                                     Math.Round(result7),
                                     Math.Round(result8),
                                     Math.Round(result9),
                                     Math.Round(result10),
                                     Math.Round(result11),
                                     Math.Round(result12),
                                     "ProRate"
                                    );

                return dtYtdSales;
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
                return null;
            }
        }

        #endregion


        /// <summary>
        /// retrives values for filling gridview from database based on the user input from dropdown list on Quantity radio button checked
        /// </summary>
        /// <param name="Year"> year specifies whether it is actual year or current year</param>
        /// <param name="flag">specifies whether it is MTD SALE or MTD PLAN, Or YTD sale or YTD plan</param>
        /// <param name="valuein"> specifies whether it is units or lakshs</param>
        /// <returns></returns>

        #region load Grid By Quantites
        protected DataTable getMonthlyquantites(int Year, string flag, int valuein = 1000)
        {
            try
            {
                DataTable dtmonthval = new DataTable();
                string roleId = Session["RoleId"].ToString();
                string userId = Session["UserId"].ToString();
                string branchcode = Convert.ToString(Session["SelectedBranchList"]);
                string SalesengList = Convert.ToString(Session["SelectedSalesEngineers"]);
                string CustomerNamelist = Convert.ToString(Session["SelectedCustomerNames"]);
                string CustomerNumlist = Convert.ToString(Session["SelectedCustomerNumbers"]);
                string ProductFamily = Convert.ToString(Session["SelectedProductFamily"]);
                string ProductGroup = Convert.ToString(Session["SelectedProductGroup"]);
                string ApplicationListVal = Convert.ToString(Session["SelectedApplications"]);
                if (roleId == "TM")
                {
                    objRSum.BranchCode = branchcode == "ALL" ? userId : branchcode;
                }
                else if (roleId == "BM" || roleId == "SE")
                {
                    objRSum.BranchCode = branchcode == "ALL" || branchcode == "" ? Session["BranchCode"].ToString() : branchcode;
                }
                else { objRSum.BranchCode = branchcode == "ALL" ? null : branchcode; }
                if (roleId == "SE")
                {
                    objRSum.salesengineer_id = SalesengList == "ALL" || SalesengList == "" ? Session["UserId"].ToString() : SalesengList;
                }
                else { objRSum.salesengineer_id = SalesengList == "ALL" ? null : SalesengList; }
                objRSum.customer_type = ddlcustomertype.SelectedItem.Text == "ALL" ? null : ddlcustomertype.SelectedItem.Value;
                objRSum.customer_number = CustomerNumlist == "ALL" ? null : CustomerNumlist;
                objRSum.item_family_name = ProductFamily == "ALL" || ProductFamily == "" ? null : ProductFamily;
                objRSum.item_sub_family_name = null;
                objRSum.Year = Year;
                objRSum.flag = flag;
                objRSum.product_group = ProductGroup == "ALL" || ProductGroup == "" ? null : ProductGroup;
                objRSum.item_code = ApplicationListVal == "ALL" || ApplicationListVal == "" ? null : ApplicationListVal;
                objRSum.valuein = valuein;
                objRSum.cter = cter;
                dtmonthval = objRSum.getMonthlyQty(objRSum);

                return dtmonthval;
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
                return null;
            }

        }

        protected DataTable loadGridQty()
        {
            try
            {
                var culture = new CultureInfo("en-us", true)
                {
                    NumberFormat =
                    {
                        NumberGroupSizes = new int[] { 2, 2 }
                    }
                };
                DataTable dtYtdSales = new DataTable();
                dtYtdSales.Columns.Add("title", typeof(string));
                dtYtdSales.Columns.Add("jan", typeof(string));
                dtYtdSales.Columns.Add("feb", typeof(string));
                dtYtdSales.Columns.Add("mar", typeof(string));
                dtYtdSales.Columns.Add("apr", typeof(string));
                dtYtdSales.Columns.Add("may", typeof(string));
                dtYtdSales.Columns.Add("jun", typeof(string));
                dtYtdSales.Columns.Add("jul", typeof(string));
                dtYtdSales.Columns.Add("aug", typeof(string));
                dtYtdSales.Columns.Add("sep", typeof(string));
                dtYtdSales.Columns.Add("oct", typeof(string));
                dtYtdSales.Columns.Add("nov", typeof(string));
                dtYtdSales.Columns.Add("dec", typeof(string));
                dtYtdSales.Columns.Add("flag", typeof(string));
                DataTable temp = new DataTable();
                int ActualYear = objConfig.getActualYear() - 1;
                int P_ActulaYear= objConfig.getActualYear() - 2;
                int YTDYear = ActualYear + 1;
                int MTDYear = ActualYear + 1;
                int currentmonth = System.DateTime.Now.Month;
                int Remain_months = 12 - currentmonth;

                /// Adding Header to Table
                ///  
                dtYtdSales.Rows.Add("", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "Heading");
                #region YTDSALEQTY_PACT
                /// STEP: 5 YTD SALE 2014
                temp = null;
                temp = getMonthlyquantites(ActualYear - 1, "YTD Sale");

                for (int i = 0; i < temp.Rows.Count; i++)
                {
                    int loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString());
                    int loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString());
                    int loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString());
                    int loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString());
                    int loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[4].ToString());
                    int loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[5].ToString());
                    int loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[6].ToString());
                    int loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[7].ToString());
                    int loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[8].ToString());
                    int loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[9].ToString());
                    int loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[10].ToString());
                    int loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[11].ToString());
                    //pending to add all values
                    dtYtdSales.Rows.Add("YTD SALE " + (ActualYear - 1),
                                       loc1.ToString("N0", culture),
                                       loc2.ToString("N0", culture),
                                       loc3.ToString("N0", culture),
                                       loc4.ToString("N0", culture),
                                       loc5.ToString("N0", culture),
                                       loc6.ToString("N0", culture),
                                       loc7.ToString("N0", culture),
                                       loc8.ToString("N0", culture),
                                       loc9.ToString("N0", culture),
                                       loc10.ToString("N0", culture),
                                       loc11.ToString("N0", culture),
                                       loc12.ToString("N0", culture),
                                       "YTDSALE ActualYearP_ACT"
                                       );

                }

                #endregion
                #region YTDsaleqtyACT
                /// --------------------------------------------------------------------------------------------------------------------
                /// STEP: 5 YTD SALE 2014
                temp = null;
                temp = getMonthlyquantites(ActualYear, "YTD Sale");

                for (int i = 0; i < temp.Rows.Count; i++)
                {
                    int loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString());
                    int loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString());
                    int loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString());
                    int loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString());
                    int loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[4].ToString());
                    int loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[5].ToString());
                    int loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[6].ToString());
                    int loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[7].ToString());
                    int loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[8].ToString());
                    int loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[9].ToString());
                    int loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[10].ToString());
                    int loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[11].ToString());
                    //pending to add all values
                    dtYtdSales.Rows.Add("YTD SALE " + ActualYear,
                                       loc1.ToString("N0", culture),
                                       loc2.ToString("N0", culture),
                                       loc3.ToString("N0", culture),
                                       loc4.ToString("N0", culture),
                                       loc5.ToString("N0", culture),
                                       loc6.ToString("N0", culture),
                                       loc7.ToString("N0", culture),
                                       loc8.ToString("N0", culture),
                                       loc9.ToString("N0", culture),
                                       loc10.ToString("N0", culture),
                                       loc11.ToString("N0", culture),
                                       loc12.ToString("N0", culture),
                                       "YTDSALE ActualYear"
                                       );

                }


                #endregion
                #region MTDPLANQTY
                /// 
                /// Step:1 Mtd PLAN 2015
                /// 
                /// --------------------------------------------------------------------------------------------------------------------
                /// MTD PLAN 2015 

                temp = getMonthlyquantites(MTDYear, "MTD Plan");
                decimal YTDBudget = 0; ;
                ///
                /// MTD PLAN 2015 formula = ((BUDGET 2015)-(YTD SALE 2015))/(No. of months remaining) 
                /// Work in progress
                ///               

                DataTable dtSale = new DataTable();
                decimal val1 = 0, val2 = 0, val3 = 0, val4 = 0, val5 = 0, val6 = 0, val7 = 0, val8 = 0, val9 = 0, val10 = 0, val11 = 0, val12 = 0;
                dtSale = getMonthlyquantites(YTDYear, "YTD Sale");
                for (int i = 0; i < temp.Rows.Count; i++)
                {
                    YTDBudget = temp.Rows[i].ItemArray[0].ToString() != "" ? Convert.ToDecimal(temp.Rows[i].ItemArray[0].ToString()) : 0;
                    val1 = YTDBudget / 12; // jan
                    for (int j = 0; j < dtSale.Rows.Count; j++)
                    {
                        if (i == j)
                        {
                            val2 = currentmonth > 1 ? (YTDBudget - (dtSale.Rows[j].ItemArray[0].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[0].ToString()) : 0)) / 11 : val1; //feb
                            val3 = currentmonth > 2 ? (YTDBudget - (dtSale.Rows[j].ItemArray[1].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[1].ToString()) : 0)) / 10 : val2; // mar
                            val4 = currentmonth > 3 ? (YTDBudget - (dtSale.Rows[j].ItemArray[2].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[2].ToString()) : 0)) / 9 : val3; // apr
                            val5 = currentmonth > 4 ? (YTDBudget - (dtSale.Rows[j].ItemArray[3].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[3].ToString()) : 0)) / 8 : val4; //may

                            val6 = currentmonth > 5 ? (YTDBudget - (dtSale.Rows[j].ItemArray[4].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[4].ToString()) : 0)) / 7 : val5;
                            val7 = currentmonth > 6 ? (YTDBudget - (dtSale.Rows[j].ItemArray[5].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[5].ToString()) : 0)) / 6 : val6;
                            val8 = currentmonth > 7 ? (YTDBudget - (dtSale.Rows[j].ItemArray[6].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[6].ToString()) : 0)) / 5 : val7;
                            val9 = currentmonth > 8 ? (YTDBudget - (dtSale.Rows[j].ItemArray[7].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[7].ToString()) : 0)) / 4 : val8;
                            val10 = currentmonth > 9 ? (YTDBudget - (dtSale.Rows[j].ItemArray[8].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[8].ToString()) : 0)) / 3 : val9;
                            val11 = currentmonth > 10 ? (YTDBudget - (dtSale.Rows[j].ItemArray[9].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[9].ToString()) : 0)) / 2 : val10;
                            val12 = currentmonth > 11 ? (YTDBudget - (dtSale.Rows[j].ItemArray[10].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[10].ToString()) : 0)) / 1 : val11;

                            try
                            {
                                dtYtdSales.Rows.Add("MTD PLAN " + MTDYear,
                                                   Math.Round(val1).ToString("N0", culture), //1
                                                   Math.Round(val2).ToString("N0", culture), //2
                                                   Math.Round(val3).ToString("N0", culture), //3
                                                   Math.Round(val4).ToString("N0", culture),//4
                                                   Math.Round(val5).ToString("N0", culture),//5
                                                   Math.Round(val6).ToString("N0", culture),//6
                                                   Math.Round(val7).ToString("N0", culture),//7
                                                   Math.Round(val8).ToString("N0", culture),//8
                                                   Math.Round(val9).ToString("N0", culture),//9
                                                   Math.Round(val10).ToString("N0", culture),//10
                                                   Math.Round(val11).ToString("N0", culture),//11
                                                   Math.Round(val12).ToString("N0", culture),//12
                                                   "MTDPLAN CurrentYear"
                                                   );
                            }
                            catch (Exception ex) { }
                        }
                    }
                }

                #endregion
                #region YTDPLANQTY
                /// STEP: 3 YTD PLAN 2015
                /// --------------------------------------------------------------------------------------------------------------------
                /// YTD PLAN 2015
                /// 
                ///If  Cell_month != Actual_month THEN   YTD_Plan(Cell_month) = YTD_Plan(Cell_month -1) + MTD_Plan(Cell_Month) 
                ///If  Cell_month = Actual_month THEN   YTD_Plan(Cell_month) = YTD_actuals(Cell_month -1) + MTD_Plan(Cell_Month) 
                ///
                ///
                temp = null;


                decimal YTD_val1 = 0, YTD_val2 = 0, YTD_val3 = 0, YTD_val4 = 0, YTD_val5 = 0, YTD_val6 = 0, YTD_val7 = 0, YTD_val8 = 0, YTD_val9 = 0, YTD_val10 = 0, YTD_val11 = 0, YTD_val12 = 0;

                for (int j = 0; j < dtSale.Rows.Count; j++)
                {
                    YTD_val1 = YTDBudget / 12; // jan
                    YTD_val2 = currentmonth != 2 ? YTD_val1 + val2 : (val2 + (dtSale.Rows[j].ItemArray[0].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[0].ToString()) : 0)); //feb
                    YTD_val3 = currentmonth != 3 ? YTD_val2 + val3 : (val3 + (dtSale.Rows[j].ItemArray[1].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[1].ToString()) : 0));  // mar
                    YTD_val4 = currentmonth != 4 ? YTD_val3 + val4 : (val4 + (dtSale.Rows[j].ItemArray[2].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[2].ToString()) : 0)); // apr
                    YTD_val5 = currentmonth != 5 ? YTD_val4 + val5 : (val5 + (dtSale.Rows[j].ItemArray[3].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[3].ToString()) : 0)); //may
                    YTD_val6 = currentmonth != 6 ? YTD_val5 + val6 : (val6 + (dtSale.Rows[j].ItemArray[4].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[4].ToString()) : 0));
                    YTD_val7 = currentmonth != 7 ? YTD_val6 + val7 : (val7 + (dtSale.Rows[j].ItemArray[5].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[5].ToString()) : 0));
                    YTD_val8 = currentmonth != 8 ? YTD_val7 + val8 : (val8 + (dtSale.Rows[j].ItemArray[6].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[6].ToString()) : 0));
                    YTD_val9 = currentmonth != 9 ? YTD_val8 + val9 : (val9 + (dtSale.Rows[j].ItemArray[7].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[7].ToString()) : 0));
                    YTD_val10 = currentmonth != 10 ? YTD_val9 + val10 : (val10 + (dtSale.Rows[j].ItemArray[8].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[8].ToString()) : 0));
                    YTD_val11 = currentmonth != 11 ? YTD_val10 + val11 : (val11 + (dtSale.Rows[j].ItemArray[9].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[9].ToString()) : 0));
                    YTD_val12 = currentmonth != 12 ? YTD_val11 + val12 : (val12 + (dtSale.Rows[j].ItemArray[10].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[10].ToString()) : 0));

                    try
                    {
                        dtYtdSales.Rows.Add("YTD PLAN " + YTDYear,
                                           Math.Round(YTD_val1).ToString("N0", culture), //1
                                           Math.Round(YTD_val2).ToString("N0", culture), //2
                                           Math.Round(YTD_val3).ToString("N0", culture), //3
                                           Math.Round(YTD_val4).ToString("N0", culture),//4
                                           Math.Round(YTD_val5).ToString("N0", culture),//5
                                           Math.Round(YTD_val6).ToString("N0", culture),//6
                                           Math.Round(YTD_val7).ToString("N0", culture),//7
                                           Math.Round(YTD_val8).ToString("N0", culture),//8
                                           Math.Round(YTD_val9).ToString("N0", culture),//9
                                           Math.Round(YTD_val10).ToString("N0", culture),//10
                                           Math.Round(YTD_val11).ToString("N0", culture),//11
                                           Math.Round(YTD_val12).ToString("N0", culture),//12
                                           "YTDPLAN CurrentYear"
                                           );
                    }
                    catch (Exception ex) { }

                }


                #endregion
                #region MTDSALEQTY
                /// Step:2 MTD SALE 2015
                ///---------------------------------------------------------------------------------------------------------------------------
                ///MTD SALE 2015 
                ///MTD SALE 2015 = Actual sale for that month in 2015 ( so if we have logged in June’15,
                ///then Jun-Dec fields would show 0 value; 
                ///only after end of month will data show for that month because we would get monthly dumps only)
                ///
                temp = null;
                temp = getMonthlyquantites(MTDYear, "MTD Sale");
                for (int i = 0; i < temp.Rows.Count; i++)
                {
                    int loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString());
                    int loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString());
                    int loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString());
                    int loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString());
                    int loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[4].ToString());
                    int loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[5].ToString());
                    int loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[6].ToString());
                    int loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[7].ToString());
                    int loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[8].ToString());
                    int loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[9].ToString());
                    int loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[10].ToString());
                    int loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[11].ToString());
                    dtYtdSales.Rows.Add("MTD SALE " + MTDYear,
                                       loc1.ToString("N0", culture),
                                       loc2.ToString("N0", culture),
                                       loc3.ToString("N0", culture),
                                       loc4.ToString("N0", culture),
                                       loc5.ToString("N0", culture),
                                       loc6.ToString("N0", culture),
                                       loc7.ToString("N0", culture),
                                       loc8.ToString("N0", culture),
                                       loc9.ToString("N0", culture),
                                       loc10.ToString("N0", culture),
                                       loc11.ToString("N0", culture),
                                       loc12.ToString("N0", culture),

                                        "MTDSALE CurrentYear");

                }
                #endregion

              
                #region YTDSALEQTY
                /// STEP: 4 YTD SALE 2015
                /// 
                ///-----------------------------------------------------------------------------------------------------------------------------------
                ///YTD SALE 2015 
                ///YTD SALE 2015 = Cumulative total of MTD sales, as in d); 
                ///however, if logged in June’15, then Jun-Dec fields would show same data as NA not applicable)
                ///
                temp = null;

                temp = getMonthlyquantites(YTDYear, "YTD Sale");
                for (int i = 0; i < temp.Rows.Count; i++)
                {

                    int loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString());
                    int loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString());
                    int loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString());
                    int loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString());
                    int loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[4].ToString());
                    int loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[5].ToString());
                    int loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[6].ToString());
                    int loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[7].ToString());
                    int loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[8].ToString());
                    int loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[9].ToString());
                    int loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[10].ToString());
                    int loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[11].ToString());

                    dtYtdSales.Rows.Add("YTD SALE " + YTDYear,
                                       currentmonth < 1 ? "0" : Convert.ToInt32(loc1).ToString("N0", culture),
                                       currentmonth < 2 ? "0" : Convert.ToInt32(loc2).ToString("N0", culture),
                                       currentmonth < 3 ? "0" : Convert.ToInt32(loc3).ToString("N0", culture),
                                       currentmonth < 4 ? "0" : Convert.ToInt32(loc4).ToString("N0", culture),
                                       currentmonth < 5 ? "0" : Convert.ToInt32(loc5).ToString("N0", culture),
                                       currentmonth < 6 ? "0" : Convert.ToInt32(loc6).ToString("N0", culture),
                                       currentmonth < 7 ? "0" : Convert.ToInt32(loc7).ToString("N0", culture),
                                       currentmonth < 8 ? "0" : Convert.ToInt32(loc8).ToString("N0", culture),
                                       currentmonth < 9 ? "0" : Convert.ToInt32(loc9).ToString("N0", culture),
                                       currentmonth < 10 ? "0" : Convert.ToInt32(loc10).ToString("N0", culture),
                                       currentmonth < 11 ? "0" : Convert.ToInt32(loc11).ToString("N0", culture),
                                       currentmonth < 12 ? "0" : Convert.ToInt32(loc12).ToString("N0", culture),
                                       "YTDSALE CurrentYear");
                }
                #endregion

              

             
                decimal temp1 = 0, temp2 = 0, temp3 = 0, temp4 = 0, temp5 = 0, temp6 = 0, temp7 = 0, temp8 = 0, temp9 = 0, temp10 = 0, temp11 = 0, temp12 = 0;
                decimal ys1 = 0, ys2 = 0, ys3 = 0, ys4 = 0, ys5 = 0, ys6 = 0, ys7 = 0, ys8 = 0, ys9 = 0, ys10 = 0, ys11 = 0, ys12 = 0;
                decimal ys_a1 = 0, ys_a2 = 0, ys_a3 = 0, ys_a4 = 0, ys_a5 = 0, ys_a6 = 0, ys_a7 = 0, ys_a8 = 0, ys_a9 = 0, ys_a10 = 0, ys_a11 = 0, ys_a12 = 0;
                for (int i = 0; i < dtYtdSales.Rows.Count; i++)
                {
                    string flag = dtYtdSales.Rows[i].ItemArray[13].ToString();
                    //execute one time
                    if (flag == "YTDSALE ActualYear")
                    {
                        ys_a1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                        ys_a2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                        ys_a3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                        ys_a4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                        ys_a5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                        ys_a6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                        ys_a7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                        ys_a8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                        ys_a9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                        ys_a10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                        ys_a11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                        ys_a12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                    }
                    if (flag == "YTDPLAN CurrentYear")
                    {
                        temp1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                        temp2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                        temp3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                        temp4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                        temp5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                        temp6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                        temp7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                        temp8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                        temp9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                        temp10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                        temp11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                        temp12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                    }
                    if (flag == "YTDSALE CurrentYear")
                    {
                        ys1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                        ys2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                        ys3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                        ys4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                        ys5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                        ys6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                        ys7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                        ys8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                        ys9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                        ys10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                        ys11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                        ys12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                    }

                }

                ///
                ///PRO-RATE ACH% = ((YTD SALE 2015)/(YTD PLAN 2015))x100
                ///GROWTH% = ((YTD SALES 2015)/(YTD SALE 2014))x100             

                decimal result1 = 0, result2 = 0, result3 = 0, result4 = 0, result5 = 0, result6 = 0, result7 = 0, result8 = 0, result9 = 0, result10 = 0, result11 = 0, result12 = 0;
                ///changed Growth% = ((YTD SALE 2015)-(YTD SALE 2014))*100/YTD PLAN 2014
                result1 = 0; result2 = 0; result3 = 0; result4 = 0; result5 = 0; result6 = 0; result7 = 0; result8 = 0; result9 = 0; result10 = 0; result11 = 0; result12 = 0;
                result1 = ys_a1 != 0 ? (((decimal)ys1 - (decimal)ys_a1) * 100) / (decimal)ys_a1 : 0;
                result2 = ys_a1 != 0 ? (((decimal)ys2 - (decimal)ys_a2) * 100) / (decimal)ys_a2 : 0;
                result3 = ys_a1 != 0 ? (((decimal)ys3 - (decimal)ys_a3) * 100) / (decimal)ys_a3 : 0;
                result4 = ys_a1 != 0 ? (((decimal)ys4 - (decimal)ys_a4) * 100) / (decimal)ys_a4 : 0;
                result5 = ys_a1 != 0 ? (((decimal)ys5 - (decimal)ys_a5) * 100) / (decimal)ys_a5 : 0;
                result6 = ys_a1 != 0 ? (((decimal)ys6 - (decimal)ys_a6) * 100) / (decimal)ys_a6 : 0;
                result7 = ys_a1 != 0 ? (((decimal)ys7 - (decimal)ys_a7) * 100) / (decimal)ys_a7 : 0;
                result8 = ys_a1 != 0 ? (((decimal)ys8 - (decimal)ys_a8) * 100) / (decimal)ys_a8 : 0;
                result9 = ys_a1 != 0 ? (((decimal)ys9 - (decimal)ys_a9) * 100) / (decimal)ys_a9 : 0;
                result10 = ys_a1 != 0 ? (((decimal)ys10 - (decimal)ys_a10) * 100) / (decimal)ys_a10 : 0;
                result11 = ys_a1 != 0 ? (((decimal)ys11 - (decimal)ys_a11) * 100) / (decimal)ys_a11 : 0;
                result12 = ys_a1 != 0 ? (((decimal)ys12 - (decimal)ys_a12) * 100) / (decimal)ys_a12 : 0;
                dtYtdSales.Rows.Add("GROWTH%",
                                     Math.Round(result1),
                                     Math.Round(result2),
                                     Math.Round(result3),
                                     Math.Round(result4),
                                     Math.Round(result5),
                                     Math.Round(result6),
                                     Math.Round(result7),
                                     Math.Round(result8),
                                     Math.Round(result9),
                                     Math.Round(result10),
                                     Math.Round(result11),
                                     Math.Round(result12),
                                     "Growth"
                                    );

                   
                result1 = temp1 != 0 ? ((decimal)ys1 / (decimal)temp1) * 100 : 0;
                result2 = temp2 != 0 ? (((decimal)ys2 / (decimal)temp2) * 100) : 0;
                result3 = temp3 != 0 ? (((decimal)ys3 / (decimal)temp3) * 100) : 0;
                result4 = temp4 != 0 ? (((decimal)ys4 / (decimal)temp4) * 100) : 0;
                result5 = temp5 != 0 ? (((decimal)ys5 / (decimal)temp5) * 100) : 0;
                result6 = temp6 != 0 ? (((decimal)ys6 / (decimal)temp6) * 100) : 0;
                result7 = temp7 != 0 ? (((decimal)ys7 / (decimal)temp7) * 100) : 0;
                result8 = temp8 != 0 ? (((decimal)ys8 / (decimal)temp8) * 100) : 0;
                result9 = temp9 != 0 ? (((decimal)ys9 / (decimal)temp9) * 100) : 0;
                result10 = temp10 != 0 ? (((decimal)ys10 / (decimal)temp10) * 100) : 0;
                result11 = temp11 != 0 ? (((decimal)ys11 / (decimal)temp11) * 100) : 0;
                result12 = temp12 != 0 ? (((decimal)ys12 / (decimal)temp12) * 100) : 0;
                dtYtdSales.Rows.Add("PRO-RATA ACH%",
                                     Math.Round(result1),
                                     Math.Round(result2),
                                     Math.Round(result3),
                                     Math.Round(result4),
                                     Math.Round(result5),
                                     Math.Round(result6),
                                     Math.Round(result7),
                                     Math.Round(result8),
                                     Math.Round(result9),
                                     Math.Round(result10),
                                     Math.Round(result11),
                                     Math.Round(result12),
                                     "ProRate"
                                    );
                return dtYtdSales;
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
                return null;
            }
               
        }

        protected DataTable loadGrid_lakhQty()
        {
            try
            {
                var culture = new CultureInfo("en-us", true)
                {
                    NumberFormat =
                    {
                        NumberGroupSizes = new int[] { 2, 2 }
                    }
                };
                DataTable dtYtdSales = new DataTable();
                dtYtdSales.Columns.Add("title", typeof(string));
                dtYtdSales.Columns.Add("jan", typeof(string));
                dtYtdSales.Columns.Add("feb", typeof(string));
                dtYtdSales.Columns.Add("mar", typeof(string));
                dtYtdSales.Columns.Add("apr", typeof(string));
                dtYtdSales.Columns.Add("may", typeof(string));
                dtYtdSales.Columns.Add("jun", typeof(string));
                dtYtdSales.Columns.Add("jul", typeof(string));
                dtYtdSales.Columns.Add("aug", typeof(string));
                dtYtdSales.Columns.Add("sep", typeof(string));
                dtYtdSales.Columns.Add("oct", typeof(string));
                dtYtdSales.Columns.Add("nov", typeof(string));
                dtYtdSales.Columns.Add("dec", typeof(string));
                dtYtdSales.Columns.Add("flag", typeof(string));
                DataTable temp = new DataTable();
                int ActualYear = objConfig.getActualYear() - 1;
                int YTDYear = ActualYear + 1;
                int MTDYear = ActualYear + 1;
                int currentmonth = System.DateTime.Now.Month;
                int Remain_months = 12 - currentmonth;

                /// Adding Header to Table
                ///  
                dtYtdSales.Rows.Add("", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "Heading");
                #region YTDSALEP_ACT
                temp = null;
                temp = getMonthlyquantites(ActualYear - 1, "YTD Sale", 100000);

                for (int i = 0; i < temp.Rows.Count; i++)
                {
                    int loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString());
                    int loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString());
                    int loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString());
                    int loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString());
                    int loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[4].ToString());
                    int loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[5].ToString());
                    int loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[6].ToString());
                    int loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[7].ToString());
                    int loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[8].ToString());
                    int loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[9].ToString());
                    int loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[10].ToString());
                    int loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[11].ToString());
                    //pending to add all values
                    dtYtdSales.Rows.Add("YTD SALE " + (ActualYear - 1),
                                       loc1.ToString("N0", culture),
                                       loc2.ToString("N0", culture),
                                       loc3.ToString("N0", culture),
                                       loc4.ToString("N0", culture),
                                       loc5.ToString("N0", culture),
                                       loc6.ToString("N0", culture),
                                       loc7.ToString("N0", culture),
                                       loc8.ToString("N0", culture),
                                       loc9.ToString("N0", culture),
                                       loc10.ToString("N0", culture),
                                       loc11.ToString("N0", culture),
                                       loc12.ToString("N0", culture),
                                       "YTDSALE ActualYearP_ACT"
                                       );

                }
                #endregion
                #region YTDSALEACT
                /// --------------------------------------------------------------------------------------------------------------------
                /// STEP: 5 YTD SALE 2014
                temp = null;
                temp = getMonthlyquantites(ActualYear, "YTD Sale", 100000);

                for (int i = 0; i < temp.Rows.Count; i++)
                {
                    int loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString());
                    int loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString());
                    int loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString());
                    int loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString());
                    int loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[4].ToString());
                    int loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[5].ToString());
                    int loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[6].ToString());
                    int loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[7].ToString());
                    int loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[8].ToString());
                    int loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[9].ToString());
                    int loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[10].ToString());
                    int loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[11].ToString());
                    //pending to add all values
                    dtYtdSales.Rows.Add("YTD SALE " + ActualYear,
                                       loc1.ToString("N0", culture),
                                       loc2.ToString("N0", culture),
                                       loc3.ToString("N0", culture),
                                       loc4.ToString("N0", culture),
                                       loc5.ToString("N0", culture),
                                       loc6.ToString("N0", culture),
                                       loc7.ToString("N0", culture),
                                       loc8.ToString("N0", culture),
                                       loc9.ToString("N0", culture),
                                       loc10.ToString("N0", culture),
                                       loc11.ToString("N0", culture),
                                       loc12.ToString("N0", culture),
                                       "YTDSALE ActualYear"
                                       );

                }

                #endregion
                #region MTDPLAN
                /// 
                /// Step:1 Mtd PLAN 2015
                /// 
                /// --------------------------------------------------------------------------------------------------------------------
                /// MTD PLAN 2015 

                temp = getMonthlyquantites(MTDYear, "MTD Plan", 100000);
                decimal YTDBudget = 0; ;
                ///
                /// MTD PLAN 2015 formula = ((BUDGET 2015)-(YTD SALE 2015))/(No. of months remaining) 
                /// Work in progress
                ///               

                DataTable dtSale = new DataTable();
                decimal val1 = 0, val2 = 0, val3 = 0, val4 = 0, val5 = 0, val6 = 0, val7 = 0, val8 = 0, val9 = 0, val10 = 0, val11 = 0, val12 = 0;
                dtSale = getMonthlyquantites(YTDYear, "YTD Sale", 100000);
                for (int i = 0; i < temp.Rows.Count; i++)
                {
                    YTDBudget = temp.Rows[i].ItemArray[0].ToString() != "" ? Convert.ToDecimal(temp.Rows[i].ItemArray[0].ToString()) : 0;
                    val1 = YTDBudget / 12; // jan
                    for (int j = 0; j < dtSale.Rows.Count; j++)
                    {
                        if (i == j)
                        {
                            val2 = currentmonth > 1 ? (YTDBudget - (dtSale.Rows[j].ItemArray[0].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[0].ToString()) : 0)) / 11 : val1; //feb
                            val3 = currentmonth > 2 ? (YTDBudget - (dtSale.Rows[j].ItemArray[1].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[1].ToString()) : 0)) / 10 : val2; // mar
                            val4 = currentmonth > 3 ? (YTDBudget - (dtSale.Rows[j].ItemArray[2].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[2].ToString()) : 0)) / 9 : val3; // apr
                            val5 = currentmonth > 4 ? (YTDBudget - (dtSale.Rows[j].ItemArray[3].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[3].ToString()) : 0)) / 8 : val4; //may

                            val6 = currentmonth > 5 ? (YTDBudget - (dtSale.Rows[j].ItemArray[4].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[4].ToString()) : 0)) / 7 : val5;
                            val7 = currentmonth > 6 ? (YTDBudget - (dtSale.Rows[j].ItemArray[5].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[5].ToString()) : 0)) / 6 : val6;
                            val8 = currentmonth > 7 ? (YTDBudget - (dtSale.Rows[j].ItemArray[6].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[6].ToString()) : 0)) / 5 : val7;
                            val9 = currentmonth > 8 ? (YTDBudget - (dtSale.Rows[j].ItemArray[7].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[7].ToString()) : 0)) / 4 : val8;
                            val10 = currentmonth > 9 ? (YTDBudget - (dtSale.Rows[j].ItemArray[8].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[8].ToString()) : 0)) / 3 : val9;
                            val11 = currentmonth > 10 ? (YTDBudget - (dtSale.Rows[j].ItemArray[9].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[9].ToString()) : 0)) / 2 : val10;
                            val12 = currentmonth > 11 ? (YTDBudget - (dtSale.Rows[j].ItemArray[10].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[10].ToString()) : 0)) / 1 : val11;

                            try
                            {
                                dtYtdSales.Rows.Add("MTD PLAN " + MTDYear,
                                                   Math.Round(val1).ToString("N0", culture), //1
                                                   Math.Round(val2).ToString("N0", culture), //2
                                                   Math.Round(val3).ToString("N0", culture), //3
                                                   Math.Round(val4).ToString("N0", culture),//4
                                                   Math.Round(val5).ToString("N0", culture),//5
                                                   Math.Round(val6).ToString("N0", culture),//6
                                                   Math.Round(val7).ToString("N0", culture),//7
                                                   Math.Round(val8).ToString("N0", culture),//8
                                                   Math.Round(val9).ToString("N0", culture),//9
                                                   Math.Round(val10).ToString("N0", culture),//10
                                                   Math.Round(val11).ToString("N0", culture),//11
                                                   Math.Round(val12).ToString("N0", culture),//12
                                                   "MTDPLAN CurrentYear"
                                                   );
                            }
                            catch (Exception ex) { }
                        }
                    }
                }

                #endregion
                #region YTDPLAN
                /// STEP: 3 YTD PLAN 2015
                /// --------------------------------------------------------------------------------------------------------------------
                /// YTD PLAN 2015
                /// 
                ///If  Cell_month != Actual_month THEN   YTD_Plan(Cell_month) = YTD_Plan(Cell_month -1) + MTD_Plan(Cell_Month) 
                ///If  Cell_month = Actual_month THEN   YTD_Plan(Cell_month) = YTD_actuals(Cell_month -1) + MTD_Plan(Cell_Month) 
                ///
                ///
                temp = null;


                decimal YTD_val1 = 0, YTD_val2 = 0, YTD_val3 = 0, YTD_val4 = 0, YTD_val5 = 0, YTD_val6 = 0, YTD_val7 = 0, YTD_val8 = 0, YTD_val9 = 0, YTD_val10 = 0, YTD_val11 = 0, YTD_val12 = 0;

                for (int j = 0; j < dtSale.Rows.Count; j++)
                {
                    YTD_val1 = YTDBudget / 12; // jan
                    YTD_val2 = currentmonth != 2 ? YTD_val1 + val2 : (val2 + (dtSale.Rows[j].ItemArray[0].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[0].ToString()) : 0)); //feb
                    YTD_val3 = currentmonth != 3 ? YTD_val2 + val3 : (val3 + (dtSale.Rows[j].ItemArray[1].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[1].ToString()) : 0));  // mar
                    YTD_val4 = currentmonth != 4 ? YTD_val3 + val4 : (val4 + (dtSale.Rows[j].ItemArray[2].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[2].ToString()) : 0)); // apr
                    YTD_val5 = currentmonth != 5 ? YTD_val4 + val5 : (val5 + (dtSale.Rows[j].ItemArray[3].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[3].ToString()) : 0)); //may
                    YTD_val6 = currentmonth != 6 ? YTD_val5 + val6 : (val6 + (dtSale.Rows[j].ItemArray[4].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[4].ToString()) : 0));
                    YTD_val7 = currentmonth != 7 ? YTD_val6 + val7 : (val7 + (dtSale.Rows[j].ItemArray[5].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[5].ToString()) : 0));
                    YTD_val8 = currentmonth != 8 ? YTD_val7 + val8 : (val8 + (dtSale.Rows[j].ItemArray[6].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[6].ToString()) : 0));
                    YTD_val9 = currentmonth != 9 ? YTD_val8 + val9 : (val9 + (dtSale.Rows[j].ItemArray[7].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[7].ToString()) : 0));
                    YTD_val10 = currentmonth != 10 ? YTD_val9 + val10 : (val10 + (dtSale.Rows[j].ItemArray[8].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[8].ToString()) : 0));
                    YTD_val11 = currentmonth != 11 ? YTD_val10 + val11 : (val11 + (dtSale.Rows[j].ItemArray[9].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[9].ToString()) : 0));
                    YTD_val12 = currentmonth != 12 ? YTD_val11 + val12 : (val12 + (dtSale.Rows[j].ItemArray[10].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[10].ToString()) : 0));

                    try
                    {
                        dtYtdSales.Rows.Add("YTD PLAN " + YTDYear,
                                           Math.Round(YTD_val1).ToString("N0", culture), //1
                                           Math.Round(YTD_val2).ToString("N0", culture), //2
                                           Math.Round(YTD_val3).ToString("N0", culture), //3
                                           Math.Round(YTD_val4).ToString("N0", culture),//4
                                           Math.Round(YTD_val5).ToString("N0", culture),//5
                                           Math.Round(YTD_val6).ToString("N0", culture),//6
                                           Math.Round(YTD_val7).ToString("N0", culture),//7
                                           Math.Round(YTD_val8).ToString("N0", culture),//8
                                           Math.Round(YTD_val9).ToString("N0", culture),//9
                                           Math.Round(YTD_val10).ToString("N0", culture),//10
                                           Math.Round(YTD_val11).ToString("N0", culture),//11
                                           Math.Round(YTD_val12).ToString("N0", culture),//12
                                           "YTDPLAN CurrentYear"
                                           );
                    }
                    catch (Exception ex) { }

                }

                #endregion
                #region MTDSALE
                /// Step:2 MTD SALE 2015
                ///---------------------------------------------------------------------------------------------------------------------------
                ///MTD SALE 2015 
                ///MTD SALE 2015 = Actual sale for that month in 2015 ( so if we have logged in June’15,
                ///then Jun-Dec fields would show 0 value; 
                ///only after end of month will data show for that month because we would get monthly dumps only)
                ///
                temp = null;
                temp = getMonthlyquantites(MTDYear, "MTD Sale", 100000);

                for (int i = 0; i < temp.Rows.Count; i++)
                {
                    int loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString());
                    int loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString());
                    int loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString());
                    int loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString());
                    int loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[4].ToString());
                    int loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[5].ToString());
                    int loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[6].ToString());
                    int loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[7].ToString());
                    int loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[8].ToString());
                    int loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[9].ToString());
                    int loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[10].ToString());
                    int loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[11].ToString());
                    dtYtdSales.Rows.Add("MTD SALE " + MTDYear,
                                       loc1.ToString("N0", culture),
                                       loc2.ToString("N0", culture),
                                       loc3.ToString("N0", culture),
                                       loc4.ToString("N0", culture),
                                       loc5.ToString("N0", culture),
                                       loc6.ToString("N0", culture),
                                       loc7.ToString("N0", culture),
                                       loc8.ToString("N0", culture),
                                       loc9.ToString("N0", culture),
                                       loc10.ToString("N0", culture),
                                       loc11.ToString("N0", culture),
                                       loc12.ToString("N0", culture),

                                        "MTDSALE CurrentYear");

                }
                #endregion
              
                #region YTDSALE
                /// STEP: 4 YTD SALE 2015
                /// 
                ///-----------------------------------------------------------------------------------------------------------------------------------
                ///YTD SALE 2015 
                ///YTD SALE 2015 = Cumulative total of MTD sales, as in d); 
                ///however, if logged in June’15, then Jun-Dec fields would show same data as NA not applicable)
                ///
                temp = null;

                temp = getMonthlyquantites(YTDYear, "YTD Sale", 100000);

                for (int i = 0; i < temp.Rows.Count; i++)
                {
                    int loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString());
                    int loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString());
                    int loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString());
                    int loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString());
                    int loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[4].ToString());
                    int loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[5].ToString());
                    int loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[6].ToString());
                    int loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[7].ToString());
                    int loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[8].ToString());
                    int loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[9].ToString());
                    int loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[10].ToString());
                    int loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[11].ToString());

                    dtYtdSales.Rows.Add("YTD SALE " + YTDYear,
                                       currentmonth < 1 ? "0" : Convert.ToInt32(loc1).ToString("N0", culture),
                                       currentmonth < 2 ? "0" : Convert.ToInt32(loc2).ToString("N0", culture),
                                       currentmonth < 3 ? "0" : Convert.ToInt32(loc3).ToString("N0", culture),
                                       currentmonth < 4 ? "0" : Convert.ToInt32(loc4).ToString("N0", culture),
                                       currentmonth < 5 ? "0" : Convert.ToInt32(loc5).ToString("N0", culture),
                                       currentmonth < 6 ? "0" : Convert.ToInt32(loc6).ToString("N0", culture),
                                       currentmonth < 7 ? "0" : Convert.ToInt32(loc7).ToString("N0", culture),
                                       currentmonth < 8 ? "0" : Convert.ToInt32(loc8).ToString("N0", culture),
                                       currentmonth < 9 ? "0" : Convert.ToInt32(loc9).ToString("N0", culture),
                                       currentmonth < 10 ? "0" : Convert.ToInt32(loc10).ToString("N0", culture),
                                       currentmonth < 11 ? "0" : Convert.ToInt32(loc11).ToString("N0", culture),
                                       currentmonth < 12 ? "0" : Convert.ToInt32(loc12).ToString("N0", culture),
                                       "YTDSALE CurrentYear");
                }
                #endregion

               



                decimal temp1 = 0, temp2 = 0, temp3 = 0, temp4 = 0, temp5 = 0, temp6 = 0, temp7 = 0, temp8 = 0, temp9 = 0, temp10 = 0, temp11 = 0, temp12 = 0;
                decimal ys1 = 0, ys2 = 0, ys3 = 0, ys4 = 0, ys5 = 0, ys6 = 0, ys7 = 0, ys8 = 0, ys9 = 0, ys10 = 0, ys11 = 0, ys12 = 0;
                decimal ys_a1 = 0, ys_a2 = 0, ys_a3 = 0, ys_a4 = 0, ys_a5 = 0, ys_a6 = 0, ys_a7 = 0, ys_a8 = 0, ys_a9 = 0, ys_a10 = 0, ys_a11 = 0, ys_a12 = 0;
                for (int i = 0; i < dtYtdSales.Rows.Count; i++)
                {
                    string flag = dtYtdSales.Rows[i].ItemArray[13].ToString();
                    //execute one time
                    if (flag == "YTDSALE ActualYear")
                    {
                        ys_a1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                        ys_a2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                        ys_a3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                        ys_a4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                        ys_a5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                        ys_a6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                        ys_a7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                        ys_a8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                        ys_a9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                        ys_a10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                        ys_a11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                        ys_a12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                    }
                    if (flag == "YTDPLAN CurrentYear")
                    {
                        temp1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                        temp2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                        temp3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                        temp4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                        temp5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                        temp6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                        temp7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                        temp8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                        temp9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                        temp10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                        temp11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                        temp12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                    }
                    if (flag == "YTDSALE CurrentYear")
                    {
                        ys1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                        ys2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                        ys3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                        ys4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                        ys5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                        ys6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                        ys7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                        ys8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                        ys9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                        ys10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                        ys11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                        ys12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                    }

                }

                ///
                ///PRO-RATE ACH% = ((YTD SALE 2015)/(YTD PLAN 2015))x100

                decimal result1 = 0, result2 = 0, result3 = 0, result4 = 0, result5 = 0, result6 = 0, result7 = 0, result8 = 0, result9 = 0, result10 = 0, result11 = 0, result12 = 0;
             
                ///changed Growth% = ((YTD SALE 2015)-(YTD SALE 2014))*100/YTD SALE 2014
                result1 = 0; result2 = 0; result3 = 0; result4 = 0; result5 = 0; result6 = 0; result7 = 0; result8 = 0; result9 = 0; result10 = 0; result11 = 0; result12 = 0;
                result1 = ys_a1 != 0 ? (((decimal)ys1 - (decimal)ys_a1) * 100) / (decimal)ys_a1 : 0;
                result2 = ys_a1 != 0 ? (((decimal)ys2 - (decimal)ys_a2) * 100) / (decimal)ys_a2 : 0;
                result3 = ys_a1 != 0 ? (((decimal)ys3 - (decimal)ys_a3) * 100) / (decimal)ys_a3 : 0;
                result4 = ys_a1 != 0 ? (((decimal)ys4 - (decimal)ys_a4) * 100) / (decimal)ys_a4 : 0;
                result5 = ys_a1 != 0 ? (((decimal)ys5 - (decimal)ys_a5) * 100) / (decimal)ys_a5 : 0;
                result6 = ys_a1 != 0 ? (((decimal)ys6 - (decimal)ys_a6) * 100) / (decimal)ys_a6 : 0;
                result7 = ys_a1 != 0 ? (((decimal)ys7 - (decimal)ys_a7) * 100) / (decimal)ys_a7 : 0;
                result8 = ys_a1 != 0 ? (((decimal)ys8 - (decimal)ys_a8) * 100) / (decimal)ys_a8 : 0;
                result9 = ys_a1 != 0 ? (((decimal)ys9 - (decimal)ys_a9) * 100) / (decimal)ys_a9 : 0;
                result10 = ys_a1 != 0 ? (((decimal)ys10 - (decimal)ys_a10) * 100) / (decimal)ys_a10 : 0;
                result11 = ys_a1 != 0 ? (((decimal)ys11 - (decimal)ys_a11) * 100) / (decimal)ys_a11 : 0;
                result12 = ys_a1 != 0 ? (((decimal)ys12 - (decimal)ys_a12) * 100) / (decimal)ys_a12 : 0;
                dtYtdSales.Rows.Add("GROWTH%",
                                     Math.Round(result1),
                                     Math.Round(result2),
                                     Math.Round(result3),
                                     Math.Round(result4),
                                     Math.Round(result5),
                                     Math.Round(result6),
                                     Math.Round(result7),
                                     Math.Round(result8),
                                     Math.Round(result9),
                                     Math.Round(result10),
                                     Math.Round(result11),
                                     Math.Round(result12),
                                     "Growth"
                                    );
                result1 = temp1 != 0 ? ((decimal)ys1 / (decimal)temp1) * 100 : 0;
                result2 = temp2 != 0 ? (((decimal)ys2 / (decimal)temp2) * 100) : 0;
                result3 = temp3 != 0 ? (((decimal)ys3 / (decimal)temp3) * 100) : 0;
                result4 = temp4 != 0 ? (((decimal)ys4 / (decimal)temp4) * 100) : 0;
                result5 = temp5 != 0 ? (((decimal)ys5 / (decimal)temp5) * 100) : 0;
                result6 = temp6 != 0 ? (((decimal)ys6 / (decimal)temp6) * 100) : 0;
                result7 = temp7 != 0 ? (((decimal)ys7 / (decimal)temp7) * 100) : 0;
                result8 = temp8 != 0 ? (((decimal)ys8 / (decimal)temp8) * 100) : 0;
                result9 = temp9 != 0 ? (((decimal)ys9 / (decimal)temp9) * 100) : 0;
                result10 = temp10 != 0 ? (((decimal)ys10 / (decimal)temp10) * 100) : 0;
                result11 = temp11 != 0 ? (((decimal)ys11 / (decimal)temp11) * 100) : 0;
                result12 = temp12 != 0 ? (((decimal)ys12 / (decimal)temp12) * 100) : 0;
                dtYtdSales.Rows.Add("PRO-RATA ACH%",
                                     Math.Round(result1),
                                     Math.Round(result2),
                                     Math.Round(result3),
                                     Math.Round(result4),
                                     Math.Round(result5),
                                     Math.Round(result6),
                                     Math.Round(result7),
                                     Math.Round(result8),
                                     Math.Round(result9),
                                     Math.Round(result10),
                                     Math.Round(result11),
                                     Math.Round(result12),
                                     "ProRate"
                                    );


                return dtYtdSales;
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
                return null;
            }
        }

        protected DataTable loadGrid_UnitQty()
        {
            try
            {
                var culture = new CultureInfo("en-us", true)
                {
                    NumberFormat =
                    {
                        NumberGroupSizes = new int[] { 3, 3 }
                    }
                };
                DataTable dtYtdSales = new DataTable();
                dtYtdSales.Columns.Add("title", typeof(string));
                dtYtdSales.Columns.Add("jan", typeof(string));
                dtYtdSales.Columns.Add("feb", typeof(string));
                dtYtdSales.Columns.Add("mar", typeof(string));
                dtYtdSales.Columns.Add("apr", typeof(string));
                dtYtdSales.Columns.Add("may", typeof(string));
                dtYtdSales.Columns.Add("jun", typeof(string));
                dtYtdSales.Columns.Add("jul", typeof(string));
                dtYtdSales.Columns.Add("aug", typeof(string));
                dtYtdSales.Columns.Add("sep", typeof(string));
                dtYtdSales.Columns.Add("oct", typeof(string));
                dtYtdSales.Columns.Add("nov", typeof(string));
                dtYtdSales.Columns.Add("dec", typeof(string));
                dtYtdSales.Columns.Add("flag", typeof(string));
                DataTable temp = new DataTable();
                int ActualYear = objConfig.getActualYear() - 1;
                int YTDYear = ActualYear + 1;
                int MTDYear = ActualYear + 1;
                int currentmonth = System.DateTime.Now.Month;
                int Remain_months = 12 - currentmonth;

                /// Adding Header to Table
                ///  
                dtYtdSales.Rows.Add("", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "Heading");

                  #region YTDSALE_PACT

                   /// STEP: 5 YTD SALE 2014
                temp = null;
                temp = getMonthlyquantites(ActualYear-1, "YTD Sale", 1);

                for (int i = 0; i < temp.Rows.Count; i++)
                {
                    long loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[0].ToString());
                    long loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[1].ToString());
                    long loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[2].ToString());
                    long loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[3].ToString());
                    long loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[4].ToString());
                    long loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[5].ToString());
                    long loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[6].ToString());
                    long loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[7].ToString());
                    long loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[8].ToString());
                    long loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[9].ToString());
                    long loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[10].ToString());
                    long loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[11].ToString());
                    //pending to add all values
                    dtYtdSales.Rows.Add("YTD SALE " + (ActualYear - 1),
                                       loc1.ToString("N0", culture),
                                       loc2.ToString("N0", culture),
                                       loc3.ToString("N0", culture),
                                       loc4.ToString("N0", culture),
                                       loc5.ToString("N0", culture),
                                       loc6.ToString("N0", culture),
                                       loc7.ToString("N0", culture),
                                       loc8.ToString("N0", culture),
                                       loc9.ToString("N0", culture),
                                       loc10.ToString("N0", culture),
                                       loc11.ToString("N0", culture),
                                       loc12.ToString("N0", culture),
                                       "YTDSALE ActualYear_PACT"
                                       );
                }
                #endregion

                       #region YTDSALE_ACT
                /// --------------------------------------------------------------------------------------------------------------------
                /// STEP: 5 YTD SALE 2014
                temp = null;
                temp = getMonthlyquantites(ActualYear, "YTD Sale", 1);

                for (int i = 0; i < temp.Rows.Count; i++)
                {
                    long loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[0].ToString());
                    long loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[1].ToString());
                    long loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[2].ToString());
                    long loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[3].ToString());
                    long loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[4].ToString());
                    long loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[5].ToString());
                    long loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[6].ToString());
                    long loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[7].ToString());
                    long loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[8].ToString());
                    long loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[9].ToString());
                    long loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[10].ToString());
                    long loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[11].ToString());
                    //pending to add all values
                    dtYtdSales.Rows.Add("YTD SALE " + ActualYear,
                                       loc1.ToString("N0", culture),
                                       loc2.ToString("N0", culture),
                                       loc3.ToString("N0", culture),
                                       loc4.ToString("N0", culture),
                                       loc5.ToString("N0", culture),
                                       loc6.ToString("N0", culture),
                                       loc7.ToString("N0", culture),
                                       loc8.ToString("N0", culture),
                                       loc9.ToString("N0", culture),
                                       loc10.ToString("N0", culture),
                                       loc11.ToString("N0", culture),
                                       loc12.ToString("N0", culture),
                                       "YTDSALE ActualYear"
                                       );
                }
                #endregion
                #region MTDPLAN
                /// 
                /// Step:1 Mtd PLAN 2015
                /// 
                /// --------------------------------------------------------------------------------------------------------------------
                /// MTD PLAN 2015 

                temp = getMonthlyquantites(MTDYear, "MTD Plan", 1);
                decimal YTDBudget = 0; ;
                ///
                /// MTD PLAN 2015 formula = ((BUDGET 2015)-(YTD SALE 2015))/(No. of months remaining) 
                /// Work in progress
                ///               

                DataTable dtSale = new DataTable();
                decimal val1 = 0, val2 = 0, val3 = 0, val4 = 0, val5 = 0, val6 = 0, val7 = 0, val8 = 0, val9 = 0, val10 = 0, val11 = 0, val12 = 0;
                dtSale = getMonthlyquantites(YTDYear, "YTD Sale", 1);
                for (int i = 0; i < temp.Rows.Count; i++)
                {
                    YTDBudget = temp.Rows[i].ItemArray[0].ToString() != "" ? Convert.ToDecimal(temp.Rows[i].ItemArray[0].ToString()) : 0;
                    val1 = YTDBudget / 12; // jan
                    for (int j = 0; j < dtSale.Rows.Count; j++)
                    {
                        if (i == j)
                        {
                            val2 = currentmonth > 1 ? (YTDBudget - (dtSale.Rows[j].ItemArray[0].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[0].ToString()) : 0)) / 11 : val1; //feb
                            val3 = currentmonth > 2 ? (YTDBudget - (dtSale.Rows[j].ItemArray[1].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[1].ToString()) : 0)) / 10 : val2; // mar
                            val4 = currentmonth > 3 ? (YTDBudget - (dtSale.Rows[j].ItemArray[2].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[2].ToString()) : 0)) / 9 : val3; // apr
                            val5 = currentmonth > 4 ? (YTDBudget - (dtSale.Rows[j].ItemArray[3].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[3].ToString()) : 0)) / 8 : val4; //may

                            val6 = currentmonth > 5 ? (YTDBudget - (dtSale.Rows[j].ItemArray[4].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[4].ToString()) : 0)) / 7 : val5;
                            val7 = currentmonth > 6 ? (YTDBudget - (dtSale.Rows[j].ItemArray[5].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[5].ToString()) : 0)) / 6 : val6;
                            val8 = currentmonth > 7 ? (YTDBudget - (dtSale.Rows[j].ItemArray[6].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[6].ToString()) : 0)) / 5 : val7;
                            val9 = currentmonth > 8 ? (YTDBudget - (dtSale.Rows[j].ItemArray[7].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[7].ToString()) : 0)) / 4 : val8;
                            val10 = currentmonth > 9 ? (YTDBudget - (dtSale.Rows[j].ItemArray[8].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[8].ToString()) : 0)) / 3 : val9;
                            val11 = currentmonth > 10 ? (YTDBudget - (dtSale.Rows[j].ItemArray[9].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[9].ToString()) : 0)) / 2 : val10;
                            val12 = currentmonth > 11 ? (YTDBudget - (dtSale.Rows[j].ItemArray[10].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[10].ToString()) : 0)) / 1 : val11;

                            try
                            {
                                dtYtdSales.Rows.Add("MTD PLAN " + MTDYear,
                                                   Math.Round(val1).ToString("N0", culture), //1
                                                   Math.Round(val2).ToString("N0", culture), //2
                                                   Math.Round(val3).ToString("N0", culture), //3
                                                   Math.Round(val4).ToString("N0", culture),//4
                                                   Math.Round(val5).ToString("N0", culture),//5
                                                   Math.Round(val6).ToString("N0", culture),//6
                                                   Math.Round(val7).ToString("N0", culture),//7
                                                   Math.Round(val8).ToString("N0", culture),//8
                                                   Math.Round(val9).ToString("N0", culture),//9
                                                   Math.Round(val10).ToString("N0", culture),//10
                                                   Math.Round(val11).ToString("N0", culture),//11
                                                   Math.Round(val12).ToString("N0", culture),//12
                                                   "MTDPLAN CurrentYear"
                                                   );
                            }
                            catch (Exception ex) { }
                        }
                    }
                }

                #endregion
    #region YTDPLAN 
                /// STEP: 3 YTD PLAN 2015
                /// --------------------------------------------------------------------------------------------------------------------
                /// YTD PLAN 2015
                /// 
                ///If  Cell_month != Actual_month THEN   YTD_Plan(Cell_month) = YTD_Plan(Cell_month -1) + MTD_Plan(Cell_Month) 
                ///If  Cell_month = Actual_month THEN   YTD_Plan(Cell_month) = YTD_actuals(Cell_month -1) + MTD_Plan(Cell_Month) 
                ///
                ///
                temp = null;


                decimal YTD_val1 = 0, YTD_val2 = 0, YTD_val3 = 0, YTD_val4 = 0, YTD_val5 = 0, YTD_val6 = 0, YTD_val7 = 0, YTD_val8 = 0, YTD_val9 = 0, YTD_val10 = 0, YTD_val11 = 0, YTD_val12 = 0;

                for (int j = 0; j < dtSale.Rows.Count; j++)
                {
                    YTD_val1 = YTDBudget / 12; // jan
                    YTD_val2 = currentmonth != 2 ? YTD_val1 + val2 : (val2 + (dtSale.Rows[j].ItemArray[0].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[0].ToString()) : 0)); //feb
                    YTD_val3 = currentmonth != 3 ? YTD_val2 + val3 : (val3 + (dtSale.Rows[j].ItemArray[1].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[1].ToString()) : 0));  // mar
                    YTD_val4 = currentmonth != 4 ? YTD_val3 + val4 : (val4 + (dtSale.Rows[j].ItemArray[2].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[2].ToString()) : 0)); // apr
                    YTD_val5 = currentmonth != 5 ? YTD_val4 + val5 : (val5 + (dtSale.Rows[j].ItemArray[3].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[3].ToString()) : 0)); //may
                    YTD_val6 = currentmonth != 6 ? YTD_val5 + val6 : (val6 + (dtSale.Rows[j].ItemArray[4].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[4].ToString()) : 0));
                    YTD_val7 = currentmonth != 7 ? YTD_val6 + val7 : (val7 + (dtSale.Rows[j].ItemArray[5].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[5].ToString()) : 0));
                    YTD_val8 = currentmonth != 8 ? YTD_val7 + val8 : (val8 + (dtSale.Rows[j].ItemArray[6].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[6].ToString()) : 0));
                    YTD_val9 = currentmonth != 9 ? YTD_val8 + val9 : (val9 + (dtSale.Rows[j].ItemArray[7].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[7].ToString()) : 0));
                    YTD_val10 = currentmonth != 10 ? YTD_val9 + val10 : (val10 + (dtSale.Rows[j].ItemArray[8].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[8].ToString()) : 0));
                    YTD_val11 = currentmonth != 11 ? YTD_val10 + val11 : (val11 + (dtSale.Rows[j].ItemArray[9].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[9].ToString()) : 0));
                    YTD_val12 = currentmonth != 12 ? YTD_val11 + val12 : (val12 + (dtSale.Rows[j].ItemArray[10].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[10].ToString()) : 0));

                    try
                    {
                        dtYtdSales.Rows.Add("YTD PLAN " + YTDYear,
                                           Math.Round(YTD_val1).ToString("N0", culture), //1
                                           Math.Round(YTD_val2).ToString("N0", culture), //2
                                           Math.Round(YTD_val3).ToString("N0", culture), //3
                                           Math.Round(YTD_val4).ToString("N0", culture),//4
                                           Math.Round(YTD_val5).ToString("N0", culture),//5
                                           Math.Round(YTD_val6).ToString("N0", culture),//6
                                           Math.Round(YTD_val7).ToString("N0", culture),//7
                                           Math.Round(YTD_val8).ToString("N0", culture),//8
                                           Math.Round(YTD_val9).ToString("N0", culture),//9
                                           Math.Round(YTD_val10).ToString("N0", culture),//10
                                           Math.Round(YTD_val11).ToString("N0", culture),//11
                                           Math.Round(YTD_val12).ToString("N0", culture),//12
                                           "YTDPLAN CurrentYear"
                                           );
                    }
                    catch (Exception ex) { }

                }
                #endregion
                #region MTDSALE
                /// Step:2 MTD SALE 2015
                ///---------------------------------------------------------------------------------------------------------------------------
                ///MTD SALE 2015 
                ///MTD SALE 2015 = Actual sale for that month in 2015 ( so if we have logged in June’15,
                ///then Jun-Dec fields would show 0 value; 
                ///only after end of month will data show for that month because we would get monthly dumps only)
                ///
                temp = null;
                temp = getMonthlyquantites(MTDYear, "MTD Sale", 1);

                for (int i = 0; i < temp.Rows.Count; i++)
                {
                    long loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[0].ToString());
                    long loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[1].ToString());
                    long loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[2].ToString());
                    long loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[3].ToString());
                    long loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[4].ToString());
                    long loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[5].ToString());
                    long loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[6].ToString());
                    long loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[7].ToString());
                    long loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[8].ToString());
                    long loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[9].ToString());
                    long loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[10].ToString());
                    long loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[11].ToString());
                    dtYtdSales.Rows.Add("MTD SALE " + MTDYear,
                                       loc1.ToString("N0", culture),
                                       loc2.ToString("N0", culture),
                                       loc3.ToString("N0", culture),
                                       loc4.ToString("N0", culture),
                                       loc5.ToString("N0", culture),
                                       loc6.ToString("N0", culture),
                                       loc7.ToString("N0", culture),
                                       loc8.ToString("N0", culture),
                                       loc9.ToString("N0", culture),
                                       loc10.ToString("N0", culture),
                                       loc11.ToString("N0", culture),
                                       loc12.ToString("N0", culture),

                                        "MTDSALE CurrentYear");

                }

                #endregion
            
                #region YTDSALE
                /// STEP: 4 YTD SALE 2015
                /// 
                ///-----------------------------------------------------------------------------------------------------------------------------------
                ///YTD SALE 2015 
                ///YTD SALE 2015 = Cumulative total of MTD sales, as in d); 
                ///however, if logged in June’15, then Jun-Dec fields would show same data as NA not applicable)
                ///
                temp = null;

                temp = getMonthlyquantites(YTDYear, "YTD Sale", 1);

                for (int i = 0; i < temp.Rows.Count; i++)
                {

                    long loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[0].ToString());
                    long loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[1].ToString());
                    long loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[2].ToString());
                    long loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[3].ToString());
                    long loc5 = temp.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[4].ToString());
                    long loc6 = temp.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[5].ToString());
                    long loc7 = temp.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[6].ToString());
                    long loc8 = temp.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[7].ToString());
                    long loc9 = temp.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[8].ToString());
                    long loc10 = temp.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[9].ToString());
                    long loc11 = temp.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[10].ToString());
                    long loc12 = temp.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToInt64(temp.Rows[i].ItemArray[11].ToString());

                    dtYtdSales.Rows.Add("YTD SALE " + YTDYear,
                                       currentmonth < 1 ? "0" : Convert.ToInt64(loc1).ToString("N0", culture),
                                       currentmonth < 2 ? "0" : Convert.ToInt64(loc2).ToString("N0", culture),
                                       currentmonth < 3 ? "0" : Convert.ToInt64(loc3).ToString("N0", culture),
                                       currentmonth < 4 ? "0" : Convert.ToInt64(loc4).ToString("N0", culture),
                                       currentmonth < 5 ? "0" : Convert.ToInt64(loc5).ToString("N0", culture),
                                       currentmonth < 6 ? "0" : Convert.ToInt64(loc6).ToString("N0", culture),
                                       currentmonth < 7 ? "0" : Convert.ToInt64(loc7).ToString("N0", culture),
                                       currentmonth < 8 ? "0" : Convert.ToInt64(loc8).ToString("N0", culture),
                                       currentmonth < 9 ? "0" : Convert.ToInt64(loc9).ToString("N0", culture),
                                       currentmonth < 10 ? "0" : Convert.ToInt64(loc10).ToString("N0", culture),
                                       currentmonth < 11 ? "0" : Convert.ToInt64(loc11).ToString("N0", culture),
                                       currentmonth < 12 ? "0" : Convert.ToInt64(loc12).ToString("N0", culture),
                                       "YTDSALE CurrentYear");
                }
                #endregion
             

              

                decimal temp1 = 0, temp2 = 0, temp3 = 0, temp4 = 0, temp5 = 0, temp6 = 0, temp7 = 0, temp8 = 0, temp9 = 0, temp10 = 0, temp11 = 0, temp12 = 0;
                decimal ys1 = 0, ys2 = 0, ys3 = 0, ys4 = 0, ys5 = 0, ys6 = 0, ys7 = 0, ys8 = 0, ys9 = 0, ys10 = 0, ys11 = 0, ys12 = 0;
                decimal ys_a1 = 0, ys_a2 = 0, ys_a3 = 0, ys_a4 = 0, ys_a5 = 0, ys_a6 = 0, ys_a7 = 0, ys_a8 = 0, ys_a9 = 0, ys_a10 = 0, ys_a11 = 0, ys_a12 = 0;
                for (int i = 0; i < dtYtdSales.Rows.Count; i++)
                {
                    string flag = dtYtdSales.Rows[i].ItemArray[13].ToString();
                    //execute one time
                    if (flag == "YTDSALE ActualYear")
                    {
                        ys_a1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                        ys_a2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                        ys_a3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                        ys_a4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                        ys_a5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                        ys_a6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                        ys_a7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                        ys_a8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                        ys_a9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                        ys_a10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                        ys_a11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                        ys_a12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                    }
                    if (flag == "YTDPLAN CurrentYear")
                    {
                        temp1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                        temp2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                        temp3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                        temp4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                        temp5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                        temp6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                        temp7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                        temp8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                        temp9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                        temp10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                        temp11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                        temp12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                    }
                    if (flag == "YTDSALE CurrentYear")
                    {
                        ys1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                        ys2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                        ys3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                        ys4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                        ys5 = dtYtdSales.Rows[i].ItemArray[5].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[5].ToString());
                        ys6 = dtYtdSales.Rows[i].ItemArray[6].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[6].ToString());
                        ys7 = dtYtdSales.Rows[i].ItemArray[7].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[7].ToString());
                        ys8 = dtYtdSales.Rows[i].ItemArray[8].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[8].ToString());
                        ys9 = dtYtdSales.Rows[i].ItemArray[9].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[9].ToString());
                        ys10 = dtYtdSales.Rows[i].ItemArray[10].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[10].ToString());
                        ys11 = dtYtdSales.Rows[i].ItemArray[11].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[11].ToString());
                        ys12 = dtYtdSales.Rows[i].ItemArray[12].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[12].ToString());
                    }

                }

                ///
                ///PRO-RATE ACH% = ((YTD SALE 2015)/(YTD PLAN 2015))x100

                decimal result1 = 0, result2 = 0, result3 = 0, result4 = 0, result5 = 0, result6 = 0, result7 = 0, result8 = 0, result9 = 0, result10 = 0, result11 = 0, result12 = 0;
               

                ///changed Growth% = ((YTD SALE 2015)-(YTD SALE 2014))*100/YTD SALE 2014
                result1 = 0; result2 = 0; result3 = 0; result4 = 0; result5 = 0; result6 = 0; result7 = 0; result8 = 0; result9 = 0; result10 = 0; result11 = 0; result12 = 0;
                result1 = ys_a1 != 0 ? (((decimal)ys1 - (decimal)ys_a1) * 100) / (decimal)ys_a1 : 0;
                result2 = ys_a1 != 0 ? (((decimal)ys2 - (decimal)ys_a2) * 100) / (decimal)ys_a2 : 0;
                result3 = ys_a1 != 0 ? (((decimal)ys3 - (decimal)ys_a3) * 100) / (decimal)ys_a3 : 0;
                result4 = ys_a1 != 0 ? (((decimal)ys4 - (decimal)ys_a4) * 100) / (decimal)ys_a4 : 0;
                result5 = ys_a1 != 0 ? (((decimal)ys5 - (decimal)ys_a5) * 100) / (decimal)ys_a5 : 0;
                result6 = ys_a1 != 0 ? (((decimal)ys6 - (decimal)ys_a6) * 100) / (decimal)ys_a6 : 0;
                result7 = ys_a1 != 0 ? (((decimal)ys7 - (decimal)ys_a7) * 100) / (decimal)ys_a7 : 0;
                result8 = ys_a1 != 0 ? (((decimal)ys8 - (decimal)ys_a8) * 100) / (decimal)ys_a8 : 0;
                result9 = ys_a1 != 0 ? (((decimal)ys9 - (decimal)ys_a9) * 100) / (decimal)ys_a9 : 0;
                result10 = ys_a1 != 0 ? (((decimal)ys10 - (decimal)ys_a10) * 100) / (decimal)ys_a10 : 0;
                result11 = ys_a1 != 0 ? (((decimal)ys11 - (decimal)ys_a11) * 100) / (decimal)ys_a11 : 0;
                result12 = ys_a1 != 0 ? (((decimal)ys12 - (decimal)ys_a12) * 100) / (decimal)ys_a12 : 0;
                dtYtdSales.Rows.Add("GROWTH%",
                                     Math.Round(result1),
                                     Math.Round(result2),
                                     Math.Round(result3),
                                     Math.Round(result4),
                                     Math.Round(result5),
                                     Math.Round(result6),
                                     Math.Round(result7),
                                     Math.Round(result8),
                                     Math.Round(result9),
                                     Math.Round(result10),
                                     Math.Round(result11),
                                     Math.Round(result12),
                                     "Growth"
                                    );

                result1 = temp1 != 0 ? ((decimal)ys1 / (decimal)temp1) * 100 : 0;
                result2 = temp2 != 0 ? (((decimal)ys2 / (decimal)temp2) * 100) : 0;
                result3 = temp3 != 0 ? (((decimal)ys3 / (decimal)temp3) * 100) : 0;
                result4 = temp4 != 0 ? (((decimal)ys4 / (decimal)temp4) * 100) : 0;
                result5 = temp5 != 0 ? (((decimal)ys5 / (decimal)temp5) * 100) : 0;
                result6 = temp6 != 0 ? (((decimal)ys6 / (decimal)temp6) * 100) : 0;
                result7 = temp7 != 0 ? (((decimal)ys7 / (decimal)temp7) * 100) : 0;
                result8 = temp8 != 0 ? (((decimal)ys8 / (decimal)temp8) * 100) : 0;
                result9 = temp9 != 0 ? (((decimal)ys9 / (decimal)temp9) * 100) : 0;
                result10 = temp10 != 0 ? (((decimal)ys10 / (decimal)temp10) * 100) : 0;
                result11 = temp11 != 0 ? (((decimal)ys11 / (decimal)temp11) * 100) : 0;
                result12 = temp12 != 0 ? (((decimal)ys12 / (decimal)temp12) * 100) : 0;
                dtYtdSales.Rows.Add("PRO-RATA ACH%",
                                     Math.Round(result1),
                                     Math.Round(result2),
                                     Math.Round(result3),
                                     Math.Round(result4),
                                     Math.Round(result5),
                                     Math.Round(result6),
                                     Math.Round(result7),
                                     Math.Round(result8),
                                     Math.Round(result9),
                                     Math.Round(result10),
                                     Math.Round(result11),
                                     Math.Round(result12),
                                     "ProRate"
                                    );
                return dtYtdSales;
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
                return null;
            }
        }

        #endregion



        /// <summary>
        ///  Author : 
        /// Date : 
        /// Desc : For Transposing Datatable for providing it as datasource to chart
        /// </summary>
        /// <param name="inputTable"></param>
        /// <returns></returns>

        private DataTable GenerateTransposedTable(DataTable inputTable)
        {

            try
            {
                DataTable outputTable = new DataTable();

                // Add columns by looping rows

                // Header row's first column is same as in inputTable
                outputTable.Columns.Add(inputTable.Columns[0].ColumnName.ToString());

                // Header row's second column onwards, 'inputTable's first column taken
                foreach (DataRow inRow in inputTable.Rows)
                {
                    string newColName = inRow[0].ToString();
                    outputTable.Columns.Add(newColName);
                }

                // Add rows by looping columns        
                for (int rCount = 1; rCount <= inputTable.Columns.Count - 1; rCount++)
                {
                    DataRow newRow = outputTable.NewRow();

                    // First column is inputTable's Header row's second column
                    newRow[0] = inputTable.Columns[rCount].ColumnName.ToString();
                    for (int cCount = 0; cCount <= inputTable.Rows.Count - 1; cCount++)
                    {
                        string colValue = inputTable.Rows[cCount][rCount].ToString();
                        newRow[cCount + 1] = colValue;
                    }
                    outputTable.Rows.Add(newRow);
                }

                return outputTable;
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
                return null;
            }
        }

       /// <summary>
        /// Author  : 
        /// Date    : 
        /// Desc    :  Load Branches into ListBox 
       /// </summary>
        protected void LoadBranches()
        {
            try
            {

                string name_desc = "", name_code = "";
                int count = 0;
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                string roleId = Session["RoleId"].ToString();
                string userId = Session["UserId"].ToString();
                string branchcode = Session["BranchCode"].ToString();
                DataTable dtData = new DataTable();
                objRSum.BranchCode = userId; // passing here territory Engineer Id  as branch code IF role is TM 
                objRSum.roleId = roleId;
                objRSum.flag = "Branch";
                objRSum.cter = cter;
                dtData = objRSum.getFilterAreaValue(objRSum);
                BranchList.DataSource = dtData;
                BranchList.DataTextField = "BranchDesc";
                BranchList.DataValueField = "BranchCode";
                BranchList.DataBind();
                Session["DtBranchList"] = dtData;
                foreach (ListItem val in BranchList.Items)
                {
                    val.Selected = true;

                    if (val.Selected)
                    {
                        count++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                    }
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                // ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);




            }

            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }
        /// <summary>
        /// Author  : 
        /// Date    : 
        /// Desc    :  Load ProductFamily into ListBox 
        /// </summary>
        protected void LoadProductFamliy()
        {
            string name_desc = "", name_code = "";
            int count = 0;
            try
            {
                DataTable dtProductFamilyList = new DataTable();
                dtProductFamilyList = objBudget.LoadFamilyId();
                ProductFamilyList.DataSource = dtProductFamilyList;
                ProductFamilyList.DataTextField = "item_family_name";
                ProductFamilyList.DataValueField = "item_family_id";

                ProductFamilyList.DataBind();
                //ddlProductFamliy.Items.Insert(0, "ALL");

                foreach (ListItem val in ProductFamilyList.Items)
                {
                    val.Selected = true;
                    if (val.Selected)
                    {
                        count++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        /// <summary>
      /// Author  : 
        /// Date    : 
        /// Desc    :  Load ProductGroup into ListBox 
        /// </summary>
        protected void LoadProductGroup()
        {
            string name_desc = "", name_code = "";
            int count = 0;
            int B_Budgetyear=0;
            try
            {
                B_Budgetyear = Convert.ToInt32(objAdmin.GetProfile("B_BUDGET_YEAR"));
                DataTable ProductGroups = objRSum.GetProductGroups(B_Budgetyear);

                //DataTable selectedTable = ProductGroups.AsEnumerable()
                //                .Where(r => r.Field<int>("year") == B_Budgetyear)
                //                .CopyToDataTable();


                for (int i = 0; i < ProductGroups.Rows.Count; i++)
                {

                    if (Convert.ToString(ProductGroups.Rows[i][1]) == "FIVE YEARS")
                    {
                        ProductGroups.Rows[i][1] = "5YRS";
                    }
                    else if (Convert.ToString(ProductGroups.Rows[i][1]) == "TEN YEARS")
                    {
                        ProductGroups.Rows[i][1] = "10YRS";
                    }
                }
                ProductGrpList.DataSource = ProductGroups;
                ProductGrpList.DataTextField = "splgrps";
                ProductGrpList.DataValueField = "splgrps";
                ProductGrpList.DataBind();

                foreach (ListItem val in ProductGrpList.Items)
                {
                    val.Selected = true;
                    if (val.Selected)
                    {
                        count++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                    }
                }

                //  ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                //  ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }

        /// <summary>
        ///  Author  : 
        /// Date    : 
        /// Desc    :  add css class to the header of gridview  by value
        /// </summary>
        protected void bindgridColor()
        {
            try
            {
                if (grdviewAllValues.Rows.Count != 0)
                {
                    int color = 0;

                    foreach (GridViewRow row in grdviewAllValues.Rows)
                    {
                        var Flag = row.FindControl("lblFlag") as Label;

                        if (Flag.Text == "Heading")
                        {
                            for (int i = 0; i < row.Cells.Count; i++) { row.Cells[i].CssClass = "HeadergridAll"; }
                            row.CssClass = "HeadergridAll";
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }

        /// <summary>
        /// Author:
        /// Date:
        /// Description:Preparing HTML for generating PDF.
        /// </summary>

        private static void PDFCreationWithChart()
        {


            string filename = string.Empty;
            string filepath = string.Empty;
            string html = string.Empty;
            string imagepath = string.Empty;
            try
            {

                html = "<html ><head></head><body>";
                filename = "ReviewMonthly_" + Convert.ToString(HttpContext.Current.Session["UserId"]) + ".pdf";
                filepath = ConfigurationManager.AppSettings["PDF_Folder"].ToString();
                html += heading("ReviewMonthly(Value)");
                html += ExportDatatableToHtml((DataTable)HttpContext.Current.Session["dtYtdSales_inLakh_Val"]);
                imagepath = ConfigurationManager.AppSettings["ChartImageFile"].ToString();
                html += "</body></html>";
                convertPDF(html, filepath, filename, imagepath);
                //bindchart((DataTable)Session["dtChart"]);
                HttpContext.Current.Session["PDF_file"] = Convert.ToString(filepath + filename);
                html = "<html ><head></head><body>";
                filename = "ReviewMonthly_qty_" + Convert.ToString(HttpContext.Current.Session["UserId"]) + ".pdf";
                filepath = ConfigurationManager.AppSettings["PDF_Folder"].ToString();
                html += heading("ReviewMonthly(Quantity)");
                html += ExportDatatableToHtml((DataTable)HttpContext.Current.Session["dtYtdSales_inLakh_QTY"]);
                imagepath = ConfigurationManager.AppSettings["ChartImageFile_qty"].ToString();
                html += "</body></html>";
                convertPDF(html, filepath, filename, imagepath);
                //bindchart((DataTable)Session["dtChart_qty"]);

                HttpContext.Current.Session["PDF_qty_file"] = Convert.ToString(filepath + filename);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                //objCom.LogError(ex);
            }
        }

        /// <summary>
        /// Author:
        /// Date:
        /// Desc:add css class to the header of gridview  by Qty
        /// </summary>
        protected void bindgridColorQty()
        {
            try
            {
                if (grdviewAllQuantites.Rows.Count != 0)
                {
                    int colorqty = 0;

                    foreach (GridViewRow row in grdviewAllQuantites.Rows)
                    {
                        var Flag = row.FindControl("lblFlag") as Label;

                        if (Flag.Text == "Heading")
                        {
                            for (int i = 0; i < row.Cells.Count; i++) { row.Cells[i].CssClass = "HeadergridAll"; }
                            row.CssClass = "HeadergridAll";
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="control"></param>
        public override void VerifyRenderingInServerForm(Control control)
        {

        }


        /// <summary>
        /// Author:
        /// Date:
        /// Desc: for convertin the input datatable to HTML table
        /// </summary>
        /// <param name="dt">input datatable</param>
        /// <returns></returns>
        protected static string ExportDatatableToHtml(DataTable dt)
        {
            StringBuilder strHTMLBuilder = new StringBuilder();
            string Htmltext = string.Empty;
            int fontsize;
            try
            {
                //if (Language.ToLower() == "chinese" || Language.ToLower() == "thai")
                fontsize = 10;
                //else
                //   fontsize = 9;
                strHTMLBuilder.Append("<html ><head></head><body>");
                strHTMLBuilder.Append("<table width='100%'  cellspacing='0' cellpadding='0' style='border:1px black solid; border-collapse: collapse; border-spacing:0;'>");

                //  strHTMLBuilder.Append("<table cellspacing='0' cellpadding='0' style='width:100%;border:1px black solid; border-collapse: collapse; border-spacing:0;'>"); 


                foreach (DataRow myRow in dt.Rows)
                {

                    strHTMLBuilder.Append("<tr style='background-color:white; height:30px'>");
                    foreach (DataColumn myColumn in dt.Columns)
                    {
                        if (Convert.ToString(myColumn.ColumnName) != "flag")
                        {
                            strHTMLBuilder.Append("<td width='50px' style='border-collapse: collapse; border: 1px solid darkgray; font-family:Gotham-Book; font-size:" + fontsize + "px;'>");
                            strHTMLBuilder.Append(Convert.ToString(myRow[myColumn.ColumnName]));
                            strHTMLBuilder.Append("</td>");
                        }
                    }
                    strHTMLBuilder.Append("</tr>");
                }

                strHTMLBuilder.Append("</table>");
                //strHTMLBuilder.Append("</body></html>");
                Htmltext = strHTMLBuilder.ToString();
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }
            return Htmltext;
        }
        /// <summary>
        /// Author:
        /// Date:
        /// Description:we are saving all the html content (table) as  test.htm file
        /// </summary>
        /// <param name="example_html">HTML to export to pdf </param>
        /// <param name="filepath">specifies where html file should be saved</param>
        /// <param name="filename">specifies with what name pdf file should be saved</param>
        /// <param name="dsImage"> specifies the path of the chart image from where it should it get</param>
        private static void convertPDF(string example_html, string filepath, string filename, string dsImage)
        {
            try
            {
                using (FileStream fs = new FileStream(Path.Combine(filepath, "test.htm"), FileMode.Create))
                {
                    using (StreamWriter w = new StreamWriter(fs, Encoding.UTF8))
                    {
                        w.WriteLine(example_html);
                    }
                }

                GeneratePdfFromHtml(filepath, filename, dsImage);

            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }
        }
        /// <summary>
        /// Author:
        /// Date:
        /// Description: here we are creating a inputfile(HTML file) and outputfile(PDF File)with the specified file name and creating an input and output file streams 
        ///                 input stream means html file,output stream means the pdf file. 
        /// </summary>
        /// <param name="filepath">path of html file</param>
        /// <param name="filename">specifies the what should be the name of the pdf file</param>
        /// <param name="dsImage">specifies the path of the chart image </param>

        public static void GeneratePdfFromHtml(string filepath, string filename, string dsImage)
        {
            try
            {
                string outputFilename = Path.Combine(filepath, filename);
                string inputFilename = Path.Combine(filepath, "test.htm");

                using (var input = new FileStream(inputFilename, FileMode.Open))
                using (var output = new FileStream(outputFilename, FileMode.Create))
                {
                    CreatePdf(filepath, filename, input, output, dsImage);
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }

        }
        /// <summary>
        /// Author:
        /// Date:
        /// Description: For generating PDF with the specified filename to the specified filepath using HTMLINPUt and the chart image
        /// </summary>
        /// <param name="filepath">path of pdf file</param>
        /// <param name="filename">the name of pdf file </param>
        /// <param name="htmlInput">the input stream </param>
        /// <param name="pdfOutput">out put stream</param>
        /// <param name="dsImage">path of the chart image</param>
        public static void CreatePdf(string filepath, string filename, Stream htmlInput, Stream pdfOutput, string dsImage)
        {
            string imageURL;
            iTextSharp.text.Paragraph paragraph;
            try
            {
                using (var document = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4.Rotate(), 10, 10, 10, 10))
                {
                    var writer = PdfWriter.GetInstance(document, pdfOutput);
                    var worker = XMLWorkerHelper.GetInstance();
                    TextReader tr = new StreamReader(htmlInput);
                    document.Open();
                    //document.NewPage();
                    worker.ParseXHtml(writer, document, htmlInput, null, Encoding.UTF8);

                    if (!string.IsNullOrEmpty(Convert.ToString(dsImage)))
                    {
                        imageURL = string.Empty;
                        paragraph = new iTextSharp.text.Paragraph(Convert.ToString("Graph :  Summary Of Sales Budget Monthly"));
                        //images = Convert.ToString(dsImage.Tables[j].Rows[i]["image_file_name"]).Split(';');
                        document.Add(paragraph);

                        //imageURL = Convert.ToString(dsImage.Tables[j].Rows[i]["image_file_name"]);
                        iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(dsImage);
                        jpg.ScaleToFit(800f, 600f);
                        jpg.SpacingBefore = 10f;
                        jpg.SpacingAfter = 1f;
                        jpg.Alignment = iTextSharp.text.Element.ALIGN_CENTER;

                        document.Add(jpg);
                    }

                    document.Close();
                }

            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }
        }

        public class UnicodeFontFactory : iTextSharp.text.FontFactoryImp
        {
            private static readonly string FontPath = ConfigurationManager.AppSettings["fontpath"].ToString();

            private readonly BaseFont _baseFont;

            public UnicodeFontFactory()
            {
                _baseFont = BaseFont.CreateFont(FontPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

            }

            public override iTextSharp.text.Font GetFont(string fontname, string encoding, bool embedded, float size, int style, iTextSharp.text.BaseColor color,
              bool cached)
            {
                return new iTextSharp.text.Font(_baseFont, size, style, color);
            }
        }
        /// <summary>
        /// Author:
        /// Date:
        /// Description: For creating a HTMl Table with User selections of branchlist, selist, customername list, customer number list, application list and product group and family list 
        /// </summary>
        /// <param name="heading">input string it is the heading of the pdf file for Value it is value and for Qty it is Qty  </param>
        /// <returns></returns>

        private static string heading(string heading)
        {
            string output = string.Empty;
            StringBuilder strHTMLBuilder = new StringBuilder();
            string imageURL = string.Empty;
            DataTable dtHeading = new DataTable();
            try
            {
                strHTMLBuilder.Append("<table  width='100%'  cellspacing='0' style='border: 1px solid darkgray; font-family:Gotham-Book; border-collapse:collapse;'>");

                strHTMLBuilder.Append("<tr height='15px' style='font-family:Gotham-Book; font-size:12px; text-align:center; background-color:#006780; color:white; border-collapse: collapse;'>");
                strHTMLBuilder.Append("<td colspan='4'>");
                strHTMLBuilder.Append(heading);
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("</tr>");

                strHTMLBuilder.Append("<tr >");
                strHTMLBuilder.Append("<td width='150px' style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>Branch</td>");
                strHTMLBuilder.Append("<td width='350px' style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                strHTMLBuilder.Append((!string.IsNullOrEmpty(Convert.ToString(HttpContext.Current.Session["SelectedBranchList"]))) ? Convert.ToString(HttpContext.Current.Session["SelectedBranchList"]) : "ALL");
                //strHTMLBuilder.Append(Convert.ToString(Session["SelectedBranchList"]));
                strHTMLBuilder.Append("</td>");

                strHTMLBuilder.Append("<td width='150px' style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>Sales Engineer</td>");
                strHTMLBuilder.Append("<td width='350px' style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                strHTMLBuilder.Append((!string.IsNullOrEmpty(Convert.ToString(HttpContext.Current.Session["SelectedSalesEngineers"]))) ? Convert.ToString(HttpContext.Current.Session["SelectedSalesEngineers"]) : "ALL");
                //strHTMLBuilder.Append(Convert.ToString(Session["SelectedSalesEngineers"]));
                strHTMLBuilder.Append("</td>");

                strHTMLBuilder.Append("</tr>");
                strHTMLBuilder.Append("<tr >");
                strHTMLBuilder.Append("<td width='150px' style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>Cust Type</td>");
                strHTMLBuilder.Append("<td width='350px' style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                strHTMLBuilder.Append((!string.IsNullOrEmpty(Convert.ToString(HttpContext.Current.Session["ddlcustomertype"]))) ? Convert.ToString(HttpContext.Current.Session["ddlcustomertype"]) : "ALL");
                //strHTMLBuilder.Append(Convert.ToString(ddlcustomertype.SelectedItem.Value));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td width='150px' style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>Cust Name</td>");
                strHTMLBuilder.Append("<td width='350px' style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                strHTMLBuilder.Append((!string.IsNullOrEmpty(Convert.ToString(HttpContext.Current.Session["SelectedCustomerNames"]))) ? Convert.ToString(HttpContext.Current.Session["SelectedCustomerNames"]) : "ALL");
                //strHTMLBuilder.Append(Convert.ToString(Session["SelectedCustomerNames"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("</tr>");
                strHTMLBuilder.Append("<tr >");
                strHTMLBuilder.Append("<td width='150px' style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>Cust Number</td>");
                strHTMLBuilder.Append("<td width='350px' style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                strHTMLBuilder.Append((!string.IsNullOrEmpty(Convert.ToString(HttpContext.Current.Session["SelectedCustomerNumbers"]))) ? Convert.ToString(HttpContext.Current.Session["SelectedCustomerNumbers"]) : "ALL");
                //strHTMLBuilder.Append(Convert.ToString(Session["SelectedCustomerNumbers"]));
                strHTMLBuilder.Append("</td>");

                strHTMLBuilder.Append("<td width='150px' style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>Product Group</td>");
                strHTMLBuilder.Append("<td width='350px' style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                strHTMLBuilder.Append((!string.IsNullOrEmpty(Convert.ToString(HttpContext.Current.Session["SelectedProductGroup"]))) ? Convert.ToString(HttpContext.Current.Session["SelectedProductGroup"]) : "ALL");
                //strHTMLBuilder.Append(Convert.ToString(Session["SelectedProductGroup"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("</tr>");


                strHTMLBuilder.Append("<tr >");

                strHTMLBuilder.Append("<td style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>Product Family</td>");

                strHTMLBuilder.Append("<td style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                strHTMLBuilder.Append((!string.IsNullOrEmpty(Convert.ToString(HttpContext.Current.Session["SelectedProductFamily"]))) ? Convert.ToString(HttpContext.Current.Session["SelectedProductFamily"]) : "ALL");
                //strHTMLBuilder.Append(Convert.ToString(Session["SelectedProductFamily"]));
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("<td style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>Application</td>");

                strHTMLBuilder.Append("<td style='font-weight:normal; font-size:10px; border: 1px solid darkgray; border-collapse:collapse;'>");
                strHTMLBuilder.Append((!string.IsNullOrEmpty(Convert.ToString(HttpContext.Current.Session["SelectedApplications"]))) ? Convert.ToString(HttpContext.Current.Session["SelectedApplications"]) : "ALL");
                strHTMLBuilder.Append("</td>");
                strHTMLBuilder.Append("</tr>");


                strHTMLBuilder.Append("</table>");

                output = strHTMLBuilder.ToString();
            }
            catch (Exception ex)
            {
                //objCom.LogError(ex);
            }
            return output;
        }

        /// <summary>
        /// Author:K.LakshmiBindu
        /// Date:
        /// Desc: For Converting the DataTable to Json String which is used as dataprovider for amchart
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        public static string DataTableToJSONWithStringBuilder(DataTable table)
        {
            var JSONString = new StringBuilder();
            try
            {
                if (table != null)
                {
                    if (table.Rows.Count > 0)
                    {
                        JSONString.Append("[");
                        for (int i = 0; i < table.Rows.Count - 1; i++)
                        {
                            JSONString.Append("{");
                            for (int j = 0; j < table.Columns.Count; j++)
                            {
                                if (j < table.Columns.Count - 1)
                                {
                                    int charindex = table.Rows[i][j].ToString().IndexOf(',');
                                    if (charindex == -1)
                                    {
                                        JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\",");
                                    }
                                    else
                                    {
                                        string s = table.Rows[i][j].ToString();
                                        var chars = s.ToCharArray().ToList();
                                        var builder = new StringBuilder();
                                        foreach (var character in chars)
                                        {
                                            if (char.IsNumber(character))
                                                builder.Append(character);
                                        }
                                        JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + builder + "\",");
                                    }
                                }
                                else if (j == table.Columns.Count - 1)
                                {
                                    JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\"");
                                }
                            }
                            if (i == table.Rows.Count - 2)
                            {
                                JSONString.Append("}");


                            }
                            else
                            {
                                JSONString.Append("},");
                            }
                        }
                        JSONString.Append("]");
                    }

                }
                return JSONString.ToString();
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
                return null;
            }
        }


        #endregion

        #region Compression
        private byte[] Compress(byte[] b)
        {
            MemoryStream ms = new MemoryStream();
            GZipStream zs = new GZipStream(ms, CompressionMode.Compress, true);
            zs.Write(b, 0, b.Length);
            zs.Close();
            return ms.ToArray();
        }

        /// This method takes the compressed byte stream as parameter
        /// and return a decompressed bytestream.

        private byte[] Decompress(byte[] b)
        {
            MemoryStream ms = new MemoryStream();
            GZipStream zs = new GZipStream(new MemoryStream(b),
                                           CompressionMode.Decompress, true);
            byte[] buffer = new byte[4096];
            int size;
            while (true)
            {
                size = zs.Read(buffer, 0, buffer.Length);
                if (size > 0)
                    ms.Write(buffer, 0, size);
                else break;
            }
            zs.Close();
            return ms.ToArray();
        }

        protected override object LoadPageStateFromPersistenceMedium()
        {
            System.Web.UI.PageStatePersister pageStatePersister1 = this.PageStatePersister;
            pageStatePersister1.Load();
            String vState = pageStatePersister1.ViewState.ToString();
            byte[] pBytes = System.Convert.FromBase64String(vState);
            pBytes = Decompress(pBytes);
            LosFormatter mFormat = new LosFormatter();
            Object ViewState = mFormat.Deserialize(System.Convert.ToBase64String(pBytes));
            return new Pair(pageStatePersister1.ControlState, ViewState);
        }

        protected override void SavePageStateToPersistenceMedium(Object pViewState)
        {
            Pair pair1;
            System.Web.UI.PageStatePersister pageStatePersister1 = this.PageStatePersister;
            Object ViewState;
            if (pViewState is Pair)
            {

                pair1 = ((Pair)pViewState);
                pageStatePersister1.ControlState = pair1.First;
                ViewState = pair1.Second;
            }
            else
            {
                ViewState = pViewState;
            }
            LosFormatter mFormat = new LosFormatter();
            StringWriter mWriter = new StringWriter();
            mFormat.Serialize(mWriter, ViewState);
            String mViewStateStr = mWriter.ToString();
            byte[] pBytes = System.Convert.FromBase64String(mViewStateStr);
            pBytes = Compress(pBytes);
            String vStateStr = System.Convert.ToBase64String(pBytes);
            pageStatePersister1.ViewState = vStateStr;
            pageStatePersister1.Save();
        }
        #endregion

        #region Webmethods
        /// <summary>
        /// Author:K.LakshmiBindu
        /// Date:
        /// Desc:For returning data as Json string to amchart
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static string AmchartGraphVal()
        {
            try
            {
                DataTable dtChart = null;
                string jsonString = null;
                Random rnd = new Random();

                dtChart = HttpContext.Current.Session["dtChart_val"] as DataTable;

                if (dtChart != null)
                {

                    if (!dtChart.Columns.Contains("Color"))
                    {
                        DataColumn dc = new DataColumn("Color");

                        dtChart.Columns.Add(dc);
                        for (int i = 0; i < dtChart.Rows.Count - 1; i++)
                        {
                            Color myColor = Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256));
                            string hex = myColor.R.ToString("X2") + myColor.G.ToString("X2") + myColor.B.ToString("X2");
                            DataRow dr = dtChart.Rows[i];
                            dr[dc] = "#" + hex;
                            //  dtChart.Rows.Add(dr);
                        }
                    }
                    int rowcount = dtChart.Rows.Count;
                   // dtChart.Rows[rowcount - 1].Delete();
                    jsonString = DataTableToJSONWithStringBuilder(dtChart);

                }
                return jsonString;
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
                return null;
            }
        }

        /// <summary>
        ///  Author:K.LakshmiBindu
        /// Date:
        /// Desc:For returning data as Json string to amchart
        /// </summary>
        /// <returns></returns>
        [WebMethod]
        public static string AmchartGraphQty()
        {
            try
            {
                DataTable dtChart = null;
                string jsonString = null;
                Random rnd = new Random();


                dtChart = HttpContext.Current.Session["dtChart_Qty"] as DataTable;
                //   HttpContext.Current.Session["dtChart_qty"] = null;


                if (dtChart != null)
                {
                    //Changing last row column header

                    if (!dtChart.Columns.Contains("Color"))
                    {
                        DataColumn dc = new DataColumn("Color");

                        dtChart.Columns.Add(dc);
                        for (int i = 0; i < dtChart.Rows.Count - 1; i++)
                        {
                            Color myColor = Color.FromArgb(rnd.Next(256), rnd.Next(256), rnd.Next(256));
                            string hex = myColor.R.ToString("X2") + myColor.G.ToString("X2") + myColor.B.ToString("X2");
                            DataRow dr = dtChart.Rows[i];
                            dr[dc] = "#" + hex;
                            //  dtChart.Rows.Add(dr);
                        }
                    }
                    jsonString = DataTableToJSONWithStringBuilder(dtChart);

                }
                return jsonString;
            }
            catch (Exception ex)
            {

                CommonFunctions.LogErrorStatic(ex);
                return null;
            }
        }


        [WebMethod]
        public static void ToPNG(string base64String, string s)
        {
            try
            {

                String testing = base64String;
                Console.Write(testing.IndexOf("base64") + Environment.NewLine);
                testing = testing.Substring(testing.LastIndexOf("base64"));
                base64String = testing.Substring(7);
                //HttpContext.Current.Session["base64String"] = base64String;
                byte[] data = Convert.FromBase64String(base64String);
                //using (var stream = new MemoryStream(data, 0, data.Length))
                using (var stream = new MemoryStream(data))
                {
                    System.Drawing.Image image = System.Drawing.Image.FromStream(stream);
                    //image.Save(Convert.ToString(ConfigurationManager.AppSettings["ChartImageFile"]));
                    if (s == "value")
                    {
                        HttpContext.Current.Session["topng"] = 1;

                        image.Save(Convert.ToString(ConfigurationManager.AppSettings["ChartImageFile"]), System.Drawing.Imaging.ImageFormat.Png);
                    } if (s == "Qty")
                    {
                        HttpContext.Current.Session["topng"] = 1;
                        image.Save(Convert.ToString(ConfigurationManager.AppSettings["ChartImageFile_qty"]), System.Drawing.Imaging.ImageFormat.Png);
                    }
                }


                //   System.Drawing.Image image =  System.Drawing.Image.FromStream(stream);
                //   PDFCreationWithChart();
                //TODO: do something with image


                PDFCreationWithChart();


            }

            catch (Exception e)
            {
                CommonFunctions.LogErrorStatic(e);
            }

        }
        #endregion
    }
}