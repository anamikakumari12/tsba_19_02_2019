﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;

namespace TaegutecSalesBudget
{
    public class CommonFunctions
    {
        Budget objBudget = new Budget();
        public void LogError(Exception ex)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex.Message);
            message += Environment.NewLine;
            message += string.Format("StackTrace: {0}", ex.StackTrace);
            message += Environment.NewLine;
            message += string.Format("Source: {0}", ex.Source);
            message += Environment.NewLine;
            message += string.Format("TargetSite: {0}", ex.TargetSite.ToString());
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;


            StreamWriter log;

            if (!File.Exists("logfile.txt"))
            {
                log = new StreamWriter(("C:\\TSBA_Log\\logfile.txt"), true);
            }
            else
            {
                log = File.AppendText(("C:\\TSBA_Log\\logfile.txt"));
            }

            // Write to the file:
            //log.WriteLine("Date Time:" + DateTime.Now.ToString());
            log.WriteLine(message);
            //log.WriteLine(count);
            //log.WriteLine(names);
            //log.WriteLine(use + "\n");
            // Close the stream:
            log.Close();

            //string errorLogPath = "~/ErrorLog/ErrorLog.txt";
            ////errorLogPath = AppendTimeStamp(errorLogPath);
            ////if (!(File.Exists(errorLogPath)))
            ////{
            ////    File.Create(errorLogPath);
            ////}
            //string path = System.Web.HttpContext.Current.Server.MapPath(errorLogPath);
            //using (StreamWriter writer = new StreamWriter(path, true))
            //{
            //    writer.WriteLine(message);
            //    writer.Close();
            //}
        }

        public static void LogErrorStatic(Exception ex)
        {
            string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;
            message += string.Format("Message: {0}", ex.Message);
            message += Environment.NewLine;
            message += string.Format("StackTrace: {0}", ex.StackTrace);
            message += Environment.NewLine;
            message += string.Format("Source: {0}", ex.Source);
            message += Environment.NewLine;
            message += string.Format("TargetSite: {0}", ex.TargetSite.ToString());
            message += Environment.NewLine;
            message += "-----------------------------------------------------------";
            message += Environment.NewLine;


            StreamWriter log;

            if (!File.Exists("logfile.txt"))
            {
                log = new StreamWriter(("C:\\TSBA_Log\\logfile.txt"), true);
            }
            else
            {
                log = File.AppendText(("C:\\TSBA_Log\\logfile.txt"));
            }

            // Write to the file:
            //log.WriteLine("Date Time:" + DateTime.Now.ToString());
            log.WriteLine(message);
            //log.WriteLine(count);
            //log.WriteLine(names);
            //log.WriteLine(use + "\n");
            // Close the stream:
            log.Close();

            //string errorLogPath = "~/ErrorLog/ErrorLog.txt";
            ////errorLogPath = AppendTimeStamp(errorLogPath);
            ////if (!(File.Exists(errorLogPath)))
            ////{
            ////    File.Create(errorLogPath);
            ////}
            //string path = System.Web.HttpContext.Current.Server.MapPath(errorLogPath);
            //using (StreamWriter writer = new StreamWriter(path, true))
            //{
            //    writer.WriteLine(message);
            //    writer.Close();
            //}
        }

        public string AppendTimeStamp(string fileName)
        {
            string file = string.Empty;
            file = string.Concat(
           Path.GetFileNameWithoutExtension(fileName),
           DateTime.Now.ToString("_yyyy_MM_dd"),
           Path.GetExtension(fileName)
           );
            return file;
        }

        /// <summary>
        /// Author : Anamika
        /// Date : Nov 14, 2016
        /// Desc : A function to add 3 columns Inserts/Tool 
        /// </summary>
        /// <param name="dtMain"></param>
        /// <returns></returns>
        public DataTable AddInsertPerTools(DataTable dtMain)
        {
            try
            {
                DataTable Dtbudget = new DataTable();

                Dtbudget.Columns.Add("gold_flag", typeof(string)); //1
                Dtbudget.Columns.Add("top_flag", typeof(string)); //2
                Dtbudget.Columns.Add("five_years_flag", typeof(string)); //3
                Dtbudget.Columns.Add("bb_flag", typeof(string)); //4
                Dtbudget.Columns.Add("SPC_flag", typeof(string));//5
                Dtbudget.Columns.Add("ten_years_flag", typeof(string));

                Dtbudget.Columns.Add("item_id", typeof(string)); //6
                Dtbudget.Columns.Add("item_family_id", typeof(string));//7
                Dtbudget.Columns.Add("item_family_name", typeof(string)); //8
                Dtbudget.Columns.Add("item_sub_family_id", typeof(string)); //9
                Dtbudget.Columns.Add("item_sub_family_name", typeof(string)); //10

                Dtbudget.Columns.Add("item_group_code", typeof(string)); //11
                Dtbudget.Columns.Add("insert_or_tool_flag", typeof(string)); //12
                Dtbudget.Columns.Add("item_code", typeof(string)); //13
                Dtbudget.Columns.Add("item_short_name", typeof(string)); //14
                Dtbudget.Columns.Add("item_description", typeof(string)); //15
                Dtbudget.Columns.Add("Customer_region", typeof(string)); //16

                Dtbudget.Columns.Add("sales_qty_year_2", typeof(string)); //17
                Dtbudget.Columns.Add("sales_qty_year_1", typeof(string)); //18
                Dtbudget.Columns.Add("sales_qty_year_0", typeof(string)); //19
                Dtbudget.Columns.Add("estimate_qty_next_year", typeof(string)); //20

                Dtbudget.Columns.Add("insertpertool_year_2", typeof(string)); //17
                Dtbudget.Columns.Add("insertpertool_year_1", typeof(string)); //18
                Dtbudget.Columns.Add("insertpertool_year_0", typeof(string)); //19
                Dtbudget.Columns.Add("insertpertool_next_year", typeof(string)); //20

                Dtbudget.Columns.Add("sales_value_year_2", typeof(string)); //21
                Dtbudget.Columns.Add("sales_value_year_1", typeof(string)); //22
                Dtbudget.Columns.Add("sales_value_year_0", typeof(string)); //23
                Dtbudget.Columns.Add("estimate_value_next_year", typeof(string)); //24

                Dtbudget.Columns.Add("sumFlag", typeof(string)); //25

                //quality variance
                Dtbudget.Columns.Add("QuantityVariance_sales_qty_year_2", typeof(string)); //26
                Dtbudget.Columns.Add("QuantityVariance_sales_qty_year_1", typeof(string)); //27
                Dtbudget.Columns.Add("QuantityVariance_sales_qty_year_0", typeof(string)); //28

                //Quality Percentage
                Dtbudget.Columns.Add("QuantityPercentage_sales_qty_year_2", typeof(string)); //29
                Dtbudget.Columns.Add("QuantityPercentage_sales_qty_year_1", typeof(string)); //30
                Dtbudget.Columns.Add("QuantityPercentage_sales_qty_year_0", typeof(string)); //31

                //Value Percentage
                Dtbudget.Columns.Add("ValuePercentage_sales_value_year_2", typeof(string));//32
                Dtbudget.Columns.Add("ValuePercentage_sales_value_year_1", typeof(string));//33
                Dtbudget.Columns.Add("ValuePercentage_sales_value_year_0", typeof(string));//34

                //price per unit
                Dtbudget.Columns.Add("PricePerUnit_sales_value_year_2", typeof(string));//35
                Dtbudget.Columns.Add("PricePerUnit_sales_value_year_1", typeof(string));//36
                Dtbudget.Columns.Add("PricePerUnit_sales_value_year_0", typeof(string));//37
                Dtbudget.Columns.Add("PricePerUnit_sales_value_year_P", typeof(string));//38

                //price changes per year
                Dtbudget.Columns.Add("priceschange_sales_value_year_2", typeof(string)); //39
                Dtbudget.Columns.Add("priceschange_sales_value_year_1", typeof(string)); //40
                Dtbudget.Columns.Add("priceschange_sales_value_year_0", typeof(string)); //41
                for (int j = 0; j < dtMain.Rows.Count; j++)
                {
                    Dtbudget.Rows.Add(
                                     dtMain.Rows[j].ItemArray[0].ToString(),//gold_flag
                                     dtMain.Rows[j].ItemArray[1].ToString(),//top_flag
                                     dtMain.Rows[j].ItemArray[2].ToString(),//five_years_flag
                                     dtMain.Rows[j].ItemArray[3].ToString(),//bb_flag
                                     dtMain.Rows[j].ItemArray[4].ToString(),//SPC_flag

                                     dtMain.Rows[j].ItemArray[5].ToString(),//ten_years_flag
                                     dtMain.Rows[j].ItemArray[6].ToString(),
                                     dtMain.Rows[j].ItemArray[7].ToString(),
                                     dtMain.Rows[j].ItemArray[8].ToString(),
                                     dtMain.Rows[j].ItemArray[9].ToString(),

                                     dtMain.Rows[j].ItemArray[10].ToString(),
                                     dtMain.Rows[j].ItemArray[11].ToString(),
                                     dtMain.Rows[j].ItemArray[12].ToString(),
                                     dtMain.Rows[j].ItemArray[13].ToString(),
                                     dtMain.Rows[j].ItemArray[14].ToString(),
                                     dtMain.Rows[j].ItemArray[15].ToString(),

                                     dtMain.Rows[j].ItemArray[16].ToString(),
                                     dtMain.Rows[j].ItemArray[17].ToString(),
                                     dtMain.Rows[j].ItemArray[18].ToString(),
                                     dtMain.Rows[j].ItemArray[19].ToString(),
                                     "", "", "", "",
                                     dtMain.Rows[j].ItemArray[20].ToString(),
                                     dtMain.Rows[j].ItemArray[21].ToString(),
                                     dtMain.Rows[j].ItemArray[22].ToString(),
                                     dtMain.Rows[j].ItemArray[23].ToString(),

                                     dtMain.Rows[j].ItemArray[24].ToString(),//sum flag
                                     dtMain.Rows[j].ItemArray[25].ToString(),
                                     dtMain.Rows[j].ItemArray[26].ToString(),
                                     dtMain.Rows[j].ItemArray[27].ToString(),
                                     dtMain.Rows[j].ItemArray[28].ToString(),
                                     dtMain.Rows[j].ItemArray[29].ToString(),
                                     dtMain.Rows[j].ItemArray[30].ToString(),
                                     dtMain.Rows[j].ItemArray[31].ToString(),
                                     dtMain.Rows[j].ItemArray[32].ToString(),
                                     dtMain.Rows[j].ItemArray[33].ToString(),
                                     dtMain.Rows[j].ItemArray[34].ToString(),
                                     dtMain.Rows[j].ItemArray[35].ToString(),
                                     dtMain.Rows[j].ItemArray[36].ToString(),
                                     dtMain.Rows[j].ItemArray[37].ToString(),
                                     dtMain.Rows[j].ItemArray[38].ToString(),
                                     dtMain.Rows[j].ItemArray[39].ToString(),
                                     dtMain.Rows[j].ItemArray[40].ToString(),
                                     dtMain.Rows[j].ItemArray[41].ToString()

                                     );
                }

                DataTable dtreturn = objBudget.AddInsertPerTool(Dtbudget);
                //dtMain.Merge(dtreturn);
                dtMain.Columns.Add("insertpertool_year_2", typeof(string)); //17
                dtMain.Columns.Add("insertpertool_year_1", typeof(string)); //18
                dtMain.Columns.Add("insertpertool_year_0", typeof(string)); //19
                dtMain.Columns.Add("insertpertool_next_year", typeof(string)); //20
                dtMain.Columns.Add("ID", typeof(int)); //20
                dtMain = objBudget.RemoveEmptyRows(dtMain);
                int ji = 0;
                foreach (DataRow row in dtMain.Rows)
                {
                    if (dtreturn.Rows.Count > ji && row["insert_or_tool_flag"] != "FamilyHeading" && row["insert_or_tool_flag"] != "SubFamHeading")
                    {
                        //row["ID"] = ji;
                        row["insertpertool_year_2"] = dtreturn.Rows[ji].ItemArray[20].ToString();
                        row["insertpertool_year_1"] = dtreturn.Rows[ji].ItemArray[21].ToString();
                        row["insertpertool_year_0"] = dtreturn.Rows[ji].ItemArray[22].ToString();
                        row["insertpertool_next_year"] = dtreturn.Rows[ji].ItemArray[23].ToString();
                    }
                    ji++;
                }

                
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            return dtMain;
        }

        /// <summary>
        /// Anantha
        /// </summary>
        /// <param name="dtMain"></param>
        /// <returns></returns>
        public DataTable GrandTotal(DataTable dtMain)
        {
            DataTable Dtbudget = new DataTable();

            Dtbudget.Columns.Add("gold_flag", typeof(string)); //1
            Dtbudget.Columns.Add("top_flag", typeof(string)); //2
            Dtbudget.Columns.Add("five_years_flag", typeof(string)); //3
            Dtbudget.Columns.Add("bb_flag", typeof(string)); //4
            Dtbudget.Columns.Add("SPC_flag", typeof(string));//5
            Dtbudget.Columns.Add("ten_years_flag", typeof(string)); //3

            Dtbudget.Columns.Add("item_id", typeof(string)); //6
            Dtbudget.Columns.Add("item_family_id", typeof(string));//7
            Dtbudget.Columns.Add("item_family_name", typeof(string)); //8
            Dtbudget.Columns.Add("item_sub_family_id", typeof(string)); //9
            Dtbudget.Columns.Add("item_sub_family_name", typeof(string)); //10

            Dtbudget.Columns.Add("item_group_code", typeof(string)); //11
            Dtbudget.Columns.Add("insert_or_tool_flag", typeof(string)); //12
            Dtbudget.Columns.Add("item_code", typeof(string)); //13
            Dtbudget.Columns.Add("item_short_name", typeof(string)); //14
            Dtbudget.Columns.Add("item_description", typeof(string)); //15
            Dtbudget.Columns.Add("Customer_region", typeof(string)); //16

            Dtbudget.Columns.Add("sales_qty_year_2", typeof(string)); //17
            Dtbudget.Columns.Add("sales_qty_year_1", typeof(string)); //18
            Dtbudget.Columns.Add("sales_qty_year_0", typeof(string)); //19
            Dtbudget.Columns.Add("estimate_qty_next_year", typeof(string)); //20

            Dtbudget.Columns.Add("sales_value_year_2", typeof(string)); //21
            Dtbudget.Columns.Add("sales_value_year_1", typeof(string)); //22
            Dtbudget.Columns.Add("sales_value_year_0", typeof(string)); //23
            Dtbudget.Columns.Add("estimate_value_next_year", typeof(string)); //24

            Dtbudget.Columns.Add("sumFlag", typeof(string)); //25

            //quality variance
            Dtbudget.Columns.Add("QuantityVariance_sales_qty_year_2", typeof(string)); //26
            Dtbudget.Columns.Add("QuantityVariance_sales_qty_year_1", typeof(string)); //27
            Dtbudget.Columns.Add("QuantityVariance_sales_qty_year_0", typeof(string)); //28

            //Quality Percentage
            Dtbudget.Columns.Add("QuantityPercentage_sales_qty_year_2", typeof(string)); //29
            Dtbudget.Columns.Add("QuantityPercentage_sales_qty_year_1", typeof(string)); //30
            Dtbudget.Columns.Add("QuantityPercentage_sales_qty_year_0", typeof(string)); //31

            //Value Percentage
            Dtbudget.Columns.Add("ValuePercentage_sales_value_year_2", typeof(string));//32
            Dtbudget.Columns.Add("ValuePercentage_sales_value_year_1", typeof(string));//33
            Dtbudget.Columns.Add("ValuePercentage_sales_value_year_0", typeof(string));//34

            //price per unit
            Dtbudget.Columns.Add("PricePerUnit_sales_value_year_2", typeof(string));//35
            Dtbudget.Columns.Add("PricePerUnit_sales_value_year_1", typeof(string));//36
            Dtbudget.Columns.Add("PricePerUnit_sales_value_year_0", typeof(string));//37
            Dtbudget.Columns.Add("PricePerUnit_sales_value_year_P", typeof(string));//38

            //price changes per year
            Dtbudget.Columns.Add("priceschange_sales_value_year_2", typeof(string)); //39
            Dtbudget.Columns.Add("priceschange_sales_value_year_1", typeof(string)); //40
            Dtbudget.Columns.Add("priceschange_sales_value_year_0", typeof(string)); //41
            Dtbudget.Rows.Add(
                                   dtMain.Rows[0].ItemArray[0].ToString(),//gold_flag
                                   dtMain.Rows[0].ItemArray[1].ToString(),//top_flag
                                   dtMain.Rows[0].ItemArray[2].ToString(),//five_years_flag
                                   dtMain.Rows[0].ItemArray[3].ToString(),//bb_flag
                                   dtMain.Rows[0].ItemArray[4].ToString(),//SPC_flag
                                   dtMain.Rows[0].ItemArray[5].ToString(),
                                   dtMain.Rows[0].ItemArray[6].ToString(),
                                   dtMain.Rows[0].ItemArray[7].ToString(),
                                   dtMain.Rows[0].ItemArray[8].ToString(),
                                   dtMain.Rows[0].ItemArray[9].ToString(),
                                   dtMain.Rows[0].ItemArray[10].ToString(),
                                   dtMain.Rows[0].ItemArray[11].ToString(),
                                   dtMain.Rows[0].ItemArray[12].ToString(),
                                   dtMain.Rows[0].ItemArray[13].ToString(),
                                   dtMain.Rows[0].ItemArray[14].ToString(),
                                   dtMain.Rows[0].ItemArray[15].ToString(),
                                   dtMain.Rows[0].ItemArray[16].ToString(),
                                   dtMain.Rows[0].ItemArray[17].ToString(),
                                   dtMain.Rows[0].ItemArray[18].ToString(),
                                   dtMain.Rows[0].ItemArray[19].ToString(),
                                   dtMain.Rows[0].ItemArray[20].ToString(),
                                   dtMain.Rows[0].ItemArray[21].ToString(),
                                   dtMain.Rows[0].ItemArray[22].ToString(),
                                   dtMain.Rows[0].ItemArray[23].ToString(),
                                   dtMain.Rows[0].ItemArray[24].ToString(),
                                   dtMain.Rows[0].ItemArray[25].ToString(),
                                   dtMain.Rows[0].ItemArray[26].ToString(),
                                   dtMain.Rows[0].ItemArray[27].ToString(),
                                   dtMain.Rows[0].ItemArray[28].ToString(),
                                   dtMain.Rows[0].ItemArray[29].ToString(),
                                   dtMain.Rows[0].ItemArray[30].ToString(),
                                   dtMain.Rows[0].ItemArray[31].ToString(),
                                   dtMain.Rows[0].ItemArray[32].ToString(),
                                   dtMain.Rows[0].ItemArray[33].ToString(),
                                   dtMain.Rows[0].ItemArray[34].ToString(),
                                   dtMain.Rows[0].ItemArray[35].ToString(),
                                   dtMain.Rows[0].ItemArray[36].ToString(),
                                   dtMain.Rows[0].ItemArray[37].ToString(),
                                   dtMain.Rows[0].ItemArray[38].ToString(),
                                   dtMain.Rows[0].ItemArray[39].ToString(),
                                   dtMain.Rows[0].ItemArray[40].ToString(),
                                   dtMain.Rows[0].ItemArray[41].ToString()
                                   );

            int ji = dtMain.Rows.Count;

            Dtbudget.Rows.Add(
                                  dtMain.Rows[ji - 1].ItemArray[0].ToString(),//gold_flag
                                  dtMain.Rows[ji - 1].ItemArray[1].ToString(),//top_flag
                                  dtMain.Rows[ji - 1].ItemArray[2].ToString(),//five_years_flag
                                  dtMain.Rows[ji - 1].ItemArray[3].ToString(),//bb_flag
                                  dtMain.Rows[ji - 1].ItemArray[4].ToString(),//SPC_flag
                                  dtMain.Rows[ji - 1].ItemArray[5].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[6].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[7].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[8].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[9].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[10].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[11].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[12].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[13].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[14].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[15].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[16].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[17].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[18].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[19].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[20].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[21].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[22].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[23].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[24].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[25].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[26].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[27].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[28].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[29].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[30].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[31].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[32].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[33].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[34].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[35].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[36].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[37].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[38].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[39].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[40].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[41].ToString()
                                  );
            for (int j = 1; j < dtMain.Rows.Count; j++)
            {
                Dtbudget.Rows.Add(
                                 dtMain.Rows[j].ItemArray[0].ToString(),//gold_flag
                                 dtMain.Rows[j].ItemArray[1].ToString(),//top_flag
                                 dtMain.Rows[j].ItemArray[2].ToString(),//five_years_flag
                                 dtMain.Rows[j].ItemArray[3].ToString(),//bb_flag
                                 dtMain.Rows[j].ItemArray[4].ToString(),//SPC_flag
                                 dtMain.Rows[j].ItemArray[5].ToString(),
                                 dtMain.Rows[j].ItemArray[6].ToString(),
                                 dtMain.Rows[j].ItemArray[7].ToString(),
                                 dtMain.Rows[j].ItemArray[8].ToString(),
                                 dtMain.Rows[j].ItemArray[9].ToString(),
                                 dtMain.Rows[j].ItemArray[10].ToString(),
                                 dtMain.Rows[j].ItemArray[11].ToString(),
                                 dtMain.Rows[j].ItemArray[12].ToString(),
                                 dtMain.Rows[j].ItemArray[13].ToString(),
                                 dtMain.Rows[j].ItemArray[14].ToString(),
                                 dtMain.Rows[j].ItemArray[15].ToString(),
                                 dtMain.Rows[j].ItemArray[16].ToString(),
                                 dtMain.Rows[j].ItemArray[17].ToString(),
                                 dtMain.Rows[j].ItemArray[18].ToString(),
                                 dtMain.Rows[j].ItemArray[19].ToString(),
                                 dtMain.Rows[j].ItemArray[20].ToString(),
                                 dtMain.Rows[j].ItemArray[21].ToString(),
                                 dtMain.Rows[j].ItemArray[22].ToString(),
                                 dtMain.Rows[j].ItemArray[23].ToString(),
                                 dtMain.Rows[j].ItemArray[24].ToString(),
                                 dtMain.Rows[j].ItemArray[25].ToString(),
                                 dtMain.Rows[j].ItemArray[26].ToString(),
                                 dtMain.Rows[j].ItemArray[27].ToString(),
                                 dtMain.Rows[j].ItemArray[28].ToString(),
                                 dtMain.Rows[j].ItemArray[29].ToString(),
                                 dtMain.Rows[j].ItemArray[30].ToString(),
                                 dtMain.Rows[j].ItemArray[31].ToString(),
                                 dtMain.Rows[j].ItemArray[32].ToString(),
                                 dtMain.Rows[j].ItemArray[33].ToString(),
                                 dtMain.Rows[j].ItemArray[34].ToString(),
                                 dtMain.Rows[j].ItemArray[35].ToString(),
                                 dtMain.Rows[j].ItemArray[36].ToString(),
                                 dtMain.Rows[j].ItemArray[37].ToString(),
                                 dtMain.Rows[j].ItemArray[38].ToString(),
                                 dtMain.Rows[j].ItemArray[39].ToString(),
                                 dtMain.Rows[j].ItemArray[40].ToString(),
                                 dtMain.Rows[j].ItemArray[41].ToString()
                                 );
            }
            return Dtbudget;
        }

        /// <summary>
        /// Anantha
        /// </summary>
        /// <param name="dtMain"></param>
        /// <returns></returns>
        public DataTable GrandTotalforEntry(DataTable dtMain)
        {
            DataTable Dtbudget = new DataTable();

            Dtbudget.Columns.Add("gold_flag", typeof(string));        //1
            Dtbudget.Columns.Add("top_flag", typeof(string));         //2
            Dtbudget.Columns.Add("five_years_flag", typeof(string));   //3
            Dtbudget.Columns.Add("bb_flag", typeof(string));            //4
            Dtbudget.Columns.Add("SPC_flag", typeof(string));           //5
            Dtbudget.Columns.Add("ten_years_flag", typeof(string));

            Dtbudget.Columns.Add("item_id", typeof(string));               //6
            Dtbudget.Columns.Add("item_family_id", typeof(string));         //7
            Dtbudget.Columns.Add("item_family_name", typeof(string));           //8
            Dtbudget.Columns.Add("item_sub_family_id", typeof(string));        //9
            Dtbudget.Columns.Add("item_sub_family_name", typeof(string));      //10

            Dtbudget.Columns.Add("item_group_code", typeof(string));           //11
            Dtbudget.Columns.Add("insert_or_tool_flag", typeof(string));       //12
            Dtbudget.Columns.Add("item_code", typeof(string));               //13
            Dtbudget.Columns.Add("item_short_name", typeof(string));         //14
            Dtbudget.Columns.Add("item_description", typeof(string));          //15
            Dtbudget.Columns.Add("customer_number", typeof(string));             //16

            Dtbudget.Columns.Add("sales_qty_year_2", typeof(string));        //17
            Dtbudget.Columns.Add("sales_qty_year_1", typeof(string));         //18
            Dtbudget.Columns.Add("sales_qty_year_0", typeof(string));            //19
            Dtbudget.Columns.Add("estimate_qty_next_year", typeof(string));     //20

            Dtbudget.Columns.Add("sales_value_year_2", typeof(string));     //21
            Dtbudget.Columns.Add("sales_value_year_1", typeof(string));  //22
            Dtbudget.Columns.Add("sales_value_year_0", typeof(string));     //23
            Dtbudget.Columns.Add("estimate_value_next_year", typeof(string));    //24

            Dtbudget.Columns.Add("review_flag", typeof(string));    //25
            Dtbudget.Columns.Add("salesengineer_id", typeof(string));     //26
            Dtbudget.Columns.Add("status_sales_engineer", typeof(string));      //27
            Dtbudget.Columns.Add("status_branch_manager", typeof(string));    //28
            Dtbudget.Columns.Add("status_ho", typeof(string));                 //29

            Dtbudget.Columns.Add("sumFlag", typeof(string));      //30

            //quality variance
            Dtbudget.Columns.Add("QuantityVariance_sales_qty_year_2", typeof(string));     //31
            Dtbudget.Columns.Add("QuantityVariance_sales_qty_year_1", typeof(string));       //32
            Dtbudget.Columns.Add("QuantityVariance_sales_qty_year_0", typeof(string));      //33

            //Quality Percentage
            Dtbudget.Columns.Add("QuantityPercentage_sales_qty_year_2", typeof(string));    //34
            Dtbudget.Columns.Add("QuantityPercentage_sales_qty_year_1", typeof(string));     //35
            Dtbudget.Columns.Add("QuantityPercentage_sales_qty_year_0", typeof(string));    //36

            //Value Percentage
            Dtbudget.Columns.Add("ValuePercentage_sales_value_year_2", typeof(string));   //37
            Dtbudget.Columns.Add("ValuePercentage_sales_value_year_1", typeof(string));    //38
            Dtbudget.Columns.Add("ValuePercentage_sales_value_year_0", typeof(string));    //39

            //price per unit
            Dtbudget.Columns.Add("PricePerUnit_sales_value_year_2", typeof(string));    //40
            Dtbudget.Columns.Add("PricePerUnit_sales_value_year_1", typeof(string));    //41
            Dtbudget.Columns.Add("PricePerUnit_sales_value_year_0", typeof(string));    //42
            Dtbudget.Columns.Add("PricePerUnit_sales_value_year_P", typeof(string));     //43

            //price changes per year
            Dtbudget.Columns.Add("priceschange_sales_value_year_2", typeof(string));     //44
            Dtbudget.Columns.Add("priceschange_sales_value_year_1", typeof(string));     //45
            Dtbudget.Columns.Add("priceschange_sales_value_year_0", typeof(string));     //46

            Dtbudget.Columns.Add("budget_id", typeof(string));    //47
            Dtbudget.Columns.Add("estimate_rate_next_year", typeof(string));    //48
            Dtbudget.Rows.Add(
                                   dtMain.Rows[0].ItemArray[0].ToString(),//gold_flag
                                   dtMain.Rows[0].ItemArray[1].ToString(),//top_flag
                                   dtMain.Rows[0].ItemArray[2].ToString(),//five_years_flag
                                   dtMain.Rows[0].ItemArray[3].ToString(),//bb_flag
                                   dtMain.Rows[0].ItemArray[4].ToString(),//SPC_flag
                                   dtMain.Rows[0].ItemArray[5].ToString(),
                                   dtMain.Rows[0].ItemArray[6].ToString(),
                                   dtMain.Rows[0].ItemArray[7].ToString(),
                                   dtMain.Rows[0].ItemArray[8].ToString(),
                                   dtMain.Rows[0].ItemArray[9].ToString(),
                                   dtMain.Rows[0].ItemArray[10].ToString(),
                                   dtMain.Rows[0].ItemArray[11].ToString(),
                                   dtMain.Rows[0].ItemArray[12].ToString(),
                                   dtMain.Rows[0].ItemArray[13].ToString(),
                                   dtMain.Rows[0].ItemArray[14].ToString(),
                                   dtMain.Rows[0].ItemArray[15].ToString(),
                                   dtMain.Rows[0].ItemArray[16].ToString(),
                                   dtMain.Rows[0].ItemArray[17].ToString(),
                                   dtMain.Rows[0].ItemArray[18].ToString(),
                                   dtMain.Rows[0].ItemArray[19].ToString(),
                                   dtMain.Rows[0].ItemArray[20].ToString(),
                                   dtMain.Rows[0].ItemArray[21].ToString(),
                                   dtMain.Rows[0].ItemArray[22].ToString(),
                                   dtMain.Rows[0].ItemArray[23].ToString(),
                                   dtMain.Rows[0].ItemArray[24].ToString(),
                                   dtMain.Rows[0].ItemArray[25].ToString(),
                                   dtMain.Rows[0].ItemArray[26].ToString(),
                                   dtMain.Rows[0].ItemArray[27].ToString(),
                                   dtMain.Rows[0].ItemArray[28].ToString(),
                                   dtMain.Rows[0].ItemArray[29].ToString(),
                                   dtMain.Rows[0].ItemArray[30].ToString(),
                                   dtMain.Rows[0].ItemArray[31].ToString(),
                                   dtMain.Rows[0].ItemArray[32].ToString(),
                                   dtMain.Rows[0].ItemArray[33].ToString(),
                                   dtMain.Rows[0].ItemArray[34].ToString(),
                                   dtMain.Rows[0].ItemArray[35].ToString(),
                                   dtMain.Rows[0].ItemArray[36].ToString(),
                                   dtMain.Rows[0].ItemArray[37].ToString(),
                                   dtMain.Rows[0].ItemArray[38].ToString(),
                                   dtMain.Rows[0].ItemArray[39].ToString(),
                                   dtMain.Rows[0].ItemArray[40].ToString(),
                                   dtMain.Rows[0].ItemArray[41].ToString(),
                                   dtMain.Rows[0].ItemArray[42].ToString(),
                                   dtMain.Rows[0].ItemArray[43].ToString(),
                                   dtMain.Rows[0].ItemArray[44].ToString(),
                                   dtMain.Rows[0].ItemArray[45].ToString(),
                                   dtMain.Rows[0].ItemArray[46].ToString(),
                                   dtMain.Rows[0].ItemArray[47].ToString(),
                                    dtMain.Rows[0].ItemArray[48].ToString()
                                   );

            int ji = dtMain.Rows.Count;

            Dtbudget.Rows.Add(
                                  dtMain.Rows[ji - 1].ItemArray[0].ToString(),//gold_flag
                                  dtMain.Rows[ji - 1].ItemArray[1].ToString(),//top_flag
                                  dtMain.Rows[ji - 1].ItemArray[2].ToString(),//five_years_flag
                                  dtMain.Rows[ji - 1].ItemArray[3].ToString(),//bb_flag
                                  dtMain.Rows[ji - 1].ItemArray[4].ToString(),//SPC_flag
                                  dtMain.Rows[ji - 1].ItemArray[5].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[6].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[7].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[8].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[9].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[10].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[11].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[12].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[13].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[14].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[15].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[16].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[17].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[18].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[19].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[20].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[21].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[22].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[23].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[24].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[25].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[26].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[27].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[28].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[29].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[30].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[31].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[32].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[33].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[34].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[35].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[36].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[37].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[38].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[39].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[40].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[41].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[42].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[43].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[44].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[45].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[46].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[47].ToString(),
                                  dtMain.Rows[ji - 1].ItemArray[48].ToString()
                                  );
            for (int j = 1; j < dtMain.Rows.Count; j++)
            {
                Dtbudget.Rows.Add(
                                 dtMain.Rows[j].ItemArray[0].ToString(),//gold_flag
                                 dtMain.Rows[j].ItemArray[1].ToString(),//top_flag
                                 dtMain.Rows[j].ItemArray[2].ToString(),//five_years_flag
                                 dtMain.Rows[j].ItemArray[3].ToString(),//bb_flag
                                 dtMain.Rows[j].ItemArray[4].ToString(),//SPC_flag
                                 dtMain.Rows[j].ItemArray[5].ToString(),
                                 dtMain.Rows[j].ItemArray[6].ToString(),
                                 dtMain.Rows[j].ItemArray[7].ToString(),
                                 dtMain.Rows[j].ItemArray[8].ToString(),
                                 dtMain.Rows[j].ItemArray[9].ToString(),
                                 dtMain.Rows[j].ItemArray[10].ToString(),
                                 dtMain.Rows[j].ItemArray[11].ToString(),
                                 dtMain.Rows[j].ItemArray[12].ToString(),
                                 dtMain.Rows[j].ItemArray[13].ToString(),
                                 dtMain.Rows[j].ItemArray[14].ToString(),
                                 dtMain.Rows[j].ItemArray[15].ToString(),
                                 dtMain.Rows[j].ItemArray[16].ToString(),
                                 dtMain.Rows[j].ItemArray[17].ToString(),
                                 dtMain.Rows[j].ItemArray[18].ToString(),
                                 dtMain.Rows[j].ItemArray[19].ToString(),
                                 dtMain.Rows[j].ItemArray[20].ToString(),
                                 dtMain.Rows[j].ItemArray[21].ToString(),
                                 dtMain.Rows[j].ItemArray[22].ToString(),
                                 dtMain.Rows[j].ItemArray[23].ToString(),
                                 dtMain.Rows[j].ItemArray[24].ToString(),
                                 dtMain.Rows[j].ItemArray[25].ToString(),
                                 dtMain.Rows[j].ItemArray[26].ToString(),
                                 dtMain.Rows[j].ItemArray[27].ToString(),
                                 dtMain.Rows[j].ItemArray[28].ToString(),
                                 dtMain.Rows[j].ItemArray[29].ToString(),
                                 dtMain.Rows[j].ItemArray[30].ToString(),
                                 dtMain.Rows[j].ItemArray[31].ToString(),
                                 dtMain.Rows[j].ItemArray[32].ToString(),
                                 dtMain.Rows[j].ItemArray[33].ToString(),
                                 dtMain.Rows[j].ItemArray[34].ToString(),
                                 dtMain.Rows[j].ItemArray[35].ToString(),
                                 dtMain.Rows[j].ItemArray[36].ToString(),
                                 dtMain.Rows[j].ItemArray[37].ToString(),
                                 dtMain.Rows[j].ItemArray[38].ToString(),
                                 dtMain.Rows[j].ItemArray[39].ToString(),
                                 dtMain.Rows[j].ItemArray[40].ToString(),
                                 dtMain.Rows[j].ItemArray[41].ToString(),
                                 dtMain.Rows[j].ItemArray[42].ToString(),
                                 dtMain.Rows[j].ItemArray[43].ToString(),
                                 dtMain.Rows[j].ItemArray[44].ToString(),
                                 dtMain.Rows[j].ItemArray[45].ToString(),
                                 dtMain.Rows[j].ItemArray[46].ToString(),
                                 dtMain.Rows[j].ItemArray[47].ToString(),
                                 dtMain.Rows[j].ItemArray[48].ToString()
                                 );
            }
            return Dtbudget;
        }

        /// <summary>
        /// Anantha
        /// </summary>
        /// <param name="projectReport"></param>
        /// <returns></returns>
        public DataTable GrandtotalforMdpReports(DataTable projectReport)
        {
            DataTable DtProject = new DataTable();

            DtProject.Columns.Add("ID", typeof(string));             //0
            DtProject.Columns.Add("customer_num", typeof(string));    //1
            DtProject.Columns.Add("customer_name", typeof(string));    //2
            DtProject.Columns.Add("industry_lbl", typeof(string));    //3
            DtProject.Columns.Add("overall_pot_lbl", typeof(string));    //4
           // DtProject.Columns.Add("potential_lakhs_lbl", typeof(string));    //5
            DtProject.Columns.Add("potential_lakhs_lbl", typeof(decimal));  
            DtProject.Columns.Add("existing_product_lbl", typeof(string));    //6
            DtProject.Columns.Add("component_lbl", typeof(string));    //7
            DtProject.Columns.Add("Stage_Completed", typeof(string));    //8
            DtProject.Columns.Add("NoOfStages", typeof(string));    //9
            DtProject.Columns.Add("txt_remarks", typeof(string));    //10
            DtProject.Columns.Add("IsCanceled", typeof(string));    //11
            DtProject.Columns.Add("projects", typeof(string));    //12
            DtProject.Columns.Add("project_type", typeof(string));    //12
            DtProject.Columns.Add("project_num", typeof(string));    //13
            DtProject.Columns.Add("project_status", typeof(string));    //14
           // DtProject.Columns.Add("monthly_exp_val", typeof(string));    //15
            DtProject.Columns.Add("monthly_exp_val", typeof(decimal)); 
            DtProject.Columns.Add("assigned_salesengineer_id", typeof(string));    //16
            DtProject.Columns.Add("Reviewer", typeof(string));    //17
            DtProject.Columns.Add("Escalated_To", typeof(string));    //18
            DtProject.Columns.Add("sub_industry_lbl", typeof(string));    //19
            DtProject.Columns.Add("distributor_num", typeof(string));    //20
            DtProject.Columns.Add("creation_date_lbl", typeof(string));    //21
            DtProject.Columns.Add("Engineer_Name", typeof(string));    //22
           // DtProject.Columns.Add("Business_Expected", typeof(string));    //23
            DtProject.Columns.Add("Business_Expected", typeof(decimal));
            DtProject.Columns.Add("customer_lbl", typeof(string));    //24
            DtProject.Columns.Add("distributor_lbl", typeof(string));    //25
            DtProject.Columns.Add("status_lbl", typeof(string));    //26
            DtProject.Columns.Add("Percentage", typeof(string));    //27
            DtProject.Columns.Add("red", typeof(string));    //28
            DtProject.Columns.Add("yellow", typeof(string));    //29
            DtProject.Columns.Add("green", typeof(string));    //30
            DtProject.Columns.Add("project_title", typeof(string));    //31
            DtProject.Columns.Add("cancel", typeof(string));    //32         


            int ji = projectReport.Rows.Count;

            DtProject.Rows.Add(
                                  projectReport.Rows[ji - 1].ItemArray[0].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[1].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[2].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[3].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[4].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[5].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[6].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[7].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[8].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[9].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[10].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[11].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[12].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[13].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[14].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[15].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[16].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[17].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[18].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[19].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[20].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[21].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[22].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[23].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[24].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[25].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[26].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[27].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[28].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[29].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[30].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[31].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[32].ToString(),
                                  projectReport.Rows[ji - 1].ItemArray[33].ToString()
                                  );
            for (int j = 0; j < projectReport.Rows.Count; j++)
            {
                DtProject.Rows.Add(
                                 projectReport.Rows[j].ItemArray[0].ToString(),
                                 projectReport.Rows[j].ItemArray[1].ToString(),
                                 projectReport.Rows[j].ItemArray[2].ToString(),
                                 projectReport.Rows[j].ItemArray[3].ToString(),
                                 projectReport.Rows[j].ItemArray[4].ToString(),
                                 projectReport.Rows[j].ItemArray[5].ToString(),
                                 projectReport.Rows[j].ItemArray[6].ToString(),
                                 projectReport.Rows[j].ItemArray[7].ToString(),
                                 projectReport.Rows[j].ItemArray[8].ToString(),
                                 projectReport.Rows[j].ItemArray[9].ToString(),
                                 projectReport.Rows[j].ItemArray[10].ToString(),
                                 projectReport.Rows[j].ItemArray[11].ToString(),
                                 projectReport.Rows[j].ItemArray[12].ToString(),
                                 projectReport.Rows[j].ItemArray[13].ToString(),
                                 projectReport.Rows[j].ItemArray[14].ToString(),
                                 projectReport.Rows[j].ItemArray[15].ToString(),
                                 projectReport.Rows[j].ItemArray[16].ToString(),
                                 projectReport.Rows[j].ItemArray[17].ToString(),
                                 projectReport.Rows[j].ItemArray[18].ToString(),
                                 projectReport.Rows[j].ItemArray[19].ToString(),
                                 projectReport.Rows[j].ItemArray[20].ToString(),
                                 projectReport.Rows[j].ItemArray[21].ToString(),
                                 projectReport.Rows[j].ItemArray[22].ToString(),
                                 projectReport.Rows[j].ItemArray[23].ToString(),
                                 projectReport.Rows[j].ItemArray[24].ToString(),
                                 projectReport.Rows[j].ItemArray[25].ToString(),
                                 projectReport.Rows[j].ItemArray[26].ToString(),
                                 projectReport.Rows[j].ItemArray[27].ToString(),
                                 projectReport.Rows[j].ItemArray[28].ToString(),
                                 projectReport.Rows[j].ItemArray[29].ToString(),
                                 projectReport.Rows[j].ItemArray[30].ToString(),
                                 projectReport.Rows[j].ItemArray[31].ToString(),
                                 projectReport.Rows[j].ItemArray[32].ToString(),
                                 projectReport.Rows[j].ItemArray[33].ToString()
                                 );
            }


            DtProject.Rows[DtProject.Rows.Count - 1].Delete();
            return DtProject;




        }

        /// <summary>
        /// Author : Anamika 
        /// Date : Nov 30, 2016
        /// Desc : Calling a fuction to call data from stored procedure
        /// </summary>
        /// <param name="Region"></param>
        /// <param name="SalesEngineer"></param>
        /// <param name="CustomerType"></param>
        /// <param name="customerId"></param>
        /// <param name="cter"></param>
        /// <param name="valueF"></param>
        /// <returns></returns>
        internal DataTable LoadBudget(string Region, string SalesEngineer, string CustomerType, string customerId, string cter=null, int valueF = 1000)
        {
            DataTable dtGrandMain=new DataTable();
            try
            {
                dtGrandMain = objBudget.getGrandMain(Region, SalesEngineer, CustomerType, customerId, cter, valueF);
            }
            catch (Exception ex)
            {
                LogError(ex);
            }
            
            return dtGrandMain;
        }

        /// <summary>
        /// Author : Anamika
        /// Date : Dec 2, 2016
        /// Desc : Calling a fuction to call budget entry data from stored procedure
        /// </summary>
        /// <param name="customerNumber"></param>
        /// <returns></returns>
        internal DataTable LoadBudget(string customerNumber)
        {
            DataTable dtGrandMain = new DataTable();
            try
            {
                dtGrandMain = objBudget.getEntryMain(customerNumber);
            }
            catch (Exception ex)
            {
                LogError(ex);
            }

            return dtGrandMain;
        }

    }
}