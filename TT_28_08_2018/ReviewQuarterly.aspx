﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReviewQuarterly.aspx.cs" Inherits="TaegutecSalesBudget.ReviewQuarterly" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="act" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">


    <script type="text/javascript" src="js/Chart.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <link href="css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="css/buttons.dataTables.min.css" rel="stylesheet" />
    
      <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/funnel.js"></script>
    <script src="https://www.amcharts.com/lib/3/gauge.js"></script>
    <script src="https://www.amcharts.com/lib/3/radar.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/pie.js"></script>
    <script src="https://www.amcharts.com/lib/3/plugins/dataloader/dataloader.min.js"></script>
    <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
 

    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>



    <script src="js/jquery.sumoselect.min.js"></script>
    <link href="css/sumoselect.css" rel="stylesheet" />
    <style type="text/css">




        .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
            border: 1px solid #ddd;
            padding: 5px;
        }

        .HeadergridAll {
            background: #ebeef5;
            color: black;
            font-weight: 600;
            border-color: #ebeef5;
            border-top-color: #ebeef5;
            text-align: center !important;
        }

        #MainContent_grdviewAllValues td:first-child {
            text-align: left;
        }

        #MainContent_grdviewAllQuantites td:first-child {
            text-align: left;
        }

        td {
            background: #fff;
            text-align: left;
            padding: 5px;
            border-bottom: solid 1px #ddd;
        }

        th {
            padding: 10px;
        }

        table {
            width: 100%;
        }

        .form-group input[type="radio"], .form-group input[type="checkbox"] {
            margin: 4px 4px 0;
        }

        .form-group label {
            vertical-align: middle;
        }

        .form-group table {
            border-top: solid 1px #ddd;
        }

        .control_dropdown {
            width: 180px;
            height: 30px;
            border-radius: 4px!important;
        }

        .label {
            padding-top: 9px;
            width: 100%;
            color: black;
        }

        .control {
            padding-top: 2px;
        }

        .SelectBox 
        {
             padding-top: 9px;
             width: 180px!important;
             border-radius: 4px!important;
        }

        .btn.green {
            margin-top: 15px;
        }
        
        .amcharts-chart-div a {
            display: none !important;
        }

        .legend-title {
          font-family: Verdana;
          font-weight: bold;
          text-align:center
        }
           #amGraphQuaterly {
          width: 100%;
          height: 295px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <!-- End : Breadcrumbs -->
    <act:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="360" EnableHistory="true" EnablePartialRendering="true" EnablePageMethods="true">
    </act:ToolkitScriptManager>
    <%-- <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360" EnableHistory="true" EnablePartialRendering="true" EnablePageMethods="true">
    </asp:ScriptManager>--%>


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="crumbs">
                <!-- Start : Breadcrumbs -->
                <ul id="breadcrumbs" class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a>Reports</a>
                    </li>
                    <li>Reviews</li>
                    <li class="current">Quarterly Review</li>
                    <div>
                        <ul>
                            <li class="title_bedcrum" style="list-style: none;">QUARTERLY REVIEW</li>
                        </ul>
                    </div>
                    <div>
                        <ul style="float: right; list-style: none; margin-top: -4px; width: 265px; margin-right: -5px;" class="alert alert-danger fade in">
                            <li>
                                <span style="margin-right: 4px; vertical-align: text-bottom;">Val In '000</span>
                                <asp:RadioButton ID="rbtn_Thousand" OnCheckedChanged="Thousand_CheckedChanged" AutoPostBack="true" GroupName="customer" runat="server" Checked="True" />
                                <span style="margin-right: 4px; margin-left: 4px; vertical-align: text-bottom;">Val In Lakhs</span>
                                <asp:RadioButton ID="rbtn_Lakhs" OnCheckedChanged="Lakhs_CheckedChanged" AutoPostBack="true" GroupName="customer" runat="server" />
                            </li>
                        </ul>
                    </div>
                </ul>
            </div>
            <div id="collapsebtn" class="row">
                <img id="product_image" src="images/up_arrow.png" align="left" style="margin-left: 46%;" />
            </div>
            <div class="row filter_panel" id="reportdrpdwns" runat="server">
                <div runat="server" id="cterDiv" visible="false">
                    <ul class="btn-info rbtn_panel">
                        <li><span style="margin-right: 4px; vertical-align: text-bottom;">TAEGUTEC</span>
                            <asp:RadioButton ID="rdBtnTaegutec" AutoPostBack="true" Checked="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                            <span style="margin-right: 4px; margin-left: 4px; vertical-align: text-bottom;">DURACARB</span>
                            <asp:RadioButton ID="rdBtnDuraCab" AutoPostBack="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                        </li>
                    </ul>
                </div>

                <div class="col-md-2 control" runat="server" id="divBranch">
                    <label class="label">BRANCH </label>
                    <asp:ListBox runat="server" CssClass="control_dropdown" ID="BranchList" SelectionMode="Multiple" OnSelectedIndexChanged="BranchList_SelectedIndexChanged" AutoPostBack="true"></asp:ListBox>
                    <%--                        
                    <asp:TextBox ID="txtbranchlist" runat="server" AutoPostBack="True" CssClass="control_dropdown" onkeydown="return false;"></asp:TextBox>
                    <act:PopupControlExtender ID="PopupControlExtender1" runat="server"
                        Enabled="True" ExtenderControlID="" TargetControlID="txtbranchlist" PopupControlID="panelbranch"
                        OffsetY="32">
                    </act:PopupControlExtender>
                    <asp:Panel ID="panelbranch" runat="server" Height="150px" Width="230px" BorderStyle="Solid" BorderColor="gray"
                        BorderWidth="1px" Direction="NotSet" ScrollBars="Auto" BackColor="white"
                        Style="display: none">
                        <asp:CheckBox ID="cbAll" runat="server" CssClass="mn_all" AutoPostBack="true" Checked="true" Text="ALL" OnCheckedChanged="cbAll_CheckedChanged" />
                        <asp:CheckBoxList ID="ChkBranches" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlBranchList_SelectedIndexChanged">
                        </asp:CheckBoxList>
                    </asp:Panel>--%>
                </div>

                <div class="col-md-2 control" runat="server" id="divSE">
                    <label class="label">SALES ENGINEER </label>
                    <asp:ListBox runat="server" CssClass="control_dropdown" ID="SalesEngList" SelectionMode="Multiple" OnSelectedIndexChanged="SalesEngList_SelectedIndexChanged" AutoPostBack="true"></asp:ListBox>
                </div>

                <div class="col-md-2 control">
                    <label class="label">CUSTOMER TYPE </label>
                    <asp:DropDownList ID="ddlcustomertype" runat="server" AutoPostBack="True" OnSelectedIndexChanged="customertype_SelectedIndexChanged"
                        CssClass="control_dropdown">
                        <asp:ListItem Text="ALL" Value="ALL" />
                        <asp:ListItem Text="CUSTOMER" Value="C" />
                        <asp:ListItem Text="CHANNEL PARTNER" Value="D" />
                    </asp:DropDownList>
                </div>

                <div class="col-md-2 control">
                    <label class="label">CUSTOMER NAME </label>
                   <asp:ListBox runat="server" CssClass="control_dropdown" ID="CustNameList" SelectionMode="Multiple" OnSelectedIndexChanged="CustNameList_SelectedIndexChanged" AutoPostBack="true" class="search-txt"></asp:ListBox>
                </div>

                <div class="col-md-2 control ">
                    <label class="label ">CUSTOMER NUMBER</label>
                   <asp:ListBox runat="server" CssClass="control_dropdown" ID="CustNumList" SelectionMode="Multiple" OnSelectedIndexChanged="CustNumList_SelectedIndexChanged" AutoPostBack="true" class="search-txt"></asp:ListBox>
                </div>

                <div class="col-md-2 control">
                    <label class="label ">PRODUCT GROUP</label>
                    <asp:ListBox runat="server" CssClass="control_dropdown" ID="ProductGrpList" SelectionMode="Multiple" OnSelectedIndexChanged="ProductGrpList_SelectedIndexChanged" AutoPostBack="true">
                     </asp:ListBox>
                </div>

                <div class="col-md-2 control ">
                    <label class="label ">PRODUCT FAMILY</label>
                
                    <asp:ListBox runat="server" CssClass="control_dropdown" ID="ProductFamilyList" SelectionMode="Multiple" OnSelectedIndexChanged="ProductFamilyList_SelectedIndexChanged" AutoPostBack="true"></asp:ListBox>
                </div>

                <div class="col-md-2 control ">
                    <label class="label ">APPLICATION</label>
                    <%-- <asp:DropDownList ID="ddlApplication" runat="server" CssClass="form-control" Width="230px">
                                <asp:ListItem>SELECT Application</asp:ListItem>
                            </asp:DropDownList>--%>
                     <asp:ListBox runat="server" CssClass="control_dropdown" ID="ApplicationList" SelectionMode="Multiple" OnSelectedIndexChanged="ApplicationList_SelectedIndexChanged" AutoPostBack="true"></asp:ListBox>
                </div>

                <div class="col-md-4 ">
                    <div class="form-group">
                        <div class="col-md-4">
                            <asp:Button ID="reports" runat="server" Text="FILTER" OnClick="reports_Click" CssClass="btn green" />
                        </div>
                    </div>
                </div>
            </div>

            <br />
            <br />
            <div class="row" runat="server" id="divGridGraph" visible="false">
                <div class="portlet-body shadow" style="background: #f1f1f1;">
                    <div class="col-md-12 ">
                        <div style="float: left; padding-right: 10px; padding-left: 10px; width: 50%;" class="col-md-6 tabe ">
                            <asp:GridView ID="grdviewAllValues" runat="server" ViewStateMode="Enabled" class="table table-bordered " AutoGenerateColumns="False" ShowHeader="false" Width="100%">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblTitle" Text='<%# Eval("title") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lbljan" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("quarter1").ToString() == "0")? "NA" : Eval("quarter1") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblfeb" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("quarter2").ToString() == "0")? "NA" : Eval("quarter2") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblmar" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("quarter3").ToString() == "0")? "NA" : Eval("quarter3") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lbldec" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("quarter4").ToString() == "0")? "NA" : Eval("quarter4") %>'></asp:Label>
                                            <asp:Label runat="server" ID="lblFlag" Text='<%# Eval("flag") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                        <div style="float: left; padding-right: 10px; padding-left: 10px;" class="col-md-6">
                             <div id="amGraphQuaterly"></div>
                          <%-- <asp:Chart ID="Chart2" runat="server" Width="600px" Visible="False" ViewStateMode="Enabled" Style="max-width: 100% !important" BackColor="#E1E1E1">
                                <Titles>
                                    <asp:Title Text="Summary of Sales Budget Quarterly "></asp:Title>
                                </Titles>
                                <Legends>
                                    <asp:Legend Alignment="Center" Docking="Top" IsTextAutoFit="true" Name="Legend2" LegendStyle="Row" />
                                </Legends>--%>
                                <%-- <Series>
                       <asp:Series Name="YTD SALE" ShadowOffset="1" Enabled="True" LabelForeColor="White"  LabelAngle="-90" 
                           CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true"   ChartType="Column"></asp:Series>
                       <asp:Series Name="YTD PLAN" ShadowOffset="1" ChartType="Line"></asp:Series>
                       <asp:Series Name="Series4" IsValueShownAsLabel="True" ShadowOffset="1" ChartType="Point"></asp:Series>
                       <asp:Series Name="YTD SALE PREVIOUS YEAR" ShadowOffset="1" ChartType="Line"></asp:Series>
                       <asp:Series Name="Series5" IsValueShownAsLabel="True" ShadowOffset="1" ChartType="Point"></asp:Series>
                   </Series>    --%>

                              <%-- <ChartAreas>
                                    <asp:ChartArea Name="ChartArea1" BackColor="#ACD1E9">
                                        <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap">
                                            <MajorGrid LineWidth="0" />
                                            <LabelStyle Font="Verdana, 8.25pt" />
                                        </AxisX>
                                        <AxisY>
                                            <MajorGrid LineWidth="0" />
                                        </AxisY>
                                    </asp:ChartArea>
                                </ChartAreas>
                                <BorderSkin BackColor="Transparent" PageColor="Transparent"
                                    SkinStyle="Emboss" />
                            </asp:Chart> --%>  

                        </div>
                    </div>
                </div>
            </div>

            <br />
            <br />
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="BranchList" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="SalesEngList" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlcustomertype" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ProductFamilyList" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="reports" EventName="Click" />

            <%-- <asp:AsyncPostBackTrigger ControlID="rbtn_Thousand" EventName="OnCheckedChanged"/>
           <asp:AsyncPostBackTrigger ControlID="rbtn_Lakhs" EventName="OnCheckedChanged"/>--%>
        </Triggers>
    </asp:UpdatePanel>

    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">

                <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff">Please wait</span>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>

    <style>
        .newa1 {
            background-color: #EBF4FA;
            -webkit-box-shadow: 0px 1px 10px rgba(10, 10, 10, 0.25);
            -moz-box-shadow: 0px 1px 10px rgba(10, 10, 10, 0.25);
            box-shadow: 0px 1px 10px rgba(10, 10, 10, 0.25);
            padding: 12px;
            width: 100%;
            margin-bottom: 10px;
            float: left;
            text-align: center;
        }

        .portlet.box.new > .portlet-title {
            background-color: #667F67;
        }

        #MainContent_grdviewAllValues td {
            text-align: right;
            height: 20px;
        }


         .SelectClass,
        .SumoUnder {
          position: absolute;
          top: 0;
          left: 0;
          right: 0;
          height: 100%;
          width: 100%;
          border: none;
          -webkit-box-sizing: border-box;
          -moz-box-sizing: border-box;
          box-sizing: border-box;
          -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
          filter: alpha(opacity=0);
          -moz-opacity: 0;
          -khtml-opacity: 0;
          opacity: 0;
        }

        .SelectClass {
          z-index: 1;
        }

        .SumoSelect > .optWrapper > .options li.opt label,
        .SumoSelect > .CaptionCont,
        .SumoSelect .select-all > label {
          user-select: none;
          -o-user-select: none;
          -moz-user-select: none;
          -khtml-user-select: none;
          -webkit-user-select: none;
          overflow-wrap:normal;
        }

        .SumoSelect {
          display:inline-block;
          position: relative;
          outline: none;
        }

        .SumoSelect:focus > .CaptionCont,
        .SumoSelect:hover > .CaptionCont,
        .SumoSelect.open > .CaptionCont {
          box-shadow: 0 0 2px #7799D0;
          border-color: #7799D0;
        }

        .SumoSelect > .CaptionCont {
          position: relative;
          border: 1px solid #A4A4A4;
          min-height: 14px;
          background-color: #fff;
          border-radius: 2px;
          margin: 0;
        }

        .SumoSelect > .CaptionCont > span {
          display: block;
          padding-right: 30px;
          text-overflow: ellipsis;
          white-space: nowrap;
          overflow: hidden;
          cursor: default;
         /* margin-left:65px;*/
        }


        /*placeholder style*/

        .SumoSelect > .CaptionCont > span.placeholder {
          color: #ccc;
          font-style: italic;
        }

        .SumoSelect > .CaptionCont > label {
          position: absolute;
          top: 0;
          right: 0;
          bottom: 0;
          width: 30px;
        }

        .SumoSelect > .CaptionCont > label > i {
          background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAANCAYAAABy6+R8AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3wMdBhAJ/fwnjwAAAGFJREFUKM9jYBh+gBFKuzEwMKQwMDB8xaOWlYGB4T4DA0MrsuapDAwM//HgNwwMDDbYTJuGQ8MHBgYGJ1xOYGNgYJiBpuEpAwODHSF/siDZ+ISBgcGClEDqZ2Bg8B6CkQsAPRga0cpRtDEAAAAASUVORK5CYII=');
          background-position: center center;
          width: 16px;
          height: 16px;
          display: block;
          position: absolute;
          top: 0;
          left: 0;
          right: 0;
          bottom: 0;
          margin: auto;
          background-repeat: no-repeat;
          opacity: 0.8;
        }

        .SumoSelect > .optWrapper {
          display: none;
          z-index: 1000;
          top: 30px;
          width: 100%;
 
          position: absolute;
          left: 0;
          -webkit-box-sizing: border-box;
          -moz-box-sizing: border-box;
          box-sizing: border-box;
          background: #fff;
          border: 1px solid #ddd;
          box-shadow: 2px 3px 3px rgba(0, 0, 0, 0.11);
          border-radius: 3px;
          overflow: visible;
          padding-bottom:0px;
          padding-right:0px;
          padding-left:0px;
          padding-top:0px;
        }

        .SumoSelect.open > .optWrapper {
          top: 35px;
          display: block;

        }

        .SumoSelect.open > .optWrapper.up {
          top: auto;
          bottom: 100%;
          margin-bottom: 5px;
        }

        .SumoSelect > .optWrapper ul {
          list-style: none;
          display: block;
          padding: 0;
          margin: 0;
          overflow: auto;
        }

        .SumoSelect > .optWrapper > .options {
          border-radius: 2px;
          position: relative;
          /*Set the height of pop up here (only for desktop mode)*/
         max-height: 250px;
          /*height*/
        }

        .SumoSelect > .optWrapper > .options li.group.disabled > label {
          opacity: 0.5;
        }

        .SumoSelect > .optWrapper > .options li ul li.opt {
          padding-left: 22px;
        }

        .SumoSelect > .optWrapper.multiple > .options li ul li.opt {
          padding-left: 50px;
        }

        .SumoSelect > .optWrapper.isFloating > .options {
          max-height: 100%;
          box-shadow: 0 0 100px #595959;
        }

        .SumoSelect > .optWrapper > .options li.opt {
          padding: 6px 6px;
          position: relative;
          border-bottom: 1px solid #f5f5f5;
        }

        .SumoSelect > .optWrapper > .options > li.opt:first-child {
          border-radius: 2px 2px 0 0;
        }

        .SumoSelect > .optWrapper > .options > li.opt:last-child {
          border-radius: 0 0 2px 2px;
          border-bottom: none;
        }

        .SumoSelect > .optWrapper > .options li.opt:hover {
          background-color: #E4E4E4;
        }

        .SumoSelect > .optWrapper > .options li.opt.sel {
          background-color: #a1c0e4;
          border-bottom: 1px solid #a1c0e4;
        }

        .SumoSelect > .optWrapper > .options li label {
          text-overflow: ellipsis;
          white-space: nowrap;
          overflow: hidden;
          display: block;
          cursor: pointer;
        }

        .SumoSelect > .optWrapper > .options li span {
          display: none;
        }

        .SumoSelect > .optWrapper > .options li.group > label {
          cursor: default;
          padding: 8px 6px;
          font-weight: bold;
        }


        /*Floating styles*/

        .SumoSelect > .optWrapper.isFloating {
          position: fixed;
          top: 0;
          left: 0;
          right: 0;
          width: 90%;
          bottom: 0;
          margin: auto;
          max-height: 90%;
        }


        /*disabled state*/

        .SumoSelect > .optWrapper > .options li.opt.disabled {
          background-color: inherit;
          pointer-events: none;
        }

        .SumoSelect > .optWrapper > .options li.opt.disabled * {
          -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=50)";
          /* IE 5-7 */
          filter: alpha(opacity=50);
          /* Netscape */
          -moz-opacity: 0.5;
          /* Safari 1.x */
          -khtml-opacity: 0.5;
          /* Good browsers */
          opacity: 0.5;
        }


        /*styling for multiple select*/

        .SumoSelect > .optWrapper.multiple > .options li.opt {
          padding-left: 35px;
          cursor: pointer;
        }
        
        .multiple {
            padding: 8px 10px;
          /*  height: 300px !important;*/
            font-size: 12px;
            border: 1px solid #dadada;
        }
        .SumoSelect > .optWrapper.multiple > .options li.opt span,
        .SumoSelect .select-all > span {
          position: absolute;
          display: block;
          width: 30px;
          top: 0;
          bottom: 0;
          margin-top:5px;

        }

        .SumoSelect > .optWrapper.multiple > .options li.opt span i,
        .SumoSelect .select-all > span i {
          position: absolute;
          margin: auto;
          left: 0;
          right: 0;
          top: 0;
          bottom: 0;
          width: 14px;
          height: 14px;
          border: 1px solid #AEAEAE;
          border-radius: 2px;
          box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.15);
          background-color: #fff;
        }

        .SumoSelect > .optWrapper > .MultiControls {
          display: inline;
          border-top: 1px solid #ddd;
          background-color: #fff;
          box-shadow: 0 0 2px rgba(0, 0, 0, 0.13);
          border-radius: 0 0 3px 3px;
        }

        .SumoSelect > .optWrapper.multiple.isFloating > .MultiControls {
          display: block;
          margin-top: 5px;
          position: absolute;
          bottom: 0;
          width: 100%;
  
        }


        .SumoSelect > .optWrapper.multiple.okCancelInMulti > .MultiControls {
          display: block;

 
        }

        .SumoSelect > .optWrapper.multiple.okCancelInMulti > .MultiControls > p {
         /* padding: 6px;*/
        }

        .SumoSelect > .optWrapper.multiple > .MultiControls > p {
          display: inline-block;
          cursor: pointer;
          /*padding: 12px;*/
          width: 50%;
          box-sizing: border-box;
          text-align: center;
        }

        .SumoSelect > .optWrapper.multiple > .MultiControls > p:hover {
          background-color: #f1f1f1;
          margin-bottom:10px;
        }

        .SumoSelect > .optWrapper.multiple > .MultiControls > p.btnOk {
          border-right: 1px solid #DBDBDB;
          border-radius: 0 0 0 3px;
        }

        .SumoSelect > .optWrapper.multiple > .MultiControls > p.btnCancel {
          border-radius: 0 0 3px 0;
         /* margin-bottom:5px;*/
        }


        /*styling for select on popup mode*/

        .SumoSelect > .optWrapper.isFloating > .options li.opt {
          padding: 12px 6px;
        }


        /*styling for only multiple select on popup mode*/

        .SumoSelect > .optWrapper.multiple.isFloating > .options li.opt {
          padding-left: 35px;
        }

        .SumoSelect > .optWrapper.multiple.isFloating {
        /*padding-bottom: 43px;*/
        }

        .SumoSelect > .optWrapper.multiple > .options li.opt.selected span i,
        .SumoSelect .select-all.selected > span i,
        .SumoSelect .select-all.partial > span i {
          background-color: rgb(5, 130, 183);
          box-shadow: none;
          border-color: transparent;
          background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAGCAYAAAD+Bd/7AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABx0RVh0U29mdHdhcmUAQWRvYmUgRmlyZXdvcmtzIENTNXG14zYAAABMSURBVAiZfc0xDkAAFIPhd2Kr1WRjcAExuIgzGUTIZ/AkImjSofnbNBAfHvzAHjOKNzhiQ42IDFXCDivaaxAJd0xYshT3QqBxqnxeHvhunpu23xnmAAAAAElFTkSuQmCC');
          background-repeat: no-repeat;
          background-position: center center;
          padding:0px;
          margin-top:0px;
        }


        /*disabled state*/

        .SumoSelect.disabled {
          opacity: 0.7;
          cursor: not-allowed;
        }

        .SumoSelect.disabled > .CaptionCont {
          border-color: #ccc;
          box-shadow: none;
        }

        /**Select all button**/

        .SumoSelect .select-all {
          border-radius: 3px 3px 0 0;
          position: relative;
          border-bottom: 1px solid #ddd;
          background-color: #fff;
          padding: 8px 0 3px 35px;
         height: 30px;
          cursor: pointer;
        }

        .SumoSelect .select-all > label,
        .SumoSelect .select-all > span i {
          cursor: pointer;
        }

        .SumoSelect .select-all.partial > span i {
          background-color: #ccc;
        }

        /*styling for optgroups*/

        .SumoSelect > .optWrapper > .options li.optGroup {
          padding-left: 5px;
          text-decoration: underline;
        } 
    </style>
    <script>
        $(document).ready(function () {
       //     amchartquaterly();
            triggerPostGridLodedActions();
        });
        function bindGridView() {
            var head_content = $('#MainContent_grdviewAllValues tr:first').html();
            $('#MainContent_grdviewAllValues').prepend('<thead></thead>')
            $('#MainContent_grdviewAllValues thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_grdviewAllValues tbody tr:first').hide();
            $('#MainContent_grdviewAllValues').DataTable(
                     {
                         "info": false
                     });
        }

        function triggerPostGridLodedActions() {
            bindGridView();
            amchartquaterly();
            var RoleID = '<%=Session["RoleId"].ToString()%>';
            if (RoleID == "HO" || RoleID == "TM") {
                $(<%=BranchList.ClientID%>).SumoSelect({ selectAll: true, okCancelInMulti: true, search: true });
                $(<%=SalesEngList.ClientID%>).SumoSelect({ selectAll: true, okCancelInMulti: true, search: true });
                $(<%=CustNameList.ClientID%>).SumoSelect({ selectAll: true, okCancelInMulti: true, search: true });
                $(<%=CustNumList.ClientID%>).SumoSelect({ selectAll: true, okCancelInMulti: true, search: true });
            }

            if (RoleID == "BM") {
                $(<%=SalesEngList.ClientID%>).SumoSelect({ selectAll: true, okCancelInMulti: true });
                    $(<%=CustNameList.ClientID%>).SumoSelect({ selectAll: true, okCancelInMulti: true });
                    $(<%=CustNumList.ClientID%>).SumoSelect({ selectAll: true, okCancelInMulti: true });
                }

                if (RoleID == "SE") {
                    $(<%=CustNameList.ClientID%>).SumoSelect({ selectAll: true, okCancelInMulti: true });
                    $(<%=CustNumList.ClientID%>).SumoSelect({ selectAll: true, okCancelInMulti: true });
                }

            $(<%=ProductGrpList.ClientID%>).SumoSelect({ selectAll: true, okCancelInMulti: true, search: true });
            $(<%=ProductFamilyList.ClientID%>).SumoSelect({ selectAll: true, okCancelInMulti: true, search: true });
            $(<%=ApplicationList.ClientID%>).SumoSelect({ selectAll: true, okCancelInMulti: true, search: true });
            $('#MainContent_ddlCustomerList').change(function () {
                var ddlslectedText = $("#MainContent_ddlCustomerList option:selected").val();
                $("#MainContent_ddlCustomerNumber").val(ddlslectedText);
            });
            $('#MainContent_ddlCustomerNumber').change(function () {
                var ddlslectedText = $("#MainContent_ddlCustomerNumber").val();
                $("#MainContent_ddlCustomerList").val(ddlslectedText);
            });
            $('#product_image').unbind('click').bind('click', function (e) {
                var attr = $('#product_image').attr('src');
                $("#MainContent_reportdrpdwns").slideToggle();
                if (attr == "images/up_arrow.png") {
                    $("#product_image").attr("src", "images/down_arrow.png");
                } else {
                    $("#product_image").attr("src", "images/up_arrow.png");
                }
            });
        }

     


        function amchartquaterly() {

            debugger;
          
            var dt = new Date();
            acy = dt.getFullYear();
            pcy = acy - 1;
            var value = "Qty";
            var chart;
            var cd = document.getElementById("MainContent_ay");
            var ayear = cd.value;
            var cyear = parseInt(ayear) + 1;
            cyear = cyear.toString();
            $.ajax({
                type: "POST",
                url: "ReviewQuarterly.aspx/AmchartGraphQuaterly",
                data: "",
                contentType: "Application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    tmp = msg.d;
                    console.log(tmp)
                    debugger;
                    chart = AmCharts.makeChart("amGraphQuaterly",
                           {
                              
                               "type": "serial",
                               "theme": "light",
                               "dataProvider": tmp,
                               "categoryField": "Heading",
                         
                               "legend": {
                              
                                   "horizontalGap": 10,
                                   "maxColumns": 1,
                                   "position": "top",
                                   "useGraphSettings": true,
                                   "markerSize": 16,
                                   "marginTop": 20,
                                   "valueAlign": "left",
                                   "align": "right",
                               },
                             
                               "chartCursor": {
                                   "categoryBalloonEnabled": false,
                                   "cursorAlpha": 0,
                                   "zoomable": false
                               },
                               "categoryAxis": {
                                   "gridPosition": "start",
                                   "axisAlpha": 0.8,
                                   "gridAlpha": 0

                               },
                               "valueAxes": [{
                                   "position": "left",
                                   "axisAlpha": 0.8,
                                   "gridAlpha": 0,
                                   "titleBold":true
                               }],
                               "graphs": [{
                                   "colorField": "Color",
                                   "valueField": "YTDSALE CurrentYear",
                                   "title": "YTDSALE "+acy,
                                   "fillAlphas": 1,
                                   "lineAlpha": 0.1,
                                   "type": "column",
                                   "topRadius": 0.8
                               },
                               {
                                   "bullet": "round",
                                   "valueField": "YTDPLAN CurrentYear",
                                   "title": "YTDPLAN " + acy,
                                   "labelText": "[[value]]",
                                   "labelPosition": "bottom"
                               },
                                 {
                                     "bullet": "square",
                                     "valueField": "YTDSALE ActualYear",
                                     "labelText": "[[value]]",
                                     "title": "YTDSALE "+pcy,
                                     "labelPosition": "top"

                                 }],
                               "export": {
                                   "enabled": true,
                                   "menu": []
                               },
                               "listeners": [{
                                   "event": "drawn",
                                   "method": addLegendLabel
                               }]

                           });
                    function addLegendLabel(e) {
                        var title = document.createElement("div");
                        title.innerHTML = "Summary of Sales Budget Quarterly";
                        title.className = "legend-title";
                        e.chart.legendDiv.appendChild(title)
                    }
                    //   chart.fontSize = 17;
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
       
                }
     



    </script>

    <asp:HiddenField  runat="server" ID="ay"/>
   
</asp:Content>
