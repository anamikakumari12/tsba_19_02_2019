﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TaegutecSalesBudget
{
    public partial class BudgetDetails : System.Web.UI.Page
    {
        #region GlobalDeclarations
        Reports objReports = new Reports();
        Budget objBudget = new Budget();
        AdminConfiguration objConfig = new AdminConfiguration();
        DashboardReports objRprtsDashbrd = new DashboardReports();
        CommonFunctions objFunc = new CommonFunctions();
        public string strUserRole, UserId, BranchCode, BranchDesc;
        public static int byValueIn;
        public static string cter;
        Review objRSum = new Review();
        CommonFunctions objCom = new CommonFunctions();
        #endregion
        #region Events
        /// <summary>
        /// Author  : K.LakshmiBindu
        /// Date    : 7 Dec,2018
        /// Desc    : Based on user role we are loading branches,cterdiv
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                lbmessage.Visible = false;
                BranchDesc = Convert.ToString(Session["BranchDesc"]);
                UserId = Convert.ToString(Session["UserId"]);
                if (UserId == null) { Response.Redirect("Login.aspx"); return; }

                strUserRole = Convert.ToString(Session["RoleId"]);
                if (strUserRole == "SE") { Response.Redirect("Login.aspx"); return; }
                UserId = Convert.ToString(Session["UserId"]);
                
                BranchCode = Convert.ToString(Session["BranchCode"]);
                if (!IsPostBack)
                {
                    
                    byValueIn = 1000;
                    cter = null;
                    if (string.IsNullOrEmpty(Convert.ToString(Session["cter"])) && strUserRole != "HO")
                    {
                        Session["cter"] = Session["Territory"];
                    }
                    cter = Convert.ToString(Session["cter"]);
                    if (strUserRole == "HO" || strUserRole == "TM")
                    {
                        
                        if (Convert.ToString(Session["RoleId"]) == "HO")
                        {
                            btnApproveAtHo.Visible = true;
                            if (Session["cter"] == null && strUserRole == "HO")
                            {
                                Session["cter"] = "TTA";
                                cter = "TTA";
                              
                               
                            }
                            if (Convert.ToString(Session["cter"]) == "DUR")
                            {
                                rdBtnDuraCab.Checked = true;
                                rdBtnTaegutec.Checked = false;
                                cter = "DUR";
                            }
                            else
                            {
                                rdBtnTaegutec.Checked = true;
                                rdBtnDuraCab.Checked = false;
                                cter = "TTA";
                            }
                            divCter.Visible = true;
                        }
                        btnApproveAtBM.Visible = true;
                        LoadBranches();
                       
                        // LoadGridViews(Convert.ToString(BranchList.SelectedValue));
                    }
                   
                    if (strUserRole == "BM")
                    {
                        btnApproveAtBM.Text = "APPROVE";
                        btnApproveAtBM.Visible = true;
                        btnApproveAtHo.Visible = false;

                        //reportdrpdwns.Visible = false;
                        //divBranch.Visible = false;
                        LoadBranches();
                        BranchList.SelectedValue = BranchCode;
                        BranchList.Enabled = false;
                        Session["SelectedBranchList"] = BranchCode;
                        DataTable dtSalesEngDetails = objReports.LoadUserInfo(null, BranchCode);
                        if (dtSalesEngDetails != null)
                        {
                            salesEngineerList.DataSource = dtSalesEngDetails;
                            salesEngineerList.DataTextField = "EngineerName";
                            salesEngineerList.DataValueField = "EngineerId";
                            salesEngineerList.DataBind();
                            //ddlSalesEngineerList.Items.Insert(0, "SELECT SALES ENGINEER");
                            // ChkSalesEng.Items.Insert(0, "ALL");
                        }

                        foreach (ListItem val in salesEngineerList.Items)
                        {
                            val.Selected = true;
                        }
                        LoadGridViews(BranchCode);
                    }
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        /// <summary>
        /// Author  : K.LakshmiBindu
        /// Date    : 7 Dec,2018
        /// Desc    :Loads Gridviews for selected branch
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BranchList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name_desc = "", name_code = "";
            int count = 0;
            string SalesengList = null;
            string SalesengnameList = null;
            DataTable dtData = null;
            try
            {
                if (BranchList.SelectedIndex == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "msgboxx", "alert('Please select Branch');", true);
                  
                    GvBudget.DataSource = null;
                    GvBudget.DataBind();
                    GvBudget.Visible = false;
                    divApproveBtns.Visible = false;
                    GvExcepCustDetails.Visible = false;
                    salesEngineerList.Items.Clear();
                   
                }
                else
                {
                    string branchcode = BranchList.SelectedValue;

                   // objRSum.BranchCode = strUserRole == "TM"  ? UserId : branchcode;
                    objRSum.BranchCode = branchcode;
                    objRSum.roleId = strUserRole;
                    objRSum.flag = "SalesEngineer";
                    objRSum.cter = cter;
                    dtData = objRSum.getFilterAreaValue(objRSum);
                    if (dtData != null)
                    {
                        if (dtData.Rows.Count != 0)
                        {

                            salesEngineerList.DataSource = dtData;
                            salesEngineerList.DataTextField = "EngineerName";
                            salesEngineerList.DataValueField = "EngineerId";
                            salesEngineerList.DataBind();
                            // ChkSalesEng.Items.Insert(0, "ALL");
                        }
                        else
                        {
                            salesEngineerList.DataSource = dtData;
                            salesEngineerList.DataTextField = "EngineerName";
                            salesEngineerList.DataValueField = "EngineerId";
                            salesEngineerList.DataBind();
                            //ChkSalesEng.Items.Insert(0, "NO SALES ENGINEER");
                        }
                    }

                    foreach (ListItem val in salesEngineerList.Items)
                    {
                        val.Selected = true;
                        count++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + ",";
                    }
                    if (count == salesEngineerList.Items.Count)
                    {
                        Session["SelectedSalesEngineers"] = "ALL";
                    }
                    else
                    {
                        SalesengList = name_code.Substring(0, Math.Max(0, name_code.Length - 1));
                        SalesengnameList = name_desc.Substring(0, Math.Max(0, name_desc.Length - 2));
                        Session["SelectedSalesEngineers"] = SalesengList;
                    }


                    LoadGridViews(BranchList.SelectedValue);
                    //   LoadExcepCustDetails();
                    //LoadGridViews(Convert.ToString(BranchList.SelectedValue));
                }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }
        /// <summary>
        /// Author  : K.LakshmiBindu
        /// Date    : 7 Dec,2018
        /// Desc    : Loads gridview in laksh
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Lakhs_CheckedChanged(object sender, EventArgs e)
        {
            byValueIn = 100000;
            loadGridViewOnValueChange();
        }
        /// <summary>
        /// Author  : K.LakshmiBindu
        /// Date    : 7 Dec,2018
        /// Desc    :Loads Gridvieew in Thousands
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Thousand_CheckedChanged(object sender, EventArgs e)
        {
            byValueIn = 1000;
            loadGridViewOnValueChange();
        }
        /// <summary>
        /// Author  : K.LakshmiBindu
        /// Date    : 7 Dec,2018
        /// Desc    :Loads GridView in units
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Units_CheckedChanged(object sender, EventArgs e)
        {
            byValueIn = 1;
            loadGridViewOnValueChange();
        }
        /// <summary>
        /// Author  : K.LakshmiBindu
        /// Date    : 7 Dec,2018
        /// Desc    : Loads  branches for duracab or taegutec based on selection
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rdBtnTaegutec_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (rdBtnTaegutec.Checked)
                {
                    Session["cter"] = "TTA";
                    cter = "TTA";
                }
                if (rdBtnDuraCab.Checked)
                {
                    Session["cter"] = "DUR";
                    cter = "DUR";
                }
                LoadBranches();
                BranchList_SelectedIndexChanged(null, null);

              //  LoadGridViews(Convert.ToString(BranchList.SelectedValue));

            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }
        /// <summary>
        /// Author:K.LakshmiBindu
        /// Date:12 Dec,2018
        /// Desc: stores selected sales engineer list into session for future use 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SalesEngList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name_desc = "", name_code = "";
            int count = 0;
            string SalesengList = null;
            string SalesengnameList = null; ;
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

                foreach (ListItem val in salesEngineerList.Items)
                {
                    if (val.Selected)
                    {
                        count++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + ",";
                    }
                }
                if (count == salesEngineerList.Items.Count)
                {
                    Session["SelectedSalesEngineers"] = "ALL";
                }
                else
                {

                    SalesengList = name_code.Substring(0, Math.Max(0, name_code.Length - 1));
                    SalesengnameList = name_desc.Substring(0, Math.Max(0, name_desc.Length - 2));
                    Session["SelectedSalesEngineers"] = SalesengList;
                }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }



            //objBudget.getBudget(Convert.ToString(salesEngineerList.SelectedValue));
        }
        /// <summary>
        /// Author:K.LakshmiBindu
        /// Date: 12 Dec,2018
        /// Desc: Approves Budget for Branch  and for SE
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //protected void btnBranchApproval_Click(object sender, EventArgs e)
        //{
        //    string Branch = BranchList.SelectedValue;
        //    string SalesengList = null;
        //    int resultnumber = 1;
        //    string level = null;
        //    try
        //    {
        //        Button approve = sender as Button;
        //        level = approve.Text;
        //        if (level == "APPROVE(BM LEVEL)")
        //        {
        //            level = "BM";
        //        }
        //        else if (level == "APPROVE(HO LEVEL)")
        //        {
        //            level = "HO";
        //        }
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

        //        if (Branch == "SELECT BRANCH")
        //        {
        //            BranchList.SelectedIndex = 0;
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPpup", "alert('Please select branch and approve it');", true);

        //        }
        //        else
        //        {
        //            if (salesEngineerList.Items.Count > 0)
        //            {
        //                SalesengList = Convert.ToString(Session["SelectedSalesEngineers"]);
        //                objRSum.salesengineer_id = SalesengList.ToString() == "ALL" || SalesengList.ToString() == "" ? null : SalesengList;
        //                objRSum.BranchCode = Convert.ToString(BranchList.SelectedValue);
        //                objRSum.roleId = strUserRole;

        //                string result = objBudget.AprroveBudget(objRSum,level,out resultnumber);
        //                if (result != null)
        //                {

        //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "msgbox", "alert('" + result + "');", true);
        //                    if (resultnumber == 0)
        //                    {
        //                        btnApproveAtBM.Visible = false;
        //                    }
        //                    else
        //                    {
        //                        btnApproveAtBM.Visible = true;
        //                    }
        //                }
        //                else
        //                {
        //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPop", "alert('There is error in approving, please try again.');", true);
        //                    //  BranchList.SelectedIndex = 0;

        //                }

        //            }
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        CommonFunctions.LogErrorStatic(ex);
        //    }
        //}
        /// <summary>
        /// Author:K.LakshmiBindu
        /// Date:12 Dec,2018
        /// Desc:Displays Budget for the selected Branch and SE in Gridview
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void reports_Click(object sender, EventArgs e)
        {
            if (strUserRole != "BM")
            {
                LoadGridViews(BranchList.SelectedValue);
            }
            else
            {
                LoadGridViews(BranchCode);
            }
        }
        /// <summary>
        /// Author:K.LakshmiBindu
        /// Date:26 Dec,2018
        /// Desc: Approves Budget at BM level
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnApproveAtBM_Click(object sender, EventArgs e)
        {
            approve("BM");

        }
        /// <summary>
        /// Author:K.LakshmiBindu
        /// Date:26 Dec,2018
        /// Desc: Approves Budget at HO level
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnApproveAtHo_Click(object sender, EventArgs e)
        {
            approve("HO");
        }

        #endregion

        #region Methods
        /// <summary>
        /// Author  : K.LakshmiBindu
        /// Date    : 7 Dec,2018
        /// Desc: Loads Branches for HO and TM
        /// </summary>
        protected void LoadBranches()
        {
            DataTable dtData = null;
            try
            {
                if (UserId == null) { Response.Redirect("Login.aspx"); return; }

                objRSum.BranchCode = UserId; // passing here territory Engineer Id  as branch code IF role is TM 
                objRSum.roleId = strUserRole;
                objRSum.flag = "Branch";
                objRSum.cter = cter;
                dtData = objRSum.getFilterAreaValue(objRSum);
                if (dtData != null)
                {
                    if (dtData.Rows.Count > 0)
                    {
                        BranchList.DataSource = dtData;
                        BranchList.DataTextField = "BranchDesc";
                        BranchList.DataValueField = "BranchCode";
                        BranchList.DataBind();
                        BranchList.Items.Insert(0, "SELECT BRANCH");
                    }
                    else
                    {

                        BranchList.DataSource = dtData;
                        BranchList.DataTextField = "BranchDesc";
                        BranchList.DataValueField = "BranchCode";
                        BranchList.DataBind();
                        BranchList.Items.Insert(0, new ListItem(BranchDesc, BranchCode));

                    }
                }
            }

            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }

        /// <summary>
        /// Author  : K.LakshmiBindu
        /// Date    : 7 Dec,2018
        /// Desc    :Loads Gridview for BM,TM,HO
        /// 
        /// </summary>
        /// <param name="BranchCode"></param>
        public void LoadGridViews(string BranchCode)
        {
            DataTable ds = null;
            string B_budgetyear = null;
            int ApproveFalg;
            try
            {
                objRSum.BranchCode = BranchCode;
                objRSum.roleId = strUserRole;
                objRSum.salesengineer_id = Convert.ToString(Session["SelectedSalesEngineers"]) == "ALL" ? null : Convert.ToString(Session["SelectedSalesEngineers"]);


                ds = objBudget.GetBudgetDetailsByFamilyCustomerChannelPatner(objRSum, byValueIn);
                ApproveFalg = Convert.ToInt32(objBudget.ApproveFalgForBudget(objRSum));
                B_budgetyear = objConfig.GetProfile("B_BUDGET_YEAR");


                if (ds != null)
                {
                    Decimal TotalforCustomer = ds.AsEnumerable().Sum(x => Convert.ToDecimal(x.Field<Decimal>("Budget_value_cust")));
                    Decimal TotalforChannelPatnerCustomer = ds.AsEnumerable().Sum(x => Convert.ToDecimal(x.Field<Decimal>("budget_value_cp")));
                    Decimal TotalBudget = ds.AsEnumerable().Sum(x => Convert.ToDecimal(x.Field<Decimal>("budget_value_total")));
                    //if (ds.Rows.Count > 0 && ApproveFalg != 0)
                    //{
                    //    divApproveBtns.Visible = true;
                    //    if (strUserRole == "HO")
                    //    {
                    //        btnApproveAtHo.Visible = true;
                    //        btnApproveAtBM.Visible = true;
                    //        btnApproveAtBM.Enabled = true;
                    //        btnApproveAtHo.Enabled = true;
                    //    }
                    //    else
                    //    {
                    //        btnApproveAtBM.Visible = true;
                    //        btnApproveAtBM.Enabled = true;
                    //    }
                    //}
                   if (ds.Rows.Count > 0 && ApproveFalg == 0)
                    {
                        GvExcepCustDetails.Visible = false;
                        divApproveBtns.Visible = false;
                        if (strUserRole == "HO")
                        {
                            btnApproveAtHo.Visible = false;
                            btnApproveAtBM.Visible = false;
                        //    btnApproveAtHo.Visible = true;
                         //   btnApproveAtBM.Visible = true;
                            btnApproveAtHo.Enabled = false;
                            btnApproveAtBM.Enabled = false;
                        }
                        else
                        {
                            btnApproveAtBM.Visible = false;
                            btnApproveAtBM.Enabled = false;
                        }

                    }

                    if (ds.Rows.Count > 0)
                    {
                        GvBudget.DataSource = ds;
                        GvBudget.DataBind();
                        GvBudget.Visible = true;
                        Label Labelhcpnu = GvBudget.HeaderRow.FindControl("Labelhcpnu") as Label;
                        if (Labelhcpnu != null)
                        {
                            Labelhcpnu.Text = Labelhcpnu.Text + "(" + B_budgetyear + ")";
                        }
                        Label Labelhbvcp = GvBudget.HeaderRow.FindControl("Labelhbvcp") as Label;
                        if (Labelhbvcp != null)
                        {
                            Labelhbvcp.Text = Labelhbvcp.Text + "(" + B_budgetyear + ")";
                        }
                        Label LabelhbvT = GvBudget.HeaderRow.FindControl("LabelhbvT") as Label;
                        if (LabelhbvT != null)
                        {
                            LabelhbvT.Text = LabelhbvT.Text + "(" + B_budgetyear + ")";
                        }
                        Label lbltotal = GvBudget.FooterRow.FindControl("lbltotal") as Label;
                        if (lbltotal != null)
                        {
                            lbltotal.Text = Convert.ToString(TotalBudget);
                        }
                        Label lblcusttotal = GvBudget.FooterRow.FindControl("lblcusttotal") as Label;
                        if (lblcusttotal != null)
                        {
                            lblcusttotal.Text = Convert.ToString(TotalforCustomer);
                        }
                        Label lblcptotal = GvBudget.FooterRow.FindControl("lblcptotal") as Label;
                        if (lblcptotal != null)
                        {
                            lblcptotal.Text = Convert.ToString(TotalforChannelPatnerCustomer);

                        }
                    }
                    else
                    {
                        GvBudget.DataSource = null;
                        GvBudget.DataBind();
                        GvBudget.Visible = true;
                        divApproveBtns.Visible = false;
                        //if (strUserRole == "HO")
                        //{
                        //    btnApproveAtHo.Visible = true;
                        //    btnApproveAtBM.Visible = false;
                        //}
                        //else
                        //{
                        //    btnApproveAtBM.Visible = false;
                        //}

                       // btnApproveAtBM.Visible = false;
                    }
                }
                if (ApproveFalg !=0)
                {
                    LoadExcepCustDetails(objRSum);
                }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }
        /// <summary>
        /// Author  : K.LakshmiBindu
        /// Date    : 7 Dec,2018
        /// Desc    :Loads Gridview  on value changed
        /// </summary>
        public void loadGridViewOnValueChange()
        {
            try
            {
                if (strUserRole == "BM")
                {
                    LoadGridViews(Convert.ToString(Session["SelectedBranchList"]));
                }
                else
                {
                    LoadGridViews(Convert.ToString(BranchList.SelectedValue));
                }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }
        public void LoadExcepCustDetails(Review objRsum)
        {
            objRSum.cter = cter;
            DataSet dsGetExceptionCustForBudget = new DataSet();
            try
            {
               dsGetExceptionCustForBudget=objBudget.GetExceptionCustForBudget(objRSum);
               if (dsGetExceptionCustForBudget != null)
               {
                   if (dsGetExceptionCustForBudget.Tables.Count > 0)
                   {
                       if (dsGetExceptionCustForBudget.Tables[0] != null)
                       {
                           if (dsGetExceptionCustForBudget.Tables[0].Rows.Count > 0)
                           {
                               GvExcepCustDetails.DataSource = dsGetExceptionCustForBudget.Tables[0];
                               GvExcepCustDetails.DataBind();
                               GvExcepCustDetails.Visible = true;
                               lbmessage.Visible = true;
                               lbmessage.Text = "Below are the customers with status, please check customers' status and then proceed.";
                           }
                           else
                           {
                               GvExcepCustDetails.DataSource = null;
                               GvExcepCustDetails.DataBind();
                               GvExcepCustDetails.Visible = true;
                           }
                       }
                       else
                       {
                           GvExcepCustDetails.DataSource = null;
                           GvExcepCustDetails.DataBind();
                           divApproveBtns.Visible = false;
                           GvExcepCustDetails.Visible = false;
                           lbmessage.Text = "Some issue ";
                         
                       }
                       if (dsGetExceptionCustForBudget.Tables[1] != null)
                       {
                           if (dsGetExceptionCustForBudget.Tables[1].Rows.Count > 0)
                           {
                               int BM_flag = Convert.ToInt32(dsGetExceptionCustForBudget.Tables[1].Rows[0]["BM_Flag"]);
                               int HO_flag = Convert.ToInt32(string.IsNullOrEmpty(Convert.ToString(dsGetExceptionCustForBudget.Tables[1].Rows[0]["HO_Flag"]))?1:Convert.ToInt32(Convert.ToString(dsGetExceptionCustForBudget.Tables[1].Rows[0]["HO_Flag"])));
                               if (BM_flag > 0)
                               {
                                   divApproveBtns.Visible = true;
                                   if (strUserRole == "HO")
                                   {
                                       btnApproveAtBM.Visible = true;
                                       btnApproveAtHo.Visible = true;
                                       btnApproveAtHo.Enabled = false;
                                       btnApproveAtBM.Enabled = false;
                                   }
                                   else
                                   {
                                       btnApproveAtBM.Visible = true;
                                       btnApproveAtBM.Enabled = false;
                                       btnApproveAtHo.Visible = false;
                                   }

                               }
                               else
                               {
                                   divApproveBtns.Visible = true;
                                   if (strUserRole == "HO")
                                   {
                                       btnApproveAtBM.Visible = true;
                                       btnApproveAtHo.Visible = true;
                                       btnApproveAtHo.Enabled = true;
                                       btnApproveAtBM.Enabled = true;
                                   }
                                   else
                                   {
                                       btnApproveAtBM.Visible = true;
                                       btnApproveAtBM.Enabled = true;
                                       btnApproveAtHo.Visible = false;
                                   }
                               }
                               if (HO_flag > 0)
                               {
                                   if (strUserRole == "HO")
                                   {
                                       btnApproveAtBM.Visible = true;
                                       btnApproveAtHo.Visible = true;
                                       btnApproveAtHo.Enabled = false;
                                       btnApproveAtBM.Enabled = false;
                                   }
                                   else
                                   {
                                       btnApproveAtHo.Visible = false;
                                       btnApproveAtBM.Visible = true;
                                       btnApproveAtBM.Enabled = false;
                                   }
                               }
                               else
                               {
                                   divApproveBtns.Visible = true;
                                   if (strUserRole == "HO")
                                   {
                                       btnApproveAtBM.Visible = true;
                                       btnApproveAtHo.Visible = true;
                                       btnApproveAtHo.Enabled = true;
                                       btnApproveAtBM.Enabled = true;
                                   }
                                   else
                                   {
                                       btnApproveAtBM.Visible = true;
                                       btnApproveAtBM.Enabled = true;
                                       btnApproveAtHo.Visible = false;
                                   }
                               }
                           }
                           else
                           {
                               divApproveBtns.Visible = true;
                               btnApproveAtHo.Enabled = true;
                               btnApproveAtHo.Visible = true;
                               btnApproveAtBM.Enabled = true;
                           }
                          
                       }
                       else
                       {
                           divApproveBtns.Visible = false;
                           btnApproveAtHo.Enabled = false;
                           btnApproveAtBM.Enabled = false;
                           lbmessage.Visible = true;
                           lbmessage.Text = "Please try Again.";
                       }
                   }
                   else
                   {
                       GvExcepCustDetails.DataSource = null;
                       GvExcepCustDetails.DataBind();
                     
                   }
               }
               else
               {
                   GvExcepCustDetails.DataSource = null;
                   GvExcepCustDetails.DataBind();
                   lbmessage.Visible = true;
                   lbmessage.Text = "there is an issue in fetching the records please try again";
                   GvExcepCustDetails.Visible = false;
                   divApproveBtns.Visible = false;


               }
              
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }
        }
        /// <summary>
        /// Author:K.LakshmiBindu
        /// Date:26 Dec,2018
        /// Desc: Approves Budget at HO,BM level
        /// </summary>
        /// <param name="level"></param>
        protected void approve(string level)
        {
            string Branch = BranchList.SelectedValue;
            string SalesengList = null;
            int resultnumber = 1;

            int ApproveFalg;
            try
            {

                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
              
                if (Branch == "SELECT BRANCH")
                {
                    BranchList.SelectedIndex = 0;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPpup", "alert('Please select branch and approve it');", true);

                }
                else
                {
                    if (salesEngineerList.Items.Count > 0)
                    {
                        SalesengList = Convert.ToString(Session["SelectedSalesEngineers"]);
                        objRSum.salesengineer_id = SalesengList.ToString() == "ALL" || SalesengList.ToString() == "" ? null : SalesengList;
                        objRSum.BranchCode = Convert.ToString(BranchList.SelectedValue);
                        objRSum.roleId = strUserRole;

                        string result = objBudget.AprroveBudget(objRSum, level, out resultnumber);
                        ApproveFalg = Convert.ToInt32(objBudget.ApproveFalgForBudget(objRSum));
                        if (result != null)
                        {

                            ScriptManager.RegisterStartupScript(this, this.GetType(), "msgbox", "alert('" + result + "');", true);
                            if (resultnumber == 0)
                            {
                                    divApproveBtns.Visible = true;
                                    if (strUserRole == "HO")
                                    {
                                        if (level == "BM")
                                        {
                                            btnApproveAtHo.Visible = true;
                                            btnApproveAtBM.Visible = false;
                                        }
                                        else
                                        {
                                            btnApproveAtHo.Visible = false;
                                            btnApproveAtBM.Visible = false;
                                            divApproveBtns.Visible = false;
                                        }

                                    }
                                    else
                                    {
                                        btnApproveAtBM.Visible = false;
                                    }
                               
                            }
                            else
                            {
                                divApproveBtns.Visible = true;
                                if (strUserRole == "HO")
                                {
                                    btnApproveAtHo.Visible = true;
                                    btnApproveAtBM.Visible = true;
                                }
                                else
                                {
                                    btnApproveAtBM.Visible = true;
                                }
                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPop", "alert('There is error in approving, please try again.');", true);
                            //  BranchList.SelectedIndex = 0;

                        }

                    }
                }

            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }
        }


    }
}

        #endregion

      

    