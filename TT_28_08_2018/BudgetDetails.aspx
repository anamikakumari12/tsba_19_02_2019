﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BudgetDetails.aspx.cs" Inherits="TaegutecSalesBudget.BudgetDetails" MasterPageFile="~/Site.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="HeadContent">
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
 
<script src="https://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>

<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css" />
     <script src="js/jquery.dataTables.min.js"></script>
    <link href="css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="css/buttons.dataTables.min.css" rel="stylesheet" />
       <script src="js/jquery.sumoselect.min.js"></script>
    <link href="css/sumoselect.css" rel="stylesheet" />
  
       <script type="text/javascript" src="gridscroll.js"></script>
    <style type="text/css">
        thead {
            border: solid 1px #ebeef5;
        }

        .valuealign {
            margin-right: 30px;
        }

        tfoot {
            border: solid 1px #ebeef5;
            padding: 3px 10px;
        }

            tfoot tr th {
                text-align: right;
                padding: 3px!important;
            }

        tbody {
            border-color: #fff;
        }

        .control_dropdown {
            width: 180px;
            height: 30px;
            border-radius: 4px!important;
        }

        .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
            border: 1px solid #ddd;
            padding: 5px;
        }

        td {
            background: #fff;
            padding: 5px;
            border-bottom: solid 1px #ddd;
        }

        th {
            padding: 10px;
        }

        table {
            width: 100%;
        }

        .panel-heading1 {
            background: #0582b7;
            color: #29AAe1 !important;
            padding: 15px;
            color: #fff;
            font-size: 16px;
            font-weight: bolder;
            text-transform: uppercase;
        }

        .panel-heading2 {
            background: #999;
            color: #333 !important;
            padding: 15px;
            color: #fff;
            font-size: 16px;
            font-weight: bolder;
            text-transform: uppercase;
        }

        .panel-heading3 {
            background: #333;
            color: #8ac340 !important;
            padding: 15px;
            color: #fff;
            font-size: 16px;
            font-weight: bolder;
            text-transform: uppercase;
        }

        .panel-heading4 {
            background: #999;
            color: #91298E !important;
            padding: 15px;
            color: #fff;
            font-size: 16px;
            font-weight: bolder;
            text-transform: uppercase;
        }

        .panel-heading5 {
            background: #006780;
            color: #ffffff !important;
            padding: 15px;
            color: #fff;
            font-size: 16px;
            font-weight: bolder;
            text-transform: uppercase;
        }
         .width {
            width:35px !important;
        }
        .widthCN {
 width:175px !important;
        }
         .widthSN {
 width:125px !important;
        }
        .panel-heading6 {
            background: #999;
            color: #002238 !important;
            padding: 15px;
            color: #fff;
            font-size: 16px;
            font-weight: bolder;
            text-transform: uppercase;
        }

        .panel-heading7 {
            background: #333;
            color: #fff !important;
            padding: 15px;
            color: #fff;
            font-size: 16px;
            font-weight: bolder;
            text-transform: uppercase;
        }

        .panel-group .panel {
            border-radius: 4px;
            margin-bottom: 0;
            overflow: hidden;
            margin-top: 20px;
            margin-bottom: 20px;
        }

        .label {
            padding-top: 15px;
            width: 100%;
            color: black;
        }

        .HeadergridAll {
            background: #ebeef5;
            color: #fff;
            font-weight: 600;
            text-align: center;
            border-color: #ebeef5;
            font-size: 12px;
           
        }

    </style>
    <script type="text/javascript">
       
        $(document).ready(function () {
          //  bindGridView();
        })
        //function gridviewScrollTrigger() {
        //    
        //    gridView1 = $('#MainContent_GvExcepCustDetails').gridviewScroll({
        //        width: $(window).width() - 60,
        //        height: findminofscreenheight($(window).height(), 500),
        //        railcolor: "#F0F0F0",
        //        barcolor: "#606060",
        //        barhovercolor: "#606060",
        //        bgcolor: "#F0F0F0",
        //        freezesize: 0,
        //        arrowsize: 30,
        //        varrowtopimg: "Images/arrowvt.png",
        //        varrowbottomimg: "Images/arrowvb.png",
        //        harrowleftimg: "Images/arrowhl.png",
        //        harrowrightimg: "Images/arrowhr.png",
        //        headerrowcount: 1,
        //        railsize: 16,
        //        barsize: 14,
        //        verticalbar: "auto",
        //        horizontalbar: "auto",
        //        wheelstep: 1,
        //    });
        //}

        //function findminofscreenheight(a, b) {
        //    a = a - $("#MainContent_BranchList").offset().top;
        //    return a < b ? a : b;
            
        //}

        function bindGridView() {
            debugger;
            var head_content = $('#MainContent_GvExcepCustDetails tr:first').html();
            console.log("HeadConytent"+head_content);
            //var foot_content = $('#MainContent_GvExcepCustDetails tr:last').html();
            //console.log(foot_content);
            $('#MainContent_GvExcepCustDetails').prepend('<thead></thead>')
            $('#MainContent_GvExcepCustDetails thead').html('<tr>' + head_content + '</tr>');
            //$('#MainContent_GvExcepCustDetails').prepend('<tfoot></tfoot>')
            //$('#MainContent_GvExcepCustDetails tfoot').html('<tr>' + foot_content + '</tr>');
            $('#MainContent_GvExcepCustDetails tbody tr:first').hide();
            //$('#MainContent_GvExcepCustDetails tbody tr:last').hide();
            $('#MainContent_GvExcepCustDetails').DataTable(
             {
                 "info": false,
             });

        }
        function triggerPostGridLodedActions() {
            debugger;
            $(<%=salesEngineerList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true, search: true });
            bindGridView();
            //bindGridView();
        }


    </script>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableHistory="true" EnablePartialRendering="true" EnablePageMethods="true" AsyncPostBackTimeout="360">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="crumbs">
                <!-- Start : Breadcrumbs -->
                <ul id="breadcrumbs" class="breadcrumb" style="margin-top:-5px;">
                    <li>
                        <i class="fa fa-home"></i>
                        <a>Budget</a>
                    </li>
                    <li class="current">Budget Details</li>

                    <div>
                        <ul style="float: right; list-style: none; margin-top: -4px; width: 265px; margin-right: -5px;" class="alert alert-danger fade in">
                            <li>
                                <span style="margin-right: -1px; margin-left: -35px; vertical-align: text-bottom;">Val In Units</span>
                                <asp:RadioButton ID="rbtn_Units" OnCheckedChanged="Units_CheckedChanged" GroupName="customer" runat="server" AutoPostBack="true" />
                                <span style="margin-right: -1px; margin-left: 5px; vertical-align: text-bottom;">Val In '000</span>
                                <asp:RadioButton ID="rbtn_Thousand" OnCheckedChanged="Thousand_CheckedChanged" GroupName="customer" runat="server" Checked="True" AutoPostBack="true" />
                                <span style="margin-right: 0px; margin-left: 6px; vertical-align: text-bottom;">Val In Lakhs</span>
                                <asp:RadioButton ID="rbtn_Lakhs" OnCheckedChanged="Lakhs_CheckedChanged" GroupName="customer" runat="server" AutoPostBack="true" />
                            </li>
                        </ul>
                    </div>
                </ul>

            </div>
            <!-- END : Breadcrumbs -->

            <div class="col-md-12" style="margin-top: 5px;">


                <div class="row filter_panel" id="reportdrpdwns" runat="server" style="padding-bottom:0px;height:45px">
                    <div class="col-md-4 control" runat="server" id="divBranch">
                       <div style="float:left;margin-top:5px">
                            <label class="label" >BRANCH</label>
                         </div>
                        <div style="float:left">
                            <asp:DropDownList runat="server" CssClass="control_dropdown"  ID="BranchList" AutoPostBack="true" OnSelectedIndexChanged="BranchList_SelectedIndexChanged" Width="250px"></asp:DropDownList>
                     </div>
                    </div>
                           <div class="col-md-4 control" runat="server" id="div2">
                         <div style="float:left;margin-top:5px">
                            <label class="label">SALES ENGINEER</label>
                       
                        </div>
                                 <div style="float:left">
                          <asp:ListBox ID="salesEngineerList" runat="server"  SelectionMode="Multiple" OnSelectedIndexChanged="SalesEngList_SelectedIndexChanged" CssClass="control_dropdow"></asp:ListBox>
                      </div>
                    </div>

                    <div class="col-md-1 control" runat="server" id="div4">

                        <div style="margin-top:2px;margin-left:25px">
                       <asp:Button ID="reports" Text="FILTER" CssClass="btn green" runat="server" OnClick="reports_Click" />
                             
                        </div>

                    </div>
                     <div class="col-md-2 control" runat="server" id="div1">
                    
                    </div>
                    <div class="col-md-2" style="float: right;">
                        <ul id="divCter" runat="server" class="btn-info rbtn_panel" style="float: right;" visible="false">
                            <li><span style="margin-right: 4px; vertical-align: text-bottom;">TAEGUTEC</span>

                                <asp:RadioButton ID="rdBtnTaegutec" AutoPostBack="true" Checked="true" GroupName="byCmpnyCodeInradiobtn" runat="server" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" />
                                <span style="margin-right: 4px; margin-left: 4px; vertical-align: text-bottom;">DURACARB</span>
                                <asp:RadioButton ID="rdBtnDuraCab" AutoPostBack="true" GroupName="byCmpnyCodeInradiobtn" runat="server" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" />
                            </li>
                        </ul>
                    </div>
                </div>

            </div>
          
               <div class="row" style="margin-top:10px">
                 

              <div class="col-md-2"  margin-bottom:0">
                        <ul id="Ul1" runat="server"  style="float: left;">
                        
                        </ul>
                    </div>
           </div>

            <div class="clearfix"></div>
          
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet-body">
                            <div id="accordion" class="panel-group">
                               <%-- <div id="Div1" class="panel panel-default" runat="server" style="float: left; width: 700px; margin-top: 0px; margin-left: 25px">--%>
                                    
                                        <asp:GridView runat="server" ShowFooter="true" ID="GvBudget" AutoGenerateColumns="false" Width="800px" style="margin-left:150px" CssClass="table table-bordered">
                                            <Columns>
                                               
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <asp:Label ID="Labelhcpna" Text="Family Name"  HeaderStyle-CssClass="HeadergridAll" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    
                                                    <ItemTemplate>
                                                        <asp:Label ID="Labelcpna" runat="server" Text='<%# Eval("item_family_name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                         <div style="text-align:right;" >
                                                        <asp:Label runat="server" Text="Total :"> </asp:Label></div>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    
                                                    <HeaderTemplate>
                                                        <asp:Label ID="Labelhcpnu" Text="Budget Value For Customer"  HeaderStyle-CssClass="HeadergridAll" runat="server" ></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <div style="text-align:right;" >
                                                        <asp:Label ID="Labelcpnu" style="text-align:right;" runat="server" Text='<%# Eval("Budget_value_cust" ) %>'></asp:Label>
                                                            </div>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                         <div style="text-align:right;" >
                                                        <asp:Label runat="server" ID="lblcusttotal" Text="test"></asp:Label></div>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                       <asp:TemplateField>
                                                    
                                                    <HeaderTemplate>
                                                        <asp:Label ID="Labelhbvcp" Text="Budget Value For ChannelPartner"  HeaderStyle-CssClass="HeadergridAll" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <div style="text-align:right;" >
                                                        <asp:Label ID="Labelbvcp" runat="server" Text='<%# Eval("budget_value_cp") %>'></asp:Label>
                                                            </div>
                                                    </ItemTemplate>
                                                           <FooterTemplate>
                                                                <div style="text-align:right;" >
                                                        <asp:Label runat="server" ID="lblcptotal" Text="test"></asp:Label></div>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    
                                                    <HeaderTemplate>
                                                        <asp:Label ID="LabelhbvT" Text="Total Budget Value"  HeaderStyle-CssClass="HeadergridAll" runat="server"></asp:Label>
                                                    </HeaderTemplate>
                                                    <ItemTemplate >
                                                        <div style="text-align:right;" >
                                                        <asp:Label ID="LabelbvT" runat="server" Text='<%# Eval("budget_value_total") %>'></asp:Label>
                                                            </div>
                                                    </ItemTemplate>
                                                    <FooterTemplate>
                                                         <div style="text-align:right; ">
                                                        <asp:Label runat="server" ID="lbltotal" Text="test"></asp:Label></div>
                                                    </FooterTemplate>
                                                </asp:TemplateField>
                                                
                                            </Columns>
                                           <FooterStyle Font-Bold="true"/>
                                            <EmptyDataTemplate>No Records Found</EmptyDataTemplate>
                                        </asp:GridView>


                            <%--</div>--%>
                        </div>
                    </div>
       </div>
            </div> 
               <div class="row filter_panel" id="divApproveBtns" runat="server" style="padding-top:0px;padding-bottom:0px" visible="false">
                      
                    <div style="margin-top:18px;margin-left:35px;float:left; margin-right:35px;" id="divApproveBM" runat="server">
                     <asp:Button Text="APPROVE (BM LEVEL)" CssClass="btn green" runat="server" OnClick="btnApproveAtBM_Click" ID="btnApproveAtBM" Width="190px"  Visible="false"/>
                              
                  </div>
                    <div style="margin-top:18px;float:left; margin-right:35px;" id="divApproveHO" runat="server">
                     <asp:Button Text="APPROVE (HO LEVEL)" CssClass="btn green" runat="server" OnClick="btnApproveAtHo_Click"  ID="btnApproveAtHo" Width="190px" Visible="false"/>
                              </div>

                   </div>
            <div style="margin-top:5px">
               <asp:label runat="server" ID="lbmessage" Visible="false" ></asp:label>
                </div>
            <div style="margin-top:15px">
            <asp:GridView AutoGenerateColumns="false" runat="server" ID="GvExcepCustDetails"  style="margin-left:150px" Width="1000px" CssClass="table table-bordered">
                <Columns>
                  
   <asp:TemplateField HeaderText="Customer NO" HeaderStyle-CssClass="HeadergridAll width" Visible="true" ItemStyle-Width="40px" ItemStyle-CssClass="align-right">
                        <ItemTemplate>
                           <asp:Label ID="lbcustomernumber" Text='<%# Eval("customer_number") %>' runat="server" ></asp:Label>
                       </ItemTemplate>
                    </asp:TemplateField>
                        
                       <asp:TemplateField HeaderText="Customer Name" HeaderStyle-CssClass="HeadergridAll widthCN" Visible="true" ItemStyle-Width="75px" ItemStyle-CssClass="align-left">
                        <ItemTemplate>
                           <asp:Label ID="lbcustomername" Text='<%# Eval("customer_name") %>' runat="server"></asp:Label>
                       </ItemTemplate>
                    </asp:TemplateField>
                      <asp:TemplateField HeaderText="Engineer Name" HeaderStyle-CssClass="HeadergridAll widthSN" Visible="true" ItemStyle-Width="75px" ItemStyle-CssClass="align-left">
                        <ItemTemplate>
                           <asp:Label ID="lbEngineername" Text='<%# Eval("EngineerName") %>' runat="server"></asp:Label>
                       </ItemTemplate>
                    </asp:TemplateField>
                       <asp:TemplateField HeaderText="Status At SE" HeaderStyle-CssClass="HeadergridAll width" Visible="true" ItemStyle-Width="75px" ItemStyle-CssClass="align-center">
                        <ItemTemplate>
                           <asp:Label ID="lbStatus_sales_engineer" Text='<%# Eval("Status_sales_engineer") %>' runat="server"></asp:Label>
                       </ItemTemplate>
                    </asp:TemplateField>
                       <asp:TemplateField HeaderText="Status At BM " HeaderStyle-CssClass="HeadergridAll width" Visible="true" ItemStyle-Width="75px" ItemStyle-CssClass="align-center">
                        <ItemTemplate>
                           <asp:Label ID="lbStatus_branch_manager" Text='<%# Eval("Status_branch_manager") %>' runat="server"></asp:Label>
                       </ItemTemplate>
                    </asp:TemplateField>
                       <asp:TemplateField HeaderText="Status At HO" HeaderStyle-CssClass="HeadergridAll width" Visible="true" ItemStyle-Width="75px" ItemStyle-CssClass="align-center">
                        <ItemTemplate>
                           <asp:Label ID="lbstatus_ho" Text='<%# Eval("status_ho") %>' runat="server"></asp:Label>
                       </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                  <EmptyDataTemplate>No Records Found</EmptyDataTemplate>
            </asp:GridView>
                </div>
         
        </ContentTemplate>
       <Triggers>
           <asp:AsyncPostBackTrigger ControlID="BranchList" EventName="SelectedIndexChanged" />
           <asp:AsyncPostBackTrigger ControlID="reports" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="rdBtnTaegutec" EventName="CheckedChanged" />
            <asp:AsyncPostBackTrigger ControlID="rdBtnDuraCab" EventName="CheckedChanged" />
           <asp:AsyncPostBackTrigger ControlID="btnApproveAtBM" EventName="Click" />
           <asp:AsyncPostBackTrigger ControlID="btnApproveAtHo" EventName="Click" />
       </Triggers>
    </asp:UpdatePanel>
     <asp:UpdateProgress ID="updateProgress" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff">Please wait</span>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    
</asp:Content>
