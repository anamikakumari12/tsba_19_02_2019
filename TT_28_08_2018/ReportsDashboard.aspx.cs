﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;

namespace TaegutecSalesBudget
{
    public partial class ReportsDashboard : System.Web.UI.Page
    {

        #region Global declaration

        Reports objReports = new Reports();
        AdminConfiguration objConfig = new AdminConfiguration();
        DashboardReports objRprtsDashbrd = new DashboardReports();
        CommonFunctions objFunc = new CommonFunctions();
        public static DataTable dtTopBranches, dtTopBranches_Growth, dtBottomBranches, dtBottomSalesEngineers,
              dtBottomChannelPartners, dtTopGoldCustomers, dtTopGoldCustomers_Growth, dtGoldBranches, dtGoldBranches_Growth,
            dtTopGoldChannelPartners, dtGoldChannelPartners_Growth, dtTopGoldSe, dtTopGoldSe_Growth, dtBottomGoldSe, dtBottomGoldSe_Growth, dtFamily, dtcnsldtd,
        dtTopGoldSe_withBranch, dtTopGoldCustomers_withBranch, dtTopGoldChannelPartners_WithBranch;
        public static DataSet dtTopChannelPartners, dtTopCustomers, dtBottomCustomers, dtTopSalesEngineers, dtGold, dsTopBranches;
        public string strUserRole, UserId, BranchCode;
        public static bool potSort;
        public static int byValueIn, actual_mnth, current_tab;
        public static string asc = "asc", desc = "desc", cter;
        Review objRSum = new Review();
        CommonFunctions objCom = new CommonFunctions();
        string strSelectedBranch = string.Empty;
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (!IsPostBack)
                {
                    cter = null;
                    actual_mnth = (objConfig.getActualMonth());
                    hdnYear.Value = Convert.ToString(objConfig.getBudgetYear());
                    strUserRole = Session["RoleId"].ToString();
                    UserId = Session["UserId"].ToString();
                    BranchCode = Session["BranchCode"].ToString();
                    current_tab = 1;
                    if (strUserRole != "HO")
                    {
                        divCter.Visible = false;
                    }
                    if (Session["cter"] == null && strUserRole == "HO")
                    {
                        Session["cter"] = "TTA";
                        cter = "TTA";
                    }
                    if (strUserRole == "HO" || strUserRole == "TM")
                    {
                        //divBranch.Visible = true;
                        LoadBranches();
                        ddlPerfBy.Items.Insert(0, new ListItem("BRANCHES","BRANCH"));
                        ddlPerfBy.Items.Insert(1, new ListItem("SALES ENGINEERS","SE"));
                        ddlPerfBy.Items.Insert(2, new ListItem("CUSTOMERS", "CUSTOMERS"));
                        ddlPerfBy.Items.Insert(3, new ListItem("CHANNEL PARTNERS", "CP"));
                        ddlPerfBy.Items.Insert(4, new ListItem("GOLD PRODUCTS","GOLD"));
                        ddlPerfBy.Items.Insert(5, new ListItem("PRODUCT FAMILY" ,"FAMILY"));
                        BranchList_SelectedIndexChanged(null, null);
                    }
                    else
                    {
                        if (strUserRole == "BM")
                        {
                            ddlPerfBy.Items.Insert(0, new ListItem("SALES ENGINEERS", "SE"));
                            ddlPerfBy.Items.Insert(1, new ListItem("CUSTOMERS", "CUSTOMERS"));
                            ddlPerfBy.Items.Insert(2, new ListItem("CHANNEL PARTNERS", "CP"));
                            ddlPerfBy.Items.Insert(3, new ListItem("GOLD PRODUCTS", "GOLD"));
                            ddlPerfBy.Items.Insert(4, new ListItem("PRODUCT FAMILY", "FAMILY"));
                            hdnsearch.Value = "SE";
                        }
                        else if (strUserRole == "SE")
                        {

                            ddlPerfBy.Items.Insert(0, new ListItem("CUSTOMERS", "CUSTOMERS"));
                            ddlPerfBy.Items.Insert(1, new ListItem("CHANNEL PARTNERS", "CP"));
                            ddlPerfBy.Items.Insert(2, new ListItem("GOLD PRODUCTS", "GOLD"));
                            ddlPerfBy.Items.Insert(3, new ListItem("PRODUCT FAMILY", "FAMILY"));
                            hdnsearch.Value = "CUST";
                        }
                        //divBranch.Visible = false;
                        Session["DashboardBranchList"] = "ALL";

                        LoadData();
                    }
                    divBranch.Visible = false;
                    
                    //ChartBranch.Visible = true;
                }
                byValueIn = 1000;
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "LoadChartConsolidated();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "LoadCharts(); ZoomImages();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }

        protected void branch_perf(object sender, EventArgs e)
        {
            try
            {
                hdnsearch.Value = "BRANCH";
                divBranch.Visible = false;
                strUserRole = Session["RoleId"].ToString();
                UserId = Session["UserId"].ToString();
                BranchCode = Session["BranchCode"].ToString();
                if (strUserRole == "HO")
                {
                    dtcnsldtd = objRprtsDashbrd.getValueSum(null, null, null, null, null, null, null, cter);

                    dsTopBranches = objRprtsDashbrd.getTopAndBottomSalesReports_ds("Branches", null, null, null, null, "desc", null, cter);
                    //dtBottomBranches = dtTopBranches;// objRprtsDashbrd.getTopAndBottomSalesReports("Branches", null, null, null, null, "asc");
                    if (dsTopBranches.Tables.Count > 0)
                    {
                        if (dsTopBranches.Tables[0].Rows.Count > 0)
                        {
                            dtTopBranches = dsTopBranches.Tables[0];
                        }
                        if (dsTopBranches.Tables[1].Rows.Count > 0)
                        {
                            dtTopBranches_Growth = dsTopBranches.Tables[1];
                        }
                    }
                }
                else if (strUserRole == "TM")
                {
                    dtcnsldtd = objRprtsDashbrd.getValueSum(UserId, null, null, null, null, null, null);
                    dsTopBranches = objRprtsDashbrd.getTopAndBottomSalesReports_ds("Branches", null, UserId, null, null, desc, null);
                    //dtBottomBranches = dtTopBranches;// objRprtsDashbrd.getTopAndBottomSalesReports("Branches", null, UserId, null, null, asc);
                    if (dsTopBranches.Tables.Count > 0)
                    {
                        if (dsTopBranches.Tables[0].Rows.Count > 0)
                        {
                            dtTopBranches = dsTopBranches.Tables[0];
                        }
                        if (dsTopBranches.Tables[1].Rows.Count > 0)
                        {
                            dtTopBranches_Growth = dsTopBranches.Tables[1];
                        }
                    }
                }
                else if (strUserRole == "BM")
                {
                }
                else if (strUserRole == "SE")
                {
                }

                current_tab = 1;
                Loadgraphs("BRANCH");
                //divcnsldtd.Visible = true;
                divbranches.Visible = true;
                //ChartBranch.Visible = true;
                divSE.Visible = false;
                divCustomers.Visible = false;
                divCP.Visible = false;
                divGold.Visible = false;
                divGoldCP.Visible = false;
                divFamily.Visible = false;
                //divlnkn_SE.Attributes.Add("Class", "external-event");
                //divlnkn_branches.Attributes.Add("Class", "external-event active");
                //divlnkn_CP.Attributes.Add("Class", "external-event");
                //divlnkn_Customers.Attributes.Add("Class", "external-event");
                //divlnkn_Gold.Attributes.Add("Class", "external-event");
                //divlnk_Family.Attributes.Add("Class", "external-event");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "LoadCharts();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }
        protected void se_perf(object sender, EventArgs e)
        {
            try
            {
                hdnsearch.Value = "SE";
                strUserRole = Session["RoleId"].ToString();
                if (strUserRole == "HO" || strUserRole == "TM")
                {
                    divBranch.Visible = true;
                }
                else
                {
                    divBranch.Visible = false;
                }
                UserId = Session["UserId"].ToString();
                BranchCode = Session["BranchCode"].ToString();
                if (Convert.ToString(Session["DashboardBranchList"]) == "ALL")
                    strSelectedBranch = null;
                else
                    strSelectedBranch = Convert.ToString(Session["DashboardBranchList"]);
                if (strUserRole == "HO")
                {
                    dtcnsldtd = objRprtsDashbrd.getValueSum(strSelectedBranch, null, null, null, null, null, null, cter);
                    dtTopSalesEngineers = objRprtsDashbrd.getTopAndBottomSalesReports_ds("salesEngineers", strSelectedBranch, null, null, null, "desc", null, cter);
                    //dtBottomSalesEngineers = objRprtsDashbrd.getTopAndBottomSalesReports("salesEngineers", null, null, null, null, "asc", null, cter);
                }
                else if (strUserRole == "TM")
                {
                    dtTopSalesEngineers = objRprtsDashbrd.getTopAndBottomSalesReports_ds("salesEngineers", strSelectedBranch, UserId, null, null, desc, null);
                    // dtBottomSalesEngineers = objRprtsDashbrd.getTopAndBottomSalesReports("salesEngineers", null, UserId, null, null, asc);
                }
                else if (strUserRole == "BM")
                {
                    dtcnsldtd = objRprtsDashbrd.getValueSum(BranchCode, null);
                    dtTopSalesEngineers = objRprtsDashbrd.getTopAndBottomSalesReports_ds("salesEngineers", BranchCode, null, null, null, desc, null);
                    //dtBottomSalesEngineers = objRprtsDashbrd.getTopAndBottomSalesReports("salesEngineers", BranchCode, null, null, null, asc, null);
                }
                else if (strUserRole == "SE")
                {
                }

                current_tab = 2;
                Loadgraphs("SE");
                //divcnsldtd.Visible = true;
                divbranches.Visible = false;
                //ChartBranch.Visible = false;
                //ChartTopSEAch.Visible = true;
                divSE.Visible = true;
                divCustomers.Visible = false;
                divCP.Visible = false;
                divGold.Visible = false;
                divGoldCP.Visible = false;
                divFamily.Visible = false;
                //divlnkn_SE.Attributes.Add("Class", "external-event active");
                //divlnkn_branches.Attributes.Add("Class", "external-event");
                //divlnkn_CP.Attributes.Add("Class", "external-event");
                //divlnkn_Customers.Attributes.Add("Class", "external-event");
                //divlnkn_Gold.Attributes.Add("Class", "external-event");
                //divlnk_Family.Attributes.Add("Class", "external-event");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "$('#MainContent_GridTopSE').append('<tfoot><tr> <th style=\"text-align:right\">TOTAL:</th><th style=\"text-align: right\"></th><th style=\"text-align: right\">" + hdn_Sales_value_year_0.Value + "</th><th style=\"text-align: right\">" + hdn_budget_value.Value + "</th><th style=\"text-align: right\">" + hdn_ytd_plan.Value + "</th><th style=\"text-align: right\">" + hdn_Sales_ytd_value.Value + "</th><th style=\"text-align: right\">" + hdn_askrate.Value + "</th><th style=\"text-align: right\">" + hdn_achvmnt.Value + "</th><th style=\"text-align: right\">" + hdn_growth.Value + "</th></tr></tfoot>'); ", true);
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "LoadCharts();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }
        protected void customers_perf(object sender, EventArgs e)
        {
            try
            {
                hdnsearch.Value = "CUST";
                strUserRole = Session["RoleId"].ToString();
                if (strUserRole == "HO" || strUserRole == "TM")
                {
                    divBranch.Visible = true;
                }
                else
                {
                    divBranch.Visible = false;
                }
                UserId = Session["UserId"].ToString();
                BranchCode = Session["BranchCode"].ToString();
                string budget_val = "2500000";
                if (Convert.ToString(Session["DashboardBranchList"]) == "ALL")
                    strSelectedBranch = null;
                else
                    strSelectedBranch = Convert.ToString(Session["DashboardBranchList"]);
                if (strUserRole == "HO")
                {
                    dtcnsldtd = objRprtsDashbrd.getValueSum(strSelectedBranch, null, null, null, null, null, "C", cter);
                    dtTopCustomers = objRprtsDashbrd.getTopAndBottomSalesReports_ds("Customers", strSelectedBranch, null, null, "C", desc, budget_val, cter);
                    //dtBottomCustomers = objRprtsDashbrd.getTopAndBottomSalesReports_ds("BtmCustomers", null, null, null, "C", asc, budget_val, cter);
                }
                else if (strUserRole == "TM")
                {
                    dtcnsldtd = objRprtsDashbrd.getValueSum(UserId, null, null, null, null, null, "C");
                    dtTopCustomers = objRprtsDashbrd.getTopAndBottomSalesReports_ds("Customers", strSelectedBranch, UserId, null, "C", desc, budget_val);
                    //dtBottomCustomers = objRprtsDashbrd.getTopAndBottomSalesReports_ds("BtmCustomers", null, UserId, null, "C", asc, budget_val);
                }
                else if (strUserRole == "BM")
                {
                    dtcnsldtd = objRprtsDashbrd.getValueSum(BranchCode, null, null, null, null, null, "C");
                    dtTopCustomers = objRprtsDashbrd.getTopAndBottomSalesReports_ds("Customers", BranchCode, null, null, "C", desc, budget_val);
                    //dtBottomCustomers = objRprtsDashbrd.getTopAndBottomSalesReports_ds("BtmCustomers", BranchCode, null, null, "C", asc, budget_val);
                }
                else if (strUserRole == "SE")
                {
                    dtcnsldtd = objRprtsDashbrd.getValueSum(null, null, UserId, null, null, null, "C");
                    dtTopCustomers = objRprtsDashbrd.getTopAndBottomSalesReports_ds("Customers", null, null, UserId, "C", desc, budget_val);
                    // dtBottomCustomers = objRprtsDashbrd.getTopAndBottomSalesReports_ds("BtmCustomers", null, null, UserId, "C", asc, budget_val);
                }

                current_tab = 3;
                Loadgraphs("CUSTOMER");
                //divcnsldtd.Visible = true;
                divbranches.Visible = false;
                //ChartBranch.Visible = false;
                divSE.Visible = false;
                divCustomers.Visible = true;
                divCP.Visible = false;
                divGold.Visible = false;
                divGoldCP.Visible = false;
                divFamily.Visible = false;
                //divlnkn_SE.Attributes.Add("Class", "external-event");
                //divlnkn_branches.Attributes.Add("Class", "external-event");
                //divlnkn_CP.Attributes.Add("Class", "external-event");
                //divlnkn_Customers.Attributes.Add("Class", "external-event active");
                //divlnkn_Gold.Attributes.Add("Class", "external-event");
                //divlnk_Family.Attributes.Add("Class", "external-event");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "$('#MainContent_GridCustByAch').append('<tfoot><tr> <th style=\"text-align:right\">TOTAL:</th><th style=\"text-align: right\"></th><th style=\"text-align: right\"></th><th style=\"text-align: right\"></th><th style=\"text-align: right\">" + hdn_Sales_value_year_0.Value + "</th><th style=\"text-align: right\">" + hdn_budget_value.Value + "</th><th style=\"text-align: right\">" + hdn_ytd_plan.Value + "</th><th style=\"text-align: right\">" + hdn_Sales_ytd_value.Value + "</th><th style=\"text-align: right\">" + hdn_askrate.Value + "</th><th style=\"text-align: right\">" + hdn_achvmnt.Value + "</th><th style=\"text-align: right\">" + hdn_growth.Value + "</th></tr></tfoot>');", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }
        protected void cp_perf(object sender, EventArgs e)
        {
            try
            {
                hdnsearch.Value = "CP";
                strUserRole = Session["RoleId"].ToString();
                if (strUserRole == "HO" || strUserRole == "TM")
                {
                    divBranch.Visible = true;
                }
                else
                {
                    divBranch.Visible = false;
                }
                UserId = Session["UserId"].ToString();
                BranchCode = Session["BranchCode"].ToString();
                string budget_val = "3000000";
                if (Convert.ToString(Session["DashboardBranchList"]) == "ALL")
                    strSelectedBranch = null;
                else
                    strSelectedBranch = Convert.ToString(Session["DashboardBranchList"]);
                if (strUserRole == "HO")
                {
                    dtcnsldtd = objRprtsDashbrd.getValueSum(strSelectedBranch, null, null, null, null, null, "D", cter);
                    dtTopChannelPartners = objRprtsDashbrd.getTopAndBottomSalesReports_ds("Customers", strSelectedBranch, null, null, "D", desc, budget_val, cter);
                    //dtBottomChannelPartners = objRprtsDashbrd.getTopAndBottomSalesReports("BtmCustomers", null, null, null, "D", asc, budget_val, cter);
                }
                else if (strUserRole == "TM")
                {
                    dtcnsldtd = objRprtsDashbrd.getValueSum(UserId, null, null, null, null, null, "D");
                    dtTopChannelPartners = objRprtsDashbrd.getTopAndBottomSalesReports_ds("Customers", strSelectedBranch, UserId, null, "D", desc, budget_val);
                    //dtBottomChannelPartners = objRprtsDashbrd.getTopAndBottomSalesReports("BtmCustomers", null, UserId, null, "D", asc, budget_val);
                }
                else if (strUserRole == "BM")
                {
                    dtcnsldtd = objRprtsDashbrd.getValueSum(BranchCode, null, null, null, null, null, "D");
                    dtTopChannelPartners = objRprtsDashbrd.getTopAndBottomSalesReports_ds("Customers", BranchCode, null, null, "D", desc, budget_val);
                    //dtBottomChannelPartners = objRprtsDashbrd.getTopAndBottomSalesReports("BtmCustomers", BranchCode, null, null, "D", asc, budget_val);
                }
                else if (strUserRole == "SE")
                {
                    dtcnsldtd = objRprtsDashbrd.getValueSum(null, null, UserId, null, null, null, "D");
                    dtTopChannelPartners = objRprtsDashbrd.getTopAndBottomSalesReports_ds("Customers", null, null, UserId, "D", desc, budget_val);
                    //dtBottomChannelPartners = objRprtsDashbrd.getTopAndBottomSalesReports("BtmCustomers", null, null, UserId, "D", asc, budget_val);
                }
                //if (dtTopChannelPartners.Tables.Count > 0)
                //{
                //    if (dtTopChannelPartners.Tables[0].Rows.Count > 0)
                //    {
                //        //dtTopBranches = dtTopSalesEngineers.Tables[0];
                //        DataRow[] foundRows = dtTopChannelPartners.Tables[0].Select("customer_number='Total'");
                //        hdn_Sales_value_year_0.Value = Convert.ToString(Convert.ToInt64(foundRows[0]["Sales_value_year_0"]) / byValueIn);
                //        hdn_budget_value.Value = Convert.ToString(Convert.ToInt64(foundRows[0]["budget_value"]) / byValueIn);
                //        hdn_Sales_ytd_value.Value = Convert.ToString(Convert.ToInt64(foundRows[0]["Sales_ytd_value"]) / byValueIn);
                //        hdn_ytd_plan.Value = Convert.ToString(Convert.ToInt64(foundRows[0]["ytd_plan"]) / byValueIn);
                //        hdn_achvmnt.Value = Convert.ToString(Convert.ToInt64(foundRows[0]["achvmnt"]) / byValueIn);
                //        hdn_askrate.Value = Convert.ToString(foundRows[0]["askrate"]);
                //        hdn_growth.Value = Convert.ToString(foundRows[0]["growth"]);
                //        foundRows[0].Delete();
                //        dtTopChannelPartners.Tables[0].AcceptChanges();
                //    }
                //}
                current_tab = 4;
                Loadgraphs("CP");
                //divcnsldtd.Visible = true;
                divbranches.Visible = false;
                //ChartBranch.Visible = false;
                divSE.Visible = false;
                divCustomers.Visible = false;
                divCP.Visible = true;
                divGold.Visible = false;
                divGoldCP.Visible = false;
                divFamily.Visible = false;
                //divlnkn_SE.Attributes.Add("Class", "external-event");
                //divlnkn_branches.Attributes.Add("Class", "external-event");
                //divlnkn_CP.Attributes.Add("Class", "external-event active");
                //divlnkn_Customers.Attributes.Add("Class", "external-event");
                //divlnkn_Gold.Attributes.Add("Class", "external-event");
                //divlnk_Family.Attributes.Add("Class", "external-event");
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "$('#MainContent_GridTopCP').append('<tfoot><tr> <th style=\"text-align:right\">TOTAL:</th><th style=\"text-align: right\"></th><th style=\"text-align: right\"></th><th style=\"text-align: right\"></th><th style=\"text-align: right\">" + hdn_Sales_value_year_0.Value + "</th><th style=\"text-align: right\">" + hdn_budget_value.Value + "</th><th style=\"text-align: right\">" + hdn_ytd_plan.Value + "</th><th style=\"text-align: right\">" + hdn_Sales_ytd_value.Value + "</th><th style=\"text-align: right\">" + hdn_askrate.Value + "</th><th style=\"text-align: right\">" + hdn_achvmnt.Value + "</th><th style=\"text-align: right\">" + hdn_growth.Value + "</th></tr></tfoot>');", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }
        protected void goldfamily_perf(object sender, EventArgs e)
        {
            try
            {
                hdnsearch.Value = "GOLD";
                strUserRole = Session["RoleId"].ToString();
                if (strUserRole == "HO" || strUserRole == "TM")
                {
                    divBranch.Visible = true;
                }
                else
                {
                    divBranch.Visible = false;
                }
                UserId = Session["UserId"].ToString();
                BranchCode = Session["BranchCode"].ToString();
                if (Convert.ToString(Session["DashboardBranchList"]) == "ALL")
                    strSelectedBranch = null;
                else
                    strSelectedBranch = Convert.ToString(Session["DashboardBranchList"]);
                if (strUserRole == "HO")
                {
                    hdnGold.Value = "HO";
                    dtcnsldtd = objRprtsDashbrd.getValueSum(strSelectedBranch, null, null, null, "GOLD", null, null, cter);
                    dtGold = objRprtsDashbrd.getTopAndBottomSalesReports_ds("Gold", strSelectedBranch, null, null, null, desc, null, cter);
                    if (dtGold.Tables.Count > 0)
                    {
                        if (dtGold.Tables[0].Rows.Count > 0)
                        {
                            dtGoldBranches = dtGold.Tables[0];
                        }
                        if (dtGold.Tables[1].Rows.Count > 0)
                        {
                            dtGoldBranches_Growth = dtGold.Tables[1];
                        }
                        if (dtGold.Tables[2].Rows.Count > 0)
                        {
                            dtTopGoldSe = dtGold.Tables[2];
                            dtTopGoldSe_withBranch = dtGold.Tables[2];
                        }
                        if (dtGold.Tables[3].Rows.Count > 0)
                        {
                            dtTopGoldSe_Growth = dtGold.Tables[3];
                        }
                        if (dtGold.Tables[4].Rows.Count > 0)
                        {
                            dtBottomGoldSe = dtGold.Tables[4];
                        }
                        if (dtGold.Tables[5].Rows.Count > 0)
                        {
                            dtBottomGoldSe_Growth = dtGold.Tables[5];
                        }
                        if (dtGold.Tables[6].Rows.Count > 0)
                        {
                            dtTopGoldCustomers = dtGold.Tables[6];
                            dtTopGoldCustomers_withBranch = dtGold.Tables[6];
                        }
                        if (dtGold.Tables[7].Rows.Count > 0)
                        {
                            dtTopGoldCustomers_Growth = dtGold.Tables[7];
                        }
                        if (dtGold.Tables[8].Rows.Count > 0)
                        {
                            dtTopGoldChannelPartners = dtGold.Tables[8];
                            dtTopGoldChannelPartners_WithBranch = dtGold.Tables[8];
                        }
                        if (dtGold.Tables[9].Rows.Count > 0)
                        {
                            dtGoldChannelPartners_Growth = dtGold.Tables[9];
                        }

                    }


                    //dtGoldBranches = objRprtsDashbrd.getTopAndBottomSalesReports("GoldBranches", null, null, null, null, desc, null, cter);
                    //dtTopGoldSe = objRprtsDashbrd.getTopAndBottomSalesReports("GoldSE", null, null, null, null, desc, null, cter);
                    //dtBottomGoldSe = objRprtsDashbrd.getTopAndBottomSalesReports("GoldSE", null, null, null, null, asc, null, cter);
                    //dtTopGoldCustomers = objRprtsDashbrd.getTopAndBottomSalesReports("GoldCustomers", null, null, null, "C", desc, null, cter);
                    //dtTopGoldChannelPartners = objRprtsDashbrd.getTopAndBottomSalesReports("GoldCustomers", null, null, null, "D", desc, null, cter);

                }
                else if (strUserRole == "TM")
                {
                    hdnGold.Value = "TM";
                    dtcnsldtd = objRprtsDashbrd.getValueSum(UserId, null, null, null, "GOLD", null, null);
                    dtGold = objRprtsDashbrd.getTopAndBottomSalesReports_ds("Gold", UserId, null, null, null, desc, null, cter);
                    if (dtGold.Tables.Count > 0)
                    {
                        if (dtGold.Tables[0].Rows.Count > 0)
                        {
                            dtGoldBranches = dtGold.Tables[0];
                        }
                        if (dtGold.Tables[1].Rows.Count > 0)
                        {
                            dtGoldBranches_Growth = dtGold.Tables[1];
                        }
                        if (dtGold.Tables[2].Rows.Count > 0)
                        {
                            dtTopGoldSe_withBranch = dtGold.Tables[2];
                            dtTopGoldSe = dtGold.Tables[2];
                        }
                        if (dtGold.Tables[3].Rows.Count > 0)
                        {
                            dtTopGoldSe_Growth = dtGold.Tables[3];
                        }
                        if (dtGold.Tables[4].Rows.Count > 0)
                        {
                            dtBottomGoldSe = dtGold.Tables[4];
                        }
                        if (dtGold.Tables[5].Rows.Count > 0)
                        {
                            dtBottomGoldSe_Growth = dtGold.Tables[5];
                        }
                        if (dtGold.Tables[6].Rows.Count > 0)
                        {
                            dtTopGoldCustomers = dtGold.Tables[6];
                            dtTopGoldCustomers_withBranch = dtGold.Tables[6];
                        }
                        if (dtGold.Tables[7].Rows.Count > 0)
                        {
                            dtTopGoldCustomers_Growth = dtGold.Tables[7];
                        }
                        if (dtGold.Tables[8].Rows.Count > 0)
                        {
                            dtTopGoldChannelPartners = dtGold.Tables[8];
                            dtTopGoldChannelPartners_WithBranch = dtGold.Tables[8];
                        }
                        if (dtGold.Tables[9].Rows.Count > 0)
                        {
                            dtGoldChannelPartners_Growth = dtGold.Tables[9];
                        }

                    }
                    //dtGoldBranches = objRprtsDashbrd.getTopAndBottomSalesReports("GoldBranches", UserId, null, null, null, desc, null);
                    //dtTopGoldSe = objRprtsDashbrd.getTopAndBottomSalesReports("GoldSE", UserId, null, null, null, desc, null);
                    //dtBottomGoldSe = objRprtsDashbrd.getTopAndBottomSalesReports("GoldSE", UserId, null, null, null, asc, null);
                    //dtTopGoldCustomers = objRprtsDashbrd.getTopAndBottomSalesReports("GoldCustomers", UserId, null, null, "C", desc, null);
                    //dtTopGoldChannelPartners = objRprtsDashbrd.getTopAndBottomSalesReports("GoldCustomers", UserId, null, null, "D", desc, null);
                }
                else if (strUserRole == "BM")
                {
                    hdnGold.Value = "BM";
                    dtcnsldtd = objRprtsDashbrd.getValueSum(BranchCode, null, null, null, "GOLD", null, null);
                    dtGold = objRprtsDashbrd.getTopAndBottomSalesReports_ds("Gold", BranchCode, null, null, null, desc, null, cter);
                    if (dtGold.Tables.Count > 0)
                    {
                        if (dtGold.Tables[2].Rows.Count > 0)
                        {

                            dtTopGoldSe = dtGold.Tables[2];
                            dtTopGoldSe_withBranch = dtGold.Tables[2];
                        }
                        if (dtGold.Tables[3].Rows.Count > 0)
                        {
                            dtTopGoldSe_Growth = dtGold.Tables[3];
                        }
                        if (dtGold.Tables[4].Rows.Count > 0)
                        {
                            dtBottomGoldSe = dtGold.Tables[4];
                        }
                        if (dtGold.Tables[5].Rows.Count > 0)
                        {
                            dtBottomGoldSe_Growth = dtGold.Tables[5];
                        }
                        if (dtGold.Tables[6].Rows.Count > 0)
                        {
                            dtTopGoldCustomers = dtGold.Tables[6];
                            dtTopGoldCustomers_withBranch = dtGold.Tables[6];
                        }
                        if (dtGold.Tables[7].Rows.Count > 0)
                        {
                            dtTopGoldCustomers_Growth = dtGold.Tables[7];
                        }
                        if (dtGold.Tables[8].Rows.Count > 0)
                        {
                            dtTopGoldChannelPartners = dtGold.Tables[8];
                            dtTopGoldChannelPartners_WithBranch = dtGold.Tables[8];
                        }
                        if (dtGold.Tables[9].Rows.Count > 0)
                        {
                            dtGoldChannelPartners_Growth = dtGold.Tables[9];
                        }

                    }
                    //dtTopGoldSe = objRprtsDashbrd.getTopAndBottomSalesReports("GoldSE", BranchCode, null, null, null, desc, null);
                    //dtBottomGoldSe = objRprtsDashbrd.getTopAndBottomSalesReports("GoldSE", BranchCode, null, null, null, asc, null);
                    //dtTopGoldCustomers = objRprtsDashbrd.getTopAndBottomSalesReports("GoldCustomers", BranchCode, null, null, "C", desc, null);
                    //dtTopGoldChannelPartners = objRprtsDashbrd.getTopAndBottomSalesReports("GoldCustomers", BranchCode, null, null, "D", desc, null);
                }
                else if (strUserRole == "SE")
                {
                    hdnGold.Value = "SE";
                    dtcnsldtd = objRprtsDashbrd.getValueSum(null, null, UserId, null, "GOLD", null, null);
                    dtGold = objRprtsDashbrd.getTopAndBottomSalesReports_ds("Gold", null, null, UserId, null, desc, null, cter);
                    if (dtGold.Tables.Count > 0)
                    {
                        if (dtGold.Tables[6].Rows.Count > 0)
                        {
                            dtTopGoldCustomers = dtGold.Tables[6];
                            dtTopGoldCustomers_withBranch = dtGold.Tables[6];
                        }
                        if (dtGold.Tables[7].Rows.Count > 0)
                        {
                            dtTopGoldCustomers_Growth = dtGold.Tables[7];
                        }
                        if (dtGold.Tables[8].Rows.Count > 0)
                        {
                            dtTopGoldChannelPartners = dtGold.Tables[8];
                            dtTopGoldChannelPartners_WithBranch = dtGold.Tables[8];
                        }
                        if (dtGold.Tables[9].Rows.Count > 0)
                        {
                            dtGoldChannelPartners_Growth = dtGold.Tables[9];
                        }

                    }
                    //dtTopGoldCustomers = objRprtsDashbrd.getTopAndBottomSalesReports("GoldCustomers", null, null, UserId, "C", desc, null);
                    ////dtBottomGoldCustomers = dtTopGoldCustomers;// objRprtsDashbrd.getTopAndBottomSalesReports("GoldCustomers", null, null, UserId, "C", asc);
                    //dtTopGoldChannelPartners = objRprtsDashbrd.getTopAndBottomSalesReports("GoldCustomers", null, null, UserId, "D", desc);
                    ////dtBottomGoldChannelPartners = dtTopGoldChannelPartners;// objRprtsDashbrd.getTopAndBottomSalesReports("GoldCustomers", null, null, UserId, "D", asc);
                }


                //if (dtTopGoldCustomers.Rows.Count > 0)
                //{
                //    //dtTopBranches = dtTopSalesEngineers.Tables[0];
                //    DataRow[] foundRows = dtTopGoldCustomers.Select("customer_number='Total'");
                //    if (foundRows.Length > 0)
                //    {
                //        cus_Sales_value_year_0 = Convert.ToString(Convert.ToInt64(foundRows[0]["Sales_value_year_0"]) / byValueIn);
                //        cus_budget_value = Convert.ToString(Convert.ToInt64(foundRows[0]["budget_value"]) / byValueIn);
                //        cus_Sales_ytd_value = Convert.ToString(Convert.ToInt64(foundRows[0]["Sales_ytd_value"]) / byValueIn);
                //        cus_ytd_plan = Convert.ToString(Convert.ToInt64(foundRows[0]["ytd_plan"]) / byValueIn);
                //        cus_achvmnt = Convert.ToString(Convert.ToInt64(foundRows[0]["achvmnt"]) / byValueIn);
                //        cus_askrate = Convert.ToString(foundRows[0]["askrate"]);
                //        // cus_NOVALUES = Convert.ToString(foundRows[0]["NOVALUES"]);
                //        cus_growth = Convert.ToString(foundRows[0]["growth"]);
                //        foundRows[0].Delete();
                //        dtTopGoldCustomers.AcceptChanges();
                //    }
                //    foundRows = dtTopGoldCustomers_withBranch.Select("customer_number='Total'");
                //    if (foundRows.Length > 0)
                //    {
                //        foundRows[0].Delete();
                //        dtTopGoldCustomers_withBranch.AcceptChanges();
                //    }
                //}

                //if (dtTopGoldChannelPartners.Rows.Count > 0)
                //{
                //    //dtTopBranches = dtTopSalesEngineers.Tables[0];
                //    DataRow[] foundRows = dtTopGoldChannelPartners.Select("customer_number='Total'");
                //    if (foundRows.Length > 0)
                //    {
                //        cp_Sales_value_year_0 = Convert.ToString(Convert.ToInt64(foundRows[0]["Sales_value_year_0"]) / byValueIn);
                //        cp_budget_value = Convert.ToString(Convert.ToInt64(foundRows[0]["budget_value"]) / byValueIn);
                //        cp_Sales_ytd_value = Convert.ToString(Convert.ToInt64(foundRows[0]["Sales_ytd_value"]) / byValueIn);
                //        cp_ytd_plan = Convert.ToString(Convert.ToInt64(foundRows[0]["ytd_plan"]) / byValueIn);
                //        cp_achvmnt = Convert.ToString(Convert.ToInt64(foundRows[0]["achvmnt"]) / byValueIn);
                //        cp_askrate = Convert.ToString(foundRows[0]["askrate"]);
                //        //cp_NOVALUES = Convert.ToString(foundRows[0]["NOVALUES"]);
                //        cp_growth = Convert.ToString(foundRows[0]["growth"]);
                //        foundRows[0].Delete();
                //        dtTopGoldChannelPartners.AcceptChanges();
                //    }
                //    foundRows = dtTopGoldChannelPartners_WithBranch.Select("customer_number='Total'");
                //    if (foundRows.Length > 0)
                //    {
                //        foundRows[0].Delete();
                //        dtTopGoldChannelPartners_WithBranch.AcceptChanges();
                //    }
                //    Session["dtTopGoldChannelPartners_WithBranch"] = dtTopGoldChannelPartners_WithBranch;
                //}

                //if (dtTopGoldSe.Rows.Count > 0)
                //{
                //    //dtTopBranches = dtTopSalesEngineers.Tables[0];
                //    DataRow[] foundRows = dtTopGoldSe.Select("EngineerName='Total'");
                //    if (foundRows.Length > 0)
                //    {
                //        se_Sales_value_year_0 = Convert.ToString(Convert.ToInt64(foundRows[0]["Sales_value_year_0"]) / byValueIn);
                //        se_budget_value = Convert.ToString(Convert.ToInt64(foundRows[0]["budget_value"]) / byValueIn);
                //        se_Sales_ytd_value = Convert.ToString(Convert.ToInt64(foundRows[0]["Sales_ytd_value"]) / byValueIn);
                //        se_ytd_plan = Convert.ToString(Convert.ToInt64(foundRows[0]["ytd_plan"]) / byValueIn);
                //        se_achvmnt = Convert.ToString(Convert.ToInt64(foundRows[0]["achvmnt"]) / byValueIn);
                //        se_askrate = Convert.ToString(foundRows[0]["askrate"]);
                //        // se_NOVALUES = Convert.ToString(foundRows[0]["NOVALUES"]);
                //        se_growth = Convert.ToString(foundRows[0]["growth"]);
                //        foundRows[0].Delete();
                //        dtTopGoldSe.AcceptChanges();
                //    }
                //    foundRows = dtTopGoldSe_withBranch.Select("EngineerName='Total'");
                //    if (foundRows.Length > 0)
                //    {
                //        foundRows[0].Delete();
                //        dtTopGoldSe_withBranch.AcceptChanges();
                //    }
                //}

                //if (dtGoldBranches.Rows.Count > 0)
                //{
                //    //dtTopBranches = dtTopSalesEngineers.Tables[0];
                //    DataRow[] foundRows = dtGoldBranches.Select("region_description='Total'");
                //    if (foundRows.Length > 0)
                //    {
                //        branch_Sales_value_year_0 = Convert.ToString(Convert.ToInt64(foundRows[0]["Sales_value_year_0"]) / byValueIn);
                //        branch_budget_value = Convert.ToString(Convert.ToInt64(foundRows[0]["budget_value"]) / byValueIn);
                //        branch_Sales_ytd_value = Convert.ToString(Convert.ToInt64(foundRows[0]["Sales_ytd_value"]) / byValueIn);
                //        branch_ytd_plan = Convert.ToString(Convert.ToInt64(foundRows[0]["ytd_plan"]) / byValueIn);
                //        branch_achvmnt = Convert.ToString(Convert.ToInt64(foundRows[0]["achvmnt"]) / byValueIn);
                //        branch_askrate = Convert.ToString(foundRows[0]["askrate"]);
                //        //branch_NOVALUES = Convert.ToString(foundRows[0]["NOVALUES"]);
                //        branch_growth = Convert.ToString(foundRows[0]["growth"]);
                //        foundRows[0].Delete();
                //        dtGoldBranches.AcceptChanges();
                //    }
                //}
                Loadgraphs("GOLD");
                current_tab = 5;
                divbranches.Visible = false;
                //ChartBranch.Visible = false;
                divSE.Visible = false;
                divCustomers.Visible = false;
                divCP.Visible = false;
                divGold.Visible = true;
                divGoldCP.Visible = true;
                divFamily.Visible = false;
                //divlnkn_SE.Attributes.Add("Class", "external-event ");
                //divlnkn_branches.Attributes.Add("Class", "external-event");
                //divlnkn_CP.Attributes.Add("Class", "external-event");
                //divlnkn_Customers.Attributes.Add("Class", "external-event");
                //divlnkn_Gold.Attributes.Add("Class", "external-event active");
                //divlnk_Family.Attributes.Add("Class", "external-event");
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }
        protected void family_perf(object sender, EventArgs e)
        {
            try
            {
                hdnsearch.Value = "FAMILY";

                strUserRole = Session["RoleId"].ToString();
                if (strUserRole == "HO" || strUserRole == "TM")
                {
                    divBranch.Visible = true;
                }
                else
                {
                    divBranch.Visible = false;
                }
                UserId = Session["UserId"].ToString();
                BranchCode = Session["BranchCode"].ToString();
                if (Convert.ToString(Session["DashboardBranchList"]) == "ALL")
                    strSelectedBranch = Convert.ToString(Session["DashboardBranchList"]);
                else
                    strSelectedBranch = Convert.ToString(Session["DashboardBranchList"]);
                if (strUserRole == "HO")
                {
                    dtcnsldtd = objRprtsDashbrd.getValueSum(strSelectedBranch, null, null, null, null, null, null, cter);
                    dtFamily = objRprtsDashbrd.getTopAndBottomSalesReports("Family", strSelectedBranch, "ALL", "ALL", "ALL", null, null, cter);
                }
                else if (strUserRole == "TM")
                {
                    dtcnsldtd = objRprtsDashbrd.getValueSum(UserId, null);
                    dtFamily = objRprtsDashbrd.getTopAndBottomSalesReports("Family", strSelectedBranch, UserId, "ALL", "ALL", "ALL", "ALL");
                }
                else if (strUserRole == "BM")
                {
                    dtcnsldtd = objRprtsDashbrd.getValueSum(BranchCode, null);
                    dtFamily = objRprtsDashbrd.getTopAndBottomSalesReports("Family", BranchCode, "ALL", "ALL", "ALL", "ALL");
                }
                else if (strUserRole == "SE")
                {
                    dtcnsldtd = objRprtsDashbrd.getValueSum(null, null, UserId, null, null, null, null);
                    dtFamily = objRprtsDashbrd.getTopAndBottomSalesReports("Family", "ALL", "ALL", UserId, "ALL", "desc");
                }
                //Loadgraphs();
                //if (dtFamily.Rows.Count > 0)
                //{
                //    //dtTopBranches = dtTopSalesEngineers.Tables[0];
                //    DataRow[] foundRows = dtFamily.Select("item_family_name='Total'");
                //    hdn_Sales_value_year_0.Value = Convert.ToString(Convert.ToInt64(foundRows[0]["Sales_value_year_0"]) / byValueIn);
                //    hdn_budget_value.Value = Convert.ToString(Convert.ToInt64(foundRows[0]["budget_value"]) / byValueIn);
                //    hdn_Sales_ytd_value.Value = Convert.ToString(Convert.ToInt64(foundRows[0]["Sales_ytd_value"]) / byValueIn);
                //    hdn_ytd_plan.Value = Convert.ToString(Convert.ToInt64(foundRows[0]["ytd_plan"]) / byValueIn);
                //    hdn_achvmnt.Value = Convert.ToString(Convert.ToInt64(foundRows[0]["achvmnt"]) / byValueIn);
                //    hdn_askrate.Value = Convert.ToString(foundRows[0]["askrate"]);
                //    hdn_growth.Value = Convert.ToString(foundRows[0]["growth"]);
                //    foundRows[0].Delete();
                //    dtFamily.AcceptChanges();
                //}

                current_tab = 6;
                Loadgraphs("FAMILY");
                //divcnsldtd.Visible = true;
                divbranches.Visible = false;
                //ChartBranch.Visible = false;
                divSE.Visible = false;
                divCustomers.Visible = false;
                divCP.Visible = false;
                divGold.Visible = false;
                divGoldCP.Visible = false;
                divFamily.Visible = true;
                //divlnkn_SE.Attributes.Add("Class", "external-event");
                //divlnkn_branches.Attributes.Add("Class", "external-event");
                //divlnkn_CP.Attributes.Add("Class", "external-event");
                //divlnkn_Customers.Attributes.Add("Class", "external-event");
                //divlnkn_Gold.Attributes.Add("Class", "external-event");
                //divlnk_Family.Attributes.Add("Class", "external-event active");
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        protected void BranchList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string name_desc = "", name_code = "";
                int count = 0;
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

                foreach (ListItem val in BranchList.Items)
                {
                    if (val.Selected)
                    {
                        count++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                    }
                }

                name_code = "'" + name_code;
                // txtbranchlist.Text = name_desc;

                string branchlist = name_code.Substring(0, Math.Max(0, name_code.Length - 2));

                if (count == BranchList.Items.Count)
                {
                    Session["DashboardBranchList"] = "ALL";
                }
                else
                {
                    Session["DashboardBranchList"] = branchlist;
                }
                LoadData();
                //ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "LoadCharts();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }
        protected void byValueIn_CheckedChanged(Object sender, EventArgs e)
        {
            try
            {
                if (ValInThsnd.Checked)
                {
                    byValueIn = 1000;
                    //Loadgraphs("ALL");
                    //LoadFamilyGraph();
                    ddlPerfBy_SelectedIndexChanged(null, null);
                }

                if (ValInLakhs.Checked)
                {
                    byValueIn = 100000;
                    //Loadgraphs("ALL");
                    //LoadFamilyGraph();
                    ddlPerfBy_SelectedIndexChanged(null, null);
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "LoadCharts();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        protected void gridheader_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                int BudgetYear = objConfig.getBudgetYear();
                int ActualYear = objConfig.getActualYear() - 1;
                string ActualValueyear1 = (ActualYear).ToString();
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    if (e.Row.Cells[1].Text == "BRANCH")
                    {
                        e.Row.Cells[2].Text = "YTD SALE" + " " + ActualValueyear1;
                        e.Row.Cells[3].Text = BudgetYear + "B";
                        e.Row.Cells[4].Text = "YTD PLAN" + " " + BudgetYear;
                        e.Row.Cells[5].Text = "YTD SALE" + " " + BudgetYear;

                        e.Row.Cells[6].Text = BudgetYear + "B" + " <br/> ASK.RATE";
                        e.Row.Cells[7].Text = "PRO-RATA ACH%";
                        e.Row.Cells[8].Text = "GROWTH%";
                    }
                    else
                    {
                        e.Row.Cells[1].Text = "YTD SALE" + " " + ActualValueyear1;
                        e.Row.Cells[2].Text = BudgetYear + "B";
                        e.Row.Cells[3].Text = "YTD PLAN" + " " + BudgetYear;
                        e.Row.Cells[4].Text = "YTD SALE" + " " + BudgetYear;

                        e.Row.Cells[5].Text = BudgetYear + "B" + " <br/> ASK.RATE";
                        e.Row.Cells[6].Text = "PRO-RATA ACH%";
                        e.Row.Cells[7].Text = "GROWTH%";
                    }
                }
                //if (e.Row.Cells[1].Text == "YTD SALE" + " " + ActualValueyear1)
                //{
                //    TextBox textBox = new TextBox();
                //    textBox.ID = "txtDynamicText";
                //    //textBox.Attributes.Add("runat", "server");
                //    textBox.CssClass = "Color";
                //    this.gridtopbranches.Rows[-1].Cells[1].Controls.Add(textBox);
                //}
                //if (e.Row.RowType == DataControlRowType.DataRow)
                //{
                //    if (e.Row.d.Cells[0].Text == "Total")
                //    {
                //        e.Row.CssClass = "grdview_total";
                //    }
                //}
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        protected void GridTopCstmrs_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    int BudgetYear = objConfig.getBudgetYear();
                    int ActualYear = objConfig.getActualYear() - 1;
                    string ActualValueyear1 = (ActualYear).ToString();
                    if (e.Row.Cells[2].Text == "BRANCH")
                    {
                        e.Row.Cells[4].Text = "YTD SALE" + " " + ActualValueyear1;
                        e.Row.Cells[5].Text = BudgetYear + "B";
                        e.Row.Cells[6].Text = "YTD PLAN" + " " + BudgetYear;
                        e.Row.Cells[7].Text = "YTD SALE" + " " + BudgetYear;
                        e.Row.Cells[8].Text = BudgetYear + "B" + " <br/> ASK.RATE";
                        e.Row.Cells[9].Text = "PRO-RATA ACH%";
                        e.Row.Cells[10].Text = "GROWTH%";
                    }
                    else
                    {
                        e.Row.Cells[2].Text = "YTD SALE" + " " + ActualValueyear1;
                        e.Row.Cells[3].Text = BudgetYear + "B";
                        e.Row.Cells[4].Text = "YTD PLAN" + " " + BudgetYear;
                        e.Row.Cells[5].Text = "YTD SALE" + " " + BudgetYear;
                        e.Row.Cells[6].Text = BudgetYear + "B" + " <br/> ASK.RATE";
                        e.Row.Cells[7].Text = "PRO-RATA ACH%";
                        e.Row.Cells[8].Text = "GROWTH%";
                    }
                }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        protected override object LoadPageStateFromPersistenceMedium()
        {
            System.Web.UI.PageStatePersister pageStatePersister1 = this.PageStatePersister;
            pageStatePersister1.Load();
            String vState = pageStatePersister1.ViewState.ToString();
            byte[] pBytes = System.Convert.FromBase64String(vState);
            pBytes = Decompress(pBytes);
            LosFormatter mFormat = new LosFormatter();
            Object ViewState = mFormat.Deserialize(System.Convert.ToBase64String(pBytes));
            return new Pair(pageStatePersister1.ControlState, ViewState);
        }

        protected override void SavePageStateToPersistenceMedium(Object pViewState)
        {
            Pair pair1;
            System.Web.UI.PageStatePersister pageStatePersister1 = this.PageStatePersister;
            Object ViewState;
            if (pViewState is Pair)
            {
                pair1 = ((Pair)pViewState);
                pageStatePersister1.ControlState = pair1.First;
                ViewState = pair1.Second;
            }
            else
            {
                ViewState = pViewState;
            }
            LosFormatter mFormat = new LosFormatter();
            StringWriter mWriter = new StringWriter();
            mFormat.Serialize(mWriter, ViewState);
            String mViewStateStr = mWriter.ToString();
            byte[] pBytes = System.Convert.FromBase64String(mViewStateStr);
            pBytes = Compress(pBytes);
            String vStateStr = System.Convert.ToBase64String(pBytes);
            pageStatePersister1.ViewState = vStateStr;
            pageStatePersister1.Save();
        }

        protected void rdBtnTaegutec_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (rdBtnTaegutec.Checked)
                {
                    Session["cter"] = "TTA";
                    cter = "TTA";
                    LoadBranches();
                    LoadData();
                }
                if (rdBtnDuraCab.Checked)
                {
                    Session["cter"] = "DUR";
                    cter = "DUR";
                    LoadBranches();
                    LoadData();
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "LoadCharts();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        #endregion

        #region Methods

        protected void LoadBranches()
        {
            try
            {
                string name_desc = "", name_code = "";
                int count = 0;
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                string roleId = Session["RoleId"].ToString();
                string userId = Session["UserId"].ToString();
                string branchcode = Session["BranchCode"].ToString();
                DataTable dtData = new DataTable();
                objRSum.BranchCode = userId; // passing here territory Engineer Id  as branch code IF role is TM 
                objRSum.roleId = roleId;
                objRSum.flag = "Branch";
                objRSum.cter = Convert.ToString(Session["cter"]);
                dtData = objRSum.getFilterAreaValue(objRSum);

                BranchList.DataSource = dtData;
                BranchList.DataTextField = "BranchDesc";
                BranchList.DataValueField = "BranchCode";
                BranchList.DataBind();
                // ChkBranches.Items.Insert(0, "ALL");

                foreach (ListItem val in BranchList.Items)
                {
                    val.Selected = true;
                    if (val.Selected)
                    {
                        count++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                    }
                }

                // ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }

            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }
        protected void LoadData()
        {
            dtTopBranches = new DataTable();
            dtTopSalesEngineers = new DataSet();
            dtTopCustomers = new DataSet();
            dtTopChannelPartners = new DataSet();
            dtTopGoldCustomers = new DataTable();
            dtTopGoldCustomers_withBranch = new DataTable();
            dtTopGoldChannelPartners = new DataTable();
            dtTopGoldChannelPartners_WithBranch = new DataTable();
            //dtBottomBranches = new DataTable();
            dtBottomChannelPartners = new DataTable();
            dtBottomCustomers = new DataSet();
            dtTopGoldSe = new DataTable();
            dtTopGoldSe_withBranch = new DataTable();
            dtBottomGoldSe = new DataTable();
            dtGoldBranches = new DataTable();
            dtBottomSalesEngineers = new DataTable();
            dtFamily = new DataTable();
            dtcnsldtd = new DataTable();

            //added by anamika
            dtGoldBranches_Growth = new DataTable();
            dtGoldChannelPartners_Growth = new DataTable();
            dtTopGoldCustomers_Growth = new DataTable();
            dtTopGoldSe_Growth = new DataTable();
            dtBottomGoldSe_Growth = new DataTable();
            dsTopBranches = new DataSet();
            dtTopBranches_Growth = new DataTable();
            // byValueIn = 1000;
            try
            {
                if (ValInThsnd.Checked)
                {
                    byValueIn = 1000;
                }

                if (ValInLakhs.Checked)
                {
                    byValueIn = 100000;
                }
                strUserRole = Session["RoleId"].ToString();

                if (strUserRole == "HO")
                {
                    if (Session["cter"].ToString() == "DUR")
                    {
                        rdBtnDuraCab.Checked = true;
                        rdBtnTaegutec.Checked = false;
                        cter = "DUR";
                    }
                    else
                    {
                        rdBtnTaegutec.Checked = true;
                        rdBtnDuraCab.Checked = false;
                        cter = "TTA";
                    }

                    if (current_tab == 1)
                    {
                        branch_perf(null, null);
                    }
                    else if (current_tab == 2)
                    {
                        se_perf(null, null);
                    }
                    else if (current_tab == 3)
                    {
                        customers_perf(null, null);
                    }
                    else if (current_tab == 4)
                    {
                        cp_perf(null, null);
                    }
                    else if (current_tab == 5)
                    {
                        goldfamily_perf(null, null);
                    }
                    else if (current_tab == 6)
                    {
                        family_perf(null, null);
                    }

                }
                else if (strUserRole == "TM")
                {
                    branch_perf(null, null);
                }
                else if (strUserRole == "BM")
                {
                    //divlnkn_branches.Visible = false;

                    string BranchCode = Session["BranchCode"].ToString();
                    se_perf(null, null);

                }
                else if (strUserRole == "SE")
                {
                    //divlnkn_branches.Visible = false;
                    //divlnkn_SE.Visible = false;
                    //lnkbtn_se.Visible = false;
                    string UserId = Session["UserId"].ToString();
                    customers_perf(null, null);
                }


            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }

        protected void Loadgraphs(string flag)
        {
            DataView view;
            DataTable distinctValues = new DataTable();
            DataTable dtCustomer;
            try
            {
                if (ValInThsnd.Checked)
                {
                    byValueIn = 1000;
                }

                if (ValInLakhs.Checked)
                {
                    byValueIn = 100000;
                }


                #region Consolidated bar graph
                //consolidated bar graph
                //bargraphcnsldtd.DataSource = chart_dtcnsldtd(dtcnsldtd);
                //int sales_value_yr_0 = objConfig.getActualYear() - 1;
                //int budget = objConfig.getBudgetYear();
                //string sls_year_0 = "SALE" + " " + sales_value_yr_0;
                //string budget_year = "BUDGET  " + budget;
                //string ytd_sales = "YTD SALE" + " " + budget;
                //string ytd_plan = "YTD PLAN" + " " + budget;
                //bargraphcnsldtd.Series.Clear();
                //bargraphcnsldtd.Series.Add(sls_year_0);
                //bargraphcnsldtd.Series.Add(budget_year);
                //bargraphcnsldtd.Series.Add(ytd_plan);
                //bargraphcnsldtd.Series.Add(ytd_sales);

                //bargraphcnsldtd.Series[sls_year_0].YValueMembers = "Sales_value_year_0";
                //bargraphcnsldtd.Series[budget_year].YValueMembers = "budget_value";
                //bargraphcnsldtd.Series[ytd_plan].YValueMembers = "ytd_plan";
                //bargraphcnsldtd.Series[ytd_sales].YValueMembers = "Sales_ytd_value";

                //bargraphcnsldtd.ChartAreas[0].AxisY.Title = "VALUE";
                //bargraphcnsldtd.ChartAreas[0].AxisX.Interval = 110;
                //bargraphcnsldtd.Series[sls_year_0].IsValueShownAsLabel = true;
                //bargraphcnsldtd.Series[budget_year].IsValueShownAsLabel = true;
                //bargraphcnsldtd.Series[ytd_sales].IsValueShownAsLabel = true;
                //bargraphcnsldtd.Series[ytd_plan].IsValueShownAsLabel = true;
                //bargraphcnsldtd.Series[sls_year_0].ToolTip = sls_year_0 + " " + ":" + " " + "#VALY";
                //bargraphcnsldtd.Series[budget_year].ToolTip = budget_year + " " + ":" + " " + "#VALY";
                //bargraphcnsldtd.Series[ytd_sales].ToolTip = ytd_sales + " " + ":" + " " + "#VALY";
                //bargraphcnsldtd.Series[ytd_plan].ToolTip = ytd_plan + " " + ":" + " " + "#VALY";
                //bargraphcnsldtd.Series[sls_year_0].Color = ColorTranslator.FromHtml("#097054");
                //bargraphcnsldtd.Series[budget_year].Color = ColorTranslator.FromHtml("#FFDE00");
                //bargraphcnsldtd.Series[ytd_sales].Color = ColorTranslator.FromHtml("#6599FF");
                //bargraphcnsldtd.Series[ytd_plan].Color = ColorTranslator.FromHtml("#FF9900");
                //bargraphcnsldtd.Series[sls_year_0].SetCustomProperty("DrawingStyle", "Cylinder");
                //bargraphcnsldtd.Series[budget_year].SetCustomProperty("DrawingStyle", "Cylinder");
                //bargraphcnsldtd.Series[ytd_sales].SetCustomProperty("DrawingStyle", "Cylinder");
                //bargraphcnsldtd.Series[ytd_plan].SetCustomProperty("DrawingStyle", "Cylinder");
                Session["Consolidated"] = GenerateTransposedTable(chart_dtcnsldtd(dtcnsldtd));
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "LoadChart", "LoadCharts()", true);
                //ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

                #endregion
                Random random = new Random();
                string[] column;
                if (flag == "BRANCH" || flag == "ALL")
                {
                    #region Branch
                    ////bar graph of top branches
                    //bargraphtopbranches.DataSource = chartDataTable_all(dtTopBranches);
                    //bargraphtopbranches.Series["Ach"].YValueMembers = "ach";
                    //bargraphtopbranches.Series["Ach"].XValueMember = "Name";
                    //bargraphtopbranches.ChartAreas[0].AxisY.Title = "PRO-RATA ACH%";
                    //bargraphtopbranches.ChartAreas[0].AxisX.TitleFont = new System.Drawing.Font("Arial", 11);
                    //bargraphtopbranches.DataBind();

                    //bargraphtopbranches_growth.DataSource = chartDataTable_all(dtTopBranches_Growth);
                    //bargraphtopbranches_growth.Series["Ach"].YValueMembers = "growth";
                    //bargraphtopbranches_growth.Series["Ach"].XValueMember = "Name";
                    //bargraphtopbranches_growth.ChartAreas[0].AxisY.Title = "GROWTH%";
                    //bargraphtopbranches_growth.ChartAreas[0].AxisX.TitleFont = new System.Drawing.Font("Arial", 11);
                    //bargraphtopbranches_growth.DataBind();


                    //foreach (var item in bargraphtopbranches.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}
                    //foreach (var item in bargraphtopbranches_growth.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}
                    ////added aswini Start
                    //bargraphtopbranches1.DataSource = chartDataTable(dtTopBranches);
                    //bargraphtopbranches1.Series["Ach"].YValueMembers = "ach";
                    //bargraphtopbranches1.Series["Ach"].XValueMember = "Name";
                    //bargraphtopbranches1.ChartAreas[0].AxisY.Title = "PRO-RATA ACH%";
                    //bargraphtopbranches1.ChartAreas[0].AxisX.TitleFont = new System.Drawing.Font("Arial", 11);
                    //bargraphtopbranches1.DataBind();
                    //foreach (var item in bargraphtopbranches1.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}

                    //bargraphtopbranches_growth1.DataSource = chartDataTable_all(dtTopBranches);
                    //bargraphtopbranches_growth1.Series["Ach"].YValueMembers = "growth";
                    //bargraphtopbranches_growth1.Series["Ach"].XValueMember = "Name";
                    //bargraphtopbranches_growth1.ChartAreas[0].AxisY.Title = "GROWTH%";
                    //bargraphtopbranches_growth1.ChartAreas[0].AxisX.TitleFont = new System.Drawing.Font("Arial", 11);
                    //bargraphtopbranches_growth1.DataBind();
                    //foreach (var item in bargraphtopbranches_growth1.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}
                    DataTable dtChartBranch = new DataTable();
                    dtChartBranch = chartDataTable_all(dtTopBranches);
                    foreach (DataRow dr in dtChartBranch.Rows)
                    {
                        if (Convert.ToString(dr["Name"]) == "Total")
                        {
                            dr.Delete();
                            break;
                        }
                    }

                    dtChartBranch.AcceptChanges();
                    Session["BranchAchChart"] = dtChartBranch;

                    dtChartBranch = new DataTable();
                    dtChartBranch = chartDataTable_all(dtTopBranches_Growth);
                    foreach (DataRow dr in dtChartBranch.Rows)
                    {
                        if (Convert.ToString(dr["Name"]) == "Total")
                        {
                            dr.Delete();
                            break;
                        }
                    }

                    dtChartBranch.AcceptChanges();
                    Session["BranchGroChart"] = dtChartBranch;
                    // End

                    DataTable dt = dtTopBranches.Copy();
                    dt = gridtable_all(dt, "BRANCH");

                    if (dt.Rows.Count > 0)
                    {
                        DataRow[] foundRows = dt.Select("Name='Total'");
                        hdn_Sales_value_year_0.Value = Convert.ToString(foundRows[0]["Sales_value_year_0"]);
                        hdn_budget_value.Value = Convert.ToString(foundRows[0]["budget"]);
                        hdn_Sales_ytd_value.Value = Convert.ToString(foundRows[0]["ytd"]);
                        hdn_ytd_plan.Value = Convert.ToString(foundRows[0]["ytd_plan"]);
                        hdn_achvmnt.Value = Convert.ToString(foundRows[0]["ach"]);
                        hdn_askrate.Value = Convert.ToString(foundRows[0]["arate"]);
                        hdn_growth.Value = Convert.ToString(foundRows[0]["growth"]);
                        foundRows[0].Delete();
                        dt.AcceptChanges();
                    }

                    Session["dtBranch"] = dt;
                    gridtopbranches.DataSource = dt;
                    gridtopbranches.DataBind();
                    bindgridColor_Branch();


                    #endregion
                }
                if (flag == "SE" || flag == "ALL")
                {
                    #region Sales Engineers

                    //bar graph for top sales engineers
                    dtCustomer = new DataTable();
                    distinctValues = new DataTable();
                    column = new string[] { "EngineerName", "Branch", "Sales_value_year_0", "budget_value", "Sales_ytd_value", "ytd_plan", "achvmnt", "Askrate", "No_Column", "growth" };
                    if (dtTopSalesEngineers.Tables.Count > 0)
                    {
                        if (dtTopSalesEngineers.Tables[0].Rows.Count > 0)
                        {
                            dtCustomer = dtTopSalesEngineers.Tables[0];
                            //if (dtCustomer.Columns.Contains("Branch"))
                            //    dtCustomer.Columns.Remove("Branch");
                            view = new DataView(dtCustomer);
                            distinctValues = new DataTable();
                            dtCustomer = new DataTable();
                            dtCustomer = view.ToTable(false, column);
                            distinctValues = view.ToTable(true, "achvmnt");
                        }
                    }

                    Session["TopSEAchChart"] = chartDataTable_Sorted(dtCustomer, distinctValues);

                    //bargraphtopse.DataSource = chartDataTable_Sorted(dtCustomer, distinctValues);
                    ////bargraphtopse.DataSource = chartDataTable(dtTopSalesEngineers);
                    //bargraphtopse.Series["Ach"].YValueMembers = "ach";
                    //bargraphtopse.Series["Ach"].XValueMember = "Name";
                    //bargraphtopse.ChartAreas[0].AxisY.Title = "PRO-RATA ACH%";
                    //bargraphtopse.DataBind();
                    //foreach (var item in bargraphtopse.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}

                    dtCustomer = new DataTable();
                    distinctValues = new DataTable();
                    if (dtTopSalesEngineers.Tables.Count > 0)
                    {
                        if (dtTopSalesEngineers.Tables[1].Rows.Count > 0)
                        {
                            dtCustomer = dtTopSalesEngineers.Tables[1];
                            //if (dtCustomer.Columns.Contains("Branch"))
                            //    dtCustomer.Columns.Remove("Branch");
                            view = new DataView(dtCustomer);
                            distinctValues = new DataTable();
                            dtCustomer = new DataTable();
                            dtCustomer = view.ToTable(false, column);
                            distinctValues = view.ToTable(true, "growth");
                        }
                    }
                    Session["TopSEGroChart"] = chartDataTable_Sorted(dtCustomer, distinctValues);
                    //bargraphtopse_growth.DataSource = chartDataTable_Sorted(dtCustomer, distinctValues);
                    //// bargraphtopse_growth.DataSource = chartDataTable(dtTopSalesEngineers);
                    //bargraphtopse_growth.Series["Ach"].YValueMembers = "growth";
                    //bargraphtopse_growth.Series["Ach"].XValueMember = "Name";
                    //bargraphtopse_growth.ChartAreas[0].AxisY.Title = "GROWTH%";
                    //bargraphtopse_growth.DataBind();
                    //foreach (var item in bargraphtopse_growth.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}

                    dtCustomer = new DataTable();
                    distinctValues = new DataTable();
                    if (dtTopSalesEngineers.Tables.Count > 0)
                    {
                        if (dtTopSalesEngineers.Tables[1].Rows.Count > 0)
                        {
                            dtCustomer = dtTopSalesEngineers.Tables[1];
                            //if (dtCustomer.Columns.Contains("Branch"))
                            //dtCustomer.Columns.Remove("Branch");
                            view = new DataView(dtCustomer);
                            distinctValues = new DataTable();
                            dtCustomer = new DataTable();
                            dtCustomer = view.ToTable(false, column);
                            distinctValues = view.ToTable(true, "growth");
                        }
                    }

                    bargraphtopse_growth1.DataSource = chartDataTable_Sorted(dtCustomer, distinctValues);
                    //bargraphtopse_growth1.DataSource = chartDataTable(dtTopSalesEngineers);
                    bargraphtopse_growth1.Series["Ach"].YValueMembers = "growth";
                    bargraphtopse_growth1.Series["Ach"].XValueMember = "Name";
                    bargraphtopse_growth1.ChartAreas[0].AxisY.Title = "GROWTH%";
                    bargraphtopse_growth1.DataBind();
                    foreach (var item in bargraphtopse_growth1.Series["Ach"].Points)
                    {
                        Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                        item.Color = c;
                    }


                    dtCustomer = new DataTable();
                    distinctValues = new DataTable();
                    if (dtTopSalesEngineers.Tables.Count > 0)
                    {
                        if (dtTopSalesEngineers.Tables[0].Rows.Count > 0)
                        {
                            dtCustomer = dtTopSalesEngineers.Tables[0];
                            //if (dtCustomer.Columns.Contains("Branch"))
                            //dtCustomer.Columns.Remove("Branch");
                            view = new DataView(dtCustomer);
                            distinctValues = new DataTable();
                            dtCustomer = new DataTable();
                            dtCustomer = view.ToTable(false, column);
                            distinctValues = view.ToTable(true, "achvmnt");
                        }
                    }

                    bargraphtopse1.DataSource = chartDataTable_Sorted(dtCustomer, distinctValues);
                    //bargraphtopse1.DataSource = chartDataTable(dtTopSalesEngineers);
                    bargraphtopse1.Series["Ach"].YValueMembers = "ach";
                    bargraphtopse1.Series["Ach"].XValueMember = "Name";
                    bargraphtopse1.ChartAreas[0].AxisY.Title = "PRO-RATA ACH%";
                    bargraphtopse1.DataBind();
                    foreach (var item in bargraphtopse1.Series["Ach"].Points)
                    {
                        Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                        item.Color = c;
                    }

                    //bar graph for bottom sales engineers
                    dtCustomer = new DataTable();
                    distinctValues = new DataTable();
                    if (dtTopSalesEngineers.Tables.Count > 0)
                    {
                        if (dtTopSalesEngineers.Tables[2].Rows.Count > 0)
                        {
                            dtCustomer = dtTopSalesEngineers.Tables[2];
                            //if (dtCustomer.Columns.Contains("Branch"))
                            //dtCustomer.Columns.Remove("Branch");
                            view = new DataView(dtCustomer);
                            distinctValues = new DataTable();
                            dtCustomer = new DataTable();
                            dtCustomer = view.ToTable(false, column);
                            distinctValues = view.ToTable(true, "achvmnt");
                        }
                    }
                    Session["BottomSEAchChart"] = chartDataTable_Sorted(dtCustomer, distinctValues, "BOTTOM");
                    //bargraphbottomse.DataSource = chartDataTable_Sorted(dtCustomer, distinctValues, "BOTTOM");
                    ////bargraphbottomse.DataSource = chartDataTable(dtBottomSalesEngineers, "BOTTOM");
                    //bargraphbottomse.Series["Ach"].YValueMembers = "ach";
                    //bargraphbottomse.Series["Ach"].XValueMember = "Name";
                    //bargraphbottomse.ChartAreas[0].AxisY.Title = "PRO-RATA ACH%";
                    //bargraphbottomse.DataBind();
                    //foreach (var item in bargraphbottomse.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}


                    dtCustomer = new DataTable();
                    distinctValues = new DataTable();
                    if (dtTopSalesEngineers.Tables.Count > 0)
                    {
                        if (dtTopSalesEngineers.Tables[3].Rows.Count > 0)
                        {
                            dtCustomer = dtTopSalesEngineers.Tables[3];
                            //if (dtCustomer.Columns.Contains("Branch"))
                            //dtCustomer.Columns.Remove("Branch");
                            view = new DataView(dtCustomer);
                            distinctValues = new DataTable();
                            dtCustomer = new DataTable();
                            dtCustomer = view.ToTable(false, column);
                            distinctValues = view.ToTable(true, "growth");
                        }
                    }
                    Session["BottomSEGroChart"] = chartDataTable_Sorted(dtCustomer, distinctValues, "BOTTOM");
                    //bargraphbottomse_growth.DataSource = chartDataTable_Sorted(dtCustomer, distinctValues, "BOTTOM");
                    ////bargraphbottomse_growth.DataSource = chartDataTable(dtBottomSalesEngineers, "BOTTOM");
                    //bargraphbottomse_growth.Series["Ach"].YValueMembers = "growth";
                    //bargraphbottomse_growth.Series["Ach"].XValueMember = "Name";
                    //bargraphbottomse_growth.ChartAreas[0].AxisY.Title = "GROWTH%";
                    //bargraphbottomse_growth.DataBind();
                    //foreach (var item in bargraphbottomse_growth.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}

                    dtCustomer = new DataTable();
                    distinctValues = new DataTable();
                    if (dtTopSalesEngineers.Tables.Count > 0)
                    {
                        if (dtTopSalesEngineers.Tables[2].Rows.Count > 0)
                        {
                            dtCustomer = dtTopSalesEngineers.Tables[2];
                            //if (dtCustomer.Columns.Contains("Branch"))
                            //dtCustomer.Columns.Remove("Branch");
                            view = new DataView(dtCustomer);
                            distinctValues = new DataTable();
                            dtCustomer = new DataTable();
                            dtCustomer = view.ToTable(false, column);
                            distinctValues = view.ToTable(true, "achvmnt");
                        }
                    }
                    bargraphbottomse1.DataSource = chartDataTable_Sorted(dtCustomer, distinctValues, "BOTTOM");
                    //bargraphbottomse1.DataSource = chartDataTable(dtBottomSalesEngineers, "BOTTOM");
                    bargraphbottomse1.Series["Ach"].YValueMembers = "ach";
                    bargraphbottomse1.Series["Ach"].XValueMember = "Name";
                    bargraphbottomse1.ChartAreas[0].AxisY.Title = "PRO-RATA ACH%";
                    bargraphbottomse1.DataBind();
                    foreach (var item in bargraphbottomse1.Series["Ach"].Points)
                    {
                        Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                        item.Color = c;
                    }

                    dtCustomer = new DataTable();
                    distinctValues = new DataTable();
                    if (dtTopSalesEngineers.Tables.Count > 0)
                    {
                        if (dtTopSalesEngineers.Tables[3].Rows.Count > 0)
                        {
                            dtCustomer = dtTopSalesEngineers.Tables[3];
                            //if (dtCustomer.Columns.Contains("Branch"))
                            //dtCustomer.Columns.Remove("Branch");
                            view = new DataView(dtCustomer);
                            distinctValues = new DataTable();
                            dtCustomer = new DataTable();
                            dtCustomer = view.ToTable(false, column);
                            distinctValues = view.ToTable(true, "growth");
                        }
                    }
                    bargraphbottomse_growth1.DataSource = chartDataTable_Sorted(dtCustomer, distinctValues, "BOTTOM");
                    //bargraphbottomse_growth1.DataSource = chartDataTable(dtBottomSalesEngineers, "BOTTOM");
                    bargraphbottomse_growth1.Series["Ach"].YValueMembers = "growth";
                    bargraphbottomse_growth1.Series["Ach"].XValueMember = "Name";
                    bargraphbottomse_growth1.ChartAreas[0].AxisY.Title = "GROWTH%";
                    bargraphbottomse_growth1.DataBind();
                    foreach (var item in bargraphbottomse_growth1.Series["Ach"].Points)
                    {
                        Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                        item.Color = c;
                    }

                    if (dtTopSalesEngineers.Tables.Count > 0)
                    {
                        DataTable dt = new DataTable();
                        dt = gridtable_all(dtTopSalesEngineers.Tables[0], "SE");

                        if (dt.Rows.Count > 0)
                        {
                            //dtTopBranches = dtTopSalesEngineers.Tables[0];
                            DataRow[] foundRows = dt.Select("Name='Total'");
                            hdn_Sales_value_year_0.Value = Convert.ToString(foundRows[0]["Sales_value_year_0"]);
                            hdn_budget_value.Value = Convert.ToString(foundRows[0]["budget"]);
                            hdn_Sales_ytd_value.Value = Convert.ToString(foundRows[0]["ytd"]);
                            hdn_ytd_plan.Value = Convert.ToString(foundRows[0]["ytd_plan"]);
                            hdn_achvmnt.Value = Convert.ToString(foundRows[0]["ach"]);
                            hdn_askrate.Value = Convert.ToString(foundRows[0]["arate"]);
                            hdn_growth.Value = Convert.ToString(foundRows[0]["growth"]);
                            foundRows[0].Delete();
                            dt.AcceptChanges();
                        }
                        Session["dtSE"] = dt;
                        GridTopSE.DataSource = dt;
                    }
                    else
                    {
                        GridTopSE.DataSource = null;
                    }

                    GridTopSE.DataBind();
                    bindgridColor_SE();

                    #endregion
                }

                if (flag == "CUSTOMER" || flag == "ALL")
                {
                    #region Customers
                    //bar graph for top customers

                    dtCustomer = new DataTable();
                    distinctValues = new DataTable();
                    column = new string[] { "customer_short_name", "Branch", "Sales_value_year_0", "budget_value", "Sales_ytd_value", "ytd_plan", "achvmnt", "Askrate", "customer_number", "growth" };

                    if (dtTopCustomers.Tables.Count > 0)
                    {
                        if (dtTopCustomers.Tables[0].Rows.Count > 0)
                        {
                            dtCustomer = dtTopCustomers.Tables[0];
                            view = new DataView(dtTopCustomers.Tables[0]);
                            distinctValues = new DataTable();
                            dtCustomer = new DataTable();
                            dtCustomer = view.ToTable(false, column);
                            distinctValues = view.ToTable(true, "achvmnt");
                        }
                    }
                    Session["TopCustAchChart"] = chartDataTable_Sorted(dtCustomer, distinctValues);
                    //bargraphtopcustomers.DataSource = chartDataTable_Sorted(dtCustomer, distinctValues);
                    //bargraphtopcustomers.Series["Ach"].YValueMembers = "ach";
                    //bargraphtopcustomers.Series["Ach"].XValueMember = "Name";
                    //bargraphtopcustomers.ChartAreas[0].AxisY.Title = "PRO-RATA ACH%";
                    //bargraphtopcustomers.DataBind();
                    //foreach (var item in bargraphtopcustomers.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}
                    dtCustomer = new DataTable();
                    if (dtTopCustomers.Tables.Count > 0)
                    {
                        if (dtTopCustomers.Tables[1].Rows.Count > 0)
                        {
                            dtCustomer = dtTopCustomers.Tables[1];
                            view = new DataView(dtTopCustomers.Tables[1]);
                            distinctValues = new DataTable();
                            dtCustomer = new DataTable();
                            dtCustomer = view.ToTable(false, column);
                            distinctValues = view.ToTable(true, "growth");
                            distinctValues.DefaultView.Sort = "growth";
                        }
                    }
                    Session["TopCustGroChart"] = chartDataTable_Sorted(dtCustomer, distinctValues);
                    //bargraphtopcustomers_growth.DataSource = chartDataTable_Sorted(dtCustomer, distinctValues);
                    //bargraphtopcustomers_growth.Series["Ach"].YValueMembers = "growth";
                    //bargraphtopcustomers_growth.Series["Ach"].XValueMember = "Name";
                    //bargraphtopcustomers_growth.ChartAreas[0].AxisY.Title = "GROWTH%";
                    //bargraphtopcustomers_growth.DataBind();
                    //foreach (var item in bargraphtopcustomers_growth.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}
                    dtCustomer = new DataTable();
                    if (dtTopCustomers.Tables.Count > 0)
                    {
                        if (dtTopCustomers.Tables[1].Rows.Count > 0)
                        {
                            dtCustomer = dtTopCustomers.Tables[1];
                            view = new DataView(dtTopCustomers.Tables[1]);
                            distinctValues = new DataTable();
                            dtCustomer = new DataTable();
                            dtCustomer = view.ToTable(false, column);
                            distinctValues = view.ToTable(true, "growth");
                            distinctValues.DefaultView.Sort = "growth";
                        }
                    }
                    bargraphtopcustomers_growth1.DataSource = chartDataTable_Sorted(dtCustomer, distinctValues);
                    bargraphtopcustomers_growth1.Series["Ach"].YValueMembers = "growth";
                    bargraphtopcustomers_growth1.Series["Ach"].XValueMember = "Name";
                    bargraphtopcustomers_growth1.ChartAreas[0].AxisY.Title = "GROWTH%";
                    bargraphtopcustomers_growth1.DataBind();
                    foreach (var item in bargraphtopcustomers_growth1.Series["Ach"].Points)
                    {
                        Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                        item.Color = c;
                    }
                    dtCustomer = new DataTable();
                    if (dtTopCustomers.Tables.Count > 0)
                    {
                        if (dtTopCustomers.Tables[0].Rows.Count > 0)
                        {
                            dtCustomer = dtTopCustomers.Tables[0];
                            view = new DataView(dtTopCustomers.Tables[0]);
                            distinctValues = new DataTable();
                            dtCustomer = new DataTable();
                            dtCustomer = view.ToTable(false, column);
                            distinctValues = view.ToTable(true, "achvmnt");
                        }
                    }
                    bargraphtopcustomers1.DataSource = chartDataTable_Sorted(dtCustomer, distinctValues);
                    bargraphtopcustomers1.Series["Ach"].YValueMembers = "ach";
                    bargraphtopcustomers1.Series["Ach"].XValueMember = "Name";
                    bargraphtopcustomers1.ChartAreas[0].AxisY.Title = "PRO-RATA ACH%";
                    bargraphtopcustomers1.DataBind();
                    foreach (var item in bargraphtopcustomers1.Series["Ach"].Points)
                    {
                        Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                        item.Color = c;
                    }


                    //bar graph for Bottom customers
                    dtCustomer = new DataTable();
                    if (dtTopCustomers.Tables.Count > 0)
                    {
                        if (dtTopCustomers.Tables[2].Rows.Count > 0)
                        {
                            dtCustomer = dtTopCustomers.Tables[2];
                            view = new DataView(dtTopCustomers.Tables[2]);
                            distinctValues = new DataTable();
                            dtCustomer = new DataTable();
                            dtCustomer = view.ToTable(false, column);
                            distinctValues = view.ToTable(true, "achvmnt");
                        }
                    }
                    Session["BottomCustAchChart"] = chartDataTable_Sorted(dtCustomer, distinctValues, "BOTTOM");
                    //bargraphbottomCustomers.DataSource = chartDataTable_Sorted(dtCustomer, distinctValues, "BOTTOM");
                    //bargraphbottomCustomers.Series["Ach"].YValueMembers = "ach";
                    //bargraphbottomCustomers.Series["Ach"].XValueMember = "Name";
                    //bargraphbottomCustomers.ChartAreas[0].AxisY.Title = "PRO-RATA ACH%";
                    //bargraphbottomCustomers.DataBind();
                    //foreach (var item in bargraphbottomCustomers.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}
                    dtCustomer = new DataTable();
                    if (dtTopCustomers.Tables.Count > 0)
                    {
                        if (dtTopCustomers.Tables[3].Rows.Count > 0)
                        {
                            dtCustomer = dtTopCustomers.Tables[3];
                            view = new DataView(dtTopCustomers.Tables[3]);
                            distinctValues = new DataTable();
                            dtCustomer = new DataTable();
                            dtCustomer = view.ToTable(false, column);
                            distinctValues = view.ToTable(true, "growth");
                            distinctValues.DefaultView.Sort = "growth desc";
                        }
                    }
                    Session["BottomCustGroChart"] = chartDataTable_Sorted(dtCustomer, distinctValues, "BOTTOM");
                    //bargraphbottomCustomers_growth.DataSource = chartDataTable_Sorted(dtCustomer, distinctValues, "BOTTOM");
                    //bargraphbottomCustomers_growth.Series["Ach"].YValueMembers = "growth";
                    //bargraphbottomCustomers_growth.Series["Ach"].XValueMember = "Name";
                    //bargraphbottomCustomers_growth.ChartAreas[0].AxisY.Title = "GROWTH%";
                    //bargraphbottomCustomers_growth.DataBind();
                    //foreach (var item in bargraphbottomCustomers_growth.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}
                    dtCustomer = new DataTable();
                    if (dtTopCustomers.Tables.Count > 0)
                    {
                        if (dtTopCustomers.Tables[3].Rows.Count > 0)
                        {
                            dtCustomer = dtTopCustomers.Tables[3];
                            view = new DataView(dtTopCustomers.Tables[3]);
                            distinctValues = new DataTable();
                            dtCustomer = new DataTable();
                            dtCustomer = view.ToTable(false, column);
                            distinctValues = view.ToTable(true, "growth");
                            distinctValues.DefaultView.Sort = "growth desc";
                        }
                    }
                    bargraphbottomCustomers_growth1.DataSource = chartDataTable_Sorted(dtCustomer, distinctValues, "BOTTOM");
                    bargraphbottomCustomers_growth1.Series["Ach"].YValueMembers = "growth";
                    bargraphbottomCustomers_growth1.Series["Ach"].XValueMember = "Name";
                    bargraphbottomCustomers_growth1.ChartAreas[0].AxisY.Title = "GROWTH%";
                    bargraphbottomCustomers_growth1.DataBind();
                    foreach (var item in bargraphbottomCustomers_growth1.Series["Ach"].Points)
                    {
                        Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                        item.Color = c;
                    }

                    ///Aswini added here
                    ///start
                    dtCustomer = new DataTable();
                    if (dtTopCustomers.Tables.Count > 0)
                    {
                        if (dtTopCustomers.Tables[2].Rows.Count > 0)
                        {
                            dtCustomer = dtTopCustomers.Tables[2];
                            view = new DataView(dtTopCustomers.Tables[2]);
                            distinctValues = new DataTable();
                            dtCustomer = new DataTable();
                            dtCustomer = view.ToTable(false, column);
                            distinctValues = view.ToTable(true, "achvmnt");
                        }
                    }
                    bargraphbottomCustomers1.DataSource = chartDataTable_Sorted(dtCustomer, distinctValues, "BOTTOM");
                    bargraphbottomCustomers1.Series["Ach"].YValueMembers = "ach";
                    bargraphbottomCustomers1.Series["Ach"].XValueMember = "Name";
                    bargraphbottomCustomers1.ChartAreas[0].AxisY.Title = "PRO-RATA ACH%";
                    bargraphbottomCustomers1.DataBind();
                    foreach (var item in bargraphbottomCustomers1.Series["Ach"].Points)
                    {
                        Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                        item.Color = c;
                    }

                    ///end
                    //
                    DataTable dt = new DataTable();
                    dt = gridtable_all(dtTopCustomers.Tables[0], "CUST");
                    if (dt.Rows.Count > 0)
                    {
                        //dtTopBranches = dtTopSalesEngineers.Tables[0];
                        DataRow[] foundRows = dt.Select("customerNumber='Total'");
                        hdn_Sales_value_year_0.Value = Convert.ToString(foundRows[0]["Sales_value_year_0"]);
                        hdn_budget_value.Value = Convert.ToString(foundRows[0]["budget"]);
                        hdn_Sales_ytd_value.Value = Convert.ToString(foundRows[0]["ytd"]);
                        hdn_ytd_plan.Value = Convert.ToString(foundRows[0]["ytd_plan"]);
                        hdn_achvmnt.Value = Convert.ToString(foundRows[0]["ach"]);
                        hdn_askrate.Value = Convert.ToString(foundRows[0]["arate"]);
                        hdn_growth.Value = Convert.ToString(foundRows[0]["growth"]);
                        foundRows[0].Delete();
                        dt.AcceptChanges();
                    }

                    if (dtTopCustomers.Tables.Count > 0)
                    {
                        Session["dtCustomer"] = dt;
                        GridCustByAch.DataSource = dt;
                    }
                    else
                    {
                        GridCustByAch.DataSource = null;
                    }

                    GridCustByAch.DataBind();
                    bindgridColor_Customer();


                    #endregion
                }
                if (flag == "CP" || flag == "ALL")
                {
                    #region Channel Partners
                    //bar graph for TOP channel partners
                    //DataView view;
                    //DataTable distinctValues;
                    dtCustomer = new DataTable();
                    distinctValues = new DataTable();
                    column = new string[] { "customer_short_name", "Branch", "Sales_value_year_0", "budget_value", "Sales_ytd_value", "ytd_plan", "achvmnt", "Askrate", "customer_number", "growth" };
                    if (dtTopChannelPartners.Tables.Count > 0)
                    {
                        if (dtTopChannelPartners.Tables[0].Rows.Count > 0)
                        {
                            dtCustomer = dtTopChannelPartners.Tables[0];
                            view = new DataView(dtTopChannelPartners.Tables[0]);
                            distinctValues = new DataTable();
                            dtCustomer = new DataTable();
                            dtCustomer = view.ToTable(false, column);
                            distinctValues = view.ToTable(true, "achvmnt");
                        }
                    }
                    Session["TopCPAchChart"] = chartDataTable_Sorted(dtCustomer, distinctValues);
                    //bargraphtopcp.DataSource = chartDataTable_Sorted(dtCustomer, distinctValues);
                    ////bargraphtopcp.DataSource = chartDataTable(dtTopChannelPartners);
                    //bargraphtopcp.Series["Ach"].YValueMembers = "ach";
                    //bargraphtopcp.Series["Ach"].XValueMember = "Name";
                    //bargraphtopcp.ChartAreas[0].AxisY.Title = "PRO-RATA ACH%";
                    //bargraphtopcp.DataBind();
                    //foreach (var item in bargraphtopcp.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}


                    dtCustomer = new DataTable();
                    distinctValues = new DataTable();
                    if (dtTopChannelPartners.Tables.Count > 0)
                    {
                        if (dtTopChannelPartners.Tables[1].Rows.Count > 0)
                        {
                            dtCustomer = dtTopChannelPartners.Tables[1];
                            view = new DataView(dtTopChannelPartners.Tables[1]);
                            distinctValues = new DataTable();
                            dtCustomer = new DataTable();
                            dtCustomer = view.ToTable(false, column);
                            distinctValues = view.ToTable(true, "growth");
                        }
                    }
                    Session["TopCPGroChart"] = chartDataTable_Sorted(dtCustomer, distinctValues);
                    //bargraphtopcp_growth.DataSource = chartDataTable_Sorted(dtCustomer, distinctValues);
                    ////bargraphtopcp_growth.DataSource = chartDataTable(dtTopChannelPartners);
                    //bargraphtopcp_growth.Series["Ach"].YValueMembers = "growth";
                    //bargraphtopcp_growth.Series["Ach"].XValueMember = "Name";
                    //bargraphtopcp_growth.ChartAreas[0].AxisY.Title = "GROWTH%";
                    //bargraphtopcp_growth.DataBind();
                    //foreach (var item in bargraphtopcp_growth.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}

                    //dtCustomer = new DataTable();
                    //distinctValues = new DataTable();
                    //if (dtTopChannelPartners.Tables.Count > 0)
                    //{
                    //    if (dtTopChannelPartners.Tables[1].Rows.Count > 0)
                    //    {
                    //        dtCustomer = dtTopChannelPartners.Tables[1];
                    //        view = new DataView(dtTopChannelPartners.Tables[1]);
                    //        distinctValues = new DataTable();
                    //        dtCustomer = new DataTable();
                    //        dtCustomer = view.ToTable(false, column);
                    //        distinctValues = view.ToTable(true, "growth");
                    //    }
                    //}
                    //bargraphtopcp_growth1.DataSource = chartDataTable_Sorted(dtCustomer, distinctValues);
                    ////bargraphtopcp_growth1.DataSource = chartDataTable(dtTopChannelPartners);
                    //bargraphtopcp_growth1.Series["Ach"].YValueMembers = "growth";
                    //bargraphtopcp_growth1.Series["Ach"].XValueMember = "Name";
                    //bargraphtopcp_growth1.ChartAreas[0].AxisY.Title = "GROWTH%";
                    //bargraphtopcp_growth1.DataBind();
                    //foreach (var item in bargraphtopcp_growth1.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}

                    //dtCustomer = new DataTable();
                    //distinctValues = new DataTable();
                    //if (dtTopChannelPartners.Tables.Count > 0)
                    //{
                    //    if (dtTopChannelPartners.Tables[0].Rows.Count > 0)
                    //    {
                    //        dtCustomer = dtTopChannelPartners.Tables[0];
                    //        view = new DataView(dtTopChannelPartners.Tables[0]);
                    //        distinctValues = new DataTable();
                    //        dtCustomer = new DataTable();
                    //        dtCustomer = view.ToTable(false, column);
                    //        distinctValues = view.ToTable(true, "achvmnt");
                    //    }
                    //}
                    //bargraphtopcp1.DataSource = chartDataTable_Sorted(dtCustomer, distinctValues);
                    ////bargraphtopcp1.DataSource = chartDataTable(dtTopChannelPartners);
                    //bargraphtopcp1.Series["Ach"].YValueMembers = "ach";
                    //bargraphtopcp1.Series["Ach"].XValueMember = "Name";
                    //bargraphtopcp1.ChartAreas[0].AxisY.Title = "PRO-RATA ACH%";
                    //bargraphtopcp1.DataBind();
                    //foreach (var item in bargraphtopcp1.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}
                    //bar graph for BOTTOM channel partners
                    dtCustomer = new DataTable();
                    distinctValues = new DataTable();
                    if (dtTopChannelPartners.Tables.Count > 0)
                    {
                        if (dtTopChannelPartners.Tables[2].Rows.Count > 0)
                        {
                            dtCustomer = dtTopChannelPartners.Tables[2];
                            view = new DataView(dtTopChannelPartners.Tables[2]);
                            distinctValues = new DataTable();
                            dtCustomer = new DataTable();
                            dtCustomer = view.ToTable(false, column);
                            distinctValues = view.ToTable(true, "achvmnt");
                        }
                    }
                    Session["BottomCPAchChart"] = chartDataTable_Sorted(dtCustomer, distinctValues, "BOTTOM");
                    //bargraphbottomcp.DataSource = chartDataTable_Sorted(dtCustomer, distinctValues, "BOTTOM");
                    ////bargraphbottomcp.DataSource = chartDataTable(dtBottomChannelPartners, "BOTTOM");
                    //bargraphbottomcp.Series["Ach"].YValueMembers = "ach";
                    //bargraphbottomcp.Series["Ach"].XValueMember = "Name";
                    //bargraphbottomcp.ChartAreas[0].AxisY.Title = "PRO-RATA ACH%";
                    //bargraphbottomcp.DataBind();
                    //foreach (var item in bargraphbottomcp.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}

                    dtCustomer = new DataTable();
                    distinctValues = new DataTable();
                    if (dtTopChannelPartners.Tables.Count > 0)
                    {
                        if (dtTopChannelPartners.Tables[3].Rows.Count > 0)
                        {
                            dtCustomer = dtTopChannelPartners.Tables[3];
                            view = new DataView(dtTopChannelPartners.Tables[3]);
                            distinctValues = new DataTable();
                            dtCustomer = new DataTable();
                            dtCustomer = view.ToTable(false, column);
                            distinctValues = view.ToTable(true, "growth");
                        }
                    }
                    Session["BottomCPGroChart"] = chartDataTable_Sorted(dtCustomer, distinctValues, "BOTTOM");
                    //bargraphbottomcp_growth.DataSource = chartDataTable_Sorted(dtCustomer, distinctValues, "BOTTOM");
                    ////bargraphbottomcp_growth.DataSource = chartDataTable(dtBottomChannelPartners, "BOTTOM");
                    //bargraphbottomcp_growth.Series["Ach"].YValueMembers = "growth";
                    //bargraphbottomcp_growth.Series["Ach"].XValueMember = "Name";
                    //bargraphbottomcp_growth.ChartAreas[0].AxisY.Title = "GROWTH%";
                    //bargraphbottomcp_growth.DataBind();
                    //foreach (var item in bargraphbottomcp_growth.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}

                    dtCustomer = new DataTable();
                    distinctValues = new DataTable();
                    if (dtTopChannelPartners.Tables.Count > 0)
                    {
                        if (dtTopChannelPartners.Tables[3].Rows.Count > 0)
                        {
                            dtCustomer = dtTopChannelPartners.Tables[3];
                            view = new DataView(dtTopChannelPartners.Tables[3]);
                            distinctValues = new DataTable();
                            dtCustomer = new DataTable();
                            dtCustomer = view.ToTable(false, column);
                            distinctValues = view.ToTable(true, "growth");
                        }
                    }
                    bargraphbottomcp_growth1.DataSource = chartDataTable_Sorted(dtCustomer, distinctValues, "BOTTOM");
                    //bargraphbottomcp_growth1.DataSource = chartDataTable(dtBottomChannelPartners, "BOTTOM");
                    bargraphbottomcp_growth1.Series["Ach"].YValueMembers = "growth";
                    bargraphbottomcp_growth1.Series["Ach"].XValueMember = "Name";
                    bargraphbottomcp_growth1.ChartAreas[0].AxisY.Title = "GROWTH%";
                    bargraphbottomcp_growth1.DataBind();
                    foreach (var item in bargraphbottomcp_growth1.Series["Ach"].Points)
                    {
                        Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                        item.Color = c;
                    }

                    dtCustomer = new DataTable();
                    distinctValues = new DataTable();
                    if (dtTopChannelPartners.Tables.Count > 0)
                    {
                        if (dtTopChannelPartners.Tables[2].Rows.Count > 0)
                        {
                            dtCustomer = dtTopChannelPartners.Tables[2];
                            view = new DataView(dtTopChannelPartners.Tables[2]);
                            distinctValues = new DataTable();
                            dtCustomer = new DataTable();
                            dtCustomer = view.ToTable(false, column);
                            distinctValues = view.ToTable(true, "achvmnt");
                        }
                    }
                    bargraphbottomcp1.DataSource = chartDataTable_Sorted(dtCustomer, distinctValues, "BOTTOM");
                    //bargraphbottomcp1.DataSource = chartDataTable(dtBottomChannelPartners, "BOTTOM");
                    bargraphbottomcp1.Series["Ach"].YValueMembers = "ach";
                    bargraphbottomcp1.Series["Ach"].XValueMember = "Name";
                    bargraphbottomcp1.ChartAreas[0].AxisY.Title = "PRO-RATA ACH%";
                    bargraphbottomcp1.DataBind();
                    foreach (var item in bargraphbottomcp1.Series["Ach"].Points)
                    {
                        Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                        item.Color = c;
                    }


                    //GRIDS BINding
                    DataTable dt = new DataTable();
                    dt = gridtable_all(dtTopChannelPartners.Tables[0], "CUST");

                    if (dt.Rows.Count > 0)
                    {
                        //dtTopBranches = dtTopSalesEngineers.Tables[0];
                        DataRow[] foundRows = dt.Select("customerNumber='Total'");
                        hdn_Sales_value_year_0.Value = Convert.ToString(foundRows[0]["sales_value_year_0"]);
                        hdn_budget_value.Value = Convert.ToString(foundRows[0]["budget"]);
                        hdn_Sales_ytd_value.Value = Convert.ToString(foundRows[0]["ytd"]);
                        hdn_ytd_plan.Value = Convert.ToString(foundRows[0]["ytd_plan"]);
                        hdn_achvmnt.Value = Convert.ToString(foundRows[0]["ach"]);
                        hdn_askrate.Value = Convert.ToString(foundRows[0]["arate"]);
                        hdn_growth.Value = Convert.ToString(foundRows[0]["growth"]);
                        foundRows[0].Delete();
                        dt.AcceptChanges();
                    }

                    if (dt.Rows.Count > 0)
                    {
                        Session["dtCP"] = dt;
                        GridTopCP.DataSource = dt;
                    }
                    else
                    {
                        GridTopCP.DataSource = null;
                    }

                    //GridTopCP.DataSource = gridtable(dtTopChannelPartners);
                    GridTopCP.DataBind();
                    bindgridColor_Channelpatner();

                    //GridBottomCP.DataSource = gridtable(dtBottomChannelPartners, "BOTTOM");
                    //GridBottomCP.DataBind();

                    #endregion
                }
                if (flag == "GOLD" || flag == "ALL")
                {
                    #region GOLD



                    int year = objConfig.getBudgetYear();
                    string cus_Sales_value_year_0 = string.Empty;
                    string cus_budget_value = string.Empty;
                    string cus_Sales_ytd_value = string.Empty;
                    string cus_ytd_plan = string.Empty;
                    string cus_achvmnt = string.Empty;
                    string cus_askrate = string.Empty;
                    string cus_NOVALUES = string.Empty;
                    string cus_growth = string.Empty;
                    string cp_Sales_value_year_0 = string.Empty;
                    string cp_budget_value = string.Empty;
                    string cp_Sales_ytd_value = string.Empty;
                    string cp_ytd_plan = string.Empty;
                    string cp_achvmnt = string.Empty;
                    string cp_askrate = string.Empty;
                    string cp_NOVALUES = string.Empty;
                    string cp_growth = string.Empty;
                    string se_Sales_value_year_0 = string.Empty;
                    string se_budget_value = string.Empty;
                    string se_Sales_ytd_value = string.Empty;
                    string se_ytd_plan = string.Empty;
                    string se_achvmnt = string.Empty;
                    string se_askrate = string.Empty;
                    string se_NOVALUES = string.Empty;
                    string se_growth = string.Empty;
                    string branch_Sales_value_year_0 = string.Empty;
                    string branch_budget_value = string.Empty;
                    string branch_Sales_ytd_value = string.Empty;
                    string branch_ytd_plan = string.Empty;
                    string branch_achvmnt = string.Empty;
                    string branch_askrate = string.Empty;
                    string branch_NOVALUES = string.Empty;
                    string branch_growth = string.Empty;


                    //bar graph of gold branches
                    #region GOLD BRANCH

                    DataTable dt = new DataTable();
                    dt = gridtable_all(dtGoldBranches, "BRANCH");
                    if (dt.Rows.Count > 0)
                    {
                        DataRow[] foundRows = dt.Select("Name='Total'");
                        branch_Sales_value_year_0 = Convert.ToString(foundRows[0]["Sales_value_year_0"]);
                        branch_budget_value = Convert.ToString(foundRows[0]["budget"]);
                        branch_Sales_ytd_value = Convert.ToString(foundRows[0]["ytd"]);
                        branch_ytd_plan = Convert.ToString(foundRows[0]["ytd_plan"]);
                        branch_achvmnt = Convert.ToString(foundRows[0]["ach"]);
                        branch_askrate = Convert.ToString(foundRows[0]["arate"]);
                        branch_growth = Convert.ToString(foundRows[0]["growth"]);
                        foundRows[0].Delete();
                        dt.AcceptChanges();
                    }
                    Session["dtGoldBranch"] = dt;
                    GridTopGoldBranches.DataSource = dt;
                    GridTopGoldBranches.DataBind();
                    bindgridColor_GoldBranches();
                    DataTable dtChartBranch = new DataTable();
                    dtChartBranch = chartDataTable_all(dtGoldBranches);
                    foreach (DataRow dr in dtChartBranch.Rows)
                    {
                        if (Convert.ToString(dr["Name"]) == "Total")
                        {
                            dr.Delete();
                            break;
                        }
                    }

                    dtChartBranch.AcceptChanges();
                    Session["BranchAchChart"] = dtChartBranch;

                    //bargraphtopgoldbranches.DataSource = chartDataTable_all(dtGoldBranches);
                    //bargraphtopgoldbranches.Series["Ach"].YValueMembers = "budget_value";
                    //bargraphtopgoldbranches.Series["Ach"].XValueMember = "Name";
                    //bargraphtopgoldbranches.ChartAreas[0].AxisY.Title = year + " BUDGET";
                    //bargraphtopgoldbranches.DataBind();
                    //foreach (var item in bargraphtopgoldbranches.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}
                    //bargraphtopgoldbranches1.DataSource = chartDataTable_all(dtGoldBranches);
                    //bargraphtopgoldbranches1.Series["Ach"].YValueMembers = "budget_value";
                    //bargraphtopgoldbranches1.Series["Ach"].XValueMember = "Name";
                    //bargraphtopgoldbranches1.ChartAreas[0].AxisY.Title = year + " BUDGET";
                    //bargraphtopgoldbranches1.DataBind();
                    //foreach (var item in bargraphtopgoldbranches1.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}

                    dtChartBranch = new DataTable();
                    dtChartBranch = chartDataTable_all(dtGoldBranches_Growth);
                    foreach (DataRow dr in dtChartBranch.Rows)
                    {
                        if (Convert.ToString(dr["Name"]) == "Total")
                        {
                            dr.Delete();
                            break;
                        }
                    }

                    dtChartBranch.AcceptChanges();
                    Session["BranchGroChart"] = dtChartBranch;

                    //bargraphtopgoldbranches_growth.DataSource = chartDataTable_all(dtGoldBranches_Growth);
                    //bargraphtopgoldbranches_growth.Series["Ach"].YValueMembers = "growth";
                    //bargraphtopgoldbranches_growth.Series["Ach"].XValueMember = "Name";
                    //bargraphtopgoldbranches_growth.ChartAreas[0].AxisY.Title = "GROWTH%";
                    //bargraphtopgoldbranches_growth.DataBind();
                    //foreach (var item in bargraphtopgoldbranches_growth.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}
                    //bargraphtopgoldbranches_growth1.DataSource = chartDataTable_all(dtGoldBranches_Growth);
                    //bargraphtopgoldbranches_growth1.Series["Ach"].YValueMembers = "growth";
                    //bargraphtopgoldbranches_growth1.Series["Ach"].XValueMember = "Name";
                    //bargraphtopgoldbranches_growth1.ChartAreas[0].AxisY.Title = "GROWTH%";
                    //bargraphtopgoldbranches_growth1.DataBind();
                    //foreach (var item in bargraphtopgoldbranches_growth1.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}
                    #endregion

                    //end


                    //bar graph of top gold se
                    #region GOLD SE

                    dt = gridtable_all(dtTopGoldSe_withBranch, "SE");

                    if (dt.Rows.Count > 0)
                    {
                        //dtTopBranches = dtTopSalesEngineers.Tables[0];
                        DataRow[] foundRows = dt.Select("Name='Total'");
                        se_Sales_value_year_0 = Convert.ToString(foundRows[0]["Sales_value_year_0"]);
                        se_budget_value = Convert.ToString(foundRows[0]["budget"]);
                        se_Sales_ytd_value = Convert.ToString(foundRows[0]["ytd"]);
                        se_ytd_plan = Convert.ToString(foundRows[0]["ytd_plan"]);
                        se_achvmnt = Convert.ToString(foundRows[0]["ach"]);
                        se_askrate = Convert.ToString(foundRows[0]["arate"]);
                        se_growth = Convert.ToString(foundRows[0]["growth"]);
                        foundRows[0].Delete();
                        dt.AcceptChanges();
                    }
                    Session["dtGoldSE"] = dt;
                    GridTopGoldSE.DataSource = dt;
                    GridTopGoldSE.DataBind();
                    bindgridColor_GoldSE();
                    column = new string[] { "EngineerName", "Sales_value_year_0", "budget_value", "Sales_ytd_value", "ytd_plan", "achvmnt", "Askrate", "No_Column", "growth" };
                    if (dtTopGoldSe.Rows.Count > 0)
                    {
                        view = new DataView(dtTopGoldSe);
                        distinctValues = new DataTable();
                        dtTopGoldSe = new DataTable();
                        dtTopGoldSe = view.ToTable(false, column);
                        distinctValues = view.ToTable(true, "budget_value");
                    }

                    Session["TopSEAchChart"] = chartDataTable_GOLD_Sorted(dtTopGoldSe, distinctValues);
                    //bargraphTopGoldSE.DataSource = chartDataTable_GOLD_Sorted(dtTopGoldSe, distinctValues);
                    //bargraphTopGoldSE.Series["Ach"].YValueMembers = "budget_value";
                    //bargraphTopGoldSE.Series["Ach"].XValueMember = "Name";
                    //bargraphTopGoldSE.ChartAreas[0].AxisY.Title = year + " BUDGET";
                    //bargraphTopGoldSE.DataBind();
                    //foreach (var item in bargraphTopGoldSE.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}

                    if (dtTopGoldSe.Rows.Count > 0)
                    {
                        view = new DataView(dtTopGoldSe);
                        distinctValues = new DataTable();
                        dtTopGoldSe = new DataTable();
                        dtTopGoldSe = view.ToTable(false, column);
                        distinctValues = view.ToTable(true, "budget_value");
                    }
                    //bargraphTopGoldSE1.DataSource = chartDataTable_GOLD_Sorted(dtTopGoldSe, distinctValues);
                    //bargraphTopGoldSE1.Series["Ach"].YValueMembers = "budget_value";
                    //bargraphTopGoldSE1.Series["Ach"].XValueMember = "Name";
                    //bargraphTopGoldSE1.ChartAreas[0].AxisY.Title = year + " BUDGET";
                    //bargraphTopGoldSE1.DataBind();
                    //foreach (var item in bargraphTopGoldSE1.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}

                    if (dtTopGoldSe_Growth.Rows.Count > 0)
                    {
                        view = new DataView(dtTopGoldSe_Growth);
                        distinctValues = new DataTable();
                        dtTopGoldSe_Growth = new DataTable();
                        dtTopGoldSe_Growth = view.ToTable(false, column);
                        distinctValues = view.ToTable(true, "growth");
                    }
                    Session["TopSEGroChart"] = chartDataTable_GOLD_Sorted(dtTopGoldSe_Growth, distinctValues);
                    //bargraphTopGoldSE_growth.DataSource = chartDataTable_GOLD_Sorted(dtTopGoldSe_Growth, distinctValues);
                    //bargraphTopGoldSE_growth.Series["Ach"].YValueMembers = "growth";
                    //bargraphTopGoldSE_growth.Series["Ach"].XValueMember = "Name";
                    //bargraphTopGoldSE_growth.ChartAreas[0].AxisY.Title = "GROWTH%";
                    //bargraphTopGoldSE_growth.DataBind();
                    //foreach (var item in bargraphTopGoldSE_growth.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}

                    if (dtTopGoldSe_Growth.Rows.Count > 0)
                    {
                        view = new DataView(dtTopGoldSe_Growth);
                        distinctValues = new DataTable();
                        dtTopGoldSe_Growth = new DataTable();
                        dtTopGoldSe_Growth = view.ToTable(false, column);
                        distinctValues = view.ToTable(true, "growth");
                    }
                    //bargraphTopGoldSE_growth1.DataSource = chartDataTable_GOLD_Sorted(dtTopGoldSe_Growth, distinctValues);
                    //bargraphTopGoldSE_growth1.Series["Ach"].YValueMembers = "growth";
                    //bargraphTopGoldSE_growth1.Series["Ach"].XValueMember = "Name";
                    //bargraphTopGoldSE_growth1.ChartAreas[0].AxisY.Title = "GROWTH%";
                    //bargraphTopGoldSE_growth1.DataBind();
                    //foreach (var item in bargraphTopGoldSE_growth1.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}
                    //end


                    //bar graph for bottom gold se

                    //GridBtmGoldSE.DataSource = gridtable_GOLD(dtBottomGoldSe, "BOTTOM");
                    //GridBtmGoldSE.DataBind();
                    if (dtBottomGoldSe.Rows.Count > 0)
                    {
                        view = new DataView(dtBottomGoldSe);
                        distinctValues = new DataTable();
                        dtBottomGoldSe = new DataTable();
                        dtBottomGoldSe = view.ToTable(false, column);
                        distinctValues = view.ToTable(true, "budget_value");
                    }
                    Session["BottomSEAchChart"] = chartDataTable_GOLD_Sorted(dtBottomGoldSe, distinctValues, "BOTTOM");
                    //bargraphBtmGoldSE.DataSource = chartDataTable_GOLD_Sorted(dtBottomGoldSe, distinctValues, "BOTTOM");
                    //bargraphBtmGoldSE.Series["Ach"].YValueMembers = "budget_value";
                    //bargraphBtmGoldSE.Series["Ach"].XValueMember = "Name";
                    //bargraphBtmGoldSE.ChartAreas[0].AxisY.Title = year + " BUDGET";
                    //bargraphBtmGoldSE.DataBind();
                    //foreach (var item in bargraphBtmGoldSE.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}

                    if (dtBottomGoldSe.Rows.Count > 0)
                    {
                        view = new DataView(dtBottomGoldSe);
                        distinctValues = new DataTable();
                        dtBottomGoldSe = new DataTable();
                        dtBottomGoldSe = view.ToTable(false, column);
                        distinctValues = view.ToTable(true, "budget_value");
                    }
                    //bargraphBtmGoldSE1.DataSource = chartDataTable_GOLD_Sorted(dtBottomGoldSe, distinctValues, "BOTTOM");
                    //bargraphBtmGoldSE1.Series["Ach"].YValueMembers = "budget_value";
                    //bargraphBtmGoldSE1.Series["Ach"].XValueMember = "Name";
                    //bargraphBtmGoldSE1.ChartAreas[0].AxisY.Title = year + " BUDGET";
                    //bargraphBtmGoldSE1.DataBind();

                    //foreach (var item in bargraphBtmGoldSE1.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}

                    if (dtBottomGoldSe_Growth.Rows.Count > 0)
                    {
                        view = new DataView(dtBottomGoldSe_Growth);
                        distinctValues = new DataTable();
                        dtBottomGoldSe_Growth = new DataTable();
                        dtBottomGoldSe_Growth = view.ToTable(false, column);
                        distinctValues = view.ToTable(true, "growth");
                    }
                    Session["BottomSEGroChart"] = chartDataTable_GOLD_Sorted(dtBottomGoldSe_Growth, distinctValues, "BOTTOM");
                    //bargraphBtmGoldSE_growth.DataSource = chartDataTable_GOLD_Sorted(dtBottomGoldSe_Growth, distinctValues, "BOTTOM");
                    //bargraphBtmGoldSE_growth.Series["Ach"].YValueMembers = "growth";
                    //bargraphBtmGoldSE_growth.Series["Ach"].XValueMember = "Name";
                    //bargraphBtmGoldSE_growth.ChartAreas[0].AxisY.Title = "GROWTH%";
                    //bargraphBtmGoldSE_growth.DataBind();
                    //foreach (var item in bargraphBtmGoldSE_growth.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}

                    if (dtBottomGoldSe_Growth.Rows.Count > 0)
                    {
                        view = new DataView(dtBottomGoldSe_Growth);
                        distinctValues = new DataTable();
                        dtBottomGoldSe_Growth = new DataTable();
                        dtBottomGoldSe_Growth = view.ToTable(false, column);
                        distinctValues = view.ToTable(true, "growth");
                    }
                    //bargraphBtmGoldSE_growth1.DataSource = chartDataTable_GOLD_Sorted(dtBottomGoldSe_Growth, distinctValues, "BOTTOM");
                    //bargraphBtmGoldSE_growth1.Series["Ach"].YValueMembers = "growth";
                    //bargraphBtmGoldSE_growth1.Series["Ach"].XValueMember = "Name";
                    //bargraphBtmGoldSE_growth1.ChartAreas[0].AxisY.Title = "GROWTH%";
                    //bargraphBtmGoldSE_growth1.DataBind();
                    //foreach (var item in bargraphBtmGoldSE_growth1.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}
                    #endregion
                    //end

                    //bar graph for TOP GOLD CUSTOMERS
                    #region GOLD CUSTOMERS
                    dt = gridtable_all(dtTopGoldCustomers_withBranch, "CUST");
                    if (dt.Rows.Count > 0)
                    {
                        //dtTopBranches = dtTopSalesEngineers.Tables[0];
                        DataRow[] foundRows = dt.Select("customerNumber='Total'");
                        cus_Sales_value_year_0 = Convert.ToString(foundRows[0]["Sales_value_year_0"]);
                        cus_budget_value = Convert.ToString(foundRows[0]["budget"]);
                        cus_Sales_ytd_value = Convert.ToString(foundRows[0]["ytd"]);
                        cus_ytd_plan = Convert.ToString(foundRows[0]["ytd_plan"]);
                        cus_achvmnt = Convert.ToString(foundRows[0]["ach"]);
                        cus_askrate = Convert.ToString(foundRows[0]["arate"]);
                        cus_growth = Convert.ToString(foundRows[0]["growth"]);
                        foundRows[0].Delete();
                        dt.AcceptChanges();
                    }
                    Session["dtGoldCust"] = dt;
                    GridTopGoldCstmrs.DataSource = dt;
                    GridTopGoldCstmrs.DataBind();
                    bindgridColor_GoldCustomers();
                    column = new string[] { "customer_short_name", "Sales_value_year_0", "budget_value", "Sales_ytd_value", "ytd_plan", "achvmnt", "Askrate", "customer_number", "growth" };

                    if (dtTopGoldCustomers.Rows.Count > 0)
                    {
                        view = new DataView(dtTopGoldCustomers);
                        distinctValues = new DataTable();
                        dtTopGoldCustomers = new DataTable();
                        dtTopGoldCustomers = view.ToTable(false, column);
                        distinctValues = view.ToTable(true, "budget_value");
                    }
                    Session["TopCustAchChart"] = chartDataTable_GOLD_Sorted(dtTopGoldCustomers, distinctValues, "TOP", 10);
                    //bargraphtopgoldcustomers.DataSource = chartDataTable_GOLD_Sorted(dtTopGoldCustomers, distinctValues, "TOP", 10);
                    //bargraphtopgoldcustomers.Series["Ach"].YValueMembers = "budget_value";
                    //bargraphtopgoldcustomers.Series["Ach"].XValueMember = "Name";
                    //bargraphtopgoldcustomers.ChartAreas[0].AxisY.Title = year + " BUDGET";
                    //bargraphtopgoldcustomers.DataBind();
                    //foreach (var item in bargraphtopgoldcustomers.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}

                    //if (dtTopGoldCustomers.Rows.Count > 0)
                    //{
                    //    view = new DataView(dtTopGoldCustomers);
                    //    distinctValues = new DataTable();
                    //    dtTopGoldCustomers = new DataTable();
                    //    dtTopGoldCustomers = view.ToTable(false, column);
                    //    distinctValues = view.ToTable(true, "budget_value");
                    //}
                    //bargraphtopgoldcustomers1.DataSource = chartDataTable_GOLD_Sorted(dtTopGoldCustomers, distinctValues, "TOP", 10);
                    //bargraphtopgoldcustomers1.Series["Ach"].YValueMembers = "budget_value";
                    //bargraphtopgoldcustomers1.Series["Ach"].XValueMember = "Name";
                    //bargraphtopgoldcustomers1.ChartAreas[0].AxisY.Title = year + " BUDGET";
                    //bargraphtopgoldcustomers1.DataBind();
                    //foreach (var item in bargraphtopgoldcustomers1.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}


                    if (dtTopGoldCustomers_Growth.Rows.Count > 0)
                    {
                        view = new DataView(dtTopGoldCustomers_Growth);
                        distinctValues = new DataTable();
                        dtTopGoldCustomers_Growth = new DataTable();
                        dtTopGoldCustomers_Growth = view.ToTable(false, column);
                        distinctValues = view.ToTable(true, "growth");
                    }
                    Session["TopCustGroChart"] = chartDataTable_GOLD_Sorted(dtTopGoldCustomers_Growth, distinctValues, "TOP", 10);
                    //bargraphtopgoldcustomers_growth.DataSource = chartDataTable_GOLD_Sorted(dtTopGoldCustomers_Growth, distinctValues, "TOP", 10);
                    //bargraphtopgoldcustomers_growth.Series["Ach"].YValueMembers = "growth";
                    //bargraphtopgoldcustomers_growth.Series["Ach"].XValueMember = "Name";
                    //bargraphtopgoldcustomers_growth.ChartAreas[0].AxisY.Title = "GROWTH%";
                    //bargraphtopgoldcustomers_growth.DataBind();
                    //foreach (var item in bargraphtopgoldcustomers_growth.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}

                    //if (dtTopGoldCustomers_Growth.Rows.Count > 0)
                    //{
                    //    view = new DataView(dtTopGoldCustomers_Growth);
                    //    distinctValues = new DataTable();
                    //    dtTopGoldCustomers_Growth = new DataTable();
                    //    dtTopGoldCustomers_Growth = view.ToTable(false, column);
                    //    distinctValues = view.ToTable(true, "growth");
                    //}
                    //bargraphtopgoldcustomers_growth1.DataSource = chartDataTable_GOLD_Sorted(dtTopGoldCustomers_Growth, distinctValues, "TOP", 10);
                    //bargraphtopgoldcustomers_growth1.Series["Ach"].YValueMembers = "growth";
                    //bargraphtopgoldcustomers_growth1.Series["Ach"].XValueMember = "Name";
                    //bargraphtopgoldcustomers_growth1.ChartAreas[0].AxisY.Title = "GROWTH%";
                    //bargraphtopgoldcustomers_growth1.DataBind();
                    //foreach (var item in bargraphtopgoldcustomers_growth1.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}
                    #endregion
                    //end

                    //BAR GRAPH FOR TOP CP
                    #region GOLD CHANNEL PARTNERS
                    dt = gridtable_all(dtTopGoldChannelPartners_WithBranch, "CUST");
                    if (dt.Rows.Count > 0)
                    {
                        //dtTopBranches = dtTopSalesEngineers.Tables[0];
                        DataRow[] foundRows = dt.Select("customerNumber='Total'");
                        cp_Sales_value_year_0 = Convert.ToString(foundRows[0]["Sales_value_year_0"]);
                        cp_budget_value = Convert.ToString(foundRows[0]["budget"]);
                        cp_Sales_ytd_value = Convert.ToString(foundRows[0]["ytd"]);
                        cp_ytd_plan = Convert.ToString(foundRows[0]["ytd_plan"]);
                        cp_achvmnt = Convert.ToString(foundRows[0]["ach"]);
                        cp_askrate = Convert.ToString(foundRows[0]["arate"]);
                        cp_growth = Convert.ToString(foundRows[0]["growth"]);
                        foundRows[0].Delete();
                        dt.AcceptChanges();
                    }
                    Session["dtGoldCP"] = dt;
                    GridTopGoldCP.DataSource = dt;
                    GridTopGoldCP.DataBind();
                    bindgridColor_GoldCP();

                    if (dtTopGoldChannelPartners.Rows.Count > 0)
                    {
                        view = new DataView(dtTopGoldChannelPartners);
                        distinctValues = new DataTable();
                        distinctValues = view.ToTable(true, "budget_value");
                    }
                    if (dtTopGoldChannelPartners.Columns.Contains("EngineerName"))
                    {
                        dtTopGoldChannelPartners.Columns.Remove("EngineerName");
                    }
                    Session["TopCPAchChart"] = chartDataTable_GOLD_Sorted(dtTopGoldChannelPartners, distinctValues, "TOP", 10);
                    //bargraphtopgoldcp.DataSource = chartDataTable_GOLD_Sorted(dtTopGoldChannelPartners, distinctValues, "TOP", 10);
                    //bargraphtopgoldcp.Series["Ach"].YValueMembers = "budget_value";
                    //bargraphtopgoldcp.Series["Ach"].XValueMember = "Name";
                    //bargraphtopgoldcp.ChartAreas[0].AxisY.Title = year + " BUDGET";
                    //bargraphtopgoldcp.DataBind();
                    //foreach (var item in bargraphtopgoldcp.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}

                    //if (dtTopGoldChannelPartners.Rows.Count > 0)
                    //{
                    //    view = new DataView(dtTopGoldChannelPartners);
                    //    distinctValues = new DataTable();
                    //    distinctValues = view.ToTable(true, "budget_value");
                    //}
                    //bargraphtopgoldcp1.DataSource = chartDataTable_GOLD_Sorted(dtTopGoldChannelPartners, distinctValues, "TOP", 10);
                    //bargraphtopgoldcp1.Series["Ach"].YValueMembers = "budget_value";
                    //bargraphtopgoldcp1.Series["Ach"].XValueMember = "Name";
                    //bargraphtopgoldcp1.ChartAreas[0].AxisY.Title = year + " BUDGET";
                    //bargraphtopgoldcp1.DataBind();
                    //foreach (var item in bargraphtopgoldcp1.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}

                    if (dtGoldChannelPartners_Growth.Rows.Count > 0)
                    {
                        view = new DataView(dtGoldChannelPartners_Growth);
                        distinctValues = new DataTable();
                        distinctValues = view.ToTable(true, "growth");
                    }
                    if (dtGoldChannelPartners_Growth.Columns.Contains("EngineerName"))
                    {
                        dtGoldChannelPartners_Growth.Columns.Remove("EngineerName");
                    }
                    Session["TopCPGroChart"] = chartDataTable_GOLD_Sorted(dtGoldChannelPartners_Growth, distinctValues, "TOP", 10);
                    //bargraphtopgoldcp_growth.DataSource = chartDataTable_GOLD_Sorted(dtGoldChannelPartners_Growth, distinctValues, "TOP", 10);
                    //bargraphtopgoldcp_growth.Series["Ach"].YValueMembers = "growth";
                    //bargraphtopgoldcp_growth.Series["Ach"].XValueMember = "Name";
                    //bargraphtopgoldcp_growth.ChartAreas[0].AxisY.Title = "GROWTH%";
                    //bargraphtopgoldcp_growth.DataBind();
                    //foreach (var item in bargraphtopgoldcp_growth.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}

                    //if (dtGoldChannelPartners_Growth.Rows.Count > 0)
                    //{
                    //    view = new DataView(dtGoldChannelPartners_Growth);
                    //    distinctValues = new DataTable();
                    //    distinctValues = view.ToTable(true, "growth");
                    //}
                    //bargraphtopgoldcp_growth1.DataSource = chartDataTable_GOLD_Sorted(dtGoldChannelPartners_Growth, distinctValues, "TOP", 10);
                    //bargraphtopgoldcp_growth1.Series["Ach"].YValueMembers = "growth";
                    //bargraphtopgoldcp_growth1.Series["Ach"].XValueMember = "Name";
                    //bargraphtopgoldcp_growth1.ChartAreas[0].AxisY.Title = "GROWTH%";
                    //bargraphtopgoldcp_growth1.DataBind();
                    //foreach (var item in bargraphtopgoldcp_growth1.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}
                    #endregion

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "$('#MainContent_GridTopGoldCstmrs').append('<tfoot><tr> <th style=\"text-align:right\">TOTAL:</th><th style=\"text-align: right\"></th><th style=\"text-align: right\"></th><th style=\"text-align: right\"></th><th style=\"text-align: right\">" + cus_Sales_value_year_0 + "</th><th style=\"text-align: right\">" + cus_budget_value + "</th><th style=\"text-align: right\">" + cus_ytd_plan + "</th><th style=\"text-align: right\">" + cus_Sales_ytd_value + "</th><th style=\"text-align: right\">" + cus_askrate + "</th><th style=\"text-align: right\">" + cus_achvmnt + "</th><th style=\"text-align: right\">" + cus_growth + "</th></tr></tfoot>');"
                    + "$('#MainContent_GridTopGoldCP').append('<tfoot><tr> <th style=\"text-align:right\">TOTAL:</th><th style=\"text-align: right\"></th><th style=\"text-align: right\"></th><th style=\"text-align: right\"></th><th style=\"text-align: right\">" + cp_Sales_value_year_0 + "</th><th style=\"text-align: right\">" + cp_budget_value + "</th><th style=\"text-align: right\">" + cp_ytd_plan + "</th><th style=\"text-align: right\">" + cp_Sales_ytd_value + "</th><th style=\"text-align: right\">" + cp_askrate + "</th><th style=\"text-align: right\">" + cp_achvmnt + "</th><th style=\"text-align: right\">" + cp_growth + "</th></tr></tfoot>');"
                    + "$('#MainContent_GridTopGoldSE').append('<tfoot><tr> <th style=\"text-align:right\">TOTAL:</th><th style=\"text-align: right\"></th><th style=\"text-align: right\">" + se_Sales_value_year_0 + "</th><th style=\"text-align: right\">" + se_budget_value + "</th><th style=\"text-align: right\">" + se_ytd_plan + "</th><th style=\"text-align: right\">" + se_Sales_ytd_value + "</th><th style=\"text-align: right\">" + se_askrate + "</th><th style=\"text-align: right\">" + se_achvmnt + "</th><th style=\"text-align: right\">" + se_growth + "</th></tr></tfoot>');"
                    + "$('#MainContent_GridTopGoldBranches').append('<tfoot><tr> <th style=\"text-align:right\">TOTAL:</th><th style=\"text-align: right\">" + branch_Sales_value_year_0 + "</th><th style=\"text-align: right\">" + branch_budget_value + "</th><th style=\"text-align: right\">" + branch_ytd_plan + "</th><th style=\"text-align: right\">" + branch_Sales_ytd_value + "</th><th style=\"text-align: right\">" + branch_askrate + "</th><th style=\"text-align: right\">" + branch_achvmnt + "</th><th style=\"text-align: right\">" + branch_growth + "</th></tr></tfoot>');", true);

                    #endregion
                }

                if (flag == "FAMILY" || flag == "ALL")
                {
                    #region Family
                    //bar graph for FAMILY
                    //LoadFamilyGraph();
                    Session["FamAchChart"] = chartDataTable_all(dtFamily);
                    //bargraphgamily.DataSource = chartDataTable_all(dtFamily);
                    //bargraphgamily.Series["Ach"].YValueMembers = "ach";
                    //bargraphgamily.Series["Ach"].XValueMember = "Name";
                    //bargraphgamily.ChartAreas[0].AxisY.Title = "PRO-RATA ACH%";
                    //bargraphgamily.DataBind();
                    //foreach (var item in bargraphgamily.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}
                    Session["FamGroChart"] = chartDataTable_all(dtFamily);
                    //bargraphgamily_growth.DataSource = chartDataTable_all(dtFamily);
                    //bargraphgamily_growth.Series["Ach"].YValueMembers = "growth";
                    //bargraphgamily_growth.Series["Ach"].XValueMember = "Name";
                    //bargraphgamily_growth.ChartAreas[0].AxisY.Title = "GROWTH%";
                    //bargraphgamily_growth.DataBind();
                    //foreach (var item in bargraphgamily_growth.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}
                    //bargraphgamily_growth1.DataSource = chartDataTable_all(dtFamily);
                    //bargraphgamily_growth1.Series["Ach"].YValueMembers = "growth";
                    //bargraphgamily_growth1.Series["Ach"].XValueMember = "Name";
                    //bargraphgamily_growth1.ChartAreas[0].AxisY.Title = "GROWTH%";
                    //bargraphgamily_growth1.DataBind();
                    //foreach (var item in bargraphgamily_growth1.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}

                    //bargraphgamily1.DataSource = chartDataTable_all(dtFamily);
                    //bargraphgamily1.Series["Ach"].YValueMembers = "ach";
                    //bargraphgamily1.Series["Ach"].XValueMember = "Name";
                    //bargraphgamily1.ChartAreas[0].AxisY.Title = "PRO-RATA ACH%";
                    //bargraphgamily1.DataBind();
                    //foreach (var item in bargraphgamily1.Series["Ach"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}


                    DataTable dt = new DataTable();
                    dt = gridtable_all(dtFamily, "FAM");

                    if (dt.Rows.Count > 0)
                    {
                        //dtTopBranches = dtTopSalesEngineers.Tables[0];
                        DataRow[] foundRows = dt.Select("Name='Total'");
                        hdn_Sales_value_year_0.Value = Convert.ToString(foundRows[0]["sales_value_year_0"]);
                        hdn_budget_value.Value = Convert.ToString(foundRows[0]["budget"]);
                        hdn_Sales_ytd_value.Value = Convert.ToString(foundRows[0]["ytd"]);
                        hdn_ytd_plan.Value = Convert.ToString(foundRows[0]["ytd_plan"]);
                        hdn_achvmnt.Value = Convert.ToString(foundRows[0]["ach"]);
                        hdn_askrate.Value = Convert.ToString(foundRows[0]["arate"]);
                        hdn_growth.Value = Convert.ToString(foundRows[0]["growth"]);
                        foundRows[0].Delete();
                        dt.AcceptChanges();
                    }
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Alert", "$('#MainContent_GridFamily').append('<tfoot><tr> <th style=\"text-align:right\">TOTAL:</th><th style=\"text-align: right\">" + hdn_Sales_value_year_0.Value + "</th><th style=\"text-align: right\">" + hdn_budget_value.Value + "</th><th style=\"text-align: right\">" + hdn_ytd_plan.Value + "</th><th style=\"text-align: right\">" + hdn_Sales_ytd_value.Value + "</th><th style=\"text-align: right\">" + hdn_askrate.Value + "</th><th style=\"text-align: right\">" + hdn_achvmnt.Value + "</th><th style=\"text-align: right\">" + hdn_growth.Value + "</th></tr></tfoot>');", true);

                    if (dt.Rows.Count > 0)
                    {
                        Session["dtFam"] = dt;
                        GridFamily.DataSource = dt;
                    }
                    else
                    {
                        GridFamily.DataSource = null;
                    }
                    //GridFamily.DataSource = gridtable_all(dtFamily,"FAM");
                    GridFamily.DataBind();
                    bindgridColor_Family();

                    #endregion
                }
                if (strUserRole == "BM" || strUserRole == "SE")
                {
                    divGoldBranch.Visible = false;
                }
                if (strUserRole == "SE")
                {
                    divGoldSe.Visible = false;
                }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        protected DataTable gridtable_all(DataTable dt, string flag)
        {


            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };
            DataTable griddt = new DataTable();
            if (flag == "CUST")
            {
                if (dt.Rows.Count != 0)
                {

                    griddt.Columns.Add("customerNumber", typeof(string));
                    griddt.Columns.Add("Name", typeof(string));
                    griddt.Columns.Add("Branch", typeof(string));
                    griddt.Columns.Add("EngineerName", typeof(string));
                    griddt.Columns.Add("sales_value_year_0", typeof(string));
                    griddt.Columns.Add("budget", typeof(string));
                    griddt.Columns.Add("ytd", typeof(string));
                    griddt.Columns.Add("ytd_plan", typeof(string));
                    griddt.Columns.Add("ach", typeof(string));
                    griddt.Columns.Add("arate", typeof(string));
                    griddt.Columns.Add("growth", typeof(string));
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        string name = dt.Rows[i].ItemArray[0].ToString() == null ? ""
                                       : (dt.Rows[i]["customer_short_name"].ToString());
                        string branch = dt.Rows[i].ItemArray[0].ToString() == null ? ""
                                       : (dt.Rows[i]["Branch"].ToString());
                        string se = dt.Rows[i].ItemArray[0].ToString() == null ? ""
                                       : (dt.Rows[i]["EngineerName"].ToString());
                        float sales_value_year_0 = dt.Rows[i]["Sales_value_year_0"].ToString() == "" || dt.Rows[i]["Sales_value_year_0"].ToString() == null ? 0
                               : Convert.ToInt64(dt.Rows[i]["Sales_value_year_0"].ToString());
                        float budget = dt.Rows[i]["budget_value"].ToString() == "" || dt.Rows[i]["budget_value"].ToString() == null ? 0
                              : Convert.ToInt64(dt.Rows[i]["budget_value"].ToString());
                        float ytd = dt.Rows[i]["Sales_ytd_value"].ToString() == "" || dt.Rows[i]["Sales_ytd_value"].ToString() == null ? 0
                             : Convert.ToInt64(dt.Rows[i]["Sales_ytd_value"].ToString());
                        float ytd_plan = dt.Rows[i]["ytd_plan"].ToString() == "" || dt.Rows[i]["ytd_plan"].ToString() == null ? 0
                           : Convert.ToInt64(dt.Rows[i]["ytd_plan"].ToString());
                        float achvs = dt.Rows[i]["achvmnt"].ToString() == "" || dt.Rows[i]["achvmnt"].ToString() == null ? 0
                           : Convert.ToInt64(dt.Rows[i]["achvmnt"].ToString());
                        float ask_rate = dt.Rows[i]["askrate"].ToString() == "" || dt.Rows[i]["askrate"].ToString() == null ? 0
                           : Convert.ToInt64(dt.Rows[i]["askrate"].ToString());
                        string customerNumber = dt.Rows[i]["customer_number"].ToString() == "" || dt.Rows[i]["customer_number"].ToString() == null ? ""
                           : (dt.Rows[i]["customer_number"].ToString());
                        float growth = dt.Rows[i]["growth"].ToString() == "" || dt.Rows[i]["growth"].ToString() == null ? 0
                           : Convert.ToInt64(dt.Rows[i]["growth"].ToString());

                        sales_value_year_0 = sales_value_year_0 / byValueIn;
                        budget = budget / byValueIn;
                        ytd = ytd / byValueIn;
                        ytd_plan = ytd_plan / byValueIn;
                        ask_rate = ask_rate / byValueIn;
                        bool allCoulmnszero = objReports.DeleteEmptyRows(0, sales_value_year_0, budget, ytd, ytd_plan, achvs, ask_rate);
                        if (allCoulmnszero == false)
                        {
                            griddt.Rows.Add(customerNumber, name, branch, se, Math.Round(sales_value_year_0).ToString("N0", culture), Math.Round(budget).ToString("N0", culture), Math.Round(ytd).ToString("N0", culture),
                                Math.Round(ytd_plan).ToString("N0", culture), achvs.ToString(), ask_rate.ToString("N0", culture), growth.ToString());
                        }
                    }
                }
            }
            else if (flag == "BRANCH")
            {
                if (dt.Rows.Count != 0)
                {

                    griddt.Columns.Add("Name", typeof(string));
                    griddt.Columns.Add("sales_value_year_0", typeof(string));
                    griddt.Columns.Add("budget", typeof(string));
                    griddt.Columns.Add("ytd", typeof(string));
                    griddt.Columns.Add("ytd_plan", typeof(string));
                    griddt.Columns.Add("ach", typeof(string));
                    griddt.Columns.Add("arate", typeof(string));
                    griddt.Columns.Add("growth", typeof(string));
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        string name = dt.Rows[i].ItemArray[0].ToString() == null ? ""
                                       : (dt.Rows[i]["region_description"].ToString());
                        float sales_value_year_0 = dt.Rows[i]["Sales_value_year_0"].ToString() == "" || dt.Rows[i]["Sales_value_year_0"].ToString() == null ? 0
                               : Convert.ToInt64(dt.Rows[i]["Sales_value_year_0"].ToString());
                        float budget = dt.Rows[i]["budget_value"].ToString() == "" || dt.Rows[i]["budget_value"].ToString() == null ? 0
                              : Convert.ToInt64(dt.Rows[i]["budget_value"].ToString());
                        float ytd = dt.Rows[i]["Sales_ytd_value"].ToString() == "" || dt.Rows[i]["Sales_ytd_value"].ToString() == null ? 0
                             : Convert.ToInt64(dt.Rows[i]["Sales_ytd_value"].ToString());
                        float ytd_plan = dt.Rows[i]["ytd_plan"].ToString() == "" || dt.Rows[i]["ytd_plan"].ToString() == null ? 0
                           : Convert.ToInt64(dt.Rows[i]["ytd_plan"].ToString());
                        float achvs = dt.Rows[i]["achvmnt"].ToString() == "" || dt.Rows[i]["achvmnt"].ToString() == null ? 0
                           : Convert.ToInt64(dt.Rows[i]["achvmnt"].ToString());
                        float ask_rate = dt.Rows[i]["askrate"].ToString() == "" || dt.Rows[i]["askrate"].ToString() == null ? 0
                           : Convert.ToInt64(dt.Rows[i]["askrate"].ToString());
                        float growth = dt.Rows[i]["growth"].ToString() == "" || dt.Rows[i]["growth"].ToString() == null ? 0
                           : Convert.ToInt64(dt.Rows[i]["growth"].ToString());

                        sales_value_year_0 = sales_value_year_0 / byValueIn;
                        budget = budget / byValueIn;
                        ytd = ytd / byValueIn;
                        ytd_plan = ytd_plan / byValueIn;
                        ask_rate = ask_rate / byValueIn;
                        bool allCoulmnszero = objReports.DeleteEmptyRows(0, sales_value_year_0, budget, ytd, ytd_plan, achvs, ask_rate);
                        if (allCoulmnszero == false)
                        {
                            griddt.Rows.Add(name, Math.Round(sales_value_year_0).ToString("N0", culture), Math.Round(budget).ToString("N0", culture), Math.Round(ytd).ToString("N0", culture),
                                Math.Round(ytd_plan).ToString("N0", culture), achvs.ToString(), ask_rate.ToString("N0", culture), growth.ToString());
                        }
                    }
                }
            }
            else if (flag == "SE")
            {
                if (dt.Rows.Count != 0)
                {

                    griddt.Columns.Add("Name", typeof(string));
                    griddt.Columns.Add("Branch", typeof(string));
                    griddt.Columns.Add("sales_value_year_0", typeof(string));
                    griddt.Columns.Add("budget", typeof(string));
                    griddt.Columns.Add("ytd", typeof(string));
                    griddt.Columns.Add("ytd_plan", typeof(string));
                    griddt.Columns.Add("ach", typeof(string));
                    griddt.Columns.Add("arate", typeof(string));
                    griddt.Columns.Add("growth", typeof(string));
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        string name = dt.Rows[i].ItemArray[0].ToString() == null ? ""
                                       : (dt.Rows[i]["EngineerName"].ToString());
                        string branch = dt.Rows[i].ItemArray[0].ToString() == null ? ""
                                       : (dt.Rows[i]["Branch"].ToString());
                        float sales_value_year_0 = dt.Rows[i]["Sales_value_year_0"].ToString() == "" || dt.Rows[i]["Sales_value_year_0"].ToString() == null ? 0
                               : Convert.ToInt64(dt.Rows[i]["Sales_value_year_0"].ToString());
                        float budget = dt.Rows[i]["budget_value"].ToString() == "" || dt.Rows[i]["budget_value"].ToString() == null ? 0
                              : Convert.ToInt64(dt.Rows[i]["budget_value"].ToString());
                        float ytd = dt.Rows[i]["Sales_ytd_value"].ToString() == "" || dt.Rows[i]["Sales_ytd_value"].ToString() == null ? 0
                             : Convert.ToInt64(dt.Rows[i]["Sales_ytd_value"].ToString());
                        float ytd_plan = dt.Rows[i]["ytd_plan"].ToString() == "" || dt.Rows[i]["ytd_plan"].ToString() == null ? 0
                           : Convert.ToInt64(dt.Rows[i]["ytd_plan"].ToString());
                        float achvs = dt.Rows[i]["achvmnt"].ToString() == "" || dt.Rows[i]["achvmnt"].ToString() == null ? 0
                           : Convert.ToInt64(dt.Rows[i]["achvmnt"].ToString());
                        float ask_rate = dt.Rows[i]["askrate"].ToString() == "" || dt.Rows[i]["askrate"].ToString() == null ? 0
                           : Convert.ToInt64(dt.Rows[i]["askrate"].ToString());
                        float growth = dt.Rows[i]["growth"].ToString() == "" || dt.Rows[i]["growth"].ToString() == null ? 0
                           : Convert.ToInt64(dt.Rows[i]["growth"].ToString());

                        sales_value_year_0 = sales_value_year_0 / byValueIn;
                        budget = budget / byValueIn;
                        ytd = ytd / byValueIn;
                        ytd_plan = ytd_plan / byValueIn;
                        ask_rate = ask_rate / byValueIn;
                        bool allCoulmnszero = objReports.DeleteEmptyRows(0, sales_value_year_0, budget, ytd, ytd_plan, achvs, ask_rate);
                        if (allCoulmnszero == false)
                        {
                            griddt.Rows.Add(name, branch, Math.Round(sales_value_year_0).ToString("N0", culture), Math.Round(budget).ToString("N0", culture), Math.Round(ytd).ToString("N0", culture),
                                Math.Round(ytd_plan).ToString("N0", culture), achvs.ToString(), ask_rate.ToString("N0", culture), growth.ToString());
                        }
                    }
                }
            }
            else if (flag == "FAM")
            {
                if (dt.Rows.Count != 0)
                {

                    griddt.Columns.Add("Name", typeof(string));
                    griddt.Columns.Add("sales_value_year_0", typeof(string));
                    griddt.Columns.Add("budget", typeof(string));
                    griddt.Columns.Add("ytd", typeof(string));
                    griddt.Columns.Add("ytd_plan", typeof(string));
                    griddt.Columns.Add("ach", typeof(string));
                    griddt.Columns.Add("arate", typeof(string));
                    griddt.Columns.Add("growth", typeof(string));
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {

                        string name = dt.Rows[i].ItemArray[0].ToString() == null ? ""
                                       : (dt.Rows[i]["item_family_name"].ToString());
                        float sales_value_year_0 = dt.Rows[i]["Sales_value_year_0"].ToString() == "" || dt.Rows[i]["Sales_value_year_0"].ToString() == null ? 0
                               : Convert.ToInt64(dt.Rows[i]["Sales_value_year_0"].ToString());
                        float budget = dt.Rows[i]["budget_value"].ToString() == "" || dt.Rows[i]["budget_value"].ToString() == null ? 0
                              : Convert.ToInt64(dt.Rows[i]["budget_value"].ToString());
                        float ytd = dt.Rows[i]["Sales_ytd_value"].ToString() == "" || dt.Rows[i]["Sales_ytd_value"].ToString() == null ? 0
                             : Convert.ToInt64(dt.Rows[i]["Sales_ytd_value"].ToString());
                        float ytd_plan = dt.Rows[i]["ytd_plan"].ToString() == "" || dt.Rows[i]["ytd_plan"].ToString() == null ? 0
                           : Convert.ToInt64(dt.Rows[i]["ytd_plan"].ToString());
                        float achvs = dt.Rows[i]["achvmnt"].ToString() == "" || dt.Rows[i]["achvmnt"].ToString() == null ? 0
                           : Convert.ToInt64(dt.Rows[i]["achvmnt"].ToString());
                        float ask_rate = dt.Rows[i]["askrate"].ToString() == "" || dt.Rows[i]["askrate"].ToString() == null ? 0
                           : Convert.ToInt64(dt.Rows[i]["askrate"].ToString());
                        float growth = dt.Rows[i]["growth"].ToString() == "" || dt.Rows[i]["growth"].ToString() == null ? 0
                           : Convert.ToInt64(dt.Rows[i]["growth"].ToString());

                        sales_value_year_0 = sales_value_year_0 / byValueIn;
                        budget = budget / byValueIn;
                        ytd = ytd / byValueIn;
                        ytd_plan = ytd_plan / byValueIn;
                        ask_rate = ask_rate / byValueIn;
                        bool allCoulmnszero = objReports.DeleteEmptyRows(0, sales_value_year_0, budget, ytd, ytd_plan, achvs, ask_rate);
                        if (allCoulmnszero == false)
                        {
                            griddt.Rows.Add(name, Math.Round(sales_value_year_0).ToString("N0", culture), Math.Round(budget).ToString("N0", culture), Math.Round(ytd).ToString("N0", culture),
                                Math.Round(ytd_plan).ToString("N0", culture), achvs.ToString(), ask_rate.ToString("N0", culture), growth.ToString());
                        }
                    }
                }
            }
            return griddt;

        }
        protected DataTable chartDataTable_all(DataTable dt)
        {
            actual_mnth = (objConfig.getActualMonth());
            DataTable griddt = new DataTable();
            if (dt.Rows.Count != 0)
            {

                griddt.Columns.Add("Name", typeof(string));

                griddt.Columns.Add("sales_value_year_0", typeof(string));
                griddt.Columns.Add("budget_value", typeof(string));
                griddt.Columns.Add("Sales_ytd_value", typeof(string));
                griddt.Columns.Add("ytd_plan", typeof(string));
                griddt.Columns.Add("ach", typeof(string));
                griddt.Columns.Add("Askrate", typeof(string));
                griddt.Columns.Add("growth", typeof(string));
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    string name = dt.Rows[i].ItemArray[0].ToString() == null ? ""
                                   : (dt.Rows[i].ItemArray[0].ToString());
                    float sales_value_year_0 = dt.Rows[i].ItemArray[1].ToString() == "" || dt.Rows[i].ItemArray[1].ToString() == null ? 0
                           : Convert.ToInt64(dt.Rows[i].ItemArray[1].ToString());
                    float budget = dt.Rows[i].ItemArray[2].ToString() == "" || dt.Rows[i].ItemArray[2].ToString() == null ? 0
                          : Convert.ToInt64(dt.Rows[i].ItemArray[2].ToString());
                    float ytd = dt.Rows[i].ItemArray[3].ToString() == "" || dt.Rows[i].ItemArray[3].ToString() == null ? 0
                         : Convert.ToInt64(dt.Rows[i].ItemArray[3].ToString());
                    float ytd_plan = dt.Rows[i].ItemArray[4].ToString() == "" || dt.Rows[i].ItemArray[4].ToString() == null ? 0
                       : Convert.ToInt64(dt.Rows[i].ItemArray[4].ToString());
                    float achvs = dt.Rows[i].ItemArray[5].ToString() == "" || dt.Rows[i].ItemArray[5].ToString() == null ? 0
                       : Convert.ToInt64(dt.Rows[i].ItemArray[5].ToString());
                    float ask_rate = dt.Rows[i].ItemArray[6].ToString() == "" || dt.Rows[i].ItemArray[6].ToString() == null ? 0
                       : Convert.ToInt64(dt.Rows[i].ItemArray[6].ToString());
                    float growth = dt.Rows[i].ItemArray[8].ToString() == "" || dt.Rows[i].ItemArray[8].ToString() == null ? 0
                       : Convert.ToInt64(dt.Rows[i].ItemArray[8].ToString());

                    sales_value_year_0 = sales_value_year_0 / byValueIn;
                    budget = budget / byValueIn;
                    ytd = ytd / byValueIn;
                    ytd_plan = ytd_plan / byValueIn;
                    ask_rate = ask_rate / byValueIn;
                    bool allCoulmnszero = objReports.DeleteEmptyRows(0, sales_value_year_0, budget, ytd, ytd_plan, achvs, ask_rate);
                    if (allCoulmnszero == false)
                    {
                        griddt.Rows.Add(name, Math.Round(sales_value_year_0, 0).ToString(), Math.Round(budget, 0).ToString(), Math.Round(ytd, 0).ToString(),
                            Math.Round(ytd_plan, 0).ToString(), achvs.ToString(), Math.Round(ask_rate, 0).ToString(), growth.ToString());
                    }
                }
            }
            return griddt;
        }

        protected DataTable gridtable(DataTable dt, string sortOrder = "TOP", int numberOfRows = 5)
        {


            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };
            DataTable griddt = new DataTable();
            if (dt.Rows.Count != 0)
            {
                dt = Get_T_B_FiveValues(dt, sortOrder, numberOfRows);
                griddt.Columns.Add("customerNumber", typeof(string));
                griddt.Columns.Add("Name", typeof(string));

                griddt.Columns.Add("sales_value_year_0", typeof(string));
                griddt.Columns.Add("budget", typeof(string));
                griddt.Columns.Add("ytd", typeof(string));
                griddt.Columns.Add("ytd_plan", typeof(string));
                griddt.Columns.Add("ach", typeof(string));
                griddt.Columns.Add("arate", typeof(string));
                griddt.Columns.Add("growth", typeof(string));
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    string name = dt.Rows[i].ItemArray[0].ToString() == null ? ""
                                   : (dt.Rows[i].ItemArray[0].ToString());
                    float sales_value_year_0 = dt.Rows[i].ItemArray[1].ToString() == "" || dt.Rows[i].ItemArray[1].ToString() == null ? 0
                           : Convert.ToInt64(dt.Rows[i].ItemArray[1].ToString());
                    float budget = dt.Rows[i].ItemArray[2].ToString() == "" || dt.Rows[i].ItemArray[2].ToString() == null ? 0
                          : Convert.ToInt64(dt.Rows[i].ItemArray[2].ToString());
                    float ytd = dt.Rows[i].ItemArray[3].ToString() == "" || dt.Rows[i].ItemArray[3].ToString() == null ? 0
                         : Convert.ToInt64(dt.Rows[i].ItemArray[3].ToString());
                    float ytd_plan = dt.Rows[i].ItemArray[4].ToString() == "" || dt.Rows[i].ItemArray[4].ToString() == null ? 0
                       : Convert.ToInt64(dt.Rows[i].ItemArray[4].ToString());
                    float achvs = dt.Rows[i].ItemArray[5].ToString() == "" || dt.Rows[i].ItemArray[5].ToString() == null ? 0
                       : Convert.ToInt64(dt.Rows[i].ItemArray[5].ToString());
                    float ask_rate = dt.Rows[i].ItemArray[6].ToString() == "" || dt.Rows[i].ItemArray[6].ToString() == null ? 0
                       : Convert.ToInt64(dt.Rows[i].ItemArray[6].ToString());
                    string customerNumber = dt.Rows[i].ItemArray[7].ToString() == "" || dt.Rows[i].ItemArray[7].ToString() == null ? ""
                       : (dt.Rows[i].ItemArray[7].ToString());

                    float growth = dt.Rows[i].ItemArray[8].ToString() == "" || dt.Rows[i].ItemArray[8].ToString() == null ? 0
                       : Convert.ToInt64(dt.Rows[i].ItemArray[8].ToString());
                    sales_value_year_0 = sales_value_year_0 / byValueIn;
                    budget = budget / byValueIn;
                    ytd = ytd / byValueIn;
                    ytd_plan = ytd_plan / byValueIn;
                    ask_rate = ask_rate / byValueIn;
                    griddt.Rows.Add(customerNumber, name, Math.Round(sales_value_year_0).ToString("N0", culture), Math.Round(budget).ToString("N0", culture), Math.Round(ytd).ToString("N0", culture),
                        Math.Round(ytd_plan).ToString("N0", culture), achvs.ToString(), ask_rate.ToString("N0", culture), growth.ToString());
                }
            }
            return griddt;
        }
        protected DataTable gridtable_GOLD(DataTable dt, string sortOrder = "TOP", int numberOfRows = 5)
        {


            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };
            DataTable griddt = new DataTable();
            if (dt.Rows.Count != 0)
            {
                dt = Get_T_B_FiveValues_GOLD(dt, sortOrder, numberOfRows);
                griddt.Columns.Add("customerNumber", typeof(string));
                griddt.Columns.Add("Name", typeof(string));

                griddt.Columns.Add("sales_value_year_0", typeof(string));
                griddt.Columns.Add("budget", typeof(string));
                griddt.Columns.Add("ytd", typeof(string));
                griddt.Columns.Add("ytd_plan", typeof(string));
                griddt.Columns.Add("ach", typeof(string));
                griddt.Columns.Add("arate", typeof(string));
                griddt.Columns.Add("growth", typeof(string));
                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    string name = dt.Rows[i].ItemArray[0].ToString() == null ? ""
                                   : (dt.Rows[i].ItemArray[0].ToString());
                    float sales_value_year_0 = dt.Rows[i].ItemArray[1].ToString() == "" || dt.Rows[i].ItemArray[1].ToString() == null ? 0
                           : Convert.ToInt64(dt.Rows[i].ItemArray[1].ToString());
                    float budget = dt.Rows[i].ItemArray[2].ToString() == "" || dt.Rows[i].ItemArray[2].ToString() == null ? 0
                          : Convert.ToInt64(dt.Rows[i].ItemArray[2].ToString());
                    float ytd = dt.Rows[i].ItemArray[3].ToString() == "" || dt.Rows[i].ItemArray[3].ToString() == null ? 0
                         : Convert.ToInt64(dt.Rows[i].ItemArray[3].ToString());
                    float ytd_plan = dt.Rows[i].ItemArray[4].ToString() == "" || dt.Rows[i].ItemArray[4].ToString() == null ? 0
                       : Convert.ToInt64(dt.Rows[i].ItemArray[4].ToString());
                    float achvs = dt.Rows[i].ItemArray[5].ToString() == "" || dt.Rows[i].ItemArray[5].ToString() == null ? 0
                       : Convert.ToInt64(dt.Rows[i].ItemArray[5].ToString());
                    float ask_rate = dt.Rows[i].ItemArray[6].ToString() == "" || dt.Rows[i].ItemArray[6].ToString() == null ? 0
                       : Convert.ToInt64(dt.Rows[i].ItemArray[6].ToString());
                    string customerNumber = dt.Rows[i].ItemArray[7].ToString() == "" || dt.Rows[i].ItemArray[7].ToString() == null ? ""
                       : (dt.Rows[i].ItemArray[7].ToString());

                    float growth = dt.Rows[i].ItemArray[8].ToString() == "" || dt.Rows[i].ItemArray[8].ToString() == null ? 0
                       : Convert.ToInt64(dt.Rows[i].ItemArray[8].ToString());
                    sales_value_year_0 = sales_value_year_0 / byValueIn;
                    budget = budget / byValueIn;
                    ytd = ytd / byValueIn;
                    ytd_plan = ytd_plan / byValueIn;
                    ask_rate = ask_rate / byValueIn;
                    griddt.Rows.Add(customerNumber, name, Math.Round(sales_value_year_0).ToString("N0", culture), Math.Round(budget).ToString("N0", culture), Math.Round(ytd).ToString("N0", culture),
                        Math.Round(ytd_plan).ToString("N0", culture), achvs.ToString(), ask_rate.ToString("N0", culture), growth.ToString());
                }
            }
            return griddt;
        }

        /// <summary>
        /// Author :Anamika
        /// Date : April 13,2017
        /// </summary>
        /// <param name="dtTemp"></param>
        /// <param name="distinctValues"></param>
        /// <param name="flag"></param>
        /// <param name="numberOfRows"></param>
        /// <returns></returns>
        protected DataTable Get_T_B_FiveValues_Sorted(DataTable dtTemp, DataTable distinctValues, string flag, int numberOfRows)
        {
            DataView view = new DataView(dtTemp);
            DataTable dtMain = new DataTable();
            dtMain = dtTemp.Clone();
            int count = 0;
            for (int i = 0; i < distinctValues.Rows.Count; i++)
            {

                for (int j = 0; j < dtTemp.Rows.Count; j++)
                {
                    if (count < numberOfRows)
                    {
                        if (distinctValues.Columns[0].ColumnName.ToString() == "growth")
                        {
                            if (dtTemp.Rows[j].ItemArray[8].ToString().Equals(distinctValues.Rows[i].ItemArray[0].ToString()))
                            {
                                count++;
                                dtMain.ImportRow(dtTemp.Rows[j]);
                            }
                        }
                        else
                        {
                            if (dtTemp.Rows[j].ItemArray[5].ToString().Equals(distinctValues.Rows[i].ItemArray[0].ToString()))
                            {
                                count++;
                                dtMain.ImportRow(dtTemp.Rows[j]);
                            }
                        }

                    }
                    else
                    {
                        break;
                    }
                }
            }
            return dtMain;
        }

        protected DataTable Get_T_B_FiveValues_GOLD_Sorted(DataTable dtTemp, DataTable distinctValues, string flag, int numberOfRows)
        {
            DataView view = new DataView(dtTemp);
            DataTable dtMain = new DataTable();
            dtMain = dtTemp.Clone();
            int count = 0;
            for (int i = 0; i < distinctValues.Rows.Count; i++)
            {

                for (int j = 0; j < dtTemp.Rows.Count; j++)
                {
                    if (count < numberOfRows)
                    {
                        if (distinctValues.Columns[0].ColumnName.ToString() == "growth")
                        {
                            if (dtTemp.Rows[j].ItemArray[8].ToString().Equals(distinctValues.Rows[i].ItemArray[0].ToString()))
                            {
                                count++;
                                dtMain.ImportRow(dtTemp.Rows[j]);
                            }
                        }
                        else
                        {
                            if (dtTemp.Rows[j].ItemArray[2].ToString() == distinctValues.Rows[i].ItemArray[0].ToString())
                            {
                                count++;
                                dtMain.ImportRow(dtTemp.Rows[j]);
                            }
                        }
                    }

                    else
                    {
                        break;
                    }
                }
            }

            return dtMain;
        }

        protected DataTable Get_T_B_FiveValues(DataTable dtTemp, string flag, int numberOfRows)
        {
            DataView view = new DataView(dtTemp);
            DataTable distinctValues = view.ToTable(true, "achvmnt");
            DataTable dtMain = new DataTable();
            dtMain = dtTemp.Clone();

            if (flag == "TOP")
            {

                for (int i = 0; i < distinctValues.Rows.Count; i++)
                {
                    if (i < numberOfRows)
                    {
                        for (int j = 0; j < dtTemp.Rows.Count; j++)
                        {
                            if (dtTemp.Rows[j].ItemArray[5].ToString() == distinctValues.Rows[i].ItemArray[0].ToString())
                            {
                                dtMain.ImportRow(dtTemp.Rows[j]);
                            }
                        }
                    }
                }
            }
            else
            {

                int count = 0;
                for (int i = distinctValues.Rows.Count - 1; i >= 0; i--)
                {

                    if (count <= numberOfRows)
                    {
                        for (int j = 0; j < dtTemp.Rows.Count; j++)
                        {
                            if (dtTemp.Rows[j].ItemArray[5].ToString() == distinctValues.Rows[i].ItemArray[0].ToString())
                            {
                                count++;
                                dtMain.ImportRow(dtTemp.Rows[j]);
                            }
                        }
                    }
                }
            }
            return dtMain;
        }

        protected DataTable Get_T_B_FiveValues_GOLD(DataTable dtTemp, string flag, int numberOfRows)
        {
            DataView view = new DataView(dtTemp);
            DataTable distinctValues = view.ToTable(true, "budget_value");
            DataTable dtMain = new DataTable();
            dtMain = dtTemp.Clone();

            if (flag == "TOP")
            {
                for (int i = 0; i < distinctValues.Rows.Count; i++)
                {
                    if (i < numberOfRows)
                    {
                        for (int j = 0; j < dtTemp.Rows.Count; j++)
                        {
                            if (dtTemp.Rows[j].ItemArray[2].ToString() == distinctValues.Rows[i].ItemArray[0].ToString())
                            {
                                dtMain.ImportRow(dtTemp.Rows[j]);
                            }
                        }
                    }
                }
            }
            else
            {
                int count = 0;
                for (int i = distinctValues.Rows.Count - 1; i >= 0; i--)
                {
                    count++;
                    if (count <= numberOfRows)
                    {
                        for (int j = 0; j < dtTemp.Rows.Count; j++)
                        {
                            if (dtTemp.Rows[j].ItemArray[2].ToString() == distinctValues.Rows[i].ItemArray[0].ToString())
                            {
                                dtMain.ImportRow(dtTemp.Rows[j]);
                            }
                        }
                    }
                }
            }
            return dtMain;
        }


        /// <summary>
        /// Author : Anamika
        /// </summary>
        /// <param name="dt"></param>
        /// <param name="dt1"></param>
        /// <param name="sortOrder"></param>
        /// <param name="numberOfRows"></param>
        /// <returns></returns>
        protected DataTable chartDataTable_Sorted(DataTable dt, DataTable dt1, string sortOrder = "TOP", int numberOfRows = 5)
        {
            actual_mnth = (objConfig.getActualMonth());
            DataTable griddt = new DataTable();
            if (dt.Rows.Count != 0)
            {
                if (dt.Columns.Contains("Branch"))
                    dt.Columns.Remove("Branch");
                dt = Get_T_B_FiveValues_Sorted(dt, dt1, sortOrder, numberOfRows);
                griddt.Columns.Add("Name", typeof(string));

                griddt.Columns.Add("sales_value_year_0", typeof(string));
                griddt.Columns.Add("budget_value", typeof(string));
                griddt.Columns.Add("Sales_ytd_value", typeof(string));
                griddt.Columns.Add("ytd_plan", typeof(string));
                griddt.Columns.Add("ach", typeof(string));
                griddt.Columns.Add("Askrate", typeof(string));
                griddt.Columns.Add("growth", typeof(string));

                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    string name = dt.Rows[i].ItemArray[0].ToString() == null ? ""
                                   : (dt.Rows[i].ItemArray[0].ToString());
                    float sales_value_year_0 = dt.Rows[i].ItemArray[1].ToString() == "" || dt.Rows[i].ItemArray[1].ToString() == null ? 0
                           : Convert.ToInt64(dt.Rows[i].ItemArray[1].ToString());
                    float budget = dt.Rows[i].ItemArray[2].ToString() == "" || dt.Rows[i].ItemArray[2].ToString() == null ? 0
                          : Convert.ToInt64(dt.Rows[i].ItemArray[2].ToString());
                    float ytd = dt.Rows[i].ItemArray[3].ToString() == "" || dt.Rows[i].ItemArray[3].ToString() == null ? 0
                         : Convert.ToInt64(dt.Rows[i].ItemArray[3].ToString());
                    float ytd_plan = dt.Rows[i].ItemArray[4].ToString() == "" || dt.Rows[i].ItemArray[4].ToString() == null ? 0
                       : Convert.ToInt64(dt.Rows[i].ItemArray[4].ToString());
                    float achvs = dt.Rows[i].ItemArray[5].ToString() == "" || dt.Rows[i].ItemArray[5].ToString() == null ? 0
                       : Convert.ToInt64(dt.Rows[i].ItemArray[5].ToString());
                    float ask_rate = dt.Rows[i].ItemArray[6].ToString() == "" || dt.Rows[i].ItemArray[6].ToString() == null ? 0
                       : Convert.ToInt64(dt.Rows[i].ItemArray[6].ToString());

                    float growth = dt.Rows[i].ItemArray[8].ToString() == "" || dt.Rows[i].ItemArray[8].ToString() == null ? 0
                       : Convert.ToInt64(dt.Rows[i].ItemArray[8].ToString());

                    sales_value_year_0 = sales_value_year_0 / byValueIn;
                    budget = budget / byValueIn;
                    ytd = ytd / byValueIn;
                    ytd_plan = ytd_plan / byValueIn;
                    ask_rate = ask_rate / byValueIn;
                    griddt.Rows.Add(name, Math.Round(sales_value_year_0, 0).ToString(), Math.Round(budget, 0).ToString(), Math.Round(ytd, 0).ToString(),
                        Math.Round(ytd_plan, 0).ToString(), achvs.ToString(), Math.Round(ask_rate, 0).ToString(), growth.ToString());
                }
            }
            return griddt;
        }


        protected DataTable chartDataTable_GOLD_Sorted(DataTable dt, DataTable dt1, string sortOrder = "TOP", int numberOfRows = 5)
        {
            actual_mnth = (objConfig.getActualMonth());
            DataTable griddt = new DataTable();
            if (dt.Rows.Count != 0)
            {
                if (dt.Columns.Contains("Branch"))
                {
                    dt.Columns.Remove("Branch");
                    dt.AcceptChanges();
                }
                dt = Get_T_B_FiveValues_GOLD_Sorted(dt, dt1, sortOrder, numberOfRows);
                griddt.Columns.Add("Name", typeof(string));

                griddt.Columns.Add("sales_value_year_0", typeof(string));
                griddt.Columns.Add("budget_value", typeof(string));
                griddt.Columns.Add("Sales_ytd_value", typeof(string));
                griddt.Columns.Add("ytd_plan", typeof(string));
                griddt.Columns.Add("ach", typeof(string));
                griddt.Columns.Add("Askrate", typeof(string));
                griddt.Columns.Add("growth", typeof(string));


                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    string name = dt.Rows[i].ItemArray[0].ToString() == null ? ""
                                   : (dt.Rows[i].ItemArray[0].ToString());
                    float sales_value_year_0 = dt.Rows[i].ItemArray[1].ToString() == "" || dt.Rows[i].ItemArray[1].ToString() == null ? 0
                           : Convert.ToInt64(dt.Rows[i].ItemArray[1].ToString());
                    float budget = dt.Rows[i].ItemArray[2].ToString() == "" || dt.Rows[i].ItemArray[2].ToString() == null ? 0
                          : Convert.ToInt64(dt.Rows[i].ItemArray[2].ToString());
                    float ytd = dt.Rows[i].ItemArray[3].ToString() == "" || dt.Rows[i].ItemArray[3].ToString() == null ? 0
                         : Convert.ToInt64(dt.Rows[i].ItemArray[3].ToString());
                    float ytd_plan = dt.Rows[i].ItemArray[4].ToString() == "" || dt.Rows[i].ItemArray[4].ToString() == null ? 0
                       : Convert.ToInt64(dt.Rows[i].ItemArray[4].ToString());
                    float achvs = dt.Rows[i].ItemArray[5].ToString() == "" || dt.Rows[i].ItemArray[5].ToString() == null ? 0
                       : Convert.ToInt64(dt.Rows[i].ItemArray[5].ToString());
                    float ask_rate = dt.Rows[i].ItemArray[6].ToString() == "" || dt.Rows[i].ItemArray[6].ToString() == null ? 0
                       : Convert.ToInt64(dt.Rows[i].ItemArray[6].ToString());

                    float growth = dt.Rows[i].ItemArray[8].ToString() == "" || dt.Rows[i].ItemArray[8].ToString() == null ? 0
                       : Convert.ToInt64(dt.Rows[i].ItemArray[8].ToString());

                    sales_value_year_0 = sales_value_year_0 / byValueIn;
                    budget = budget / byValueIn;
                    ytd = ytd / byValueIn;
                    ytd_plan = ytd_plan / byValueIn;
                    ask_rate = ask_rate / byValueIn;
                    griddt.Rows.Add(name, Math.Round(sales_value_year_0, 0).ToString(), Math.Round(budget, 0).ToString(), Math.Round(ytd, 0).ToString(),
                        Math.Round(ytd_plan, 0).ToString(), achvs.ToString(), Math.Round(ask_rate, 0).ToString(), growth.ToString());
                }
            }
            return griddt;
        }


        protected DataTable chartDataTable(DataTable dt, string sortOrder = "TOP", int numberOfRows = 5)
        {
            actual_mnth = (objConfig.getActualMonth());
            DataTable griddt = new DataTable();
            if (dt.Rows.Count != 0)
            {
                if (dt.Columns.Contains("Branch"))
                    dt.Columns.Remove("Branch");
                dt = Get_T_B_FiveValues(dt, sortOrder, numberOfRows);
                griddt.Columns.Add("Name", typeof(string));

                griddt.Columns.Add("sales_value_year_0", typeof(string));
                griddt.Columns.Add("budget_value", typeof(string));
                griddt.Columns.Add("Sales_ytd_value", typeof(string));
                griddt.Columns.Add("ytd_plan", typeof(string));
                griddt.Columns.Add("ach", typeof(string));
                griddt.Columns.Add("Askrate", typeof(string));
                griddt.Columns.Add("growth", typeof(string));

                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    string name = dt.Rows[i].ItemArray[0].ToString() == null ? ""
                                   : (dt.Rows[i].ItemArray[0].ToString());
                    float sales_value_year_0 = dt.Rows[i].ItemArray[1].ToString() == "" || dt.Rows[i].ItemArray[1].ToString() == null ? 0
                           : Convert.ToInt64(dt.Rows[i].ItemArray[1].ToString());
                    float budget = dt.Rows[i].ItemArray[2].ToString() == "" || dt.Rows[i].ItemArray[2].ToString() == null ? 0
                          : Convert.ToInt64(dt.Rows[i].ItemArray[2].ToString());
                    float ytd = dt.Rows[i].ItemArray[3].ToString() == "" || dt.Rows[i].ItemArray[3].ToString() == null ? 0
                         : Convert.ToInt64(dt.Rows[i].ItemArray[3].ToString());
                    float ytd_plan = dt.Rows[i].ItemArray[4].ToString() == "" || dt.Rows[i].ItemArray[4].ToString() == null ? 0
                       : Convert.ToInt64(dt.Rows[i].ItemArray[4].ToString());
                    float achvs = dt.Rows[i].ItemArray[5].ToString() == "" || dt.Rows[i].ItemArray[5].ToString() == null ? 0
                       : Convert.ToInt64(dt.Rows[i].ItemArray[5].ToString());
                    float ask_rate = dt.Rows[i].ItemArray[6].ToString() == "" || dt.Rows[i].ItemArray[6].ToString() == null ? 0
                       : Convert.ToInt64(dt.Rows[i].ItemArray[6].ToString());

                    float growth = dt.Rows[i].ItemArray[8].ToString() == "" || dt.Rows[i].ItemArray[8].ToString() == null ? 0
                       : Convert.ToInt64(dt.Rows[i].ItemArray[8].ToString());

                    sales_value_year_0 = sales_value_year_0 / byValueIn;
                    budget = budget / byValueIn;
                    ytd = ytd / byValueIn;
                    ytd_plan = ytd_plan / byValueIn;
                    ask_rate = ask_rate / byValueIn;
                    griddt.Rows.Add(name, Math.Round(sales_value_year_0, 0).ToString(), Math.Round(budget, 0).ToString(), Math.Round(ytd, 0).ToString(),
                        Math.Round(ytd_plan, 0).ToString(), achvs.ToString(), Math.Round(ask_rate, 0).ToString(), growth.ToString());
                }
            }
            return griddt;
        }

        protected DataTable chartDataTable_GOLD(DataTable dt, string sortOrder = "TOP", int numberOfRows = 5)
        {
            actual_mnth = (objConfig.getActualMonth());
            DataTable griddt = new DataTable();
            if (dt.Rows.Count != 0)
            {
                if (dt.Columns.Contains("Branch"))
                    dt.Columns.Remove("Branch");
                dt = Get_T_B_FiveValues_GOLD(dt, sortOrder, numberOfRows);
                griddt.Columns.Add("Name", typeof(string));

                griddt.Columns.Add("sales_value_year_0", typeof(string));
                griddt.Columns.Add("budget_value", typeof(string));
                griddt.Columns.Add("Sales_ytd_value", typeof(string));
                griddt.Columns.Add("ytd_plan", typeof(string));
                griddt.Columns.Add("ach", typeof(string));
                griddt.Columns.Add("Askrate", typeof(string));
                griddt.Columns.Add("growth", typeof(string));

                for (int i = 0; i < dt.Rows.Count; i++)
                {

                    string name = dt.Rows[i].ItemArray[0].ToString() == null ? ""
                                   : (dt.Rows[i].ItemArray[0].ToString());
                    float sales_value_year_0 = dt.Rows[i].ItemArray[1].ToString() == "" || dt.Rows[i].ItemArray[1].ToString() == null ? 0
                           : Convert.ToInt64(dt.Rows[i].ItemArray[1].ToString());
                    float budget = dt.Rows[i].ItemArray[2].ToString() == "" || dt.Rows[i].ItemArray[2].ToString() == null ? 0
                          : Convert.ToInt64(dt.Rows[i].ItemArray[2].ToString());
                    float ytd = dt.Rows[i].ItemArray[3].ToString() == "" || dt.Rows[i].ItemArray[3].ToString() == null ? 0
                         : Convert.ToInt64(dt.Rows[i].ItemArray[3].ToString());
                    float ytd_plan = dt.Rows[i].ItemArray[4].ToString() == "" || dt.Rows[i].ItemArray[4].ToString() == null ? 0
                       : Convert.ToInt64(dt.Rows[i].ItemArray[4].ToString());
                    float achvs = dt.Rows[i].ItemArray[5].ToString() == "" || dt.Rows[i].ItemArray[5].ToString() == null ? 0
                       : Convert.ToInt64(dt.Rows[i].ItemArray[5].ToString());
                    float ask_rate = dt.Rows[i].ItemArray[6].ToString() == "" || dt.Rows[i].ItemArray[6].ToString() == null ? 0
                       : Convert.ToInt64(dt.Rows[i].ItemArray[6].ToString());

                    float growth = dt.Rows[i].ItemArray[8].ToString() == "" || dt.Rows[i].ItemArray[8].ToString() == null ? 0
                       : Convert.ToInt64(dt.Rows[i].ItemArray[8].ToString());

                    sales_value_year_0 = sales_value_year_0 / byValueIn;
                    budget = budget / byValueIn;
                    ytd = ytd / byValueIn;
                    ytd_plan = ytd_plan / byValueIn;
                    ask_rate = ask_rate / byValueIn;
                    griddt.Rows.Add(name, Math.Round(sales_value_year_0, 0).ToString(), Math.Round(budget, 0).ToString(), Math.Round(ytd, 0).ToString(),
                        Math.Round(ytd_plan, 0).ToString(), achvs.ToString(), Math.Round(ask_rate, 0).ToString(), growth.ToString());
                }
            }
            return griddt;
        }

        protected DataTable chart_dtcnsldtd(DataTable dt)
        {
            actual_mnth = (objConfig.getActualMonth());
            DataTable cdtcnsldtd = new DataTable();
            //actual_mnth = 12 - (objConfig.getActualMonth());
            if (dt.Rows.Count != 0)
            {
                cdtcnsldtd.Columns.Add("Sales_value_year_0", typeof(string));
                cdtcnsldtd.Columns.Add("budget_value", typeof(string));
                cdtcnsldtd.Columns.Add("ytd_plan", typeof(string));
                cdtcnsldtd.Columns.Add("Sales_ytd_value", typeof(string));

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    float sales_value_year_0 = dt.Rows[i].ItemArray[0].ToString() == "" || dt.Rows[i].ItemArray[0].ToString() == null ? 0
                                              : Convert.ToInt64(dt.Rows[i].ItemArray[0].ToString());
                    float budget = dt.Rows[i].ItemArray[1].ToString() == "" || dt.Rows[i].ItemArray[1].ToString() == null ? 0
                          : Convert.ToInt64(dt.Rows[i].ItemArray[1].ToString());
                    float ytd_plan = dt.Rows[i].ItemArray[2].ToString() == "" || dt.Rows[i].ItemArray[2].ToString() == null ? 0
                         : Convert.ToInt64(dt.Rows[i].ItemArray[2].ToString());
                    float ytd = dt.Rows[i].ItemArray[3].ToString() == "" || dt.Rows[i].ItemArray[3].ToString() == null ? 0
                        : Convert.ToInt64(dt.Rows[i].ItemArray[3].ToString());
                    sales_value_year_0 = sales_value_year_0 / byValueIn;
                    budget = budget / byValueIn;
                    ytd = ytd / byValueIn;
                    ytd_plan = ytd_plan / byValueIn;

                    cdtcnsldtd.Rows.Add(Math.Round(sales_value_year_0, 0).ToString(), Math.Round(budget, 0).ToString(),
                        Math.Round(ytd_plan, 0).ToString(), Math.Round(ytd, 0).ToString());

                }
            }
            return cdtcnsldtd;
        }

        private byte[] Compress(byte[] b)
        {
            MemoryStream ms = new MemoryStream();
            GZipStream zs = new GZipStream(ms, CompressionMode.Compress, true);
            zs.Write(b, 0, b.Length);
            zs.Close();
            return ms.ToArray();
        }

        /// This method takes the compressed byte stream as parameter
        /// and return a decompressed bytestream.

        private byte[] Decompress(byte[] b)
        {
            MemoryStream ms = new MemoryStream();
            GZipStream zs = new GZipStream(new MemoryStream(b),
                                           CompressionMode.Decompress, true);
            byte[] buffer = new byte[4096];
            int size;
            while (true)
            {
                size = zs.Read(buffer, 0, buffer.Length);
                if (size > 0)
                    ms.Write(buffer, 0, size);
                else break;
            }
            zs.Close();
            return ms.ToArray();
        }


        #endregion


        #region WebMethods

        [WebMethod]
        public static List<string> setBranchTotal(string search)
        {
            DataTable dt = new DataTable();
            DataTable dtOutput = new DataTable();
            List<string> hdndata = new List<string>();
            List<string> searchList = new List<string>();
            DataRow[] filteredRows;
            dt = (DataTable)HttpContext.Current.Session["dtBranch"];
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(search))
                    {
                        if (search.Contains(' '))
                        {
                            searchList = search.Split(' ').ToList();
                        }
                        else
                        {
                            searchList.Add(search);
                        }
                        for (int i = 0; i < searchList.Count; i++)
                        {
                            dtOutput = new DataTable();
                            dtOutput.Columns.Add("Name");
                            dtOutput.Columns.Add("sales_value_year_0");
                            dtOutput.Columns.Add("budget");
                            dtOutput.Columns.Add("ytd");
                            dtOutput.Columns.Add("ytd_plan");
                            dtOutput.Columns.Add("ach");
                            dtOutput.Columns.Add("arate");
                            dtOutput.Columns.Add("growth");

                            search = Convert.ToString(searchList[i]);

                            filteredRows = dt.Select("CONVERT(Name, System.String) LIKE '%" + search
                              + "%' OR CONVERT(sales_value_year_0, System.String) LIKE'%" + search
                              + "%' OR CONVERT(budget, System.String) LIKE'%" + search
                              + "%' OR CONVERT(ytd, System.String) LIKE'%" + search
                              + "%' OR CONVERT(ytd_plan, System.String) LIKE'%" + search
                              + "%' OR CONVERT(ach, System.String) LIKE'%" + search
                              + "%' OR CONVERT(arate, System.String) LIKE'%" + search
                              + "%' OR CONVERT(growth, System.String) LIKE '%" + search
                              + "%'");

                            foreach (DataRow dr in filteredRows)
                            {
                                dtOutput.Rows.Add(dr.ItemArray);
                            }
                            dt = dtOutput;
                        }
                    }


                    string Sales_value_year_0 = "0";
                    string Sales_ytd_value = "0";
                    string budget_value = "0";
                    string ytd_plan = "0";
                    string achvmnt = "0";
                    string askrate = "0";
                    string growth = "0";

                    if (dt.Rows.Count != null)
                    {

                        Sales_value_year_0 = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["sales_value_year_0"])).ToString();
                        Sales_ytd_value = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ytd"])).ToString();
                        budget_value = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["budget"])).ToString();
                        ytd_plan = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ytd_plan"])).ToString();
                        //achvmnt = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ach"])).ToString();
                        askrate = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["arate"])).ToString();
                        //growth = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["growth"])).ToString();
                        achvmnt = Convert.ToInt32(ytd_plan) == 0 ? Convert.ToString(0) : Convert.ToString((Convert.ToInt32(Sales_ytd_value) * 100) / Convert.ToInt32(ytd_plan));
                        //growth = Convert.ToString(Math.Ceiling(Convert.ToDecimal(((Convert.ToInt32(Sales_ytd_value) / Convert.ToInt32(Sales_value_year_0))-1)*100)));
                        growth = Convert.ToInt32(Sales_value_year_0) == 0 ? Convert.ToString(0) : Convert.ToString((Convert.ToInt32(Sales_ytd_value) - Convert.ToInt32(Sales_value_year_0)) * 100 / Convert.ToInt32(Sales_value_year_0));
                        //if (Convert.ToDecimal(Sales_value_year_0) != 0)
                        //    growth = Convert.ToString(Math.Round(((Convert.ToDecimal(Sales_ytd_value) / Convert.ToDecimal(Sales_value_year_0))-1) * 100, 0));


                    }
                    hdndata.Add(Convert.ToString(Sales_value_year_0));
                    hdndata.Add(Convert.ToString(Sales_ytd_value));
                    hdndata.Add(Convert.ToString(budget_value));
                    hdndata.Add(Convert.ToString(ytd_plan));
                    hdndata.Add(Convert.ToString(achvmnt));
                    hdndata.Add(Convert.ToString(askrate));
                    hdndata.Add(Convert.ToString(growth));


                }
            }
            return hdndata;
        }


        [WebMethod]
        public static List<string> setSETotal(string search)
        {
            DataTable dt = new DataTable();
            DataTable dtOutput = new DataTable();
            List<string> hdndata = new List<string>();
            List<string> searchList = new List<string>();
            DataRow[] filteredRows;
            dt = (DataTable)HttpContext.Current.Session["dtSE"];
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(search))
                    {
                        if (search.Contains(' '))
                        {
                            searchList = search.Split(' ').ToList();
                        }
                        else
                        {
                            searchList.Add(search);
                        }
                        for (int i = 0; i < searchList.Count; i++)
                        {
                            dtOutput = new DataTable();
                            dtOutput.Columns.Add("Name");
                            dtOutput.Columns.Add("Branch");
                            dtOutput.Columns.Add("sales_value_year_0");
                            dtOutput.Columns.Add("budget");
                            dtOutput.Columns.Add("ytd");
                            dtOutput.Columns.Add("ytd_plan");
                            dtOutput.Columns.Add("ach");
                            dtOutput.Columns.Add("arate");
                            dtOutput.Columns.Add("growth");
                            search = Convert.ToString(searchList[i]);
                            filteredRows = dt.Select("CONVERT(Name, System.String) LIKE '%" + search
                                + "%' OR CONVERT(Branch, System.String) LIKE'%" + search
                              + "%' OR CONVERT(sales_value_year_0, System.String) LIKE'%" + search
                              + "%' OR CONVERT(budget, System.String) LIKE'%" + search
                              + "%' OR CONVERT(ytd, System.String) LIKE'%" + search
                              + "%' OR CONVERT(ytd_plan, System.String) LIKE'%" + search
                              + "%' OR CONVERT(ach, System.String) LIKE'%" + search
                              + "%' OR CONVERT(arate, System.String) LIKE'%" + search
                              + "%' OR CONVERT(growth, System.String) LIKE '%" + search
                              + "%'");

                            foreach (DataRow dr in filteredRows)
                            {
                                dtOutput.Rows.Add(dr.ItemArray);
                            }
                            dt = new DataTable();
                            dt = dtOutput;
                        }
                    }


                    string Sales_value_year_0 = "0";
                    string Sales_ytd_value = "0";
                    string budget_value = "0";
                    string ytd_plan = "0";
                    string achvmnt = "0";
                    string askrate = "0";
                    string growth = "0";

                    if (dt.Rows.Count != null)
                    {

                        Sales_value_year_0 = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["sales_value_year_0"])).ToString();
                        Sales_ytd_value = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ytd"])).ToString();
                        budget_value = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["budget"])).ToString();
                        ytd_plan = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ytd_plan"])).ToString();
                        //achvmnt = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ach"])).ToString();
                        askrate = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["arate"])).ToString();
                        //growth = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["growth"])).ToString();
                        achvmnt = Convert.ToInt32(ytd_plan) == 0 ? Convert.ToString(0) : Convert.ToString((Convert.ToInt32(Sales_ytd_value) * 100) / Convert.ToInt32(ytd_plan));
                        //growth = Convert.ToString(Math.Ceiling(Convert.ToDecimal(((Convert.ToInt32(Sales_ytd_value) / Convert.ToInt32(Sales_value_year_0))-1)*100)));
                        growth = Convert.ToInt32(Sales_value_year_0) == 0 ? Convert.ToString(0) : Convert.ToString((Convert.ToInt32(Sales_ytd_value) - Convert.ToInt32(Sales_value_year_0)) * 100 / Convert.ToInt32(Sales_value_year_0));
                        
                        //if (Convert.ToDecimal(Sales_value_year_0) != 0)
                        //    growth = Convert.ToString(Math.Round(((Convert.ToDecimal(Sales_ytd_value) / Convert.ToDecimal(Sales_value_year_0))-1) * 100, 0));


                    }
                    hdndata.Add(Convert.ToString(Sales_value_year_0));
                    hdndata.Add(Convert.ToString(Sales_ytd_value));
                    hdndata.Add(Convert.ToString(budget_value));
                    hdndata.Add(Convert.ToString(ytd_plan));
                    hdndata.Add(Convert.ToString(achvmnt));
                    hdndata.Add(Convert.ToString(askrate));
                    hdndata.Add(Convert.ToString(growth));


                }
            }
            return hdndata;
        }

        [WebMethod]
        public static List<string> setCustTotal(string search)
        {
            DataTable dt = new DataTable();
            DataTable dtOutput = new DataTable();
            List<string> hdndata = new List<string>();
            List<string> searchList = new List<string>();
            DataRow[] filteredRows;
            dt = (DataTable)HttpContext.Current.Session["dtCustomer"];
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(search))
                    {
                        if (search.Contains(' '))
                        {
                            searchList = search.Split(' ').ToList();
                        }
                        else
                        {
                            searchList.Add(search);
                        }
                        for (int i = 0; i < searchList.Count; i++)
                        {
                            dtOutput = new DataTable();
                            dtOutput.Columns.Add("customerNumber");
                            dtOutput.Columns.Add("Name");
                            dtOutput.Columns.Add("Branch");
                            dtOutput.Columns.Add("EngineerName");
                            dtOutput.Columns.Add("sales_value_year_0");
                            dtOutput.Columns.Add("budget");
                            dtOutput.Columns.Add("ytd");
                            dtOutput.Columns.Add("ytd_plan");
                            dtOutput.Columns.Add("ach");
                            dtOutput.Columns.Add("arate");
                            dtOutput.Columns.Add("growth");
                            search = Convert.ToString(searchList[i]);
                            filteredRows = dt.Select("CONVERT(Name, System.String) LIKE '%" + search
                                + "%' OR CONVERT(customerNumber, System.String) LIKE'%" + search
                                + "%' OR CONVERT(Branch, System.String) LIKE'%" + search
                                + "%' OR CONVERT(EngineerName, System.String) LIKE'%" + search
                              + "%' OR CONVERT(sales_value_year_0, System.String) LIKE'%" + search
                              + "%' OR CONVERT(budget, System.String) LIKE'%" + search
                              + "%' OR CONVERT(ytd, System.String) LIKE'%" + search
                              + "%' OR CONVERT(ytd_plan, System.String) LIKE'%" + search
                              + "%' OR CONVERT(ach, System.String) LIKE'%" + search
                              + "%' OR CONVERT(arate, System.String) LIKE'%" + search
                              + "%' OR CONVERT(growth, System.String) LIKE '%" + search
                              + "%'");

                            foreach (DataRow dr in filteredRows)
                            {
                                dtOutput.Rows.Add(dr.ItemArray);
                            }
                            dt = new DataTable();
                            dt = dtOutput;
                        }
                    }


                    string Sales_value_year_0 = "0";
                    string Sales_ytd_value = "0";
                    string budget_value = "0";
                    string ytd_plan = "0";
                    string achvmnt = "0";
                    string askrate = "0";
                    string growth = "0";

                    if (dt.Rows.Count != null)
                    {

                        Sales_value_year_0 = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["sales_value_year_0"])).ToString();
                        Sales_ytd_value = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ytd"])).ToString();
                        budget_value = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["budget"])).ToString();
                        ytd_plan = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ytd_plan"])).ToString();
                        //achvmnt = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ach"])).ToString();
                        askrate = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["arate"])).ToString();
                        //growth = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["growth"])).ToString();
                        achvmnt = Convert.ToInt32(ytd_plan) == 0 ? Convert.ToString(0) : Convert.ToString((Convert.ToInt32(Sales_ytd_value) * 100) / Convert.ToInt32(ytd_plan));
                        //growth = Convert.ToString(Math.Ceiling(Convert.ToDecimal(((Convert.ToInt32(Sales_ytd_value) / Convert.ToInt32(Sales_value_year_0))-1)*100)));
                        growth = Convert.ToInt32(Sales_value_year_0) == 0 ? Convert.ToString(0) : Convert.ToString((Convert.ToInt32(Sales_ytd_value) - Convert.ToInt32(Sales_value_year_0)) * 100 / Convert.ToInt32(Sales_value_year_0));
                        
                        //if (Convert.ToDecimal(Sales_value_year_0) != 0)
                        //    growth = Convert.ToString(Math.Round(((Convert.ToDecimal(Sales_ytd_value) / Convert.ToDecimal(Sales_value_year_0))-1) * 100, 0));


                    }
                    hdndata.Add(Convert.ToString(Sales_value_year_0));
                    hdndata.Add(Convert.ToString(Sales_ytd_value));
                    hdndata.Add(Convert.ToString(budget_value));
                    hdndata.Add(Convert.ToString(ytd_plan));
                    hdndata.Add(Convert.ToString(achvmnt));
                    hdndata.Add(Convert.ToString(askrate));
                    hdndata.Add(Convert.ToString(growth));


                }
            }
            return hdndata;
        }

        [WebMethod]
        public static List<string> setCPTotal(string search)
        {
            DataTable dt = new DataTable();
            DataTable dtOutput = new DataTable();
            List<string> hdndata = new List<string>();
            List<string> searchList = new List<string>();
            DataRow[] filteredRows;
            dt = (DataTable)HttpContext.Current.Session["dtCP"];
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(search))
                    {
                        if (search.Contains(' '))
                        {
                            searchList = search.Split(' ').ToList();
                        }
                        else
                        {
                            searchList.Add(search);
                        }
                        for (int i = 0; i < searchList.Count; i++)
                        {
                            dtOutput = new DataTable();
                            dtOutput.Columns.Add("customerNumber");
                            dtOutput.Columns.Add("Name");
                            dtOutput.Columns.Add("Branch");
                            dtOutput.Columns.Add("EngineerName");
                            dtOutput.Columns.Add("sales_value_year_0");
                            dtOutput.Columns.Add("budget");
                            dtOutput.Columns.Add("ytd");
                            dtOutput.Columns.Add("ytd_plan");
                            dtOutput.Columns.Add("ach");
                            dtOutput.Columns.Add("arate");
                            dtOutput.Columns.Add("growth");
                            search = Convert.ToString(searchList[i]);
                            filteredRows = dt.Select("CONVERT(Name, System.String) LIKE '%" + search
                                + "%' OR CONVERT(customerNumber, System.String) LIKE'%" + search
                                + "%' OR CONVERT(Branch, System.String) LIKE'%" + search
                                + "%' OR CONVERT(EngineerName, System.String) LIKE'%" + search
                              + "%' OR CONVERT(sales_value_year_0, System.String) LIKE'%" + search
                              + "%' OR CONVERT(budget, System.String) LIKE'%" + search
                              + "%' OR CONVERT(ytd, System.String) LIKE'%" + search
                              + "%' OR CONVERT(ytd_plan, System.String) LIKE'%" + search
                              + "%' OR CONVERT(ach, System.String) LIKE'%" + search
                              + "%' OR CONVERT(arate, System.String) LIKE'%" + search
                              + "%' OR CONVERT(growth, System.String) LIKE '%" + search
                              + "%'");

                            foreach (DataRow dr in filteredRows)
                            {
                                dtOutput.Rows.Add(dr.ItemArray);
                            }
                            dt = new DataTable();
                            dt = dtOutput;
                        }
                    }


                    string Sales_value_year_0 = "0";
                    string Sales_ytd_value = "0";
                    string budget_value = "0";
                    string ytd_plan = "0";
                    string achvmnt = "0";
                    string askrate = "0";
                    string growth = "0";

                    if (dt.Rows.Count != null)
                    {

                        Sales_value_year_0 = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["sales_value_year_0"])).ToString();
                        Sales_ytd_value = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ytd"])).ToString();
                        budget_value = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["budget"])).ToString();
                        ytd_plan = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ytd_plan"])).ToString();
                        //achvmnt = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ach"])).ToString();
                        askrate = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["arate"])).ToString();
                        //growth = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["growth"])).ToString();
                        achvmnt = Convert.ToInt32(ytd_plan)==0? Convert.ToString(0) : Convert.ToString((Convert.ToInt32(Sales_ytd_value) * 100) / Convert.ToInt32(ytd_plan));
                        //growth = Convert.ToString(Math.Ceiling(Convert.ToDecimal(((Convert.ToInt32(Sales_ytd_value) / Convert.ToInt32(Sales_value_year_0))-1)*100)));
                        growth = Convert.ToInt32(Sales_value_year_0) == 0 ? Convert.ToString(0) : Convert.ToString((Convert.ToInt32(Sales_ytd_value) - Convert.ToInt32(Sales_value_year_0)) * 100 / Convert.ToInt32(Sales_value_year_0));
                        
                        //if (Convert.ToDecimal(Sales_value_year_0) != 0)
                        //    growth = Convert.ToString(Math.Round(((Convert.ToDecimal(Sales_ytd_value) / Convert.ToDecimal(Sales_value_year_0))-1) * 100, 0));


                    }
                    hdndata.Add(Convert.ToString(Sales_value_year_0));
                    hdndata.Add(Convert.ToString(Sales_ytd_value));
                    hdndata.Add(Convert.ToString(budget_value));
                    hdndata.Add(Convert.ToString(ytd_plan));
                    hdndata.Add(Convert.ToString(achvmnt));
                    hdndata.Add(Convert.ToString(askrate));
                    hdndata.Add(Convert.ToString(growth));


                }
            }
            return hdndata;
        }

        [WebMethod]
        public static List<string> setGoldBranchTotal(string search)
        {
            DataTable dt = new DataTable();
            DataTable dtOutput = new DataTable();
            List<string> hdndata = new List<string>();
            List<string> searchList = new List<string>();
            DataRow[] filteredRows;
            dt = (DataTable)HttpContext.Current.Session["dtGoldBranch"];
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(search))
                    {
                        if (search.Contains(' '))
                        {
                            searchList = search.Split(' ').ToList();
                        }
                        else
                        {
                            searchList.Add(search);
                        }
                        for (int i = 0; i < searchList.Count; i++)
                        {
                            dtOutput = new DataTable();
                            dtOutput.Columns.Add("Name");
                            dtOutput.Columns.Add("sales_value_year_0");
                            dtOutput.Columns.Add("budget");
                            dtOutput.Columns.Add("ytd");
                            dtOutput.Columns.Add("ytd_plan");
                            dtOutput.Columns.Add("ach");
                            dtOutput.Columns.Add("arate");
                            dtOutput.Columns.Add("growth");
                            search = Convert.ToString(searchList[i]);
                            filteredRows = dt.Select("CONVERT(Name, System.String) LIKE '%" + search
                              + "%' OR CONVERT(sales_value_year_0, System.String) LIKE'%" + search
                              + "%' OR CONVERT(budget, System.String) LIKE'%" + search
                              + "%' OR CONVERT(ytd, System.String) LIKE'%" + search
                              + "%' OR CONVERT(ytd_plan, System.String) LIKE'%" + search
                              + "%' OR CONVERT(ach, System.String) LIKE'%" + search
                              + "%' OR CONVERT(arate, System.String) LIKE'%" + search
                              + "%' OR CONVERT(growth, System.String) LIKE '%" + search
                              + "%'");

                            foreach (DataRow dr in filteredRows)
                            {
                                dtOutput.Rows.Add(dr.ItemArray);
                            }
                            dt = dtOutput;
                        }
                    }


                    string Sales_value_year_0 = "0";
                    string Sales_ytd_value = "0";
                    string budget_value = "0";
                    string ytd_plan = "0";
                    string achvmnt = "0";
                    string askrate = "0";
                    string growth = "0";

                    if (dt.Rows.Count != null)
                    {

                        Sales_value_year_0 = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["sales_value_year_0"])).ToString();
                        Sales_ytd_value = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ytd"])).ToString();
                        budget_value = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["budget"])).ToString();
                        ytd_plan = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ytd_plan"])).ToString();
                        //achvmnt = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ach"])).ToString();
                        askrate = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["arate"])).ToString();
                        //growth = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["growth"])).ToString();
                        achvmnt = Convert.ToInt32(ytd_plan) == 0 ? Convert.ToString(0) : Convert.ToString((Convert.ToInt32(Sales_ytd_value) * 100) / Convert.ToInt32(ytd_plan));
                        //growth = Convert.ToString(Math.Ceiling(Convert.ToDecimal(((Convert.ToInt32(Sales_ytd_value) / Convert.ToInt32(Sales_value_year_0))-1)*100)));
                        growth = Convert.ToInt32(Sales_value_year_0) == 0 ? Convert.ToString(0) : Convert.ToString((Convert.ToInt32(Sales_ytd_value) - Convert.ToInt32(Sales_value_year_0)) * 100 / Convert.ToInt32(Sales_value_year_0));
                        
                        //if (Convert.ToDecimal(Sales_value_year_0) != 0)
                        //    growth = Convert.ToString(Math.Round(((Convert.ToDecimal(Sales_ytd_value) / Convert.ToDecimal(Sales_value_year_0))-1) * 100, 0));


                    }
                    hdndata.Add(Convert.ToString(Sales_value_year_0));
                    hdndata.Add(Convert.ToString(Sales_ytd_value));
                    hdndata.Add(Convert.ToString(budget_value));
                    hdndata.Add(Convert.ToString(ytd_plan));
                    hdndata.Add(Convert.ToString(achvmnt));
                    hdndata.Add(Convert.ToString(askrate));
                    hdndata.Add(Convert.ToString(growth));


                }
            }
            return hdndata;
        }


        [WebMethod]
        public static List<string> setGoldSETotal(string search)
        {
            DataTable dt = new DataTable();
            DataTable dtOutput = new DataTable();
            List<string> hdndata = new List<string>();
            List<string> searchList = new List<string>();
            DataRow[] filteredRows;
            dt = (DataTable)HttpContext.Current.Session["dtGoldSE"];
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(search))
                    {
                        if (search.Contains(' '))
                        {
                            searchList = search.Split(' ').ToList();
                        }
                        else
                        {
                            searchList.Add(search);
                        }
                        for (int i = 0; i < searchList.Count; i++)
                        {
                            dtOutput = new DataTable();
                            dtOutput.Columns.Add("Name");
                            dtOutput.Columns.Add("Branch");
                            dtOutput.Columns.Add("sales_value_year_0");
                            dtOutput.Columns.Add("budget");
                            dtOutput.Columns.Add("ytd");
                            dtOutput.Columns.Add("ytd_plan");
                            dtOutput.Columns.Add("ach");
                            dtOutput.Columns.Add("arate");
                            dtOutput.Columns.Add("growth");
                            search = Convert.ToString(searchList[i]);
                            filteredRows = dt.Select("CONVERT(Name, System.String) LIKE '%" + search
                                + "%' OR CONVERT(Branch, System.String) LIKE'%" + search
                              + "%' OR CONVERT(sales_value_year_0, System.String) LIKE'%" + search
                              + "%' OR CONVERT(budget, System.String) LIKE'%" + search
                              + "%' OR CONVERT(ytd, System.String) LIKE'%" + search
                              + "%' OR CONVERT(ytd_plan, System.String) LIKE'%" + search
                              + "%' OR CONVERT(ach, System.String) LIKE'%" + search
                              + "%' OR CONVERT(arate, System.String) LIKE'%" + search
                              + "%' OR CONVERT(growth, System.String) LIKE '%" + search
                              + "%'");

                            foreach (DataRow dr in filteredRows)
                            {
                                dtOutput.Rows.Add(dr.ItemArray);
                            }
                            dt = new DataTable();
                            dt = dtOutput;
                        }
                    }


                    string Sales_value_year_0 = "0";
                    string Sales_ytd_value = "0";
                    string budget_value = "0";
                    string ytd_plan = "0";
                    string achvmnt = "0";
                    string askrate = "0";
                    string growth = "0";

                    if (dt.Rows.Count != null)
                    {

                        Sales_value_year_0 = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["sales_value_year_0"])).ToString();
                        Sales_ytd_value = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ytd"])).ToString();
                        budget_value = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["budget"])).ToString();
                        ytd_plan = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ytd_plan"])).ToString();
                        //achvmnt = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ach"])).ToString();
                        askrate = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["arate"])).ToString();
                        //growth = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["growth"])).ToString();
                        achvmnt = Convert.ToInt32(ytd_plan) == 0 ? Convert.ToString(0) : Convert.ToString((Convert.ToInt32(Sales_ytd_value) * 100) / Convert.ToInt32(ytd_plan));
                        //growth = Convert.ToString(Math.Ceiling(Convert.ToDecimal(((Convert.ToInt32(Sales_ytd_value) / Convert.ToInt32(Sales_value_year_0))-1)*100)));
                        growth = Convert.ToInt32(Sales_value_year_0) == 0 ? Convert.ToString(0) : Convert.ToString((Convert.ToInt32(Sales_ytd_value) - Convert.ToInt32(Sales_value_year_0)) * 100 / Convert.ToInt32(Sales_value_year_0));
                        
                        //if (Convert.ToDecimal(Sales_value_year_0) != 0)
                        //    growth = Convert.ToString(Math.Round(((Convert.ToDecimal(Sales_ytd_value) / Convert.ToDecimal(Sales_value_year_0))-1) * 100, 0));


                    }
                    hdndata.Add(Convert.ToString(Sales_value_year_0));
                    hdndata.Add(Convert.ToString(Sales_ytd_value));
                    hdndata.Add(Convert.ToString(budget_value));
                    hdndata.Add(Convert.ToString(ytd_plan));
                    hdndata.Add(Convert.ToString(achvmnt));
                    hdndata.Add(Convert.ToString(askrate));
                    hdndata.Add(Convert.ToString(growth));


                }
            }
            return hdndata;
        }

        [WebMethod]
        public static List<string> setGoldCustTotal(string search)
        {
            DataTable dt = new DataTable();
            DataTable dtOutput = new DataTable();
            List<string> hdndata = new List<string>();
            List<string> searchList = new List<string>();
            DataRow[] filteredRows;
            dt = (DataTable)HttpContext.Current.Session["dtGoldCust"];
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(search))
                    {
                        if (search.Contains(' '))
                        {
                            searchList = search.Split(' ').ToList();
                        }
                        else
                        {
                            searchList.Add(search);
                        }
                        for (int i = 0; i < searchList.Count; i++)
                        {
                            dtOutput = new DataTable();
                            dtOutput.Columns.Add("customerNumber");
                            dtOutput.Columns.Add("Name");
                            dtOutput.Columns.Add("Branch");
                            dtOutput.Columns.Add("EngineerName");
                            dtOutput.Columns.Add("sales_value_year_0");
                            dtOutput.Columns.Add("budget");
                            dtOutput.Columns.Add("ytd");
                            dtOutput.Columns.Add("ytd_plan");
                            dtOutput.Columns.Add("ach");
                            dtOutput.Columns.Add("arate");
                            dtOutput.Columns.Add("growth");
                            search = Convert.ToString(searchList[i]);
                            filteredRows = dt.Select("CONVERT(Name, System.String) LIKE '%" + search
                                + "%' OR CONVERT(customerNumber, System.String) LIKE'%" + search
                                + "%' OR CONVERT(Branch, System.String) LIKE'%" + search
                                + "%' OR CONVERT(EngineerName, System.String) LIKE'%" + search
                              + "%' OR CONVERT(sales_value_year_0, System.String) LIKE'%" + search
                              + "%' OR CONVERT(budget, System.String) LIKE'%" + search
                              + "%' OR CONVERT(ytd, System.String) LIKE'%" + search
                              + "%' OR CONVERT(ytd_plan, System.String) LIKE'%" + search
                              + "%' OR CONVERT(ach, System.String) LIKE'%" + search
                              + "%' OR CONVERT(arate, System.String) LIKE'%" + search
                              + "%' OR CONVERT(growth, System.String) LIKE '%" + search
                              + "%'");

                            foreach (DataRow dr in filteredRows)
                            {
                                dtOutput.Rows.Add(dr.ItemArray);
                            }
                            dt = new DataTable();
                            dt = dtOutput;
                        }
                    }


                    string Sales_value_year_0 = "0";
                    string Sales_ytd_value = "0";
                    string budget_value = "0";
                    string ytd_plan = "0";
                    string achvmnt = "0";
                    string askrate = "0";
                    string growth = "0";

                    if (dt.Rows.Count != null)
                    {

                        Sales_value_year_0 = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["sales_value_year_0"])).ToString();
                        Sales_ytd_value = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ytd"])).ToString();
                        budget_value = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["budget"])).ToString();
                        ytd_plan = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ytd_plan"])).ToString();
                        //achvmnt = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ach"])).ToString();
                        askrate = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["arate"])).ToString();
                        //growth = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["growth"])).ToString();
                        achvmnt = Convert.ToInt32(ytd_plan) == 0 ? Convert.ToString(0) : Convert.ToString((Convert.ToInt32(Sales_ytd_value) * 100) / Convert.ToInt32(ytd_plan));
                        //growth = Convert.ToString(Math.Ceiling(Convert.ToDecimal(((Convert.ToInt32(Sales_ytd_value) / Convert.ToInt32(Sales_value_year_0))-1)*100)));
                        growth = Convert.ToInt32(Sales_value_year_0) == 0 ? Convert.ToString(0) : Convert.ToString((Convert.ToInt32(Sales_ytd_value) - Convert.ToInt32(Sales_value_year_0)) * 100 / Convert.ToInt32(Sales_value_year_0));
                        
                        //if (Convert.ToDecimal(Sales_value_year_0) != 0)
                        //    growth = Convert.ToString(Math.Round(((Convert.ToDecimal(Sales_ytd_value) / Convert.ToDecimal(Sales_value_year_0))-1) * 100, 0));


                    }
                    hdndata.Add(Convert.ToString(Sales_value_year_0));
                    hdndata.Add(Convert.ToString(Sales_ytd_value));
                    hdndata.Add(Convert.ToString(budget_value));
                    hdndata.Add(Convert.ToString(ytd_plan));
                    hdndata.Add(Convert.ToString(achvmnt));
                    hdndata.Add(Convert.ToString(askrate));
                    hdndata.Add(Convert.ToString(growth));


                }
            }
            return hdndata;
        }

        [WebMethod]
        public static List<string> setGoldCPTotal(string search)
        {
            DataTable dt = new DataTable();
            DataTable dtOutput = new DataTable();
            List<string> hdndata = new List<string>();
            List<string> searchList = new List<string>();
            DataRow[] filteredRows;
            dt = (DataTable)HttpContext.Current.Session["dtGoldCP"];
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(search))
                    {
                        if (search.Contains(' '))
                        {
                            searchList = search.Split(' ').ToList();
                        }
                        else
                        {
                            searchList.Add(search);
                        }
                        for (int i = 0; i < searchList.Count; i++)
                        {
                            dtOutput = new DataTable();
                            dtOutput.Columns.Add("customerNumber");
                            dtOutput.Columns.Add("Name");
                            dtOutput.Columns.Add("Branch");
                            dtOutput.Columns.Add("EngineerName");
                            dtOutput.Columns.Add("sales_value_year_0");
                            dtOutput.Columns.Add("budget");
                            dtOutput.Columns.Add("ytd");
                            dtOutput.Columns.Add("ytd_plan");
                            dtOutput.Columns.Add("ach");
                            dtOutput.Columns.Add("arate");
                            dtOutput.Columns.Add("growth");
                            search = Convert.ToString(searchList[i]);
                            filteredRows = dt.Select("CONVERT(Name, System.String) LIKE '%" + search
                                + "%' OR CONVERT(customerNumber, System.String) LIKE'%" + search
                                + "%' OR CONVERT(Branch, System.String) LIKE'%" + search
                                + "%' OR CONVERT(EngineerName, System.String) LIKE'%" + search
                              + "%' OR CONVERT(sales_value_year_0, System.String) LIKE'%" + search
                              + "%' OR CONVERT(budget, System.String) LIKE'%" + search
                              + "%' OR CONVERT(ytd, System.String) LIKE'%" + search
                              + "%' OR CONVERT(ytd_plan, System.String) LIKE'%" + search
                              + "%' OR CONVERT(ach, System.String) LIKE'%" + search
                              + "%' OR CONVERT(arate, System.String) LIKE'%" + search
                              + "%' OR CONVERT(growth, System.String) LIKE '%" + search
                              + "%'");

                            foreach (DataRow dr in filteredRows)
                            {
                                dtOutput.Rows.Add(dr.ItemArray);
                            }
                            dt = new DataTable();
                            dt = dtOutput;
                        }
                    }


                    string Sales_value_year_0 = "0";
                    string Sales_ytd_value = "0";
                    string budget_value = "0";
                    string ytd_plan = "0";
                    string achvmnt = "0";
                    string askrate = "0";
                    string growth = "0";

                    if (dt.Rows.Count != null)
                    {

                        Sales_value_year_0 = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["sales_value_year_0"])).ToString();
                        Sales_ytd_value = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ytd"])).ToString();
                        budget_value = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["budget"])).ToString();
                        ytd_plan = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ytd_plan"])).ToString();
                        //achvmnt = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ach"])).ToString();
                        askrate = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["arate"])).ToString();
                        //growth = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["growth"])).ToString();
                        achvmnt = Convert.ToInt32(ytd_plan) == 0 ? Convert.ToString(0) : Convert.ToString((Convert.ToInt32(Sales_ytd_value) * 100) / Convert.ToInt32(ytd_plan));
                        //growth = Convert.ToString(Math.Ceiling(Convert.ToDecimal(((Convert.ToInt32(Sales_ytd_value) / Convert.ToInt32(Sales_value_year_0))-1)*100)));
                        growth = Convert.ToInt32(Sales_value_year_0) == 0 ? Convert.ToString(0) : Convert.ToString((Convert.ToInt32(Sales_ytd_value) - Convert.ToInt32(Sales_value_year_0)) * 100 / Convert.ToInt32(Sales_value_year_0));
                        
                        //if (Convert.ToDecimal(Sales_value_year_0) != 0)
                        //    growth = Convert.ToString(Math.Round(((Convert.ToDecimal(Sales_ytd_value) / Convert.ToDecimal(Sales_value_year_0))-1) * 100, 0));


                    }
                    hdndata.Add(Convert.ToString(Sales_value_year_0));
                    hdndata.Add(Convert.ToString(Sales_ytd_value));
                    hdndata.Add(Convert.ToString(budget_value));
                    hdndata.Add(Convert.ToString(ytd_plan));
                    hdndata.Add(Convert.ToString(achvmnt));
                    hdndata.Add(Convert.ToString(askrate));
                    hdndata.Add(Convert.ToString(growth));


                }
            }
            return hdndata;
        }

        [WebMethod]
        public static List<string> setFamilyTotal(string search)
        {
            DataTable dt = new DataTable();
            DataTable dtOutput = new DataTable();
            List<string> hdndata = new List<string>();
            List<string> searchList = new List<string>();
            DataRow[] filteredRows;
            dt = (DataTable)HttpContext.Current.Session["dtFam"];
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(search))
                    {
                        if (search.Contains(' '))
                        {
                            searchList = search.Split(' ').ToList();
                        }
                        else
                        {
                            searchList.Add(search);
                        }
                        for (int i = 0; i < searchList.Count; i++)
                        {
                            dtOutput = new DataTable();
                            dtOutput.Columns.Add("Name");
                            dtOutput.Columns.Add("sales_value_year_0");
                            dtOutput.Columns.Add("budget");
                            dtOutput.Columns.Add("ytd");
                            dtOutput.Columns.Add("ytd_plan");
                            dtOutput.Columns.Add("ach");
                            dtOutput.Columns.Add("arate");
                            dtOutput.Columns.Add("growth");
                            search = Convert.ToString(searchList[i]);
                            filteredRows = dt.Select("CONVERT(Name, System.String) LIKE '%" + search
                              + "%' OR CONVERT(sales_value_year_0, System.String) LIKE'%" + search
                              + "%' OR CONVERT(budget, System.String) LIKE'%" + search
                              + "%' OR CONVERT(ytd, System.String) LIKE'%" + search
                              + "%' OR CONVERT(ytd_plan, System.String) LIKE'%" + search
                              + "%' OR CONVERT(ach, System.String) LIKE'%" + search
                              + "%' OR CONVERT(arate, System.String) LIKE'%" + search
                              + "%' OR CONVERT(growth, System.String) LIKE '%" + search
                              + "%'");

                            foreach (DataRow dr in filteredRows)
                            {
                                dtOutput.Rows.Add(dr.ItemArray);
                            }
                            dt = dtOutput;
                        }
                    }


                    string Sales_value_year_0 = "0";
                    string Sales_ytd_value = "0";
                    string budget_value = "0";
                    string ytd_plan = "0";
                    string achvmnt = "0";
                    string askrate = "0";
                    string growth = "0";

                    if (dt.Rows.Count != null)
                    {

                        Sales_value_year_0 = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["sales_value_year_0"])).ToString();
                        Sales_ytd_value = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ytd"])).ToString();
                        budget_value = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["budget"])).ToString();
                        ytd_plan = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ytd_plan"])).ToString();
                        //achvmnt = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["ach"])).ToString();
                        askrate = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["arate"])).ToString();
                        //growth = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["growth"])).ToString();
                        achvmnt = Convert.ToInt32(ytd_plan) == 0 ? Convert.ToString(0) : Convert.ToString((Convert.ToInt32(Sales_ytd_value) * 100) / Convert.ToInt32(ytd_plan));
                        //growth = Convert.ToString(Math.Ceiling(Convert.ToDecimal(((Convert.ToInt32(Sales_ytd_value) / Convert.ToInt32(Sales_value_year_0))-1)*100)));
                        growth = Convert.ToInt32(Sales_value_year_0) == 0 ? Convert.ToString(0) : Convert.ToString((Convert.ToInt32(Sales_ytd_value) - Convert.ToInt32(Sales_value_year_0)) * 100 / Convert.ToInt32(Sales_value_year_0));
                        
                        //if (Convert.ToDecimal(Sales_value_year_0) != 0)
                        //    growth = Convert.ToString(Math.Round(((Convert.ToDecimal(Sales_ytd_value) / Convert.ToDecimal(Sales_value_year_0))-1) * 100, 0));


                    }
                    hdndata.Add(Convert.ToString(Sales_value_year_0));
                    hdndata.Add(Convert.ToString(Sales_ytd_value));
                    hdndata.Add(Convert.ToString(budget_value));
                    hdndata.Add(Convert.ToString(ytd_plan));
                    hdndata.Add(Convert.ToString(achvmnt));
                    hdndata.Add(Convert.ToString(askrate));
                    hdndata.Add(Convert.ToString(growth));


                }
            }
            return hdndata;
        }

        [WebMethod]
        public static string LoadChartConsolidated()
        {
            CommonFunctions objCom = new CommonFunctions();
            DataTable dtoutput;
            DataTable dtfinal;
            string output = string.Empty;
            int tot = 0;
            int intval = 0;
            try
            {
                dtoutput = new DataTable();
                dtfinal = new DataTable();
                dtoutput = (DataTable)HttpContext.Current.Session["Consolidated"];
                dtfinal.Columns.Add("color");
                dtfinal.Columns.Add("startValue");
                dtfinal.Columns.Add("endValue");
                dtfinal.Columns.Add("radius");
                dtfinal.Columns.Add("innerRadius");
                dtfinal.Columns.Add("balloonText");
                DataRow dr;
                tot = Convert.ToInt32(dtoutput.AsEnumerable().Sum(x => Convert.ToDecimal(x["value"])));
                if (dtoutput != null)
                    if (dtoutput.Rows.Count > 0)
                    {
                        for (int i = 0; i < dtoutput.Rows.Count; i++)
                        {
                            intval = Math.Abs(Convert.ToInt32(dtoutput.Rows[i]["value"]) * 200 / tot);
                            for (int j = 0; j < 2; j++)
                            {
                                dr = dtfinal.NewRow();
                                dr["startValue"] = 0;
                                if (j == 1)
                                {
                                    dr["color"] = dtoutput.Rows[i]["color"];
                                    dr["endValue"] = intval;
                                }
                                else
                                {
                                    dr["color"] = "#eee";
                                    dr["endValue"] = 100;
                                }
                                if (i == 0)
                                {
                                    dr["radius"] = "100%";
                                    dr["innerRadius"] = "85%";
                                }
                                else if (i == 1)
                                {
                                    dr["radius"] = "80%";
                                    dr["innerRadius"] = "65%";
                                }
                                else if (i == 2)
                                {
                                    dr["radius"] = "60%";
                                    dr["innerRadius"] = "45%";
                                }
                                else
                                {
                                    dr["radius"] = "40%";
                                    dr["innerRadius"] = "25%";
                                }
                                if (j == 1)
                                {
                                    dr["balloonText"] = dtoutput.Rows[i]["value"];
                                }
                                dtfinal.Rows.Add(dr);
                            }

                        }
                        output = DataTableToJSONWithStringBuilder(dtfinal);
                    }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            return output;
        }

        [WebMethod]
        public static string LoadChartBranchAch()
        {
            CommonFunctions objCom = new CommonFunctions();
            DataTable dtoutput;
            string output = string.Empty;
            try
            {
                dtoutput = new DataTable();
                dtoutput = (DataTable)HttpContext.Current.Session["BranchAchChart"];
                //dtoutput.Columns.Add("color");
                if (dtoutput != null)
                    if (dtoutput.Rows.Count > 0)
                    {
                        //foreach (DataRow dr in dtoutput.Rows)
                        //{
                        //    dr["color"] = "#FF0F00";
                        //}
                        output = DataTableToJSONWithStringBuilder(dtoutput);
                    }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            return output;
        }

        [WebMethod]
        public static string LoadChartBranchGrowth()
        {
            CommonFunctions objCom = new CommonFunctions();
            DataTable dtoutput;
            string output = string.Empty;
            try
            {
                dtoutput = new DataTable();
                dtoutput = (DataTable)HttpContext.Current.Session["BranchGroChart"];
                //dtoutput.Columns.Add("color");

                if (dtoutput != null)
                    if (dtoutput.Rows.Count > 0)
                    {
                        //foreach (DataRow dr in dtoutput.Rows)
                        //{
                        //    dr["color"] = "#FF0F00";
                        //}
                        output = DataTableToJSONWithStringBuilder(dtoutput);
                    }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            return output;
        }

        [WebMethod]
        public static string LoadChartTopSEAch()
        {
            CommonFunctions objCom = new CommonFunctions();
            DataTable dtoutput;
            string output = string.Empty;
            try
            {
                dtoutput = new DataTable();
                dtoutput = (DataTable)HttpContext.Current.Session["TopSEAchChart"];
                DataRow[] foundRows = dtoutput.Select("Name='Total'");
                if (foundRows.Length > 0)
                {
                    foundRows[0].Delete();
                    dtoutput.AcceptChanges();
                }
                if (dtoutput != null)
                    if (dtoutput.Rows.Count > 0)
                    {
                        dtoutput.Columns.Add("ach1");
                        for (int i = 0; i < dtoutput.Rows.Count; i++)
                        {
                            dtoutput.Rows[i]["ach1"] = Math.Abs(Convert.ToInt32(dtoutput.Rows[i]["ach"]));
                            if (Math.Abs(Convert.ToInt32(dtoutput.Rows[i]["ach"])) == 0)
                            {
                                dtoutput.Rows[i]["ach1"] = 1;
                            }
                        }
                        output = DataTableToJSONWithStringBuilder(dtoutput);
                    }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            return output;
        }

        [WebMethod]
        public static string LoadChartTopSEGrowth()
        {
            CommonFunctions objCom = new CommonFunctions();
            DataTable dtoutput;
            string output = string.Empty;
            try
            {
                dtoutput = new DataTable();
                dtoutput = (DataTable)HttpContext.Current.Session["TopSEGroChart"];
                DataRow[] foundRows = dtoutput.Select("Name='Total'");
                if (foundRows.Length > 0)
                {
                    foundRows[0].Delete();
                    dtoutput.AcceptChanges();
                }
                if (dtoutput != null)
                    if (dtoutput.Rows.Count > 0)
                    {
                        //output = DataTableToJSONWithStringBuilder(dtoutput);
                        dtoutput.Columns.Add("growth1");
                        for (int i = 0; i < dtoutput.Rows.Count; i++)
                        {
                            dtoutput.Rows[i]["growth1"] = Math.Abs(Convert.ToInt32(dtoutput.Rows[i]["growth"]));
                            if (Math.Abs(Convert.ToInt32(dtoutput.Rows[i]["growth"])) == 0)
                            {
                                dtoutput.Rows[i]["growth1"] = 1;
                            }
                        }
                        output = DataTableToJSONWithStringBuilder(dtoutput);
                    }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            return output;
        }

        [WebMethod]
        public static string LoadChartBottomSEAch()
        {
            CommonFunctions objCom = new CommonFunctions();
            DataTable dtoutput;
            string output = string.Empty;
            try
            {
                dtoutput = new DataTable();
                dtoutput = (DataTable)HttpContext.Current.Session["BottomSEAchChart"];
                DataRow[] foundRows = dtoutput.Select("Name='Total'");
                if (foundRows.Length > 0)
                {
                    foundRows[0].Delete();
                    dtoutput.AcceptChanges();
                }
                if (dtoutput != null)
                    if (dtoutput.Rows.Count > 0)
                    {
                        //output = DataTableToJSONWithStringBuilder(dtoutput);
                        dtoutput.Columns.Add("ach1");
                        for (int i = 0; i < dtoutput.Rows.Count; i++)
                        {
                            dtoutput.Rows[i]["ach1"] = Math.Abs(Convert.ToInt32(dtoutput.Rows[i]["ach"]));
                            if (Math.Abs(Convert.ToInt32(dtoutput.Rows[i]["ach"])) == 0)
                            {
                                dtoutput.Rows[i]["ach1"] = 1;
                            }
                        }
                        output = DataTableToJSONWithStringBuilder(dtoutput);
                    }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            return output;
        }

        [WebMethod]
        public static string LoadChartBottomSEGrowth()
        {
            CommonFunctions objCom = new CommonFunctions();
            DataTable dtoutput;
            string output = string.Empty;
            try
            {
                dtoutput = new DataTable();
                dtoutput = (DataTable)HttpContext.Current.Session["BottomSEGroChart"];
                DataRow[] foundRows = dtoutput.Select("Name='Total'");
                if (foundRows.Length > 0)
                {
                    foundRows[0].Delete();
                    dtoutput.AcceptChanges();
                }
                if (dtoutput != null)
                    if (dtoutput.Rows.Count > 0)
                    {
                        dtoutput.Columns.Add("growth1");
                        for (int i = 0; i < dtoutput.Rows.Count; i++)
                        {
                            dtoutput.Rows[i]["growth1"] = Math.Abs(Convert.ToInt32(dtoutput.Rows[i]["growth"]));
                            if (Math.Abs(Convert.ToInt32(dtoutput.Rows[i]["growth"])) == 0)
                            {
                                dtoutput.Rows[i]["growth1"] = 1;
                            }
                        }
                        output = DataTableToJSONWithStringBuilder(dtoutput);
                    }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            return output;
        }

        [WebMethod]
        public static string LoadChartTopCustAch()
        {
            CommonFunctions objCom = new CommonFunctions();
            DataTable dtoutput;
            string output = string.Empty;
            try
            {
                dtoutput = new DataTable();
                dtoutput = (DataTable)HttpContext.Current.Session["TopCustAchChart"];
                DataRow[] dr = dtoutput.Select("Name=''");
                if (dr.Length > 0)
                {
                    dr[0].Delete();
                    dtoutput.AcceptChanges();
                }
                if (dtoutput != null)
                    if (dtoutput.Rows.Count > 0)
                    {
                        dtoutput.Columns.Add("ach1");
                        for (int i = 0; i < dtoutput.Rows.Count; i++)
                        {
                            dtoutput.Rows[i]["ach1"] = Math.Abs(Convert.ToInt32(dtoutput.Rows[i]["ach"]));
                            if (Math.Abs(Convert.ToInt32(dtoutput.Rows[i]["ach"])) == 0)
                            {
                                dtoutput.Rows[i]["ach1"] = 1;
                            }
                        }
                        output = DataTableToJSONWithStringBuilder(dtoutput);
                    }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            return output;
        }

        [WebMethod]
        public static string LoadChartTopCustGrowth()
        {
            CommonFunctions objCom = new CommonFunctions();
            DataTable dtoutput;
            string output = string.Empty;
            try
            {
                dtoutput = new DataTable();
                dtoutput = (DataTable)HttpContext.Current.Session["TopCustGroChart"];
                DataRow[] dr = dtoutput.Select("Name=''");
                if (dr.Length > 0)
                {
                    dr[0].Delete();
                    dtoutput.AcceptChanges();
                }
                if (dtoutput != null)
                    if (dtoutput.Rows.Count > 0)
                    {
                        dtoutput.Columns.Add("growth1");
                        for (int i = 0; i < dtoutput.Rows.Count; i++)
                        {
                            dtoutput.Rows[i]["growth1"] = Math.Abs(Convert.ToInt32(dtoutput.Rows[i]["growth"]));
                            if (Math.Abs(Convert.ToInt32(dtoutput.Rows[i]["growth"])) == 0)
                            {
                                dtoutput.Rows[i]["growth1"] = 1;
                            }
                        }
                        output = DataTableToJSONWithStringBuilder(dtoutput);
                    }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            return output;
        }

        [WebMethod]
        public static string LoadChartBottomCustAch()
        {
            CommonFunctions objCom = new CommonFunctions();
            DataTable dtoutput;
            string output = string.Empty;
            try
            {
                dtoutput = new DataTable();
                dtoutput = (DataTable)HttpContext.Current.Session["BottomCustAchChart"];
                DataRow[] dr = dtoutput.Select("Name=''");
                if (dr.Length > 0)
                {
                    dr[0].Delete();
                    dtoutput.AcceptChanges();
                }
                if (dtoutput != null)
                    if (dtoutput.Rows.Count > 0)
                    {
                        dtoutput.Columns.Add("ach1");
                        for (int i = 0; i < dtoutput.Rows.Count; i++)
                        {
                            dtoutput.Rows[i]["ach1"] = Math.Abs(Convert.ToInt32(dtoutput.Rows[i]["ach"]));
                            if (Math.Abs(Convert.ToInt32(dtoutput.Rows[i]["ach"])) == 0)
                            {
                                dtoutput.Rows[i]["ach1"] = 1;
                            }
                        }
                        output = DataTableToJSONWithStringBuilder(dtoutput);
                    }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            return output;
        }

        [WebMethod]
        public static string LoadChartBottomCustGrowth()
        {
            CommonFunctions objCom = new CommonFunctions();
            DataTable dtoutput;
            string output = string.Empty;
            try
            {
                dtoutput = new DataTable();
                dtoutput = (DataTable)HttpContext.Current.Session["BottomCustGroChart"];
                DataRow[] dr = dtoutput.Select("Name=''");
                if (dr.Length > 0)
                {
                    dr[0].Delete();
                    dtoutput.AcceptChanges();
                }
                if (dtoutput != null)
                    if (dtoutput.Rows.Count > 0)
                    {
                        dtoutput.Columns.Add("growth1");
                        for (int i = 0; i < dtoutput.Rows.Count; i++)
                        {
                            dtoutput.Rows[i]["growth1"] = Math.Abs(Convert.ToInt32(dtoutput.Rows[i]["growth"]));
                            if (Math.Abs(Convert.ToInt32(dtoutput.Rows[i]["growth"])) == 0)
                            {
                                dtoutput.Rows[i]["growth1"] = 1;
                            }
                        }
                        output = DataTableToJSONWithStringBuilder(dtoutput);
                    }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            return output;
        }

        [WebMethod]
        public static string LoadChartTopCPAch()
        {
            CommonFunctions objCom = new CommonFunctions();
            DataTable dtoutput;
            string output = string.Empty;
            try
            {
                dtoutput = new DataTable();
                dtoutput = (DataTable)HttpContext.Current.Session["TopCPAchChart"];
                DataRow[] dr = dtoutput.Select("Name=''");
                if (dr.Length > 0)
                {
                    dr[0].Delete();
                    dtoutput.AcceptChanges();
                }
                if (dtoutput != null)
                    if (dtoutput.Rows.Count > 0)
                    {
                        dtoutput.Columns.Add("ach1");
                        for (int i = 0; i < dtoutput.Rows.Count; i++)
                        {
                            dtoutput.Rows[i]["ach1"] = Math.Abs(Convert.ToInt32(dtoutput.Rows[i]["ach"]));
                            if (Math.Abs(Convert.ToInt32(dtoutput.Rows[i]["ach"])) == 0)
                            {
                                dtoutput.Rows[i]["ach1"] = 1;
                            }
                        }
                        output = DataTableToJSONWithStringBuilder(dtoutput);
                    }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            return output;
        }

        [WebMethod]
        public static string LoadChartTopCPGrowth()
        {
            CommonFunctions objCom = new CommonFunctions();
            DataTable dtoutput;
            string output = string.Empty;
            try
            {
                dtoutput = new DataTable();
                dtoutput = (DataTable)HttpContext.Current.Session["TopCPGroChart"];
                DataRow[] dr = dtoutput.Select("Name=''");
                if (dr.Length > 0)
                {
                    dr[0].Delete();
                    dtoutput.AcceptChanges();
                }
                if (dtoutput != null)
                    if (dtoutput.Rows.Count > 0)
                    {
                        dtoutput.Columns.Add("growth1");
                        for (int i = 0; i < dtoutput.Rows.Count; i++)
                        {
                            dtoutput.Rows[i]["growth1"] = Math.Abs(Convert.ToInt32(dtoutput.Rows[i]["growth"]));
                            if (Math.Abs(Convert.ToInt32(dtoutput.Rows[i]["growth"])) == 0)
                            {
                                dtoutput.Rows[i]["growth1"] = 1;
                            }
                        }
                        output = DataTableToJSONWithStringBuilder(dtoutput);
                    }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            return output;
        }

        [WebMethod]
        public static string LoadChartBottomCPAch()
        {
            CommonFunctions objCom = new CommonFunctions();
            DataTable dtoutput;
            string output = string.Empty;
            try
            {
                dtoutput = new DataTable();
                dtoutput = (DataTable)HttpContext.Current.Session["BottomCPAchChart"];
                DataRow[] dr = dtoutput.Select("Name=''");
                if (dr.Length > 0)
                {
                    dr[0].Delete();
                    dtoutput.AcceptChanges();
                }
                if (dtoutput != null)
                    if (dtoutput.Rows.Count > 0)
                    {
                        dtoutput.Columns.Add("ach1");
                        for (int i = 0; i < dtoutput.Rows.Count; i++)
                        {
                            dtoutput.Rows[i]["ach1"] = Math.Abs(Convert.ToInt32(dtoutput.Rows[i]["ach"]));
                            if (Math.Abs(Convert.ToInt32(dtoutput.Rows[i]["ach"])) == 0)
                            {
                                dtoutput.Rows[i]["ach1"] = 1;
                            }
                        }
                        output = DataTableToJSONWithStringBuilder(dtoutput);
                    }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            return output;
        }

        [WebMethod]
        public static string LoadChartBottomCPGrowth()
        {
            CommonFunctions objCom = new CommonFunctions();
            DataTable dtoutput;
            string output = string.Empty;
            try
            {
                dtoutput = new DataTable();
                dtoutput = (DataTable)HttpContext.Current.Session["BottomCPGroChart"];
                DataRow[] dr = dtoutput.Select("Name=''");
                if (dr.Length > 0)
                {
                    dr[0].Delete();
                    dtoutput.AcceptChanges();
                }
                if (dtoutput != null)
                    if (dtoutput.Rows.Count > 0)
                    {
                        dtoutput.Columns.Add("growth1");
                        for (int i = 0; i < dtoutput.Rows.Count; i++)
                        {
                            dtoutput.Rows[i]["growth1"] = Math.Abs(Convert.ToInt32(dtoutput.Rows[i]["growth"]));
                            if (Math.Abs(Convert.ToInt32(dtoutput.Rows[i]["growth"])) == 0)
                            {
                                dtoutput.Rows[i]["growth1"] = 1;
                            }
                        }
                        output = DataTableToJSONWithStringBuilder(dtoutput);
                    }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            return output;
        }

        [WebMethod]
        public static string LoadChartFamAch()
        {
            CommonFunctions objCom = new CommonFunctions();
            DataTable dtoutput;
            string output = string.Empty;
            Random random = new Random();
            try
            {
                dtoutput = new DataTable();
                dtoutput = (DataTable)HttpContext.Current.Session["FamAchChart"];
                //dtoutput.Columns.Add("color");
                foreach (DataRow dr in dtoutput.Rows)
                {
                    //Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //dr["color"] = "#FF0F00";
                    if (Convert.ToString(dr["Name"]) == "Total")
                    {
                        dr.Delete();
                        break;
                    }
                }
                if (dtoutput != null)
                    if (dtoutput.Rows.Count > 0)
                    {
                        output = DataTableToJSONWithStringBuilder(dtoutput);
                    }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            return output;
        }

        [WebMethod]
        public static string LoadChartFamGrowth()
        {
            CommonFunctions objCom = new CommonFunctions();
            DataTable dtoutput;
            string output = string.Empty;
            Random random = new Random();
            try
            {
                dtoutput = new DataTable();
                dtoutput = (DataTable)HttpContext.Current.Session["FamGroChart"];
                //dtoutput.Columns.Add("color");
                foreach (DataRow dr in dtoutput.Rows)
                {
                    //dr["color"] = "#FF0F00";
                    if (Convert.ToString(dr["Name"]) == "Total")
                    {
                        dr.Delete();
                        break;
                    }
                }

                if (dtoutput != null)
                    if (dtoutput.Rows.Count > 0)
                    {
                        output = DataTableToJSONWithStringBuilder(dtoutput);
                    }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            return output;
        }

        private DataTable GenerateTransposedTable(DataTable inputTable)
        {
            DataTable outputTable = new DataTable();


            DataColumn Col = inputTable.Columns.Add("category");
            Col.SetOrdinal(0);
            // Add columns by looping rows
            inputTable.Rows[0]["category"] = "value";

            // Header row's first column is same as in inputTable
            outputTable.Columns.Add(inputTable.Columns["category"].ColumnName.ToString());

            // Header row's second column onwards, 'inputTable's first column taken
            foreach (DataRow inRow in inputTable.Rows)
            {
                string newColName = (inRow[0].ToString()).Replace(",", "");
                outputTable.Columns.Add(newColName);
            }
            outputTable.Columns.Add("color");
            // Add rows by looping columns        
            for (int rCount = 1; rCount <= inputTable.Columns.Count - 1; rCount++)
            {
                DataRow newRow = outputTable.NewRow();

                // First column is inputTable's Header row's second column
                newRow[0] = inputTable.Columns[rCount].ColumnName.ToString().Replace(",", "");
                for (int cCount = 0; cCount <= inputTable.Rows.Count - 1; cCount++)
                {
                    string colValue = inputTable.Rows[cCount][rCount].ToString().Replace(",", "");
                    if (colValue == "Sales_value_year_0")
                        colValue = "SALE " + (DateTime.Now.Year - 1);
                    else if (colValue == "budget_value")
                        colValue = "BUDGET " + DateTime.Now.Year;
                    else if (colValue == "ytd_plan")
                        colValue = "YTD PLAN " + DateTime.Now.Year;
                    else if (colValue == "Sales_ytd_value")
                        colValue = "YTD SALE " + DateTime.Now.Year;
                    newRow[cCount + 1] = colValue;
                }
                outputTable.Rows.Add(newRow);
            }
            for (int cCount = 0; cCount <= outputTable.Rows.Count - 1; cCount++)
            {
                string color = string.Empty;
                string Value = Convert.ToString(outputTable.Rows[cCount]["category"]);
                if (Value == "Sales_value_year_0")
                {
                    Value = "SALE " + (DateTime.Now.Year - 1);
                    color = "#097054";
                }
                else if (Value == "budget_value")
                {
                    Value = "BUDGET " + DateTime.Now.Year;
                    color = "#FFDE00";
                }
                else if (Value == "ytd_plan")
                {
                    Value = "YTD PLAN " + DateTime.Now.Year;
                    color = "#6599FF";
                }
                else if (Value == "Sales_ytd_value")
                {
                    Value = "YTD SALE " + DateTime.Now.Year;
                    color = "#FF9900";
                }
                outputTable.Rows[cCount]["category"] = Value;
                outputTable.Rows[cCount]["color"] = color;
            }
            return outputTable;
        }
        public static string DataTableToJSONWithStringBuilder(DataTable table)
        {
            var JSONString = new StringBuilder();
            if (table.Rows.Count > 0)
            {
                JSONString.Append("[");
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    JSONString.Append("{");
                    for (int j = 0; j < table.Columns.Count; j++)
                    {
                        if (j < table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\",");
                        }
                        else if (j == table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\"");
                        }
                    }
                    if (i == table.Rows.Count - 1)
                    {
                        JSONString.Append("}");
                    }
                    else
                    {
                        JSONString.Append("},");
                    }
                }
                JSONString.Append("]");
            }
            return JSONString.ToString();
        }
        #endregion

        //protected DataTable sumOfRows(DataTable dt)
        //{
        //    actual_mnth = (objConfig.getActualMonth());
        //    DataTable griddt = new DataTable();
        //    if (dt.Rows.Count != 0)
        //    {



        //        griddt.Columns.Add("sales_value_year_0", typeof(string));
        //        griddt.Columns.Add("budget_value", typeof(string));
        //        griddt.Columns.Add("Sales_ytd_value", typeof(string));
        //        griddt.Columns.Add("ytd_plan", typeof(string));

        //        decimal sales_value_year_0 = 0, budget = sales_value_year_0, ytd = 0, ytd_plan = 0;
        //        for (int i = 0; i < dt.Rows.Count; i++)
        //        {


        //            sales_value_year_0 += dt.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dt.Rows[i].ItemArray[1].ToString());
        //            budget += dt.Rows[i].ItemArray[2].ToString() == "" || dt.Rows[i].ItemArray[2].ToString() == null ? 0
        //                 : Convert.ToDecimal(dt.Rows[i].ItemArray[2].ToString());
        //            ytd += dt.Rows[i].ItemArray[3].ToString() == "" || dt.Rows[i].ItemArray[3].ToString() == null ? 0
        //                : Convert.ToDecimal(dt.Rows[i].ItemArray[3].ToString());
        //            ytd_plan += dt.Rows[i].ItemArray[4].ToString() == "" || dt.Rows[i].ItemArray[4].ToString() == null ? 0
        //              : Convert.ToDecimal(dt.Rows[i].ItemArray[4].ToString());

        //        }
        //        sales_value_year_0 = sales_value_year_0 / byValueIn;
        //        budget = budget / byValueIn;
        //        ytd = ytd / byValueIn;
        //        ytd_plan = ytd_plan / byValueIn;
        //        griddt.Rows.Add(Math.Round(sales_value_year_0, 0).ToString(), Math.Round(budget, 0).ToString(), Math.Round(ytd, 0).ToString(),
        //            Math.Round(ytd_plan, 0).ToString());

        //    }
        //    return griddt;
        //}


        #region Bind background color for total
        /// <summary>
        /// 
        /// </summary>
        protected void bindgridColor_Branch()
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (gridtopbranches.Rows.Count != 0)
                {
                    foreach (GridViewRow row in gridtopbranches.Rows)
                    {
                        var check = row.FindControl("lbl_branch") as Label;
                        if (check.Text.ToString() == "Total")
                        {
                            row.CssClass = "grdview_total";

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void bindgridColor_SE()
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (GridTopSE.Rows.Count != 0)
                {
                    foreach (GridViewRow row in GridTopSE.Rows)
                    {
                        var check = row.FindControl("lbl_Eng") as Label;
                        if (check.Text.ToString() == "Total")
                        {
                            row.CssClass = "grdview_total";

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }


        protected void bindgridColor_Customer()
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (GridCustByAch.Rows.Count != 0)
                {
                    foreach (GridViewRow row in GridCustByAch.Rows)
                    {
                        var check = row.FindControl("lbl_custnum") as Label;
                        if (check.Text.ToString() == "Total")
                        {
                            row.CssClass = "grdview_total";

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }


        protected void bindgridColor_Channelpatner()
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (GridTopCP.Rows.Count != 0)
                {
                    foreach (GridViewRow row in GridTopCP.Rows)
                    {
                        var check = row.FindControl("lbl_custnum") as Label;
                        if (check.Text.ToString() == "Total")
                        {
                            row.CssClass = "grdview_total";

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }


        protected void bindgridColor_GoldBranches()
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (GridTopGoldBranches.Rows.Count != 0)
                {
                    foreach (GridViewRow row in GridTopGoldBranches.Rows)
                    {
                        var check = row.FindControl("lbl_branch") as Label;
                        if (check.Text.ToString() == "Total")
                        {
                            row.CssClass = "grdview_total";

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void bindgridColor_GoldSE()
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (GridTopGoldSE.Rows.Count != 0)
                {
                    foreach (GridViewRow row in GridTopGoldSE.Rows)
                    {
                        var check = row.FindControl("lbl_eng") as Label;
                        if (check.Text.ToString() == "Total")
                        {
                            row.CssClass = "grdview_total";

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void bindgridColor_GoldCustomers()
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (GridTopGoldCstmrs.Rows.Count != 0)
                {
                    foreach (GridViewRow row in GridTopGoldCstmrs.Rows)
                    {
                        var check = row.FindControl("lbl_custnum") as Label;
                        if (check.Text.ToString() == "Total")
                        {
                            row.CssClass = "grdview_total";

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void bindgridColor_GoldCP()
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (GridTopGoldCP.Rows.Count != 0)
                {
                    foreach (GridViewRow row in GridTopGoldCP.Rows)
                    {
                        var check = row.FindControl("lbl_custnumber") as Label;
                        if (check.Text.ToString() == "Total")
                        {
                            row.CssClass = "grdview_total";

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }


        protected void bindgridColor_Family()
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (GridFamily.Rows.Count != 0)
                {
                    foreach (GridViewRow row in GridFamily.Rows)
                    {
                        var check = row.FindControl("lbl_familyname") as Label;
                        if (check.Text.ToString() == "Total")
                        {
                            row.CssClass = "grdview_total";

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }
        #endregion

        #region sorting ANANTHA
        private string GetSortDirection(SortDirection sortDirection)
        {
            string newSortDirection = String.Empty;
            try
            {

                switch (sortDirection)
                {
                    case SortDirection.Ascending:
                        newSortDirection = "ASC";
                        break;
                    case SortDirection.Descending:
                        newSortDirection = "DESC";
                        break;
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            return newSortDirection;
        }

        protected void ddlPerfBy_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Convert.ToString(ddlPerfBy.SelectedValue) == "BRANCH")
            {
                branch_perf(sender, e);
            }
            else if (Convert.ToString(ddlPerfBy.SelectedValue) == "SE")
            {
                se_perf(sender, e);
            }
            else if (Convert.ToString(ddlPerfBy.SelectedValue) == "CUSTOMERS")
            {
                customers_perf(sender, e);
            }
            else if (Convert.ToString(ddlPerfBy.SelectedValue) == "CP")
            {
                cp_perf(sender, e);
            }
            else if (Convert.ToString(ddlPerfBy.SelectedValue) == "GOLD")
            {
                goldfamily_perf(sender, e);
            }
            else if (Convert.ToString(ddlPerfBy.SelectedValue) == "FAMILY")
            {
                family_perf(sender, e);
            }
        }

        //protected void gridtopbranches_Sorting(object sender, GridViewSortEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        //Retrieve the table from the session object.

        //        DataTable dt = dtTopBranches.Copy();
        //        dt = gridtable_all(dt);
        //        //DataTable dt1 = dtTopBranches_Growth.Copy();
        //        //dt1 = gridtable_all(dt1);
        //        EnumerableRowCollection<DataRow> dr1 = null;

        //        if (dt != null)
        //        {
        //            if (GetSortDirection(e.SortDirection) == "ASC")
        //            {
        //                dr1 = (from row in dt.AsEnumerable()

        //                       orderby row[e.SortExpression] ascending

        //                       select row);
        //            }
        //            else
        //            {
        //                dr1 = (from row in dt.AsEnumerable()

        //                       orderby row[e.SortExpression] descending

        //                       select row);
        //            }
        //            DataTable dx = dr1.AsDataView().ToTable();
        //            gridtopbranches.DataSource = dx;
        //            gridtopbranches.DataBind();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //}
        //protected void sort_branch_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        if (potSort == false)
        //        {
        //            gridtopbranches_Sorting(null, new GridViewSortEventArgs("Name", SortDirection.Ascending));
        //            potSort = true;
        //        }
        //        else
        //        {
        //            gridtopbranches_Sorting(null, new GridViewSortEventArgs("Name", SortDirection.Descending));
        //            potSort = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        //}


        //protected void GridTopSE_Sorting(object sender, GridViewSortEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        //Retrieve the table from the session object.

        //        DataTable SortTopSE = dtTopSalesEngineers.Tables[0].Copy();
        //        EnumerableRowCollection<DataRow> dr1 = null;

        //        if (SortTopSE != null)
        //        {
        //            if (GetSortDirection(e.SortDirection) == "ASC")
        //            {
        //                dr1 = (from row in SortTopSE.AsEnumerable()

        //                       orderby row[e.SortExpression] ascending

        //                       select row);
        //            }
        //            else
        //            {
        //                dr1 = (from row in SortTopSE.AsEnumerable()

        //                       orderby row[e.SortExpression] descending

        //                       select row);
        //            }
        //            DataTable dx = dr1.AsDataView().ToTable();
        //            GridTopSE.DataSource = dx;
        //            GridTopSE.DataBind();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //}
        //protected void sort_engineername_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        if (potSort == false)
        //        {
        //            GridTopSE_Sorting(null, new GridViewSortEventArgs("EngineerName", SortDirection.Ascending));
        //            potSort = true;
        //        }
        //        else
        //        {
        //            GridTopSE_Sorting(null, new GridViewSortEventArgs("EngineerName", SortDirection.Descending));
        //            potSort = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        //}


        //protected void GridCustByAch_Sorting(object sender, GridViewSortEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        //Retrieve the table from the session object.

        //        DataTable SortCustomers = dtTopCustomers.Tables[0].Copy();
        //        EnumerableRowCollection<DataRow> dr1 = null;

        //        if (SortCustomers != null)
        //        {
        //            if (GetSortDirection(e.SortDirection) == "ASC")
        //            {
        //                dr1 = (from row in SortCustomers.AsEnumerable()

        //                       orderby row[e.SortExpression] ascending

        //                       select row);
        //            }
        //            else
        //            {
        //                dr1 = (from row in SortCustomers.AsEnumerable()

        //                       orderby row[e.SortExpression] descending

        //                       select row);
        //            }
        //            DataTable dx = dr1.AsDataView().ToTable();
        //            GridCustByAch.DataSource = dx;
        //            GridCustByAch.DataBind();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //}
        //protected void sort_custnum_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        if (potSort == false)
        //        {
        //            GridCustByAch_Sorting(null, new GridViewSortEventArgs("customer_number", SortDirection.Ascending));
        //            potSort = true;
        //        }
        //        else
        //        {
        //            GridCustByAch_Sorting(null, new GridViewSortEventArgs("customer_number", SortDirection.Descending));
        //            potSort = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        //}


        //protected void GridTopCP_Sorting(object sender, GridViewSortEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        //Retrieve the table from the session object.

        //        DataTable SortCustomers = dtTopChannelPartners.Tables[0].Copy();
        //        EnumerableRowCollection<DataRow> dr1 = null;

        //        if (SortCustomers != null)
        //        {
        //            if (GetSortDirection(e.SortDirection) == "ASC")
        //            {
        //                dr1 = (from row in SortCustomers.AsEnumerable()

        //                       orderby row[e.SortExpression] ascending

        //                       select row);
        //            }
        //            else
        //            {
        //                dr1 = (from row in SortCustomers.AsEnumerable()

        //                       orderby row[e.SortExpression] descending

        //                       select row);
        //            }
        //            DataTable dx = dr1.AsDataView().ToTable();
        //            GridTopCP.DataSource = dx;
        //            GridTopCP.DataBind();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //}
        //protected void sort_CPcustnum_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        if (potSort == false)
        //        {
        //            GridTopCP_Sorting(null, new GridViewSortEventArgs("customer_number", SortDirection.Ascending));
        //            potSort = true;
        //        }
        //        else
        //        {
        //            GridTopCP_Sorting(null, new GridViewSortEventArgs("customer_number", SortDirection.Descending));
        //            potSort = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        //}

        //protected void GridTopGoldBranches_Sorting(object sender, GridViewSortEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        //Retrieve the table from the session object.

        //        DataTable dt = dtGoldBranches.Copy();
        //        dt = gridtable_all(dt);
        //        EnumerableRowCollection<DataRow> dr1 = null;

        //        if (dt != null)
        //        {
        //            if (GetSortDirection(e.SortDirection) == "ASC")
        //            {
        //                dr1 = (from row in dt.AsEnumerable()

        //                       orderby row[e.SortExpression] ascending

        //                       select row);
        //            }
        //            else
        //            {
        //                dr1 = (from row in dt.AsEnumerable()

        //                       orderby row[e.SortExpression] descending

        //                       select row);
        //            }
        //            DataTable dx = dr1.AsDataView().ToTable();
        //            GridTopGoldBranches.DataSource = dx;
        //            GridTopGoldBranches.DataBind();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //}
        //protected void sort_Goldbranch_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        if (potSort == false)
        //        {
        //            GridTopGoldBranches_Sorting(null, new GridViewSortEventArgs("Name", SortDirection.Ascending));
        //            potSort = true;
        //        }
        //        else
        //        {
        //            GridTopGoldBranches_Sorting(null, new GridViewSortEventArgs("Name", SortDirection.Descending));
        //            potSort = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        //}


        //protected void GridTopGoldSE_Sorting(object sender, GridViewSortEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        //Retrieve the table from the session object.

        //        DataTable dt = dtTopGoldSe.Copy();

        //        EnumerableRowCollection<DataRow> dr1 = null;

        //        if (dt != null)
        //        {
        //            if (GetSortDirection(e.SortDirection) == "ASC")
        //            {
        //                dr1 = (from row in dt.AsEnumerable()

        //                       orderby row[e.SortExpression] ascending

        //                       select row);
        //            }
        //            else
        //            {
        //                dr1 = (from row in dt.AsEnumerable()

        //                       orderby row[e.SortExpression] descending

        //                       select row);
        //            }
        //            DataTable dx = dr1.AsDataView().ToTable();
        //            GridTopGoldSE.DataSource = dx;
        //            GridTopGoldSE.DataBind();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //}
        //protected void sort_GoldSE_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        if (potSort == false)
        //        {
        //            GridTopGoldSE_Sorting(null, new GridViewSortEventArgs("EngineerName", SortDirection.Ascending));
        //            potSort = true;
        //        }
        //        else
        //        {
        //            GridTopGoldSE_Sorting(null, new GridViewSortEventArgs("EngineerName", SortDirection.Descending));
        //            potSort = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        //}

        //protected void GridTopGoldCstmrs_Sorting(object sender, GridViewSortEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        //Retrieve the table from the session object.

        //        DataTable dt = dtTopGoldCustomers.Copy();

        //        EnumerableRowCollection<DataRow> dr1 = null;

        //        if (dt != null)
        //        {
        //            if (GetSortDirection(e.SortDirection) == "ASC")
        //            {
        //                dr1 = (from row in dt.AsEnumerable()

        //                       orderby row[e.SortExpression] ascending

        //                       select row);
        //            }
        //            else
        //            {
        //                dr1 = (from row in dt.AsEnumerable()

        //                       orderby row[e.SortExpression] descending

        //                       select row);
        //            }
        //            DataTable dx = dr1.AsDataView().ToTable();
        //            GridTopGoldCstmrs.DataSource = dx;
        //            GridTopGoldCstmrs.DataBind();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //}
        //protected void sort_CustNum_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        if (potSort == false)
        //        {
        //            GridTopGoldCstmrs_Sorting(null, new GridViewSortEventArgs("customer_number", SortDirection.Ascending));
        //            potSort = true;
        //        }
        //        else
        //        {
        //            GridTopGoldCstmrs_Sorting(null, new GridViewSortEventArgs("customer_number", SortDirection.Descending));
        //            potSort = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        //}
        //protected void sort_CustName_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        if (potSort == false)
        //        {
        //            GridTopGoldCstmrs_Sorting(null, new GridViewSortEventArgs("customer_short_name", SortDirection.Ascending));
        //            potSort = true;
        //        }
        //        else
        //        {
        //            GridTopGoldCstmrs_Sorting(null, new GridViewSortEventArgs("customer_short_name", SortDirection.Descending));
        //            potSort = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        //}

        //protected void GridTopGoldCP_Sorting(object sender, GridViewSortEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        //Retrieve the table from the session object.

        //        DataTable dt = dtTopGoldChannelPartners.Copy();

        //        EnumerableRowCollection<DataRow> dr1 = null;

        //        if (dt != null)
        //        {
        //            if (GetSortDirection(e.SortDirection) == "ASC")
        //            {
        //                dr1 = (from row in dt.AsEnumerable()

        //                       orderby row[e.SortExpression] ascending

        //                       select row);
        //            }
        //            else
        //            {
        //                dr1 = (from row in dt.AsEnumerable()

        //                       orderby row[e.SortExpression] descending

        //                       select row);
        //            }
        //            DataTable dx = dr1.AsDataView().ToTable();
        //            GridTopGoldCP.DataSource = dx;
        //            GridTopGoldCP.DataBind();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //}
        //protected void sort_CPCustNum_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        if (potSort == false)
        //        {
        //            GridTopGoldCP_Sorting(null, new GridViewSortEventArgs("customer_number", SortDirection.Ascending));
        //            potSort = true;
        //        }
        //        else
        //        {
        //            GridTopGoldCP_Sorting(null, new GridViewSortEventArgs("customer_number", SortDirection.Descending));
        //            potSort = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        //}
        //protected void sort_CPCustName_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        if (potSort == false)
        //        {
        //            GridTopGoldCP_Sorting(null, new GridViewSortEventArgs("customer_short_name", SortDirection.Ascending));
        //            potSort = true;
        //        }
        //        else
        //        {
        //            GridTopGoldCP_Sorting(null, new GridViewSortEventArgs("customer_short_name", SortDirection.Descending));
        //            potSort = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        //}


        //protected void GridFamily_Sorting(object sender, GridViewSortEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        //Retrieve the table from the session object.

        //        DataTable dt = dtFamily.Copy();

        //        EnumerableRowCollection<DataRow> dr1 = null;

        //        if (dt != null)
        //        {
        //            if (GetSortDirection(e.SortDirection) == "ASC")
        //            {
        //                dr1 = (from row in dt.AsEnumerable()

        //                       orderby row[e.SortExpression] ascending

        //                       select row);
        //            }
        //            else
        //            {
        //                dr1 = (from row in dt.AsEnumerable()

        //                       orderby row[e.SortExpression] descending

        //                       select row);
        //            }
        //            DataTable dx = dr1.AsDataView().ToTable();
        //            GridFamily.DataSource = dx;
        //            GridFamily.DataBind();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //}
        //protected void sort_familyname_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        if (potSort == false)
        //        {
        //            GridFamily_Sorting(null, new GridViewSortEventArgs("Name", SortDirection.Ascending));
        //            potSort = true;
        //        }
        //        else
        //        {
        //            GridFamily_Sorting(null, new GridViewSortEventArgs("Name", SortDirection.Descending));
        //            potSort = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        //}


        #endregion

    }

}

