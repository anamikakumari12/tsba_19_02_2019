﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TaegutecSalesBudget
{
    public partial class AdminProfile : System.Web.UI.Page
    {
        #region GlobalDeclareation
        public string Empnumber, Empname, Empmail, Empusertype, EmpPassword, EmpBranch, EditEmpstatus, editempcontact;
        public int Empstatus;
        public long Empcontact;
        EngineerInfo objEngInfo = new EngineerInfo();
        AdminConfiguration objAdmin = new AdminConfiguration();
        LoginAuthentication objauth = new LoginAuthentication();
        Reports objReports = new Reports();
        List<string> cssList = new List<string>();
        Review objRSum = new Review();
        #endregion

        #region Events
        public static string cter;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            if (!IsPostBack)
            {
                if (Session["cter"] == null)
                {
                    Session["cter"] = "TTA";
                    cter = "TTA";
                    rdBtnTaegutec.Checked = true;
                    rdBtnDuraCab.Checked = false;

                }
                else if (Session["cter"].ToString() == "DUR")
                {
                    cter = "DUR";
                    rdBtnDuraCab.Checked = true;
                    rdBtnTaegutec.Checked = false;
                }
                else
                {
                    cter = "TTA";
                }
                LoadBranches();
                LoadBranchesforBranchmanager();
                LoadBranchesforTerritary();
                LoadEngineerInfo();
                bindgridColor();
            }
            if (ViewState["GridData"] != null)
            {
                DataTable dtData = ViewState["GridData"] as DataTable;
                if (dtData.Rows.Count != 0)
                {
                    GridEngInfo.DataSource = dtData;
                    GridEngInfo.DataBind();
                }
            }
            engineerIdScript();
     //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "submit();", true);
        }
        protected void GridEngInfo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            string data = "";
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string c = (e.Row.FindControl("lbl_branch") as Label).Text;
                string[] lines = Regex.Split(c, ",");
                foreach (var item in lines)
                {
                    PlaceHolder emails = e.Row.FindControl("ph_Region") as PlaceHolder;

                    data = item.ToString();
                    Label tags = new Label();
                    tags.EnableViewState = true;
                    tags.Text = data;
                    emails.Controls.Add(tags);
                    if (lines.Length != 1)
                    {
                        emails.Controls.Add(new LiteralControl("<br />"));
                    }
                }
            }
        }
        /// <summary>
        /// Mod By :Neha
        /// Mod Date:Dec 7th,2018
        /// Mod Des: deleted gridviewScrollTrigger function 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rdBtnTaegutec_CheckedChanged(object sender, EventArgs e)
        {
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "gridviewScrollTrigger();$('#alertmsg').show().delay(5000).fadeOut();", true);

            if (rdBtnTaegutec.Checked)
            {
                cter = "TTA";
                rdBtnTaegutec.Checked = true;
                rdBtnDuraCab.Checked = false;
            }
            if (rdBtnDuraCab.Checked)
            {
                cter = "DUR";
                rdBtnDuraCab.Checked = true;
                rdBtnTaegutec.Checked = false;
            }
            LoadBranches();
            LoadBranchesforBranchmanager();
            GridEngInfo.DataSource = null;
            GridEngInfo.DataBind();
            Ok_Click(null, null);
            txtEmpcode.Text = "";
            txtEmpName.Text = "";
            txtEmpContact.Text = "";
            txtEmail.Text = "";

           // ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopups", "submit();", true);
            engineerIdScript();


        }
        /// <summary>
        /// Mod By :Neha
        /// Mod Date:Dec 7th,2018
        /// Mod Des: deleted gridviewScrollTrigger function 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Delete_Click1(object sender, ImageClickEventArgs e)
        {
            int flag = 0;
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            {
                ImageButton ibtn1 = sender as ImageButton;
                int rowIndex = Convert.ToInt32(ibtn1.Attributes["RowIndex"]);
                GridViewRow row = GridEngInfo.Rows[rowIndex];
                var id = row.FindControl("lbl_engid") as System.Web.UI.WebControls.Label;
                string EngId = id.Text;
                var role = row.FindControl("lbl_role") as System.Web.UI.WebControls.Label;
                string rolename = role.Text;
                if (rolename == "TERRITORY MANAGER")
                {
                    flag = 1;
                }
                objEngInfo.Deletebyadmin(EngId, flag);
                Ok_Click(null, null);

                //LoadEngineerInfo();
               // LoadBranches();
                LoadBranchesforBranchmanager();
                LoadBranchesforTerritary();
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "submit();", true);
          
        }

        /// <summary>
        /// Mod By :Neha
        /// Mod Date:Dec 7th,2018
        /// Mod Des: deleted gridviewScrollTrigger function 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Deletbranch_Click(object sender, ImageClickEventArgs e)
        {
            int flag = 0;
            string ConfirmBranch = Request.Form["confirm_valueBranch"];
            if (ConfirmBranch == "Yes")
            {

                ImageButton ibtn1 = sender as ImageButton;
                int rowIndex = Convert.ToInt32(ibtn1.Attributes["RowIndex"]);
                GridViewRow row = GridEngInfo.Rows[rowIndex];
                var id = row.FindControl("lbl_engid") as System.Web.UI.WebControls.Label;
                string EngId = id.Text;
                var barnchcode = row.FindControl("lbl_branch") as System.Web.UI.WebControls.Label;
                string Branchcode = barnchcode.Text;
                var role = row.FindControl("lbl_role") as System.Web.UI.WebControls.Label;
                string rolename = role.Text;
                if (rolename == "TERRITORY MANAGER")
                {
                    flag = 1;
                }
                objEngInfo.DeletebyadminBranch(EngId, Branchcode, flag);
                Ok_Click(null, null);

                //LoadEngineerInfo();
               // LoadBranches();
                LoadBranchesforBranchmanager();
                LoadBranchesforTerritary();
            }
          
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "submit();", true);
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {

            Empnumber = txtEmpcode.Text;
            Empname = txtEmpName.Text;
            Empmail = txtEmail.Text;
            string pwd = PasswordSecurityGenerate.Generate(8, 8);
            EmpPassword = objauth.Encrypt(pwd);
            Empcontact = string.IsNullOrEmpty(txtEmpContact.Text) ? 0 : Convert.ToInt64(txtEmpContact.Text);
            Empstatus = Convert.ToInt32(ddlstatus.SelectedValue);
            Empusertype = ddlUserType.SelectedValue;
            if (Empusertype == "BM")
            {
                EmpBranch = ddlBranch.SelectedValue;
                objEngInfo.Createnewuser(Empnumber, Empname, EmpBranch, Empmail, Empcontact, Empstatus, Empusertype, EmpPassword);
            }
            else if (Empusertype == "TM")
            {
                EmpBranch = null;
                objEngInfo.Createnewuser(Empnumber, Empname, EmpBranch, Empmail, Empcontact, Empstatus, Empusertype, EmpPassword);
                DataTable dtbranches = new DataTable();
                dtbranches.Columns.Add("regioncode", typeof(string));
                foreach (ListItem item in ddlBranchterritary.Items)
                {
                    if (item != null)
                    {
                        if (item.Selected)
                        {
                            dtbranches.Rows.Add(item.Value.ToString());

                        }
                    }
                }
                objEngInfo.InsertTag_for_territary(Empnumber, dtbranches);
                objEngInfo.Update_TE_for_Customer(Empnumber);
            }
            else
            {
                EmpBranch = null;
                objEngInfo.Createnewuser(Empnumber, Empname, EmpBranch, Empmail, Empcontact, Empstatus, Empusertype, EmpPassword);
            }
            try
            {
                MailMessage email = new MailMessage();
                string pwd1 = pwd;
                email.To.Add(new MailAddress(Empmail)); //Destination Recipient e-mail address.
                email.Subject = " Welcome  To Sales-Budget & Performance Monitoring";//Subject for your request
                email.Body = " <br/><br/>Your Username: " + Empmail + "<br/><br/>Your Password: " + pwd + "<br/><br/>" + "From" + "<br/>" + "IT Team - TaeguTec";
                email.IsBodyHtml = true;
                SmtpClient smtpc = new SmtpClient();
                smtpc.Send(email);
            }
            catch (Exception Ex)
            {

                ScriptManager.RegisterStartupScript(this, GetType(), "mailfailed", "alert('Failed to send email');", true);
            }
            Ok_Click(null, null);

          //  LoadBranches();
            LoadBranchesforBranchmanager();
            LoadBranchesforTerritary();
            string alert = "alert('User: " + Empname + " Created Successfully');";
            txtEmpcode.Text = "";
            txtEmpName.Text = "";
            txtEmpContact.Text = "";
            txtEmail.Text = "";
            ddlstatus.SelectedValue = "1";
            ddlUserType.SelectedValue = "RO";
            ScriptManager.RegisterStartupScript(this, GetType(), "showalerts", "alert('Sucessfully created');", true);
      
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "clearTextvalue();submit();", true);
           
        }
        /// <summary>
        /// Mod By :Neha
        /// Mod Date:Dec 7th,2018
        /// Mod Des: deleted gridviewScrollTrigger function 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditAdd_Click(object sender, EventArgs e)
        {
            int output = 0;
            string[] number, name, email, usertype, status, Branch, contact;
            Empnumber = txteditempnumber.Text;
            number = Empnumber.Split(',');
            Empnumber = number[0];
            Empname = txteditempname.Text;
            name = Empname.Split(',');
            Empname = name[0];
            Empmail = txteditempmail.Text;
            email = Empmail.Split(',');
            Empmail = email[0];
            editempcontact = Request.Form[txteditempcontact.UniqueID];
            contact = editempcontact.Split(',');
            if (editempcontact != ",")
            {
                Empcontact = Convert.ToInt64(contact[0]);
            }

            Empusertype = Request.Form[txteditempusertype.UniqueID];
            usertype = Empusertype.Split(',');
            Empusertype = usertype[0];
            EditEmpstatus = Request.Form[txteditempstatus.UniqueID];
            status = EditEmpstatus.Split(',');
            Empstatus = Convert.ToInt32(status[0]);

            string[] splitemail;
            splitemail = Empmail.Split('@');
            if (Empname != "" && Empmail != "" && splitemail[0] != "")
            {
                if ((rdBtnTaegutec.Checked == true && splitemail[1] == "taegutec-india.com") || (rdBtnDuraCab.Checked == true && splitemail[1] == "duracarb-india.com"))
                {
                    if (Empusertype == "BM")
                    {
                        EmpBranch = Request.Form[editddlBranch.UniqueID];
                        Branch = EmpBranch.Split(',');
                        EmpBranch = Branch[0];
                        output = objEngInfo.EditUserInfo(Empnumber, Empname, EmpBranch, Empmail, Empcontact, Empstatus, Empusertype);
                    }
                    else if (Empusertype == "TM")
                    {
                        EmpBranch = Request.Form[editddlBranch.UniqueID];
                        Branch = EmpBranch.Split(',');
                        EmpBranch = Branch[0];
                        output = objEngInfo.EditUserInfo(Empnumber, Empname, EmpBranch, Empmail, Empcontact, Empstatus, Empusertype);

                        DataTable dtbranches = new DataTable();
                        dtbranches.Columns.Add("regioncode", typeof(string));

                        foreach (ListItem item in Cblterritorybranch.Items)
                        {
                            if (item != null)
                            {
                                if (item.Selected)
                                {
                                    dtbranches.Rows.Add(item.Value.ToString());

                                }
                            }
                        }
                        //Update TE Id in tt_territory_region_relation Table              
                        objEngInfo.InsertTag_for_territary(Empnumber, dtbranches);

                        //Update TE Id in tt_customer_master Table
                        objEngInfo.Update_TE_for_Customer(Empnumber);
                        // LoadBranchesforTerritary();
                    }
                    else
                    {
                        EmpBranch = null;
                        output = objEngInfo.EditUserInfo(Empnumber, Empname, EmpBranch, Empmail, Empcontact, Empstatus, Empusertype);
                    }
                }
            }
            if (output == 0)
            {
               
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('Not updated'); submit();", true);
             
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup", "alert('sucessfully updated');", true);
            }

            //LoadEngineerInfo();
            Ok_Click(null, null);
            LoadBranches();
            LoadBranchesforBranchmanager();
            LoadBranchesforTerritary();
          
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopup","submit();", true);
            
        }
        /// <summary>
        /// Modified by     :Neha
        /// Date            :11th feb,2019
        /// Description     :Binding gridview with null and calling submit 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnNotifctn_Click(object sender, EventArgs e)
        {
            string confirm_emailNotification = Request.Form["confirm_emailNotification"];
            if (confirm_emailNotification == "No")
            {
                //Response.Redirect("AdminProfile.aspx?RD");
               // Page_Load(null, null);
             //   ClientScript.RegisterStartupScript(Page.GetType(), "closeWindow", "<script type='text/javascript'>RefreshParentPage()</script>");
               ScriptManager.RegisterStartupScript(this, this.GetType(), "scripts", "submit();", true);
              
            }
            else
            {
                DataTable dtActiveUsers = new DataTable();
                string password;
                dtActiveUsers = objAdmin.getActiveUsers();
                if (dtActiveUsers.Rows.Count > 0)
                {
                    for (int i = 0; i < dtActiveUsers.Rows.Count; i++)
                    {
                        string emailId = dtActiveUsers.Rows[0].ItemArray[0].ToString();
                        password = dtActiveUsers.Rows[0].ItemArray[1].ToString();
                        if (string.IsNullOrEmpty(password) && emailId != null)
                        {
                            //if the password is null,create a new password
                            string generatedPassword = PasswordSecurityGenerate.Generate(8, 8);
                            generatedPassword = objauth.Encrypt(generatedPassword);
                            string result = objauth.setPassword(emailId, generatedPassword);
                            if (result != "Success")
                            {
                                ScriptManager.RegisterStartupScript(this, GetType(), "failedcreate", "alert('Failed to  create password for the new user');", true);
                            }
                            else
                            {
                                password = objauth.Decrypt(generatedPassword);
                            }
                        }
                        else
                        {
                            password = objauth.Decrypt(password);
                        }
                        try
                        {
                            MailMessage email = new MailMessage();
                            email.To.Add(new MailAddress(emailId)); //Destination Recipient e-mail address.
                            string applicationPath = VirtualPathUtility.GetDirectory(Request.Path);//HttpContext.Current.Request.Url.Authority;
                            string applicationPath1 = Request.Url.GetLeftPart(UriPartial.Authority) + Request.ApplicationPath;
                            applicationPath = applicationPath1;
                            email.Subject = " Welcome  To Sales-Budget & Performance Monitoring";//Subject for your request
                            string link = "Please <a href=\" " + applicationPath + ">login</a>";
                            email.Body = " <br/><br/>Your Username: " + emailId + "<br/><br/>Your Password: " + password + "<br/><br/>" + "From" + "<br/>" + "IT Team - TaeguTec" + "<br/>" + applicationPath;
                            email.IsBodyHtml = true;
                            SmtpClient smtpc = new SmtpClient();
                            smtpc.Send(email);
                        }
                        catch (Exception Ex)
                        {
                      
                            ScriptManager.RegisterStartupScript(this, GetType(), "failedmail", "submit(); alert('Failed to send email');", true);
                           
                        }
                    }
            
                    ScriptManager.RegisterStartupScript(this, GetType(), "showalert", "submit(); alert('Sucessfully sent email notification ');", true);
                   
                }
              
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "scripts", "submit();", true);
            
            }
        }
        /// <summary>
        ///  Mod By :Neha
        /// Mod Date:Dec 7th,2018
        /// Mod Des: deleted gridviewScrollTrigger function
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Ok_Click(object sender, EventArgs e)
        {
            string hoRoleId = null; string bmRoleId = null; string seRoleId = null; string adminRoleId = null; string TmRoledId = null;
            string region = ddlregiontype.SelectedItem.Value;
            DataTable dt = new DataTable();
            if (CheckBoxHo.Checked == true)
            {
                hoRoleId = "HO";
            }
            if (CheckBoxBm.Checked == true)
            {
                bmRoleId = "BM";
            }
            if (CheckBoxSe.Checked == true)
            {
                seRoleId = "SE";
            }
            if (CheckBoxAdmin.Checked == true)
            {
                adminRoleId = "Admin";
            }
            if (CheckBoxTM.Checked == true)
            {
                TmRoledId = "TM";
            }
            if (region == "ALL")
            {
                //anamika
                if (CheckBoxHo.Checked == false && CheckBoxBm.Checked == false && CheckBoxSe.Checked == false && CheckBoxAdmin.Checked == false && CheckBoxTM.Checked == false)
                {
                    hoRoleId = "HO";
                    bmRoleId = "BM";
                    seRoleId = "SE";
                    adminRoleId = "Admin";
                    TmRoledId = "TM";
                }
                //end
                region = null;
            }
            if (region != "ALL")
            {
                //anamika
                if (CheckBoxHo.Checked == false && CheckBoxBm.Checked == false && CheckBoxSe.Checked == false && CheckBoxAdmin.Checked == false && CheckBoxTM.Checked == false)
                {
                    hoRoleId = "HO";
                    bmRoleId = "BM";
                    seRoleId = "SE";
                    adminRoleId = "Admin";
                    TmRoledId = "TM";
                }
                //end
            }
            dt = objEngInfo.getEnginnerData(hoRoleId, bmRoleId, seRoleId, adminRoleId, TmRoledId, region, cter);
            ViewState["GridData"] = dt;
            GridEngInfo.DataSource = dt;
            GridEngInfo.DataBind();

            //divbminfo.Visible = false;
            if (CheckBoxSe.Checked == true && region != null)
            {
                //dt = objEngInfo.getEnginnerData("BM", region);
                //divbminfo.Visible = true;
                //foreach (DataRow row in dt.Rows)
                //{
                //    string name = row.Field<string>("EngineerName");
                //    LabelBmName.Text = "BRANCH MANGER :" + " " + name;
                //}

            }
            if (GridEngInfo.Rows.Count < 1)
            {
                GridEngInfo.Visible = false;
              
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "submit();alert('No results found');", true);
              
            }
            else
            {
                GridEngInfo.Visible = true;
            }
         
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup","submit();", true);
         
            engineerIdScript();
        }
        /// <summary>
        /// Author     :Neha
        /// Date       :11th feb,2019
        /// Description:on click of cancel calling sub,it method through script manager
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Cancels(object sender, EventArgs e)
        {
            //ClientScript.RegisterStartupScript(Page.GetType(), "closeWindow", "<script type='text/javascript'>RefreshParentPage()</script>");
           ScriptManager.RegisterStartupScript(this, GetType(), "ShowPopup", "submit();", true);
           
        }

        //protected void GeneratePopUp(object sender, EventArgs e)
        //{
        ////    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "S", "GeneratePopUp();", true);
        //}
        #endregion

        #region Methods
        protected void LoadEngineerInfo()
        {
            DataTable dt = new DataTable();
            dt = objEngInfo.getEnginnerData("HO", "BM", "SE", "Admin", "TM", null, cter);
            ViewState["GridData"] = null;
            ViewState["GridData"] = dt;
            GridEngInfo.DataSource = dt;
            GridEngInfo.DataBind();
        }
       /// <summary>
        ///  Mod By :Neha
        /// Mod Date:Dec 7th,2018
        /// Mod Des: deleted gridviewScrollTrigger function
       /// </summary>
        protected void LoadBranches()
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

            DataTable dtData = new DataTable();
            objRSum.roleId = "HO";
            objRSum.flag = "Branch";
            objRSum.cter = cter;
            dtData = objRSum.getFilterAreaValue(objRSum);

            ddlregiontype.DataSource = dtData;
            ddlregiontype.DataTextField = "BranchDesc";
            ddlregiontype.DataValueField = "BranchCode";
            ddlregiontype.DataBind();
            ddlregiontype.Items.Insert(0, "ALL");
        
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup","submit();", true);
           
        }
        protected void LoadBranchesforBranchmanager()
        {
            DataTable dtBranchesList = new DataTable();
            dtBranchesList = objEngInfo.LoadUserInfo(0, cter);

            ddlBranch.DataSource = dtBranchesList;
            ddlBranch.DataTextField = "region_description";
            ddlBranch.DataValueField = "region_code";
            ddlBranch.DataBind();

            editddlBranch.DataSource = dtBranchesList;
            editddlBranch.DataTextField = "region_description";
            editddlBranch.DataValueField = "region_code";
            editddlBranch.DataBind();
        }
        protected void LoadBranchesforTerritary()
        {
            DataTable dtBranchesList = new DataTable();
            dtBranchesList = objEngInfo.LoadUserInfo(1);

            ddlBranchterritary.DataSource = dtBranchesList;
            ddlBranchterritary.DataTextField = "region_description";
            ddlBranchterritary.DataValueField = "region_code";
            ddlBranchterritary.DataBind();

            Cblterritorybranch.DataSource = dtBranchesList;
            Cblterritorybranch.DataTextField = "region_description";
            Cblterritorybranch.DataValueField = "region_code";
            Cblterritorybranch.DataBind();
        }
        protected void bindgridColor()
        {
            if (GridEngInfo.Rows.Count != 0)
            {
                int color = 0;

                foreach (GridViewRow row in GridEngInfo.Rows)
                {

                    color++;
                    if (color == 1) { row.CssClass = "color_Product1 "; }
                    else if (color == 2)
                    {
                        row.CssClass = "color_Product2 ";
                        color = 0;
                    }

                }
            }
        }
        protected void LoadCSS()
        {
            cssList.Add("color_3");
            cssList.Add("color_4");
            cssList.Add("color_3");


        }
        protected string GetCSS(int colorIndex)
        {
            string index = Convert.ToString(colorIndex);
            string cIndex = index[index.Length - 1].ToString();

            if (cIndex.Contains("1"))
            { return cssList.ElementAt(1); }
            else if (cIndex.Contains("2"))
            { return cssList.ElementAt(2); }
            else { return cssList.ElementAt(2); }

        }
        protected void engineerIdScript()
        {
            string[] engineerId = new string[] { };
            DataTable dtEngineer = new DataTable();
            dtEngineer = objEngInfo.getEnginnerData("HO", "BM", "SE", "Admin", "TM", null, null);

            engineerId = dtEngineer.AsEnumerable().Select(r => r.Field<string>("EngineerId")).ToArray();
            //this.ClientScript.RegisterArrayDeclaration("engineerID", engid);
            StringBuilder sb = new StringBuilder();
            sb.Append("<script>");
            sb.Append("var jEngineerId = new Array;");
            foreach (string str in engineerId)
            {
                sb.Append("jEngineerId.push('" + str + "');");
            }
            sb.Append("</script>");

            if (!Page.ClientScript.IsClientScriptBlockRegistered(this.GetType(), "engineerIdArrayScript"))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "engineerIdArrayScript", sb.ToString(), false);
            }
            //ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "ShowPopups", "gridviewScrollTrigger(),submit();", true);

        }
        #endregion


    }
}