﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;

namespace TaegutecSalesBudget
{
    public partial class Dashboard : System.Web.UI.Page
    {
        #region Global declaration
        DashboardOverView objdashboard = new DashboardOverView();
        Budget objBudget = new Budget();
        Reports objReports = new Reports();
        Review objRSum = new Review();
        List<string> cssListFamilyHead = new List<string>();
        List<string> cssList = new List<string>();
        List<string> lsType = new List<string>();
        List<string> se_status = new List<string>();
        List<string> bm_status = new List<string>();
        List<string> ho_status = new List<string>();
        public static DataTable dtstatus, dtData;
        public static int se_statuscnt, bm_statuscnt, ho_statuscnt, total_cnt, se_cust_cnt, bm_cust_cnt, value;
        public static string cter;
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            //string ipaddress = string.Empty;
            //ipaddress = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName()).AddressList[1].ToString();

            if (Session["UserName"] != null)
            {
                if (!IsPostBack)
                {
                    string roleId = roleId = Session["RoleId"].ToString();
                    hdnRole.Value = roleId;
                    if (roleId == "HO")
                    {
                        if (Session["cter"] == null)
                        {
                            Session["cter"] = "TTA";
                            cter = "TTA";

                        }
                        if (Session["cter"].ToString() == "DUR")
                        {
                            rdBtnDuraCab.Checked = true;
                            rdBtnTaegutec.Checked = false;
                            cter = "DUR";
                        }
                        else
                        {
                            rdBtnTaegutec.Checked = true;
                            rdBtnDuraCab.Checked = false;
                            cter = "TTA";
                        }
                    }
                    LoadBranches();
                    //divbargraph.Visible = false;
                    //divbmbargraph.Visible = false;
                    LoadCSS();
                    LoadDashboard();
                    bindgridColor();
                    LoadBranchbyApproved();
                }
                if (Session["RoleId"] != null)
                {
                    string result = string.Empty;
                    result = objdashboard.getVisibilityStatus(lnkbtnBulkapproval.Text.ToString(), Session["RoleId"].ToString());
                    if (Session["RoleId"].ToString() == "HO" || Session["RoleId"].ToString() == "TM")
                    {
                        if (result == "1")
                        {
                            lnkbtnBulkapproval.Visible = true;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", "visibleLink();", true);
                            cterDiv.Visible = true;
                        }
                        else
                        {
                            lnkbtnBulkapproval.Visible = false;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "popup7", "disableLink();", true);
                            cterDiv.Visible = true;
                        }
                    }
                    else
                    {
                        lnkbtnBulkapproval.Visible = false;
                        cterDiv.Visible = false;
                    }
                }
            }
            else { Response.Redirect("Login.aspx?Login"); }

        }

        protected void lnkbtn_Vustomer_name_Click(object sender, EventArgs e)
        {
            Session["CustomerNumber"] = hdn_CustomerNum.Value;
            if (Session["CustomerNumber"] != null)
            {
                Response.Redirect("Budget.aspx?Budget");
            }
        }

        protected void btnCTOk_Click(object sender, EventArgs e)
        {
            if (Session["UserName"] == null) { Response.Redirect("Login.aspx?Login"); }
            if (ischeckedALL())
            {
                loadgrid_for_allchecks_true();
                return;
            }
            loadFilterDashboard();
            btnCTOk.Attributes.Remove("disabled");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "gridviewScrollTrigger();", true);
          
        }

        protected void btn_ho_s_ok_Click(object sender, EventArgs e)
        {
            if (Session["UserName"] == null) { Response.Redirect("Login.aspx?Login"); }
            if (ischeckedALL())
            {
                loadgrid_for_allchecks_true();
                return;
            }
            loadFilterDashboard();
        }

        protected void btn_bm_s_ok_Click(object sender, EventArgs e)
        {
            if (Session["UserName"] == null) { Response.Redirect("Login.aspx?Login"); }
            if (ischeckedALL())
            {
                loadgrid_for_allchecks_true();
                return;
            }
            loadFilterDashboard();
        }

        protected void btn_se_s_ok_Click(object sender, EventArgs e)
        {
            if (Session["UserName"] == null) { Response.Redirect("Login.aspx?Login"); }
            if (ischeckedALL())
            {
                loadgrid_for_allchecks_true();
                return;
            }
            loadFilterDashboard();
        }

        protected void btnRegionOk_Click(object sender, EventArgs e)
        {
            if (Session["UserName"] == null) { Response.Redirect("Login.aspx?Login"); }
            if (ischeckedALL())
            {
                loadgrid_for_allchecks_true();
                return;
            }
            loadFilterDashboard();
        }

        protected void btnBranchApproval_Click(object sender, EventArgs e)
        {
            string Branch = ddlBranchList.SelectedItem.Value;
            if (Branch == "--SELECT BRANCH--")
            {
                ddlBranchList.SelectedIndex = 0;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Please select branch and approve it');", true);

            }
            else
            {
                string result = objBudget.Approve_by_Branch(Branch);
                if (result == "Approved Successfully")
                {
                    ddlBranchList.SelectedIndex = 0;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Approved Successfully');", true);

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Selected Branch not Approved');", true);
                    ddlBranchList.SelectedIndex = 0;

                }
            }
        }

        protected void rbtn_Thousand_CheckedChanged(object sender, EventArgs e)
        {
            LoadDashboard();
            bindgridColor();
            LoadBranchbyApproved();
        }

        protected void rbtn_Lakhs_CheckedChanged(object sender, EventArgs e)
        {
            LoadDashboard();
            bindgridColor();
            LoadBranchbyApproved();
        }

        protected void rdBtnTaegutec_CheckedChanged(object sender, EventArgs e)
        {
            if (rdBtnTaegutec.Checked)
            {
                Session["cter"] = "TTA";
                cter = "TTA";
            }
            if (rdBtnDuraCab.Checked)
            {
                Session["cter"] = "DUR";
                cter = "DUR";
            }
            LoadBranches();
            LoadCSS();
            LoadDashboard();
            bindgridColor();
            LoadBranchbyApproved();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "gridviewScrollTrigger();", true);
            //this.Page.ClientScript.RegisterStartupScript(this.GetType(), "ShowPopup", "gridviewScrollTrigger();", true);
        }

        #endregion

        #region Method
        protected void LoadBranches()
        {

            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string roleId = Session["RoleId"].ToString();
            string userId = Session["UserId"].ToString();
            string branchcode = Session["BranchCode"].ToString();
            DataTable dtData = new DataTable();
            objRSum.BranchCode = userId; // passing here territory Engineer Id  as branch code IF role is TM 
            objRSum.roleId = roleId;
            objRSum.flag = "Branch";
            objRSum.cter = cter;
            dtData = objRSum.getFilterAreaValue(objRSum);
            cblRegion.DataSource = dtData;
            cblRegion.DataTextField = "BranchDesc";
            cblRegion.DataValueField = "BranchCode";
            cblRegion.DataBind();
            //cblRegion.Items.Insert(0, "ALL");


        }

        protected DataTable values_cs(DataTable dt)
        {
            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };

            DataTable dtValues = dt.Clone();

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string numString = dt.Rows[i].ItemArray[3].ToString(); //"1287543.0" will return false for a long 
                long number1 = 0;
                bool canConvert = long.TryParse(numString, out number1);
                if (canConvert == true)
                {
                    dtValues.Rows.Add(dt.Rows[i].ItemArray[0],
                                      dt.Rows[i].ItemArray[1],
                                      dt.Rows[i].ItemArray[2],
                                      Convert.ToInt32(dt.Rows[i].ItemArray[3].ToString()).ToString("N0", culture),
                                      dt.Rows[i].ItemArray[4],
                                      dt.Rows[i].ItemArray[5],
                                      dt.Rows[i].ItemArray[6],
                                      dt.Rows[i].ItemArray[7],
                                      dt.Rows[i].ItemArray[8],
                                      dt.Rows[i].ItemArray[9]
                                        );
                }
                else
                {
                    dtValues.Rows.Add(dt.Rows[i].ItemArray[0],
                                    dt.Rows[i].ItemArray[1],
                                    dt.Rows[i].ItemArray[2],
                                    dt.Rows[i].ItemArray[3],
                                    dt.Rows[i].ItemArray[4],
                                    dt.Rows[i].ItemArray[5],
                                    dt.Rows[i].ItemArray[6],
                                    dt.Rows[i].ItemArray[7],
                                    dt.Rows[i].ItemArray[8],
                                    dt.Rows[i].ItemArray[9]
                                      );
                }
            }

            return dtValues;
        }

        protected void LoadDashboard()
        {
            if (Session["UserId"] != null)
            {
                
                string Role = Session["RoleId"].ToString();
                string region = Session["BranchCode"].ToString();
                string salesEngineerId = Session["UserId"].ToString();
                dtData = new DataTable();
                dtData = objdashboard.getDashboardData(region, salesEngineerId, Role, cter);
                if (rbtn_Thousand.Checked) { dtData = getDetails(dtData); }
                if (rbtn_Lakhs.Checked) { dtData = getDetails(dtData, 100000); }

                loadgauges(dtData);
                // loadpiecharts(dtData);
                LoadPieCharts();
                DataTable dtData1 = values_cs(dtData);
                ViewState["AllData"] = dtData1;
                grdviewAllValues.DataSource = dtData1;
                grdviewAllValues.DataBind();

            }
        }

        private void LoadPieCharts()
        {
            DataTable dtPieCustData;
            DataTable dtPieCPData;
            DataTable dtPieTotalData;
            try
            {
                dtPieCustData=new DataTable();
                dtPieCPData=new DataTable();
                dtPieTotalData=new DataTable();
                string Role = Session["RoleId"].ToString();
                string Branch = Session["BranchCode"].ToString();
                string UserId = Session["UserId"].ToString();
                dtPieCustData = objdashboard.getBudgetDashboardData(Branch, UserId, Role, "C", cter);
                Session["DashboardDataCust"] = dtPieCustData;
                dtPieCPData = objdashboard.getBudgetDashboardData(Branch, UserId, Role, "D", cter);
                Session["DashboardDataCP"] = dtPieCPData;
                dtPieTotalData = new DataTable();
                dtPieTotalData.Columns.Add("Status");
                dtPieTotalData.Columns.Add("c_count");
                dtPieTotalData.Columns.Add("value");

                DataRow dr;
                for (int i = 0; i < dtPieCustData.Rows.Count; i++)
                {
                    dr = dtPieTotalData.NewRow();
                    dr["Status"] = Convert.ToString(dtPieCustData.Rows[i]["Status"]);
                    dr["c_count"] = Convert.ToInt32(dtPieCustData.Rows[i]["c_count"]) + Convert.ToInt32(dtPieCPData.Rows[i]["c_count"]);
                    dr["value"] = Convert.ToInt32(dtPieCustData.Rows[i]["value"]) + Convert.ToInt32(dtPieCPData.Rows[i]["value"]);
                    dtPieTotalData.Rows.Add(dr);
                }
                Session["DashboardDataTotal"] = dtPieTotalData;

            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }
        }
        public void loadgauges(DataTable dt)
        {
            se_statuscnt = 0; bm_statuscnt = 0; ho_statuscnt = 0; total_cnt = 0; se_cust_cnt = 0;
            DataTable dtData = dt;
            if (dtData.Rows.Count > 0)
            {
                for (int i = 0; i < dtData.Rows.Count; i++)
                {
                    if (dtData.Rows[i].ItemArray[5].ToString() == "Submitted")
                    {
                        se_statuscnt++;
                    }
                    if (dtData.Rows[i].ItemArray[6].ToString() == "Approved")
                    {
                        bm_statuscnt++;
                    }
                    if (dtData.Rows[i].ItemArray[7].ToString() == "Approved")
                    {
                        ho_statuscnt++;
                    }
                    if (dtData.Rows[i].ItemArray[5].ToString() == "Submitted" || dtData.Rows[i].ItemArray[5].ToString() == "Not Initiated" || dtData.Rows[i].ItemArray[5].ToString() == "Pending Submission")
                    {
                        se_cust_cnt++;
                    }
                    total_cnt = i;
                }
            }
            hdn_se_cust_cnt.Value = se_cust_cnt.ToString();
            hdn_se_statuscnt.Value = se_statuscnt.ToString();
            lblsestatus.Text = "No. of Customers Submitted:" + se_statuscnt.ToString();
            hdn_bm_statuscnt.Value = bm_statuscnt.ToString();
            lblbmstatus.Text = "No. of Customers Approved:" + bm_statuscnt.ToString();
            hdn_ho_statuscnt.Value = ho_statuscnt.ToString();
            lblhostatus.Text = "No. of Customers Approved:" + ho_statuscnt.ToString();
            hdn_total_cnt.Value = (total_cnt - 1).ToString();

        }

        protected DataTable getDetails(DataTable dt, int flag = 1000)
        {
            se_statuscnt = 0; bm_statuscnt = 0; ho_statuscnt = 0;
            string strUserId = Session["UserId"].ToString();
            string MainRole = Session["RoleId"].ToString();
            DataTable dtCustomer = new DataTable();
            if (MainRole == "HO") { dtCustomer = objdashboard.getCustomerDetails_for_BM(); }
            else if (MainRole == "BM") { dtCustomer = objBudget.LoadCustomerDetails(strUserId, "SE"); }


            DataTable dtDetails = new DataTable();
            dtDetails.Columns.Add("customer_number", typeof(string)); //0
            dtDetails.Columns.Add("customer_name", typeof(string)); // 1
            dtDetails.Columns.Add("Customer_region", typeof(string));// 2
            dtDetails.Columns.Add("estimate_value_next_year", typeof(string)); //3
            dtDetails.Columns.Add("salesengineer_id", typeof(string)); //4
            dtDetails.Columns.Add("status_sales_engineer", typeof(string)); //5
            dtDetails.Columns.Add("status_branch_manager", typeof(string)); //6
            dtDetails.Columns.Add("status_ho", typeof(string)); //7
            dtDetails.Columns.Add("customer_type", typeof(string)); //8
            dtDetails.Columns.Add("flag", typeof(string)); //9

            decimal totalValue = 0;
            if (MainRole == "BM" || MainRole == "HO")
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        dtDetails.Rows.Add("Customer Number",
                                            "Customer Name",
                                            "Region",
                                            "Value",
                                            "Sales Engineer Id",
                                            "Sales Engineer Status",
                                            "Branch Manager Status",
                                            "HO Status",
                                            "Customer Type",
                                            "Heading"

                                            );
                    }

                    decimal value = dt.Rows[i].ItemArray[3].ToString() != "" ? Convert.ToDecimal(dt.Rows[i].ItemArray[3].ToString()) : 0;
                    totalValue = totalValue + value;

                    string SEStatus = dt.Rows[i].ItemArray[5].ToString();
                    string BMStatus = dt.Rows[i].ItemArray[6].ToString();
                    string HOStatus = dt.Rows[i].ItemArray[7].ToString();
                    string CustType = dt.Rows[i].ItemArray[8].ToString();
                    if (CustType.Contains("C")) { CustType = "CUSTOMER"; }
                    else if (CustType.Contains("D")) { CustType = "CHANNEL PARTNER"; }

                    int count = 0;
                    for (int j = 0; j < dtCustomer.Rows.Count; j++)
                    {
                        string custNum = dtCustomer.Rows[j].ItemArray[1].ToString();
                        if (custNum == dt.Rows[i].ItemArray[0].ToString())
                        {

                            if (BMStatus.Contains("SAVE")) { BMStatus = "Pending Submission"; SEStatus = ""; }
                            else if (BMStatus.Contains("REVIEW")) { BMStatus = "Pending Submission"; SEStatus = ""; }
                            else if (BMStatus.Contains("APPROVE")) { BMStatus = "Submitted"; SEStatus = ""; }
                            else
                            {
                                BMStatus = "Not Initiated"; SEStatus = "";
                            }
                            if (HOStatus.Contains("REVIEW")) { HOStatus = ""; }
                            if (HOStatus.Contains("SAVE")) { HOStatus = ""; }
                            if (HOStatus.Contains("APPROVE")) { HOStatus = "Approved"; }
                            if (BMStatus == "Submitted" && !HOStatus.Contains("Approved")) { HOStatus = "To be Reviewed"; }
                            count++;
                        }
                    }
                    if (count == 0)
                    {
                        if (SEStatus.Contains("SAVE")) { SEStatus = "Pending Submission"; }
                        else if (SEStatus.Contains("REVIEW")) { SEStatus = "Pending Submission"; }
                        else if (SEStatus.Contains("APPROVE")) { SEStatus = "Submitted"; }
                        else { SEStatus = "Not Initiated"; }

                        if (BMStatus.Contains("SAVE")) { BMStatus = "Pending Submission"; }
                        else if (BMStatus.Contains("REVIEW")) { BMStatus = "Sent for Review"; }
                        else if (BMStatus.Contains("APPROVE")) { BMStatus = "Approved"; }

                        if (HOStatus.Contains("APPROVE")) { HOStatus = "Approved"; }
                        if (HOStatus.Contains("REVIEW")) { HOStatus = ""; }
                        if (HOStatus.Contains("SAVE")) { HOStatus = ""; }

                        if (BMStatus == "Pending Submission" && SEStatus == "Not Initiated") { SEStatus = ""; }
                        if (BMStatus == "Approved" && SEStatus == "Not Initiated") { BMStatus = "Pending Submission"; }
                        if (BMStatus == "" && SEStatus == "Submitted") { BMStatus = "To be Reviewed"; }
                        if (BMStatus == "Approved" && SEStatus == "Submitted" && !HOStatus.Contains("Approved")) { HOStatus = "To be Reviewed"; }
                        if (BMStatus == "Approved" && SEStatus == "Submitted" && HOStatus.Contains("Approved")) { HOStatus = "Approved"; }
                    }



                    dtDetails.Rows.Add(dt.Rows[i].ItemArray[0].ToString(),
                                        dt.Rows[i].ItemArray[1].ToString(),
                                        dt.Rows[i].ItemArray[2].ToString(),
                                        Math.Round(value / flag),
                                        dt.Rows[i].ItemArray[4].ToString(),
                                        SEStatus,
                                        BMStatus,
                                        HOStatus,
                                        CustType,
                                        "customer"
                                        );
                }
            }
            else
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        dtDetails.Rows.Add("Customer Number",
                                            "Customer Name",
                                            "Region",
                                            "Value",
                                            "Sales Engineer Id",
                                            "Sales Engineer Status",
                                            "Branch Manager Status",
                                            "HO Status",
                                            "Customer Type",
                                            "Heading"

                                            );
                    }

                    decimal value = dt.Rows[i].ItemArray[3].ToString() != "" ? Convert.ToDecimal(dt.Rows[i].ItemArray[3].ToString()) : 0;
                    totalValue = totalValue + value;

                    string SEStatus = dt.Rows[i].ItemArray[5].ToString();
                    string BMStatus = dt.Rows[i].ItemArray[6].ToString();
                    string HOStatus = dt.Rows[i].ItemArray[7].ToString();
                    string CustType = dt.Rows[i].ItemArray[8].ToString();
                    if (CustType.Contains("C")) { CustType = "CUSTOMER"; }
                    else if (CustType.Contains("D")) { CustType = "CHANNEL PARTNER"; }
                    if (SEStatus.Contains("SAVE")) { SEStatus = "Pending Submission"; }
                    else if (SEStatus.Contains("REVIEW")) { SEStatus = "Pending Submission"; }
                    else if (SEStatus.Contains("APPROVE")) { SEStatus = "Submitted"; }
                    else { SEStatus = "Not Initiated"; }

                    if (BMStatus.Contains("SAVE")) { BMStatus = "Pending Submission"; }
                    else if (BMStatus.Contains("REVIEW")) { BMStatus = "Sent for Review"; }
                    else if (BMStatus.Contains("APPROVE")) { BMStatus = "Approved"; }

                    if (HOStatus.Contains("APPROVE")) { HOStatus = "Approved"; }
                    if (HOStatus.Contains("REVIEW")) { HOStatus = ""; }
                    if (HOStatus.Contains("SAVE")) { HOStatus = ""; }
                    if (BMStatus == "Pending Submission" && SEStatus == "Not Initiated")
                    { SEStatus = ""; }
                    if (BMStatus == "Approved" && SEStatus == "Not Initiated") { BMStatus = "Pending Submission"; }
                    if (BMStatus == "" && SEStatus == "Submitted") { BMStatus = "To be Reviewed"; }

                    if (BMStatus == "Approved" && SEStatus == "Submitted" && !HOStatus.Contains("Approved")) { HOStatus = "To be Reviewed"; }
                    if (BMStatus == "Approved" && SEStatus == "Submitted" && HOStatus.Contains("Approved")) { HOStatus = "Approved"; }
                    dtDetails.Rows.Add(dt.Rows[i].ItemArray[0].ToString(),
                                        dt.Rows[i].ItemArray[1].ToString(),
                                        dt.Rows[i].ItemArray[2].ToString(),
                                        Math.Round(value / flag),
                                        dt.Rows[i].ItemArray[4].ToString(),
                                        SEStatus,
                                        BMStatus,
                                        HOStatus,
                                        CustType,
                                        "customer"
                                        );
                }
            }
            dtDetails.Rows.Add("",
                                "Grand Total",
                                "",
                                Math.Round(totalValue / flag),
                                "",
                                "",
                                "",
                                "",
                                "",
                                "Total");
            return dtDetails;
        }

        protected void bindgridColor()
        {
            if (grdviewAllValues.Rows.Count != 0)
            {

                int color = 0;

                foreach (GridViewRow row in grdviewAllValues.Rows)
                {
                    var Flag = row.FindControl("lblFlag") as Label;
                    if (Flag.Text == "customer")
                    {
                        color++;
                        if (color == 1) { row.CssClass = "color_Product1 "; }
                        else if (color == 2)
                        {
                            row.CssClass = "color_Product2 ";
                            color = 0;
                        }
                        for (int j = 0; j < row.Cells.Count; j++) { row.Cells[7].CssClass = "subTotalRowGrid"; }
                    }
                    else if (Flag.Text == "Heading")
                    {
                        row.CssClass = "greendarkDashboard";
                    }
                    else if (Flag.Text == "Total")
                    {
                        row.CssClass = "MainTotal";
                        //for (int j = 0; j < row.Cells.Count; j++) { row.Cells[6].CssClass = "MainTotal"; }
                    }
                }
            }




        }
        protected void LoadCSS()
        {
            cssList.Add("color_3");
            cssList.Add("color_4");
            cssList.Add("color_3");


        }
        protected string GetCSS(int colorIndex)
        {
            string index = Convert.ToString(colorIndex);
            string cIndex = index[index.Length - 1].ToString();

            if (cIndex.Contains("1"))
            { return cssList.ElementAt(1); }
            else if (cIndex.Contains("2"))
            { return cssList.ElementAt(2); }
            else { return cssList.ElementAt(2); }

        }
        protected void LoadBranchbyApproved()
        {
            DataTable dtBranchesList = new DataTable();
            string role = Session["RoleId"].ToString();
            if (role == "TM")
            {
                string TeId = Session["UserId"].ToString();
                dtBranchesList = objBudget.getBranch_by_managerApproval(cter, role, TeId);
            }
            else { dtBranchesList = objBudget.getBranch_by_managerApproval(cter); }
            if (dtBranchesList != null)
            {
                ddlBranchList.DataSource = dtBranchesList;
                ddlBranchList.DataTextField = "region_description";
                ddlBranchList.DataValueField = "Customer_region";
                ddlBranchList.DataBind();
                ddlBranchList.Items.Insert(0, "--SELECT BRANCH--");
            }

        }

        protected bool ischeckedALL()
        {
            bool check;
            int countRegion = 0;
            int countCheckedRegion = 0;
            bool regionCheck = false; ;
            foreach (ListItem item in cblRegion.Items)
            {
                countRegion++;
                if (item.Selected) { countCheckedRegion++; }
            }
            if (countRegion == countCheckedRegion)
            {
                regionCheck = true;
            }

            if ((cbCTCustomer.Checked) && (cbCTDistributor.Checked) && (regionCheck)
                    && (cb_se_NI.Checked) && (cb_se_PS.Checked) && (cb_se_Sub.Checked)
                    && (cb_bm_tr.Checked) && (cb_bm_sr.Checked) && (cb_bm_app.Checked) && (cb_bm_NI.Checked) && (cb_bm_PS.Checked) && (cb_bm_Sub.Checked)
                    && (cb_ho_tr.Checked) && (cb_ho_app.Checked))
            { check = true; }

            else { check = false; }
            return check;
        }

        protected void clearAll()
        {
            try
            {
                
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }
        }

        protected void loadgrid_for_allchecks_true()
        {
            if (ViewState["AllData"] != null)
            {
                DataTable dtData = ViewState["AllData"] as DataTable;
                if (dtData.Rows.Count != 0)
                {
                    grdviewAllValues.DataSource = dtData;
                    grdviewAllValues.DataBind();
                }
            }
            else
            {
                LoadDashboard();
            }
            LoadCSS();
            bindgridColor();
        }

        #region Query Making for filter
        protected string FilterQueryMaking()
        {
            string query = "";
            //region pending

            string ctType = "";
            if (!cbCTCustomer.Checked) { ctType += " customer_type NOT LIKE 'C' AND "; }
            if (!cbCTDistributor.Checked) { ctType += " customer_type NOT LIKE 'D' AND "; }

            string region = "";
            foreach (ListItem item in cblRegion.Items)
            {
                if (!item.Selected)
                {
                    region += " Customer_region NOT Like '" + item.Text + "' AND ";
                }
            }

            string rawquery = ctType + region;
            if (rawquery.Length > 4)
            {
                rawquery = rawquery.Remove(rawquery.Length - 4);
                query = "SELECT *  FROM tt_dashboard_v  WHERE " + rawquery + " ORDER BY customer_name ASC ";
            }
            else
            {
                query = "SELECT *  FROM tt_dashboard_v ORDER BY customer_name ASC ";
            }

            return query;
        }
        #endregion

        protected void loadFilterDashboard()
        {
            string query = FilterQueryMaking();
            if (ViewState["AllData"] != null)
            {
                DataTable dtDashboard = ViewState["AllData"] as DataTable;//objdashboard.GetFilterData(query);
                if (dtDashboard.Rows.Count != 0)
                {
                    dtDashboard = reFilter(dtDashboard);
                    if (dtDashboard.Rows.Count > 2)
                    {
                        //DataTable dtDashboard1 = values_cs(dtDashboard);
                        grdviewAllValues.DataSource = dtDashboard;
                        grdviewAllValues.DataBind();
                        //loadgauges(dtDashboard);
                        //loadpiecharts(dtDashboard);
                        footer.Style.Add("Height", "20px");
                        divcnsldt.Style.Add("display", "block");
                        cnsldt_img.Attributes["Src"] = "images/button_minus.gif";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "gridviewScrollTrigger();", true);
                        LoadCSS();
                        bindgridColor();
                    }
                    else
                    {
                        dtDashboard.Rows.Clear();
                        DataTable dt = new DataTable();

                        dtDashboard.Rows.Add("Customer Number",
                                            "Customer Name",
                                            "Region",
                                            "Value",
                                            "Sales Engineer Id",
                                            "Sales Engineer Status",
                                            "Branch Manager Status",
                                            "HO Status",
                                            "Customer Type",
                                            "Heading"

                                            );
                        dtDashboard.Rows.Add("", "", "", "", "", "", "", "", "", "");
                        DataTable dtDashboard1 = values_cs(dtDashboard);
                        grdviewAllValues.DataSource = dtDashboard1;
                        grdviewAllValues.DataBind();
                        footer.Style.Add("Height", "200px");
                        divcnsldt.Style.Add("display", "block");
                        cnsldt_img.Attributes["Src"] = "images/button_minus.gif";
                        LoadCSS();
                        bindgridColor();
                        int columnsCount = grdviewAllValues.Columns.Count;
                        grdviewAllValues.Rows[1].Cells.Clear();// clear all the cells in the row
                        grdviewAllValues.Rows[1].Cells.Add(new TableCell()); //add a new blank cell

                        //You can set the styles here
                        grdviewAllValues.Rows[1].Cells[0].HorizontalAlign = HorizontalAlign.Center;
                        grdviewAllValues.Rows[1].Cells[0].ForeColor = System.Drawing.Color.Red;
                        grdviewAllValues.Rows[1].Cells[0].Font.Bold = true;

                        grdviewAllValues.Rows[1].Cells[0].Text = "NO RESULTS FOUND!";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "gridviewScrollTrigger();", true);

                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('No data for this selection');", true);
                    }

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Oops something went wrong, we can not filter');", true);
                }
            }
            else
            {
                string Role = Session["RoleId"].ToString();
                string region = Session["BranchCode"].ToString();
                string salesEngineerId = Session["UserId"].ToString();
                DataTable dtDashboard = objdashboard.getDashboardData(region, salesEngineerId, Role, cter);
                if (rbtn_Thousand.Checked) { dtDashboard = getDetails(dtData); }
                if (rbtn_Lakhs.Checked) { dtDashboard = getDetails(dtData, 100000); }
                //dtDashboard = getDetails(dtDashboard);
                if (dtDashboard.Rows.Count != 0)
                {
                    dtDashboard = reFilter(dtDashboard);
                    if (dtDashboard.Rows.Count > 2)
                    {
                        DataTable dtDashboard1 = values_cs(dtDashboard);
                        grdviewAllValues.DataSource = dtDashboard1;
                        grdviewAllValues.DataBind();
                        //loadgauges(dtDashboard);
                        //loadpiecharts(dtDashboard);
                        footer.Style.Add("Height", "20px");
                        divcnsldt.Style.Add("display", "block");
                        cnsldt_img.Attributes["Src"] = "images/button_minus.gif";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "gridviewScrollTrigger();", true);
                        LoadCSS();
                        bindgridColor();
                    }
                    else
                    {

                        dtDashboard.Rows.Clear();
                        DataTable dt = new DataTable();

                        dtDashboard.Rows.Add("Customer Number",
                                            "Customer Name",
                                            "Region",
                                            "Value",
                                            "Sales Engineer Id",
                                            "Sales Engineer Status",
                                            "Branch Manager Status",
                                            "HO Status",
                                            "Customer Type",
                                            "Heading"

                                            );
                        dtDashboard.Rows.Add("", "", "", "", "", "", "", "", "", "");

                        DataTable dtDashboard1 = values_cs(dtDashboard);
                        grdviewAllValues.DataSource = dtDashboard1;
                        grdviewAllValues.DataBind();
                        footer.Style.Add("Height", "200px");
                        divcnsldt.Style.Add("display", "block");
                        cnsldt_img.Attributes["Src"] = "images/button_minus.gif";
                        LoadCSS();
                        bindgridColor();
                        int columnsCount = grdviewAllValues.Columns.Count;
                        grdviewAllValues.Rows[1].Cells.Clear();// clear all the cells in the row
                        grdviewAllValues.Rows[1].Cells.Add(new TableCell()); //add a new blank cell

                        //You can set the styles here
                        grdviewAllValues.Rows[1].Cells[0].HorizontalAlign = HorizontalAlign.Center;
                        grdviewAllValues.Rows[1].Cells[0].ForeColor = System.Drawing.Color.Red;
                        grdviewAllValues.Rows[1].Cells[0].Font.Bold = true;

                        grdviewAllValues.Rows[1].Cells[0].Text = "NO RESULTS FOUND!";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "gridviewScrollTrigger();", true);

                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('No data for this selection');", true);
                    }

                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Oops something went wrong, we can not filter');", true);
                }
            }
        }

        protected DataTable reFilter(DataTable dtFilter)
        {
            DataTable dtDashboard = new DataTable();
            dtDashboard = dtFilter.Clone();

            string cmdquery = "";

            string SE_val = "";
            string BM_Val = "";
            string HO_Val = "";
            if (cb_se_NI.Checked) { SE_val += " 'Not Initiated' , "; }
            if (cb_se_PS.Checked) { SE_val += " 'Pending Submission' , "; }
            if (cb_se_Sub.Checked) { SE_val += " 'Submitted' , "; }

            if (cb_bm_tr.Checked) { BM_Val += " 'To be Reviewed' , "; }
            if (cb_bm_sr.Checked) { BM_Val += " 'Sent for Review' , "; }
            if (cb_bm_app.Checked) { BM_Val += " 'Approved' , "; }
            if (cb_bm_NI.Checked) { BM_Val += " 'Not Initiated' , "; }
            if (cb_bm_PS.Checked) { BM_Val += " 'Pending Submission' , "; }
            if (cb_bm_Sub.Checked) { BM_Val += " 'Submitted' , "; }

            if (cb_ho_tr.Checked) { HO_Val += " 'To be Reviewed' , "; }
            if (cb_ho_app.Checked) { HO_Val += " 'Approved' , "; }
            //if (ch_ho_null.Checked) { HO_Val += " '' , "; }

            string ctType = "";
            if (cbCTCustomer.Checked) { ctType += "'CUSTOMER' , "; }
            if (cbCTDistributor.Checked) { ctType += "'CHANNEL PARTNER' , "; }

            string region = "";

            foreach (ListItem item in cblRegion.Items)
            {
                if (item.Selected)
                {
                    region += " '" + item.Text + "' , ";
                }
            }


            if (SE_val.Length > 2)
            {
                SE_val = SE_val.Remove(SE_val.Length - 2);
            }
            if (BM_Val.Length > 2)
            {
                BM_Val = BM_Val.Remove(BM_Val.Length - 2);
            }
            if (HO_Val.Length > 2)
            {
                HO_Val = HO_Val.Remove(HO_Val.Length - 2);
            }
            if (ctType.Length > 2) { ctType = ctType.Remove(ctType.Length - 2); }
            if (region.Length > 2) { region = region.Remove(region.Length - 2); }


            if (SE_val.Length > 2)
            {
                cmdquery += " status_sales_engineer IN (" + SE_val + ")" + " AND ";
            }
            if (BM_Val.Length > 2)
            {
                cmdquery += " status_branch_manager IN (" + BM_Val + ")" + " AND ";
            }
            if (HO_Val.Length > 2)
            {
                cmdquery += " status_ho IN (" + HO_Val + ")" + " AND ";
            }
            if (region.Length > 2) { cmdquery += " Customer_region IN (" + region + ")" + " AND "; }
            if (ctType.Length > 2) { cmdquery += " customer_type IN (" + ctType + ")" + " AND "; }

            if (cmdquery.Length > 4)
            {
                cmdquery = cmdquery.Remove(cmdquery.Length - 4);
            }

            DataRow[] drarray;
            string sortExp = "customer_name";
            drarray = dtFilter.Select(cmdquery, sortExp);
            int rowcount = 0;
            decimal totalValue = 0;
            foreach (DataRow row in drarray)
            {
                if (rowcount == 0)
                {
                    // add here header
                    dtDashboard.Rows.Add("Customer Number",
                                            "Customer Name",
                                            "Region",
                                            "Value",
                                            "Sales Engineer Id",
                                            "Sales Engineer Status",
                                            "Branch Manager Status",
                                            "HO Status",
                                            "Customer Type",
                                            "Heading"

                                            );
                }
                decimal value = row.ItemArray[3].ToString() != "" ? Convert.ToDecimal(row.ItemArray[3].ToString()) : 0;
                totalValue = totalValue + value;

                dtDashboard.Rows.Add(row.ItemArray[0].ToString(),
                                   row.ItemArray[1].ToString(),
                                   row.ItemArray[2].ToString(),
                                   row.ItemArray[3].ToString(),
                                   row.ItemArray[4].ToString(),
                                   row.ItemArray[5].ToString(),
                                   row.ItemArray[6].ToString(),
                                   row.ItemArray[7].ToString(),
                                   row.ItemArray[8].ToString(),
                                   row.ItemArray[9].ToString()
                                    );
                rowcount++;
            }

            dtDashboard.Rows.Add("",
                                "Grand Total",
                                "",
                                Math.Round(totalValue),
                                "",
                                "",
                                "",
                                "",
                                "",
                                "Total");
            return dtDashboard;
        }

        public static string DataTableToJSONWithStringBuilder(DataTable table)
        {
            var JSONString = new StringBuilder();
            if (table.Rows.Count > 0)
            {
                JSONString.Append("[");
                for (int i = 0; i < table.Rows.Count; i++)
                {
                    JSONString.Append("{");
                    for (int j = 0; j < table.Columns.Count; j++)
                    {
                        if (j < table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\",");
                        }
                        else if (j == table.Columns.Count - 1)
                        {
                            JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\"");
                        }
                    }
                    if (i == table.Rows.Count - 1)
                    {
                        JSONString.Append("}");
                    }
                    else
                    {
                        JSONString.Append("},");
                    }
                }
                JSONString.Append("]");
            }
            return JSONString.ToString();
        }
        #endregion

        #region WebMethod
        [WebMethod]
        public static string LoadChartByCustomer()
        {
            CommonFunctions objCom = new CommonFunctions();
            DataTable dtoutput;
            string output = string.Empty;
            try
            {
                dtoutput = (DataTable)HttpContext.Current.Session["DashboardDataCust"];
               
                if (dtoutput.Rows.Count > 0)
                {
                    output = DataTableToJSONWithStringBuilder(dtoutput);
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }
            return output;
        }

        [WebMethod]
        public static string LoadChartByCP()
        {
            CommonFunctions objCom = new CommonFunctions();
            DataTable dtoutput;
            string output = string.Empty;
            try
            {
                dtoutput = (DataTable)HttpContext.Current.Session["DashboardDataCP"];

                if (dtoutput.Rows.Count > 0)
                {
                    output = DataTableToJSONWithStringBuilder(dtoutput);
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }
            return output;
        }

        [WebMethod]
        public static string LoadChartTotal()
        {
            CommonFunctions objCom = new CommonFunctions();
            DataTable dtoutput;
            string output = string.Empty;
            try
            {
                dtoutput = (DataTable)HttpContext.Current.Session["DashboardDataTotal"];

                if (dtoutput.Rows.Count > 0)
                {
                    output = DataTableToJSONWithStringBuilder(dtoutput);
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }
            return output;
        }
        #endregion

        #region System Code
        private byte[] Compress(byte[] b)
        {
            MemoryStream ms = new MemoryStream();
            GZipStream zs = new GZipStream(ms, CompressionMode.Compress, true);
            zs.Write(b, 0, b.Length);
            zs.Close();
            return ms.ToArray();
        }

        /// This method takes the compressed byte stream as parameter
        /// and return a decompressed bytestream.

        private byte[] Decompress(byte[] b)
        {
            MemoryStream ms = new MemoryStream();
            GZipStream zs = new GZipStream(new MemoryStream(b),
                                           CompressionMode.Decompress, true);
            byte[] buffer = new byte[4096];
            int size;
            while (true)
            {
                size = zs.Read(buffer, 0, buffer.Length);
                if (size > 0)
                    ms.Write(buffer, 0, size);
                else break;
            }
            zs.Close();
            return ms.ToArray();
        }

        protected override object LoadPageStateFromPersistenceMedium()
        {
            System.Web.UI.PageStatePersister pageStatePersister1 = this.PageStatePersister;
            pageStatePersister1.Load();
            String vState = pageStatePersister1.ViewState.ToString();
            byte[] pBytes = System.Convert.FromBase64String(vState);
            pBytes = Decompress(pBytes);
            LosFormatter mFormat = new LosFormatter();
            Object ViewState = mFormat.Deserialize(System.Convert.ToBase64String(pBytes));
            return new Pair(pageStatePersister1.ControlState, ViewState);
        }

        protected override void SavePageStateToPersistenceMedium(Object pViewState)
        {
            Pair pair1;
            System.Web.UI.PageStatePersister pageStatePersister1 = this.PageStatePersister;
            Object ViewState;
            if (pViewState is Pair)
            {
                pair1 = ((Pair)pViewState);
                pageStatePersister1.ControlState = pair1.First;
                ViewState = pair1.Second;
            }
            else
            {
                ViewState = pViewState;
            }
            LosFormatter mFormat = new LosFormatter();
            StringWriter mWriter = new StringWriter();
            mFormat.Serialize(mWriter, ViewState);
            String mViewStateStr = mWriter.ToString();
            byte[] pBytes = System.Convert.FromBase64String(mViewStateStr);
            pBytes = Compress(pBytes);
            String vStateStr = System.Convert.ToBase64String(pBytes);
            pageStatePersister1.ViewState = vStateStr;
            pageStatePersister1.Save();
        }

        #endregion

        #region Commented Code


        //public int status_cnt(DataTable dt, string status_type, string cust_type, int column_no)
        //{
        //    int count = 0;
        //    if (dt.Rows.Count > 0)
        //    {
        //        for (int i = 0; i < dt.Rows.Count; i++)
        //        {
        //            if (dt.Rows[i].ItemArray[8].ToString() == cust_type)
        //            {
        //                if (dt.Rows[i].ItemArray[column_no].ToString() == status_type)
        //                {
        //                    count++;

        //                }
        //            }
        //        }
        //    }
        //    return count;
        //}
        ////This is for totals(no customer type)
        //public int status_total_cnt(DataTable dt, string status_type, int column_no)
        //{
        //    int count = 0;
        //    if (dt.Rows.Count > 0)
        //    {
        //        for (int i = 0; i < dt.Rows.Count; i++)
        //        {

        //            if (dt.Rows[i].ItemArray[column_no].ToString() == status_type)
        //            {
        //                count++;
        //            }

        //        }
        //    }
        //    return count;
        //}

        //public int budget_val(DataTable dt, string status_type, string cust_type, int column_no)
        //{
        //    value = 0;
        //    if (dt.Rows.Count > 0)
        //    {
        //        for (int i = 0; i < dt.Rows.Count; i++)
        //        {
        //            if (dt.Rows[i].ItemArray[8].ToString() == cust_type)
        //            {
        //                if (dt.Rows[i].ItemArray[column_no].ToString() == status_type)
        //                {

        //                    value = value + Convert.ToInt32(dt.Rows[i].ItemArray[3].ToString());
        //                }
        //            }
        //        }
        //    }
        //    return value;
        //}

        //public int total_budget_val(DataTable dt, string status_type, int column_no)
        //{
        //    value = 0;
        //    if (dt.Rows.Count > 0)
        //    {
        //        for (int i = 0; i < dt.Rows.Count; i++)
        //        {

        //            if (dt.Rows[i].ItemArray[column_no].ToString() == status_type)
        //            {

        //                value = value + Convert.ToInt32(dt.Rows[i].ItemArray[3].ToString());
        //            }

        //        }
        //    }
        //    return value;
        //}

        //protected void bm_bdgt_val_Click(object sender, EventArgs e)
        //{
        //    bindbargraph("BM");
        //    divbmbargraph.Visible = true;
        //    bm_chart.Style.Add("display", "block");
        //    // bm_charts_image.Style.Add("Src", "images/button_minus.gif");
        //    bm_charts_image.Attributes["Src"] = "images/button_minus.gif";
        //    bmbar_graphs_image.Attributes["Src"] = "images/button_minus.gif";
        //    if (ho_chart.Style.Value != "display:block;")
        //    {
        //        ho_chart.Style.Add("display", "none");
        //        ho_charts_image.Attributes["Src"] = "images/button_plus.gif";
        //        divbargraph.Visible = false;
        //        bar_graphs_image.Attributes["Src"] = "images/button_plus.gif";
        //    }
        //}
        
        //protected void ho_bdgt_val_Click(object sender, EventArgs e)
        //{
        //    bindbargraph("HO");
        //    divbargraph.Visible = true;
        //    ho_chart.Style.Add("display", "block");
        //    // ho_charts_image.Style.Add("Src", "images/button_minus.gif");
        //    ho_charts_image.Attributes["Src"] = "images/button_minus.gif";
        //    bar_graphs_image.Attributes["Src"] = "images/button_minus.gif";
        //    lbl_bdgt_text.Text = "HO STATUS-VALUE";
        //    if (bm_chart.Style.Value != "display:block;")
        //    {

        //        bm_chart.Style.Add("display", "none");
        //        bm_charts_image.Attributes["Src"] = "images/button_plus.gif";
        //        bmbar_graphs_image.Attributes["Src"] = "images/button_plus.gif";
        //        divbmbargraph.Visible = false;
        //    }
        //}

        //protected void bindbargraph(string roletype)
        //{
        //    List<string> status_type = new List<string>();
        //    //DataTable dtData = dt;
        //    dtstatus = new DataTable();
        //    int column_num = 0;
        //    if (roletype == "BM")
        //    {
        //        status_type.Add("Submitted"); status_type.Add("To be Reviewed"); status_type.Add("Sent for Review"); status_type.Add("Approved");
        //        column_num = 6;
        //        dtstatus.Columns.Add("Status");
        //        dtstatus.Columns.Add("Count");
        //        dtstatus.Columns.Add("Value");
        //        foreach (string status in status_type)
        //        {
        //            int cnt = status_cnt(dtData, status, "CUSTOMER", column_num);
        //            int cbdgt_val = budget_val(dtData, status, "CUSTOMER", column_num);
        //            dtstatus.Rows.Add(status, cnt, cbdgt_val);   //adding the status count  to dtstatus

        //        }
        //        cbmbdgt_bargraph.DataSource = dtstatus;

        //        cbmbdgt_bargraph.Series["Series1"]["DrawingStyle"] = "Cylinder";

        //        cbmbdgt_bargraph.Series["Series1"].IsValueShownAsLabel = true;
        //        cbmbdgt_bargraph.Series["Series1"].XValueMember = "Status";
        //        cbmbdgt_bargraph.Series["Series1"].YValueMembers = "Value";
        //        cbmbdgt_bargraph.DataBind();
        //        foreach (Series charts in cbmbdgt_bargraph.Series)
        //        {
        //            foreach (DataPoint point in charts.Points)
        //            {
        //                switch (point.AxisLabel)
        //                {
        //                    case "ALL": point.Color = Color.Red; break;
        //                    case "Not Initiated": point.Color = ColorTranslator.FromHtml("#EFD279"); break;
        //                    case "Pending Submission": point.Color = ColorTranslator.FromHtml("#95CBE9"); break;
        //                    case "Submitted": point.Color = ColorTranslator.FromHtml("#024769"); break;
        //                    case "To be Reviewed": point.Color = ColorTranslator.FromHtml("#AFD775"); break;
        //                    case "Sent for Review": point.Color = ColorTranslator.FromHtml("#2C5700"); break;
        //                    case "Approved": point.Color = ColorTranslator.FromHtml("#DE9D7F"); break;
        //                }
        //                //point.Label = string.Format("{0:0} - {1}", point.YValues[0], point.AxisLabel);

        //            }
        //        }
        //        //Axis properties
        //        cbmbdgt_bargraph.ChartAreas[0].AxisX.Title = "Status";
        //        cbmbdgt_bargraph.ChartAreas[0].AxisX.TitleFont = new System.Drawing.Font("Helvetica Neue", 10, System.Drawing.FontStyle.Bold);
        //        cbmbdgt_bargraph.ChartAreas[0].AxisY.Title = "Budget Value";
        //        cbmbdgt_bargraph.ChartAreas[0].AxisY.TitleFont = new System.Drawing.Font("Helvetica Neue", 10, System.Drawing.FontStyle.Bold);

        //        dtstatus.Rows.Clear();
        //        foreach (string status in status_type)
        //        {
        //            int cnt = status_cnt(dtData, status, "CHANNEL PARTNER", column_num);
        //            int cbdgt_val = budget_val(dtData, status, "CHANNEL PARTNER", column_num);
        //            dtstatus.Rows.Add(status, cnt, cbdgt_val);   //adding the status count  to dtstatus

        //        }


        //        dbmbdgt_bargraph.DataSource = dtstatus;
        //        dbmbdgt_bargraph.Series["Series1"]["DrawingStyle"] = "Cylinder";
        //        //dbmbdgt_bargraph.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
        //        dbmbdgt_bargraph.Series["Series1"].IsValueShownAsLabel = true;
        //        dbmbdgt_bargraph.Series["Series1"].XValueMember = "Status";
        //        dbmbdgt_bargraph.Series["Series1"].YValueMembers = "Value";
        //        dbmbdgt_bargraph.DataBind();
        //        foreach (Series charts in dbmbdgt_bargraph.Series)
        //        {
        //            foreach (DataPoint point in charts.Points)
        //            {
        //                switch (point.AxisLabel)
        //                {
        //                    case "ALL": point.Color = Color.Red; break;
        //                    case "Not Initiated": point.Color = ColorTranslator.FromHtml("#EFD279"); break;
        //                    case "Pending Submission": point.Color = ColorTranslator.FromHtml("#95CBE9"); break;
        //                    case "Submitted": point.Color = ColorTranslator.FromHtml("#024769"); break;
        //                    case "To be Reviewed": point.Color = ColorTranslator.FromHtml("#AFD775"); break;
        //                    case "Sent for Review": point.Color = ColorTranslator.FromHtml("#2C5700"); break;
        //                    case "Approved": point.Color = ColorTranslator.FromHtml("#DE9D7F"); break;
        //                }
        //                //point.Label = string.Format("{0:0} - {1}", point.YValues[0], point.AxisLabel);

        //            }
        //        }
        //        dbmbdgt_bargraph.ChartAreas[0].AxisX.Title = "Status";
        //        dbmbdgt_bargraph.ChartAreas[0].AxisX.TitleFont = new System.Drawing.Font("Helvetica Neue", 10, System.Drawing.FontStyle.Bold);
        //        dbmbdgt_bargraph.ChartAreas[0].AxisY.Title = "Budget Value";
        //        dbmbdgt_bargraph.ChartAreas[0].AxisY.TitleFont = new System.Drawing.Font("Helvetica Neue", 10, System.Drawing.FontStyle.Bold);
        //        dtstatus.Rows.Clear();

        //        foreach (string status in status_type)
        //        {
        //            int cnt = status_total_cnt(dtData, status, column_num);
        //            int tbdgt_val = total_budget_val(dtData, status, column_num);
        //            dtstatus.Rows.Add(status, cnt, tbdgt_val);
        //        }

        //        tbmbdgt_bargraph.DataSource = dtstatus;
        //        tbmbdgt_bargraph.Series["Series1"]["DrawingStyle"] = "Cylinder";
        //        //tbmbdgt_bargraph.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
        //        tbmbdgt_bargraph.Series["Series1"].IsValueShownAsLabel = true;
        //        tbmbdgt_bargraph.Series["Series1"].XValueMember = "Status";
        //        tbmbdgt_bargraph.Series["Series1"].YValueMembers = "Value";
        //        tbmbdgt_bargraph.DataBind();
        //        foreach (Series charts in tbmbdgt_bargraph.Series)
        //        {
        //            foreach (DataPoint point in charts.Points)
        //            {
        //                switch (point.AxisLabel)
        //                {
        //                    case "ALL": point.Color = Color.Red; break;
        //                    case "Not Initiated": point.Color = ColorTranslator.FromHtml("#EFD279"); break;
        //                    case "Pending Submission": point.Color = ColorTranslator.FromHtml("#95CBE9"); break;
        //                    case "Submitted": point.Color = ColorTranslator.FromHtml("#024769"); break;
        //                    case "To be Reviewed": point.Color = ColorTranslator.FromHtml("#AFD775"); break;
        //                    case "Sent for Review": point.Color = ColorTranslator.FromHtml("#2C5700"); break;
        //                    case "Approved": point.Color = ColorTranslator.FromHtml("#DE9D7F"); break;
        //                }
        //                //point.Label = string.Format("{0:0} - {1}", point.YValues[0], point.AxisLabel);

        //            }
        //        }
        //        tbmbdgt_bargraph.ChartAreas[0].AxisX.Title = "Status";
        //        tbmbdgt_bargraph.ChartAreas[0].AxisX.TitleFont = new System.Drawing.Font("Helvetica Neue", 10, System.Drawing.FontStyle.Bold);
        //        tbmbdgt_bargraph.ChartAreas[0].AxisY.Title = "Budget Value";
        //        tbmbdgt_bargraph.ChartAreas[0].AxisY.TitleFont = new System.Drawing.Font("Helvetica Neue", 10, System.Drawing.FontStyle.Bold);
        //        dtstatus.Rows.Clear();
        //        /////////////////////////////

        //    }
        //    else if (roletype == "HO")
        //    {
        //        status_type.Add("To be Reviewed"); status_type.Add("Approved");
        //        column_num = 7;
        //        dtstatus.Columns.Add("Status");
        //        dtstatus.Columns.Add("Count");
        //        dtstatus.Columns.Add("Value");
        //        foreach (string status in status_type)
        //        {
        //            int cnt = status_cnt(dtData, status, "CUSTOMER", column_num);
        //            int cbdgt_val = budget_val(dtData, status, "CUSTOMER", column_num);
        //            dtstatus.Rows.Add(status, cnt, cbdgt_val);   //adding the status count  to dtstatus

        //        }

        //        cbdgt_bargraph.DataSource = dtstatus;
        //        cbdgt_bargraph.Series["Series1"]["DrawingStyle"] = "Cylinder";
        //        cbdgt_bargraph.Series["Series1"].IsValueShownAsLabel = true;
        //        cbdgt_bargraph.Series["Series1"].XValueMember = "Status";
        //        cbdgt_bargraph.Series["Series1"].YValueMembers = "Value";
        //        cbdgt_bargraph.DataBind();
        //        foreach (Series charts in cbdgt_bargraph.Series)
        //        {
        //            foreach (DataPoint point in charts.Points)
        //            {
        //                switch (point.AxisLabel)
        //                {
        //                    case "ALL": point.Color = Color.Red; break;
        //                    case "Not Initiated": point.Color = ColorTranslator.FromHtml("#EFD279"); break;
        //                    case "Pending Submission": point.Color = ColorTranslator.FromHtml("#95CBE9"); break;
        //                    case "Submitted": point.Color = ColorTranslator.FromHtml("#024769"); break;
        //                    case "To be Reviewed": point.Color = ColorTranslator.FromHtml("#AFD775"); break;
        //                    case "Sent for Review": point.Color = ColorTranslator.FromHtml("#2C5700"); break;
        //                    case "Approved": point.Color = ColorTranslator.FromHtml("#DE9D7F"); break;
        //                }
        //                //point.Label = string.Format("{0:0} - {1}", point.YValues[0], point.AxisLabel);

        //            }
        //        }
        //        //Axis properties
        //        cbdgt_bargraph.ChartAreas[0].AxisX.Title = "Status";
        //        cbdgt_bargraph.ChartAreas[0].AxisX.TitleFont = new System.Drawing.Font("Helvetica Neue", 10, System.Drawing.FontStyle.Bold);
        //        cbdgt_bargraph.ChartAreas[0].AxisY.Title = "Budget Value";
        //        cbdgt_bargraph.ChartAreas[0].AxisY.TitleFont = new System.Drawing.Font("Helvetica Neue", 10, System.Drawing.FontStyle.Bold);

        //        dtstatus.Rows.Clear();
        //        foreach (string status in status_type)
        //        {
        //            int cnt = status_cnt(dtData, status, "CHANNEL PARTNER", column_num);
        //            int cbdgt_val = budget_val(dtData, status, "CHANNEL PARTNER", column_num);
        //            dtstatus.Rows.Add(status, cnt, cbdgt_val);   //adding the status count  to dtstatus

        //        }


        //        dbdgt_bargraph.DataSource = dtstatus;
        //        dbdgt_bargraph.Series["Series1"]["DrawingStyle"] = "Cylinder";
        //        dbdgt_bargraph.Series["Series1"].IsValueShownAsLabel = true;
        //        dbdgt_bargraph.Series["Series1"].XValueMember = "Status";
        //        dbdgt_bargraph.Series["Series1"].YValueMembers = "Value";
        //        dbdgt_bargraph.DataBind();
        //        foreach (Series charts in dbdgt_bargraph.Series)
        //        {
        //            foreach (DataPoint point in charts.Points)
        //            {
        //                switch (point.AxisLabel)
        //                {
        //                    case "ALL": point.Color = Color.Red; break;
        //                    case "Not Initiated": point.Color = ColorTranslator.FromHtml("#EFD279"); break;
        //                    case "Pending Submission": point.Color = ColorTranslator.FromHtml("#95CBE9"); break;
        //                    case "Submitted": point.Color = ColorTranslator.FromHtml("#024769"); break;
        //                    case "To be Reviewed": point.Color = ColorTranslator.FromHtml("#AFD775"); break;
        //                    case "Sent for Review": point.Color = ColorTranslator.FromHtml("#2C5700"); break;
        //                    case "Approved": point.Color = ColorTranslator.FromHtml("#DE9D7F"); break;
        //                }
        //                //point.Label = string.Format("{0:0} - {1}", point.YValues[0], point.AxisLabel);

        //            }
        //        }
        //        dbdgt_bargraph.ChartAreas[0].AxisX.Title = "Status";
        //        dbdgt_bargraph.ChartAreas[0].AxisX.TitleFont = new System.Drawing.Font("Helvetica Neue", 10, System.Drawing.FontStyle.Bold);
        //        dbdgt_bargraph.ChartAreas[0].AxisY.Title = "Budget Value";
        //        dbdgt_bargraph.ChartAreas[0].AxisY.TitleFont = new System.Drawing.Font("Helvetica Neue", 10, System.Drawing.FontStyle.Bold);
        //        dtstatus.Rows.Clear();

        //        foreach (string status in status_type)
        //        {
        //            int cnt = status_total_cnt(dtData, status, column_num);
        //            int tbdgt_val = total_budget_val(dtData, status, column_num);
        //            dtstatus.Rows.Add(status, cnt, tbdgt_val);
        //        }

        //        tbdgt_bargraph.DataSource = dtstatus;
        //        tbdgt_bargraph.Series["Series1"]["DrawingStyle"] = "Cylinder";
        //        tbdgt_bargraph.Series["Series1"].IsValueShownAsLabel = true;
        //        tbdgt_bargraph.Series["Series1"].XValueMember = "Status";
        //        tbdgt_bargraph.Series["Series1"].YValueMembers = "Value";
        //        tbdgt_bargraph.DataBind();
        //        foreach (Series charts in tbdgt_bargraph.Series)
        //        {
        //            foreach (DataPoint point in charts.Points)
        //            {
        //                switch (point.AxisLabel)
        //                {
        //                    case "ALL": point.Color = Color.Red; break;
        //                    case "Not Initiated": point.Color = ColorTranslator.FromHtml("#EFD279"); break;
        //                    case "Pending Submission": point.Color = ColorTranslator.FromHtml("#95CBE9"); break;
        //                    case "Submitted": point.Color = ColorTranslator.FromHtml("#024769"); break;
        //                    case "To be Reviewed": point.Color = ColorTranslator.FromHtml("#AFD775"); break;
        //                    case "Sent for Review": point.Color = ColorTranslator.FromHtml("#2C5700"); break;
        //                    case "Approved": point.Color = ColorTranslator.FromHtml("#DE9D7F"); break;
        //                }
        //                //point.Label = string.Format("{0:0} - {1}", point.YValues[0], point.AxisLabel);

        //            }
        //        }
        //        tbdgt_bargraph.ChartAreas[0].AxisX.Title = "Status";
        //        tbdgt_bargraph.ChartAreas[0].AxisX.TitleFont = new System.Drawing.Font("Helvetica Neue", 10, System.Drawing.FontStyle.Bold);
        //        tbdgt_bargraph.ChartAreas[0].AxisY.Title = "Budget Value";
        //        tbdgt_bargraph.ChartAreas[0].AxisY.TitleFont = new System.Drawing.Font("Helvetica Neue", 10, System.Drawing.FontStyle.Bold);
        //        dtstatus.Rows.Clear();

        //    }




        //}

        //public void loadpiecharts(DataTable dt)
        //{
        //    var culture = new CultureInfo("en-us", true)
        //    {
        //        NumberFormat =
        //        {
        //            NumberGroupSizes = new int[] { 2, 2 }
        //        }
        //    };
        //    DataTable dtData = dt;
        //    dtstatus = new DataTable();
        //    //SALES ENGINEER PIE CHART//
        //    se_status.Add("Not Initiated"); se_status.Add("Pending Submission"); se_status.Add("Submitted");
        //    dtstatus.Columns.Add("Status");
        //    dtstatus.Columns.Add("Count");
        //    dtstatus.Columns.Add("Value");
        //    string statusset = null;
        //    foreach (string status in se_status)
        //    {
        //        int cnt = status_cnt(dtData, status, "CUSTOMER", 5);
        //        if (status == "Pending Submission")
        //        {
        //            statusset = "Pend Subm";
        //        }
        //        else
        //        {
        //            statusset = status;
        //        }
        //        dtstatus.Rows.Add(statusset, cnt);   //adding the status count  to dtstatus

        //    }
        //    if (dtstatus.Rows.Count > 0)
        //    {
        //        for (int i = 0; i < dtstatus.Rows.Count; i++)
        //        {
        //            if (dtstatus.Rows[i].ItemArray[1].ToString() == "0")
        //            {
        //                dtstatus.Rows[i].Delete(); //deleting the rows with values containing zeroes
        //                i = -1;
        //            }
        //        }
        //    }

        //    chart_se_cust.DataSource = dtstatus;
        //    chart_se_cust.Series["Series1"]["DrawingStyle"] = "Emboss";
        //    chart_se_cust.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
        //    chart_se_cust.Series["Series1"].XValueMember = "Status";
        //    chart_se_cust.Series["Series1"].YValueMembers = "Count";
        //    chart_se_cust.DataBind();

        //    foreach (Series charts in chart_se_cust.Series)
        //    {
        //        foreach (DataPoint point in charts.Points)
        //        {
        //            switch (point.AxisLabel)
        //            {
        //                case "ALL": point.Color = Color.Red; break;
        //                case "Not Initiated": point.Color = ColorTranslator.FromHtml("#993300"); break;
        //                case "Pending Submission": point.Color = ColorTranslator.FromHtml("	#003366"); break;
        //                case "Submitted": point.Color = ColorTranslator.FromHtml("#99CC99"); break;

        //            }
        //            //point.Label = string.Format("{0:0} - {1}", point.YValues[0], point.AxisLabel);

        //        }
        //    }
        //    int bdgt_valc = budget_val(dtData, "Submitted", "CUSTOMER", 5);
        //    lbl_sec_bdgt_val.Text = "Submitted Budget Value :" + " " + bdgt_valc.ToString("N0", culture);
        //    dtstatus.Rows.Clear();//clearing the dtstatus
        //    foreach (string status in se_status)
        //    {
        //        int cnt = status_cnt(dtData, status, "CHANNEL PARTNER", 5);
        //        if (status == "Pending Submission")
        //        {
        //            statusset = "Pend Subm";
        //        }
        //        else
        //        {
        //            statusset = status;
        //        }
        //        dtstatus.Rows.Add(statusset, cnt);
        //    }
        //    if (dtstatus.Rows.Count > 0)
        //    {
        //        for (int i = 0; i < dtstatus.Rows.Count; i++)
        //        {
        //            if (dtstatus.Rows[i].ItemArray[1].ToString() == "0")
        //            {
        //                dtstatus.Rows[i].Delete();
        //                i = -1;
        //            }
        //        }
        //    }
        //    chart_se_dstrbtr.DataSource = dtstatus;
        //    chart_se_dstrbtr.Series["Series1"]["DrawingStyle"] = "Emboss";
        //    chart_se_dstrbtr.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
        //    chart_se_dstrbtr.Series["Series1"].XValueMember = "Status";
        //    chart_se_dstrbtr.Series["Series1"].YValueMembers = "Count";
        //    chart_se_dstrbtr.DataBind();
        //    foreach (Series charts in chart_se_dstrbtr.Series)
        //    {
        //        foreach (DataPoint point in charts.Points)
        //        {
        //            switch (point.AxisLabel)
        //            {
        //                case "ALL": point.Color = Color.Red; break;
        //                case "Not Initiated": point.Color = ColorTranslator.FromHtml("#993300"); break;
        //                case "Pending Submission": point.Color = ColorTranslator.FromHtml("	#003366"); break;
        //                case "Submitted": point.Color = ColorTranslator.FromHtml("#99CC99"); break;

        //            }
        //            //point.Label = string.Format("{0:0} - {1}", point.YValues[0], point.AxisLabel);

        //        }
        //    }
        //    int bdgt_vald = budget_val(dtData, "Submitted", "CHANNEL PARTNER", 5);
        //    lbl_sed_bdgt_val.Text = "Submitted Budget Value :" + " " + bdgt_vald.ToString("N0", culture); ;
        //    dtstatus.Rows.Clear();
        //    foreach (string status in se_status)
        //    {
        //        int cnt = status_total_cnt(dtData, status, 5);
        //        if (status == "Pending Submission")
        //        {
        //            statusset = "Pend Subm";
        //        }
        //        else
        //        {
        //            statusset = status;
        //        }
        //        dtstatus.Rows.Add(statusset, cnt);
        //    }
        //    if (dtstatus.Rows.Count > 0)
        //    {
        //        for (int i = 0; i < dtstatus.Rows.Count; i++)
        //        {
        //            if (dtstatus.Rows[i].ItemArray[1].ToString() == "0")
        //            {
        //                dtstatus.Rows[i].Delete();
        //                i = -1;
        //            }
        //        }
        //    }
        //    chart_se_total.DataSource = dtstatus;
        //    chart_se_total.Series["Series1"]["DrawingStyle"] = "Emboss";
        //    chart_se_total.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
        //    chart_se_total.Series["Series1"].XValueMember = "Status";
        //    chart_se_total.Series["Series1"].YValueMembers = "Count";
        //    chart_se_total.DataBind();
        //    int bdgt_valt = bdgt_valc + bdgt_vald;
        //    lbl_set_bdgt_val.Text = "Submitted Budget Value :" + " " + bdgt_valt.ToString("N0", culture); ;
        //    foreach (Series charts in chart_se_total.Series)
        //    {
        //        foreach (DataPoint point in charts.Points)
        //        {
        //            switch (point.AxisLabel)
        //            {
        //                case "ALL": point.Color = Color.Red; break;
        //                case "Not Initiated": point.Color = ColorTranslator.FromHtml("#993300"); break;
        //                case "Pending Submission": point.Color = ColorTranslator.FromHtml("	#003366"); break;
        //                case "Submitted": point.Color = ColorTranslator.FromHtml("#99CC99"); break;

        //            }
        //            //point.Label = string.Format("{0:0} - {1}", point.YValues[0], point.AxisLabel);

        //        }
        //    }
        //    //SALES ENGINEER PIE CHART END//

        //    //BRANCH MANAGER PIE CHART START//
        //    dtstatus.Rows.Clear();
        //    bm_status.Add("Not Initiated"); bm_status.Add("Pending Submission"); bm_status.Add("Submitted"); bm_status.Add("To be Reviewed"); bm_status.Add("Sent for Review"); bm_status.Add("Approved");

        //    foreach (string status in bm_status)
        //    {
        //        int cnt = status_cnt(dtData, status, "CUSTOMER", 6);
        //        if (status == "Pending Submission")
        //        {
        //            statusset = "Pend Subm";
        //        }
        //        else
        //        {
        //            statusset = status;
        //        }
        //        dtstatus.Rows.Add(statusset, cnt);
        //    }
        //    if (dtstatus.Rows.Count > 0)
        //    {
        //        for (int i = 0; i < dtstatus.Rows.Count; i++)
        //        {
        //            if (dtstatus.Rows[i].ItemArray[1].ToString() == "0")
        //            {
        //                dtstatus.Rows[i].Delete();
        //                i = -1;
        //            }
        //        }
        //    }
        //    chart_bm_cust.DataSource = dtstatus;
        //    chart_bm_cust.Series["Series1"]["DrawingStyle"] = "Emboss";
        //    chart_bm_cust.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
        //    chart_bm_cust.Series["Series1"].XValueMember = "Status";
        //    chart_bm_cust.Series["Series1"].YValueMembers = "Count";
        //    chart_bm_cust.DataBind();
        //    foreach (Series charts in chart_bm_cust.Series)
        //    {
        //        foreach (DataPoint point in charts.Points)
        //        {
        //            switch (point.AxisLabel)
        //            {
        //                case "ALL": point.Color = Color.Red; break;
        //                case "Not Initiated": point.Color = ColorTranslator.FromHtml("#EFD279"); break;
        //                case "Pending Submission": point.Color = ColorTranslator.FromHtml("#95CBE9"); break;
        //                case "Submitted": point.Color = ColorTranslator.FromHtml("#024769"); break;
        //                case "To be Reviewed": point.Color = ColorTranslator.FromHtml("#AFD775"); break;
        //                case "Sent for Review": point.Color = ColorTranslator.FromHtml("#2C5700"); break;
        //                case "Approved": point.Color = ColorTranslator.FromHtml("#DE9D7F"); break;
        //            }
        //            //point.Label = string.Format("{0:0} - {1}", point.YValues[0], point.AxisLabel);

        //        }
        //    }
        //    int bmc_bdgt_val = budget_val(dtData, "Approved", "CUSTOMER", 6);
        //    lbl_bmc_bdgt_val.Text = "Approved Budget Value :" + " " + bmc_bdgt_val.ToString("N0", culture);
        //    dtstatus.Rows.Clear();
        //    foreach (string status in bm_status)
        //    {
        //        int cnt = status_cnt(dtData, status, "CHANNEL PARTNER", 6);
        //        if (status == "Pending Submission")
        //        {
        //            statusset = "Pend Subm";
        //        }
        //        else
        //        {
        //            statusset = status;
        //        }
        //        dtstatus.Rows.Add(statusset, cnt);
        //    }
        //    if (dtstatus.Rows.Count > 0)
        //    {
        //        for (int i = 0; i < dtstatus.Rows.Count; i++)
        //        {
        //            if (dtstatus.Rows[i].ItemArray[1].ToString() == "0")
        //            {
        //                dtstatus.Rows[i].Delete();
        //                i = -1;
        //            }
        //        }
        //    }
        //    chart_bm_dstrbtr.DataSource = dtstatus;
        //    chart_bm_dstrbtr.Series["Series1"]["DrawingStyle"] = "Emboss";
        //    chart_bm_dstrbtr.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
        //    chart_bm_dstrbtr.Series["Series1"].XValueMember = "Status";
        //    chart_bm_dstrbtr.Series["Series1"].YValueMembers = "Count";
        //    chart_bm_dstrbtr.DataBind();
        //    foreach (Series charts in chart_bm_dstrbtr.Series)
        //    {
        //        foreach (DataPoint point in charts.Points)
        //        {
        //            switch (point.AxisLabel)
        //            {
        //                case "ALL": point.Color = Color.Red; break;
        //                case "Not Initiated": point.Color = ColorTranslator.FromHtml("#EFD279"); break;
        //                case "Pending Submission": point.Color = ColorTranslator.FromHtml("#95CBE9"); break;
        //                case "Submitted": point.Color = ColorTranslator.FromHtml("#024769"); break;
        //                case "To be Reviewed": point.Color = ColorTranslator.FromHtml("#AFD775"); break;
        //                case "Sent for Review": point.Color = ColorTranslator.FromHtml("#2C5700"); break;
        //                case "Approved": point.Color = ColorTranslator.FromHtml("#DE9D7F"); break;
        //            }
        //            //point.Label = string.Format("{0:0} - {1}", point.YValues[0], point.AxisLabel);

        //        }
        //    }
        //    int bmd_bdgt_val = budget_val(dtData, "Approved", "CHANNEL PARTNER", 6);
        //    lbl_bmd_bdgt_val.Text = "Approved Budget Value :" + " " + bmd_bdgt_val.ToString("N0", culture);
        //    dtstatus.Rows.Clear();
        //    foreach (string status in bm_status)
        //    {
        //        int cnt = status_total_cnt(dtData, status, 6);
        //        if (status == "Pending Submission")
        //        {
        //            statusset = "Pend Subm";
        //        }
        //        else
        //        {
        //            statusset = status;
        //        }
        //        dtstatus.Rows.Add(statusset, cnt);
        //    }
        //    if (dtstatus.Rows.Count > 0)
        //    {
        //        for (int i = 0; i < dtstatus.Rows.Count; i++)
        //        {
        //            if (dtstatus.Rows[i].ItemArray[1].ToString() == "0")
        //            {
        //                dtstatus.Rows[i].Delete();
        //                i = -1;
        //            }
        //        }
        //    }
        //    chart_bm_total.DataSource = dtstatus;
        //    chart_bm_total.Series["Series1"]["DrawingStyle"] = "Emboss";
        //    chart_bm_total.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
        //    chart_bm_total.Series["Series1"].XValueMember = "Status";
        //    chart_bm_total.Series["Series1"].YValueMembers = "Count";
        //    chart_bm_total.DataBind();
        //    int bmt_bdgt_val = bmc_bdgt_val + bmd_bdgt_val;
        //    lbl_bmt_bdgt_val.Text = "Approved Budget Value :" + " " + bmt_bdgt_val.ToString("N0", culture);
        //    foreach (Series charts in chart_bm_total.Series)
        //    {
        //        foreach (DataPoint point in charts.Points)
        //        {
        //            switch (point.AxisLabel)
        //            {
        //                case "ALL": point.Color = Color.Red; break;
        //                case "Not Initiated": point.Color = ColorTranslator.FromHtml("#EFD279"); break;
        //                case "Pending Submission": point.Color = ColorTranslator.FromHtml("#95CBE9"); break;
        //                case "Submitted": point.Color = ColorTranslator.FromHtml("#024769"); break;
        //                case "To be Reviewed": point.Color = ColorTranslator.FromHtml("#AFD775"); break;
        //                case "Sent for Review": point.Color = ColorTranslator.FromHtml("#2C5700"); break;
        //                case "Approved": point.Color = ColorTranslator.FromHtml("#DE9D7F"); break;
        //            }
        //            //point.Label = string.Format("{0:0} - {1}", point.YValues[0], point.AxisLabel);

        //        }
        //    }
        //    //BRANCH MANAGER PIE CHART END HERE//

        //    // //HO CHART START//
        //    dtstatus.Rows.Clear();
        //    ho_status.Add("To be Reviewed"); ho_status.Add("Approved");

        //    foreach (string status in ho_status)
        //    {
        //        int cnt = status_cnt(dtData, status, "CUSTOMER", 7);
        //        dtstatus.Rows.Add(status, cnt);
        //    }
        //    if (dtstatus.Rows.Count > 0)
        //    {
        //        for (int i = 0; i < dtstatus.Rows.Count; i++)
        //        {
        //            if (dtstatus.Rows[i].ItemArray[1].ToString() == "0")
        //            {
        //                dtstatus.Rows[i].Delete();
        //                i = -1;
        //            }
        //        }
        //    }
        //    chart_ho_cust.DataSource = dtstatus;
        //    chart_ho_cust.Series["Series1"]["DrawingStyle"] = "Emboss";
        //    chart_ho_cust.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
        //    chart_ho_cust.Series["Series1"].XValueMember = "Status";
        //    chart_ho_cust.Series["Series1"].YValueMembers = "Count";

        //    chart_ho_cust.DataBind();
        //    foreach (Series charts in chart_ho_cust.Series)
        //    {
        //        foreach (DataPoint point in charts.Points)
        //        {
        //            switch (point.AxisLabel)
        //            {
        //                case "ALL": point.Color = Color.Red; break;
        //                case "To be Reviewed": point.Color = ColorTranslator.FromHtml("#CC9933"); break;
        //                case "Approved": point.Color = ColorTranslator.FromHtml("#336699"); break;

        //            }
        //            //point.Label = string.Format("{0:0} - {1}", point.YValues[0], point.AxisLabel);

        //        }
        //    }
        //    int hoc_bdgt_val = budget_val(dtData, "Approved", "CUSTOMER", 7);
        //    lbl_hoc_bdgt_val.Text = "Approved Budget Value :" + " " + hoc_bdgt_val.ToString("N0", culture);
        //    dtstatus.Rows.Clear();
        //    foreach (string status in ho_status)
        //    {
        //        int cnt = status_cnt(dtData, status, "CHANNEL PARTNER", 7);
        //        dtstatus.Rows.Add(status, cnt);
        //    }
        //    if (dtstatus.Rows.Count > 0)
        //    {
        //        for (int i = 0; i < dtstatus.Rows.Count; i++)
        //        {
        //            if (dtstatus.Rows[i].ItemArray[1].ToString() == "0")
        //            {
        //                dtstatus.Rows[i].Delete();
        //                i = -1;
        //            }
        //        }
        //    }
        //    chart_ho_dstrbtr.DataSource = dtstatus;
        //    chart_ho_dstrbtr.Series["Series1"]["DrawingStyle"] = "Emboss";
        //    chart_ho_dstrbtr.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
        //    chart_ho_dstrbtr.Series["Series1"].XValueMember = "Status";
        //    chart_ho_dstrbtr.Series["Series1"].YValueMembers = "Count";
        //    chart_ho_dstrbtr.DataBind();
        //    foreach (Series charts in chart_ho_dstrbtr.Series)
        //    {
        //        foreach (DataPoint point in charts.Points)
        //        {
        //            switch (point.AxisLabel)
        //            {
        //                case "ALL": point.Color = Color.Red; break;
        //                case "To be Reviewed": point.Color = ColorTranslator.FromHtml("#CC9933"); break;
        //                case "Approved": point.Color = ColorTranslator.FromHtml("#336699"); break;
        //            }
        //            //point.Label = string.Format("{0:0} - {1}", point.YValues[0], point.AxisLabel);

        //        }
        //    }
        //    int hod_bdgt_val = budget_val(dtData, "Approved", "CHANNEL PARTNER", 7);
        //    lbl_hod_bdgt_val.Text = "Approved Budget Value :" + " " + hod_bdgt_val.ToString("N0", culture);
        //    dtstatus.Rows.Clear();
        //    foreach (string status in ho_status)
        //    {
        //        int cnt = status_total_cnt(dtData, status, 7);
        //        dtstatus.Rows.Add(status, cnt);
        //    }
        //    if (dtstatus.Rows.Count > 0)
        //    {
        //        for (int i = 0; i < dtstatus.Rows.Count; i++)
        //        {
        //            if (dtstatus.Rows[i].ItemArray[1].ToString() == "0")
        //            {
        //                dtstatus.Rows[i].Delete();
        //                i = -1;
        //            }
        //        }
        //    }
        //    // dtstatus.AcceptChanges();
        //    chart_ho_total.DataSource = dtstatus;

        //    //chart_ho_total.Series["Series1"].ChartType = SeriesChartType.Column;
        //    chart_ho_total.Series["Series1"]["DrawingStyle"] = "Emboss";
        //    chart_ho_total.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = true;
        //    //chart_ho_total.Series["Series1"].IsValueShownAsLabel = true;
        //    chart_ho_total.Series["Series1"].XValueMember = "Status";
        //    chart_ho_total.Series["Series1"].YValueMembers = "Count";
        //    chart_ho_total.DataBind();
        //    int hot_bdgt_val = hoc_bdgt_val + hod_bdgt_val;
        //    lbl_hot_bdgt_val.Text = "Approved Budget Value :" + " " + hot_bdgt_val.ToString("N0", culture);
        //    foreach (Series charts in chart_ho_total.Series)
        //    {
        //        foreach (DataPoint point in charts.Points)
        //        {
        //            switch (point.AxisLabel)
        //            {
        //                case "ALL": point.Color = Color.Red; break;
        //                case "To be Reviewed": point.Color = ColorTranslator.FromHtml("#CC9933"); break;
        //                case "Approved": point.Color = ColorTranslator.FromHtml("#336699"); break;
        //            }
        //            //point.Label = string.Format("{0:0} - {1}", point.YValues[0], point.AxisLabel);

        //        }
        //    }
        //    //HO CHART END//
        //}

        #endregion
    }
}