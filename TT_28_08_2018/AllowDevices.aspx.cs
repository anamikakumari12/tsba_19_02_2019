﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TaegutecSalesBudget
{
    public partial class AllowDevices : System.Web.UI.Page
    {

        #region GlobalDeclareation
        AdminConfiguration objAdmin = new AdminConfiguration();
        CommonFunctions objFunc = new CommonFunctions();
        AllowDevicesBOL objDev = new AllowDevicesBOL();
        #endregion

        #region Events
        /// <summary>
        /// Author     : Neha
        /// Date       :22nd jan, 2019
        /// Description: Based on user role we are loading data
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            if (!IsPostBack)
            {


                DataTable dtData = new DataTable();
                dtData = objAdmin.devicedetails();
                Session["DtDeviceDetails"] = dtData;
                if (dtData.Rows.Count != 0)
                {
                    grdallowdevices.DataSource = dtData;
                    grdallowdevices.DataBind();
                }
                else
                {
                    grdallowdevices.DataSource = null;
                    grdallowdevices.DataBind();
                }

            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "submit();", true);
        }


        /// <summary>
        /// Author     :Neha
        /// Date       : 23rd jan, 2019
        /// Description: For loading updated field
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditAdd_Click(object sender, EventArgs e)
        {
            DataTable dtdata = new DataTable();
            int timeperiod = 0;
            bool isApproved = false;
            string msg = null;
            string userEmail = Convert.ToString(Session["UserEmailId"]);
            //Dictionary<string, object> l = new Dictionary<string, object>();
            //l.Add("userEmail", userEmail);
            //l.Add("defaultdevice1", defaultdevice1.Text);
            //l.Add("defaultdevice2", defaultdevice2.Text);
            //l.Add("Requesteddevice", txtrequestdevice.Text);
            //if (rdbtnapprove.Checked)
            //{
            //    if (Convert.ToInt32(txtimeperiod.Text) > 0)
            //    {

            //        l.Add("approvedstatus", 1);
            //    }
            //    else
            //    {
            //        l.Add("approvedstatus", 0);
            //    }
            //}
            //else
            //{
            //    l.Add("approvedstatus", 0);
            //}

            //isApproved = int.TryParse(txtimeperiod.Text, out timeperiod);

            //if (isApproved)
            //{
            //    l.Add("timeperiod", txtimeperiod.Text);
            //}
            //else
            //{
            //    l.Add("timeperiod", timeperiod);
            //}
            objDev.EmailID = userEmail;
            objDev.DefaultDevice1 = Convert.ToString(defaultdevice1.Text);
            objDev.DefaultDevice2 = Convert.ToString(defaultdevice2.Text);
            objDev.RequestedDevice = Convert.ToString(txtrequestdevice.Text);
            if (rdbtnapprove.Checked)
            {
                if (Convert.ToInt32(txtimeperiod.Text) > 0)
                {
                    objDev.Status = 1;
                }
                else
                {
                    objDev.Status = 0;
                }
            }
            else
            {
                objDev.Status = 0;
            }

            isApproved = int.TryParse(txtimeperiod.Text, out timeperiod);

            if (isApproved)
            {
                objDev.TimePeriod = Convert.ToInt32(txtimeperiod.Text);
            }
            else
            {
                objDev.TimePeriod = timeperiod;
            }
            if (!(objDev.Status > 0 && objDev.TimePeriod > 0))
                msg = "There is no device requested for approval.";
            else
                msg = objAdmin.EditDeviceDetails(objDev);
            dtdata = objAdmin.devicedetails();
            if (dtdata.Rows.Count > 0)
            {
                grdallowdevices.DataSource = dtdata;
                grdallowdevices.DataBind();
            }
            Session["DtDeviceDetails"] = dtdata;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup2", "alert('" + msg + "');", true);

        }

        /// <summary>
        /// Author     :Neha
        /// Date       :23rd jan,2019
        /// Description: To access particular gridview's row
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void EditUser_Click(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string UserEmailId = Convert.ToString(e.CommandArgument);
                Session["UserEmailId"] = UserEmailId;
                DataRow drselect = selectedRow(UserEmailId);
                LoadEditPanel(drselect);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopups", "edit();", true);

            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        #endregion

        #region Methods

        /// <summary>
        /// Author     :Neha
        /// Date       :23rd jan, 2019
        /// Description: For loading gridview data in popup
        /// </summary>
        /// <param name="drselect"></param>

        public void LoadEditPanel(DataRow drselect)
        {
            txtusernamename.Text = Convert.ToString(drselect["EngineerName"]);
            txtmaild.Text = Convert.ToString(drselect["EmailId"]);
            txtroleid.Text = Convert.ToString(drselect["RoleId"]);
            defaultdevice1.Text = Convert.ToString(drselect["defaultDevice1"]);
            defaultdevice2.Text = Convert.ToString(drselect["defaultDevice2"]);
            txtrequestdevice.Text = Convert.ToString(drselect["RequestedDevice"]);
            txtimeperiod.Text = Convert.ToString(drselect["noofdays"]);
            if (Convert.ToString(drselect["statusofdevice"]) == "pending for approval")
            {
                rdbtnapprove.Checked = false;
                rdbtncancel.Checked = true;
            }

            else if (Convert.ToString(drselect["statusofdevice"]) == "Approved")
            {
                rdbtnapprove.Checked = true;
                rdbtncancel.Checked = false;
            }
            else if (Convert.ToString(drselect["statusofdevice"]) == "No Requested Device")
            {
                rdbtnapprove.Checked = false;
                rdbtncancel.Checked = true;

            }

            // rdbtncancel.Checked = false;
            //  txtimeperiod.Text = null;
        }


        private DataRow selectedRow(string UserEmailId)
        {
            DataTable dtGrid = (DataTable)Session["DtDeviceDetails"];
            DataRow drselect = (from DataRow dr in dtGrid.Rows
                                where (string)dr["EmailId"] == UserEmailId
                                select dr).FirstOrDefault();

            return drselect;
        }
        #endregion
    }
}
