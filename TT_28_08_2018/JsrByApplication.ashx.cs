﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;

namespace TaegutecSalesBudget
{
    /// <summary>
    /// Summary description for JsrByApplication
    /// </summary>
    public class JsrByApplication : IHttpHandler
    {

        csJSRDAL objJSRDAL = new csJSRDAL();
        csJSR objJSR = new csJSR();

        public void ProcessRequest(HttpContext context)
        {
            try
            {

                //Get data from database here and convert the data in the SOL data format. I created couple of method below to convert data from database to json
                //Method 1 DataTableToJsonObj to convert datatable to json format
                //Method 2 DataTbaleToJson to convert a list object to json using c# json serializer
                //You can use nay method u like
                //I am using test data as string as I don't have database
                //StringBuilder JsonString = new StringBuilder();
                //JsonString.Append("[");
                //JsonString.Append("{");
                //JsonString.Append("\"" + "type" + "\":" + "\"" + "option" + "\"," + "\"" + "value" + "\":" + "\"" + "Evil Monkey" + "\"," + "\"" + "label" + "\":" + "\"" + "Evil Monkey" + "\"" + "},");
                //JsonString.Append("{");
                //JsonString.Append("\"" + "type" + "\":" + "\"" + "option" + "\"," + "\"" + "value" + "\":" + "\"" + "Herbert" + "\"," + "\"" + "label" + "\":" + "\"" + "Herbert" + "\"" + "}");
                //JsonString.Append("]");

                //string solData = JsonString.ToString();             

             
                DataTable dtData = new DataTable();
                objJSR.branch = "H_200"; // passing here territory Engineer Id  as branch code IF role is TM 
                objJSR.roleId = "HO";
                objJSR.flag = "Branch";
                objJSR.cter = "TTA";

                dtData = objJSRDAL.getFilterAreaValue(objJSR);


                DataTableToJsonObj(dtData);

                //JavaScriptSerializer js = new JavaScriptSerializer();
                //context.Response.Write(js.Serialize(dtData));

                //context.Response.ContentType = "text/plain";
                //context.Response.Write(js);
            }
            catch (Exception ex)
            {
                context.Response.ContentType = "text/plain";
                context.Response.Write("Handler Error" + ex.Message);
            }

        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }


        /// <summary>
        /// TO convert data from dataset to JSON format
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public string DataTableToJsonObj(DataTable dt)
        {
            //Make the database call and get data as Dataset
            DataSet ds = new DataSet();
            ds.Merge(dt);
            StringBuilder JsonString = new StringBuilder();
            if (ds != null && ds.Tables[0].Rows.Count > 0)
            {
                JsonString.Append("[");
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    JsonString.Append("{");
                    for (int j = 0; j < ds.Tables[0].Columns.Count; j++)
                    {
                        if (j < ds.Tables[0].Columns.Count - 1)
                        {
                            JsonString.Append("\"" + ds.Tables[0].Columns[j].ColumnName.ToString() + "\":" + "\"" + ds.Tables[0].Rows[i][j].ToString() + "\",");
                        }
                        else if (j == ds.Tables[0].Columns.Count - 1)
                        {
                            JsonString.Append("\"" + ds.Tables[0].Columns[j].ColumnName.ToString() + "\":" + "\"" + ds.Tables[0].Rows[i][j].ToString() + "\"");
                        }
                    }
                    if (i == ds.Tables[0].Rows.Count - 1)
                    {
                        JsonString.Append("}");
                    }
                    else
                    {
                        JsonString.Append("},");
                    }
                }
                JsonString.Append("]");
                return JsonString.ToString();
            }
            else { return null; }

        }

        //Another method to get data
        public string DataTbaleToJson()
        {
            //Make the database call and get data
            //Convert the data into a dicitonary wiht key values or List object
            //Create a class of select option type wiht all properties and 
            //Convert the list object to JSOn as below
            String jsonString;
            System.Web.Script.Serialization.JavaScriptSerializer jsonserializer = new System.Web.Script.Serialization.JavaScriptSerializer();

            jsonString = jsonserializer.Serialize("ListObject");

            return jsonString;

        }
    }
}