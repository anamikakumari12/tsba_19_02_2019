﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReportsDashboard.aspx.cs" Inherits="TaegutecSalesBudget.ReportsDashboard" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <%--<script src="js/jquery.dataTables.min.js"></script>--%>
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <link href="css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="css/buttons.dataTables.min.css" rel="stylesheet" />
    <script src="js/common/jquery.sumoselect.min.js"></script>
    <link href="css/Common/sumoselect.css" rel="stylesheet" />
    <%-- <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
<script src="https://www.amcharts.com/lib/3/serial.js"></script>
<script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
<link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
<script src="https://www.amcharts.com/lib/3/themes/light.js"></script>--%>



    <script src="https://www.amcharts.com/lib/3/amcharts.js"></script>
    <script src="https://www.amcharts.com/lib/3/funnel.js"></script>
    <script src="https://www.amcharts.com/lib/3/gauge.js"></script>
    <script src="https://www.amcharts.com/lib/3/radar.js"></script>
    <script src="https://www.amcharts.com/lib/3/serial.js"></script>
    <script src="https://www.amcharts.com/lib/3/pie.js"></script>
    <script src="https://www.amcharts.com/lib/3/plugins/dataloader/dataloader.min.js"></script>
    <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
    <link rel="stylesheet" href="https://www.amcharts.com/lib/3/plugins/export/export.css" type="text/css" media="all" />
    <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
    <link rel="stylesheet" type="text/css" href="https://dc-js.github.io/dc.js/css/dc.css" />
    <script src="https://dc-js.github.io/dc.js/js/d3.js"></script>
    <script src="https://dc-js.github.io/dc.js/js/crossfilter.js"></script>
    <script src="https://dc-js.github.io/dc.js/js/dc.js"></script>
    <script src="https://rawgit.com/crossfilter/reductio/master/reductio.js"></script>
    <script src="https://npmcdn.com/universe@latest/universe.js"></script>



    <script type="text/javascript">
        $(document).ready(function () {
            bindBranch();
            bindSE();
            bindCustomer();
            bindCP();
            bindGold();
            bindFamily();
            $(<%=BranchList.ClientID%>).SumoSelect({ selectAll: true, search: true, okCancelInMulti: true });
        });
        function LoadCharts() {
            debugger;
            var hdnsearch = $('#MainContent_hdnsearch').val();
            var hdnGold = $('#MainContent_hdnGold').val();
            console.log("HDN : " + hdnsearch);
            LoadChartConsolidated();
            if (hdnsearch == "ALL" || hdnsearch == "BRANCH" || hdnsearch=="GOLD") {
                LoadChartBranchAch();
                LoadChartBranchGrowth();
                document.getElementById("ChartBranchAch").setAttribute("style", "display:block;");
                document.getElementById("ChartBranchGrowth").setAttribute("style", "display:block;");
            }
            else if (hdnsearch == "SE") {
                console.log("SE");
                document.getElementById("ChartBranchAch").setAttribute("style", "display:none;");
                document.getElementById("ChartBranchGrowth").setAttribute("style", "display:none;");
                document.getElementById("ChartTopSEAch").setAttribute("style", "display:block;");
                document.getElementById("ChartTopSEGrowth").setAttribute("style", "display:block;");
                document.getElementById("ChartBottomSEAch").setAttribute("style", "display:block;");
                document.getElementById("ChartBottomSEGrowth").setAttribute("style", "display:block;");
                document.getElementById("ChartFamAch").setAttribute("style", "display:none;");
                document.getElementById("ChartFamGrowth").setAttribute("style", "display:none;");
                LoadChartTopSEAch();
                LoadChartTopSEGrowth();
                LoadChartBottomSEAch();
                LoadChartBottomSEGrowth();
            }
            else if (hdnsearch == "CUST") {
                console.log("CUST");
                document.getElementById("ChartBranchAch").setAttribute("style", "display:none;");
                document.getElementById("ChartBranchGrowth").setAttribute("style", "display:none;");
                document.getElementById("ChartTopSEAch").setAttribute("style", "display:none;");
                document.getElementById("ChartTopSEGrowth").setAttribute("style", "display:none;");
                document.getElementById("ChartBottomSEAch").setAttribute("style", "display:none;");
                document.getElementById("ChartBottomSEGrowth").setAttribute("style", "display:none;");
                document.getElementById("ChartTopCustAch").setAttribute("style", "display:block;");
                document.getElementById("ChartTopCustGrowth").setAttribute("style", "display:block;");
                document.getElementById("ChartBottomCustAch").setAttribute("style", "display:block;");
                document.getElementById("ChartBottomCustGrowth").setAttribute("style", "display:block;");
                document.getElementById("ChartTopCPAch").setAttribute("style", "display:none;");
                document.getElementById("ChartTopCPGrowth").setAttribute("style", "display:none;");
                document.getElementById("ChartBottomCPAch").setAttribute("style", "display:none;");
                document.getElementById("ChartBottomCPGrowth").setAttribute("style", "display:none;");
                document.getElementById("ChartFamAch").setAttribute("style", "display:none;");
                document.getElementById("ChartFamGrowth").setAttribute("style", "display:none;");
                LoadChartTopCustAch();
                LoadChartTopCustGrowth();
                LoadChartBottomCustAch();
                LoadChartBottomCustGrowth();
            }
            else if (hdnsearch == "CP") {
                console.log("CP");
                document.getElementById("ChartBranchAch").setAttribute("style", "display:none;");
                document.getElementById("ChartBranchGrowth").setAttribute("style", "display:none;");
                document.getElementById("ChartTopSEAch").setAttribute("style", "display:none;");
                document.getElementById("ChartTopSEGrowth").setAttribute("style", "display:none;");
                document.getElementById("ChartBottomSEAch").setAttribute("style", "display:none;");
                document.getElementById("ChartBottomSEGrowth").setAttribute("style", "display:none;");
                document.getElementById("ChartTopCustAch").setAttribute("style", "display:none;");
                document.getElementById("ChartTopCustGrowth").setAttribute("style", "display:none;");
                document.getElementById("ChartBottomCustAch").setAttribute("style", "display:none;");
                document.getElementById("ChartBottomCustGrowth").setAttribute("style", "display:none;");
                document.getElementById("ChartTopCPAch").setAttribute("style", "display:block;");
                document.getElementById("ChartTopCPGrowth").setAttribute("style", "display:block;");
                document.getElementById("ChartBottomCPAch").setAttribute("style", "display:block;");
                document.getElementById("ChartBottomCPGrowth").setAttribute("style", "display:block;");

                document.getElementById("ChartFamAch").setAttribute("style", "display:none;");
                document.getElementById("ChartFamGrowth").setAttribute("style", "display:none;");
                LoadChartTopCPAch();
                LoadChartTopCPGrowth();
                LoadChartBottomCPAch();
                LoadChartBottomCPGrowth();
            }
            else if (hdnsearch == "FAMILY") {
                console.log("FAMILY");
                document.getElementById("ChartBranchAch").setAttribute("style", "display:none;");
                document.getElementById("ChartBranchGrowth").setAttribute("style", "display:none;");
                document.getElementById("ChartTopSEAch").setAttribute("style", "display:none;");
                document.getElementById("ChartTopSEGrowth").setAttribute("style", "display:none;");
                document.getElementById("ChartBottomSEAch").setAttribute("style", "display:none;");
                document.getElementById("ChartBottomSEGrowth").setAttribute("style", "display:none;");
                document.getElementById("ChartTopCustAch").setAttribute("style", "display:none;");
                document.getElementById("ChartTopCustGrowth").setAttribute("style", "display:none;");
                document.getElementById("ChartBottomCustAch").setAttribute("style", "display:none;");
                document.getElementById("ChartBottomCustGrowth").setAttribute("style", "display:none;");
                document.getElementById("ChartTopCPAch").setAttribute("style", "display:none;");
                document.getElementById("ChartTopCPGrowth").setAttribute("style", "display:none;");
                document.getElementById("ChartBottomCPAch").setAttribute("style", "display:none;");
                document.getElementById("ChartBottomCPGrowth").setAttribute("style", "display:none;");
                document.getElementById("ChartFamAch").setAttribute("style", "display:block;");
                document.getElementById("ChartFamGrowth").setAttribute("style", "display:block;");
                LoadChartFamAch();
                LoadChartFamGrowth();
            }
            if (hdnsearch == "GOLD")
            {
                if (hdnGold == "SE") {
                    document.getElementById("ChartBranchAch").setAttribute("style", "display:none;");
                    document.getElementById("ChartBranchGrowth").setAttribute("style", "display:none;");
                    document.getElementById("ChartTopSEAch").setAttribute("style", "display:none;");
                    document.getElementById("ChartTopSEGrowth").setAttribute("style", "display:none;");
                }
                else if (hdnGold == "BM") {
                    document.getElementById("ChartBranchAch").setAttribute("style", "display:none;");
                    document.getElementById("ChartBranchGrowth").setAttribute("style", "display:none;");
                }

                LoadChartGoldTopSEAch();
                LoadChartGoldTopSEGrowth();
                LoadChartGoldBottomSEAch();
                LoadChartGoldBottomSEGrowth();
                LoadChartGoldTopCustAch();
                LoadChartGoldTopCustGrowth();
                LoadChartGoldTopCPAch();
                LoadChartGoldTopCPGrowth();
            }

        }
        function LoadChartConsolidated() {
            console.log("LoadChartConsolidated");
            var tmp = null;
            var year = $('#MainContent_hdnYear').val();
            $.ajax({
                type: "POST",
                //url: 'ReportDashboard.aspx/LoadChartConsolidated',
                url: "ReportsDashboard.aspx/LoadChartConsolidated",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    console.log("msg : " + msg);
                    tmp = msg.d;
                    console.log("tmp1 : " + tmp);
                    tmp = AmCharts.parseJSON(tmp);
                    console.log("tmp2 : " + tmp);
                    var d = new Date();
                    var n = d.getFullYear();
                    var chart = AmCharts.makeChart("ChartConsolidated", {
                        "theme": "light",
                        "type": "gauge",
                        "titles": [{
                            "text": "CONSOLIDATED VIEW"
                        }],
                        "axes": [{
                            "axisAlpha": 0,
                            "tickAlpha": 0,
                            "labelsEnabled": false,
                            "startValue": 0,
                            "endValue": 100,
                            "startAngle": 0,
                            "endAngle": 270,
                            "bands": tmp,
                        }],
                        "allLabels": [{
                            "text": "SALE "+(parseInt(year)-1),
                            "x": "49%",
                            "y": "16%",
                            "size": 12,
                            "bold": true,
                            "color": "#097054",
                            "align": "right"
                        }, {
                            "text": "BUDGET " + year,
                            "x": "49%",
                            "y": "23%",
                            "size": 12,
                            "bold": true,
                            "color": "#FFDE00",
                            "align": "right"
                        }, {
                            "text": "YTD PLAN " + year,
                            "x": "49%",
                            "y": "31%",
                            "size": 12,
                            "bold": true,
                            "color": "#6599FF",
                            "align": "right"
                        }, {
                            "text": "YTD SALE " + year,
                            "x": "49%",
                            "y": "39%",
                            "size": 12,
                            "bold": true,
                            "color": "#FF9900",
                            "align": "right"
                        }],
                        //"startDuration": 2,
                        //"gridAboveGraphs": true,
                        "dataProvider": tmp,
                        "export": {
                            "enabled": false
                        }
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }
        function LoadChartBranch() {
            console.log("LoadChartBranch");
            var tmp = null;
            $.ajax({
                type: "POST",
                //url: 'ReportDashboard.aspx/LoadChartConsolidated',
                url: "ReportsDashboard.aspx/LoadChartBranch",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    console.log("msg : " + msg);
                    tmp = msg.d;
                    console.log("tmp2 : " + tmp);
                    var d = new Date();
                    var n = d.getFullYear();
                    var chart = AmCharts.makeChart("ChartBranch", {
                        "theme": "light",
                        "type": "serial",
                        "titles": [{
                            "text": "BRANCH PERFORMANCE"
                        }],
                        "startDuration": 1,
                        "gridAboveGraphs": true,
                        "dataProvider": tmp,
                        "valueAxes": [{
                            "gridColor": "#FFFFFF",
                            "gridAlpha": 0,
                            "dashLength": 0
                        }],
                        "graphs": [{
                            "type": "column",
                            "title": "PRO-RATA ACH%",
                            "balloonText": "[[title]]: <b>[[value]]</b>",
                            "balloonAlpha": 1,
                            "bullet": "round",
                            "bulletSize": 10,
                            "bulletBorderColor": "#ffffff",
                            "fillAlphas": 1,
                            "lineAlpha": 0.2,
                            "fillColorsField": "color",
                            "bulletBorderAlpha": 1,
                            "bulletBorderThickness": 2,
                            "labelText": "[[value]]%",
                            "labelPosition": "left",
                            "valueField": "ach",
                            "balloonPosition": "bottom"
                        },

                         {
                             "fillAlphas": 0.4,
                             "title": "GROWTH%",
                             "balloonText": "[[title]]: <b>[[value]]</b>",
                             "bullet": "round",
                             "bulletSize": 10,
                             "bulletBorderColor": "#ffffff",
                             "bulletBorderAlpha": 1,
                             "bulletBorderThickness": 2,
                             "valueField": "growth",
                             "selectedBackgroundAlpha": 0.1,
                             "selectedBackgroundColor": "#888888",
                             "graphFillAlpha": 0,
                             "graphLineAlpha": 0.5,
                             "selectedGraphFillAlpha": 0,
                             "selectedGraphLineAlpha": 1
                         }],

                        //"chartCursor": {
                        //    "categoryBalloonEnabled": false,
                        //    "cursorAlpha": 0,
                        //    "zoomable": false
                        //},
                        "depth3D": 40,
                        "angle": 30,
                        "categoryField": "Name",
                        "categoryAxis": {
                            "gridPosition": "start",
                            "axisAlpha": 0,
                            "gridAlpha": 0,
                            "labelRotation": 60
                        },
                        "export": {
                            "enabled": false
                        },
                        "legends": {
                            "position": "absolute",
                            //"top": "30px",
                            //"left": 600,
                            "useGraphSettings": true
                        }
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }
        function LoadChartBranchAch() {
            console.log("LoadChartBranchAch");
            var tmp = null;
            $.ajax({
                type: "POST",
                //url: 'ReportDashboard.aspx/LoadChartConsolidated',
                url: "ReportsDashboard.aspx/LoadChartBranchAch",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    console.log("msg : " + msg);
                    tmp = msg.d;
                    console.log("tmp1 : " + tmp);
                    var d = new Date();
                    var n = d.getFullYear();
                    var chart = AmCharts.makeChart("ChartBranchAch", {
                        "theme": "light",
                        "type": "serial",
                        "titles": [{
                            "text": "BRANCH PERFORMANCE"
                        }, {
                            "text": "(PRO-RATA ACH%)",
                            "bold": false,
                            "size": 10
                        }],
                        "startDuration": 1,
                        "dataProvider": tmp,
                        "valueAxes": [{
                            "position": "left",
                            "axisAlpha": 0,
                            "gridAlpha": 0
                        }],
                        "graphs": [{
                            "balloonText": "[[category]]: <b>[[value]]</b>",
                            "colorField": "color",
                            "fillAlphas": 0.85,
                            "lineAlpha": 0.1,
                            "type": "column",
                            //"topRadius": 1,
                            "topRadius": 0.8,
                            "valueField": "ach"
                        }],
                        "depth3D": 40,
                        "angle": 30,
                        "chartCursor": {
                            "categoryBalloonEnabled": false,
                            "cursorAlpha": 0,
                            "zoomable": false
                        },
                        "categoryField": "Name",
                        "categoryAxis": {
                            "gridPosition": "start",
                           
                            "gridAlpha": 0,
                            "labelRotation": 60,
                            "minHorizontalGap": 0
                        },
                        "export": {
                            "enabled": false
                        }
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }
        function LoadChartBranchGrowth() {
            console.log("LoadChartBranchGrowth");
            var tmp = null;
            $.ajax({
                type: "POST",
                //url: 'ReportDashboard.aspx/LoadChartConsolidated',
                url: "ReportsDashboard.aspx/LoadChartBranchGrowth",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    console.log("msg : " + msg);
                    tmp = msg.d;
                    console.log("tmp1 : " + tmp);
                    var d = new Date();
                    var n = d.getFullYear();
                    var chart = AmCharts.makeChart("ChartBranchGrowth", {
                        "theme": "light",
                        "type": "serial",
                        "titles": [{
                            "text": "BRANCH PERFORMANCE"
                        }, {
                            "text": "(GROWTH%)",
                            "bold": false,
                            "size": 10
                        }],
                        "startDuration": 2,
                        "dataProvider": tmp,
                        "valueAxes": [{
                            "position": "left",
                            "axisAlpha": 0,
                            "gridAlpha": 0
                        }],
                        "graphs": [{
                            "balloonText": "[[category]]: <b>[[value]]</b>",
                            "colorField": "color",
                            "fillAlphas": 0.85,
                            "lineAlpha": 0.1,
                            "type": "column",
                            //"topRadius": 1,
                            "topRadius": 0.8,
                            "valueField": "growth"
                        }],
                        "depth3D": 40,
                        "angle": 30,
                        "chartCursor": {
                            "categoryBalloonEnabled": false,
                            "cursorAlpha": 0,
                            "zoomable": false
                        },
                        "categoryField": "Name",
                        "categoryAxis": {
                            "gridPosition": "start",
                            "axisAlpha": 0,
                            "gridAlpha": 0,
                            "labelRotation": 60,
                            "minHorizontalGap": 0
                        },
                        "export": {
                            "enabled": false
                        }
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }
        function LoadChartTopSEAch() {
            console.log("LoadChartTopSEAch");
            var tmp = null;
            $.ajax({
                type: "POST",
                url: "ReportsDashboard.aspx/LoadChartTopSEAch",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    tmp = msg.d;
                    console.log("tmp : " + tmp);
                    var chart = AmCharts.makeChart("ChartTopSEAch", {
                        "type": "funnel",
                        "theme": "light",
                        "titles": [{
                            "text": "TOP 5 SALES ENGINEERS"
                        }, {
                            "text": "(PRO-RATA ACH%)",
                            "bold": false,
                            "size": 10
                        }],
                        "dataProvider": tmp ,
                            //"[{\"Name\":\"ABHISHEK\",\"ach\":\"91\"},{\"Name\":\"CHANDI PRASAD\",\"ach\":\"91\"},{\"Name\":\"SUNITA CHAVAN\",\"ach\":\"92\"},{\"Name\":\"SIDDHARTH\",\"ach\":\"92\"},{\"Name\":\"RAMESH\",\"ach\":\"92\"}]",
                        "balloon": {
                            "fixedPosition": true
                        },
                        "valueField": "ach1",
                        "titleField": "Name",
                        "marginRight": 10,
                        "marginLeft": 10,
                        "startX": -500,
                        "depth3D": 50,
                        "angle": 40,
                        "labelPosition": "center",
                        "labelText": "[[Name]]: [[ach]]%",
                        "balloonText": "[[Name]]: [[ach]]%",
                        "export": {
                            "enabled": false
                        }
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    console.log(AmCharts.parseJSON(tmp));
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }
        function LoadChartTopSEGrowth() {
            console.log("LoadChartTopSEGrowth");
            var tmp = null;
            $.ajax({
                type: "POST",
                url: "ReportsDashboard.aspx/LoadChartTopSEGrowth",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    tmp = msg.d;
                    var chart = AmCharts.makeChart("ChartTopSEGrowth", {
                        "type": "funnel",
                        "theme": "light",
                        "titles": [{
                            "text": "TOP 5 SALES ENGINEERS"
                        }, {
                            "text": "(GROWTH%)",
                            "bold": false,
                            "size": 10
                        }],
                        "dataProvider": tmp,

                        "balloon": {
                            "fixedPosition": true
                        },
                        "autoResize": true,
                        "valueField": "growth1",
                        "titleField": "Name",
                        "marginRight": 10,
                        "marginLeft": 10,
                        "startX": -500,
                        "depth3D": 50,
                        "angle": 40,
                        "labelPosition": "center",

                        "labelText": "[[Name]]: [[growth]]%",
                        "balloonText": "[[Name]]: [[growth]]%",
                        "export": {
                            "enabled": false
                        }
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    console.log(AmCharts.parseJSON(tmp));
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }
        function LoadChartBottomSEAch() {
            console.log("LoadChartBottomSEAch");
            var tmp = null;
            $.ajax({
                type: "POST",
                url: "ReportsDashboard.aspx/LoadChartBottomSEAch",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    tmp = msg.d;
                    var chart = AmCharts.makeChart("ChartBottomSEAch", {
                        "type": "funnel",
                        "theme": "light",
                        "titles": [{
                            "text": "BOTTOM 5 SALES ENGINEERS"
                        }, {
                            "text": "(PRO-RATA ACH%)",
                            "bold": false,
                            "size": 10
                        }],
                        "dataProvider": tmp,

                        "balloon": {
                            "fixedPosition": true
                        },
                        "valueField": "ach1",
                        "titleField": "Name",
                        "marginRight": 10,
                        "marginLeft": 10,
                        "startX": -500,
                        "rotate": true,
                        "depth3D": 50,
                        "angle": 40,
                        "labelPosition": "center",
                        "labelText": "[[Name]]: [[ach]]%",
                        "balloonText": "[[Name]]: [[ach]]%",
                        "export": {
                            "enabled": false
                        }
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }
        function LoadChartBottomSEGrowth() {
            console.log("LoadChartBottomSEGrowth");
            var tmp = null;
            $.ajax({
                type: "POST",
                url: "ReportsDashboard.aspx/LoadChartBottomSEGrowth",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    tmp = msg.d;
                    var chart = AmCharts.makeChart("ChartBottomSEGrowth", {
                        "type": "funnel",
                        "theme": "light",
                        "titles": [{
                            "text": "BOTTOM 5 SALES ENGINEERS"
                        }, {
                            "text": "(GROWTH%)",
                            "bold": false,
                            "size": 10
                        }],
                        "dataProvider": tmp,

                        "balloon": {
                            "fixedPosition": true
                        },
                        "valueField": "growth1",
                        "titleField": "Name",
                        "marginRight": 10,
                        "marginLeft": 10,
                        "startX": -500,
                        "rotate": true,
                        "depth3D": 50,
                        "angle": 40,
                        "labelPosition": "center",
                        "labelText": "[[Name]]: [[growth]]%",
                        "balloonText": "[[Name]]: [[growth]]%",
                        "export": {
                            "enabled": false
                        }
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }

        function LoadChartTopCustAch() {
            console.log("LoadChartTopCustAch");
            var tmp = null;
            $.ajax({
                type: "POST",
                url: "ReportsDashboard.aspx/LoadChartTopCustAch",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    tmp = msg.d;
                    console.log("tmp : " + tmp);
                    var chart = AmCharts.makeChart("ChartTopCustAch", {
                        "type": "funnel",
                        "theme": "light",
                        "titles": [{
                            "text": "TOP 5 CUSTOMERS"
                        }, {
                            "text": "(PRO-RATA ACH%)",
                            "bold": false,
                            "size": 10
                        }],
                        "dataProvider": tmp,
                        "balloon": {
                            "fixedPosition": true
                        },
                        "valueField": "ach1",
                        "titleField": "Name",
                        "marginRight": 10,
                        "marginLeft": 10,
                        "startX": -500,
                        "depth3D": 50,
                        "angle": 40,
                        "labelPosition": "center",
                        "labelText": "[[Name]]: [[ach]]%",
                        "balloonText": "[[Name]]: [[ach]]%",
                        "export": {
                            "enabled": false
                        }
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }
        function LoadChartTopCustGrowth() {
            console.log("LoadChartTopCustGrowth");
            var tmp = null;
            $.ajax({
                type: "POST",
                url: "ReportsDashboard.aspx/LoadChartTopCustGrowth",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    tmp = msg.d;
                    var chart = AmCharts.makeChart("ChartTopCustGrowth", {
                        "type": "funnel",
                        "theme": "light",
                        "titles": [{
                            "text": "TOP 5 CUSTOMERS"
                        }, {
                            "text": "(GROWTH%)",
                            "bold": false,
                            "size": 10
                        }],
                        "dataProvider": tmp,

                        "balloon": {
                            "fixedPosition": true
                        },
                        "autoResize": true,
                        "valueField": "growth1",
                        "titleField": "Name",
                        "marginRight": 10,
                        "marginLeft": 10,
                        "startX": -500,
                        "depth3D": 50,
                        "angle": 40,
                        "labelPosition": "center",
                        "labelText": "[[Name]]: [[growth]]%",
                        "balloonText": "[[Name]]: [[growth]]%",
                        "export": {
                            "enabled": false
                        }
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }
        function LoadChartBottomCustAch() {
            console.log("LoadChartBottomCustAch");
            var tmp = null;
            $.ajax({
                type: "POST",
                url: "ReportsDashboard.aspx/LoadChartBottomCustAch",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    tmp = msg.d;
                    console.log("Bottom : " + tmp);
                    var chart = AmCharts.makeChart("ChartBottomCustAch", {
                        "type": "funnel",
                        "theme": "light",
                        "titles": [{
                            "text": "BOTTOM 5 CUSTOMERS"
                        }, {
                            "text": "(PRO-RATA ACH%)",
                            "bold": false,
                            "size": 10
                        }],
                        "dataProvider": tmp,

                        "balloon": {
                            "fixedPosition": true
                        },
                        "valueField": "ach1",
                        "titleField": "Name",
                        "marginRight": 10,
                        "marginLeft": 10,
                        "startX": -500,
                        "rotate": true,
                        "depth3D": 50,
                        "angle": 40,
                        "labelPosition": "center",
                        "labelText": "[[Name]]: [[ach]]%",
                        "balloonText": "[[Name]]: [[ach]]%",
                        "export": {
                            "enabled": false
                        }
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }
        function LoadChartBottomCustGrowth() {
            console.log("LoadChartBottomCustGrowth");
            var tmp = null;
            $.ajax({
                type: "POST",
                url: "ReportsDashboard.aspx/LoadChartBottomCustGrowth",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    tmp = msg.d;
                    var chart = AmCharts.makeChart("ChartBottomCustGrowth", {
                        "type": "funnel",
                        "theme": "light",
                        "titles": [{
                            "text": "BOTTOM 5 CUSTOMERS"
                        }, {
                            "text": "(GROWTH%)",
                            "bold": false,
                            "size": 10
                        }],
                        "dataProvider": tmp,

                        "balloon": {
                            "fixedPosition": true
                        },
                        "valueField": "growth1",
                        "titleField": "Name",
                        "marginRight": 10,
                        "marginLeft": 10,
                        "startX": -500,
                        "rotate": true,
                        "depth3D": 50,
                        "angle": 40,
                        "labelPosition": "center",
                        "labelText": "[[Name]]: [[growth]]%",
                        "balloonText": "[[Name]]: [[growth]]%",
                        "export": {
                            "enabled": false
                        }
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }

        function LoadChartTopCPAch() {
            console.log("LoadChartTopCPAch");
            var tmp = null;
            $.ajax({
                type: "POST",
                url: "ReportsDashboard.aspx/LoadChartTopCPAch",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    tmp = msg.d;
                    console.log("tmp : " + tmp);
                    var chart = AmCharts.makeChart("ChartTopCPAch", {
                        "type": "funnel",
                        "theme": "light",
                        "titles": [{
                            "text": "TOP 5 CHANNEL PARTNERS"
                        }, {
                            "text": "(PRO-RATA ACH%)",
                            "bold": false,
                            "size": 10
                        }],
                        "dataProvider": tmp,
                        "balloon": {
                            "fixedPosition": true
                        },
                        "valueField": "ach1",
                        "titleField": "Name",
                        "marginRight": 10,
                        "marginLeft": 10,
                        "startX": -500,
                        "depth3D": 50,
                        "angle": 40,
                        "labelPosition": "center",
                        "labelText": "[[Name]]: [[ach]]%",
                        "balloonText": "[[Name]]: [[ach]]%",
                        "export": {
                            "enabled": false
                        }
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    console.log(AmCharts.parseJSON(tmp));
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }
        function LoadChartTopCPGrowth() {
            console.log("LoadChartTopCPGrowth");
            var tmp = null;
            $.ajax({
                type: "POST",
                url: "ReportsDashboard.aspx/LoadChartTopCPGrowth",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    tmp = msg.d;
                    var chart = AmCharts.makeChart("ChartTopCPGrowth", {
                        "type": "funnel",
                        "theme": "light",
                        "titles": [{
                            "text": "TOP 5 CHANNEL PARTNERS"
                        }, {
                            "text": "(GROWTH%)",
                            "bold": false,
                            "size": 10
                        }],
                        "dataProvider": tmp,

                        "balloon": {
                            "fixedPosition": true
                        },
                        "autoResize": true,
                        "valueField": "growth1",
                        "titleField": "Name",
                        "marginRight": 10,
                        "marginLeft": 10,
                        "startX": -500,
                        "depth3D": 50,
                        "angle": 40,
                        "labelPosition": "center",
                        "labelText": "[[Name]]: [[growth]]%",
                        "balloonText": "[[Name]]: [[growth]]%",
                        "export": {
                            "enabled": false
                        }
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    console.log(AmCharts.parseJSON(tmp));
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }
        function LoadChartBottomCPAch() {
            console.log("LoadChartBottomCPAch");
            var tmp = null;
            $.ajax({
                type: "POST",
                url: "ReportsDashboard.aspx/LoadChartBottomCPAch",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    tmp = msg.d;
                    var chart = AmCharts.makeChart("ChartBottomCPAch", {
                        "type": "funnel",
                        "theme": "light",
                        "titles": [{
                            "text": "BOTTOM 5 CHANNEL PARTNERS"
                        }, {
                            "text": "(PRO-RATA ACH%)",
                            "bold": false,
                            "size": 10
                        }],
                        "dataProvider": tmp,

                        "balloon": {
                            "fixedPosition": true
                        },
                        "valueField": "ach1",
                        "titleField": "Name",
                        "marginRight": 10,
                        "marginLeft": 10,
                        "startX": -500,
                        "rotate": true,
                        "depth3D": 100,
                        "angle": 40,
                        "labelPosition": "center",
                        "labelText": "[[Name]]: [[ach]]%",
                        "balloonText": "[[Name]]: [[ach]]%",
                        "export": {
                            "enabled": false
                        }
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }
        function LoadChartBottomCPGrowth() {
            console.log("LoadChartBottomCPGrowth");
            var tmp = null;
            $.ajax({
                type: "POST",
                url: "ReportsDashboard.aspx/LoadChartBottomCPGrowth",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    tmp = msg.d;
                    var chart = AmCharts.makeChart("ChartBottomCPGrowth", {
                        "type": "funnel",
                        "theme": "light",
                        "titles": [{
                            "text": "BOTTOM 5 CHANNEL PARTNERS"
                        }, {
                            "text": "(GROWTH%)",
                            "bold": false,
                            "size": 10
                        }],
                        "dataProvider": tmp,

                        "balloon": {
                            "fixedPosition": true
                        },
                        "valueField": "growth1",
                        "titleField": "Name",
                        "marginRight": 10,
                        "marginLeft": 10,
                        "startX": -500,
                        "rotate": true,
                        "depth3D": 50,
                        "angle": 40,
                        "labelPosition": "center",
                        "labelText": "[[Name]]: [[growth]]%",
                        "balloonText": "[[Name]]: [[growth]]%",
                        "export": {
                            "enabled": false
                        }
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }

        function LoadChartFamAch() {
            console.log("LoadChartFamAch");
            var tmp = null;
            $.ajax({
                type: "POST",
                //url: 'ReportDashboard.aspx/LoadChartConsolidated',
                url: "ReportsDashboard.aspx/LoadChartFamAch",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    console.log("msg : " + msg);
                    tmp = msg.d;
                    console.log("tmp1 : " + tmp);
                    var d = new Date();
                    var n = d.getFullYear();
                    var chart = AmCharts.makeChart("ChartFamAch", {
                        "theme": "light",
                        "type": "serial",
                        "titles": [{
                            "text": "FAMILY PERFORMANCE"
                        }, {
                            "text": "(PRO-RATA ACH%)",
                            "bold": false,
                            "size": 10
                        }],
                        "startDuration": 2,
                        "dataProvider": tmp,
                        "valueAxes": [{
                            "position": "left",
                            "axisAlpha": 0,
                            "gridAlpha": 0
                        }],
                        "graphs": [{
                            "balloonText": "[[category]]: <b>[[value]]</b>",
                            "colorField": "color",
                            "fillAlphas": 0.85,
                            "lineAlpha": 0.1,
                            "type": "column",
                            "topRadius": 0.8,
                            "valueField": "ach"
                        }],
                        "depth3D": 40,
                        "angle": 30,
                        "chartCursor": {
                            "categoryBalloonEnabled": false,
                            "cursorAlpha": 0,
                            "zoomable": false
                        },
                        "categoryField": "Name",
                        "categoryAxis": {
                            "gridPosition": "start",
                            "axisAlpha": 0,
                            "gridAlpha": 0,
                            "labelRotation": 60,
                            "minHorizontalGap": 0
                        },
                        "export": {
                            "enabled": false
                        }
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }
        function LoadChartFamGrowth() {
            console.log("LoadChartFamGrowth");
            var tmp = null;
            $.ajax({
                type: "POST",
                //url: 'ReportDashboard.aspx/LoadChartConsolidated',
                url: "ReportsDashboard.aspx/LoadChartFamGrowth",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    console.log("msg : " + msg);
                    tmp = msg.d;
                    console.log("tmp1 : " + tmp);
                    var d = new Date();
                    var n = d.getFullYear();
                    var chart = AmCharts.makeChart("ChartFamGrowth", {
                        "theme": "light",
                        "type": "serial",
                        "titles": [{
                            "text": "FAMILY PERFORMANCE"
                        }, {
                            "text": "(GROWTH%)",
                            "bold": false,
                            "size": 10
                        }],
                        "startDuration": 2,
                        "dataProvider": tmp,
                        "valueAxes": [{
                            "position": "left",
                            "axisAlpha": 0,
                            "gridAlpha": 0
                        }],
                        "graphs": [{
                            "balloonText": "[[category]]: <b>[[value]]</b>",
                            "colorField": "color",
                            "fillAlphas": 0.85,
                            "lineAlpha": 0.1,
                            "type": "column",
                            "topRadius": 0.8,
                            "valueField": "growth"
                        }],
                        "depth3D": 40,
                        "angle": 30,
                        "chartCursor": {
                            "categoryBalloonEnabled": false,
                            "cursorAlpha": 0,
                            "zoomable": false
                        },
                        "categoryField": "Name",
                        "categoryAxis": {
                            "gridPosition": "start",
                            "axisAlpha": 0,
                            "gridAlpha": 0,
                            "labelRotation": 60,
                            "minHorizontalGap": 0
                        },
                        "export": {
                            "enabled": false
                        }
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }

        function LoadChartGoldTopSEAch() {
            console.log("LoadChartGoldTopSEAch");
            var tmp = null;
            $.ajax({
                type: "POST",
                url: "ReportsDashboard.aspx/LoadChartTopSEAch",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    tmp = msg.d;
                    console.log("tmp : " + tmp);
                    var chart = AmCharts.makeChart("ChartGoldTopSEAch", {
                        "type": "funnel",
                        "theme": "light",
                        "titles": [{
                            "text": "TOP 5 SALES ENGINEERS"
                        }, {
                            "text": "(PRO-RATA ACH%)",
                            "bold": false,
                            "size": 10
                        }],
                        "dataProvider": tmp,
                        //"[{\"Name\":\"ABHISHEK\",\"ach\":\"91\"},{\"Name\":\"CHANDI PRASAD\",\"ach\":\"91\"},{\"Name\":\"SUNITA CHAVAN\",\"ach\":\"92\"},{\"Name\":\"SIDDHARTH\",\"ach\":\"92\"},{\"Name\":\"RAMESH\",\"ach\":\"92\"}]",
                        "balloon": {
                            "fixedPosition": true
                        },
                        "valueField": "ach1",
                        "titleField": "Name",
                        "marginRight": 10,
                        "marginLeft": 10,
                        "startX": -500,
                        "depth3D": 50,
                        "angle": 40,
                        "labelPosition": "center",
                        "labelText": "[[Name]]: [[ach]]%",
                        "balloonText": "[[Name]]: [[ach]]%",
                        "export": {
                            "enabled": false
                        }
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    console.log(AmCharts.parseJSON(tmp));
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }
        function LoadChartGoldTopSEGrowth() {
            console.log("LoadChartGoldTopSEGrowth");
            var tmp = null;
            $.ajax({
                type: "POST",
                url: "ReportsDashboard.aspx/LoadChartTopSEGrowth",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    tmp = msg.d;
                    var chart = AmCharts.makeChart("ChartGoldTopSEGrowth", {
                        "type": "funnel",
                        "theme": "light",
                        "titles": [{
                            "text": "TOP 5 SALES ENGINEERS"
                        }, {
                            "text": "(GROWTH%)",
                            "bold": false,
                            "size": 10
                        }],
                        "dataProvider": tmp,

                        "balloon": {
                            "fixedPosition": true
                        },
                        "autoResize": true,
                        "valueField": "growth1",
                        "titleField": "Name",
                        "marginRight": 10,
                        "marginLeft": 10,
                        "startX": -500,
                        "depth3D": 50,
                        "angle": 40,
                        "labelPosition": "center",
                        "labelText": "[[Name]]: [[growth]]%",
                        "balloonText": "[[Name]]: [[growth]]%",
                        "export": {
                            "enabled": false
                        }
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    console.log(AmCharts.parseJSON(tmp));
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }
        function LoadChartGoldBottomSEAch() {
            console.log("LoadChartGoldBottomSEAch");
            var tmp = null;
            $.ajax({
                type: "POST",
                url: "ReportsDashboard.aspx/LoadChartBottomSEAch",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    tmp = msg.d;
                    var chart = AmCharts.makeChart("ChartGoldBottomSEAch", {
                        "type": "funnel",
                        "theme": "light",
                        "titles": [{
                            "text": "BOTTOM 5 SALES ENGINEERS"
                        }, {
                            "text": "(PRO-RATA ACH%)",
                            "bold": false,
                            "size": 10
                        }],
                        "dataProvider": tmp,

                        "balloon": {
                            "fixedPosition": true
                        },
                        "valueField": "ach1",
                        "titleField": "Name",
                        "marginRight": 10,
                        "marginLeft": 10,
                        "startX": -500,
                        "rotate": true,
                        "depth3D": 50,
                        "angle": 40,
                        "labelPosition": "center",
                        "labelText": "[[Name]]: [[ach]]%",
                        "balloonText": "[[Name]]: [[ach]]%",
                        "export": {
                            "enabled": false
                        }
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }
        function LoadChartGoldBottomSEGrowth() {
            console.log("LoadGoldChartBottomSEGrowth");
            var tmp = null;
            $.ajax({
                type: "POST",
                url: "ReportsDashboard.aspx/LoadChartBottomSEGrowth",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    tmp = msg.d;
                    var chart = AmCharts.makeChart("ChartGoldBottomSEGrowth", {
                        "type": "funnel",
                        "theme": "light",
                        "titles": [{
                            "text": "BOTTOM 5 SALES ENGINEERS"
                        }, {
                            "text": "(GROWTH%)",
                            "bold": false,
                            "size": 10
                        }],
                        "dataProvider": tmp,

                        "balloon": {
                            "fixedPosition": true
                        },
                        "valueField": "growth1",
                        "titleField": "Name",
                        "marginRight": 10,
                        "marginLeft": 10,
                        "startX": -500,
                        "rotate": true,
                        "depth3D": 50,
                        "angle": 40,
                        "labelPosition": "center",
                        "labelText": "[[Name]]: [[growth]]%",
                        "balloonText": "[[Name]]: [[growth]]%",
                        "export": {
                            "enabled": false
                        }
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }

        function LoadChartGoldTopCustAch() {
            console.log("LoadChartGoldTopCustAch");
            var tmp = null;
            $.ajax({
                type: "POST",
                //url: 'ReportDashboard.aspx/LoadChartConsolidated',
                url: "ReportsDashboard.aspx/LoadChartTopCustAch",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    console.log("msg : " + msg);
                    tmp = msg.d;
                    console.log("tmp1 : " + tmp);
                    var d = new Date();
                    var n = d.getFullYear();
                    var chart = AmCharts.makeChart("ChartGoldTopCustAch", {
                        "theme": "light",
                        "type": "serial",
                        "titles": [{
                            "text": "TOP 10 CUSTOMER PERFORMANCE"
                        }, {
                            "text": "(BUDGET)",
                            "bold": false,
                            "size": 10
                        }],
                        "startDuration": 2,
                        "dataProvider": tmp,
                        "valueAxes": [{
                            "position": "left",
                            "axisAlpha": 0,
                            "gridAlpha": 0
                        }],
                        "graphs": [{
                            "balloonText": "[[category]]: <b>[[value]]</b>",
                            "colorField": "color",
                            "fillAlphas": 0.85,
                            "lineAlpha": 0.1,
                            "type": "column",
                            "topRadius": 0.8,
                            "valueField": "budget_value"
                        }],
                        "depth3D": 40,
                        "angle": 30,
                        "chartCursor": {
                            "categoryBalloonEnabled": false,
                            "cursorAlpha": 0,
                            "zoomable": false
                        },
                        "categoryField": "Name",
                        "categoryAxis": {
                            "gridPosition": "start",
                            "axisAlpha": 0,
                            "gridAlpha": 0,
                            "labelRotation": 60,
                            "minHorizontalGap": 0
                        },
                        "export": {
                            "enabled": false
                        }
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }
        function LoadChartGoldTopCustGrowth() {
            console.log("LoadChartGoldTopCustGrowth");
            var tmp = null;
            $.ajax({
                type: "POST",
                //url: 'ReportDashboard.aspx/LoadChartConsolidated',
                url: "ReportsDashboard.aspx/LoadChartTopCustGrowth",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    console.log("msg : " + msg);
                    tmp = msg.d;
                    console.log("tmp1 : " + tmp);
                    var d = new Date();
                    var n = d.getFullYear();
                    var chart = AmCharts.makeChart("ChartGoldTopCustGrowth", {
                        "theme": "light",
                        "type": "serial",
                        "titles": [{
                            "text": "TOP 10 CUSTOMER PERFORMANCE"
                        }, {
                            "text": "(GROWTH%)",
                            "bold": false,
                            "size": 10
                        }],
                        "startDuration": 2,
                        "dataProvider": tmp,
                        "valueAxes": [{
                            "position": "left",
                            "axisAlpha": 0,
                            "gridAlpha": 0
                        }],
                        "graphs": [{
                            "balloonText": "[[category]]: <b>[[value]]</b>",
                            "colorField": "color",
                            "fillAlphas": 0.85,
                            "lineAlpha": 0.1,
                            "type": "column",
                            "topRadius": 0.8,
                            "valueField": "growth"
                        }],
                        "depth3D": 40,
                        "angle": 30,
                        "chartCursor": {
                            "categoryBalloonEnabled": false,
                            "cursorAlpha": 0,
                            "zoomable": false
                        },
                        "categoryField": "Name",
                        "categoryAxis": {
                            "gridPosition": "start",
                            "axisAlpha": 0,
                            "gridAlpha": 0,
                            "labelRotation": 60,
                            "minHorizontalGap": 0
                        },
                        "export": {
                            "enabled": false
                        }
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }

        function LoadChartGoldTopCPAch() {
            console.log("LoadChartGoldTopCPAch");
            var tmp = null;
            $.ajax({
                type: "POST",
                //url: 'ReportDashboard.aspx/LoadChartConsolidated',
                url: "ReportsDashboard.aspx/LoadChartTopCPAch",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    console.log("msg : " + msg);
                    tmp = msg.d;
                    console.log("tmp1 : " + tmp);
                    var d = new Date();
                    var n = d.getFullYear();
                    var chart = AmCharts.makeChart("ChartGoldTopCPAch", {
                        "theme": "light",
                        "type": "serial",
                        "titles": [{
                            "text": "TOP 10 CP PERFORMANCE"
                        }, {
                            "text": "(BUDGET)",
                            "bold": false,
                            "size": 10
                        }],
                        "startDuration": 2,
                        "dataProvider": tmp,
                        "valueAxes": [{
                            "position": "left",
                            "axisAlpha": 0,
                            "gridAlpha": 0
                        }],
                        "graphs": [{
                            "balloonText": "[[category]]: <b>[[value]]</b>",
                            "colorField": "color",
                            "fillAlphas": 0.85,
                            "lineAlpha": 0.1,
                            "type": "column",
                            "topRadius": 0.8,
                            "valueField": "budget_value"
                        }],
                        "depth3D": 40,
                        "angle": 30,
                        "chartCursor": {
                            "categoryBalloonEnabled": false,
                            "cursorAlpha": 0,
                            "zoomable": false
                        },
                        "categoryField": "Name",
                        "categoryAxis": {
                            "gridPosition": "start",
                            "axisAlpha": 0,
                            "gridAlpha": 0,
                            "labelRotation": 60,
                            "minHorizontalGap": 0
                        },
                        "export": {
                            "enabled": false
                        }
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }
        function LoadChartGoldTopCPGrowth() {
            console.log("LoadChartGoldTopCPGrowth");
            var tmp = null;
            $.ajax({
                type: "POST",
                //url: 'ReportDashboard.aspx/LoadChartConsolidated',
                url: "ReportsDashboard.aspx/LoadChartTopCPGrowth",
                data: "",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    console.log("msg : " + msg);
                    tmp = msg.d;
                    console.log("tmp1 : " + tmp);
                    var d = new Date();
                    var n = d.getFullYear();
                    var chart = AmCharts.makeChart("ChartGoldTopCPGrowth", {
                        "theme": "light",
                        "type": "serial",
                        "titles": [{
                            "text": "TOP 10 CP PERFORMANCE"
                        }, {
                            "text": "(GROWTH%)",
                            "bold": false,
                            "size": 10
                        }],
                        "startDuration": 2,
                        "dataProvider": tmp,
                        "valueAxes": [{
                            "position": "left",
                            "axisAlpha": 0,
                            "gridAlpha": 0
                        }],
                        "graphs": [{
                            "balloonText": "[[category]]: <b>[[value]]</b>",
                            "colorField": "color",
                            "fillAlphas": 0.85,
                            "lineAlpha": 0.1,
                            "type": "column",
                            "topRadius": 0.8,
                            "valueField": "growth"
                        }],
                        "depth3D": 40,
                        "angle": 30,
                        "chartCursor": {
                            "categoryBalloonEnabled": false,
                            "cursorAlpha": 0,
                            "zoomable": false
                        },
                        "categoryField": "Name",
                        "categoryAxis": {
                            "gridPosition": "start",
                            "axisAlpha": 0,
                            "gridAlpha": 0,
                            "labelRotation": 60,
                            "minHorizontalGap": 0
                        },
                        "export": {
                            "enabled": false
                        }
                    });
                    chart.dataProvider = AmCharts.parseJSON(tmp);
                    chart.validateData();
                },
                error: function (e) {
                    //console(e);
                }
            });
        }


        function bindBranch() {
            var Sales_value_year_0 = $('#MainContent_hdn_Sales_value_year_0').val();
            var Sales_ytd_value = $('#MainContent_hdn_Sales_ytd_value').val();
            var budget_value = $('#MainContent_hdn_budget_value').val();
            var ytd_plan = $('#MainContent_hdn_ytd_plan').val();
            var achvmnt = $('#MainContent_hdn_achvmnt').val();
            var askrate = $('#MainContent_hdn_askrate').val();
            var NOVALUES = $('#MainContent_hdn_NOVALUES').val();
            var growth = $('#MainContent_hdn_growth').val();

            var head_content = $('#MainContent_gridtopbranches tr:first').html();
            $('#MainContent_gridtopbranches').prepend('<thead></thead>')
            $('#MainContent_gridtopbranches thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_gridtopbranches tbody tr:first').hide();
            $('#MainContent_gridtopbranches').append('<tfoot><tr> <th style="text-align:right">TOTAL:</th><th style="text-align: right">' + Sales_value_year_0 + '</th><th style="text-align: right">' + budget_value + '</th><th style="text-align: right">' + ytd_plan + '</th><th style="text-align: right">' + Sales_ytd_value + '</th><th style="text-align: right">' + askrate + '</th><th style="text-align: right">' + achvmnt +
                 '</th><th style="text-align: right">' + growth + '</th></tr></tfoot>');

            $('#MainContent_gridtopbranches').DataTable(
                     {
                         "info": false,
                         bSortable: true,
                         bRetrieve: true,
                         aoColumnDefs: [
                             { "aTargets": [0], "bSortable": true },
                             { "aTargets": [1], "bSortable": true },
                             { "aTargets": [2], "bSortable": true },
                             { "aTargets": [3], "bSortable": true }
                         ],
                     });
            $('#MainContent_gridtopbranches').on('search.dt', function () {
                var value = $('.dataTables_filter input').val();
                console.log(value);
                document.getElementById('<%=hdnsearch.ClientID%>').value = value;
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "ReportsDashboard.aspx/setBranchTotal",
                    data: "{'search':'" + value + "'}",
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        console.log(data.d);
                        var parsed = data.d;
                        console.log(parsed);
                        console.log(parsed[1]);
                        //$('#MainContent_targetvssales').prepend('<tfoot></tfoot>');
                        $('#MainContent_gridtopbranches tfoot').hide();
                        Sales_value_year_0 = parsed[0];
                        Sales_ytd_value = parsed[1];
                        budget_value = parsed[2];
                        ytd_plan = parsed[3];
                        achvmnt = parsed[4];
                        askrate = parsed[5];
                        growth = parsed[6];

                        $('#MainContent_gridtopbranches').append('<tfoot><tr> <th style="text-align:right">TOTAL:</th><th style="text-align: right">' + Sales_value_year_0 + '</th><th style="text-align: right">' + budget_value + '</th><th style="text-align: right">' + ytd_plan + '</th><th style="text-align: right">' + Sales_ytd_value + '</th><th style="text-align: right">' + askrate + '</th><th style="text-align: right">' + achvmnt +
                 '</th><th style="text-align: right">' + growth + '</th></tr></tfoot>');
                        console.log("2");
                    },
                    error: function (result) {
                        console.log("3");
                        console.log(result.responseText);
                    }
                });
            });
        }
        function bindSE() {
            var Sales_value_year_0 = $('#MainContent_hdn_Sales_value_year_0').val();
            var Sales_ytd_value = $('#MainContent_hdn_Sales_ytd_value').val();
            var budget_value = $('#MainContent_hdn_budget_value').val();
            var ytd_plan = $('#MainContent_hdn_ytd_plan').val();
            var achvmnt = $('#MainContent_hdn_achvmnt').val();
            var askrate = $('#MainContent_hdn_askrate').val();
            var NOVALUES = $('#MainContent_hdn_NOVALUES').val();
            var growth = $('#MainContent_hdn_growth').val();

            var head_content = $('#MainContent_GridTopSE tr:first').html();
            $('#MainContent_GridTopSE').prepend('<thead></thead>')
            $('#MainContent_GridTopSE thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_GridTopSE tbody tr:first').hide();


            $('#MainContent_GridTopSE').DataTable(
                     {
                         "info": false,
                         bSortable: true,
                         bRetrieve: true,
                         aoColumnDefs: [
                             { "aTargets": [0], "bSortable": true },
                             { "aTargets": [1], "bSortable": true },
                             { "aTargets": [2], "bSortable": true },
                             { "aTargets": [3], "bSortable": true }
                         ]
                     });

            $('#MainContent_GridTopSE').on('search.dt', function () {
                var value = $('.dataTables_filter input').val();
                console.log(value);
                document.getElementById('<%=hdnsearch.ClientID%>').value = value;
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "ReportsDashboard.aspx/setSETotal",
                        data: "{'search':'" + value + "'}",
                        dataType: "json",
                        success: function (data) {
                            console.log(data);
                            console.log(data.d);
                            var parsed = data.d;
                            console.log(parsed);
                            console.log(parsed[1]);
                            //$('#MainContent_targetvssales').prepend('<tfoot></tfoot>');
                            $('#MainContent_GridTopSE tfoot').hide();
                            Sales_value_year_0 = parsed[0];
                            Sales_ytd_value = parsed[1];
                            budget_value = parsed[2];
                            ytd_plan = parsed[3];
                            achvmnt = parsed[4];
                            askrate = parsed[5];
                            growth = parsed[6];
                            $('#MainContent_GridTopSE').append('<tfoot><tr> <th style="text-align:right">TOTAL:</th><th style="text-align: right"></th><th style="text-align: right">' + Sales_value_year_0 + '</th><th style="text-align: right">' + budget_value + '</th><th style="text-align: right">' + ytd_plan + '</th><th style="text-align: right">' + Sales_ytd_value + '</th><th style="text-align: right">' + askrate + '</th><th style="text-align: right">' + achvmnt +
                    '</th><th style="text-align: right">' + growth + '</th></tr></tfoot>');

                            console.log("2");
                        },
                        error: function (result) {
                            console.log("3");
                            console.log(result.responseText);
                        }
                    });
                });
            }
            function bindCustomer() {
                var head_content = $('#MainContent_GridCustByAch tr:first').html();
                $('#MainContent_GridCustByAch').prepend('<thead></thead>')
                $('#MainContent_GridCustByAch thead').html('<tr>' + head_content + '</tr>');
                $('#MainContent_GridCustByAch tbody tr:first').hide();


                $('#MainContent_GridCustByAch').DataTable(
                         {
                             "info": false
                         });
                $('#MainContent_GridCustByAch').on('search.dt', function () {
                    var value = $('.dataTables_filter input').val();
                    console.log(value);
                    document.getElementById('<%=hdnsearch.ClientID%>').value = value;
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "ReportsDashboard.aspx/setCustTotal",
                    data: "{'search':'" + value + "'}",
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        console.log(data.d);
                        var parsed = data.d;
                        console.log(parsed);
                        console.log(parsed[1]);
                        //$('#MainContent_targetvssales').prepend('<tfoot></tfoot>');
                        $('#MainContent_GridCustByAch tfoot').hide();
                        Sales_value_year_0 = parsed[0];
                        console.log(Sales_value_year_0);
                        Sales_ytd_value = parsed[1];
                        budget_value = parsed[2];
                        ytd_plan = parsed[3];
                        achvmnt = parsed[4];
                        askrate = parsed[5];
                        growth = parsed[6];
                        $('#MainContent_GridCustByAch').append('<tfoot><tr> <th style="text-align:right">TOTAL:</th><th style="text-align: right"></th><th style="text-align: right"></th><th></th><th style="text-align: right">' + Sales_value_year_0 + '</th><th style="text-align: right">' + budget_value + '</th><th style="text-align: right">' + ytd_plan + '</th><th style="text-align: right">' + Sales_ytd_value + '</th><th style="text-align: right">' + askrate + '</th><th style="text-align: right">' + achvmnt +
                '</th><th style="text-align: right">' + growth + '</th></tr></tfoot>');

                        console.log("2");
                    },
                    error: function (result) {
                        console.log("3");
                        console.log(result.responseText);
                    }
                });
            });
        }
        function bindCP() {
            var head_content = $('#MainContent_GridTopCP tr:first').html();
            $('#MainContent_GridTopCP').prepend('<thead></thead>')
            $('#MainContent_GridTopCP thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_GridTopCP tbody tr:first').hide();


            $('#MainContent_GridTopCP').DataTable(
                     {
                         "info": false
                     });
            $('#MainContent_GridTopCP').on('search.dt', function () {
                var value = $('.dataTables_filter input').val();
                console.log(value);
                document.getElementById('<%=hdnsearch.ClientID%>').value = value;
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "ReportsDashboard.aspx/setCPTotal",
                    data: "{'search':'" + value + "'}",
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        console.log(data.d);
                        var parsed = data.d;
                        console.log(parsed);
                        console.log(parsed[1]);
                        //$('#MainContent_targetvssales').prepend('<tfoot></tfoot>');
                        $('#MainContent_GridTopCP tfoot').hide();
                        Sales_value_year_0 = parsed[0];
                        console.log(Sales_value_year_0);
                        Sales_ytd_value = parsed[1];
                        budget_value = parsed[2];
                        ytd_plan = parsed[3];
                        achvmnt = parsed[4];
                        askrate = parsed[5];
                        growth = parsed[6];
                        $('#MainContent_GridTopCP').append('<tfoot><tr> <th style="text-align:right">TOTAL:</th><th style="text-align: right"></th><th style="text-align: right"></th><th></th><th>' + Sales_value_year_0 + '</th><th style="text-align: right">' + budget_value + '</th><th style="text-align: right">' + ytd_plan + '</th><th style="text-align: right">' + Sales_ytd_value + '</th><th style="text-align: right">' + askrate + '</th><th style="text-align: right">' + achvmnt +
                '</th><th style="text-align: right">' + growth + '</th></tr></tfoot>');

                        console.log("2");
                    },
                    error: function (result) {
                        console.log("3");
                        console.log(result.responseText);
                    }
                });
            });
        }
        function bindGold() {
            var head_content = $('#MainContent_GridTopGoldBranches tr:first').html();
            $('#MainContent_GridTopGoldBranches').prepend('<thead></thead>')
            $('#MainContent_GridTopGoldBranches thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_GridTopGoldBranches tbody tr:first').hide();


            $('#MainContent_GridTopGoldBranches').DataTable(
                     {
                         "info": false
                     });

            $('#MainContent_GridTopGoldBranches').on('search.dt', function () {
                var value = $('.dataTables_filter input').val();
                console.log(value);
                document.getElementById('<%=hdnsearch.ClientID%>').value = value;
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "ReportsDashboard.aspx/setGoldBranchTotal",
                    data: "{'search':'" + value + "'}",
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        console.log(data.d);
                        var parsed = data.d;
                        console.log(parsed);
                        console.log(parsed[1]);
                        //$('#MainContent_targetvssales').prepend('<tfoot></tfoot>');
                        $('#MainContent_GridTopGoldBranches tfoot').hide();
                        Sales_value_year_0 = parsed[0];
                        Sales_ytd_value = parsed[1];
                        budget_value = parsed[2];
                        ytd_plan = parsed[3];
                        achvmnt = parsed[4];
                        askrate = parsed[5];
                        growth = parsed[6];

                        $('#MainContent_GridTopGoldBranches').append('<tfoot><tr> <th style="text-align:right">TOTAL:</th><th style="text-align: right">' + Sales_value_year_0 + '</th><th style="text-align: right">' + budget_value + '</th><th style="text-align: right">' + ytd_plan + '</th><th style="text-align: right">' + Sales_ytd_value + '</th><th style="text-align: right">' + askrate + '</th><th style="text-align: right">' + achvmnt +
                 '</th><th style="text-align: right">' + growth + '</th></tr></tfoot>');
                        console.log("2");
                    },
                    error: function (result) {
                        console.log("3");
                        console.log(result.responseText);
                    }
                });
            });

            var head_content1 = $('#MainContent_GridTopGoldSE tr:first').html();
            $('#MainContent_GridTopGoldSE').prepend('<thead></thead>')
            $('#MainContent_GridTopGoldSE thead').html('<tr>' + head_content1 + '</tr>');
            $('#MainContent_GridTopGoldSE tbody tr:first').hide();



            $('#MainContent_GridTopGoldSE').DataTable(
                     {
                         "info": false
                     });
            $('#MainContent_GridTopGoldSE').on('search.dt', function () {
                var value = $('.dataTables_filter input').val();
                console.log(value);
                document.getElementById('<%=hdnsearch.ClientID%>').value = value;
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "ReportsDashboard.aspx/setGoldSETotal",
                    data: "{'search':'" + value + "'}",
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        console.log(data.d);
                        var parsed = data.d;
                        console.log(parsed);
                        console.log(parsed[1]);
                        //$('#MainContent_targetvssales').prepend('<tfoot></tfoot>');
                        $('#MainContent_GridTopGoldSE tfoot').hide();
                        Sales_value_year_0 = parsed[0];
                        Sales_ytd_value = parsed[1];
                        budget_value = parsed[2];
                        ytd_plan = parsed[3];
                        achvmnt = parsed[4];
                        askrate = parsed[5];
                        growth = parsed[6];
                        $('#MainContent_GridTopGoldSE').append('<tfoot><tr> <th style="text-align:right">TOTAL:</th><th style="text-align: right"></th><th>' + Sales_value_year_0 + '</th><th style="text-align: right">' + budget_value + '</th><th style="text-align: right">' + ytd_plan + '</th><th style="text-align: right">' + Sales_ytd_value + '</th><th style="text-align: right">' + askrate + '</th><th style="text-align: right">' + achvmnt +
                '</th><th style="text-align: right">' + growth + '</th></tr></tfoot>');

                        console.log("2");
                    },
                    error: function (result) {
                        console.log("3");
                        console.log(result.responseText);
                    }
                });
            });

            var head_content2 = $('#MainContent_GridTopGoldCstmrs tr:first').html();
            $('#MainContent_GridTopGoldCstmrs').prepend('<thead></thead>')
            $('#MainContent_GridTopGoldCstmrs thead').html('<tr>' + head_content2 + '</tr>');
            $('#MainContent_GridTopGoldCstmrs tbody tr:first').hide();


            $('#MainContent_GridTopGoldCstmrs').DataTable(
                     {
                         "info": false
                     });
            $('#MainContent_GridTopGoldCstmrs').on('search.dt', function () {
                var value = $('.dataTables_filter input').val();
                console.log(value);
                document.getElementById('<%=hdnsearch.ClientID%>').value = value;
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "ReportsDashboard.aspx/setGoldCustTotal",
                    data: "{'search':'" + value + "'}",
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        console.log(data.d);
                        var parsed = data.d;
                        console.log(parsed);
                        console.log(parsed[1]);
                        //$('#MainContent_targetvssales').prepend('<tfoot></tfoot>');
                        $('#MainContent_GridTopGoldCstmrs tfoot').hide();
                        Sales_value_year_0 = parsed[0];
                        console.log(Sales_value_year_0);
                        Sales_ytd_value = parsed[1];
                        budget_value = parsed[2];
                        ytd_plan = parsed[3];
                        achvmnt = parsed[4];
                        askrate = parsed[5];
                        growth = parsed[6];
                        $('#MainContent_GridTopGoldCstmrs').append('<tfoot><tr> <th style="text-align:right">TOTAL:</th><th style="text-align: right"></th><th style="text-align: right"></th><th></th><th>' + Sales_value_year_0 + '</th><th style="text-align: right">' + budget_value + '</th><th style="text-align: right">' + ytd_plan + '</th><th style="text-align: right">' + Sales_ytd_value + '</th><th style="text-align: right">' + askrate + '</th><th style="text-align: right">' + achvmnt +
                '</th><th style="text-align: right">' + growth + '</th></tr></tfoot>');

                        console.log("2");
                    },
                    error: function (result) {
                        console.log("3");
                        console.log(result.responseText);
                    }
                });
            });

            var head_content3 = $('#MainContent_GridTopGoldCP tr:first').html();
            $('#MainContent_GridTopGoldCP').prepend('<thead></thead>')
            $('#MainContent_GridTopGoldCP thead').html('<tr>' + head_content3 + '</tr>');
            $('#MainContent_GridTopGoldCP tbody tr:first').hide();


            $('#MainContent_GridTopGoldCP').DataTable(
                     {
                         "info": false
                     });
            $('#MainContent_GridTopGoldCP').on('search.dt', function () {
                var value = $('.dataTables_filter input').val();
                console.log(value);
                document.getElementById('<%=hdnsearch.ClientID%>').value = value;
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "ReportsDashboard.aspx/setGoldCPTotal",
                    data: "{'search':'" + value + "'}",
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        console.log(data.d);
                        var parsed = data.d;
                        console.log(parsed);
                        console.log(parsed[1]);
                        //$('#MainContent_targetvssales').prepend('<tfoot></tfoot>');
                        $('#MainContent_GridTopGoldCP tfoot').hide();
                        Sales_value_year_0 = parsed[0];
                        console.log(Sales_value_year_0);
                        Sales_ytd_value = parsed[1];
                        budget_value = parsed[2];
                        ytd_plan = parsed[3];
                        achvmnt = parsed[4];
                        askrate = parsed[5];
                        growth = parsed[6];
                        $('#MainContent_GridTopGoldCP').append('<tfoot><tr> <th style="text-align:right">TOTAL:</th><th style="text-align: right"></th><th style="text-align: right"></th><th></th><th>' + Sales_value_year_0 + '</th><th style="text-align: right">' + budget_value + '</th><th style="text-align: right">' + ytd_plan + '</th><th style="text-align: right">' + Sales_ytd_value + '</th><th style="text-align: right">' + askrate + '</th><th style="text-align: right">' + achvmnt +
                '</th><th style="text-align: right">' + growth + '</th></tr></tfoot>');

                        console.log("2");
                    },
                    error: function (result) {
                        console.log("3");
                        console.log(result.responseText);
                    }
                });
            });

        }
        function bindFamily() {
            var head_content = $('#MainContent_GridFamily tr:first').html();
            $('#MainContent_GridFamily').prepend('<thead></thead>')
            $('#MainContent_GridFamily thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_GridFamily tbody tr:first').hide();


            $('#MainContent_GridFamily').DataTable(
                     {
                         "info": false
                     });

            $('#MainContent_GridFamily').on('search.dt', function () {
                var value = $('.dataTables_filter input').val();
                console.log(value);
                document.getElementById('<%=hdnsearch.ClientID%>').value = value;
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "ReportsDashboard.aspx/setFamilyTotal",
                    data: "{'search':'" + value + "'}",
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        console.log(data.d);
                        var parsed = data.d;
                        console.log(parsed);
                        console.log(parsed[1]);
                        //$('#MainContent_targetvssales').prepend('<tfoot></tfoot>');
                        $('#MainContent_GridFamily tfoot').hide();
                        Sales_value_year_0 = parsed[0];
                        Sales_ytd_value = parsed[1];
                        budget_value = parsed[2];
                        ytd_plan = parsed[3];
                        achvmnt = parsed[4];
                        askrate = parsed[5];
                        growth = parsed[6];

                        $('#MainContent_GridFamily').append('<tfoot><tr> <th style="text-align:right">TOTAL:</th><th style="text-align: right">' + Sales_value_year_0 + '</th><th style="text-align: right">' + budget_value + '</th><th style="text-align: right">' + ytd_plan + '</th><th style="text-align: right">' + Sales_ytd_value + '</th><th style="text-align: right">' + askrate + '</th><th style="text-align: right">' + achvmnt +
                 '</th><th style="text-align: right">' + growth + '</th></tr></tfoot>');
                        console.log("2");
                    },
                    error: function (result) {
                        console.log("3");
                        console.log(result.responseText);
                    }
                });
            });

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableHistory="true" EnablePartialRendering="true" EnablePageMethods="true" AsyncPostBackTimeout="360">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="crumbs">
                <!-- Start : Breadcrumbs -->
                <ul id="breadcrumbs" class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a>Reports</a>
                    </li>
                    <li class="current">Dashboard</li>

                    <div>
                        <ul style="float: right; list-style: none; margin-top: -4px; width: 247px; margin-right: -5px;" class="alert alert-danger fade in">
                            <li><span style="margin-right: 4px; vertical-align: text-bottom;">Val In '000</span>
                                <asp:RadioButton ID="ValInThsnd" OnCheckedChanged="byValueIn_CheckedChanged" AutoPostBack="true" Checked="true" GroupName="byValueIn" runat="server" />

                                <span style="margin-right: 4px; margin-left: 4px; vertical-align: text-bottom;">Val In Lakhs</span>
                                <asp:RadioButton ID="ValInLakhs" OnCheckedChanged="byValueIn_CheckedChanged" AutoPostBack="true" GroupName="byValueIn" runat="server" />
                            </li>
                        </ul>
                    </div>
                </ul>

            </div>
            <!-- END : Breadcrumbs -->
            <asp:HiddenField ID="hdnsearch" runat="server" />
            <asp:HiddenField ID="hdnGold" runat="server" />
            <asp:HiddenField ID="hdnYear" runat="server" />
            <div class="col-md-12" style="margin-top: 5px;">
               
                <div class="col-md-10">
                    <div class="col-md-6" id="performncs">
                        <div class="col-md-6">
                        <h6 style="color: #006780; padding-left: 4px;">PERFORMANCES BY</h6>
                   </div>
                        <div class="col-md-6">
                            <asp:DropDownList ID="ddlPerfBy" runat="server" OnSelectedIndexChanged="ddlPerfBy_SelectedIndexChanged" AutoPostBack="true"  CssClass="control_dropdown">
                                <%--<asp:ListItem Text="BRANCHES" Value="BRANCH"></asp:ListItem>
                                <asp:ListItem Text="SALES ENGINEERS" Value="SE"></asp:ListItem>
                                <asp:ListItem Text="CUSTOMERS" Value="CUSTOMERS"></asp:ListItem>
                                <asp:ListItem Text="CHANNEL PARTNERS" Value="CP"></asp:ListItem>
                                <asp:ListItem Text="GOLD PRODUCTS" Value="GOLD"></asp:ListItem>
                                <asp:ListItem Text="PRODUCT FAMILY" Value="FAMILY"></asp:ListItem>--%>
                            </asp:DropDownList>
                           </div>
                        </div>
                    <div  class="col-md-6" runat="server" id="divBranch">
                        <div class="col-md-3">
                    <h6 style="color: #006780; padding-left: 4px;">BRANCH</h6></div><div class="col-md-9">
                    <asp:ListBox runat="server" CssClass="control_dropdown" style="width:50%;" ID="BranchList" SelectionMode="Multiple" OnSelectedIndexChanged="BranchList_SelectedIndexChanged" AutoPostBack="true"></asp:ListBox>
                      </div>
               </div>
                    
                </div>
                
                <div class="col-md-2" style="float: right;">
                    <ul id="divCter" runat="server" class="btn-info rbtn_panel" style="float: right;">
                        <li><span style="margin-right: 4px; vertical-align: text-bottom;">TAEGUTEC</span>

                            <asp:RadioButton ID="rdBtnTaegutec" AutoPostBack="true" Checked="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                            <span style="margin-right: 4px; margin-left: 4px; vertical-align: text-bottom;">DURACARB</span>
                            <asp:RadioButton ID="rdBtnDuraCab" AutoPostBack="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                        </li>
                    </ul>
                </div>
            


            </div>


            <div class="col-md-12">
                <div class="col-md-4" id="ChartConsolidated"></div>
                <div class="col-md-4">
                   
            <%--        <div class="col-md-6">--%>
                         <div id="ChartBranchAch" style="display: none;"></div>
                        <div id="ChartTopSEAch" style="display: none;"></div>
                        <div id="ChartTopCustAch" style="display: none;"></div>
                        <div id="ChartTopCPAch" style="display: none;"></div>
                    <div id="ChartFamAch" style="display: none;"></div>
                    </div>
                    <div class="col-md-4"> 
                        <div id="ChartBranchGrowth" style="display: none;"></div>
                        <div id="ChartTopSEGrowth" style="display: none;"></div>
                    <div id="ChartTopCustGrowth" style="display: none;"></div>
                    <div id="ChartTopCPGrowth" style="display: none;"></div>
                    <div id="ChartFamGrowth" style="display: none;"></div>
           <%--     </div>--%>
                    </div>
                <%--SE GRAPHS--%>
                    <div class="col-md-2"></div>
                    <div class="col-md-4"> 
                        <div id="ChartBottomSEAch" style="display: none;"></div>
                        <div id="ChartBottomCustAch" style="display: none;"></div>
                        <div id="ChartBottomCPAch" style="display: none;"></div>
                        </div>
                    <div class="col-md-4">
                        <div id="ChartBottomSEGrowth" style="display: none;"></div>
                        <div id="ChartBottomCustGrowth" style="display: none;"></div>
                        <div id="ChartBottomCPGrowth" style="display: none;"></div>
                        </div>
                <div class="col-md-2"></div>
                <%--CUSTOMER GRAPHS--%>

                <%--CP GRAPHS--%>
            
                
                <%--<div class="portlet" id="divcnsldtd" runat="server" style="display: none;">
                    <div class="portlet-body shadow" style="background: #f1f1f1;">
                        <div class="col-md-12 newa">
                            <h4 style="margin-bottom: -10px; font-family: arial; text-transform: uppercase; text-shadow: 5px solid #ccc;">CONSOLIDATED VIEW</h4>

                            <div class="col-md-12" style="margin-top: 15px;">
                                <asp:Chart ID="bargraphcnsldtd" runat="server" EnableViewState="true" Width="1000" Style="max-width: 100%">

                                    <ChartAreas>
                                        <asp:ChartArea Name="ChartArea1">
                                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap">
                                                <MajorGrid LineWidth="0" />
                                            </AxisX>
                                            <AxisY>
                                                <MajorGrid LineWidth="0" />
                                            </AxisY>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                    <Legends>
                                        <asp:Legend></asp:Legend>
                                    </Legends>

                                </asp:Chart>
                            </div>
                        </div>
                    </div>
                </div>--%>
                <%-- CONSOLIDATED VIEW END--%>


                <%--BRNACHES START--%>
                <div class="portlet box new" id="divbranches" runat="server">
                    <div class="portlet-title">
                        <div class="caption" style="float: left !important;">
                            <img src="images/icon_graph_bottom.PNG" style="padding-right: 5px;">BRANCH PERFORMANCE
                        </div>
                    </div>
                    <div class="portlet-body shadow" style="background: #f1f1f1;">
                        <div class="col-md-12 newa1">
                            <%-- <div class="col-md-6" style="margin-top: 15px;">
                                <asp:Chart ID="bargraphtopbranches" BackColor="#E1E1E1" runat="server" EnableViewState="true" Width="500px" Style="max-width: 100%" CssClass="dg-picture-zoom">
                                    <Titles>
                                        <asp:Title Text=" PERFORMANCE BY PRO-RATA ACH% " TextStyle="Shadow"></asp:Title>
                                    </Titles>
                                    <Series>
                                        <asp:Series Name="Ach" ToolTip="PRO-RATA ACH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" TitleFont="14" Interval="1">
                                                <LabelStyle Font="Serif" ForeColor="#333333" />

                                                <MajorGrid LineWidth="0" />
                                            </AxisX>
                                            <AxisY>
                                                <MajorGrid LineWidth="0" />
                                            </AxisY>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                   
                                    <BorderSkin PageColor="Transparent"
                                        SkinStyle="Emboss" />
                                </asp:Chart>


                            </div>

                            <div class="col-md-6" style="margin-top: 15px;">


                                <asp:Chart ID="bargraphtopbranches_growth" BackColor="#E1E1E1" runat="server" EnableViewState="true" Width="500" Style="max-width: 100%" CssClass="dg-picture-zoom">
                                    <Titles>
                                        <asp:Title Text=" PERFORMANCE BY GROWTH% " TextStyle="Shadow"></asp:Title>
                                    </Titles>
                                    <Series>
                                        <asp:Series Name="Ach" ToolTip="GROWTH  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" TitleFont="14" Interval="1">
                                                <LabelStyle Font="Serif" ForeColor="#333333" />

                                                <MajorGrid LineWidth="0" />
                                            </AxisX>
                                            <AxisY>
                                                <MajorGrid LineWidth="0" />
                                            </AxisY>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                   
                                    <BorderSkin PageColor="Transparent"
                                        SkinStyle="Emboss" />
                                </asp:Chart>


                            </div>--%>

                            <div class="col-md-12">

                                <asp:GridView ID="gridtopbranches" runat="server" Style="width: 100%; border:0px" AutoGenerateColumns="false" ShowHeader="true" OnRowDataBound="gridheader_RowDataBound" AllowSorting="true">
                                    <Columns>
                                        <asp:TemplateField HeaderText="BRANCH" ItemStyle-Width="125" HeaderStyle-CssClass="HeadergridAll" SortExpression="Name">
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            <HeaderTemplate>
                                                <asp:Label ID="Name" runat="server" Text="BRANCH"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_branch" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="2014 SALES" ItemStyle-Width="80" ItemStyle-CssClass="number" HeaderStyle-CssClass="HeadergridAll" SortExpression="sales_value_year_0">
                                            <ItemStyle HorizontalAlign="right" VerticalAlign="Middle" />
                                            <HeaderTemplate>
                                                <asp:Label ID="sales_value_year_0" runat="server" Text="2014 SALES"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_sales" runat="server" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="BUDGET" ItemStyle-Width="80" ItemStyle-CssClass="number" HeaderStyle-CssClass="HeadergridAll" SortExpression="budget">
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            <HeaderTemplate>
                                                <asp:Label ID="budget" runat="server" Text="BUDGET"></asp:Label>

                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_budget" runat="server" Text='<%# Eval("budget") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="YTD PLAN" ItemStyle-Width="80" ItemStyle-CssClass="number" HeaderStyle-CssClass="HeadergridAll" SortExpression="ytd_plan">
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            <HeaderTemplate>
                                                <asp:Label ID="ytd_plan" runat="server" Text="YTD PLAN"></asp:Label>

                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ytdplan" runat="server" Text='<%# Eval("ytd_plan") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="SALES YTD" ItemStyle-Width="80" ItemStyle-CssClass="number" HeaderStyle-CssClass="HeadergridAll" SortExpression="ytd">
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            <HeaderTemplate>
                                                <asp:Label ID="ytd" runat="server" Text="SALES YTD"></asp:Label>

                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ytd" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>


                                        <asp:TemplateField HeaderText="Ask.Rate" ItemStyle-Width="80" ItemStyle-CssClass="number" HeaderStyle-CssClass="HeadergridAll" SortExpression="arate">
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            <HeaderTemplate>
                                                <asp:Label ID="arate" runat="server" Text="Ask.Rate"></asp:Label>

                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_asktate" runat="server" Text='<%# Eval("arate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="%" ItemStyle-Width="80" ItemStyle-CssClass="number" HeaderStyle-CssClass="HeadergridAll" SortExpression="ach">
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            <HeaderTemplate>
                                                <asp:Label ID="ach" runat="server" Text="%"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ach" runat="server" Text='<%# Eval("ach") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="%" ItemStyle-Width="80" ItemStyle-CssClass="number" HeaderStyle-CssClass="HeadergridAll" SortExpression="growth">
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            <HeaderTemplate>
                                                <asp:Label ID="growth" runat="server" Text="%"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_growth" runat="server" Text='<%# Eval("growth") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                    </Columns>
                                </asp:GridView>

                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>

                </div>

                <%--BRNACHES END--%>


                <%--  SALES ENGINEERS START--%>

                <div class="portlet box new" id="divSE" runat="server">
                    <div class="portlet-title">
                        <div class="caption" style="float: left !important;">SALES ENGINEER PERFORMANCE</div>
                    </div>
                    <div class="portlet-body shadow" style="background: #f1f1f1;">
                        <div class="col-md-12 newa1">
                          <%--  <h4 style="margin-bottom: -10px; font-family: arial; text-transform: uppercase; text-shadow: 5px solid #ccc;">
                                <img src="images/icon_graph_bottom.PNG" style="padding-right: 5px; margin-top: -12px">Top 5 Sales Engineers</h4>
                            <div class="col-md-6" style="margin-top: 15px;">
                                <asp:Chart ID="bargraphtopse" BackColor="LightGray" runat="server" EnableViewState="true" Width="500" Style="max-width: 100%" CssClass="dg-picture-zoom">
                                    <Titles>
                                        <asp:Title Text=" PERFORMANCE BY PRO-RATA ACH% " TextStyle="Shadow"></asp:Title>
                                    </Titles>
                                    <Series>
                                        <asp:Series Name="Ach" ToolTip="PRO-RATA ACH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                                <MajorGrid LineWidth="0" />
                                            </AxisX>
                                            <AxisY>
                                                <MajorGrid LineWidth="0" />
                                            </AxisY>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                    
                                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                                        SkinStyle="Emboss" />
                                </asp:Chart>
                            </div>

                            <div class="col-md-6" style="margin-top: 15px;">
                                <asp:Chart ID="bargraphtopse_growth" BackColor="LightGray" runat="server" EnableViewState="true" Width="500" Style="max-width: 100%" CssClass="dg-picture-zoom">
                                    <Titles>
                                        <asp:Title Text=" PERFORMANCE BY GROWTH% " TextStyle="Shadow"></asp:Title>
                                    </Titles>
                                    <Series>
                                        <asp:Series Name="Ach" ToolTip="GROWTH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                                <MajorGrid LineWidth="0" />
                                            </AxisX>
                                            <AxisY>
                                                <MajorGrid LineWidth="0" />
                                            </AxisY>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                    
                                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                                        SkinStyle="Emboss" />
                                </asp:Chart>
                            </div>

                            <div class="clearfix"></div>
                            <h4 style="margin-bottom: -10px; font-family: arial; text-transform: uppercase; text-shadow: 5px solid #ccc;">
                                <img src="images/icon_graph_top.PNG" style="padding-right: 5px; margin-top: -12px">Bottom 5 Sales Engineers</h4>
                            <div class="col-md-6" style="margin-top: 15px;">
                                <asp:Chart ID="bargraphbottomse" BackColor="LightGray" runat="server" EnableViewState="true" Width="500" Style="max-width: 100%" CssClass="dg-picture-zoom">
                                    <Titles>
                                        <asp:Title Text=" PERFORMANCE BY PRO-RATA ACH% " TextStyle="Shadow"></asp:Title>
                                    </Titles>
                                    <Series>
                                        <asp:Series Name="Ach" ToolTip="PRO-RATA ACH% :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                                <MajorGrid LineWidth="0" />
                                            </AxisX>
                                            <AxisY>
                                                <MajorGrid LineWidth="0" />
                                            </AxisY>
                                        </asp:ChartArea>
                                    </ChartAreas>

                                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                                        SkinStyle="Emboss" />
                                </asp:Chart>
                            </div>

                            <div class="col-md-6" style="margin-top: 15px;">
                                <asp:Chart ID="bargraphbottomse_growth" BackColor="LightGray" runat="server" EnableViewState="true" Width="500" Style="max-width: 100%" CssClass="dg-picture-zoom">
                                    <Titles>
                                        <asp:Title Text=" PERFORMANCE BY GROWTH% " TextStyle="Shadow"></asp:Title>
                                    </Titles>
                                    <Series>
                                        <asp:Series Name="Ach" ToolTip="GROWTH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                                <MajorGrid LineWidth="0" />
                                            </AxisX>
                                            <AxisY>
                                                <MajorGrid LineWidth="0" />
                                            </AxisY>
                                        </asp:ChartArea>
                                    </ChartAreas>

                                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                                        SkinStyle="Emboss" />
                                </asp:Chart>
                            </div>
                            <div class="clearfix"></div>--%>
                            <div class="col-md-12">

                                <asp:GridView ID="GridTopSE" runat="server" Style="width: 100%; border:0px" AutoGenerateColumns="false" ShowHeaderWhenEmpty="false" OnRowDataBound="gridheader_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField HeaderText="SALES ENGINEER" ItemStyle-Width="200" HeaderStyle-CssClass="HeadergridAll">
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            <HeaderTemplate>
                                                <asp:Label ID="EngineerName" runat="server" Text="SALES ENGINEER"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Eng" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="BRANCH" ItemStyle-Width="150" HeaderStyle-CssClass="HeadergridAll" ItemStyle-HorizontalAlign="Left">
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_Branch" Style="float: left;" runat="server" Text='<%# Eval("Branch") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="2014 SALES" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_sales" Style="float: right;" runat="server" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="BUDGET" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_budget" Style="float: right;" runat="server" Text='<%# Eval("budget") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="YTD PLAN" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ytdplan" Style="float: right;" runat="server" Text='<%# Eval("ytd_plan") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="SALES YTD" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ytdsale" Style="float: right;" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Ask.Rate" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ask" Style="float: right;" runat="server" Text='<%# Eval("Arate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="%" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ach" Style="float: right;" runat="server" Text='<%# Eval("ach") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="%" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll" SortExpression="growth">
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            <HeaderTemplate>
                                                <asp:Label ID="growth" runat="server" Text="%"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_growth" Style="float: right;" runat="server" Text='<%# Eval("growth") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>

                            </div>
                        </div>

                        <%--  <div class="col-md-12 newa1" >
                  <h4 style="margin-bottom: -10px;  font-family: arial;  text-transform: uppercase; text-shadow:5px solid #ccc;"> <img src="images/icon_graph_top.PNG" style="padding-right: 5px;margin-top: -12px">Bottom 5 Sales Engineers</h4>
                <div class="col-md-6" style="margin-top:15px;">
                   <asp:Chart ID="bargraphbottomse" BackColor="LightGray" runat="server"  EnableViewState="true" Width="500" style="max-width:100%" CssClass="dg-picture-zoom">
                       <Titles> 
                        <asp:Title Text=" PERFORMANCE BY PRO-RATA ACH% " TextStyle="Shadow" ></asp:Title> 
                        </Titles>
                        <Series>
                           <asp:Series Name="Ach" ToolTip="PRO-RATA ACH% :  #VALY"  LabelForeColor="White"  LabelAngle="-90"  CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true"   ChartType="Column"  ></asp:Series>
                       </Series>
                       <ChartAreas>
                           <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1" >
                               <AxisX   LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                   <MajorGrid LineWidth="0" />
                               </AxisX>
                               <AxisY>
                                   <MajorGrid  LineWidth="0" />
                               </AxisY>
                           </asp:ChartArea>
                       </ChartAreas>
                     
                      <BorderSkin BackColor="Transparent" PageColor="Transparent" 
                     SkinStyle="Emboss" />
                  </asp:Chart>
                     </div>

                 <div class="col-md-6" style="margin-top:15px;">
                   <asp:Chart ID="bargraphbottomse_growth" BackColor="LightGray" runat="server"  EnableViewState="true" Width="500" style="max-width:100%" CssClass="dg-picture-zoom">
                       <Titles> 
                        <asp:Title Text=" PERFORMANCE BY GROWTH% " TextStyle="Shadow" ></asp:Title> 
                        </Titles>
                        <Series>
                           <asp:Series Name="Ach" ToolTip="GROWTH%  :  #VALY"  LabelForeColor="White"  LabelAngle="-90"  CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true"   ChartType="Column"  ></asp:Series>
                       </Series>
                       <ChartAreas>
                           <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1" >
                               <AxisX   LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                   <MajorGrid LineWidth="0" />
                               </AxisX>
                               <AxisY>
                                   <MajorGrid  LineWidth="0" />
                               </AxisY>
                           </asp:ChartArea>
                       </ChartAreas>
                    
                      <BorderSkin BackColor="Transparent" PageColor="Transparent" 
                     SkinStyle="Emboss" />
                  </asp:Chart>
                     </div>
                 <div class="clearfix"></div>
                 <div class="col-md-12" style="margin-top:22px;">
               <asp:GridView ID="GridBottomSE" runat="server"  Style="width: 100%;" AutoGenerateColumns="false" ShowHeaderWhenEmpty="false" OnRowDataBound="gridheader_RowDataBound" >
                        <columns>
                            
                           <asp:TemplateField HeaderText=" SALES ENGINEER" ItemStyle-Width="80"  HeaderStyle-CssClass="HeadergridAll"  >
                              <ItemTemplate >
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("Name") %>' ></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                           <asp:TemplateField HeaderText="2014 SALES" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                           <asp:TemplateField HeaderText="BUDGET" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("budget") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                            <asp:TemplateField HeaderText="YTD PLAN" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("ytd_plan") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                            <asp:TemplateField HeaderText="SALES YTD" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                            
                             <asp:TemplateField HeaderText="Ask.Rate" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("arate") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                            <asp:TemplateField HeaderText="%" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("ach") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                            <asp:TemplateField HeaderText="%" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("growth") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                        </columns>
                     </asp:GridView>

               </div>
                 </div>--%>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <%--  SALES ENGINEERS END--%>

                <%-- CUSTOMERS START--%>
                <div class="portlet box new" id="divCustomers" runat="server">
                    <div class="portlet-title">
                        <div class="caption" style="float: left !important;">CUSTOMERS PERFORMANCE</div>
                    </div>
                    <div class="portlet-body shadow" style="background: #f1f1f1;">
                        <div class="col-md-12 newa1">
                         <%--   <h4 style="margin-bottom: -10px; font-family: arial; text-transform: uppercase; text-shadow: 5px solid #ccc;">
                                <img src="images/icon_graph_bottom.PNG" style="padding-right: 5px; margin-top: -12px">Top 5 Customers</h4>
                            <div class="col-md-6" style="margin-top: 15px;">
                                <asp:Chart ID="bargraphtopcustomers" BackColor="LightGray" runat="server" EnableViewState="true" Width="500px" Style="max-width: 100%" CssClass="dg-picture-zoom">
                                    <Titles>
                                        <asp:Title Text=" PERFORMANCE BY PRO-RATA ACH% " TextStyle="Shadow"></asp:Title>
                                    </Titles>
                                    <Series>
                                        <asp:Series Name="Ach" ToolTip="PRO-RATA ACH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                                <MajorGrid LineWidth="0" />
                                            </AxisX>
                                            <AxisY>
                                                <MajorGrid LineWidth="0" />
                                            </AxisY>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                   
                                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                                        SkinStyle="Emboss" />
                                </asp:Chart>
                            </div>

                            <div class="col-md-6" style="margin-top: 15px;">
                                <asp:Chart ID="bargraphtopcustomers_growth" BackColor="LightGray" runat="server" EnableViewState="true" Width="500px" Style="max-width: 100%" CssClass="dg-picture-zoom">
                                    <Titles>
                                        <asp:Title Text=" PERFORMANCE BY GROWTH% " TextStyle="Shadow"></asp:Title>
                                    </Titles>
                                    <Series>
                                        <asp:Series Name="Ach" ToolTip="GROWTH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                                <MajorGrid LineWidth="0" />
                                            </AxisX>
                                            <AxisY>
                                                <MajorGrid LineWidth="0" />
                                            </AxisY>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                    
                                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                                        SkinStyle="Emboss" />
                                </asp:Chart>
                            </div>
                            <div class="clearfix"></div>
                            <h4 style="margin-bottom: -10px; font-family: arial; text-transform: uppercase; text-shadow: 5px solid #ccc;">
                                <img src="images/icon_graph_top.PNG" style="padding-right: 5px; margin-top: -12px">Bottom 5 Customers</h4>
                            <div class="col-md-6" style="margin-top: 15px;">
                                <asp:Chart ID="bargraphbottomCustomers" BackColor="LightGray" runat="server" EnableViewState="true" Width="500px" Style="max-width: 100%" CssClass="dg-picture-zoom">
                                    <Titles>
                                        <asp:Title Text=" PERFORMANCE BY PRO-RATA ACH% " TextStyle="Shadow"></asp:Title>
                                    </Titles>
                                    <Series>
                                        <asp:Series Name="Ach" ToolTip="PRO-RATA ACH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                                <MajorGrid LineWidth="0" />
                                            </AxisX>
                                            <AxisY>
                                                <MajorGrid LineWidth="0" />
                                            </AxisY>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                  
                                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                                        SkinStyle="Emboss" />
                                </asp:Chart>
                            </div>

                            <div class="col-md-6" style="margin-top: 15px;">
                                <asp:Chart ID="bargraphbottomCustomers_growth" BackColor="LightGray" runat="server" EnableViewState="true" Width="500px" Style="max-width: 100%" CssClass="dg-picture-zoom">
                                    <Titles>
                                        <asp:Title Text=" PERFORMANCE BY GROWTH% " TextStyle="Shadow"></asp:Title>
                                    </Titles>
                                    <Series>
                                        <asp:Series Name="Ach" ToolTip="GROWTH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                                <MajorGrid LineWidth="0" />
                                            </AxisX>
                                            <AxisY>
                                                <MajorGrid LineWidth="0" />
                                            </AxisY>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                   
                                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                                        SkinStyle="Emboss" />
                                </asp:Chart>
                            </div>
                            <div class="clearfix"></div>--%>

                            <div class="col-md-12">
                                <asp:GridView ID="GridCustByAch" Style="width: 100%; border:0px" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="false" OnRowDataBound="GridTopCstmrs_RowDataBound">
                                    <Columns>
                                        <asp:TemplateField HeaderText="CUSTOMER NUMBER" ItemStyle-Width="80" ItemStyle-CssClass="number" HeaderStyle-CssClass="HeadergridAll" ItemStyle-HorizontalAlign="Center" SortExpression="customer_number">
                                            <ItemStyle HorizontalAlign="right" VerticalAlign="Middle" />
                                            <HeaderTemplate>
                                                <asp:Label ID="customer_number" runat="server" Text="CUSTOMER NUMBER"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_custnum" runat="server" Text='<%# Eval("customerNumber") %>' Style="text-align: center !important;"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CUSTOMER NAME" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="string">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_custname" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="BRANCH" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="string">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_branch" runat="server" Text='<%# Eval("Branch") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="SE NAME" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="string">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_SE" runat="server" Text='<%# Eval("EngineerName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="2014 SALES" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="number">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_sales" runat="server" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="BUDGET" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="number">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_budget" runat="server" Text='<%# Eval("budget") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="YTD PLAN" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="number">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ytdplan" runat="server" Text='<%# Eval("ytd_plan") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="SALES YTD" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="number">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ytdsale" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Ask.Rate" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="number">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ask" runat="server" Text='<%# Eval("arate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="%" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="number">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ach" runat="server" Text='<%# Eval("ach") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="%" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="number">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_growth" runat="server" Text='<%# Eval("growth") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>

                        <%--<div class="col-md-12 newa1" >
                 <%-- <h4 style="margin-bottom: -10px;  font-family: arial;  text-transform: uppercase; text-shadow:5px solid #ccc;"> <img src="images/icon_graph_top.PNG" style="padding-right: 5px;margin-top: -12px">Bottom 5 Customers</h4>
                <div class="col-md-6" style="margin-top:15px;">
                   <asp:Chart ID="bargraphbottomCustomers" BackColor="LightGray" runat="server"  EnableViewState="true" Width="500px" style="max-width:100%" CssClass="dg-picture-zoom" >
                         <Titles> 
                        <asp:Title Text=" PERFORMANCE BY PRO-RATA ACH% " TextStyle="Shadow" ></asp:Title> 
                        </Titles>
                        <Series>
                           <asp:Series Name="Ach" ToolTip="PRO-RATA ACH%  :  #VALY"  LabelForeColor="White"  LabelAngle="-90"  CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true"   ChartType="Column"  ></asp:Series>
                       </Series>
                       <ChartAreas>
                           <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                               <AxisX  LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                   <MajorGrid LineWidth="0" />
                               </AxisX>
                               <AxisY>
                                   <MajorGrid LineWidth="0" />
                               </AxisY>
                           </asp:ChartArea>
                       </ChartAreas>
                      <Legends>
                           <asp:Legend></asp:Legend>
                       </Legends>
                      <BorderSkin BackColor="Transparent" PageColor="Transparent" 
                     SkinStyle="Emboss" />
                  </asp:Chart>
               </div>

                 <div class="col-md-6" style="margin-top:15px;">
                   <asp:Chart ID="bargraphbottomCustomers_growth" BackColor="LightGray" runat="server"  EnableViewState="true" Width="500px" style="max-width:100%" CssClass="dg-picture-zoom" >
                      <Titles> 
                        <asp:Title Text=" PERFORMANCE BY GROWTH% " TextStyle="Shadow" ></asp:Title> 
                        </Titles>
                        <Series>
                           <asp:Series Name="Ach" ToolTip="GROWTH%  :  #VALY"  LabelForeColor="White"  LabelAngle="-90"  CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true"   ChartType="Column"  ></asp:Series>
                       </Series>
                       <ChartAreas>
                           <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                               <AxisX  LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                   <MajorGrid LineWidth="0" />
                               </AxisX>
                               <AxisY>
                                   <MajorGrid LineWidth="0" />
                               </AxisY>
                           </asp:ChartArea>
                       </ChartAreas>
                     <Legends>
                           <asp:Legend></asp:Legend>
                       </Legends>
                      <BorderSkin BackColor="Transparent" PageColor="Transparent" 
                     SkinStyle="Emboss" />
                  </asp:Chart>
               </div>
                 <div class="clearfix"> </div>
                 <div class="col-md-12" style="margin-top:22px;">
               <asp:GridView ID="GridBottomCstmrs"  Style="width: 100%;" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="false" OnRowDataBound="GridTopCstmrs_RowDataBound"  >
                        <columns>
                            <asp:TemplateField HeaderText="CUSTOMER NUMBER" ItemStyle-Width="80"  HeaderStyle-CssClass="HeadergridAll"  >
                              <ItemTemplate >
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("customerNumber") %>' ></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField> 
                           <asp:TemplateField HeaderText="CUSTOMER NAME" ItemStyle-Width="80"  HeaderStyle-CssClass="HeadergridAll"  >
                              <ItemTemplate >
                                 <asp:Label  ID="lbl1" style="text-align:left !important;" runat="server" Text='<%# Eval("Name") %>' ></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                           <asp:TemplateField HeaderText="2014 SALES" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                           <asp:TemplateField HeaderText="BUDGET" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("budget") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                             <asp:TemplateField HeaderText="YTD PLAN" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("ytd_plan") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                            <asp:TemplateField HeaderText="SALES YTD" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                           
                             <asp:TemplateField HeaderText="Ask.Rate" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("arate") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                            <asp:TemplateField HeaderText="%" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("ach") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                            <asp:TemplateField HeaderText="%" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("growth") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                        </columns>
                     </asp:GridView>

                     </div>
                 </div>--%>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <%-- CUSTOMERS END--%>

                <%-- CHANNEL PARTNERS START--%>
                <div class="portlet box new" id="divCP" runat="server">
                    <div class="portlet-title">
                        <div class="caption" style="float: left !important;">CHANNEL PARTNERS PERFORMANCE</div>
                    </div>
                    <div class="portlet-body shadow" style="background: #f1f1f1;">
                        <div class="col-md-12 newa1">
                          <%--  <h4 style="margin-bottom: -10px; font-family: arial; text-transform: uppercase; text-shadow: 5px solid #ccc;">
                                <img src="images/icon_graph_bottom.PNG" style="padding-right: 5px; margin-top: -12px">Top 5 Channel  Partners</h4>
                            <div class="col-md-6" style="margin-top: 15px;">
                                <asp:Chart ID="bargraphtopcp" BackColor="LightGray" runat="server" EnableViewState="true" Width="500px" Style="max-width: 100%" CssClass="dg-picture-zoom">
                                    <Titles>
                                        <asp:Title Text=" PERFORMANCE BY PRO-RATA ACH% " TextStyle="Shadow"></asp:Title>
                                    </Titles>
                                    <Series>
                                        <asp:Series Name="Ach" ToolTip="PRO-RATA ACH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                                <MajorGrid LineWidth="0" />
                                            </AxisX>
                                            <AxisY>
                                                <MajorGrid LineWidth="0" />
                                            </AxisY>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                    
                                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                                        SkinStyle="Emboss" />
                                </asp:Chart>
                            </div>

                            <div class="col-md-6" style="margin-top: 15px;">
                                <asp:Chart ID="bargraphtopcp_growth" BackColor="LightGray" runat="server" EnableViewState="true" Width="500px" Style="max-width: 100%" CssClass="dg-picture-zoom">
                                    <Titles>
                                        <asp:Title Text=" PERFORMANCE BY GROWTH% " TextStyle="Shadow"></asp:Title>
                                    </Titles>
                                    <Series>
                                        <asp:Series Name="Ach" ToolTip="GROWTH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                                <MajorGrid LineWidth="0" />
                                            </AxisX>
                                            <AxisY>
                                                <MajorGrid LineWidth="0" />
                                            </AxisY>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                   
                                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                                        SkinStyle="Emboss" />
                                </asp:Chart>
                            </div>
                            <div class="clearfix"></div>
                            <h4 style="margin-bottom: -10px; font-family: arial; text-transform: uppercase; text-shadow: 5px solid #ccc;">
                                <img src="images/icon_graph_top.PNG" style="padding-right: 5px; margin-top: -12px">Bottom 5 Channel  Partners</h4>
                            <div class="col-md-6" style="margin-top: 15px;">
                                <asp:Chart ID="bargraphbottomcp" BackColor="LightGray" runat="server" EnableViewState="true" Width="500px" Style="max-width: 100%" CssClass="dg-picture-zoom">
                                    <Titles>
                                        <asp:Title Text=" PERFORMANCE BY PRO-RATA ACH% " TextStyle="Shadow"></asp:Title>
                                    </Titles>
                                    <Series>
                                        <asp:Series Name="Ach" ToolTip="PRO-RATA ACH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                                <MajorGrid LineWidth="0" />
                                            </AxisX>
                                            <AxisY>
                                                <MajorGrid LineWidth="0" />
                                            </AxisY>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                    
                                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                                        SkinStyle="Emboss" />
                                </asp:Chart>
                            </div>

                            <div class="col-md-6" style="margin-top: 15px;">
                                <asp:Chart ID="bargraphbottomcp_growth" BackColor="LightGray" runat="server" EnableViewState="true" Width="500px" Style="max-width: 100%" CssClass="dg-picture-zoom">
                                    <Titles>
                                        <asp:Title Text=" PERFORMANCE BY GROWTH% " TextStyle="Shadow"></asp:Title>
                                    </Titles>
                                    <Series>
                                        <asp:Series Name="Ach" ToolTip="GROWTH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                                <MajorGrid LineWidth="0" />
                                            </AxisX>
                                            <AxisY>
                                                <MajorGrid LineWidth="0" />
                                            </AxisY>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                   
                                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                                        SkinStyle="Emboss" />
                                </asp:Chart>
                            </div>

                            <div class="clearfix"></div>--%>
                            <div class="col-md-12">

                                <asp:GridView ID="GridTopCP" Style="width: 100%; border:0px" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="false" OnRowDataBound="GridTopCstmrs_RowDataBound">
                                    <Columns>

                                        <asp:TemplateField HeaderText="CHANNEL PARTNER  NUMBER" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll" SortExpression="customer_number">
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            <HeaderTemplate>
                                                <asp:Label ID="customer_number" runat="server" Text="CHANNEL PARTNER  NUMBER"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_custnum" runat="server" Text='<%# Eval("customerNumber") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="CHANNEL PARTNER NAME" ItemStyle-Width="180" HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="string">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_custname" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="BRANCH" ItemStyle-Width="180" HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="string">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_branch" runat="server" Text='<%# Eval("Branch") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="SE NAME" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="string">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_SE" runat="server" Text='<%# Eval("EngineerName") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="2014 SALES" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_sales" runat="server" Style="float: right;" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="BUDGET" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_budget" runat="server" Style="float: right;" Text='<%# Eval("budget") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="YTD PLAN" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ytdplan" runat="server" Style="float: right;" Text='<%# Eval("ytd_plan") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="SALES YTD" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ytdsale" runat="server" Style="float: right;" Text='<%# Eval("ytd") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Ask.Rate" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ask" runat="server" Style="float: right;" Text='<%# Eval("arate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="%" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_ach" runat="server" Style="float: right;" Text='<%# Eval("ach") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="%" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_growth" runat="server" Style="float: right;" Text='<%# Eval("growth") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>

                            </div>
                        </div>

                        <%-- <div class="col-md-12 newa1" >
                  <h4 style="margin-bottom: -10px;  font-family: arial;  text-transform: uppercase; text-shadow:5px solid #ccc;"> <img src="images/icon_graph_top.PNG" style="padding-right: 5px;margin-top: -12px">Bottom 5 Channel  Partners</h4>
                <div class="col-md-6" style="margin-top:15px;">
                   <asp:Chart ID="bargraphbottomcp" BackColor="LightGray" runat="server"  EnableViewState="true" Width="500px" style="max-width:100%" CssClass="dg-picture-zoom" >
                      <Titles> 
                        <asp:Title Text=" PERFORMANCE BY PRO-RATA ACH% " TextStyle="Shadow" ></asp:Title> 
                        </Titles>
                        <Series>
                           <asp:Series Name="Ach" ToolTip="PRO-RATA ACH%  :  #VALY"  LabelForeColor="White"  LabelAngle="-90"  CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true"   ChartType="Column"  ></asp:Series>
                       </Series>
                       <ChartAreas>
                           <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                               <AxisX   LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                   <MajorGrid LineWidth="0" />
                               </AxisX>
                               <AxisY>
                                   <MajorGrid LineWidth="0" />
                               </AxisY>
                           </asp:ChartArea>
                       </ChartAreas>
                     
                    <BorderSkin BackColor="Transparent" PageColor="Transparent" 
                     SkinStyle="Emboss" />  
                  </asp:Chart>
               </div>

                 <div class="col-md-6" style="margin-top:15px;">
                   <asp:Chart ID="bargraphbottomcp_growth" BackColor="LightGray" runat="server"  EnableViewState="true" Width="500px" style="max-width:100%" CssClass="dg-picture-zoom" >
                      <Titles> 
                        <asp:Title Text=" PERFORMANCE BY GROWTH% " TextStyle="Shadow" ></asp:Title> 
                        </Titles>
                        <Series>
                           <asp:Series Name="Ach" ToolTip="GROWTH%  :  #VALY"  LabelForeColor="White"  LabelAngle="-90"  CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true"   ChartType="Column"  ></asp:Series>
                       </Series>
                       <ChartAreas>
                           <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                               <AxisX   LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                   <MajorGrid LineWidth="0" />
                               </AxisX>
                               <AxisY>
                                   <MajorGrid LineWidth="0" />
                               </AxisY>
                           </asp:ChartArea>
                       </ChartAreas>
                   
                    <BorderSkin BackColor="Transparent" PageColor="Transparent" 
                     SkinStyle="Emboss" />  
                  </asp:Chart>
               </div>

                 <div class="clearfix"></div>
                 <div class="col-md-12" style="margin-top:22px;">
               <asp:GridView ID="GridBottomCP" Style="width: 100%;" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="false" OnRowDataBound="GridTopCstmrs_RowDataBound"  >
                        <columns>
                            <asp:TemplateField HeaderText="CHANNEL PARTNER  NUMBER" ItemStyle-Width="80"  HeaderStyle-CssClass="HeadergridAll"  >
                              <ItemTemplate >
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("customerNumber") %>' ></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                           <asp:TemplateField HeaderText="CHANNEL PARTNER NAME" ItemStyle-Width="80"  HeaderStyle-CssClass="HeadergridAll"  >
                              <ItemTemplate >
                                 <asp:Label  ID="lbl1" style="text-align:left !important;" runat="server" Text='<%# Eval("Name") %>' ></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                           <asp:TemplateField HeaderText="2014 SALES" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                           <asp:TemplateField HeaderText="BUDGET" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("budget") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                            <asp:TemplateField HeaderText="YTD PLAN" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("ytd_plan") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                            <asp:TemplateField HeaderText="SALES YTD" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="Ask.Rate" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("arate") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                            <asp:TemplateField HeaderText="%" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("ach") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                             
                            <asp:TemplateField HeaderText="%" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("growth") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>

                        </columns>
                     </asp:GridView>

                 </div>
                 </div>--%>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <%-- CHANNEL PARTNERS END--%>

                <%-- GOLD BRanches START--%>
                <div id="divGold" runat="server">
                    <div class="portlet box new" id="divGoldBranch" runat="server">
                        <div class="portlet-title">
                            <div class="caption" style="float: left !important;">
                                <img src="images/icon_graph_bottom.PNG" style="padding-right: 5px;">BRANCH PERFORMANCE
                            </div>
                        </div>
                        <div class="portlet-body shadow" style="background: #f1f1f1;">
                            <div class="col-md-12 newa1">
                               <%-- <h4 style="margin-bottom: -10px; font-family: arial; text-transform: uppercase; text-shadow: 5px solid #ccc;"></h4>
                                <div class="col-md-6" style="margin-top: 15px;">
                                    <asp:Chart ID="bargraphtopgoldbranches" BackColor="#E1E1E1" runat="server" EnableViewState="true" Width="500px" Style="max-width: 100%" CssClass="dg-picture-zoom">
                                        <Titles>
                                            <asp:Title Text=" PERFORMANCE BY BUDGET " TextStyle="Shadow"></asp:Title>
                                        </Titles>
                                        <Series>
                                            <asp:Series Name="Ach" ToolTip="BUDGET VALUE  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                                                <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" TitleFont="14" Interval="1">
                                                    <LabelStyle Font="Serif" ForeColor="#333333" />

                                                    <MajorGrid LineWidth="0" />
                                                </AxisX>
                                                <AxisY>
                                                    <MajorGrid LineWidth="0" />
                                                </AxisY>
                                            </asp:ChartArea>
                                        </ChartAreas>
                                       
                                        <BorderSkin PageColor="Transparent"
                                            SkinStyle="Emboss" />
                                    </asp:Chart>

                                </div>
                                <div class="col-md-6" style="margin-top: 15px;">
                                    <asp:Chart ID="bargraphtopgoldbranches_growth" BackColor="#E1E1E1" runat="server" EnableViewState="true" Width="500px" Style="max-width: 100%" CssClass="dg-picture-zoom">
                                        <Titles>
                                            <asp:Title Text=" PERFORMANCE BY GROWTH% " TextStyle="Shadow"></asp:Title>
                                        </Titles>
                                        <Series>
                                            <asp:Series Name="Ach" ToolTip="GROWTH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                                                <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" TitleFont="14" Interval="1">
                                                    <LabelStyle Font="Serif" ForeColor="#333333" />

                                                    <MajorGrid LineWidth="0" />
                                                </AxisX>
                                                <AxisY>
                                                    <MajorGrid LineWidth="0" />
                                                </AxisY>
                                            </asp:ChartArea>
                                        </ChartAreas>
                                        
                                        <BorderSkin PageColor="Transparent"
                                            SkinStyle="Emboss" />
                                    </asp:Chart>

                                </div>

                                <div class="clearfix"></div>--%>
                                <div class="col-md-12">

                                    <asp:GridView ID="GridTopGoldBranches" runat="server" Style="width: 100%; border:0px" AutoGenerateColumns="false" ShowHeaderWhenEmpty="false" OnRowDataBound="gridheader_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="BRANCH" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll" SortExpression="Name">
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                <HeaderTemplate>
                                                    <asp:Label ID="Name" runat="server" Text="BRANCH"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_branch" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="2014 SALES" ItemStyle-Width="80" ItemStyle-CssClass="number" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_sales" Style="float: right;" runat="server" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="BUDGET" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_budget" Style="float: right;" runat="server" Text='<%# Eval("budget") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="YTD PLAN" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_ytdplan" Style="float: right;" runat="server" Text='<%# Eval("ytd_plan") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="SALES YTD" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_ytdsale" Style="float: right;" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Ask.Rate" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_ask" Style="float: right;" runat="server" Text='<%# Eval("arate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="%" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_ach" Style="float: right;" runat="server" Text='<%# Eval("ach") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="%" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_growth" Style="float: right;" runat="server" Text='<%# Eval("growth") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>

                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>

                    </div>

                    <%--GOLD BRANCHES #END--%>
                    <%--SE-START--%>
                    <div class="portlet box new" id="divGoldSe" runat="server">
                        <div class="portlet-title">
                            <div class="caption" style="float: left !important;">SALES ENGINEER PERFORMANCE</div>
                        </div>
                        <div class="portlet-body shadow" style="background: #f1f1f1;">
                            <div class="col-md-12 newa1">
                                <div class="col-md-3" id="ChartGoldTopSEAch"></div>
                                <div class="col-md-3" id="ChartGoldTopSEGrowth"></div>
                                <div class="col-md-3" id="ChartGoldBottomSEAch"></div>
                                <div class="col-md-3" id="ChartGoldBottomSEGrowth"></div>
                                <%--<h4 style="margin-bottom: -10px; font-family: arial; text-transform: uppercase; text-shadow: 5px solid #ccc;">
                                    <img src="images/icon_graph_bottom.PNG" style="padding-right: 5px; margin-top: -12px">Top 5 SALES ENGINEERS</h4>
                                <div class="col-md-6" style="margin-top: 15px;">

                                    <asp:Chart ID="bargraphTopGoldSE" BackColor="#E1E1E1" runat="server" EnableViewState="true" Width="500px" Style="max-width: 100%" CssClass="dg-picture-zoom">
                                        <Titles>
                                            <asp:Title Text=" PERFORMANCE BY BUDGET " TextStyle="Shadow"></asp:Title>
                                        </Titles>

                                        <Series>
                                            <asp:Series Name="Ach" ToolTip="BUDGET VALUE  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                                                <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" TitleFont="14" Interval="1">
                                                    <LabelStyle Font="Serif" ForeColor="#333333" />

                                                    <MajorGrid LineWidth="0" />
                                                </AxisX>
                                                <AxisY>
                                                    <MajorGrid LineWidth="0" />
                                                </AxisY>
                                            </asp:ChartArea>
                                        </ChartAreas>
                                     
                                        <BorderSkin PageColor="Transparent"
                                            SkinStyle="Emboss" />
                                    </asp:Chart>


                                </div>
                                <div class="col-md-6" style="margin-top: 15px;">

                                    <asp:Chart ID="bargraphTopGoldSE_growth" BackColor="#E1E1E1" runat="server" EnableViewState="true" Width="500px" Style="max-width: 100%" CssClass="dg-picture-zoom">
                                        <Titles>
                                            <asp:Title Text=" PERFORMANCE BY GROWTH% " TextStyle="Shadow"></asp:Title>
                                        </Titles>
                                        <Series>
                                            <asp:Series Name="Ach" ToolTip="GROWTH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                                                <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" TitleFont="14" Interval="1">
                                                    <LabelStyle Font="Serif" ForeColor="#333333" />

                                                    <MajorGrid LineWidth="0" />
                                                </AxisX>
                                                <AxisY>
                                                    <MajorGrid LineWidth="0" />
                                                </AxisY>
                                            </asp:ChartArea>
                                        </ChartAreas>
                                     
                                        <BorderSkin PageColor="Transparent"
                                            SkinStyle="Emboss" />
                                    </asp:Chart>


                                </div>

                                <div class="clearfix"></div>
                                <h4 style="margin-bottom: -10px; font-family: arial; text-transform: uppercase; text-shadow: 5px solid #ccc;">
                                    <img src="images/icon_graph_top.PNG" style="padding-right: 5px; margin-top: -12px">BOTTOM 5 SALES ENGINEERS</h4>
                                <div class="col-md-6" style="margin-top: 15px;">

                                    <asp:Chart ID="bargraphBtmGoldSE" BackColor="#E1E1E1" runat="server" EnableViewState="true" Width="500" Style="max-width: 100%" CssClass="dg-picture-zoom">
                                        <Titles>
                                            <asp:Title Text=" PERFORMANCE BY BUDGET " TextStyle="Shadow"></asp:Title>
                                        </Titles>
                                        <Series>
                                            <asp:Series Name="Ach" ToolTip="BUDGET VALUE  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                                                <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" TitleFont="14" Interval="1">
                                                    <LabelStyle Font="Serif" ForeColor="#333333" />

                                                    <MajorGrid LineWidth="0" />
                                                </AxisX>
                                                <AxisY>
                                                    <MajorGrid LineWidth="0" />
                                                </AxisY>
                                            </asp:ChartArea>
                                        </ChartAreas>

                                        <BorderSkin PageColor="Transparent"
                                            SkinStyle="Emboss" />
                                    </asp:Chart>


                                </div>
                                <div class="col-md-6" style="margin-top: 15px;">

                                    <asp:Chart ID="bargraphBtmGoldSE_growth" BackColor="#E1E1E1" runat="server" EnableViewState="true" Width="500" Style="max-width: 100%" CssClass="dg-picture-zoom">
                                        <Titles>
                                            <asp:Title Text=" PERFORMANCE BY GROWTH% " TextStyle="Shadow"></asp:Title>
                                        </Titles>
                                        <Series>
                                            <asp:Series Name="Ach" ToolTip="GROWTH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                                                <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" TitleFont="14" Interval="1">
                                                    <LabelStyle Font="Serif" ForeColor="#333333" />

                                                    <MajorGrid LineWidth="0" />
                                                </AxisX>
                                                <AxisY>
                                                    <MajorGrid LineWidth="0" />
                                                </AxisY>
                                            </asp:ChartArea>
                                        </ChartAreas>

                                        <BorderSkin PageColor="Transparent"
                                            SkinStyle="Emboss" />
                                    </asp:Chart>


                                </div>

                                <div class="clearfix"></div>--%>
                                <div class="col-md-12">

                                    <asp:GridView ID="GridTopGoldSE" runat="server" Style="width: 100%;  border:0px" AutoGenerateColumns="false" ShowHeaderWhenEmpty="false" OnRowDataBound="gridheader_RowDataBound">
                                        <Columns>

                                            <asp:TemplateField HeaderText="SALES ENGINEER" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll" SortExpression="EngineerName">
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                <HeaderTemplate>
                                                    <asp:Label ID="EngineerName" runat="server" Text="SALES ENGINEER"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_eng" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="BRANCH" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_branch" Style="float: left;" runat="server" Text='<%# Eval("Branch") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="2014 SALES" ItemStyle-Width="80" ItemStyle-CssClass="number" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl1" runat="server" Style="float: right;" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="BUDGET" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl2" runat="server" Style="float: right;" Text='<%# Eval("budget") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="YTD PLAN" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl3" runat="server" Style="float: right;" Text='<%# Eval("ytd_plan") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="SALES YTD" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl4" runat="server" Style="float: right;" Text='<%# Eval("ytd") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Ask.Rate" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl5" runat="server" Style="float: right;" Text='<%# Eval("arate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="%" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl6" runat="server" Style="float: right;" Text='<%# Eval("ach") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="%" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl7" runat="server" Style="float: right;" Text='<%# Eval("growth") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>

                                </div>
                            </div>
                            <%--    <div class="col-md-12 newa" >
                    <h4 style="margin-bottom: -10px;  font-family: arial;  text-transform: uppercase; text-shadow:5px solid #ccc;"> <img src="images/icon_graph_top.PNG" style="padding-right: 5px;margin-top: -12px">BOTTOM 5 SALES ENGINEERS</h4>
                  <div class="col-md-6" style="margin-top:15px;">
                       
                   <asp:Chart ID="bargraphBtmGoldSE" BackColor="#E1E1E1" runat="server"  EnableViewState="true" Width="500" style="max-width:100%" CssClass="dg-picture-zoom" >
                        <Titles> 
                        <asp:Title Text=" PERFORMANCE BY BUDGET " TextStyle="Shadow" ></asp:Title> 
                        </Titles>
                        <Series>
                           <asp:Series Name="Ach" ToolTip="BUDGET VALUE  :  #VALY"  LabelForeColor="White"  LabelAngle="-90"  CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true"   ChartType="Column"  ></asp:Series>
                       </Series>
                       <ChartAreas>
                           <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                               <AxisX  LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" TitleFont="14" Interval="1" >
                                   <LabelStyle Font="Serif"  ForeColor="#333333"   />
                                   
                                   <MajorGrid LineWidth="0" />
                               </AxisX>
                               <AxisY>
                                   <MajorGrid LineWidth="0" />
                               </AxisY>
                           </asp:ChartArea>
                       </ChartAreas>
                      
                     <BorderSkin  PageColor="Transparent" 
                     SkinStyle="Emboss" />
                  </asp:Chart>
                    
                                            
                  </div>
                 <div class="col-md-6" style="margin-top:15px;">
                       
                   <asp:Chart ID="bargraphBtmGoldSE_growth" BackColor="#E1E1E1" runat="server"  EnableViewState="true" Width="500" style="max-width:100%" CssClass="dg-picture-zoom" >
                        <Titles> 
                        <asp:Title Text=" PERFORMANCE BY GROWTH% " TextStyle="Shadow" ></asp:Title> 
                        </Titles>
                        <Series>
                           <asp:Series Name="Ach" ToolTip="GROWTH%  :  #VALY"  LabelForeColor="White"  LabelAngle="-90"  CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true"   ChartType="Column"  ></asp:Series>
                       </Series>
                       <ChartAreas>
                           <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                               <AxisX  LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" TitleFont="14" Interval="1" >
                                   <LabelStyle Font="Serif"  ForeColor="#333333"   />
                                   
                                   <MajorGrid LineWidth="0" />
                               </AxisX>
                               <AxisY>
                                   <MajorGrid LineWidth="0" />
                               </AxisY>
                           </asp:ChartArea>
                       </ChartAreas>
                 
                     <BorderSkin  PageColor="Transparent" 
                     SkinStyle="Emboss" />
                  </asp:Chart>
                    
                                            
                  </div>

                <div class="clearfix"></div>
               <div class="col-md-12" style="margin-top:22px;">
               <asp:GridView ID="GridBtmGoldSE" runat="server"  Style=" width: 100%;" AutoGenerateColumns="false" ShowHeaderWhenEmpty="false" OnRowDataBound="gridheader_RowDataBound" >
                        <columns>
                            
                           <asp:TemplateField HeaderText="SALES ENGINEER" ItemStyle-Width="80"  HeaderStyle-CssClass="HeadergridAll"  >
                              <ItemTemplate >
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("Name") %>' ></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                           <asp:TemplateField HeaderText="2014 SALES" ItemStyle-Width="80" ItemStyle-CssClass="number" HeaderStyle-CssClass="HeadergridAll">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                           <asp:TemplateField HeaderText="BUDGET" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("budget") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                             <asp:TemplateField HeaderText="YTD PLAN" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("ytd_plan") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                            <asp:TemplateField HeaderText="SALES YTD" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                           
                             <asp:TemplateField HeaderText="Ask.Rate" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("arate") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                            <asp:TemplateField HeaderText="%" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("ach") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                            <asp:TemplateField HeaderText="%" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                              <ItemTemplate>
                                 <asp:Label  ID="lbl1" runat="server" Text='<%# Eval("growth") %>'></asp:Label>
                              </ItemTemplate>
                           </asp:TemplateField>
                            
                        </columns>
                     </asp:GridView>
                   </div>
               </div>--%>

                            <div class="clearfix"></div>
                        </div>

                    </div>



                    <%-- SE-END--%>

                    <div class="portlet box new">
                        <div class="portlet-title">
                            <div class="caption" style="float: left !important;">CUSTOMERS PERFORMANCE</div>
                        </div>
                        <div class="portlet-body shadow" style="background: #f1f1f1;">
                            <div class="col-md-12 newa1">
                                <div class="col-md-6" id="ChartGoldTopCustAch"></div>
                                <div class="col-md-6" id="ChartGoldTopCustGrowth"></div>
                            <%--    <h4 style="margin-bottom: -10px; font-family: arial; text-transform: uppercase; text-shadow: 5px solid #ccc;">
                                    <img src="images/icon_graph_bottom.PNG" style="padding-right: 5px; margin-top: -12px">Top 10 Customers</h4>
                                <div class="col-md-6" style="margin-top: 15px;">
                                    <asp:Chart ID="bargraphtopgoldcustomers" BackColor="LightGray" runat="server" EnableViewState="true" Width="500" Style="max-width: 100%" CssClass="dg-picture-zoom">
                                        <Titles>
                                            <asp:Title Text=" PERFORMANCE BY BUDGET " TextStyle="Shadow"></asp:Title>
                                        </Titles>
                                        <Series>
                                            <asp:Series Name="Ach" ToolTip="BUDGET VALUE  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                                                <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                                    <MajorGrid LineWidth="0" />
                                                </AxisX>
                                                <AxisY>
                                                    <MajorGrid LineWidth="0" />
                                                </AxisY>
                                            </asp:ChartArea>
                                        </ChartAreas>
                                        
                                        <BorderSkin BackColor="Transparent" PageColor="Transparent"
                                            SkinStyle="Emboss" />
                                    </asp:Chart>
                                </div>

                                <div class="col-md-6" style="margin-top: 15px;">
                                    <asp:Chart ID="bargraphtopgoldcustomers_growth" BackColor="LightGray" runat="server" EnableViewState="true" Width="500" Style="max-width: 100%" CssClass="dg-picture-zoom">
                                        <Titles>
                                            <asp:Title Text=" PERFORMANCE BY GROWTH% " TextStyle="Shadow"></asp:Title>
                                        </Titles>
                                        <Series>
                                            <asp:Series Name="Ach" ToolTip="GROWTH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                                                <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                                    <MajorGrid LineWidth="0" />
                                                </AxisX>
                                                <AxisY>
                                                    <MajorGrid LineWidth="0" />
                                                </AxisY>
                                            </asp:ChartArea>
                                        </ChartAreas>
                                      
                                        <BorderSkin BackColor="Transparent" PageColor="Transparent"
                                            SkinStyle="Emboss" />
                                    </asp:Chart>
                                </div>

                                <div class="clearfix"></div>--%>
                                <div class="col-md-12">

                                    <asp:GridView ID="GridTopGoldCstmrs" Style="width: 100%;  border:0px" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="false" OnRowDataBound="GridTopCstmrs_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="CUSTOMER NUMBER" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll" SortExpression="customer_number">
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                <HeaderTemplate>
                                                    <asp:Label ID="customer_number" runat="server" Text="CUSTOMER NUMBER"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_custnum" runat="server" Text='<%# Eval("customerNumber") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="CUSTOMER NAME" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll" SortExpression="customer_short_name">
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                <HeaderTemplate>
                                                    <asp:Label ID="customer_short_name" runat="server" Text="CUSTOMER NAME"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_custname" Style="text-align: left !important;" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="BRANCH" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_branch" runat="server" Text='<%# Eval("Branch") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="SE NAME" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="string">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_SE" runat="server" Text='<%# Eval("EngineerName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="2014 SALES" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl1" runat="server" Style="float: right;" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="BUDGET" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl2" runat="server" Style="float: right;" Text='<%# Eval("budget") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="YTD PLAN" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl3" runat="server" Style="float: right;" Text='<%# Eval("ytd_plan") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="SALES YTD" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl4" runat="server" Style="float: right;" Text='<%# Eval("ytd") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Ask.Rate" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl5" runat="server" Style="float: right;" Text='<%# Eval("arate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="%" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl6" runat="server" Style="float: right;" Text='<%# Eval("ach") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="%" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl7" runat="server" Style="float: right;" Text='<%# Eval("growth") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>

                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>

                    <%-- GOLD CUSTOMERS END--%>
                    <div class="portlet box new" id="divGoldCP" runat="server">
                        <div class="portlet-title">
                            <div class="caption" style="float: left !important;">CHANNEL PARTNERS PERFORMANCE</div>
                        </div>
                        <div class="portlet-body shadow" style="background: #f1f1f1;">
                            <div class="col-md-12 newa1">
                                <div class="col-md-6" id="ChartGoldTopCPAch"></div>
                                <div class="col-md-6" id="ChartGoldTopCPGrowth"></div>
                               <%-- <h4 style="margin-bottom: -10px; font-family: arial; text-transform: uppercase; text-shadow: 5px solid #ccc;">
                                    <img src="images/icon_graph_bottom.PNG" style="padding-right: 5px; margin-top: -12px">Top 10 Channel Partners</h4>
                                <div class="col-md-6" style="margin-top: 15px;">
                                    <asp:Chart ID="bargraphtopgoldcp" BackColor="LightGray" runat="server" EnableViewState="true" Width="500" Style="max-width: 100%" CssClass="dg-picture-zoom">
                                        <Titles>
                                            <asp:Title Text=" PERFORMANCE BY BUDGET " TextStyle="Shadow"></asp:Title>
                                        </Titles>
                                        <Series>
                                            <asp:Series Name="Ach" ToolTip="BUDGET VALUE  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                                                <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                                    <MajorGrid LineWidth="0" />
                                                </AxisX>
                                                <AxisY>
                                                    <MajorGrid LineWidth="0" />
                                                </AxisY>
                                            </asp:ChartArea>
                                        </ChartAreas>
                                        
                                        <BorderSkin BackColor="Transparent" PageColor="Transparent"
                                            SkinStyle="Emboss" />
                                    </asp:Chart>
                                </div>

                                <div class="col-md-6" style="margin-top: 15px;">
                                    <asp:Chart ID="bargraphtopgoldcp_growth" BackColor="LightGray" runat="server" EnableViewState="true" Width="500" Style="max-width: 100%" CssClass="dg-picture-zoom">
                                        <Titles>
                                            <asp:Title Text=" PERFORMANCE BY GROWTH% " TextStyle="Shadow"></asp:Title>
                                        </Titles>
                                        <Series>
                                            <asp:Series Name="Ach" ToolTip="GROWTH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                                        </Series>
                                        <ChartAreas>
                                            <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                                                <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                                    <MajorGrid LineWidth="0" />
                                                </AxisX>
                                                <AxisY>
                                                    <MajorGrid LineWidth="0" />
                                                </AxisY>
                                            </asp:ChartArea>
                                        </ChartAreas>
                                        
                                        <BorderSkin BackColor="Transparent" PageColor="Transparent"
                                            SkinStyle="Emboss" />
                                    </asp:Chart>
                                </div>

                                <div class="clearfix"></div>--%>
                                <div class="col-md-12">

                                    <asp:GridView ID="GridTopGoldCP" Style="width: 100%;" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="false" OnRowDataBound="GridTopCstmrs_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField HeaderText="CHANNEL PARTNER NUMBER" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll" SortExpression="customer_number">
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                <HeaderTemplate>
                                                    <asp:Label ID="customer_number" runat="server" Text="CHANNEL PARTNER NUMBER"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_custnumber" runat="server" Text='<%# Eval("customerNumber") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="CHANNEL PARTNER NAME" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                <HeaderTemplate>
                                                    <asp:Label ID="customer_short_name" runat="server" Text="CHANNEL PARTNER NAME"></asp:Label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_custname" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="BRANCH" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_branch" runat="server" Text='<%# Eval("Branch") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="SE NAME" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="string">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl_SE" runat="server" Text='<%# Eval("EngineerName") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="2014 SALES" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl1" Style="float: right;" runat="server" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="BUDGET" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl2" Style="float: right;" runat="server" Text='<%# Eval("budget") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="YTD PLAN" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl3" Style="float: right;" runat="server" Text='<%# Eval("ytd_plan") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="SALES YTD" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl4" Style="float: right;" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                            <asp:TemplateField HeaderText="Ask.Rate" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl5" Style="float: right;" runat="server" Text='<%# Eval("arate") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="%" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl6" Style="float: right;" runat="server" Text='<%# Eval("ach") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="%" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                                <ItemTemplate>
                                                    <asp:Label ID="lbl7" Style="float: right;" runat="server" Text='<%# Eval("growth") %>'></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>

                                </div>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <%--GOLD CHANNEL PARTNERS END--%>

                <div class="portlet box new" id="divFamily" runat="server">
                    <div class="portlet-title">
                        <div class="caption" style="float: left !important;">PRODUCT FAMILY PERFORMANCE</div>
                    </div>
                    <div class="portlet-body shadow" style="background: #f1f1f1;">
                        <div class="col-md-12 newa1">
                            <%--<h4 style="margin-bottom: -10px; font-family: arial; text-transform: uppercase; text-shadow: 5px solid #ccc;"></h4>
                            <div class="col-md-6" style="margin-top: 15px;">
                                <asp:Chart ID="bargraphgamily" BackColor="LightGray" runat="server" EnableViewState="true" Width="500" Style="max-width: 100%" CssClass="dg-picture-zoom">
                                    <Titles>
                                        <asp:Title Text=" PERFORMANCE BY PRO-RATA ACH% " TextStyle="Shadow"></asp:Title>
                                    </Titles>
                                    <Series>
                                        <asp:Series Name="Ach" ToolTip="PRO-RATA ACH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap">
                                                <MajorGrid LineWidth="0" />
                                            </AxisX>
                                            <AxisY>
                                                <MajorGrid LineWidth="0" />
                                            </AxisY>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                    
                                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                                        SkinStyle="Emboss" />
                                </asp:Chart>
                            </div>

                            <div class="col-md-6" style="margin-top: 15px;">
                                <asp:Chart ID="bargraphgamily_growth" BackColor="LightGray" runat="server" EnableViewState="true" Width="500" Style="max-width: 100%" CssClass="dg-picture-zoom">
                                    <Titles>
                                        <asp:Title Text=" PERFORMANCE BY GROWTH% " TextStyle="Shadow"></asp:Title>
                                    </Titles>
                                    <Series>
                                        <asp:Series Name="Ach" ToolTip="GROWTH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                                    </Series>
                                    <ChartAreas>
                                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap">
                                                <MajorGrid LineWidth="0" />
                                            </AxisX>
                                            <AxisY>
                                                <MajorGrid LineWidth="0" />
                                            </AxisY>
                                        </asp:ChartArea>
                                    </ChartAreas>
                                    
                                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                                        SkinStyle="Emboss" />
                                </asp:Chart>
                            </div>

                            <div class="clearfix"></div>--%>
                            <div class="col-md-12">
                                <asp:GridView ID="GridFamily" Style="width: 100%;" BackColor="LightGray" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="false" OnRowDataBound="gridheader_RowDataBound">
                                    <Columns>

                                        <asp:TemplateField HeaderText="FAMILY NAME" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll" SortExpression="Name">
                                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                            <HeaderTemplate>
                                                <asp:Label ID="Name" runat="server" Text="FAMILY NAME"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lbl_familyname" runat="server" Text='<%# Eval("Name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="2014 SALES" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl2" Style="float: right;" runat="server" Text='<%# Eval("sales_value_year_0") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="BUDGET" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl3" Style="float: right;" runat="server" Text='<%# Eval("budget") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="YTD PLAN" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl4" Style="float: right;" runat="server" Text='<%# Eval("ytd_plan") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="SALES YTD" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl5" Style="float: right;" runat="server" Text='<%# Eval("ytd") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Ask.Rate" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl6" Style="float: right;" runat="server" Text='<%# Eval("arate") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="%" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl7" Style="float: right;" runat="server" Text='<%# Eval("ach") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="%" ItemStyle-Width="80" HeaderStyle-CssClass="HeadergridAll">
                                            <ItemTemplate>
                                                <asp:Label ID="lbl8" Style="float: right;" runat="server" Text='<%# Eval("growth") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                </div>

            </div>


            <div>
                <div>
                    <asp:HiddenField ID="hdn_Sales_value_year_0" runat="server" />
                    <asp:HiddenField ID="hdn_Sales_ytd_value" runat="server" />
                    <asp:HiddenField ID="hdn_budget_value" runat="server" />
                    <asp:HiddenField ID="hdn_ytd_plan" runat="server" />
                    <asp:HiddenField ID="hdn_achvmnt" runat="server" />
                    <asp:HiddenField ID="hdn_askrate" runat="server" />
                    <asp:HiddenField ID="hdn_NOVALUES" runat="server" />
                    <asp:HiddenField ID="hdn_growth" runat="server" />

                    <asp:HiddenField ID="hdn1_Sales_value_year_0" runat="server" />
                    <asp:HiddenField ID="hdn1_Sales_ytd_value" runat="server" />
                    <asp:HiddenField ID="hdn1_budget_value" runat="server" />
                    <asp:HiddenField ID="hdn1_ytd_plan" runat="server" />
                    <asp:HiddenField ID="hdn1_achvmnt" runat="server" />
                    <asp:HiddenField ID="hdn1_askrate" runat="server" />
                    <asp:HiddenField ID="hdn1_NOVALUES" runat="server" />
                    <asp:HiddenField ID="hdn1_growth" runat="server" />
                </div>

            </div>

            <!-- Branches start-->
            <div id="dvResultPopUp" style="width: 100%; display: none; text-align: center !important;">
                <asp:Chart ID="bargraphtopbranches1" BackColor="#E1E1E1" runat="server" EnableViewState="true" Width="1200px" Height="450px" Style="max-width: 100% !important; max-height: 100% !important;">
                    <Titles>
                        <asp:Title Text=" PERFORMANCE BY PRO-RATA ACH% " TextStyle="Shadow"></asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="Ach" ToolTip="PRO-RATA ACH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" TitleFont="14" Interval="1">
                                <LabelStyle Font="Serif" ForeColor="#333333" />

                                <MajorGrid LineWidth="0" />
                            </AxisX>
                            <AxisY>
                                <MajorGrid LineWidth="0" />
                            </AxisY>
                        </asp:ChartArea>
                    </ChartAreas>
                    <%-- <Legends>
                           <asp:Legend></asp:Legend>
                       </Legends>--%>
                    <BorderSkin PageColor="Transparent"
                        SkinStyle="Emboss" />
                </asp:Chart>
            </div>

            <div id="dvResultPopUp1" style="width: 100%; display: none; text-align: center !important;">

                <asp:Chart ID="bargraphtopbranches_growth1" BackColor="LightGray" runat="server" EnableViewState="true" Width="1200px" Height="450px" Style="max-width: 100% !important; max-height: 100% !important;">
                    <Titles>
                        <asp:Title Text=" PERFORMANCE BY GROWTH% " TextStyle="Shadow"></asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="Ach" ToolTip="GROWTH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="ChartArea1" BackColor="#ACD1E9">
                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" TitleFont="14" Interval="1">

                                <MajorGrid LineWidth="0" />
                            </AxisX>
                            <AxisY>
                                <MajorGrid LineWidth="0" />
                            </AxisY>
                        </asp:ChartArea>
                    </ChartAreas>
                    <%--<Legends>
                           <asp:Legend></asp:Legend>
                       </Legends>--%>
                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                        SkinStyle="Emboss" />
                </asp:Chart>
            </div>
            <!-- Branches END-->

            <!-- Sales Engineers Start-->

            <div id="Divtopse" style="width: 100%; display: none; text-align: center !important;">
                <asp:Chart ID="bargraphtopse1" BackColor="LightGray" runat="server" EnableViewState="true" Width="1200px" Height="450px" Style="max-width: 100% !important; max-height: 100% !important;">
                    <Titles>
                        <asp:Title Text=" PERFORMANCE BY PRO-RATA ACH% " TextStyle="Shadow"></asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="Ach" ToolTip="PRO-RATA ACH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                <MajorGrid LineWidth="0" />
                            </AxisX>
                            <AxisY>
                                <MajorGrid LineWidth="0" />
                            </AxisY>
                        </asp:ChartArea>
                    </ChartAreas>
                    <%-- <Legends>
                           <asp:Legend></asp:Legend>
                       </Legends>--%>
                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                        SkinStyle="Emboss" />
                </asp:Chart>
            </div>

            <div id="Divtopse1" style="width: 100%; display: none; text-align: center !important;">
                <asp:Chart ID="bargraphtopse_growth1" BackColor="LightGray" runat="server" EnableViewState="true" Width="1200px" Height="450px" Style="max-width: 100% !important; max-height: 100% !important;">
                    <Titles>
                        <asp:Title Text=" PERFORMANCE BY GROWTH% " TextStyle="Shadow"></asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="Ach" ToolTip="GROWTH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                <MajorGrid LineWidth="0" />
                            </AxisX>
                            <AxisY>
                                <MajorGrid LineWidth="0" />
                            </AxisY>
                        </asp:ChartArea>
                    </ChartAreas>
                    <%-- <Legends>
                           <asp:Legend></asp:Legend>
                       </Legends>--%>
                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                        SkinStyle="Emboss" />
                </asp:Chart>
            </div>


            <div id="DivSE1" style="width: 100%; display: none; text-align: center !important;">

                <asp:Chart ID="bargraphbottomse1" BackColor="LightGray" runat="server" EnableViewState="true" Width="1200px" Height="450px" Style="max-width: 100% !important; max-height: 100% !important;">
                    <Titles>
                        <asp:Title Text=" PERFORMANCE BY PRO-RATA ACH% " TextStyle="Shadow"></asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="Ach" ToolTip="PRO-RATA ACH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                <MajorGrid LineWidth="0" />
                            </AxisX>
                            <AxisY>
                                <MajorGrid LineWidth="0" />
                            </AxisY>
                        </asp:ChartArea>
                    </ChartAreas>
                    <%--<Legends>
                           <asp:Legend></asp:Legend>
                       </Legends>--%>
                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                        SkinStyle="Emboss" />
                </asp:Chart>

            </div>
            <div id="DivSE2" style="width: 100%; display: none; text-align: center !important;">

                <asp:Chart ID="bargraphbottomse_growth1" BackColor="LightGray" runat="server" EnableViewState="true" Width="1200px" Height="450px" Style="max-width: 100% !important; max-height: 100% !important;">
                    <Titles>
                        <asp:Title Text=" PERFORMANCE BY GROWTH% " TextStyle="Shadow"></asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="Ach" ToolTip="GROWTH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                <MajorGrid LineWidth="0" />
                            </AxisX>
                            <AxisY>
                                <MajorGrid LineWidth="0" />
                            </AxisY>
                        </asp:ChartArea>
                    </ChartAreas>
                    <%--<Legends>
                           <asp:Legend></asp:Legend>
                       </Legends>--%>
                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                        SkinStyle="Emboss" />
                </asp:Chart>

            </div>

            <!-- Sales Engineers END-->


            <!-- Customers Start-->
            <div id="DivCustomerGraph" style="width: 100%; display: none; text-align: center !important;">
                <asp:Chart ID="bargraphtopcustomers1" BackColor="LightGray" runat="server" EnableViewState="true" Width="1200px" Height="450px" Style="max-width: 100% !important; max-height: 100% !important;">
                    <Titles>
                        <asp:Title Text=" PERFORMANCE BY PRO-RATA ACH% " TextStyle="Shadow"></asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="Ach" ToolTip="PRO-RATA ACH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                <MajorGrid LineWidth="0" />
                            </AxisX>
                            <AxisY>
                                <MajorGrid LineWidth="0" />
                            </AxisY>
                        </asp:ChartArea>
                    </ChartAreas>
                    <%-- <Legends>
                           <asp:Legend></asp:Legend>
                       </Legends>--%>
                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                        SkinStyle="Emboss" />
                </asp:Chart>
            </div>
            <div id="DivCustomerGraph_growth" style="width: 100%; display: none; text-align: center !important;">
                <asp:Chart ID="bargraphtopcustomers_growth1" BackColor="LightGray" runat="server" EnableViewState="true" Width="1200px" Height="450px" Style="max-width: 100% !important; max-height: 100% !important;">
                    <Titles>
                        <asp:Title Text=" PERFORMANCE BY GROWTH% " TextStyle="Shadow"></asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="Ach" ToolTip="GROWTH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                <MajorGrid LineWidth="0" />
                            </AxisX>
                            <AxisY>
                                <MajorGrid LineWidth="0" />
                            </AxisY>
                        </asp:ChartArea>
                    </ChartAreas>
                    <%-- <Legends>
                           <asp:Legend></asp:Legend>
                       </Legends>--%>
                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                        SkinStyle="Emboss" />
                </asp:Chart>
            </div>

            <div id="DivCustomerGraph1" style="width: 100%; display: none; text-align: center !important;">
                <asp:Chart ID="bargraphbottomCustomers1" BackColor="LightGray" runat="server" EnableViewState="true" Width="1200px" Height="450px" Style="max-width: 100% !important; max-height: 100% !important;">
                    <Titles>
                        <asp:Title Text=" PERFORMANCE BY PRO-RATA ACH% " TextStyle="Shadow"></asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="Ach" ToolTip="PRO-RATA ACH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                <MajorGrid LineWidth="0" />
                            </AxisX>
                            <AxisY>
                                <MajorGrid LineWidth="0" />
                            </AxisY>
                        </asp:ChartArea>
                    </ChartAreas>
                    <%-- <Legends>
                           <asp:Legend></asp:Legend>
                       </Legends>--%>
                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                        SkinStyle="Emboss" />
                </asp:Chart>
            </div>

            <div id="DivCustomerGraph_growth1" style="width: 100%; display: none; text-align: center !important;">
                <asp:Chart ID="bargraphbottomCustomers_growth1" BackColor="LightGray" runat="server" EnableViewState="true" Width="1200px" Height="450px" Style="max-width: 100% !important; max-height: 100% !important;">
                    <Titles>
                        <asp:Title Text=" PERFORMANCE BY GROWTH% " TextStyle="Shadow"></asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="Ach" ToolTip="GROWTH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                <MajorGrid LineWidth="0" />
                            </AxisX>
                            <AxisY>
                                <MajorGrid LineWidth="0" />
                            </AxisY>
                        </asp:ChartArea>
                    </ChartAreas>
                    <%-- <Legends>
                           <asp:Legend></asp:Legend>
                       </Legends>--%>
                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                        SkinStyle="Emboss" />
                </asp:Chart>
            </div>
            <!-- Customers END-->


            <!-- Chanel Partners Start-->
            <div id="DivCP" style="width: 100%; display: none; text-align: center !important;">
                <asp:Chart ID="bargraphtopcp1" BackColor="LightGray" runat="server" EnableViewState="true" Width="1200px" Height="450px" Style="max-width: 100% !important; max-height: 100% !important;">
                    <Titles>
                        <asp:Title Text=" PERFORMANCE BY PRO-RATA ACH% " TextStyle="Shadow"></asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="Ach" ToolTip="PRO-RATA ACH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                <MajorGrid LineWidth="0" />
                            </AxisX>
                            <AxisY>
                                <MajorGrid LineWidth="0" />
                            </AxisY>
                        </asp:ChartArea>
                    </ChartAreas>
                    <%-- <Legends>
                           <asp:Legend></asp:Legend>
                       </Legends>--%>
                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                        SkinStyle="Emboss" />
                </asp:Chart>
            </div>
            <div id="DivCP_growth" style="width: 100%; display: none; text-align: center !important;">
                <asp:Chart ID="bargraphtopcp_growth1" BackColor="LightGray" runat="server" EnableViewState="true" Width="1200px" Height="450px" Style="max-width: 100% !important; max-height: 100% !important;">
                    <Titles>
                        <asp:Title Text=" PERFORMANCE BY GROWTH% " TextStyle="Shadow"></asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="Ach" ToolTip="GROWTH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                <MajorGrid LineWidth="0" />
                            </AxisX>
                            <AxisY>
                                <MajorGrid LineWidth="0" />
                            </AxisY>
                        </asp:ChartArea>
                    </ChartAreas>
                    <%-- <Legends>
                           <asp:Legend></asp:Legend>
                       </Legends>--%>
                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                        SkinStyle="Emboss" />
                </asp:Chart>
            </div>

            <div id="DivCP1" style="width: 100%; display: none; text-align: center !important;">
                <asp:Chart ID="bargraphbottomcp1" BackColor="LightGray" runat="server" EnableViewState="true" Width="1200px" Height="450px" Style="max-width: 100% !important; max-height: 100% !important;">
                    <Titles>
                        <asp:Title Text=" PERFORMANCE BY PRO-RATA ACH% " TextStyle="Shadow"></asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="Ach" ToolTip="PRO-RATA ACH% :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                <MajorGrid LineWidth="0" />
                            </AxisX>
                            <AxisY>
                                <MajorGrid LineWidth="0" />
                            </AxisY>
                        </asp:ChartArea>
                    </ChartAreas>
                    <%-- <Legends>
                           <asp:Legend></asp:Legend>
                       </Legends>--%>
                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                        SkinStyle="Emboss" />
                </asp:Chart>
            </div>
            <div id="DivCP_growth1" style="width: 100%; display: none; text-align: center !important;">
                <asp:Chart ID="bargraphbottomcp_growth1" BackColor="LightGray" runat="server" EnableViewState="true" Width="1200px" Height="450px" Style="max-width: 100% !important; max-height: 100% !important;">
                    <Titles>
                        <asp:Title Text=" PERFORMANCE BY GROWTH% " TextStyle="Shadow"></asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="Ach" ToolTip="GROWTH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                <MajorGrid LineWidth="0" />
                            </AxisX>
                            <AxisY>
                                <MajorGrid LineWidth="0" />
                            </AxisY>
                        </asp:ChartArea>
                    </ChartAreas>
                    <%-- <Legends>
                           <asp:Legend></asp:Legend>
                       </Legends>--%>
                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                        SkinStyle="Emboss" />
                </asp:Chart>
            </div>
            <!-- Chanel Partners END-->


            <!-- GOLD Start-->

            <div id="DivGldBranch" style="width: 100%; display: none; text-align: center !important;">
                <asp:Chart ID="bargraphtopgoldbranches1" BackColor="LightGray" runat="server" EnableViewState="true" Width="1200px" Height="450px" Style="max-width: 100% !important; max-height: 100% !important;">
                    <Titles>
                        <asp:Title Text=" PERFORMANCE BY BUDGET% " TextStyle="Shadow"></asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="Ach" ToolTip="BUDGET VALUE  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                <MajorGrid LineWidth="0" />
                            </AxisX>
                            <AxisY>
                                <MajorGrid LineWidth="0" />
                            </AxisY>
                        </asp:ChartArea>
                    </ChartAreas>
                    <%-- <Legends>
                           <asp:Legend></asp:Legend>
                       </Legends>--%>
                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                        SkinStyle="Emboss" />
                </asp:Chart>
            </div>
            <div id="DivGldBranch_growth" style="width: 100%; display: none; text-align: center !important;">
                <asp:Chart ID="bargraphtopgoldbranches_growth1" BackColor="LightGray" runat="server" EnableViewState="true" Width="1200px" Height="450px" Style="max-width: 100% !important; max-height: 100% !important;">
                    <Titles>
                        <asp:Title Text=" PERFORMANCE BY GROWTH% " TextStyle="Shadow"></asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="Ach" ToolTip="GROWTH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                <MajorGrid LineWidth="0" />
                            </AxisX>
                            <AxisY>
                                <MajorGrid LineWidth="0" />
                            </AxisY>
                        </asp:ChartArea>
                    </ChartAreas>
                    <%-- <Legends>
                           <asp:Legend></asp:Legend>
                       </Legends>--%>
                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                        SkinStyle="Emboss" />
                </asp:Chart>
            </div>

            <div id="DivGldTopSe" style="width: 100%; display: none; text-align: center !important;">
                <asp:Chart ID="bargraphTopGoldSE1" BackColor="LightGray" runat="server" EnableViewState="true" Width="1200px" Height="450px" Style="max-width: 100% !important; max-height: 100% !important;">
                    <Titles>
                        <asp:Title Text=" PERFORMANCE BY BUDGET% " TextStyle="Shadow"></asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="Ach" ToolTip="BUDGET VALUE  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                <MajorGrid LineWidth="0" />
                            </AxisX>
                            <AxisY>
                                <MajorGrid LineWidth="0" />
                            </AxisY>
                        </asp:ChartArea>
                    </ChartAreas>
                    <%-- <Legends>
                           <asp:Legend></asp:Legend>
                       </Legends>--%>
                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                        SkinStyle="Emboss" />
                </asp:Chart>
            </div>

            <div id="DivGldTopSe_growth" style="width: 100%; display: none; text-align: center !important;">
                <asp:Chart ID="bargraphTopGoldSE_growth1" BackColor="LightGray" runat="server" EnableViewState="true" Width="1200px" Height="450px" Style="max-width: 100% !important; max-height: 100% !important;">
                    <Titles>
                        <asp:Title Text=" PERFORMANCE BY GROWTH% " TextStyle="Shadow"></asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="Ach" ToolTip="GROWTH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                <MajorGrid LineWidth="0" />
                            </AxisX>
                            <AxisY>
                                <MajorGrid LineWidth="0" />
                            </AxisY>
                        </asp:ChartArea>
                    </ChartAreas>
                    <%-- <Legends>
                           <asp:Legend></asp:Legend>
                       </Legends>--%>
                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                        SkinStyle="Emboss" />
                </asp:Chart>
            </div>

            <div id="DivGldBtmSe" style="width: 100%; display: none; text-align: center !important;">
                <asp:Chart ID="bargraphBtmGoldSE1" BackColor="LightGray" runat="server" EnableViewState="true" Width="1200px" Height="450px" Style="max-width: 100% !important; max-height: 100% !important;">
                    <Titles>
                        <asp:Title Text=" PERFORMANCE BY BUDGET% " TextStyle="Shadow"></asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="Ach" ToolTip="BUDGET VALUE  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                <MajorGrid LineWidth="0" />
                            </AxisX>
                            <AxisY>
                                <MajorGrid LineWidth="0" />
                            </AxisY>
                        </asp:ChartArea>
                    </ChartAreas>
                    <%-- <Legends>
                           <asp:Legend></asp:Legend>
                       </Legends>--%>
                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                        SkinStyle="Emboss" />
                </asp:Chart>
            </div>

            <div id="DivGldBtmSe_growth" style="width: 100%; display: none; text-align: center !important;">
                <asp:Chart ID="bargraphBtmGoldSE_growth1" BackColor="LightGray" runat="server" EnableViewState="true" Width="1200px" Height="450px" Style="max-width: 100% !important; max-height: 100% !important;">
                    <Titles>
                        <asp:Title Text=" PERFORMANCE BY GROWTH% " TextStyle="Shadow"></asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="Ach" ToolTip="GROWTH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                <MajorGrid LineWidth="0" />
                            </AxisX>
                            <AxisY>
                                <MajorGrid LineWidth="0" />
                            </AxisY>
                        </asp:ChartArea>
                    </ChartAreas>
                    <%-- <Legends>
                           <asp:Legend></asp:Legend>
                       </Legends>--%>
                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                        SkinStyle="Emboss" />
                </asp:Chart>
            </div>


            <div id="DivGoldC" style="width: 100%; display: none; text-align: center !important;">
                <asp:Chart ID="bargraphtopgoldcustomers1" BackColor="LightGray" runat="server" EnableViewState="true" Width="1200px" Height="450px" Style="max-width: 100% !important; max-height: 100% !important;">
                    <Titles>
                        <asp:Title Text=" PERFORMANCE BY BUDGET% " TextStyle="Shadow"></asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="Ach" ToolTip="BUDGET VALUE  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                <MajorGrid LineWidth="0" />
                            </AxisX>
                            <AxisY>
                                <MajorGrid LineWidth="0" />
                            </AxisY>
                        </asp:ChartArea>
                    </ChartAreas>
                    <%-- <Legends>
                           <asp:Legend></asp:Legend>
                       </Legends>--%>
                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                        SkinStyle="Emboss" />
                </asp:Chart>
            </div>
            <div id="DivGoldC_growth" style="width: 100%; display: none; text-align: center !important;">
                <asp:Chart ID="bargraphtopgoldcustomers_growth1" BackColor="LightGray" runat="server" EnableViewState="true" Width="1200px" Height="450px" Style="max-width: 100% !important; max-height: 100% !important;">
                    <Titles>
                        <asp:Title Text=" PERFORMANCE BY GROWTH% " TextStyle="Shadow"></asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="Ach" ToolTip="GROWTH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                <MajorGrid LineWidth="0" />
                            </AxisX>
                            <AxisY>
                                <MajorGrid LineWidth="0" />
                            </AxisY>
                        </asp:ChartArea>
                    </ChartAreas>
                    <%-- <Legends>
                           <asp:Legend></asp:Legend>
                       </Legends>--%>
                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                        SkinStyle="Emboss" />
                </asp:Chart>
            </div>

            <div id="DivGoldCPz" style="width: 100%; display: none; text-align: center !important;">
                <asp:Chart ID="bargraphtopgoldcp1" BackColor="LightGray" runat="server" EnableViewState="true" Width="1200px" Height="450px" Style="max-width: 100% !important; max-height: 100% !important;">
                    <Titles>
                        <asp:Title Text=" PERFORMANCE BY BUDGET% " TextStyle="Shadow"></asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="Ach" ToolTip="BUDGET VALUE  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                <MajorGrid LineWidth="0" />
                            </AxisX>
                            <AxisY>
                                <MajorGrid LineWidth="0" />
                            </AxisY>
                        </asp:ChartArea>
                    </ChartAreas>
                    <%--<Legends>
                           <asp:Legend></asp:Legend>
                       </Legends>--%>
                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                        SkinStyle="Emboss" />
                </asp:Chart>
            </div>

            <div id="DivGoldCPz_growth" style="width: 100%; display: none; text-align: center !important;">
                <asp:Chart ID="bargraphtopgoldcp_growth1" BackColor="LightGray" runat="server" EnableViewState="true" Width="1200px" Height="450px" Style="max-width: 100% !important; max-height: 100% !important;">
                    <Titles>
                        <asp:Title Text=" PERFORMANCE BY GROWTH% " TextStyle="Shadow"></asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="Ach" ToolTip="GROWTH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap" Interval="1">
                                <MajorGrid LineWidth="0" />
                            </AxisX>
                            <AxisY>
                                <MajorGrid LineWidth="0" />
                            </AxisY>
                        </asp:ChartArea>
                    </ChartAreas>
                    <%--<Legends>
                           <asp:Legend></asp:Legend>
                       </Legends>--%>
                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                        SkinStyle="Emboss" />
                </asp:Chart>
            </div>
            <!-- GOLD END-->


            <!-- ITEM Family Start-->
            <div id="DivItemFam" style="width: 100%; display: none; text-align: center !important;">
                <asp:Chart ID="bargraphgamily1" BackColor="LightGray" runat="server" EnableViewState="true" Width="1200px" Height="450px" Style="max-width: 100% !important; max-height: 100% !important;">

                    <Titles>
                        <asp:Title Text=" PERFORMANCE BY PRO-RATA ACH% " TextStyle="Shadow"></asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="Ach" ToolTip="PRO-RATA ACH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap">
                                <MajorGrid LineWidth="0" />
                            </AxisX>
                            <AxisY>
                                <MajorGrid LineWidth="0" />
                            </AxisY>
                        </asp:ChartArea>
                    </ChartAreas>
                    <%-- <Legends>
                           <asp:Legend></asp:Legend>
                       </Legends>--%>
                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                        SkinStyle="Emboss" />
                </asp:Chart>
            </div>

            <div id="DivItemFam1" style="width: 100%; display: none; text-align: center !important;">
                <asp:Chart ID="bargraphgamily_growth1" BackColor="LightGray" runat="server" EnableViewState="true" Width="1200px" Height="450px" Style="max-width: 100% !important; max-height: 100% !important;">
                    <Titles>
                        <asp:Title Text=" PERFORMANCE BY GROWTH% " TextStyle="Shadow"></asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="Ach" ToolTip="GROWTH%  :  #VALY" LabelForeColor="White" LabelAngle="-90" CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" IsVisibleInLegend="true" ChartType="Column"></asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea BackColor="#ACD1E9" Name="ChartArea1">
                            <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap">
                                <MajorGrid LineWidth="0" />
                            </AxisX>
                            <AxisY>
                                <MajorGrid LineWidth="0" />
                            </AxisY>
                        </asp:ChartArea>
                    </ChartAreas>
                    <%-- <Legends>
                           <asp:Legend></asp:Legend>
                       </Legends>--%>
                    <BorderSkin BackColor="Transparent" PageColor="Transparent"
                        SkinStyle="Emboss" />
                </asp:Chart>
            </div>

            <!-- ITEM Family END-->
        </ContentTemplate>
        <Triggers>
            <%--<asp:PostBackTrigger ControlID="lnkbtn_branch" />
            <asp:PostBackTrigger ControlID="lnkbtn_se" />
            <asp:PostBackTrigger ControlID="lnkbtn_customers" />
            <asp:PostBackTrigger ControlID="lnkbtn_cp" />
            <asp:PostBackTrigger ControlID="lnkbtn_gldfamily" />
            <asp:PostBackTrigger ControlID="lnkbtn_family" />--%>
            <asp:PostBackTrigger ControlID="ddlPerfBy" />
            <asp:PostBackTrigger ControlID="rdBtnTaegutec" />
            <asp:PostBackTrigger ControlID="rdBtnDuraCab" />
            <asp:PostBackTrigger ControlID="BranchList" />
            <asp:PostBackTrigger ControlID="ValInThsnd" />
            <asp:PostBackTrigger ControlID="ValInLakhs" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff">Please wait</span>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <style type="text/css">
        .portlet.box > .portlet-title
        {
            height:35px;
        }
        table.dataTable tfoot th, table.dataTable tfoot td
        {
            padding: 10px 10px 6px 10px;
        }
        .SumoSelect {
            margin-top: -65px;
        }
        .control_dropdown {
    width: 180px;
    height: 30px;
    /*border-radius: 4px!important;*/
    position: relative;
    text-shadow: 0 0 black;
}
        .amcharts-chart-div a
        {
            display: none !important;
        }

        #ChartBranchAch
        {
            width: 90%;
            height: 300px;
        }
        #ChartBranchGrowth
        {
            width: 90%;
            height: 300px;
        }
        #ChartTopSEAch
        {
            width: 70%;
            height: 300px;
            font-size: 9px;
        }
        #ChartTopSEGrowth
        {
            width: 70%;
            height: 300px;
            font-size: 9px;
        }
        #ChartBottomSEAch
        {
            width: 70%;
            height: 300px;
            font-size: 9px;
        }
        #ChartBottomSEGrowth
        {
            width: 70%;
            height: 300px;
            font-size: 9px;
        }
        #ChartTopCustAch
        {
            width: 70%;
            height: 300px;
            font-size: 9px;
        }
        #ChartTopCustGrowth
        {
            width: 70%;
            height: 300px;
            font-size: 9px;
        }
        #ChartBottomCustAch
        {
            width: 70%;
            height: 300px;
            font-size: 9px;
        }
        #ChartBottomCustGrowth
        {
            width: 70%;
            height: 300px;
            font-size: 9px;
        }

        #ChartTopCPAch
        {
            width: 70%;
            height: 300px;
            font-size: 9px;
        }
        #ChartTopCPGrowth
        {
            width: 70%;
            height: 300px;
            font-size: 9px;
        }
        #ChartBottomCPAch
        {
            width: 70%;
            height: 300px;
            font-size: 9px;
        }

        #ChartBottomCPGrowth
        {
            width: 70%;
            height: 300px;
            font-size: 9px;
        }

        #ChartGoldTopSEAch {
            width: 25%;
            height: 300px;
            font-size: 9px;
        }
        #ChartGoldTopSEGrowth {
            width: 25%;
            height: 300px;
            font-size: 9px;
        }
        #ChartGoldBottomSEAch {
            
            width: 25%;
            height: 300px;
            font-size: 9px;
        }
        #ChartGoldBottomSEGrowth {
            width: 25%;
            height: 300px;
            font-size: 9px;
        }

        #ChartGoldTopCustAch{
            width: 50%;
            height: 300px;
            font-size: 9px;
        }
        #ChartGoldTopCustGrowth{
            width: 50%;
            height: 300px;
            font-size: 9px;
        }
        #ChartGoldTopCPAch{
            width: 50%;
            height: 300px;
            font-size: 9px;
        }
        #ChartGoldTopCPGrowth{
            width: 50%;
            height: 300px;
            font-size: 9px;
        }
        #ChartFamAch
        {
            width: 90%;
            height: 300px;
            font-size: 9px;
        }
        #ChartFamGrowth
        {
            width: 90%;
            height: 300px;
            font-size: 9px;
        }


        #ChartConsolidated
        {
            /*width: 40%;*/
            height: 300px;
        }

        li
        {
            color: black;
        }

        .select-all
        {
            color: black;
        }

        .MultiControls
        {
            color: black;
        }

        .SumoSelect .CaptionCont
        {
            color: black;
        }

        .SumoSelect > .optWrapper > .options
        {
            max-height: 235px;
        }

        .optWrapper .okCancelInMulti .selall .multiple
        {
            width: 300px;
        }

        .grdview_total
        {
            background-color: yellow!important;
        }

        .dg-picture-zoom
        {
            cursor: url(http://www.dhtmlgoodies.com/scripts/picture-zoom/cursors/magnify-plus.cur), pointer !important;
        }

        .dg-picture-zoom-large
        {
            cursor: url(http://www.dhtmlgoodies.com/scripts/picture-zoom/cursors/magnify-minus.cur), pointer !important;
        }

        .dg-picture-zoom-border-container
        {
            border: 2px solid #FFF;
            background-color: #FFF;
            margin-left: -7px;
            margin-top: -7px;
            padding: 7px;
            box-shadow: 10px 10px 20px #000000;
            border-radius: 5px;
        }

        .hidebtn
        {
            display: none;
        }

        .square
        {
            width: 10px;
            height: 10px;
            margin-left: 10px;
        }
        /*th {
        font-size:12px;

    }
    td {
        font-size:10px;
    }*/

        .number
        {
            text-align: right;
        }

        .string
        {
            text-align: left;
        }

        .newa
        {
            background-color: #F1EFE2;
            -webkit-box-shadow: 0px 1px 10px rgba(10, 10, 10, 0.25);
            -moz-box-shadow: 0px 1px 10px rgba(10, 10, 10, 0.25);
            box-shadow: 0px 1px 10px rgba(10, 10, 10, 0.25);
            padding: 12px;
            width: 100%;
            margin-bottom: 10px;
            float: left;
            text-align: center;
        }

        .newa1
        {
            /*background-color: #EBF4FA;
            -webkit-box-shadow: 0px 1px 10px rgba(10, 10, 10, 0.25);
            -moz-box-shadow: 0px 1px 10px rgba(10, 10, 10, 0.25);
            box-shadow: 0px 1px 10px rgba(10, 10, 10, 0.25);*/
            padding:0 12px;
            width: 100%;
            margin-bottom: 10px;
            float: left;
            text-align: center;
        }

        .portlet.box.new > .portlet-title
        {
            background-color:#006780;
        }

        .portlet.box.new1 > .portlet-title
        {
            background-color: #975351;
        }

        .portlet.box.new2 > .portlet-title
        {
            background-color: #BEAC84;
        }

        .portlet.box.new3 > .portlet-title
        {
            background-color: #46616E;
        }

        .portlet.box.new4 > .portlet-title
        {
            background-color: #588195;
        }

        .portlet.box.new5 > .portlet-title
        {
            background-color: #666666;
        }
        /* arun */

        #external-events
        {
            background: #0582b7;
            color: #fff;
            font-size: 11px;
            cursor: pointer;
            /* padding: 8px; */
            border-bottom: 1px solid #ccc;
            width: 180px;
            padding: 0px;
        }

            #external-events h4
            {
                font-size: 16px;
                margin-top: 0;
                padding-top: 1em;
            }

        .external-event
        { /* try to mimick the look of a real event */
            padding: 10px 9px;
            background: #003050;
            color: #fff;
            font-size: 12px;
            cursor: pointer;
            padding: 8px;
            border-bottom: 1px solid #ddd;
            margin: 0px;
        }

            .external-event.active
            { /* try to mimick the look of a real event */
                padding: 10px 9px;
                background: #006780;
                color: #fff;
                padding: 8px;
                border-bottom: 1px solid #ddd;
            }

            .external-event a
            { /* try to mimick the look of a real event */
                text-decoration: none;
                color: #fff;
                font-size: 12px;
            }

                .external-event a:hover
                { /* try to mimick the look of a real event */
                    color: #ccc;
                    font-size: 12px;
                    cursor: pointer;
                }


        #external-events p
        {
            margin: 1.5em 0;
            font-size: 11px;
            color: #666;
        }

            #external-events p input
            {
                margin: 0;
                vertical-align: middle;
            }

        .portlet.box > .portlet-body
        {
            border-radius: 0px !important;
            padding: 12px !important;
        }

        .shadow
        {
            -webkit-box-shadow: 1px 2px 3px 2px rgba(227,227,227,1);
            -moz-box-shadow: 1px 2px 3px 2px rgba(227,227,227,1);
            box-shadow: 1px 2px 3px 2px rgba(227,227,227,1);
        }

        .dataTables_wrapper .HeadergridAll
        {
            background: #ddd;
            color: #000;
            font-weight: 600;
            text-align: center;
        }

        #MainContent_GridTopCstmrs td:nth-child(2),
        #MainContent_GridBottomCstmrs td:nth-child(2),
        #MainContent_GridTopCP td:nth-child(2),
        #MainContent_GridBottomCP td:nth-child(2),
        #MainContent_GridTopGoldCstmrs td:nth-child(2),
        #MainContent_GridBottomGoldCstmrs td:nth-child(2),
        #MainContent_GridTopGoldCP td:nth-child(2),
        #MainContent_GridBottomGoldCP td:nth-child(2)
        {
            text-align: left;
        }

        #MainContent_GridTopCstmrs td:nth-child(1),
        #MainContent_GridBottomCstmrs td:nth-child(1),
        #MainContent_GridTopCP td:nth-child(1),
        #MainContent_GridBottomCP td:nth-child(1),
        #MainContent_GridTopGoldCstmrs td:nth-child(1),
        #MainContent_GridBottomGoldCstmrs td:nth-child(1),
        #MainContent_GridTopGoldCP td:nth-child(1),
        #MainContent_GridBottomGoldCP td:nth-child(1)
        {
            text-align: center !important;
        }


        table tr:nth-child(even) > td:first-child
        {
            /*background: #f1f1f1;*/
            color: #333;
            text-align: left;
        }

        table tr:nth-child(odd) > td:first-child
        {
            /*background: #fff;*/
            color: #333;
            text-align: left;
        }

        table tr:nth-child(odd)
        {
            background: #fff;
        }

        table tr:nth-child(even)
        {
            background: #f1f1f1;
        }

        /*td
        {
            text-align: center;
        }*/

        .ui-dialog-osx
        {
            -moz-border-radius: 0 0 8px 8px;
            -webkit-border-radius: 0 0 8px 8px;
            border-radius: 0 0 8px 8px;
            border-width: 0 8px 8px 8px;
        }

        .col-md-7 > div
        {
            /*width: 100% !important;*/
            padding: 0px 16px 7px 0px;
        }
        .newa1 .dataTables_wrapper table
        {
            border: solid 1px #ccc !important;
        }
        .rbtn_panel li
        {
            color:#fff
        }
        #performncs
        {
            /*cursor: pointer;*/
            width: 33%;
        }

        @media (min-width: 1021px) and (max-width: 1050px)
        {

            .col-md-7 > div
            {
                width: 100% !important;
                padding: 0px 16px 7px 40px !important;
            }
        }
        /* arun */

        /*aswini*/
        td
        {
            padding: 4px;
        }
    </style>

    <script>
        //$(document).ready(function(){
        //    $('#performncs').unbind('click').bind('click', function (e) {
        //    //$('#external-events').toggle();
        //    $("#external-events").slideToggle("slow", function () {
        //        // Animation complete.
        //    });
        //});
        //});

        function gridviewScroll() {
            $('#MainContent_gridtopbranches').gridviewScroll({
                freezesize: 2,
                headerrowcount: 2
            });

            $('#GridTopSE').gridviewScroll({
                freezesize: 0,
                headerrowcount: 2
            });
        }


        $(function () {
            ZoomImages();
            gridviewScroll();
        });
        function ZoomImages() {
            //$('#performncs').unbind('click').bind('click', function (e) {
            //    //$('#external-events').toggle();
            //    $("#external-events").slideToggle("slow", function () {
            //        // Animation complete.
            //    });
            //});
            //Branch
            $("#MainContent_bargraphtopbranches").click(function () {
                $("#dvResultPopUp").dialog({
                    title: "BRANCH PERFORMANCE ",
                    modal: true,
                    draggable: false,
                    resizable: false,
                    position: ['center'],
                    show: 'blind',
                    hide: 'blind',
                    width: $(window).width() - 70,
                    dialogClass: 'ui-dialog-osx',

                });

            });
            $("#MainContent_bargraphtopbranches_growth").click(function () {
                $("#dvResultPopUp1").dialog({
                    title: "BRANCH PERFORMANCE ",
                    modal: true,
                    draggable: false,
                    resizable: false,
                    position: ['center'],
                    show: 'blind',
                    hide: 'blind',
                    width: $(window).width() - 70,
                    dialogClass: 'ui-dialog-osx',
                });
            });

            //Sales Engineers
            $("#MainContent_bargraphtopse").click(function () {
                $("#Divtopse").dialog({
                    title: "TOP 5 SALES ENGINEERS",
                    modal: true,
                    draggable: false,
                    resizable: false,
                    position: ['center'],
                    show: 'blind',
                    hide: 'blind',
                    width: $(window).width() - 70,
                    dialogClass: 'ui-dialog-osx',
                });
            });

            $("#MainContent_bargraphtopse_growth").click(function () {
                $("#Divtopse1").dialog({
                    title: "TOP 5 SALES ENGINEERS",
                    modal: true,
                    draggable: false,
                    resizable: false,
                    position: ['center'],
                    show: 'blind',
                    hide: 'blind',
                    width: $(window).width() - 70,
                    dialogClass: 'ui-dialog-osx',
                });
            });

            $("#MainContent_bargraphbottomse").click(function () {
                $("#DivSE1").dialog({
                    title: "BOTTOM 5 SALES ENGINEERS",
                    modal: true,
                    draggable: false,
                    resizable: false,
                    position: ['center'],
                    show: 'blind',
                    hide: 'blind',
                    width: $(window).width() - 70,
                    dialogClass: 'ui-dialog-osx',

                });

            });
            $("#MainContent_bargraphbottomse_growth").click(function () {
                $("#DivSE2").dialog({
                    title: "BOTTOM 5 SALES ENGINEERS",
                    modal: true,
                    draggable: false,
                    resizable: false,
                    position: ['center'],
                    show: 'blind',
                    hide: 'blind',
                    width: $(window).width() - 70,
                    dialogClass: 'ui-dialog-osx',

                });

            });


            //Customers
            $("#MainContent_bargraphtopcustomers").click(function () {
                $("#DivCustomerGraph").dialog({
                    title: "TOP 5 CUSTOMERS",
                    modal: true,
                    draggable: false,
                    resizable: false,
                    position: ['center'],
                    show: 'blind',
                    hide: 'blind',
                    width: $(window).width() - 70,
                    dialogClass: 'ui-dialog-osx',

                });

            });

            $("#MainContent_bargraphtopcustomers_growth").click(function () {
                $("#DivCustomerGraph_growth").dialog({
                    title: "TOP 5 CUSTOMERS",
                    modal: true,
                    draggable: false,
                    resizable: false,
                    position: ['center'],
                    show: 'blind',
                    hide: 'blind',
                    width: $(window).width() - 70,
                    dialogClass: 'ui-dialog-osx',

                });

            });

            $("#MainContent_bargraphbottomCustomers").click(function () {
                $("#DivCustomerGraph1").dialog({
                    title: "BOTTOM 5 CUSTOMERS",
                    modal: true,
                    draggable: false,
                    resizable: false,
                    position: ['center'],
                    show: 'blind',
                    hide: 'blind',
                    width: $(window).width() - 70,
                    dialogClass: 'ui-dialog-osx',

                });

            });
            $("#MainContent_bargraphbottomCustomers_growth").click(function () {
                $("#DivCustomerGraph_growth1").dialog({
                    title: "BOTTOM 5 CUSTOMERS",
                    modal: true,
                    draggable: false,
                    resizable: false,
                    position: ['center'],
                    show: 'blind',
                    hide: 'blind',
                    width: $(window).width() - 70,
                    dialogClass: 'ui-dialog-osx',

                });

            });
            //Chanel partners 

            $("#MainContent_bargraphtopcp").click(function () {
                $("#DivCP").dialog({
                    title: "TOP 5 CHANNEL PARTNERS",
                    modal: true,
                    draggable: false,
                    resizable: false,
                    position: ['center'],
                    show: 'blind',
                    hide: 'blind',
                    width: $(window).width() - 70,
                    dialogClass: 'ui-dialog-osx',

                });

            });


            $("#MainContent_bargraphtopcp_growth").click(function () {
                $("#DivCP_growth").dialog({
                    title: "TOP 5 CHANNEL PARTNERS",
                    modal: true,
                    draggable: false,
                    resizable: false,
                    position: ['center'],
                    show: 'blind',
                    hide: 'blind',
                    width: $(window).width() - 70,
                    dialogClass: 'ui-dialog-osx',

                });

            });

            $("#MainContent_bargraphbottomcp").click(function () {
                $("#DivCP1").dialog({
                    title: "BOTTOM 5 CHANNEL PARTNERS",
                    modal: true,
                    draggable: false,
                    resizable: false,
                    position: ['center'],
                    show: 'blind',
                    hide: 'blind',
                    width: $(window).width() - 70,
                    dialogClass: 'ui-dialog-osx',

                });

            });

            $("#MainContent_bargraphbottomcp_growth").click(function () {
                $("#DivCP_growth1").dialog({
                    title: "BOTTOM 5 CHANNEL PARTNERS",
                    modal: true,
                    draggable: false,
                    resizable: false,
                    position: ['center'],
                    show: 'blind',
                    hide: 'blind',
                    width: $(window).width() - 70,
                    dialogClass: 'ui-dialog-osx',

                });

            });

            //GOLD Customers
            $("#MainContent_bargraphtopgoldcustomers").click(function () {
                $("#DivGoldC").dialog({
                    title: "TOP 10  CUSTOMERS PERFORMANCE",
                    modal: true,
                    draggable: false,
                    resizable: false,
                    position: ['center'],
                    show: 'blind',
                    hide: 'blind',
                    width: $(window).width() - 70,
                    dialogClass: 'ui-dialog-osx',

                });

            });
            $("#MainContent_bargraphtopgoldcustomers_growth").click(function () {
                $("#DivGoldC_growth").dialog({
                    title: "TOP 10  CUSTOMERS PERFORMANCE",
                    modal: true,
                    draggable: false,
                    resizable: false,
                    position: ['center'],
                    show: 'blind',
                    hide: 'blind',
                    width: $(window).width() - 70,
                    dialogClass: 'ui-dialog-osx',

                });

            });

            ///GOLD CP
            $("#MainContent_bargraphtopgoldcp").click(function () {
                $("#DivGoldCPz").dialog({
                    title: "TOP 10 CHANNEL PARTNERS PERFORMANCE",
                    modal: true,
                    draggable: false,
                    resizable: false,
                    position: ['center'],
                    show: 'blind',
                    hide: 'blind',
                    width: $(window).width() - 70,
                    dialogClass: 'ui-dialog-osx',

                });

            });

            $("#MainContent_bargraphtopgoldcp_growth").click(function () {
                $("#DivGoldCPz_growth").dialog({
                    title: "TOP 10 CHANNEL PARTNERS PERFORMANCE",
                    modal: true,
                    draggable: false,
                    resizable: false,
                    position: ['center'],
                    show: 'blind',
                    hide: 'blind',
                    width: $(window).width() - 70,
                    dialogClass: 'ui-dialog-osx',

                });

            });


            $("#MainContent_bargraphtopgoldbranches").click(function () {
                $("#DivGldBranch").dialog({
                    title: "BRANCH PERFORMANCE",
                    modal: true,
                    draggable: false,
                    resizable: false,
                    position: ['center'],
                    show: 'blind',
                    hide: 'blind',
                    width: $(window).width() - 70,
                    dialogClass: 'ui-dialog-osx',

                });

            });
            $("#MainContent_bargraphtopgoldbranches_growth").click(function () {
                $("#DivGldBranch_growth").dialog({
                    title: "BRANCH PERFORMANCE",
                    modal: true,
                    draggable: false,
                    resizable: false,
                    position: ['center'],
                    show: 'blind',
                    hide: 'blind',
                    width: $(window).width() - 70,
                    dialogClass: 'ui-dialog-osx',

                });

            });

            $("#MainContent_bargraphTopGoldSE").click(function () {
                $("#DivGldTopSe").dialog({
                    title: "TOP  5 SALES ENGINEERS",
                    modal: true,
                    draggable: false,
                    resizable: false,
                    position: ['center'],
                    show: 'blind',
                    hide: 'blind',
                    width: $(window).width() - 70,
                    dialogClass: 'ui-dialog-osx',

                });

            });
            $("#MainContent_bargraphTopGoldSE_growth").click(function () {
                $("#DivGldTopSe_growth").dialog({
                    title: "TOP  5 SALES ENGINEERS",
                    modal: true,
                    draggable: false,
                    resizable: false,
                    position: ['center'],
                    show: 'blind',
                    hide: 'blind',
                    width: $(window).width() - 70,
                    dialogClass: 'ui-dialog-osx',

                });

            });

            $("#MainContent_bargraphBtmGoldSE").click(function () {
                $("#DivGldBtmSe").dialog({
                    title: "BOTTOM 5 SALES ENGINEERS",
                    modal: true,
                    draggable: false,
                    resizable: false,
                    position: ['center'],
                    show: 'blind',
                    hide: 'blind',
                    width: $(window).width() - 70,
                    dialogClass: 'ui-dialog-osx',

                });

            });
            $("#MainContent_bargraphBtmGoldSE_growth").click(function () {
                $("#DivGldBtmSe_growth").dialog({
                    title: "BOTTOM 5 SALES ENGINEERS",
                    modal: true,
                    draggable: false,
                    resizable: false,
                    position: ['center'],
                    show: 'blind',
                    hide: 'blind',
                    width: $(window).width() - 70,
                    dialogClass: 'ui-dialog-osx',

                });

            });
            // Item family
            $("#MainContent_bargraphgamily").click(function () {
                $("#DivItemFam").dialog({
                    title: "PRODUCT FAMILY",
                    modal: true,
                    draggable: false,
                    resizable: false,
                    position: ['center'],
                    show: 'blind',
                    hide: 'blind',
                    width: $(window).width() - 70,
                    dialogClass: 'ui-dialog-osx',

                });

            });
            $("#MainContent_bargraphgamily_growth").click(function () {
                $("#DivItemFam1").dialog({
                    title: "PRODUCT FAMILY",
                    modal: true,
                    draggable: false,
                    resizable: false,
                    position: ['center'],
                    show: 'blind',
                    hide: 'blind',
                    width: $(window).width() - 70,
                    dialogClass: 'ui-dialog-osx',

                });

            });
            //customer
            $("#MainContent_rbtn_achvmnt").change(function () {
                if ($("#MainContent_rbtn_achvmnt").is(':checked')) {
                    $("#MainContent_GridCustByAch").show();
                    $("#MainContent_GridCustByGrowth").hide();
                }
            });
            $("#MainContent_rbtn_growth").change(function () {
                if ($("#MainContent_rbtn_growth").is(':checked')) {
                    $("#MainContent_GridCustByAch").hide();
                    $("#MainContent_GridCustByGrowth").show();
                }
            });

            if ($("#MainContent_rbtn_achvmnt").is(':checked')) {
                $("#MainContent_GridCustByAch").show();
                $("#MainContent_GridCustByGrowth").hide();
            }
            if ($("#MainContent_rbtn_growth").is(':checked')) {
                $("#MainContent_GridCustByAch").hide();
                $("#MainContent_GridCustByGrowth").show();
            }
            //se
            $("#MainContent_rbtnse_achvmnt").change(function () {
                if ($("#MainContent_rbtnse_achvmnt").is(':checked')) {
                    $("#MainContent_GridTopSE").show();
                    $("#MainContent_GridTopSE_Growth").hide();
                }
            });
            $("#MainContent_rbtnse_growth").change(function () {
                if ($("#MainContent_rbtnse_growth").is(':checked')) {
                    $("#MainContent_GridTopSE").hide();
                    $("#MainContent_GridTopSE_Growth").show();
                }
            });

            if ($("#MainContent_rbtnse_achvmnt").is(':checked')) {
                $("#MainContent_GridTopSE").show();
                $("#MainContent_GridTopSE_Growth").hide();
            }

            if ($("#MainContent_rbtnse_growth").is(':checked')) {
                $("#MainContent_GridTopSE").hide();
                $("#MainContent_GridTopSE_Growth").show();
            }
            //cp
            $("#MainContent_rbtncp_achvmnt").change(function () {
                if ($("#MainContent_rbtncp_achvmnt").is(':checked')) {
                    $("#MainContent_GridTopCP").show();
                    $("#MainContent_GridTopCP_Growth").hide();
                }
            });
            $("#MainContent_rbtncp_growth").change(function () {
                if ($("#MainContent_rbtncp_growth").is(':checked')) {
                    $("#MainContent_GridTopCP").hide();
                    $("#MainContent_GridTopCP_Growth").show();
                }
            });


            if ($("#MainContent_rbtncp_growth").is(':checked')) {
                $("#MainContent_GridTopCP").hide();
                $("#MainContent_GridTopCP_Growth").show();
            }
            if ($("#MainContent_rbtncp_achvmnt").is(':checked')) {
                $("#MainContent_GridTopCP").show();
                $("#MainContent_GridTopCP_Growth").hide();
            }
            // Gold SE
            $("#MainContent_rbtngse_growth").change(function () {
                if ($("#MainContent_rbtngse_growth").is(':checked')) {
                    $("#MainContent_GridTopGoldSE").hide();
                    $("#MainContent_GridTopGoldSE_Growth").show();
                }
            });
            $("#MainContent_rbtngse_budget").change(function () {
                if ($("#MainContent_rbtngse_budget").is(':checked')) {
                    $("#MainContent_GridTopGoldSE").show();
                    $("#MainContent_GridTopGoldSE_Growth").hide();
                }
            });

            if ($("#MainContent_rbtngse_growth").is(':checked')) {
                $("#MainContent_GridTopGoldSE").hide();
                $("#MainContent_GridTopGoldSE_Growth").show();
            }
            if ($("#MainContent_rbtngse_budget").is(':checked')) {
                $("#MainContent_GridTopGoldSE").show();
                $("#MainContent_GridTopGoldSE_Growth").hide();
            }
            //Gold customer
            $("#MainContent_rbtngcust_growth").change(function () {
                if ($("#MainContent_rbtngcust_growth").is(':checked')) {
                    $("#MainContent_GridTopGoldCstmrs").hide();
                    $("#MainContent_GridTopGoldCstmrs_Growth").show();
                }
            });
            $("#MainContent_rbtngcust_budget").change(function () {
                if ($("#MainContent_rbtngcust_budget").is(':checked')) {
                    $("#MainContent_GridTopGoldCstmrs").show();
                    $("#MainContent_GridTopGoldCstmrs_Growth").hide();
                }
            });

            if ($("#MainContent_rbtngcust_growth").is(':checked')) {
                $("#MainContent_GridTopGoldCstmrs").hide();
                $("#MainContent_GridTopGoldCstmrs_Growth").show();
            }
            if ($("#MainContent_rbtngcust_budget").is(':checked')) {
                $("#MainContent_GridTopGoldCstmrs").show();
                $("#MainContent_GridTopGoldCstmrs_Growth").hide();
            }

            //Gold cp
            $("#MainContent_rbtngcp_growth").change(function () {
                if ($("#MainContent_rbtngcp_growth").is(':checked')) {
                    $("#MainContent_GridTopGoldCP").hide();
                    $("#MainContent_GridTopGoldCP_Growth").show();
                }
            });
            $("#MainContent_rbtngcp_budget").change(function () {
                if ($("#MainContent_rbtngcp_budget").is(':checked')) {
                    $("#MainContent_GridTopGoldCP").show();
                    $("#MainContent_GridTopGoldCP_Growth").hide();
                }
            });

            if ($("#MainContent_rbtngcp_growth").is(':checked')) {
                $("#MainContent_GridTopGoldCP").hide();
                $("#MainContent_GridTopGoldCP_Growth").show();
            }
            if ($("#MainContent_rbtngcp_budget").is(':checked')) {
                $("#MainContent_GridTopGoldCP").show();
                $("#MainContent_GridTopGoldCP_Growth").hide();
            }

            //Branch
            $("#MainContent_rbtnbranch_growth").change(function () {
                if ($("#MainContent_rbtnbranch_growth").is(':checked')) {
                    $("#MainContent_gridtopbranches").hide();
                    $("#MainContent_gridtopbranches_Growth").show();

                    //bindBranchGrowth();
                }
            });
            $("#MainContent_rbtnbranch_achvmnt").change(function () {
                if ($("#MainContent_rbtnbranch_achvmnt").is(':checked')) {
                    $("#MainContent_gridtopbranches").show();
                    $("#MainContent_gridtopbranches_Growth").hide();
                }
            });

            if ($("#MainContent_rbtnbranch_growth").is(':checked')) {
                $("#MainContent_gridtopbranches").hide();
                $("#MainContent_gridtopbranches_Growth").show();
            }
            if ($("#MainContent_rbtnbranch_achvmnt").is(':checked')) {
                $("#MainContent_gridtopbranches").show();
                $("#MainContent_gridtopbranches_Growth").hide();
            }
            //Gold Branch
            $("#MainContent_rbtngbranch_growth").change(function () {
                if ($("#MainContent_rbtngbranch_growth").is(':checked')) {
                    $("#MainContent_GridTopGoldBranches").hide();
                    $("#MainContent_GridTopGoldBranches_Growth").show();
                }
            });
            $("#MainContent_rbtngbranch_budget").change(function () {
                if ($("#MainContent_rbtngbranch_budget").is(':checked')) {
                    $("#MainContent_GridTopGoldBranches").show();
                    $("#MainContent_GridTopGoldBranches_Growth").hide();
                }
            });
            if ($("#MainContent_rbtngbranch_growth").is(':checked')) {
                $("#MainContent_GridTopGoldBranches").hide();
                $("#MainContent_GridTopGoldBranches_Growth").show();
            }
            if ($("#MainContent_rbtngbranch_budget").is(':checked')) {
                $("#MainContent_GridTopGoldBranches").show();
                $("#MainContent_GridTopGoldBranches_Growth").hide();
            }
        }

    </script>
</asp:Content>
