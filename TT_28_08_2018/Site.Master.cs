﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using System.Web.Services;
using System.Data.SqlClient;
using System.Data;

namespace TaegutecSalesBudget
{
    public partial class Site : System.Web.UI.MasterPage
    {
        LoginAuthentication authObj = new LoginAuthentication();
        AdminConfiguration objAdmin = new AdminConfiguration();
        protected void Page_Load(object sender, EventArgs e)
        {
            
            if (Session["UserName"] == null) { Response.Redirect("Login.aspx"); }
            displayBudgetTab();
           
            lblUserName.Text = Session["UserName"].ToString();
            if (!IsPostBack)
            {
              
                if (Session["UserName"] != null)
                {
                    #region SaveModules
                    SaveModules();
                    #endregion 
                    lblUserName.Text = Session["UserName"].ToString();
                    if (Session["Territory"].ToString()=="DUR")
                    {
                        usernav.Style.Add("background-color", "#044758 ");
                   // lblUserName.Style.Add("color", "#FFDE00");
                    }
                }
                displayBudgetTab();
            }
            if (Session["RoleId"] != null)
            {
                if (Session["RoleId"].ToString() == "Admin")
                {
                    if (Request.QueryString.Get(0) == "Budget") { Response.Redirect("AdminProfile.aspx?Profile"); return; }
                    if (Request.QueryString.Get(0) == "Summary") { Response.Redirect("AdminProfile.aspx?Profile"); return; }
                    if (Request.QueryString.Get(0) == "Reports") { Response.Redirect("AdminProfile.aspx?Profile"); return; }
                    if (Request.QueryString.Get(0) == "Dashboard") { Response.Redirect("AdminProfile.aspx?Profile"); return; }
                    if (Request.QueryString.Get(0) == "RS") { Response.Redirect("AdminProfile.aspx?Profile"); return; }
                    if (Request.QueryString.Get(0) == "RO") { Response.Redirect("AdminProfile.aspx?Profile"); return; }
                    if (Request.QueryString.Get(0) == "RMT") { Response.Redirect("AdminProfile.aspx?Profile"); return; }
                    if (Request.QueryString.Get(0) == "RV") { Response.Redirect("AdminProfile.aspx?Profile"); return; }
                    if (Request.QueryString.Get(0) == "RD") { Response.Redirect("AdminProfile.aspx?Profile"); return; }

                    if (Request.QueryString.Get(0) == "MDPEntry" || Request.QueryString.Get(0) == "MDPReports" || Request.QueryString.Get(0) == "Projects")
                    {
                        Response.Redirect("AdminProfile.aspx?Profile");
                        //Session["Project"] = "New";
                        return; }
                }

                //if (Session["RoleId"].ToString() == "HO")
                //{
                //    sereview1.Visible = true;
                //    sereview2.Visible = true;
                //    sereview3.Visible = true;
                //    sereview4.Visible = true;
                //    //li_review.Visible = false;
                //    //li_review1.Visible = false;
                //    //li_review2.Visible = false;
                //    //if (Request.QueryString.Get(0) == "RS") { Response.Redirect("Reports.aspx?Reports"); return; }
                //    //if (Request.QueryString.Get(0) == "RO") { Response.Redirect("Reports.aspx?Reports"); return; }
                //    //if (Request.QueryString.Get(0) == "RMT") { Response.Redirect("Reports.aspx?Reports"); return; }
                //    //if (Request.QueryString.Get(0) == "RV") { Response.Redirect("Reports.aspx?Reports"); return; }
                //}
                //else
                //{
                //    sereview1.Visible = false;
                //    sereview2.Visible = false;
                //    sereview3.Visible = false;
                //    sereview4.Visible = false;
                //}

            }
            Session["organization_id"] = "1"; 
            Session["p_user_name"] ="bsalla"; 
            Session["p_user_id"] ="1";
            Session["employee_id"] ="1";
            Session["employee_name"] ="Bakta G Salla";
            Session["exp_supervisor_flag"] ="Y";
            Session["vst_supervisor_flag"] ="Y"; 
            Session["access_exp_module"] ="1"; 
            Session["access_vst_module"] ="1";
            Session["access_crm_module"] ="1"; 
            Session["error_code"] ="0";
            Session["error_message"] = "Login Successful";
        }

        protected void lnkbtnLogout_Click(object sender, EventArgs e)
        {
            if (Session["UserName"] != null)
            {

                Session["UserName"] = null;
                Session["LoginMailId"] = null;
                Session["RoleId"] = null;
                Session["UserId"] = null;
                Session["Customer_Number"] = null;
                Session["Project_Number"] = null;
                Session["Distributor_Number"] = null;
                Session["cter"] = null;
                             
                Session.Clear(); 
                Session.Abandon();
                Session.RemoveAll();
                Response.Redirect("Login.aspx");
               
                //ClientScript.RegisterStartupScript(typeof(Page), "closePage", "window.close();", true);
            }
        }
        protected void confirmpwd_Click(object sender, EventArgs e)
        {
            if (Session["UserName"] != null)
            {

                string strpassword = authObj.Encrypt(pwd.Text);
                authObj.MailPassword = strpassword;
                authObj.EngineerId = Convert.ToString(Session["UserId"]);
                string ErrorMessage = authObj.resetpwd(authObj);
                if (ErrorMessage == null)
                {
                    string scriptString = "<script type='text/javascript'> alert('Password was successfully reset');window.location='Login.aspx';</script>";
                    ClientScriptManager script = Page.ClientScript;
                    script.RegisterClientScriptBlock(GetType(), "Script", scriptString);
                }
                else
                {
                    string scriptString = "<script type='text/javascript'> alert('Failed to reset');window.location='Login.aspx';</script>";
                    ClientScriptManager script = Page.ClientScript;
                    script.RegisterClientScriptBlock(GetType(), "Script", scriptString);
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }

        protected void displayBudgetTab()
        {
            if (Session["RoleId"] != null)
            {
                string role = Session["RoleId"].ToString();
                string startDate = "", endDate = "";
                int currentDate = Convert.ToInt32(DateTime.Today.ToString("yyyyMMdd"));
                if (role == "SE")
                {
                    startDate = objAdmin.GetProfile("START_DATE_SE");
                    endDate = objAdmin.GetProfile("END_DATE_SE");
                    int start = startDate == "" ? -1 : Convert.ToInt32(Regex.Replace(startDate, "-", ""));
                    int end = endDate == "" ? -1 : Convert.ToInt32(Regex.Replace(endDate, "-", ""));
                    int maincount = start == -1 ? start : start - currentDate;
                    int count = end == -1 ? end : end - currentDate;
                    if (count >= 0 && maincount <= 0)
                    {
                        li_budgetTab.Visible = true;
                        li_budgetTab1.Visible = true;
                        li_budgetTab2.Visible = true;
                        li_budgetTab3.Visible = true;
                    }
                    BDD1.Visible = false;
                    BDD2.Visible = false;
                    BDD3.Visible = false;
                    BDD4.Visible = false;
                }
                else if (role == "BM")
                {
                    startDate = objAdmin.GetProfile("START_DATE_BM");
                    endDate = objAdmin.GetProfile("END_DATE_BM");
                    int start = startDate == "" ? -1 : Convert.ToInt32(Regex.Replace(startDate, "-", ""));
                    int end = endDate == "" ? -1 : Convert.ToInt32(Regex.Replace(endDate, "-", ""));
                    int maincount = start == -1 ? start : start - currentDate;
                    int count = end == -1 ? end : end - currentDate;
                    if (count >= 0 && maincount <= 0)
                    {
                        li_budgetTab.Visible = true;
                        li_budgetTab1.Visible = true;
                        li_budgetTab2.Visible = true;
                        li_budgetTab3.Visible = true;
                    }
                }
                else if (role == "TM")
                {

                    startDate = objAdmin.GetProfile("START_DATE_TM");
                    endDate = objAdmin.GetProfile("END_DATE_TM");
                    int start = startDate == "" ? -1 : Convert.ToInt32(Regex.Replace(startDate, "-", ""));
                    int end = endDate == "" ? -1 : Convert.ToInt32(Regex.Replace(endDate, "-", ""));
                    int maincount = start == -1 ? start : start - currentDate;
                    int count = end == -1 ? end : end - currentDate;
                    if (count >= 0 && maincount <= 0)
                    {
                        li_budgetTab.Visible = true;
                        li_budgetTab1.Visible = true;
                        li_budgetTab2.Visible = true;
                        li_budgetTab3.Visible = true;
                    }
                }
                else if (role == "HO")
                {
                    startDate = objAdmin.GetProfile("START_DATE_HO");
                    endDate = objAdmin.GetProfile("END_DATE_HO");
                    int start = startDate == "" ? -1 : Convert.ToInt32(Regex.Replace(startDate, "-", ""));
                    int end = endDate == "" ? -1 : Convert.ToInt32(Regex.Replace(endDate, "-", ""));
                    int maincount = start == -1 ? start : start - currentDate;
                    int count = end == -1 ? end : end - currentDate;
                    if (count >= 0 && maincount <= 0)
                    {
                        li_budgetTab.Visible = true;
                        li_budgetTab1.Visible = true;
                        li_budgetTab2.Visible = true;
                        li_budgetTab3.Visible = true;
                    }
                }

            }
        }


        [WebMethod]
        public static string SetSession()
        {
            string output = "";
            try
            {
                HttpContext.Current.Session["Chart"]="gdfjgsdf";
              
            }
            catch (Exception ex)
            {
               // objCom.ErrorLog(ex);
            }
            return output;
        }



        /// <summary>
        /// Author:K.LakshmiBindu
        /// Desc: For Saving userlogs based on time with module info used by user
        /// Date : Feb 12, 2019
        /// </summary>
        public void SaveModules()
        {  int i=0;
            SqlConnection cn =null;

        try
        {
          cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            SqlCommand cmd = new SqlCommand("sp_saveModuleLogs", cn);
            cmd.CommandType = System.Data.CommandType.StoredProcedure;
            SqlParameter[] sp = new SqlParameter[]{
               new SqlParameter("@EngineerId",Convert.ToString(Session["UserId"])),
               new SqlParameter("@UniqueId",Convert.ToString(Session["UserGuid"])),
               new SqlParameter("@Module",Request.FilePath)
            };
            cmd.Parameters.AddRange(sp);
            cn.Open();
            i = cmd.ExecuteNonQuery();
        }
        catch (Exception ex)
        {
            CommonFunctions.LogErrorStatic(ex);
        }
        finally
        {
            if (cn.State==ConnectionState.Open)
            {
                cn.Close();
            }
        }
        }
    }
}