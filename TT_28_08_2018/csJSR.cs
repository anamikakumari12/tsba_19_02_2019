﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace TaegutecSalesBudget
{
    public class csJSR
    {
        public string branch { get; set; }
        public string salesengineer_id { get; set; }
        public int jsr_month { get; set; }
        public int jsr_year { get; set; }
        public string c_num { get; set; }
        public string customer_type { get; set; }
        public string p_group { get; set; }
        public string family { get; set; }
        public string subfamily { get; set; }
        public string application { get; set; }
        public string cter { get; set; }
        public string fiterby { get; set; }
        public string roleId { get; set; }
        public string flag { get; set; }
        public int valueIn { get; set; }

    }
    public class csJSRDAL
    {
        public DataTable getFilterAreaValue(csJSR objRSum)
        {
            DataTable dtData = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("sp_getFilterData", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@p_branch_code", SqlDbType.VarChar, 20000, null).Value = objRSum.branch;
                command.Parameters.Add("@p_salesengineer_id", SqlDbType.VarChar, 20000, null).Value = objRSum.salesengineer_id;
                command.Parameters.Add("@p_cust_dist_flag", SqlDbType.VarChar, 50, null).Value = objRSum.customer_type;
                command.Parameters.Add("@p_role_id", SqlDbType.VarChar, 50, null).Value = objRSum.roleId;
                command.Parameters.Add("@p_flag", SqlDbType.VarChar, 50, null).Value = objRSum.flag;
                command.Parameters.Add("@p_cter", SqlDbType.VarChar, 50, null).Value = objRSum.cter;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtData);

            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

            }

            return dtData;
        }

        public DataTable getFilterAreaValueTest(csJSR objRSum)
        {
            DataTable dtData = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("sp_getFilterDataTest", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@p_branch_code", SqlDbType.VarChar, 20000, null).Value = objRSum.branch;
                command.Parameters.Add("@p_salesengineer_id", SqlDbType.VarChar, 20000, null).Value = objRSum.salesengineer_id;
                command.Parameters.Add("@p_cust_dist_flag", SqlDbType.VarChar, 50, null).Value = objRSum.customer_type;
                command.Parameters.Add("@p_role_id", SqlDbType.VarChar, 50, null).Value = objRSum.roleId;
                command.Parameters.Add("@p_flag", SqlDbType.VarChar, 50, null).Value = objRSum.flag;
                command.Parameters.Add("@p_cter", SqlDbType.VarChar, 50, null).Value = objRSum.cter;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtData);

            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

            }

            return dtData;
        }

        internal DataTable getProducts(string subfamId, string group)
        {
            DataTable dtPL = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("sp_getApplicationBySubFam", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@subfamilyId", SqlDbType.VarChar, 2000, null).Value = subfamId;
                command.Parameters.Add("@product_group", SqlDbType.VarChar, 2000, null).Value = group;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtPL);

            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

            }
            return dtPL;
        }
        internal DataTable getSubFamily(string famId)
        {
            DataTable dtPL = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("sp_getSubFamilyByFam", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@FamilyId", SqlDbType.VarChar, 2000, null).Value = famId;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtPL);

            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

            }
            return dtPL;
        }

        internal DataTable getReport(csJSR objJSR)
        {
            DataTable dtPL = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("sp_calculate_columns_jsr_report", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@MONTH", SqlDbType.VarChar, 2000, null).Value = objJSR.jsr_month;
                command.Parameters.Add("@YEAR", SqlDbType.VarChar, 2000, null).Value = objJSR.jsr_year;
                command.Parameters.Add("@BRANCH", SqlDbType.VarChar, -1, null).Value = objJSR.branch;
                command.Parameters.Add("@SE", SqlDbType.VarChar, -1, null).Value = objJSR.salesengineer_id;
                command.Parameters.Add("@CTYPE", SqlDbType.VarChar, -1, null).Value = objJSR.customer_type;
                command.Parameters.Add("@C_NUM", SqlDbType.VarChar, -1, null).Value = objJSR.c_num;
                command.Parameters.Add("@P_GRP", SqlDbType.VarChar, -1, null).Value = objJSR.p_group;
                command.Parameters.Add("@FAMILY", SqlDbType.VarChar, -1, null).Value = objJSR.family;
                command.Parameters.Add("@SUBFAM", SqlDbType.VarChar, -1, null).Value = objJSR.subfamily;
                command.Parameters.Add("@APPCODE", SqlDbType.VarChar, -1, null).Value = objJSR.application;
                command.Parameters.Add("@BY", SqlDbType.VarChar, 2000, null).Value = objJSR.fiterby;
                command.Parameters.Add("@CTER", SqlDbType.VarChar, 2000, null).Value = objJSR.cter;
                command.Parameters.Add("@VALUEIN", SqlDbType.Int,100, null).Value = objJSR.valueIn;
                command.Parameters.Add("@ROLE", SqlDbType.VarChar, 10, null).Value = objJSR.roleId;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtPL);

            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

            }
            return dtPL;
        }

        internal DataTable getYear()
        {
            DataTable dtYear = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("sp_getYearForJSR", connection);
                command.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtYear);

            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

            }
            return dtYear;
        }
    }

}
