﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ionic.Zip;

namespace TaegutecSalesBudget
{
    public partial class MDPReports : System.Web.UI.Page
    {
        #region Global Declarations

        Budget objBudget = new Budget();
        MDP objMDP = new MDP();
        Reports objReports = new Reports();
        Review objRSum = new Review();
        static DataTable dtCutomerDetails = null;
        static DataTable sortedReports = null;
        public static string salesengineernumber, customernumber, branchcode, customertype, RoleID, ddCustomerClass;
        public static bool potSort;
        public static string cter;
        CommonFunctions objFunc = new CommonFunctions();
        projecttypecs objprojtype = new projecttypecs();
        #endregion

        #region Events

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                Session["Customer_Number"] = null;
                Session["Project_Number"] = null;
                Session["Project"] = null;
               // Session["Distributor_Number"] = null;
                if (!IsPostBack)
                {
                    hdnsearch.Value = "";
                    LoadAllDropDownAtFirst();
                }

            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Modified By : Anamika
        /// Date : Oct 20, 2016
        /// Desc : The loop for calculation of column is removed so that the speed should be fast.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void proceed_Click(object sender, EventArgs e)
        {
            string strProjType = string.Empty;
            try
            {
                hdnsearch.Value = "";
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                btnreset.Visible = false;
                string status_lbl = "";
                string roleId = Session["RoleId"].ToString();
                string strUserId = Session["UserId"].ToString();
                string owner = ddlOwnerList.SelectedValue;
                string reviewer = ddlReviewerList.SelectedValue;
                string escalate = ddlEscalate.SelectedValue;
                string industry = ddlIndustryList.SelectedValue;
                string subindustry = ddlsubIndustryList.SelectedValue;
                string customer = ddlCustomer.SelectedValue;
                string branch = ddlBranch.SelectedValue;
                // string outcome = ddlOutcome.SelectedValue;
                string status = ddlStatus.SelectedValue;
                //string from_target_date = Target_From.Text;
                //string to_target_date = Target_To.Text;
                //string from_created_date = Created_from.Text;
                //string to_created_date = Created_To.Text;


                string from_created_date = string.Empty;
                string to_created_date = string.Empty;
                string selectedDate = creationrange.Text;
                if (selectedDate.Contains("/"))
                {
                    string[] splittedDates = selectedDate.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                    from_created_date = Convert.ToString(splittedDates[0].TrimEnd().Trim());
                    to_created_date = Convert.ToString(splittedDates[1].TrimStart().Trim());
                }

                string from_target_date = string.Empty;
                string to_target_date = string.Empty;
                selectedDate = targetrange.Text;
                if (selectedDate.Contains("/"))
                {
                    string[] splittedDates = selectedDate.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                    from_target_date = Convert.ToString(splittedDates[0].TrimEnd().Trim());
                    to_target_date = Convert.ToString(splittedDates[1].TrimStart().Trim());
                }
                string customerClass = ddlCustomerClass.SelectedItem.Value;
                strProjType = ddlptype.SelectedValue;
                string strSearch = Convert.ToString(txtSearch.Text);
                #region Filters
                if (owner == "ALL")
                {
                    owner = null;
                }
                else
                {
                    owner = "'" + owner + "'";
                }

                if (reviewer == "ALL")
                {
                    reviewer = null;
                }
                else
                {
                    reviewer = "'" + reviewer + "'";
                }

                if (escalate == "ALL")
                {
                    escalate = null;
                }
                else
                {
                    escalate = "'" + escalate + "'";
                }
                if (customerClass == "ALL")
                {
                    customerClass = null;
                }
                else
                {
                    customerClass = "'" + customerClass + "'";
                }
                if (industry == "ALL")
                {
                    industry = null;
                }
                else
                {
                    industry = "'" + industry + "'";
                }
                if (subindustry == "ALL")
                {
                    subindustry = null;
                }
                else
                {
                    subindustry = "'" + subindustry + "'";
                }
                if (customer == "ALL")
                {
                    customer = null;
                }

                else
                {
                    customer = "'" + customer + "'";
                }

                if (branch == "ALL")
                {
                    if (roleId == "TM")
                    {
                        branch = "'" + strUserId + "'";
                    }
                    else
                    {
                        branch = null;
                    }
                }

                else
                {
                    branch = "'" + branch + "'";
                }

                //if (outcome == "ALL")
                //{
                //    outcome = null;
                //}

                //else
                //{
                //    outcome = "'" + outcome + "'";
                //}

                if (status == "ALL")
                {
                    status = null;
                }

                else
                {
                    status = "'" + status + "'";
                }

                if (from_target_date == "")
                {
                    from_target_date = null;
                }


                else
                {
                    from_target_date = "'" + from_target_date + "'";
                }

                if (to_target_date == "")
                {
                    to_target_date = null;
                }

                else
                {
                    to_target_date = "'" + to_target_date + "'";
                }

                if (from_created_date == "")
                {
                    from_created_date = null;
                }

                else
                {
                    from_created_date = "'" + from_created_date + "'";
                }

                if (to_created_date == "")
                {
                    to_created_date = null;
                }

                else
                {
                    to_created_date = "'" + to_created_date + "'";
                }
                if (strProjType == "ALL" || strProjType == Convert.ToString(0))
                {
                    strProjType = null;
                }
                else
                {
                    strProjType = "'" + strProjType + "'";
                }
               
                #endregion

                #region Loading Data To Main Table
                DataSet dsprojectReport = new DataSet();
                DataTable projectReport = new DataTable();
                DataTable dtprojectReport = new DataTable();

                dsprojectReport = objMDP.getProjectReport(roleId, owner, reviewer, escalate, industry, subindustry, customer, customerClass, branch, status, from_target_date, to_target_date, from_created_date, to_created_date, strSearch, cter, strProjType);
                //grandtotal commented by Neha
                //dtprojectReport = objFunc.GrandtotalforMdpReports(dtprojectReport);
                //dsprojectReport.Tables[0].Rows[(dsprojectReport.Tables[0].Rows.Count) - 1].Delete();
                if (dsprojectReport.Tables.Count > 0)
                {
                    if (dsprojectReport.Tables[0].Rows.Count > 0)
                    {
                        projectReport = dsprojectReport.Tables[0];
                        // projectReport.Rows[(projectReport.Rows.Count) - 1].Delete();
                        //projectReport = objFunc.GrandtotalforMdpReports(projectReport);
                        // int m = projectReport.Rows.Count - 1;
                        #region Added By Anamika
                        dtprojectReport = CloneTable(projectReport);
                        // dtprojectReport.Columns.Remove("Grand Total");
                        // dtprojectReport.Rows[0].Delete();
                        dtprojectReport.Columns.Remove("customer_name");
                        dtprojectReport.Columns.Remove("Stage_Completed");
                        dtprojectReport.Columns.Remove("NoOfStages");
                        dtprojectReport.Columns.Remove("IsCanceled");
                        dtprojectReport.Columns.Remove("projects");
                        dtprojectReport.Columns.Remove("project_status");
                        dtprojectReport.Columns.Remove("assigned_salesengineer_id");
                        dtprojectReport.Columns.Remove("Reviewer");
                        dtprojectReport.Columns.Remove("Escalated_To");
                        dtprojectReport.Columns.Remove("Percentage");
                        dtprojectReport.Columns.Add("result", typeof(string));
                        if (projectReport.Rows.Count > 0)
                        {
                            // var rowWith The value= mytable.rows.where(a=> a["Engineer_Name"]== value);
                            setsessions(dsprojectReport.Tables[0]);

                            //projectReport.Rows[(projectReport.Rows.Count) - 1].Delete();
                            //projectReport.AcceptChanges();

                            DataRow dr = projectReport.AsEnumerable().SingleOrDefault(r => r.Field<string>("Engineer_Name") == "Grand Total");
                            if (dr != null)
                            {

                                //  var rows = projectReport.AsEnumerable().Where(r => r.Field<string>("Engineer_Name") == "Grand Total");

                                //  projectReport.Rows[rows].Delete();

                                //  var rowWithData = projectReport.Rows.Where
                                //  DataRow dtRow = projectReport.Rows.Find("Total");
                                dr.Delete();
                                projectReport.AcceptChanges();
                            }
                            Session["dtreport"] = projectReport;
                            projectreports.DataSource = projectReport;
                            projectreports.DataBind();
                            // projectReport.AcceptChanges();
                            // setsessions(dsprojectReport.Tables[0]);
                            this.RegisterPostBackControl();
                            this.RegisterCancelPostBackControl();
                            this.RegisterDownloadPostBackControl();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                        }
                        else
                        {
                            Session["dtreport"] = null;
                            DataTable dtReport = new DataTable();
                            projectreports.DataSource = dtReport;
                            projectreports.DataBind();
                            // setsessions(dtReport);
                            lbl_Message.Visible = true;
                            lbl_nocustomer.Visible = false;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                        }

                        #endregion

                        // bindgridColor();
                        save.Visible = true;
                        export.Visible = true;
                        back.Visible = true;
                        lbl_values.Visible = true;
                        lbl_Message.Visible = false;
                        //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                    }

                    else
                    {
                        DataTable dtReport = new DataTable();
                        projectreports.DataSource = dtReport;
                        projectreports.DataBind();
                        //setsessions(dtReport);
                        lbl_Message.Visible = true;
                        lbl_values.Visible = false;
                        lbl_nocustomer.Visible = false;
                        btnreset.Visible = true;
                        save.Visible = false;
                        export.Visible = false;
                        back.Visible = false;
                       // ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                    }

                    #region Commented By Anamika
                    //dtprojectReport.Columns.Add("customer_num", typeof(string));
                    //dtprojectReport.Columns.Add("customer_lbl", typeof(string));
                    //dtprojectReport.Columns.Add("distributor_num", typeof(string));
                    //dtprojectReport.Columns.Add("distributor_lbl", typeof(string));
                    //dtprojectReport.Columns.Add("project_title", typeof(string));
                    //dtprojectReport.Columns.Add("project_num", typeof(string));
                    //dtprojectReport.Columns.Add("creation_date_lbl", typeof(string));
                    //dtprojectReport.Columns.Add("industry_lbl", typeof(string));
                    //dtprojectReport.Columns.Add("sub_industry_lbl", typeof(string));
                    //dtprojectReport.Columns.Add("overall_pot_lbl", typeof(decimal));
                    //dtprojectReport.Columns.Add("potential_lakhs_lbl", typeof(decimal));
                    //dtprojectReport.Columns.Add("existing_product_lbl", typeof(string));
                    //dtprojectReport.Columns.Add("component_lbl", typeof(string));
                    //dtprojectReport.Columns.Add("status_lbl", typeof(string));
                    //dtprojectReport.Columns.Add("red", typeof(string));
                    //dtprojectReport.Columns.Add("yellow", typeof(string));
                    //dtprojectReport.Columns.Add("green", typeof(string));
                    //dtprojectReport.Columns.Add("monthly_exp_val", typeof(decimal));
                    //dtprojectReport.Columns.Add("txt_remarks", typeof(string));
                    //dtprojectReport.Columns.Add("result", typeof(string));
                    //dtprojectReport.Columns.Add("cancel", typeof(string));
                    //dtprojectReport.Columns.Add("Engineer_Name", typeof(string));
                    //dtprojectReport.Columns.Add("Business_Expected", typeof(string));
                    //if (projectReport.Rows.Count != 0)
                    //{
                    //    for (int i = 0; i < projectReport.Rows.Count; i++)
                    //    {

                    //        string customer_num = projectReport.Rows[i].ItemArray[0].ToString();
                    //        string customer_lbl = projectReport.Rows[i].ItemArray[1].ToString() + " (" + projectReport.Rows[i].ItemArray[0].ToString() + " )";
                    //        string distributor_num = projectReport.Rows[i].ItemArray[19].ToString();
                    //        string distributor_lbl = "";
                    //        if (distributor_num != "")
                    //        {
                    //            distributor_lbl = objMDP.get_distributorname_by_number(distributor_num) + " (" + projectReport.Rows[i].ItemArray[19].ToString() + " )";
                    //        }

                    //        string creation_date_lbl = projectReport.Rows[i].ItemArray[20].ToString();

                    //        string industry_lbl = projectReport.Rows[i].ItemArray[2].ToString();
                    //        string sub_industry_lbl = projectReport.Rows[i].ItemArray[18].ToString();
                    //        decimal overall_pot_lbl = Convert.ToDecimal(projectReport.Rows[i].ItemArray[3].ToString());
                    //        decimal potential_lakhs_lbl = Convert.ToDecimal(projectReport.Rows[i].ItemArray[4].ToString());
                    //        string existing_product_lbl = projectReport.Rows[i].ItemArray[5].ToString();
                    //        string component_lbl = projectReport.Rows[i].ItemArray[6].ToString();
                    //        string stage_completed = projectReport.Rows[i].ItemArray[7].ToString();
                    //        string stages_total = projectReport.Rows[i].ItemArray[8].ToString();
                    //        string EngineerName = projectReport.Rows[i].ItemArray[21].ToString();
                    //        string Business_Expected = projectReport.Rows[i].ItemArray[22].ToString();
                    //        string result = "";



                    //        #region To load the stage number
                    //        if (stages_total == stage_completed)
                    //        {
                    //            if (projectReport.Rows[i].ItemArray[13].ToString() == "failure")
                    //            {
                    //                status_lbl = "FAILURE";
                    //            }
                    //            else if (projectReport.Rows[i].ItemArray[13].ToString() == "success")
                    //            {
                    //                status_lbl = "SUCCESS";
                    //            }
                    //        }
                    //        else
                    //        {
                    //            if (stage_completed != "")
                    //            {
                    //                status_lbl = "Stg" + stage_completed + "/" + "Stg" + stages_total;
                    //            }
                    //            else
                    //            {
                    //                status_lbl = "Stg 0" + "/" + "Stg" + stages_total;
                    //            }
                    //        }
                    //        #endregion
                    //        #region To load the proper status color
                    //        string red = "", yellow = "", green = "";
                    //        int stage_complete = 0; int total_stages = 0; double percent_complete = 0;
                    //        if (projectReport.Rows[i].ItemArray[7].ToString() != "")
                    //        {
                    //            stage_complete = Convert.ToInt32(projectReport.Rows[i].ItemArray[7].ToString());
                    //            total_stages = Convert.ToInt32(projectReport.Rows[i].ItemArray[8].ToString());
                    //            percent_complete = (((double)stage_complete / total_stages) * 100);
                    //        }
                    //        if (percent_complete != 0 && percent_complete <= 33.33)
                    //        {
                    //            red = "yes";
                    //            yellow = "no";
                    //            green = "no";
                    //        }

                    //        else if (percent_complete != 0 && percent_complete >= 33.33 && percent_complete <= 66.66)
                    //        {
                    //            red = "no";
                    //            yellow = "yes";
                    //            green = "no";
                    //        }

                    //        else if (percent_complete != 0 && percent_complete >= 66.66)
                    //        {
                    //            red = "no";
                    //            yellow = "no";
                    //            green = "yes";
                    //        }
                    //        else
                    //        {
                    //            red = "no";
                    //            yellow = "no";
                    //            green = "no";

                    //        }
                    //        #endregion
                    //        decimal monthly_exp_val;
                    //        if (projectReport.Rows[i].ItemArray[14].ToString() == "")
                    //        {
                    //            monthly_exp_val = Convert.ToDecimal("0");
                    //        }
                    //        else
                    //        {
                    //            monthly_exp_val = Convert.ToDecimal(projectReport.Rows[i].ItemArray[14].ToString());
                    //        }
                    //        string txt_remarks = projectReport.Rows[i].ItemArray[9].ToString();
                    //        string cancel = "";

                    //        if (projectReport.Rows[i].ItemArray[10].ToString() == "True")
                    //        {
                    //            cancel = "no";
                    //        }
                    //        else if (projectReport.Rows[i].ItemArray[10].ToString() != "True" && (roleId == "HO" || roleId == "TM"))
                    //        {
                    //            cancel = "yes";
                    //        }

                    //        string ptoject_title = projectReport.Rows[i].ItemArray[11].ToString() + " (" + projectReport.Rows[i].ItemArray[12].ToString() + ") ";
                    //        string project_num = projectReport.Rows[i].ItemArray[12].ToString();
                    //        dtprojectReport.Rows.Add(customer_num, customer_lbl, distributor_num, distributor_lbl, ptoject_title, project_num, creation_date_lbl, industry_lbl, sub_industry_lbl, overall_pot_lbl, potential_lakhs_lbl, existing_product_lbl, component_lbl, status_lbl, red, yellow, green, monthly_exp_val, txt_remarks, result, cancel, EngineerName, Business_Expected);


                    //    }
                    //    projectreports.DataSource = dtprojectReport;
                    //    projectreports.DataBind();
                    //    this.RegisterPostBackControl();
                    //    this.RegisterCancelPostBackControl();
                    //    this.RegisterDownloadPostBackControl();
                    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                    //}
                    //else
                    //{
                    //    DataTable dtReport = new DataTable();
                    //    projectreports.DataSource = dtReport;
                    //    projectreports.DataBind();
                    //    lbl_Message.Visible = true;
                    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);


                    //}
                    #endregion

                    sortedReports = dtprojectReport;


                    #region Loading Data to Grand Total
                    //DataTable initGrand = new DataTable();
                    //DataTable dtgrandTotal = new DataTable();
                    //if (dsprojectReport.Tables[1].Rows.Count > 0)
                    //{
                    //    initGrand = dsprojectReport.Tables[1];
                    //}
                    ////initGrand = objMDP.getProjectReport(owner, reviewer, escalate, industry, subindustry, customer, customerClass, branch, status, from_target_date, to_target_date, from_created_date, to_created_date, cter);
                    //dtgrandTotal.Columns.Add("customer_num", typeof(string));
                    //dtgrandTotal.Columns.Add("customer_lbl", typeof(string));
                    //dtgrandTotal.Columns.Add("project_title", typeof(string));
                    //dtgrandTotal.Columns.Add("industry_lbl", typeof(string));
                    //dtgrandTotal.Columns.Add("over_pot", typeof(decimal));
                    //dtgrandTotal.Columns.Add("pot_lakh", typeof(decimal));
                    //dtgrandTotal.Columns.Add("existing_product_lbl", typeof(string));
                    //dtgrandTotal.Columns.Add("component_lbl", typeof(string));
                    //dtgrandTotal.Columns.Add("status_lbl", typeof(string));
                    //dtgrandTotal.Columns.Add("red", typeof(string));
                    //dtgrandTotal.Columns.Add("yellow", typeof(string));
                    //dtgrandTotal.Columns.Add("green", typeof(string));
                    //dtgrandTotal.Columns.Add("total_monthly_expected", typeof(decimal));
                    //dtgrandTotal.Columns.Add("total_business_expected", typeof(decimal));
                    //dtgrandTotal.Columns.Add("txt_remarks", typeof(string));
                    //decimal over_pot = 0, pot_lakh = 0, total_monthly_expected = 0, total_business_expected = 0;
                    //if (initGrand.Rows.Count != 0)
                    //{
                    //    //for (int i = 0; i < initGrand.Rows.Count; i++)
                    //    //{
                    //    //    if (initGrand.Rows[i].ItemArray[10].ToString() != "True")
                    //    //    {
                    //    //        over_pot += initGrand.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(initGrand.Rows[i].ItemArray[3].ToString());
                    //    //        pot_lakh += initGrand.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(initGrand.Rows[i].ItemArray[4].ToString());
                    //    //        total_monthly_expected += initGrand.Rows[i].ItemArray[14].ToString() == "" ? 0 : Convert.ToDecimal(initGrand.Rows[i].ItemArray[14].ToString());
                    //    //        total_business_expected += initGrand.Rows[i].ItemArray[22].ToString() == "" ? 0 : Convert.ToDecimal(initGrand.Rows[i].ItemArray[22].ToString());
                    //    //    }
                    //    //}
                    //    if (string.IsNullOrEmpty(initGrand.Rows[0]["over_pot"].ToString()) || string.IsNullOrEmpty(initGrand.Rows[0]["pot_lakh"].ToString()) || string.IsNullOrEmpty(initGrand.Rows[0]["total_monthly_expected"].ToString()) || string.IsNullOrEmpty(initGrand.Rows[0]["total_business_expected"].ToString()))
                    //    {
                    //        DataTable dtTotal = new DataTable();
                    //        grandTotal.DataSource = dtTotal;
                    //        grandTotal.DataBind();
                    //        save.Visible = false;
                    //        export.Visible = false;
                    //        lbl_values.Visible = false;
                    //        lbl_Message.Visible = true;
                    //        back.Visible = false;
                    //        btnreset.Visible = true;
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                    //    }
                    //    else
                    //    {
                    //        over_pot = Convert.ToDecimal(initGrand.Rows[0]["over_pot"].ToString());
                    //        pot_lakh = Convert.ToDecimal(initGrand.Rows[0]["pot_lakh"].ToString());
                    //        total_monthly_expected = Convert.ToDecimal(initGrand.Rows[0]["total_monthly_expected"].ToString());
                    //        total_business_expected = Convert.ToDecimal(initGrand.Rows[0]["total_business_expected"].ToString());

                    //        dtgrandTotal.Rows.Add("",
                    //                                "GRAND TOTAL",
                    //                                "",
                    //                                "",
                    //                               over_pot,
                    //                                pot_lakh,
                    //                                "",
                    //                                "",
                    //                                "",
                    //                                "no",
                    //                                "no",
                    //                                "no",
                    //                               total_monthly_expected,
                    //                               total_business_expected,
                    //                                ""
                    //                                 );
                    //        //dtprojectReport.Merge(dtgrandTotal);


                    //        grandTotal.DataSource = dtgrandTotal;
                    //        grandTotal.DataBind();


                    //        bindgridColor();
                    //        save.Visible = true;
                    //        export.Visible = true;
                    //        back.Visible = true;
                    //        lbl_values.Visible = true;
                    //        lbl_Message.Visible = false;
                    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                    //    }
                    //}
                    //else
                    //{
                    //    DataTable dtTotal = new DataTable();
                    //    grandTotal.DataSource = dtTotal;
                    //    grandTotal.DataBind();
                    //    save.Visible = false;
                    //    export.Visible = false;
                    //    lbl_values.Visible = false;
                    //    lbl_Message.Visible = true;
                    //    back.Visible = false;
                    //    btnreset.Visible = true;
                    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);


                    //}
                    #endregion

                    //  bindcolorfirstload();
                    
                }
                #endregion
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        private void setsessions(DataTable dt)
        {
            try
            {
                if (dt.Rows.Count > 0)
                {
                    var grand = (from n in dt.AsEnumerable()
                                 where n.Field<string>("Engineer_Name").Contains("Total")
                                 select n);
                    if (grand.Any())
                    {
                        var grandtotal = grand.CopyToDataTable();

                        Session["projectpotential"] = Convert.ToDecimal(grandtotal.Rows[0]["potential_lakhs_lbl"].ToString());
                        Session["businessexpected"] = Convert.ToDecimal(grandtotal.Rows[0]["Business_Expected"].ToString());
                        Session["monthlybussinessexpected"] = Convert.ToDecimal(grandtotal.Rows[0]["monthly_exp_val"].ToString());
                    }
                    else
                    {
                        Session["projectpotential"] = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["potential_lakhs_lbl"])).ToString();
                        Session["businessexpected"] = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["Business_Expected"])).ToString();
                        Session["monthlybussinessexpected"] = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["monthly_exp_val"])).ToString();

                    }

                    //Session["projectpotential"] = Convert.ToDecimal(dt.Rows[(dt.Rows.Count) - 1]["potential_lakhs_lbl"].ToString());
                    //Session["businessexpected"] = Convert.ToDecimal(dt.Rows[(dt.Rows.Count) - 1]["Business_Expected"].ToString());
                    //Session["monthlybussinessexpected"] = Convert.ToDecimal(dt.Rows[(dt.Rows.Count) - 1]["monthly_exp_val"].ToString());


                    //Session["projectpotential"] = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["potential_lakhs_lbl"])).ToString();
                    //Session["businessexpected"] = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["Business_Expected"])).ToString();
                    //Session["monthlybussinessexpected"] = dt.AsEnumerable().Sum(x => Convert.ToDecimal(x["monthly_exp_val"])).ToString();

                    // Session["projectpotential"] = Convert.ToDecimal(projectReports.Compute("SUM(potential_lakhs_lbl)", string.Empty)); 
                    // Session["businessexpected"] = Convert.ToDecimal(projectReports.Compute("SUM(Business_Expected)", string.Empty));
                    //Session["monthlybussinessexpected"] = Convert.ToDecimal(projectReports.Compute("SUM(monthly_exp_val)", string.Empty));

                    hdnprojectpotential.Value = Convert.ToString(Session["projectpotential"]);
                    hdnbusinessexpected.Value = Convert.ToString(Session["businessexpected"]);
                    hdnmonthlybusinessexpected.Value = Convert.ToString(Session["monthlybussinessexpected"]);
                }
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }
        /// <summary>
        /// To load the sales engineer for the selected branch only
        /// Modified By : Anamika
        /// Date : Oct 19, 2016
        /// Desc : LoadAllDropDown is called.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void ddlBranch_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                hdnsearch.Value = "";
                DataTable dtSalesEngDetails = new DataTable();
                branchcode = ddlBranch.SelectedItem.Value;

                if (branchcode == "ALL")
                {
                    dtSalesEngDetails = objReports.LoadUserInfo(null, "ALL");

                    if (dtSalesEngDetails != null)
                    {
                        ddlOwnerList.DataSource = dtSalesEngDetails;
                        ddlOwnerList.DataTextField = "EngineerName";
                        ddlOwnerList.DataValueField = "EngineerId";
                        ddlOwnerList.DataBind();
                        ddlOwnerList.Items.Insert(0, "ALL");
                    }
                }

                else
                {
                    dtSalesEngDetails = objReports.LoadUserInfo(null, branchcode);
                    if (dtSalesEngDetails != null)
                    {
                        ddlOwnerList.DataSource = dtSalesEngDetails;
                        ddlOwnerList.DataTextField = "EngineerName";
                        ddlOwnerList.DataValueField = "EngineerId";
                        ddlOwnerList.DataBind();
                        ddlOwnerList.Items.Insert(0, "ALL");
                    }
                }

                ddlOwnerList_SelectedIndexChanged(null, null);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                //LoadAllDropDown();
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// To load the customers for selecte sales engineer only
        /// Modified By : Anamika
        /// Date : Oct 19, 2016
        /// Desc : LoadAllDropDown is called.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlOwnerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                LoadAllDropDown();

            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions1();", true);
        }

        /// <summary>
        /// To redirect to MDP page 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void status_lbl_click(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                LinkButton lb = (LinkButton)sender;
                GridViewRow row = (GridViewRow)lb.NamingContainer;
                if (row != null)
                {
                    int index = row.RowIndex; //gets the row index selected
                    Session["Customer_Number"] = Convert.ToString(projectreports.Rows[index].Cells[2]);
                    Session["Project_Number"] = Convert.ToString(projectreports.Rows[index].Cells[3]);
                    Session["Distributor_Number"] = Convert.ToString(projectreports.Rows[index].Cells[1]);
                    Session["Project"] = "Existing";
                    if (Session["Customer_Number"] != null)
                    {
                        Server.Transfer("MDPEntry.aspx?Projects");
                        //Response.Redirect("MDPEntry.aspx?Projects");
                    }
                }

            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        /// <summary>
        /// Method to save sale_ytd and remarks
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void save_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (Session["UserName"] == null) { Response.Redirect("Login.aspx?Login"); return; }

                foreach (GridViewRow row in projectreports.Rows)
                {
                    var cust_num = row.FindControl("hdn_customernumber") as HiddenField;
                    var project_number = row.FindControl("hdn_projectnumber") as HiddenField;
                    //var txtSaleYtd = row.FindControl("txt_sale_ytd") as TextBox;
                    var txtRemarks = row.FindControl("txt_remarks") as TextBox;
                    if (row.RowIndex < projectreports.Rows.Count)
                    {
                        objMDP.rpt_cust_num = cust_num.Value;
                        objMDP.rpt_project_number = project_number.Value;
                        objMDP.rpt_remark = txtRemarks.Text;
                        string Messege = objMDP.insertMTDReport(objMDP);
                    }
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Sucessfully Updated');", true);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "triggerPostGridLodedActions1();", true);
            proceed_Click(null, null);
        }
        protected void export_Click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

            string projectPotentialTotal = hdnprojectpotential.Value;
            string businessExpectedTotal = hdnbusinessexpected.Value;
            string monthlyBusinessExpectedTotal = hdnmonthlybusinessexpected.Value;
            try
            {
                DataTable dt = new DataTable();
                DataTable dtNew = new DataTable();
                DataRow dr;
                dt.Columns.Add(new System.Data.DataColumn("Engineer_Name", typeof(String)));
                dt.Columns.Add(new System.Data.DataColumn("distributor_lbl", typeof(String)));
                dt.Columns.Add(new System.Data.DataColumn("distributor_num", typeof(String)));
                dt.Columns.Add(new System.Data.DataColumn("customer_lbl", typeof(String)));
                dt.Columns.Add(new System.Data.DataColumn("customer_num", typeof(String)));
                dt.Columns.Add(new System.Data.DataColumn("project_title", typeof(String)));
                dt.Columns.Add(new System.Data.DataColumn("project_num", typeof(String)));
                dt.Columns.Add(new System.Data.DataColumn("project_type", typeof(String)));
                dt.Columns.Add(new System.Data.DataColumn("existing_product_lbl", typeof(String)));
                dt.Columns.Add(new System.Data.DataColumn("component_lbl", typeof(String)));
                dt.Columns.Add(new System.Data.DataColumn("potential_lakhs_lbl", typeof(Int32)));
                dt.Columns.Add(new System.Data.DataColumn("Business_Expected", typeof(Int32)));
                dt.Columns.Add(new System.Data.DataColumn("monthly_exp_val", typeof(Int32)));
                dt.Columns.Add(new System.Data.DataColumn("status_lbl", typeof(String)));
                dt.Columns.Add(new System.Data.DataColumn("txt_remarks", typeof(String)));
                dt.Columns.Add(new System.Data.DataColumn("cancel", typeof(String)));

                foreach (GridViewRow row in projectreports.Rows)
                {
                    Label salesEngineer = (Label)row.FindControl("salesengineer_lbl");
                    Label channelPartner = (Label)row.FindControl("distributor_text_lbl");
                    HiddenField distributorNumber = (HiddenField)row.FindControl("hdn_distributornumber");
                    Label endCustomer = (Label)row.FindControl("Label1");
                    HiddenField customerNumber = (HiddenField)row.FindControl("hdn_customernumber");
                    Label projectTitle = (Label)row.FindControl("Label2");
                    HiddenField projectNumber = (HiddenField)row.FindControl("hdn_projectnumber");
                    Label projectType = (Label)row.FindControl("Label3");
                    Label existingBrand = (Label)row.FindControl("existingproduct_lbl");
                    Label component = (Label)row.FindControl("tagutecproduct_lbl");
                    Label projectPotential = (Label)row.FindControl("Label6");
                    Label businessExpected = (Label)row.FindControl("businessexpected_lbl");
                    Label monthlyBusinessExpected = (Label)row.FindControl("Label7");
                    LinkButton status = (LinkButton)row.FindControl("status_lbl");
                    TextBox remarks = (TextBox)row.FindControl("txt_remarks");
                    Label cancelLabel = (Label)row.FindControl("cancel_lbl");

                    dr = dt.NewRow();
                    dr[0] = salesEngineer.Text;
                    dr[1] = channelPartner.Text;
                    dr[2] = distributorNumber.Value;
                    dr[3] = endCustomer.Text;
                    dr[4] = customerNumber.Value;
                    dr[5] = projectTitle.Text;
                    dr[6] = projectNumber.Value;
                    dr[7] = projectType.Text;
                    dr[8] = existingBrand.Text;
                    dr[9] = component.Text;
                    //string str = projectPotential.Text;
                    dr[10] = Convert.ToDecimal(projectPotential.Text);
                    dr[11] = Convert.ToDecimal(businessExpected.Text);
                    dr[12] = Convert.ToDecimal(monthlyBusinessExpected.Text);
                    dr[13] = status.Text;
                    dr[14] = remarks.Text;
                    dr[15] = cancelLabel.Text;
                    dt.Rows.Add(dr);
                }

                dt = CheckforSearchedData(dt);
                setsessions(dt);
                dtNew = addGrandTotalToExcelTable(dt);
                ExportReport(dtNew, dt);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                // ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            // ExportReport();
        }

        private DataTable CheckforSearchedData(DataTable dt)
        {
            DataTable dtOutput = new DataTable();
            string search = hdnsearch.Value;
            List<string> searchList = new List<string>();
            DataRow[] filteredRows;
            try
            {

                if (!string.IsNullOrEmpty(search))
                {
                    if (search.Contains(' '))
                    {
                        searchList = search.Split(' ').ToList();
                    }
                    else
                    {
                        searchList.Add(search);
                    }

                    for (int i = 0; i < searchList.Count; i++)
                    {
                        dtOutput = new DataTable();
                        dtOutput.Columns.Add("Engineer_Name");
                        dtOutput.Columns.Add("distributor_lbl");
                        dtOutput.Columns.Add("distributor_num");
                        dtOutput.Columns.Add("customer_lbl");
                        dtOutput.Columns.Add("customer_num");
                        dtOutput.Columns.Add("project_title");
                        dtOutput.Columns.Add("project_num");
                        dtOutput.Columns.Add("project_type");
                        dtOutput.Columns.Add("existing_product_lbl");
                        dtOutput.Columns.Add("component_lbl");
                        dtOutput.Columns.Add("potential_lakhs_lbl");
                        dtOutput.Columns.Add("Business_Expected");
                        dtOutput.Columns.Add("monthly_exp_val");
                        dtOutput.Columns.Add("status_lbl");
                        dtOutput.Columns.Add("txt_remarks");
                        dtOutput.Columns.Add("cancel");

                        filteredRows = dt.Select("Engineer_Name LIKE '%" + searchList[i].Trim()
                            + "%' OR distributor_lbl LIKE'%" + searchList[i].Trim()
                            + "%' OR customer_lbl LIKE '%" + searchList[i].Trim()
                            + "%' OR project_title  LIKE '%" + searchList[i].Trim()
                            + "%' OR project_type  LIKE '%" + searchList[i].Trim()
                            + "%' OR existing_product_lbl  LIKE '%" + searchList[i].Trim()
                            + "%' OR component_lbl  LIKE '%" + searchList[i].Trim()
                            + "%' OR CONVERT(potential_lakhs_lbl, System.String)  LIKE '%" + searchList[i].Trim()
                            + "%' OR CONVERT(Business_Expected, System.String)  LIKE '%" + searchList[i].Trim()
                            + "%' OR CONVERT(monthly_exp_val, System.String)  LIKE '%" + searchList[i].Trim()
                            + "%' OR status_lbl  LIKE '%" + searchList[i].Trim()
                            + "%' OR txt_remarks  LIKE '%" + searchList[i].Trim()
                            + "%'");
                        foreach (DataRow dr in filteredRows)
                        {
                            //  dtOutput.ImportRow(dr);
                            dtOutput.Rows.Add(dr.ItemArray);
                        }
                        dt = dtOutput;
                    }
                }
                else
                {
                    dtOutput = dt;
                }
                //setsessions(dt);
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }

            return dtOutput;
        }

        private DataTable addGrandTotalToExcelTable(DataTable dt)
        {
            try
            {
                DataRow dr = dt.NewRow();
                dr["component_lbl"] = "GRAND TOTAL : ";
                dr["potential_lakhs_lbl"] = Convert.ToString(Session["projectpotential"]);
                dr["Business_Expected"] = Convert.ToString(Session["businessexpected"]);
                dr["monthly_exp_val"] = Convert.ToString(Session["monthlybussinessexpected"]);
                dt.Rows.Add(dr);
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            return dt;
        }

        /// <summary>
        /// Method to cancel a project
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btn_cancel_click(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                objMDP.customer_num = hdn_CustomerNum.Value;
                objMDP.project_number = hdn_projectnum.Value;
                string Messege = objMDP.cancel_Project(objMDP);
                proceed_Click(null, null);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Project Deleted.');", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// On Click method for back button to load the default reports page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void back_Click(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                btnreset.Visible = true;
                string strUserId = Session["UserId"].ToString();
                string roleId = Session["RoleId"].ToString();
                string salesEngName = Session["UserName"].ToString();
                string branchCode = Session["BranchCode"].ToString();
                if (roleId == "SE")
                {
                    LoadBranches();
                    LoadAllReviewer_SE(strUserId);
                    LoadAllEscalator_BM(strUserId);
                    LoadCustomerDetails_SE(strUserId);
                    ddlBranch.SelectedValue = branchCode;
                    ddlOwnerList.SelectedValue = strUserId;
                    ddlOwnerList.Enabled = false;
                    ddlBranch.Enabled = false;
                    LoadAllSEs();
                    save.Visible = false;
                    export.Visible = false;
                    back.Visible = false;
                }

                if (roleId == "BM")
                {
                    LoadBranches();
                    LoadAllReviewer_BM(strUserId);
                    LoadAllEscalator_BM(strUserId);
                    LoadAllSE_BM();
                    LoadCustomerDetails_BM(branchCode);
                    ddlBranch.SelectedValue = branchCode;
                    ddlBranch.Enabled = false;
                    save.Visible = false;
                    export.Visible = false;
                    back.Visible = false;
                }

                if (roleId == "HO")
                {
                    LoadBranches();
                    LoadCustomerDetails_HO();
                    LoadAllSEs();
                    LoadAllReviewers();
                    LoadAllEscalator();
                    save.Visible = false;
                    export.Visible = false;
                    back.Visible = false;

                }

                if (roleId == "TM")
                {
                    LoadBranches_TM(strUserId);
                    LoadAllSE_TM(strUserId);
                    LoadAllReviewer_TM(strUserId);
                    LoadAllEscalator_TM(strUserId);
                    LoadCustomerDetails_TM(strUserId);
                    save.Visible = false;
                    export.Visible = false;
                    back.Visible = false;
                }

                //Target_To.Text = "";
                //Created_To.Text = "";
                //Target_From.Text = "";
                //Created_from.Text = "";
                creationrange.Text = "";
                targetrange.Text = "";
                DataTable ds = new DataTable();
                ds = null;
                projectreports.DataSource = ds;
                projectreports.DataBind();
                // setsessions(ds);
                lbl_values.Visible = false;
                DataTable dtTotal = new DataTable();
                //   grandTotal.DataSource = dtTotal;
                //  grandTotal.DataBind();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions1();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        #region Sorting
        //sorting  commented by Neha
        //protected void ReportGridView_Sorting(object sender, GridViewSortEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        //Retrieve the table from the session object.
        //        DataTable dt = sortedReports.Copy();

        //        EnumerableRowCollection<DataRow> dr1 = null;
        //        //dt.Rows[dt.Rows.Count - 1].Delete();
        //        dt.Rows[0].Delete();

        //        if (dt != null)
        //        {

        //            //Sort the data.

        //            if (GetSortDirection(e.SortDirection) == "ASC")
        //            {
        //                dr1 = (from row in dt.AsEnumerable()

        //                       orderby row[e.SortExpression] ascending

        //                       select row);
        //            }
        //            else
        //            {
        //                dr1 = (from row in dt.AsEnumerable()

        //                       orderby row[e.SortExpression] descending

        //                       select row);
        //            }
        //            DataTable dx = dr1.AsDataView().ToTable();
        //            //dx.Rows.Add(sortedReports.Rows[sortedReports.Rows.Count - 1].ItemArray);
        //            // dx.Rows.Add(sortedReports.Rows[0].ItemArray);

        //            for (int j = 0; j < 1; j++)
        //            {
        //                DataRow[] drlist = sortedReports.Select("Engineer_Name='Grand Total'");
        //                foreach (DataRow dr in drlist)
        //                {
        //                    DataRow drnew = dx.NewRow();
        //                    drnew["monthly_exp_val"] = dr["monthly_exp_val"];
        //                    drnew["Engineer_Name"] = dr["Engineer_Name"];
        //                    drnew["overall_pot_lbl"] = dr["overall_pot_lbl"];
        //                    drnew["potential_lakhs_lbl"] = dr["potential_lakhs_lbl"];
        //                    drnew["Business_Expected"] = dr["Business_Expected"];

        //                    dx.Rows.InsertAt(drnew, j);

        //                }

        //            }

        //            projectreports.DataSource = dx;
        //            projectreports.DataBind();

        //            //bindgridColor();
        //           // setsessions(dx);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //}

        //private string GetSortDirection(SortDirection sortDirection)
        //{
        //    string newSortDirection = String.Empty;
        //    try
        //    {

        //        switch (sortDirection)
        //        {
        //            case SortDirection.Ascending:
        //                newSortDirection = "ASC";
        //                break;
        //            case SortDirection.Descending:
        //                newSortDirection = "DESC";
        //                break;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //    return newSortDirection;
        //}
        //protected void sort_creation_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        if (potSort == false)
        //        {
        //            ReportGridView_Sorting(null, new GridViewSortEventArgs("creation_date_lbl", SortDirection.Ascending));
        //            potSort = true;
        //        }
        //        else
        //        {
        //            ReportGridView_Sorting(null, new GridViewSortEventArgs("creation_date_lbl", SortDirection.Descending));
        //            potSort = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        //}
        //protected void sort_potential_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        if (potSort == false)
        //        {
        //            ReportGridView_Sorting(null, new GridViewSortEventArgs("potential_lakhs_lbl", SortDirection.Ascending));
        //            potSort = true;
        //        }
        //        else
        //        {
        //            ReportGridView_Sorting(null, new GridViewSortEventArgs("potential_lakhs_lbl", SortDirection.Descending));
        //            potSort = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        //}

        //protected void sort_title_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        if (potSort == false)
        //        {
        //            ReportGridView_Sorting(null, new GridViewSortEventArgs("project_title", SortDirection.Ascending));
        //            potSort = true;
        //        }
        //        else
        //        {
        //            ReportGridView_Sorting(null, new GridViewSortEventArgs("project_title", SortDirection.Descending));
        //            potSort = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        //}

        //protected void sort_customer_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        if (potSort == false)
        //        {
        //            ReportGridView_Sorting(null, new GridViewSortEventArgs("customer_lbl", SortDirection.Ascending));
        //            potSort = true;
        //        }
        //        else
        //        {
        //            ReportGridView_Sorting(null, new GridViewSortEventArgs("customer_lbl", SortDirection.Descending));
        //            potSort = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        //}

        //protected void sort_distributor_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        if (potSort == false)
        //        {
        //            ReportGridView_Sorting(null, new GridViewSortEventArgs("distributor_lbl", SortDirection.Ascending));
        //            potSort = true;
        //        }
        //        else
        //        {
        //            ReportGridView_Sorting(null, new GridViewSortEventArgs("distributor_lbl", SortDirection.Descending));
        //            potSort = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        //}

        //protected void sort_salesengineer_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        if (potSort == false)
        //        {
        //            ReportGridView_Sorting(null, new GridViewSortEventArgs("Engineer_Name", SortDirection.Ascending));
        //            potSort = true;
        //        }
        //        else
        //        {
        //            ReportGridView_Sorting(null, new GridViewSortEventArgs("Engineer_Name", SortDirection.Descending));
        //            potSort = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        //}

        //protected void sort_businessexpected_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        if (potSort == false)
        //        {
        //            ReportGridView_Sorting(null, new GridViewSortEventArgs("Business_Expected", SortDirection.Ascending));
        //            potSort = true;
        //        }
        //        else
        //        {
        //            ReportGridView_Sorting(null, new GridViewSortEventArgs("Business_Expected", SortDirection.Descending));
        //            potSort = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        //}

        //protected void sort_existing_product_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        if (potSort == false)
        //        {
        //            ReportGridView_Sorting(null, new GridViewSortEventArgs("existing_product_lbl", SortDirection.Ascending));
        //            potSort = true;
        //        }
        //        else
        //        {
        //            ReportGridView_Sorting(null, new GridViewSortEventArgs("existing_product_lbl", SortDirection.Descending));
        //            potSort = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        //}

        //protected void sort_tagutecproduct_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        if (potSort == false)
        //        {
        //            ReportGridView_Sorting(null, new GridViewSortEventArgs("component_lbl", SortDirection.Ascending));
        //            potSort = true;
        //        }
        //        else
        //        {
        //            ReportGridView_Sorting(null, new GridViewSortEventArgs("component_lbl", SortDirection.Descending));
        //            potSort = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        //}

        //protected void sort_overall_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        if (potSort == false)
        //        {
        //            ReportGridView_Sorting(null, new GridViewSortEventArgs("overall_pot_lbl", SortDirection.Ascending));
        //            potSort = true;
        //        }
        //        else
        //        {
        //            ReportGridView_Sorting(null, new GridViewSortEventArgs("overall_pot_lbl", SortDirection.Descending));
        //            potSort = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        //}

        //protected void sort_monthly_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        if (potSort == false)
        //        {
        //            ReportGridView_Sorting(null, new GridViewSortEventArgs("monthly_exp_val", SortDirection.Ascending));
        //            potSort = true;
        //        }
        //        else
        //        {
        //            ReportGridView_Sorting(null, new GridViewSortEventArgs("monthly_exp_val", SortDirection.Descending));
        //            potSort = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        //}

        //protected void sort_industry_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        if (potSort == false)
        //        {
        //            ReportGridView_Sorting(null, new GridViewSortEventArgs("industry_lbl", SortDirection.Ascending));
        //            potSort = true;
        //        }
        //        else
        //        {
        //            ReportGridView_Sorting(null, new GridViewSortEventArgs("industry_lbl", SortDirection.Descending));
        //            potSort = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        //}

        //protected void sort_sub_industry_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        if (potSort == false)
        //        {
        //            ReportGridView_Sorting(null, new GridViewSortEventArgs("sub_industry_lbl", SortDirection.Ascending));
        //            potSort = true;
        //        }
        //        else
        //        {
        //            ReportGridView_Sorting(null, new GridViewSortEventArgs("sub_industry_lbl", SortDirection.Descending));
        //            potSort = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        //}

        //protected void sort_type_Click(object sender, ImageClickEventArgs e)
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        if (potSort == false)
        //        {
        //            ReportGridView_Sorting(null, new GridViewSortEventArgs("project_type", SortDirection.Ascending));
        //            potSort = true;
        //        }
        //        else
        //        {
        //            ReportGridView_Sorting(null, new GridViewSortEventArgs("project_type", SortDirection.Descending));
        //            potSort = false;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        //}
        #endregion

        protected void btn_download_click(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string customer_num = hdn_CustomerNum.Value;
            string project_number = hdn_projectnum.Value;
            try
            {
                DataTable dtFiles = objMDP.Get_File_Names(customer_num, project_number);
                if (dtFiles != null)
                {
                    if (dtFiles.Rows.Count > 0)
                    {
                        using (ZipFile zip = new ZipFile())
                        {
                            try
                            {
                                zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                                zip.AddDirectoryByName("Files");
                                int no_ofFiles = 0;
                                for (int i = 0; i < dtFiles.Rows.Count; i++)
                                {
                                    string filename = dtFiles.Rows[i].ItemArray[0].ToString();
                                    string filePath = Server.MapPath("~/Uploads/" + filename);
                                    if (File.Exists(filePath))
                                    {
                                        zip.AddFile(filePath, "Files"); no_ofFiles++;
                                    }
                                }

                                if (no_ofFiles != 0)
                                {
                                    string zipName = String.Format("ProjectFiles_{0}_{1}.zip", customer_num, project_number);
                                    Response.Clear();
                                    Response.ContentType = "application/zip";
                                    Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                                    zip.Save(Response.OutputStream);
                                    Response.Flush();
                                    HttpContext.Current.Response.End();
                                }

                                else
                                {
                                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('file(s) was not found.');", true);
                                    proceed_Click(null, null);
                                }
                            }

                            catch (Exception ex)
                            {
                                objFunc.LogError(ex);
                            }
                        }
                    }

                    else
                    {

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup1", "alert('No file available.');", true);
                        proceed_Click(null, null);

                    }

                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup2", "alert('No file available.');", true);
                proceed_Click(null, null);

            }
        }

        protected void rdBtnTaegutec_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                hdnsearch.Value = "";
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (rdBtnTaegutec.Checked)
                {
                    Session["cter"] = "TTA";
                    cter = "TTA";
                }
                if (rdBtnDuraCab.Checked)
                {
                    Session["cter"] = "DUR";
                    cter = "DUR";
                }
                LoadBranches();
                LoadCustomerDetails_HO();
                LoadAllSEs();
                LoadAllReviewers();
                LoadAllEscalator();
                save.Visible = false;
                export.Visible = false;
                back.Visible = false;
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }
        protected void ddlCustomerClass_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                ddCustomerClass = ddlCustomerClass.SelectedItem.Value == "ALL" ? null : ddlCustomerClass.SelectedItem.Value;
                string strUserId = Session["UserId"].ToString();
                string roleId = Session["RoleId"].ToString();
                string salesEngName = Session["UserName"].ToString();
                string branchCode = Session["BranchCode"].ToString();
                if (roleId == "HO")
                {
                    LoadCustomerDetails_HO();
                }
                else if (roleId == "TM")
                {
                    LoadCustomerDetails_TM(strUserId);

                }
                else if (roleId == "BM")
                {
                    LoadCustomerDetails_BM(branchCode);
                }
                else if (roleId == "SE")
                {
                    LoadCustomerDetails_SE(strUserId);
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Author : Anamika Kumari
        /// Date : Oct 18, 2016
        /// Description : It will reset all the fields for filter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnreset_Click(object sender, EventArgs e)
        {
            try
            {
                LoadAllDropDownAtFirst();
                lbl_Message.Visible = false;
                lbl_nocustomer.Visible = false;
                lbl_values.Visible = false;
                lbl_customer.Visible = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Author : Anamika
        /// Date : Oct 18, 2016
        /// Description : Based on selection of status, all the other drop down will be loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadAllDropDown();
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Author : Anamika
        /// Date : Oct 19, 2016
        /// Description : Based on selection of customer, all the other drop down will be loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlCustomer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadAllDropDown();
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Author : Anamika
        /// Date : Oct 20, 2016
        /// Description : Based on selection of reviewer, all the other drop down will be loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlReviewerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadAllDropDown();
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Author : Anamika
        /// Date : Oct 20, 2016
        /// Description : Based on selection of escalator, all the other drop down will be loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlEscalate_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                LoadAllDropDown();
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Author : Anamika
        /// Date : Oct 20, 2016
        /// Description : Clicking on status of the project will lead to MDPEntry page based on customer number
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void status_lbl_Command(object sender, CommandEventArgs e)
        {
            try
            {
                var lb = (LinkButton)sender;
                var row = (GridViewRow)lb.NamingContainer;
                if (row != null)
                {
                    var cust_num = row.FindControl("hdn_customernumber") as HiddenField;
                    var project_number = row.FindControl("hdn_projectnumber") as HiddenField;
                    var distributor_number = row.FindControl("hdn_distributornumber") as HiddenField;
                    Session["Customer_Number"] = Convert.ToString(cust_num.Value);
                    Session["Project_Number"] = Convert.ToString(project_number.Value);
                    Session["Distributor_Number"] = Convert.ToString(distributor_number.Value);
                    Session["Project"] = "Existing";
                    if (Session["Customer_Number"] != null)
                    {
                        Response.Redirect("MDPEntry.aspx?Projects");
                    }
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }
        #endregion

        #region Methods

        protected void LoadAllReviewers()
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                DataTable dtREDetails = new DataTable();
                dtREDetails = objMDP.GetAllReviewer();
                if (dtREDetails != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("RE_number", typeof(string));
                    dtDeatils.Columns.Add("RE_name", typeof(string));

                    for (int i = 0; i < dtREDetails.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(dtREDetails.Rows[i].ItemArray[0].ToString(), dtREDetails.Rows[i].ItemArray[1].ToString() + " " + "(" + dtREDetails.Rows[i].ItemArray[0].ToString() + ")");
                    }
                    ddlReviewerList.DataSource = dtDeatils;
                    ddlReviewerList.DataTextField = "RE_name";
                    ddlReviewerList.DataValueField = "RE_number";
                    ddlReviewerList.DataBind();
                    ddlReviewerList.Items.Insert(0, "ALL");
                }
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void LoadAllEscalator()
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                DataTable dtHoDetails = new DataTable();
                dtHoDetails = objMDP.GetAllHO();
                if (dtHoDetails != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("HO_number", typeof(string));
                    dtDeatils.Columns.Add("HO_name", typeof(string));


                    for (int i = 0; i < dtHoDetails.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(dtHoDetails.Rows[i].ItemArray[0].ToString(), dtHoDetails.Rows[i].ItemArray[1].ToString() + " " + "(" + dtHoDetails.Rows[i].ItemArray[0].ToString() + ")");
                    }
                    ddlEscalate.DataSource = dtDeatils;
                    ddlEscalate.DataTextField = "HO_name";
                    ddlEscalate.DataValueField = "HO_number";
                    ddlEscalate.DataBind();
                    ddlEscalate.Items.Insert(0, "ALL");
                }
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void LoadAllSEs()
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                DataTable dtSEDetails = new DataTable();
                // dtSEDetails = objMDP.GetAllSE();
                string roleId = Session["RoleId"].ToString();
                string userId = Session["UserId"].ToString();
                string branchcode = Session["BranchCode"].ToString();
                objRSum.BranchCode = roleId == "TM" && ddlBranch.SelectedItem.Value == "ALL" ? userId : ddlBranch.SelectedItem.Value;
                objRSum.roleId = roleId;
                objRSum.flag = "SalesEngineer";
                objRSum.cter = cter;
                dtSEDetails = objRSum.getFilterAreaValue(objRSum);
                if (dtSEDetails != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("SE_number", typeof(string));
                    dtDeatils.Columns.Add("SE_name", typeof(string));

                    for (int i = 0; i < dtSEDetails.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(dtSEDetails.Rows[i].ItemArray[0].ToString(), dtSEDetails.Rows[i].ItemArray[1].ToString() + " " + "(" + dtSEDetails.Rows[i].ItemArray[0].ToString() + ")");
                    }
                    ddlOwnerList.DataSource = dtDeatils;
                    ddlOwnerList.DataTextField = "SE_name";
                    ddlOwnerList.DataValueField = "SE_number";
                    ddlOwnerList.DataBind();
                    ddlOwnerList.Items.Insert(0, "ALL");
                }
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }
        protected void LoadAllSE_BM()
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                DataTable dtSEDetails = new DataTable();
                string branchcode = Session["BranchCode"].ToString();
                dtSEDetails = objMDP.GetAllSE_BM(branchcode);
                if (dtSEDetails != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("SE_number", typeof(string));
                    dtDeatils.Columns.Add("SE_name", typeof(string));


                    for (int i = 0; i < dtSEDetails.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(dtSEDetails.Rows[i].ItemArray[0].ToString(), dtSEDetails.Rows[i].ItemArray[1].ToString() + " " + "(" + dtSEDetails.Rows[i].ItemArray[0].ToString() + ")");
                    }
                    ddlOwnerList.DataSource = dtDeatils;
                    ddlOwnerList.DataTextField = "SE_name";
                    ddlOwnerList.DataValueField = "SE_number";
                    ddlOwnerList.DataBind();
                    ddlOwnerList.Items.Insert(0, "ALL");
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void LoadAllSE_TM(string strUserId)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                DataTable dtSEDetails = new DataTable();
                string branchcode = Session["BranchCode"].ToString();
                dtSEDetails = objMDP.GetAllSE_TM(strUserId);
                if (dtSEDetails != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("SE_number", typeof(string));
                    dtDeatils.Columns.Add("SE_name", typeof(string));

                    for (int i = 0; i < dtSEDetails.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(dtSEDetails.Rows[i].ItemArray[0].ToString(), dtSEDetails.Rows[i].ItemArray[1].ToString() + " " + "(" + dtSEDetails.Rows[i].ItemArray[0].ToString() + ")");
                    }

                    ddlOwnerList.DataSource = dtDeatils;
                    ddlOwnerList.DataTextField = "SE_name";
                    ddlOwnerList.DataValueField = "SE_number";
                    ddlOwnerList.DataBind();
                    ddlOwnerList.Items.Insert(0, "ALL");
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void LoadAllReviewer_BM(string territory_eng_Id)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                DataTable dtREDetails = new DataTable();
                dtREDetails = objMDP.GetAllReviewer_BM(territory_eng_Id);
                if (dtREDetails != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("RE_number", typeof(string));
                    dtDeatils.Columns.Add("RE_name", typeof(string));


                    for (int i = 0; i < dtREDetails.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(dtREDetails.Rows[i].ItemArray[0].ToString(), dtREDetails.Rows[i].ItemArray[1].ToString() + " " + "(" + dtREDetails.Rows[i].ItemArray[0].ToString() + ")");
                    }
                    ddlReviewerList.DataSource = dtDeatils;
                    ddlReviewerList.DataTextField = "RE_name";
                    ddlReviewerList.DataValueField = "RE_number";
                    ddlReviewerList.DataBind();
                    ddlReviewerList.Items.Insert(0, "ALL");
                }
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void LoadAllReviewer_TM(string branch_manager_Id)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                DataTable dtREDetails = new DataTable();
                dtREDetails = objMDP.GetAllReviewer_TM(branch_manager_Id);
                if (dtREDetails != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("RE_number", typeof(string));
                    dtDeatils.Columns.Add("RE_name", typeof(string));


                    for (int i = 0; i < dtREDetails.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(dtREDetails.Rows[i].ItemArray[0].ToString(), dtREDetails.Rows[i].ItemArray[1].ToString() + " " + "(" + dtREDetails.Rows[i].ItemArray[0].ToString() + ")");
                    }
                    ddlReviewerList.DataSource = dtDeatils;
                    ddlReviewerList.DataTextField = "RE_name";
                    ddlReviewerList.DataValueField = "RE_number";
                    ddlReviewerList.DataBind();
                    ddlReviewerList.Items.Insert(0, "ALL");

                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void LoadAllReviewer_SE(string sales_eng_id)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                DataTable dtREDetails = new DataTable();
                dtREDetails = objMDP.GetAllReviewer_SE(sales_eng_id);
                if (dtREDetails != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("RE_number", typeof(string));
                    dtDeatils.Columns.Add("RE_name", typeof(string));

                    for (int i = 0; i < dtREDetails.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(dtREDetails.Rows[i].ItemArray[0].ToString(), dtREDetails.Rows[i].ItemArray[1].ToString() + " " + "(" + dtREDetails.Rows[i].ItemArray[0].ToString() + ")");
                    }
                    ddlReviewerList.DataSource = dtDeatils;
                    ddlReviewerList.DataTextField = "RE_name";
                    ddlReviewerList.DataValueField = "RE_number";
                    ddlReviewerList.DataBind();
                    ddlReviewerList.Items.Insert(0, "ALL");
                }
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void LoadAllEscalator_BM(string branch_manager_Id)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                DataTable dtHoDetails = new DataTable();
                dtHoDetails = objMDP.GetAllEscalator_BM(branch_manager_Id);
                if (dtHoDetails != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("HO_number", typeof(string));
                    dtDeatils.Columns.Add("HO_name", typeof(string));


                    for (int i = 0; i < dtHoDetails.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(dtHoDetails.Rows[i].ItemArray[0].ToString(), dtHoDetails.Rows[i].ItemArray[1].ToString() + " " + "(" + dtHoDetails.Rows[i].ItemArray[0].ToString() + ")");
                    }
                    ddlEscalate.DataSource = dtDeatils;
                    ddlEscalate.DataTextField = "HO_name";
                    ddlEscalate.DataValueField = "HO_number";
                    ddlEscalate.DataBind();
                    ddlEscalate.Items.Insert(0, "ALL");
                }
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void LoadAllEscalator_TM(string territory_engineer_Id)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                DataTable dtHoDetails = new DataTable();
                dtHoDetails = objMDP.GetAllEscalator_TM(territory_engineer_Id);
                if (dtHoDetails != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("HO_number", typeof(string));
                    dtDeatils.Columns.Add("HO_name", typeof(string));


                    for (int i = 0; i < dtHoDetails.Rows.Count; i++)
                    {
                        dtDeatils.Rows.Add(dtHoDetails.Rows[i].ItemArray[0].ToString(), dtHoDetails.Rows[i].ItemArray[1].ToString() + " " + "(" + dtHoDetails.Rows[i].ItemArray[0].ToString() + ")");
                    }
                    ddlEscalate.DataSource = dtDeatils;
                    ddlEscalate.DataTextField = "HO_name";
                    ddlEscalate.DataValueField = "HO_number";
                    ddlEscalate.DataBind();
                    ddlEscalate.Items.Insert(0, "ALL");
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void LoadAllIndustries()
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                DataTable dtIndustry = new DataTable();
                dtIndustry = objMDP.GetAllIndustriesReports();
                if (dtIndustry != null)
                {

                    ddlIndustryList.DataSource = dtIndustry;
                    ddlIndustryList.DataTextField = "IndustryName";
                    ddlIndustryList.DataValueField = "IndustryId";
                    ddlIndustryList.DataBind();
                    ddlIndustryList.Items.Insert(0, "ALL");
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void LoadAllSubIndustries()
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                DataTable dtIndustry = new DataTable();
                dtIndustry = objMDP.GetAllSubIndustriesReports();
                if (dtIndustry != null)
                {
                    ddlsubIndustryList.DataSource = dtIndustry;
                    ddlsubIndustryList.DataTextField = "sub_IndustryName";
                    ddlsubIndustryList.DataValueField = "Sub_IndustryId";
                    ddlsubIndustryList.DataBind();
                    ddlsubIndustryList.Items.Insert(0, "ALL");

                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Load customer list for Sales Engineer
        /// </summary>
        /// <param name="strUserId"></param>
        /// <param name="roleId"></param>
        protected void LoadCustomerDetails_SE(string strUserId)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                dtCutomerDetails = new DataTable();
                // dtCutomerDetails = objMDP.LoadCustomerDetails_SE_Reports(strUserId);
                dtCutomerDetails = objMDP.LoadCustomerDetailstype(strUserId, "SE", null, null, cter, ddCustomerClass);
                if (dtCutomerDetails != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("customer_number", typeof(string));
                    dtDeatils.Columns.Add("customer_name", typeof(string));


                    for (int i = 0; i < dtCutomerDetails.Rows.Count; i++)
                    {
                        //dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[1].ToString(), dtCutomerDetails.Rows[i].ItemArray[2].ToString() + " " + "(" + dtCutomerDetails.Rows[i].ItemArray[1].ToString() + ")");
                        dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[1].ToString(), dtCutomerDetails.Rows[i].ItemArray[7].ToString() + "(" + dtCutomerDetails.Rows[i].ItemArray[1].ToString() + ")");
                    }
                    ddlCustomer.DataSource = dtDeatils;
                    ddlCustomer.DataTextField = "customer_name";
                    ddlCustomer.DataValueField = "customer_number";
                    ddlCustomer.DataBind();
                    ddlCustomer.Items.Insert(0, "ALL");
                }
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Load customer list for branch manager
        /// </summary>
        /// <param name="branchCode"></param>
        protected void LoadCustomerDetails_BM(string branchCode)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                dtCutomerDetails = new DataTable();
                //dtCutomerDetails = objMDP.LoadCustomerDetails_BM_Reports(branchCode);
                string salesEngineerId = ddlOwnerList.SelectedItem.Value == "ALL" ? null : ddlOwnerList.SelectedItem.Value;
                dtCutomerDetails = objMDP.LoadCustomerDetailstype(salesEngineerId, "BM", null, branchCode, cter, ddCustomerClass);
                if (dtCutomerDetails != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("customer_number", typeof(string));
                    dtDeatils.Columns.Add("customer_name", typeof(string));

                    for (int i = 0; i < dtCutomerDetails.Rows.Count; i++)
                    {
                        //dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[1].ToString(), dtCutomerDetails.Rows[i].ItemArray[2].ToString() + " " + "(" + dtCutomerDetails.Rows[i].ItemArray[1].ToString() + ")");
                        dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[1].ToString(), dtCutomerDetails.Rows[i].ItemArray[7].ToString() + "(" + dtCutomerDetails.Rows[i].ItemArray[1].ToString() + ")");

                    }
                    ddlCustomer.DataSource = dtDeatils;
                    ddlCustomer.DataTextField = "customer_name";
                    ddlCustomer.DataValueField = "customer_number";
                    ddlCustomer.DataBind();
                    ddlCustomer.Items.Insert(0, "ALL");
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        protected void LoadCustomerDetails_TM(string strUserId)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                dtCutomerDetails = new DataTable();
                // dtCutomerDetails = objMDP.LoadCustomerDetails_TM(strUserId);
                string branchCode = ddlBranch.SelectedItem.Value == "ALL" ? null : ddlBranch.SelectedItem.Value;
                dtCutomerDetails = objMDP.LoadCustomerDetailstype(null, "TM", null, branchCode, cter, ddCustomerClass, strUserId);
                if (dtCutomerDetails != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("customer_number", typeof(string));
                    dtDeatils.Columns.Add("customer_name", typeof(string));

                    for (int i = 0; i < dtCutomerDetails.Rows.Count; i++)
                    {
                        //dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[1].ToString(), dtCutomerDetails.Rows[i].ItemArray[2].ToString() + " " + "(" + dtCutomerDetails.Rows[i].ItemArray[1].ToString() + ")");
                        dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[1].ToString(), dtCutomerDetails.Rows[i].ItemArray[7].ToString() + "(" + dtCutomerDetails.Rows[i].ItemArray[1].ToString() + ")");
                    }
                    ddlCustomer.DataSource = dtDeatils;
                    ddlCustomer.DataTextField = "customer_name";
                    ddlCustomer.DataValueField = "customer_number";
                    ddlCustomer.DataBind();
                    ddlCustomer.Items.Insert(0, "ALL");
                }
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Load customer list for HO
        /// </summary>
        protected void LoadCustomerDetails_HO()
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                dtCutomerDetails = new DataTable();
                //dtCutomerDetails = objMDP.LoadCustomerDetails_HO_Reports();
                string branchCode = ddlBranch.SelectedItem.Value;
                dtCutomerDetails = objMDP.LoadCustomerDetailstype(null, "HO", null, branchCode, cter, ddCustomerClass);
                if (dtCutomerDetails != null)
                {
                    DataTable dtDeatils = new DataTable();
                    dtDeatils.Columns.Add("customer_number", typeof(string));
                    dtDeatils.Columns.Add("customer_name", typeof(string));
                    for (int i = 0; i < dtCutomerDetails.Rows.Count; i++)
                    {
                        //  dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[1].ToString(), dtCutomerDetails.Rows[i].ItemArray[2].ToString() + " " + "(" + dtCutomerDetails.Rows[i].ItemArray[1].ToString() + ")");
                        dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[1].ToString(), dtCutomerDetails.Rows[i].ItemArray[7].ToString() + "(" + dtCutomerDetails.Rows[i].ItemArray[1].ToString() + ")");
                    }

                    ddlCustomer.DataSource = dtDeatils;
                    ddlCustomer.DataTextField = "customer_name";
                    ddlCustomer.DataValueField = "customer_number";
                    ddlCustomer.DataBind();
                    ddlCustomer.Items.Insert(0, "ALL");
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Load All the branches
        /// </summary>
        protected void LoadBranches()
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

                string roleId = Session["RoleId"].ToString();
                string userId = Session["UserId"].ToString();
                string branchcode = Session["BranchCode"].ToString();
                Review objRSum = new Review();
                objRSum.BranchCode = userId; // passing here territory Engineer Id  as branch code IF role is TM 
                objRSum.roleId = roleId;
                objRSum.flag = "Branch";
                objRSum.cter = cter;
                DataTable dtData = new DataTable();
                dtData = objRSum.getFilterAreaValue(objRSum);
                if (dtData != null)
                {
                    if (dtData.Rows.Count != 0)
                    {
                        ddlBranch.DataSource = dtData;
                        ddlBranch.DataTextField = "BranchDesc";
                        ddlBranch.DataValueField = "BranchCode";
                        ddlBranch.DataBind();
                        ddlBranch.Items.Insert(0, "ALL");
                    }
                    if (roleId == "BM" || roleId == "SE")
                    {
                        string branchdesc = Session["BranchDesc"].ToString();
                        dtData.Columns.Add("BranchCode");
                        dtData.Columns.Add("BranchDesc");
                        dtData.Rows.Add(branchcode, branchdesc);
                        ddlBranch.DataSource = dtData;
                        ddlBranch.DataTextField = "BranchDesc";
                        ddlBranch.DataValueField = "BranchCode";
                        ddlBranch.DataBind();
                    }
                }
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }

            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        protected void LoadBranches_TM(string strUserId)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                DataTable dtBranchesList = new DataTable();
                if (Session["UserId"] != null)
                {

                    Budget objBudgets = new Budget();
                    dtBranchesList = objMDP.getBranch_TM(strUserId);
                    if (dtBranchesList != null)
                    {
                        ddlBranch.DataSource = dtBranchesList;
                        ddlBranch.DataTextField = "region_description";
                        ddlBranch.DataValueField = "BranchCode";
                        ddlBranch.DataBind();
                        ddlBranch.Items.Insert(0, "ALL");
                    }
                }
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// To bind color to the gridview
        /// </summary>
        //protected void bindgridColor()
        //{
        //    try
        //    {
        //        if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
        //        if (projectreports.Rows.Count != 0)
        //        {
        //            int color = 0;

        //            foreach (GridViewRow row in projectreports.Rows)
        //            {
        //                var check = row.FindControl("salesengineer_lbl") as Label;
        //                if (check.Text.ToString() == "Grand Total")
        //                {
        //                    row.CssClass = "MainTotal";

        //                }
        //                color++;
        //                if (color == 1) { row.CssClass = "color_Product1 "; }
        //                else if (color == 2)
        //                {
        //                    row.CssClass = "color_Product2 ";
        //                    color = 0;
        //                }
        //                if (check.Text.ToString() == "Grand Total")
        //                {
        //                    row.CssClass = "MainTotal";

        //                }
        //            }
        //        }
        //    }

        //    catch (Exception ex)
        //    {
        //        objFunc.LogError(ex);
        //    }
        //}

        #region Export Report to Excel Sheet
        private void ExportReport(DataTable dtNew, DataTable dtExport)
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                //    proceed_Click(null, null);
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "ProjectReports.xls"));
                Response.ContentType = "application/ms-excel";
                string branch = ddlBranch.SelectedItem.Text;
                string customer = ddlCustomer.SelectedItem.Text;
                string industry = ddlIndustryList.SelectedItem.Text;
                string owner = ddlOwnerList.SelectedItem.Text;
                string reviewer = ddlReviewerList.SelectedItem.Text;
                string escalated = ddlEscalate.SelectedItem.Text;
                string date = DateTime.Now.ToString("dd/MM/yyyy");
                string creationdate = creationrange.Text;
                string targetdate = targetrange.Text;
                using (StringWriter sw = new StringWriter())
                {
                    using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                    {
                        //  Create a table to contain the grid
                        Table table = new Table();

                        //  include the gridline settings
                        table.GridLines = projectreports.GridLines;
                        //  add the header row to the table
                        if (projectreports.HeaderRow != null)
                        {
                            PrepareControlForExport(projectreports.HeaderRow);
                            table.Rows.Add(projectreports.HeaderRow);
                            projectreports.DataSource = dtNew;
                            projectreports.HeaderRow.Cells[11].Visible = false;
                            projectreports.HeaderRow.Cells[12].Visible = false;
                        }
                        else
                        {
                            projectreports.DataSource = null;
                        }
                        projectreports.DataBind();

                        // dt = CheckforSearch(dt);
                        // setsessions(dt);
                        //  dtNew = addGrandTotalToExceltable(dt);
                        //  add each of the data rows to the table
                        foreach (GridViewRow row in projectreports.Rows)
                        {
                            PrepareControlForExport(row);
                            table.Rows.Add(row);
                            row.Cells[11].Visible = false;
                            row.Cells[12].Visible = false;
                        }
                        table.Rows[0].Height = 30;
                        table.Rows[0].BackColor = Color.LightSeaGreen;
                        table.Rows[0].Font.Bold = true;

                        for (int i = 0; i < dtNew.Rows.Count; i++)
                        {
                            if (Convert.ToString(dtNew.Rows[i][8]).Contains("GRAND TOTAL"))
                            {
                                table.Rows[i + 1].BackColor = Color.Gray;
                                table.Rows[i + 1].Font.Bold = true;
                            }
                        }

                        sw.WriteLine("<table><tr><td></td><td></td><td></td><td colspan=3 style='font-weight: bold; font-size:20px; '>MDP Project Reports</td></table>");
                        sw.WriteLine("<table style='margin-left: 180px;'>");

                        //  render the table into the htmlwriter
                        if (Session["RoleId"].ToString() == "HO")
                        {
                            string territory;
                            if (rdBtnTaegutec.Checked)
                            {
                                territory = "TAEGUTEC";
                            }
                            else
                            {
                                territory = "DURACARB";
                            }
                            sw.WriteLine("<tr><td></td><td></td><td></td><td style='font-weight: bold;'>TERRITORY:" + "</td><td colspan=8 style='font-style: italic;'>" + territory + "</td></tr>");
                        }

                        sw.WriteLine("<tr><td></td><td></td><td><td border='1px' style='font-weight:bold;'> BRANCH :" + "</td><td colspan=8 style='font-style: italic;'>" + branch + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td><td border='1px' style='font-weight:bold;'>CUSTOMER :" + "</td><td colspan=8 style='font-style:italic;'>" + customer + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td><td border='1px' style='font-weight:bold;'>INDUSTRY :" + "</td><td colspan=8 style='font-style:italic;'>" + industry + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td><td border='1px' style='font-weight:bold;'>OWNER :" + "</td><td colspan=8 style='font-style:italic;'>" + owner + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td><td border='1px' style='font-weight:bold;'> REVIEWER :" + "</td><td colspan=8 style='font-style:italic;'>" + reviewer + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td><td border='1px' style='font-weight:bold;'>ESCALATED TO :" + "</td><td colspan=8 style='font-style:italic;'>" + escalated + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td><td border='1px' style='font-weight:bold;'>CREATION DATE :" + "</td><td colspan=8 style='font-style:italic;'>" + creationdate + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td><td border='1px' style='font-weight:bold;'>TARGET DATE :" + "</td><td colspan=8 style='font-style:italic;'>" + targetdate + "</td></tr>");
                        if (!String.IsNullOrEmpty(hdnsearch.Value))
                        {
                            sw.WriteLine("<tr><td></td><td></td><td><td border='1px' style='font-weight:bold;'>SEARCH TEXT:" + "</td><td colspan=8 style='font-style:italic;'>" + hdnsearch.Value + "</td></tr>");
                            sw.WriteLine("</table><br/>");
                        }
                        table.RenderControl(htw);
                    }

                    Response.Write(sw.ToString());
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                    Response.End();
                    projectreports.DataSource = dtExport;
                    projectreports.DataBind();
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        public override void VerifyRenderingInServerForm(Control control)
        {
        }

        private static void PrepareControlForExport(Control control)
        {
            for (int i = 0; i < control.Controls.Count; i++)
            {
                Control current = control.Controls[i];
                if (current is LinkButton)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as LinkButton).Text));
                }
                else if (current is ImageButton)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as ImageButton).AlternateText));
                }
                else if (current is HyperLink)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as HyperLink).Text));
                }

                else if (current is TextBox)
                {
                    control.Controls.Remove(current);
                    control.Controls.AddAt(i, new LiteralControl((current as TextBox).Text));
                }
                else if (current is DropDownList)
                {
                    control.Controls.Remove(current);
                    //control.Controls.AddAt(i, new LiteralControl((current as DropDownList).SelectedItem.Text));
                }
                else if (current is HiddenField)
                {
                    control.Controls.Remove(current);
                }
                else if (current is CheckBox)
                {
                    control.Controls.Remove(current);
                    // control.Controls.AddAt(i, new LiteralControl((current as CheckBox).Checked ? "True" : "False"));
                }

                else if (current is System.Web.UI.WebControls.Image)
                {
                    control.Controls.Remove(current);
                    // control.Controls.AddAt(i, new LiteralControl((current as CheckBox).Checked ? "True" : "False"));
                }

                else if (current is Button)
                {
                    control.Controls.Remove(current);
                    // control.Controls.AddAt(i, new LiteralControl((current as CheckBox).Checked ? "True" : "False"));
                }
                if (current.HasControls())
                {
                    PrepareControlForExport(current);
                }
            }
        }
        #endregion

        private void RegisterPostBackControl()
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                foreach (GridViewRow row in projectreports.Rows)
                {
                    LinkButton lnkFull = row.FindControl("status_lbl") as LinkButton;
                    ScriptManager.GetCurrent(this).RegisterPostBackControl(lnkFull);
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }
        private void RegisterCancelPostBackControl()
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                foreach (GridViewRow row in projectreports.Rows)
                {
                    ImageButton lnkFull = row.FindControl("btn_cancel") as ImageButton;
                    ScriptManager.GetCurrent(this).RegisterPostBackControl(lnkFull);
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }
        private void RegisterDownloadPostBackControl()
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                foreach (GridViewRow row in projectreports.Rows)
                {
                    ImageButton lnkFull = row.FindControl("btn_download") as ImageButton;
                    ScriptManager.GetCurrent(this).RegisterPostBackControl(lnkFull);
                }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Author : Anamika
        /// Date : Oct 19, 2016
        /// Desc : Fetch all the details based on selection in filter and load to all the dropdowns
        /// </summary>
        private void LoadAllDropDown()
        {
            string strReviewer = string.Empty;
            string strEscalate = string.Empty;
            string strStatus = string.Empty;
            string strOwner = string.Empty;
            string strIndustry = string.Empty;
            string strSubIndustry = string.Empty;
            string strCustomer = string.Empty;
            try
            {

                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                DataSet dsmdpreports = new DataSet();
                string strUserId = Session["UserId"].ToString();
                string roleId = Session["RoleId"].ToString();
                if (roleId != "HO")
                {
                    branchcode = Session["BranchCode"].ToString();
                    strOwner = strUserId;
                }
                if (Session["cter"] == null && roleId == "HO")
                {
                    Session["cter"] = "TTA";
                    cter = "TTA";

                }
                if (Convert.ToString(Session["cter"]) == "DUR")
                {
                    rdBtnDuraCab.Checked = true;
                    rdBtnTaegutec.Checked = false;
                    cter = "DUR";
                }
                else
                {
                    rdBtnTaegutec.Checked = true;
                    rdBtnDuraCab.Checked = false;
                    cter = "TTA";
                }
                strCustomer = ddlCustomer.SelectedItem.Text == "ALL" ? null : ddlCustomer.SelectedItem.Text;
                ddCustomerClass = ddlCustomerClass.SelectedItem.Value == "ALL" ? null : ddlCustomerClass.SelectedItem.Value;
                branchcode = ddlBranch.SelectedItem.Value == "ALL" ? null : ddlBranch.SelectedItem.Value;
                customernumber = ddlCustomer.SelectedItem.Value == "ALL" ? null : ddlCustomer.SelectedItem.Value;
                strStatus = ddlStatus.SelectedItem.Value == "ALL" ? null : ddlStatus.SelectedItem.Value;
                strReviewer = ddlReviewerList.SelectedItem.Value == "ALL" ? null : ddlReviewerList.SelectedItem.Value;
                strEscalate = ddlEscalate.SelectedItem.Value == "ALL" ? null : ddlEscalate.SelectedItem.Value;
                strOwner = ddlOwnerList.SelectedItem.Value == "ALL" ? null : ddlOwnerList.SelectedItem.Value;
                strIndustry = ddlIndustryList.SelectedItem.Value == "ALL" ? null : ddlIndustryList.SelectedItem.Value;
                strSubIndustry = ddlsubIndustryList.SelectedItem.Value == "ALL" ? null : ddlsubIndustryList.SelectedItem.Value;
                dsmdpreports = objMDP.LoadFilterValue(strUserId, roleId, branchcode, customernumber, ddCustomerClass, strOwner, strStatus, strReviewer, strEscalate, cter);
                if (dsmdpreports.Tables.Count > 0)
                {
                    if (dsmdpreports.Tables[0].Rows.Count > 0)
                    {
                        #region ddlCustomer
                        ddlCustomer.DataSource = dsmdpreports.Tables[0];
                        ddlCustomer.DataTextField = "customer_name";
                        ddlCustomer.DataValueField = "customer_number";
                        ddlCustomer.DataBind();
                        ddlCustomer.Items.Insert(0, "ALL");
                        if (customernumber != null)
                        {
                            var result = dsmdpreports.Tables[0].Rows.Cast<DataRow>()
                            .Select(row => row[0].ToString())
                            .ToArray();
                            var s1 = Array.FindAll(result, s => s.Equals(customernumber));
                            if (s1.Length > 0)
                                ddlCustomer.SelectedValue = customernumber;
                            else
                                ddlCustomer.SelectedValue = "ALL";
                        }
                        else
                        {
                            ddlCustomer.SelectedValue = "ALL";
                        }
                        lbl_customer.Visible = false;
                        #endregion
                    }
                    else
                    {
                        ddlCustomer.DataSource = null;
                        ddlCustomer.DataBind();
                        ddlCustomer.Items.Insert(0, "No Customer");
                        lbl_customer.Visible = true;
                    }
                    #region ddlReviewerList
                    if (dsmdpreports.Tables[1].Rows.Count > 0)
                    {

                        ddlReviewerList.DataSource = dsmdpreports.Tables[1];
                        ddlReviewerList.DataTextField = "ReviewerName";
                        ddlReviewerList.DataValueField = "Reviewer";
                        ddlReviewerList.DataBind();
                        ddlReviewerList.Items.Insert(0, "ALL");
                        if (strReviewer != null)
                        {
                            var result = dsmdpreports.Tables[1].Rows.Cast<DataRow>()
                            .Select(row => row[0].ToString())
                            .ToArray();
                            var s1 = Array.FindAll(result, s => s.Equals(strReviewer));
                            if (s1.Length > 0)
                                ddlReviewerList.SelectedValue = strReviewer;
                            else
                                ddlReviewerList.SelectedValue = "ALL";
                        }
                        else
                        {
                            ddlReviewerList.SelectedValue = "ALL";
                        }
                        lbl_nocustomer.Visible = false;
                    }
                    else
                    {
                        if (roleId == "SE")
                        {
                            LoadAllReviewer_SE(strUserId);
                        }
                        if (roleId == "BM")
                        {
                            LoadAllReviewer_BM(strUserId);
                        }
                        if (roleId == "HO")
                        {
                            if (Session["cter"] == null && roleId == "HO")
                            {
                                Session["cter"] = "TTA";
                                cter = "TTA";

                            }
                            if (Session["cter"].ToString() == "DUR")
                            {
                                rdBtnDuraCab.Checked = true;
                                rdBtnTaegutec.Checked = false;
                                cter = "DUR";
                            }
                            else
                            {
                                rdBtnTaegutec.Checked = true;
                                rdBtnDuraCab.Checked = false;
                                cter = "TTA";
                            }
                            cterDiv.Visible = true;
                            LoadAllReviewers();
                        }

                        if (roleId == "TM")
                        {
                            LoadAllReviewer_TM(strUserId);
                            LoadAllEscalator_TM(strUserId);
                        }
                        lbl_nocustomer.InnerText = "For Customer:" + strCustomer + ", there are no projects. Please select other customer.";
                        lbl_nocustomer.Visible = true;
                    }
                    #endregion
                    #region ddlEscalate
                    if (dsmdpreports.Tables[2].Rows.Count > 0)
                    {

                        ddlEscalate.DataSource = dsmdpreports.Tables[2];
                        ddlEscalate.DataTextField = "EscalatedToName";
                        ddlEscalate.DataValueField = "Escalated_To";
                        ddlEscalate.DataBind();
                        ddlEscalate.Items.Insert(0, "ALL");
                        if (strEscalate != null)
                        {
                            var result = dsmdpreports.Tables[2].Rows.Cast<DataRow>()
                            .Select(row => row[0].ToString())
                            .ToArray();
                            var s1 = Array.FindAll(result, s => s.Equals(strEscalate));
                            if (s1.Length > 0)
                                ddlEscalate.SelectedValue = strEscalate;
                            else
                                ddlEscalate.SelectedValue = "ALL";
                        }
                        else
                        {
                            ddlEscalate.SelectedValue = "ALL";
                        }

                    }
                    else
                    {
                        if (roleId == "SE")
                        {
                            LoadAllEscalator_BM(strUserId);
                        }
                        if (roleId == "BM")
                        {
                            LoadAllEscalator_BM(strUserId);
                        }
                        if (roleId == "HO")
                        {
                            if (Session["cter"] == null && roleId == "HO")
                            {
                                Session["cter"] = "TTA";
                                cter = "TTA";

                            }
                            if (Session["cter"].ToString() == "DUR")
                            {
                                rdBtnDuraCab.Checked = true;
                                rdBtnTaegutec.Checked = false;
                                cter = "DUR";
                            }
                            else
                            {
                                rdBtnTaegutec.Checked = true;
                                rdBtnDuraCab.Checked = false;
                                cter = "TTA";
                            }
                            cterDiv.Visible = true;
                            LoadAllEscalator();
                        }

                        if (roleId == "TM")
                        {
                            LoadAllReviewer_TM(strUserId);
                            LoadAllEscalator_TM(strUserId);
                        }
                    }
                    #endregion
                    if (dsmdpreports.Tables[3].Rows.Count > 0)
                    {
                        #region ddlOwnerList
                        ddlOwnerList.DataSource = dsmdpreports.Tables[3];
                        ddlOwnerList.DataTextField = "assigned_salesengineer_name";
                        ddlOwnerList.DataValueField = "assigned_salesengineer_id";
                        ddlOwnerList.DataBind();
                        ddlOwnerList.Items.Insert(0, "ALL");
                        if (strOwner != null)
                        {
                            var result = dsmdpreports.Tables[3].Rows.Cast<DataRow>()
                            .Select(row => row[0].ToString())
                            .ToArray();
                            var s1 = Array.FindAll(result, s => s.Equals(strOwner));
                            if (s1.Length > 0)
                                ddlOwnerList.SelectedValue = strOwner;
                            else
                                ddlOwnerList.SelectedValue = "ALL";
                        }
                        else
                        {
                            ddlOwnerList.SelectedValue = "ALL";
                        }
                        #endregion
                    }
                    if (dsmdpreports.Tables[5].Rows.Count > 0)
                    {
                        #region ddlBranch

                        ddlBranch.DataSource = dsmdpreports.Tables[5];
                        ddlBranch.DataTextField = "region_description";
                        ddlBranch.DataValueField = "Customer_region";
                        ddlBranch.DataBind();
                        ddlBranch.Items.Insert(0, "ALL");
                        if (branchcode != null)
                        {
                            var result = dsmdpreports.Tables[5].Rows.Cast<DataRow>()
                            .Select(row => row[0].ToString())
                            .ToArray();
                            var s1 = Array.FindAll(result, s => s.Equals(branchcode));
                            if (s1.Length > 0)
                                ddlBranch.SelectedValue = branchcode;
                            else
                                ddlBranch.SelectedValue = "ALL";
                        }
                        else
                        {
                            if (dsmdpreports.Tables[4].Rows.Count == 1)
                                ddlBranch.SelectedValue = dsmdpreports.Tables[4].Rows[0]["Customer_region"].ToString();
                            else
                                ddlBranch.SelectedValue = "ALL";
                        }
                        #endregion
                    }

                    #region commentedcode
                    //ddlCustomerClass.DataSource = dtCutomerDetails;
                    //ddlCustomerClass.DataTextField = "customer_name";
                    //ddlCustomerClass.DataValueField = "customer_number";
                    //ddlCustomerClass.DataBind();
                    //ddlCustomerClass.Items.Insert(0, "ALL");
                    //if (ddCustomerClass != null)
                    //{
                    //    var result = dtCutomerDetails.Rows.Cast<DataRow>()
                    //    .Select(row => row[10].ToString())
                    //    .ToArray();
                    //    var s1 = Array.FindAll(result, s => s.Equals(ddCustomerClass));
                    //    if (s1.Length > 0)
                    //        ddlCustomerClass.SelectedValue = ddCustomerClass;
                    //    else
                    //        ddlCustomerClass.SelectedValue = "ALL";
                    //}
                    //else
                    //{
                    //    ddlCustomerClass.SelectedValue = "ALL";
                    //}
                    #endregion

                    //LoadAllIndustries();
                    //LoadAllSubIndustries();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                }

            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }

        /// <summary>
        /// Author : Anamika
        /// Date : Oct 20, 2016
        /// Desc : This method copies the original datatable and return
        /// </summary>
        /// <param name="originalTable"></param>
        /// <returns></returns>
        public DataTable CloneTable(DataTable originalTable)
        {
            DataTable newTable;
            newTable = originalTable.Copy();
            return newTable;
        }

        /// <summary>
        /// Author : Anamika
        /// Date : Oct 20, 2016
        /// Desc : Load all the dropdowns at first
        /// </summary>
        private void LoadAllDropDownAtFirst()
        {
            try
            {
                cter = null;

                LoadAllIndustries();
                LoadAllSubIndustries();
                ddlStatus.SelectedValue = "ALL";
                //Created_from.Text = "";
                //Created_To.Text = "";
                creationrange.Text = "";
                //Target_From.Text = "";
                //Target_To.Text = "";
                targetrange.Text = "";
                potSort = false;

                if (Session["UserId"].ToString() != null)
                {
                    string strUserId = Session["UserId"].ToString();
                    string roleId = Session["RoleId"].ToString();
                    string salesEngName = Session["UserName"].ToString();
                    string branchCode = Session["BranchCode"].ToString();
                    if (roleId == "SE")
                    {
                        LoadBranches();
                        LoadAllReviewer_SE(strUserId);
                        LoadAllEscalator_BM(strUserId);
                        LoadCustomerDetails_SE(strUserId);
                        ddlBranch.SelectedValue = branchCode;
                        ddlOwnerList.SelectedValue = strUserId;
                        ddlOwnerList.Enabled = false;
                        ddlBranch.Enabled = false;
                        LoadAllSEs();
                        save.Visible = false;
                        export.Visible = false;
                        back.Visible = false;

                    }
                    if (roleId == "BM")
                    {
                        LoadBranches();
                        LoadAllReviewer_BM(strUserId);
                        LoadAllEscalator_BM(strUserId);
                        LoadAllSE_BM();
                        LoadCustomerDetails_BM(branchCode);
                        ddlBranch.SelectedValue = branchCode;
                        ddlBranch.Enabled = false;
                        save.Visible = false;
                        export.Visible = false;
                        back.Visible = false;
                    }
                    if (roleId == "HO")
                    {
                        if (Session["cter"] == null && roleId == "HO")
                        {
                            Session["cter"] = "TTA";
                            cter = "TTA";

                        }
                        if (Session["cter"].ToString() == "DUR")
                        {
                            rdBtnDuraCab.Checked = true;
                            rdBtnTaegutec.Checked = false;
                            cter = "DUR";
                        }
                        else
                        {
                            rdBtnTaegutec.Checked = true;
                            rdBtnDuraCab.Checked = false;
                            cter = "TTA";
                        }
                        cterDiv.Visible = true;
                        LoadBranches();
                        LoadCustomerDetails_HO();
                        LoadAllSEs();
                        LoadAllReviewers();
                        LoadAllEscalator();
                        save.Visible = false;
                        export.Visible = false;
                        back.Visible = false;

                    }

                    if (roleId == "TM")
                    {
                        LoadBranches_TM(strUserId);
                        LoadAllSE_TM(strUserId);
                        LoadAllReviewer_TM(strUserId);
                        LoadAllEscalator_TM(strUserId);
                        LoadCustomerDetails_TM(strUserId);
                        save.Visible = false;
                        export.Visible = false;
                        back.Visible = false;
                    }
                    DataTable dt = new DataTable();
                    dt = objprojtype.getdataprojecttype();
                    ddlptype.DataSource = dt;
                    ddlptype.DataTextField = "Project_Type_Desc";
                    ddlptype.DataValueField = "Project_Type_Desc";
                    ddlptype.DataBind();
                    ddlptype.Items.Insert(0, "ALL");
                }
                else { Response.Redirect("Login.aspx?Login"); }
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
        }


        //private void bindcolorfirstload()
        //{
        //    if (projectreports.Rows.Count != 0)
        //    {
        //        foreach (GridViewRow row in projectreports.Rows)
        //        {
        //            var check = row.FindControl("salesengineer_lbl") as Label;

        //            if (check.Text.ToString() == "Grand Total")
        //            {
        //                row.CssClass = "MainTotal";

        //            }
        //        }
        //    }

        //}

        #endregion

        [WebMethod]

        public static List<string> setGrandtotal(string search)
        {
            DataTable dt = new DataTable();
            DataTable dtOutput = new DataTable();
            List<string> hdndata = new List<string>();
            List<string> searchList = new List<string>();
            DataRow[] filteredRows;
            dt = (DataTable)HttpContext.Current.Session["dtreport"];
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {

                    if (!string.IsNullOrEmpty(search))
                    {

                        if (search.Contains(' '))
                        {
                            searchList = search.Split(' ').ToList();
                        }
                        else
                        {
                            searchList.Add(search);
                        }

                        for (int i = 0; i < searchList.Count; i++)
                        {
                            dtOutput = new DataTable();
                            dtOutput.Columns.Add("ID");
                            dtOutput.Columns.Add("customer_num");
                            dtOutput.Columns.Add("customer_name");
                            dtOutput.Columns.Add("industry_lbl");
                            dtOutput.Columns.Add("overall_pot_lbl");
                            dtOutput.Columns.Add("potential_lakhs_lbl");
                            dtOutput.Columns.Add("existing_product_lbl");
                            dtOutput.Columns.Add("component_lbl");
                            dtOutput.Columns.Add("Stage_Completed");
                            dtOutput.Columns.Add("NoOfStages");
                            dtOutput.Columns.Add("txt_remarks");
                            dtOutput.Columns.Add("IsCanceled");
                            dtOutput.Columns.Add("projects");
                            dtOutput.Columns.Add("project_type");
                            dtOutput.Columns.Add("project_num");
                            dtOutput.Columns.Add("project_status");
                            dtOutput.Columns.Add("monthly_exp_val");
                            dtOutput.Columns.Add("assigned_salesengineer_id");
                            dtOutput.Columns.Add("Reviewer");
                            dtOutput.Columns.Add("Escalated_To");
                            dtOutput.Columns.Add("sub_industry_lbl");
                            dtOutput.Columns.Add("distributor_num");
                            dtOutput.Columns.Add("creation_date_lbl");
                            dtOutput.Columns.Add("Engineer_Name");
                            dtOutput.Columns.Add("Business_Expected");
                            dtOutput.Columns.Add("customer_lbl");
                            dtOutput.Columns.Add("distributor_lbl");
                            dtOutput.Columns.Add("status_lbl");
                            dtOutput.Columns.Add("Percentage");
                            dtOutput.Columns.Add("red");
                            dtOutput.Columns.Add("yellow");
                            dtOutput.Columns.Add("green");
                            dtOutput.Columns.Add("project_title");
                            dtOutput.Columns.Add("cancel");


                            filteredRows = dt.Select("CONVERT(customer_name, System.String) LIKE'%" + searchList[i].Trim()
                                + "%' OR CONVERT(Engineer_Name, System.String)  LIKE '%" + searchList[i].Trim()
                                + "%' OR CONVERT(Business_Expected, System.String)  LIKE '%" + searchList[i].Trim()
                                + "%' OR CONVERT(customer_lbl, System.String)  LIKE '%" + searchList[i].Trim()
                                + "%' OR CONVERT(distributor_lbl, System.String)  LIKE '%" + searchList[i].Trim()
                                + "%' OR CONVERT(status_lbl, System.String)  LIKE '%" + searchList[i].Trim()
                                + "%' OR CONVERT(potential_lakhs_lbl, System.String) LIKE '%" + searchList[i].Trim()
                                + "%' OR CONVERT(existing_product_lbl, System.String)  LIKE '%" + searchList[i].Trim()
                                + "%' OR CONVERT(component_lbl, System.String)  LIKE '%" + searchList[i].Trim()
                                + "%' OR CONVERT(txt_remarks, System.String)  LIKE '%" + searchList[i].Trim()
                                + "%' OR CONVERT(project_type, System.String)  LIKE '%" + searchList[i].Trim()
                                + "%' OR CONVERT(monthly_exp_val, System.String)  LIKE '%" + searchList[i].Trim()
                                + "%' OR CONVERT(project_title, System.String)  LIKE '%" + searchList[i].Trim()
                                + "%'");




                            //filteredRows = dt.Select("CONVERT(ID, System.String) LIKE '%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(customer_num, System.String) LIKE'%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(customer_name, System.String) LIKE'%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(industry_lbl, System.String) LIKE '%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(overall_pot_lbl , System.String) LIKE '%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(potential_lakhs_lbl, System.String) LIKE '%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(existing_product_lbl, System.String)  LIKE '%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(component_lbl, System.String)  LIKE '%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(Stage_Completed, System.String)  LIKE '%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(NoOfStages, System.String)  LIKE '%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(txt_remarks, System.String)  LIKE '%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(IsCanceled, System.String)  LIKE '%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(projects, System.String)  LIKE '%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(project_type, System.String)  LIKE '%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(project_num , System.String) LIKE '%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(project_status, System.String)  LIKE '%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(monthly_exp_val, System.String)  LIKE '%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(assigned_salesengineer_id, System.String)  LIKE '%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(Reviewer, System.String)  LIKE '%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(Escalated_To, System.String)  LIKE '%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(sub_industry_lbl, System.String)  LIKE '%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(distributor_num, System.String)  LIKE '%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(creation_date_lbl, System.String)  LIKE '%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(Engineer_Name, System.String)  LIKE '%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(Business_Expected, System.String)  LIKE '%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(customer_lbl, System.String)  LIKE '%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(distributor_lbl, System.String)  LIKE '%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(status_lbl, System.String)  LIKE '%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(Percentage, System.String)  LIKE '%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(red, System.String)  LIKE '%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(yellow, System.String)  LIKE '%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(green, System.String)  LIKE '%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(project_title, System.String)  LIKE '%" + searchList[i].Trim()
                            //    + "%' OR CONVERT(cancel, System.String)  LIKE '%" + searchList[i].Trim()
                            //    + "%'");
                            //+ "%' OR CONVERT(potential_lakhs_lbl, System.String)  LIKE '%" + search
                            //+ "%' OR CONVERT(Business_Expected, System.String)  LIKE '%" + search
                            //+ "%' OR CONVERT(monthly_exp_val, System.String)  LIKE '%" + search
                            foreach (DataRow dr in filteredRows)
                            {
                                dtOutput.Rows.Add(dr.ItemArray);
                            }
                            dt = dtOutput;
                        }
                    }
                    else
                    {
                        dtOutput = dt;
                    }
                    string projectpotential = dtOutput.AsEnumerable().Sum(x => Convert.ToDecimal(x["potential_lakhs_lbl"])).ToString();
                    string businessexpected = dtOutput.AsEnumerable().Sum(x => Convert.ToDecimal(x["Business_Expected"])).ToString();
                    string monthlybusinessexpected = dtOutput.AsEnumerable().Sum(x => Convert.ToDecimal(x["monthly_exp_val"])).ToString();

                    hdndata.Add(Convert.ToString(projectpotential));
                    hdndata.Add(Convert.ToString(businessexpected));
                    hdndata.Add(Convert.ToString(monthlybusinessexpected));
                }
            }
            return hdndata;
        }
    }
}



