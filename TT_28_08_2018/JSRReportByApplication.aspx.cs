﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.UI.DataVisualization.Charting;
using System.Drawing;
using System.IO.Compression;
using System.Globalization;
using AjaxControlToolkit;
using TaegutecSalesBudget.App_Code.BOL;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;

namespace TaegutecSalesBudget
{
    public partial class JSRReportByApplication : System.Web.UI.Page
    {
        #region GlobalDeclareation
        public static string cter;
        public static int gridLoadedStatus;
        Reports objReports = new Reports();
        csJSR objJSR = new csJSR();
        csJSRDAL objJSRDAL = new csJSRDAL();
        Budget objBudget = new Budget();
        CommonFunctions objCom = new CommonFunctions();
        Review objRSum = new Review();
        List<string> cssList = new List<string>();
        List<string> cssListFamilyHead = new List<string>();
        AdminConfiguration objAdmin = new AdminConfiguration();
        public static string strUserId,roleId;
        string ur = null;
        #endregion

        #region Events
        /// <summary>
        /// Author  : Anamika
        /// Date    : April 27,2017
        /// Desc    : Page Load
        /// Modified by : K.LakshmiBindu
        /// Modified date :Dec 4,2018
        /// Desc(modification) : some of the methods which are not required are commented 
        /// What is Done in PageLoad: We are Loading Branches,customers,salesengineers based on Roleid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {


            try
            {strUserId = Session["UserId"].ToString();
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                if (Session["ValueIn"] == null) { Session["ValueIn"] = 1000; }
                if (!IsPostBack)
                {
                    cter = null;
                    gridLoadedStatus = 0;
                   
                    roleId = Session["RoleId"].ToString();

                    if (roleId == "HO" || roleId == "TM")
                    {
                        if (roleId == "HO")
                        {
                            divGP.Visible = true;
                            if (Session["cter"] == null && roleId == "HO")
                            {
                                Session["cter"] = "TTA";
                                cter = "TTA";

                            }
                            if (Session["cter"].ToString() == "DUR")
                            {
                                rdBtnDuraCab.Checked = true;
                                rdBtnTaegutec.Checked = false;
                                cter = "DUR";
                            }
                            else
                            {
                                rdBtnTaegutec.Checked = true;
                                rdBtnDuraCab.Checked = false;
                                cter = "TTA";
                            }
                            cterDiv.Visible = true;
                        }
                        LoadBranches();
                        //  cbBranchAll_CheckedChanged(null, null);
                        ChkBranches_SelectedIndexChanged(null, null);

                    }
                    else if (Session["RoleId"].ToString() == "BM")
                    {
                        string username = Session["UserName"].ToString();
                        string branchcode = Session["BranchCode"].ToString();
                        string branchDec = Session["BranchDesc"].ToString();
                        Session["SelectedBranchList"] = "'" + branchcode + "'";
                        Session["ExportBranchList"] = branchcode;
                        ////bind branch
                        //ChkBranches.Items.Insert(0, branchcode);
                        BranchList.Items.Insert(0, branchcode);
                        divBranch.Visible = false;

                        // sales engineers loading
                        DataTable dtSalesEngDetails = objReports.LoadUserInfo(null, branchcode);
                        if (dtSalesEngDetails != null)
                        {
                            SalesEngList.DataSource = dtSalesEngDetails;
                            SalesEngList.DataTextField = "EngineerName";
                            SalesEngList.DataValueField = "EngineerId";
                            SalesEngList.DataBind();
                        }
                        //load customer details

                        //  CheckSalEngAll_CheckedChanged(null, null);
                        ChkSalesEng_SelectedIndexChanged(null, null);

                    }
                    else if (Session["RoleId"].ToString() == "SE")
                    {

                        string username = Session["UserName"].ToString();
                        string branchcode = Session["BranchCode"].ToString();
                        string branchDec = Session["BranchDesc"].ToString();
                        Session["SelectedBranchList"] = "'" + branchcode + "'";
                        Session["SelectedSalesEngineers"] = "'" + strUserId + "'";
                        //bind branch
                        BranchList.Items.Insert(0, branchcode);

                        Session["ExportBranchList"] = branchcode;
                        Session["ExportSalesEngineers"] = strUserId;
                        divBranch.Visible = false;
                        //bind Sales engineer
                        SalesEngList.Items.Insert(0, strUserId);
                        divSE.Visible = false;

                        // customers loading
                        DataTable dtCutomerDetails = objBudget.LoadCustomerDetails(strUserId, "SE"); ;
                        if (dtCutomerDetails != null)
                        {
                            DataTable dtDeatils = new DataTable();
                            dtDeatils.Columns.Add("customer_number", typeof(string));
                            dtDeatils.Columns.Add("customer_name", typeof(string));
                            for (int i = 0; i < dtCutomerDetails.Rows.Count; i++)
                            {
                                dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[1].ToString(), dtCutomerDetails.Rows[i].ItemArray[2].ToString() + "(" + dtCutomerDetails.Rows[i].ItemArray[1].ToString() + ")");
                            }
                            CustNameList.DataSource = dtDeatils;
                            CustNameList.DataTextField = "customer_name";
                            CustNameList.DataValueField = "customer_number";
                            CustNameList.DataBind();

                            CustNumList.DataSource = dtCutomerDetails;
                            CustNumList.DataTextField = "customer_number";
                            CustNumList.DataValueField = "customer_number";
                            CustNumList.DataBind();

                            foreach (System.Web.UI.WebControls.ListItem val in CustNameList.Items)
                            {
                                val.Selected = true;

                            }

                            foreach (System.Web.UI.WebControls.ListItem val in CustNumList.Items)
                            {
                                val.Selected = true;

                            }
                        }
                        


                    }

                    LoadYear();
                    LoadMonthYear();
                    LoadProductGroup();
                    LoadProductFamliy();
                    ChkCustNum_SelectedIndexChanged(null, null);
                    ChkCustName_SelectedIndexChanged(null, null);
                    ChkProductFamily_SelectedIndexChanged(null, null);
                  

                }
                ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

       
        /// <summary>
        /// Author  : Anamika
        /// Date    : April 27,2017
        /// Desc    : Select the company type and store in session
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rdBtnTaegutec_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Please click on FILTER to view results');triggerPostGridLodedActions();", true);
                if (rdBtnTaegutec.Checked)
                {
                    Session["cter"] = "TTA";
                    cter = "TTA";
                }
                if (rdBtnDuraCab.Checked)
                {
                    Session["cter"] = "DUR";
                    cter = "DUR";
                }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        /// <summary>
        /// Author  : Anamika
        /// Date    : April 27,2017
        /// Desc    : click on branches checkbox, se should be loaded accordingly and selected value should be stored in session for further use
        ///Modified by : K.LakshmiBindu
        /// Modified date ::Dec 4,2018
        /// Desc(modification) :Added code for selecting all the sales engineer list of sales engineer dropdown 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ChkBranches_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name_desc = "", name_code = "";
            int counter = 0;
           
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            try
            {
                
                
                foreach (System.Web.UI.WebControls.ListItem val in BranchList.Items)
                {
                    if (val.Selected)
                    {
                        counter++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                    }
                }
                

                name_code = "'" + name_code;
                string branchlist = name_code.Substring(0, Math.Max(0, name_code.Length - 2));

                if (BranchList.Items.Count == counter)
                {
                    //cbBranchAll.Checked = true;
                    //txtbranchlist.Text = "ALL";
                    Session["SelectedBranchList"] = "ALL";
                    Session["ExportBranchList"] = "ALL";
                }
                else
                {
                    //cbBranchAll.Checked = false;
                    //txtbranchlist.Text = name_desc;
                    Session["SelectedBranchList"] = branchlist;
                    Session["ExportBranchList"] = name_desc;
                }




                string roleId = Session["RoleId"].ToString();
                string userId = Session["UserId"].ToString();
                string branchcode = Convert.ToString(Session["SelectedBranchList"]);
                objJSR.branch = roleId == "TM" && BranchList.SelectedItem.Value == "ALL" ? userId : branchcode;
                objJSR.roleId = roleId;
                objJSR.flag = "SalesEngineer";
                DataTable dtData = objJSRDAL.getFilterAreaValue(objJSR);

                if (dtData.Rows.Count != 0)
                {
                    SalesEngList.DataSource = dtData;
                    SalesEngList.DataTextField = "EngineerName";
                    SalesEngList.DataValueField = "EngineerId";
                    SalesEngList.DataBind();
                }
                else
                {
                    SalesEngList.DataSource = dtData;
                    SalesEngList.DataTextField = "EngineerName";
                    SalesEngList.DataValueField = "EngineerId";
                    SalesEngList.DataBind();
                }

                foreach (System.Web.UI.WebControls.ListItem val in SalesEngList.Items)
                {
                    val.Selected = true;
                   
                }

                ChkSalesEng_SelectedIndexChanged(null, null);


              //  CheckSalEngAll.Checked = true;
               // CheckSalEngAll_CheckedChanged(null, null);
                ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }

    
        /// <summary>
        /// Author  : Anamika
        /// Date    : April 27,2017
        /// Desc    : click on se checkbox, customers should be loaded accordingly and selected value should be stored in session for further use
        /// Modified by : K.LakshmiBindu
        /// Modified date : Dec 4,2018
        /// Desc(modification) :Added code for selecting all the customer names and customer numbers in the dropdown list 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ChkSalesEng_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name_desc = "", name_code = "";
            int counter = 0;
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            try
            {
                if (!IsPostBack)
                {
                    foreach (System.Web.UI.WebControls.ListItem val in SalesEngList.Items)
                    {
                        val.Selected = true;
                        if (val.Selected)
                        {
                            counter++;
                            name_desc += val.Text + " , ";
                            name_code += val.Value + "','";
                        }
                    }

                }
                else
                {
                    foreach (System.Web.UI.WebControls.ListItem val in SalesEngList.Items)
                    {
                        if (val.Selected)
                        {
                            counter++;
                            name_desc += val.Text + " , ";
                            name_code += val.Value + "','";
                        }
                    }
                }

                name_code = "'" + name_code;
                string SalesengList = name_code.Substring(0, Math.Max(0, name_code.Length - 2));
                if (SalesEngList.Items.Count == counter)
                {
                  //  CheckSalEngAll.Checked = true;
                    //TxtSalesengList.Text = "ALL";
                    Session["SelectedSalesEngineers"] = "";
                    Session["ExportSalesEngineers"] = "ALL";
                }
                else
                {
                   // CheckSalEngAll.Checked = false;
                   // TxtSalesengList.Text = name_desc;
                    Session["SelectedSalesEngineers"] = SalesengList;
                    Session["ExportSalesEngineers"] = name_desc;
                }



                string roleId = Session["RoleId"].ToString();
                string userId = Session["UserId"].ToString();
                string branchcode = Convert.ToString(Session["SelectedBranchList"]);
                objJSR.branch = (roleId == "TM" && BranchList.SelectedItem.Value == "ALL") ? userId : branchcode;

                objRSum.salesengineer_id = SalesengList.ToString() == "ALL" || SalesengList.ToString() == "" ? null : SalesengList;
              //  objJSR.salesengineer_id = SalesengList.ToString() == "" ? null : SalesengList;
                objJSR.customer_type = ddlcustomertype.SelectedItem.Value;
                objJSR.roleId = roleId;
                objJSR.flag = "CustomerType";
                DataTable dtData = objJSRDAL.getFilterAreaValue(objJSR);

                if (dtData.Rows.Count != 0)
                {
                    CustNameList.DataSource = dtData;
                    CustNameList.DataTextField = "customer_short_name";
                    CustNameList.DataValueField = "customer_number";
                    CustNameList.DataBind();
                }
                else
                {
                    CustNameList.DataSource = dtData;
                    CustNameList.DataTextField = "customer_short_name";
                    CustNameList.DataValueField = "customer_number";
                    CustNameList.DataBind();
                }
                if (dtData.Rows.Count != 0)
                {
                    CustNumList.DataSource = dtData;
                    CustNumList.DataTextField = "customer_number";
                    CustNumList.DataValueField = "customer_number";
                    CustNumList.DataBind();
                }
                else
                {
                    CustNumList.DataSource = dtData;
                    CustNumList.DataTextField = "customer_number";
                    CustNumList.DataValueField = "customer_number";
                    CustNumList.DataBind();
                }

                foreach (System.Web.UI.WebControls.ListItem val in CustNameList.Items)
                {
                    val.Selected = true;

                }
                foreach (System.Web.UI.WebControls.ListItem val in CustNumList.Items)
                {
                    val.Selected = true;

                }


           //     ChkCustNumAll.Checked = true;
            //    ChkCustNumAll_CheckedChanged(null, null);
             //   ChkCustNameAll_CheckedChanged(null, null);

                ddlcustomertype_SelectedIndexChanged(null, null);
                ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        /// <summary>
        /// Author  : Anamika
        /// Date    : April 27,2017
        /// Desc    : click on customer type, customers should be loaded accordingly and selected value should be stored in session for further use
        /// Modified by : K.LakshmiBindu
        /// Modified date :  Dec 4,2018
        /// Desc(modification) :Added code for selecting all the customer numbers and customer names in dropdownlist 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlcustomertype_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            try
            {
                string roleId = Session["RoleId"].ToString();
                string userId = Session["UserId"].ToString();
                string branchcode = Convert.ToString(Session["SelectedBranchList"]);
                string SalesengList = Convert.ToString(Session["SelectedSalesEngineers"]);
                objJSR.branch = (roleId == "TM" && BranchList.SelectedItem.Value == "ALL") ? userId : branchcode;
                objJSR.salesengineer_id = SalesengList;
                objJSR.customer_type = ddlcustomertype.SelectedItem.Value;
                objJSR.roleId = roleId;
                objJSR.flag = "CustomerType";
                DataTable dtData = objJSRDAL.getFilterAreaValue(objJSR);

                if (dtData.Rows.Count != 0)
                {
                    CustNameList.DataSource = dtData;
                    CustNameList.DataTextField = "customer_short_name";
                    CustNameList.DataValueField = "customer_number";
                    CustNameList.DataBind();
                }
                else
                {
                    CustNameList.DataSource = dtData;
                    CustNameList.DataTextField = "customer_short_name";
                    CustNameList.DataValueField = "customer_number";
                    CustNameList.DataBind();
                }
                if (dtData.Rows.Count != 0)
                {
                    CustNumList.DataSource = dtData;
                    CustNumList.DataTextField = "customer_number";
                    CustNumList.DataValueField = "customer_number";
                    CustNumList.DataBind();
                }
                else
                {
                    CustNumList.DataSource = dtData;
                    CustNumList.DataTextField = "customer_number";
                    CustNumList.DataValueField = "customer_number";
                    CustNumList.DataBind();
                }

                foreach (System.Web.UI.WebControls.ListItem val in CustNameList.Items)
                {
                    val.Selected = true;

                }
                foreach (System.Web.UI.WebControls.ListItem val in CustNumList.Items)
                {
                    val.Selected = true;

                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

  

        /// <summary>
        /// Author  : Anamika
        /// Date    : April 27,2017
        /// Desc    : click on cust_name checkbox, cust_no should be checked accordingly and selected value should be stored in session for further use
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ChkCustName_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name_desc = "", name_code = "";
            string c_name_desc = "", c_name_code = "";
            int counter = 0, icounter = 0;
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            try
            {
                foreach (System.Web.UI.WebControls.ListItem val in CustNameList.Items)
                {
                    if (val.Selected)
                    {
                        counter++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                        foreach (System.Web.UI.WebControls.ListItem val1 in CustNumList.Items)
                        {
                            if (val1.Value == val.Value)
                            {
                                icounter++;
                                val1.Selected = true;
                                c_name_desc += val1.Text + " , ";
                                c_name_code += val1.Value + "','";
                            }
                        }
                    }
                    else
                    {
                        foreach (System.Web.UI.WebControls.ListItem val1 in CustNumList.Items)
                        {
                            if (val1.Value == val.Value)
                            {
                                val1.Selected = false;
                            }
                        }
                    }
                }

                name_code = "'" + name_code;
               

                c_name_code = "'" + c_name_code;
                string CustomerNumlist = c_name_code.Substring(0, Math.Max(0, c_name_code.Length - 2));
                if (CustNumList.Items.Count == icounter)
                {
                    //ChkCustNumAll.Checked = true;
                    //TxtCustomerNum.Text = "ALL";
                    Session["SelectedCustomerNumbers"] = null;
                    Session["ExportCustomers"] = "ALL";
                }
                else
                {
                    //ChkCustNumAll.Checked = false;
                    //TxtCustomerNum.Text = c_name_desc;
                    Session["SelectedCustomerNumbers"] = CustomerNumlist;
                    Session["ExportCustomers"] = c_name_desc;
                }
                string CustomerNamelist = name_code.Substring(0, Math.Max(0, name_code.Length - 2));
                Session["SelectedCustomerNames"] = CustomerNamelist;
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }

       
        /// <summary>
        /// Author  : Anamika
        /// Date    : April 27,2017
        /// Desc    : click on cust_no checkbox, cust_name should be checked accordingly and selected value should be stored in session for further use
        ///  Modified by : K.LakshmiBindu
        /// Modified date :  Dec 4,2018
        /// Desc(modification) :Commented few lines of code as it is not required 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ChkCustNum_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name_desc = "", name_code = "";
            string c_name_desc = "", c_name_code = "";
            int counter = 0, icounter = 0;
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            try
            {
                foreach (System.Web.UI.WebControls.ListItem val in CustNumList.Items)
                {
                    if (val.Selected)
                    {
                        counter++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                        foreach (System.Web.UI.WebControls.ListItem val1 in CustNameList.Items)
                        {
                            if (val1.Value == val.Value)
                            {
                                icounter++;
                                val1.Selected = true;
                                c_name_desc += val1.Text + " , ";
                                c_name_code += val1.Value + "','";

                            }
                        }
                    }
                    else
                    {
                        foreach (System.Web.UI.WebControls.ListItem val1 in CustNameList.Items)
                        {
                            if (val1.Value == val.Value)
                            {
                                val1.Selected = false;
                            }
                        }
                    }
                }

                name_code = "'" + name_code;
                string CustomerNumlist = name_code.Substring(0, Math.Max(0, name_code.Length - 2));
                //if (CustNumList.Items.Count == counter)
                //{
                //    ChkCustNumAll.Checked = true;
                //    TxtCustomerNum.Text = "ALL";
                //}
                //else
                //{
                //    ChkCustNumAll.Checked = false;
                //    TxtCustomerNum.Text = name_desc;
                //}

                c_name_code = "'" + c_name_code;
                if (CustNameList.Items.Count == icounter)
                {
                   
                    Session["SelectedCustomerNumbers"] = null;
                    Session["ExportCustomers"] = "ALL";
                }
                else
                {
                   
                    Session["SelectedCustomerNumbers"] = CustomerNumlist;
                    Session["ExportCustomers"] = c_name_desc;
                }



                string CustomerNamelist = c_name_code.Substring(0, Math.Max(0, c_name_code.Length - 2));
                Session["SelectedCustomerNames"] = CustomerNamelist;
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }


        }


        /// <summary>
        /// Author  : Anamika
        /// Date    : April 27,2017
        /// Desc    : click on group checkbox,  selected value should be stored in session for further use
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ChkProductGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            int counter = 0;
            string chakgrp_desc = string.Empty;
            string chkgrp_code = string.Empty;

            //int B_Budgetyear = Convert.ToInt32(objAdmin.GetProfile("B_BUDGET_YEAR"));
            //DataTable ProductGroups = objRSum.GetProductGroups(B_Budgetyear);

            ////DataTable selectedTable = ProductGroups.AsEnumerable()
            ////                .Where(r => r.Field<int>("year") == B_Budgetyear)
            ////                .CopyToDataTable();


            //for (int i = 0; i < ProductGroups.Rows.Count; i++)
            //{

            //    if (Convert.ToString(ProductGroups.Rows[i][1]) == "FIVE YEARS")
            //    {
            //        ProductGroups.Rows[i][1] = "5YRS";
            //    }
            //    else if (Convert.ToString(ProductGroups.Rows[i][1]) == "TEN YEARS")
            //    {
            //        ProductGroups.Rows[i][1] = "10YRS";
            //    }
            //}
            //ProductGroupList.DataSource = ProductGroups;
            //ProductGroupList.DataTextField = "splgrps";
            //ProductGroupList.DataValueField = "splgrps";
            //ProductGroupList.DataBind();
            try
            {
                foreach (System.Web.UI.WebControls.ListItem val in ProductGroupList.Items)
                {
                    if (val.Selected)
                    {
                        counter++;
                        chakgrp_desc += val.Text + " , ";
                        chkgrp_code += val.Value + "','";
                    }
                }

                chkgrp_code = "'" + chkgrp_code;
                string ProductGrpList = chkgrp_code.Substring(0, Math.Max(0, chkgrp_code.Length - 2));
                if (ProductGroupList.Items.Count == counter)
                {
                    //  ChkProductGrpAll.Checked = true;
                    //   TxtProductGrp.Text = "ALL";
                    Session["SelectedProductGroup"] = null;
                    Session["ExportProductGroup"] = "ALL";
                }
                else
                {

                    Session["SelectedProductGroup"] = ProductGrpList;
                    //    ChkProductGrpAll.Checked = false;
                    //   TxtProductGrp.Text = chakgrp_desc;
                    Session["ExportProductGroup"] = chakgrp_desc;
                }
                ChkProductFamily_SelectedIndexChanged(null, null);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

    
        /// <summary>
        /// Author  : Anamika
        /// Date    : April 27,2017
        /// Desc    : click on family checkbox, subfamily should be loaded accordingly and selected value should be stored in session for further use
        ///  Modified by : K.LakshmiBindu
        /// Modified date : Dec 4,2018
        /// Desc(modification) :Commented few lines of code as it is not required 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ChkProductFamily_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name_desc = "", name_code = "", chakgrp_desc = "", chkgrp_code = "";
            int counter = 0;
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            try
            {
                foreach (System.Web.UI.WebControls.ListItem val in ProductFamList.Items)
                {
                    if (val.Selected)
                    {
                        counter++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                    }
                }

                name_code = "'" + name_code;
                string ProductFamilyList = name_code.Substring(0, Math.Max(0, name_code.Length - 2));
                if (ProductFamList.Items.Count == counter)
                {
                  //  ChkProductFamilyAll.Checked = true;
                  //  TxtProductfamily.Text = "ALL";
                    Session["SelectedProductFamily"] = "";
                    Session["ExportFamily"] = "ALL";
                }
                else
                {
                 //   ChkProductFamilyAll.Checked = false;
                 //   TxtProductfamily.Text = name_desc;
                    Session["SelectedProductFamily"] = ProductFamilyList;
                    Session["ExportFamily"] = name_desc;
                }





                counter = 0;
                foreach (System.Web.UI.WebControls.ListItem val in ProductGroupList.Items)
                {
                    if (val.Selected)
                    {
                        counter++;
                        chakgrp_desc += val.Text + " , ";
                        chkgrp_code += val.Value + "','";
                    }
                }

                chkgrp_code = "'" + chkgrp_code;
                string ProductGrpList = chkgrp_code.Substring(0, Math.Max(0, chkgrp_code.Length - 2));
                if (ProductGroupList.Items.Count == counter)
                {
                    //ChkProductGrpAll.Checked = true;
                   // TxtProductGrp.Text = "ALL";
                    Session["SelectedProductGroup"] = null;
                    Session["ExportProductGroup"] = "ALL";
                }
                else
                {

                    Session["SelectedProductGroup"] = ProductGrpList;
                 //   ChkProductGrpAll.Checked = false;
                 //   TxtProductGrp.Text = chakgrp_desc;
                    Session["ExportProductGroup"] = chakgrp_desc;
                }





                string ProductGroup = Convert.ToString(Session["SelectedProductGroup"]);
                string ProductFamily = Convert.ToString(Session["SelectedProductFamily"]);


                string famId = ProductFamList.SelectedItem.Value == "ALL" || ProductFamList.SelectedItem.Value == null ? "0" : ProductFamily;
                DataTable dtPL = new DataTable();
                dtPL = objJSRDAL.getSubFamily(famId);
                if (dtPL.Rows.Count > 0)
                {
                    ProdSubFamilyList.DataSource = dtPL;
                    ProdSubFamilyList.DataValueField = "item_sub_family_id";
                    ProdSubFamilyList.DataTextField = "item_sub_family_name";
                    ProdSubFamilyList.DataBind();
                }
              //  ChkSubFamilyAll.Checked = true;
             //   ChkSubFamilyAll_CheckedChanged(null, null);
                foreach (System.Web.UI.WebControls.ListItem val in ProdSubFamilyList.Items)
                {
                    val.Selected = true;
                }
                ChkSubFamily_SelectedIndexChanged(null, null);
                ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }

  
        /// <summary>
        /// Author  : Anamika
        /// Date    : April 27,2017
        /// Desc    : click on Subfamily checkbox, applications should be loaded accordingly and selected value should be stored in session for further use
        /// Modified by : K.LakshmiBindu
        /// Modified date : Dec 4,2018
        /// Desc(modification) :Commented few lines of code  as it is not required  and added code for selecting all the list items of application dropdown
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ChkSubFamily_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name_desc = "", name_code = "", chakgrp_desc = "", chkgrp_code = "";
            int counter = 0;

            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            try
            {
                foreach (System.Web.UI.WebControls.ListItem val in ProdSubFamilyList.Items)
                {
                    if (val.Selected)
                    {
                        counter++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                    }
                }

                name_code = "'" + name_code;
                string SubFamilylist = name_code.Substring(0, Math.Max(0, name_code.Length - 2));
                if (counter == ProdSubFamilyList.Items.Count)
                {
                    //ChkSubFamilyAll.Checked = true;
                  //  txtSubFamily.Text = "ALL";
                    Session["SelectedSubFamily"] = "";
                    Session["ExportSubFamily"] = "ALL";
                }
                else
                {
                   // ChkSubFamilyAll.Checked = false;
                  //  txtSubFamily.Text = name_desc;
                    Session["SelectedSubFamily"] = SubFamilylist;
                    Session["ExportSubFamily"] = name_desc;
                }




                name_code = string.Empty;
                counter = 0;
                foreach (System.Web.UI.WebControls.ListItem val in ProductGroupList.Items)
                {
                    if (val.Selected)
                    {
                        counter++;
                        chakgrp_desc += val.Text + " , ";
                        chkgrp_code += val.Value + "','";
                    }
                }

                chkgrp_code = "'" + chkgrp_code;
                string ProductGrpList = chkgrp_code.Substring(0, Math.Max(0, chkgrp_code.Length - 2));
                if (counter == ProductGroupList.Items.Count)
                {
                   // TxtProductGrp.Text = "ALL";
                   // ChkProductGrpAll.Checked = true;
                    Session["SelectedProductGroup"] = null;
                    Session["ExportProductGroup"] = "ALL";
                }
                else
                {
                  //  TxtProductGrp.Text = chakgrp_desc;
                    Session["SelectedProductGroup"] = ProductGrpList;
                    Session["ExportProductGroup"] = chakgrp_desc;
                }
                DataTable dtApp = new DataTable();
                dtApp = objJSRDAL.getProducts(SubFamilylist, ProductGrpList);
                DataTable dtTemp = dtApp.Clone();
                for (int i = 0; i < dtApp.Rows.Count; i++)
                {
                    dtTemp.Rows.Add(dtApp.Rows[i].ItemArray[0], dtApp.Rows[i].ItemArray[0].ToString() + "_" + dtApp.Rows[i].ItemArray[2].ToString(), dtApp.Rows[i].ItemArray[2]);
                }
                if (dtTemp.Rows.Count != 0)
                {
                    AppList.DataSource = dtTemp;
                    AppList.DataValueField = "item_code";
                    AppList.DataTextField = "item_short_name";
                    AppList.DataBind();
                }
                else
                {

                    AppList.DataSource = dtTemp;
                    AppList.DataValueField = "item_code";
                    AppList.DataTextField = "item_short_name";
                    AppList.DataBind();
                }

                foreach (System.Web.UI.WebControls.ListItem val in AppList.Items)
                      {
                           val.Selected = true;
                      }
                ChkApplicationList_SelectedIndexChanged(null, null);
              //  ChkAppAll.Checked = true;
            //   ChkAppAll_CheckedChanged(null, null);
                ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }

      

        /// <summary>
        /// Author  : Anamika
        /// Date    : April 27,2017
        /// Desc    : click on application checkbox, selected value should be stored in session for further use
        ///  Modified by : K.LakshmiBindu
        /// Modified date :  Dec 4,2018
        /// Desc(modification) :Commented few lines of code  as it is not required 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ChkApplicationList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name_desc = "", name_code = "";
            int counter = 0;

            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            try
            {
                foreach (System.Web.UI.WebControls.ListItem val in AppList.Items)
                {
                    if (val.Selected)
                    {
                        counter++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                    }
                }

                name_code = "'" + name_code;
                string ApplicationList = name_code.Substring(0, Math.Max(0, name_code.Length - 2));
                if (counter == AppList.Items.Count)
                {
                   // ChkAppAll.Checked = true;
                   // TxtApplication.Text = "ALL";
                    Session["SelectedApplications"] = "";
                    Session["ExportApp"] = "ALL";
                }
                else
                {
                  //  ChkAppAll.Checked = false;
                  //  TxtApplication.Text = name_desc;
                    Session["SelectedApplications"] = ApplicationList;
                    Session["ExportApp"] = name_desc;
                }



            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }

        /// <summary>
        /// Author : Anamika
        /// Date : May 1, 2017
        /// Desc : Fetch report from database using all the filters
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void reports_Click(object sender, EventArgs e)
        {
            DataTable dtReport = new DataTable();
            try
            {
                objJSR.jsr_month = Convert.ToInt32(ddlMonth.SelectedValue);
                objJSR.jsr_year = Convert.ToInt32(ddlYear.SelectedValue);
                objJSR.branch = Convert.ToString(Session["SelectedBranchList"]);
                objJSR.salesengineer_id = Convert.ToString(Session["SelectedSalesEngineers"]);
                objJSR.c_num = Convert.ToString(Session["SelectedCustomerNumbers"]);
                Session["ddlcustomertype.SelectedItem.Value"] = Convert.ToString(ddlcustomertype.SelectedItem.Value);
                objJSR.customer_type = Convert.ToString(Session["ddlcustomertype.SelectedItem.Value"]);
                objJSR.family = Convert.ToString(Session["SelectedProductFamily"]);
                objJSR.subfamily = Convert.ToString(Session["SelectedSubFamily"]);
                objJSR.application = Convert.ToString(Session["SelectedApplications"]);
                objJSR.p_group = Convert.ToString(Session["SelectedProductGroup"]);
                objJSR.fiterby = "APPLICATION";
                objJSR.cter = Convert.ToString(Session["cter"]);
                objJSR.valueIn = Convert.ToInt32(Session["ValueIn"]);
                objJSR.roleId = Convert.ToString(Session["RoleId"]);
                LoadCSS();
                dtReport = objJSRDAL.getReport(objJSR);
                if (dtReport.Rows.Count > 0)
                {
                    expand.Visible = true;
                    exportexcel.Visible = true;
                    exportfamily.Visible = true;
                    collapse.Visible = true;
                    if (objJSR.valueIn == 100000)
                        lblResult.Text = "All vlaues are in lakhs.";
                    else
                        lblResult.Text = "All vlaues are in thousands.";
                    grdviewAllValues.DataSource = dtReport;
                    grdviewAllValues.DataBind();
                    bindgridColor();
                    Session["dtApplication"] = dtReport;
                    ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                }
                else
                {
                    lblResult.Text = "No data to display.";
                    grdviewAllValues.DataSource = null;
                    grdviewAllValues.DataBind();
                    gridLoadedStatus = 0;
                    Session["dtApplication"] = null;
                }

            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        /// <summary>
        /// Author : Anamika
        /// Date : May 2, 2017
        /// Desc : For every family, sub family and application, there is a link provided to open a new window for jsr report by customer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void grdviewAllValues_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                string flag = Convert.ToString(chkGP.Checked);

                if (e.CommandName == "FamilyId")
                {
                    LinkButton lnkView = (LinkButton)e.CommandSource;
                    objJSR.jsr_month = Convert.ToInt32(ddlMonth.SelectedValue);
                    objJSR.jsr_year = Convert.ToInt32(ddlYear.SelectedValue);
                    objJSR.family = Convert.ToString(lnkView.CommandArgument);
                    ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "OpenWindow", "window.open('JSRReportByApplicationCustomer.aspx?month=" + objJSR.jsr_month + "&year=" + objJSR.jsr_year + "&family=" + objJSR.family + "&flag=" + flag + "','_newtab');", true);
                }
                else if (e.CommandName == "SubFamilyId")
                {
                    LinkButton lnkView = (LinkButton)e.CommandSource;
                    string dealId = lnkView.CommandArgument;
                    objJSR.jsr_month = Convert.ToInt32(ddlMonth.SelectedValue);
                    objJSR.jsr_year = Convert.ToInt32(ddlYear.SelectedValue);
                    objJSR.subfamily = Convert.ToString(lnkView.CommandArgument);

                    //ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "OpenWindow", "window.open('JSRReportByApplicationCustomer.aspx','_newtab');", true);
                    ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "OpenWindow", "window.open('JSRReportByApplicationCustomer.aspx?month=" + objJSR.jsr_month + "&year=" + objJSR.jsr_year + "&subfamily=" + objJSR.subfamily + "&flag=" + flag + "','_newtab');", true);
                }
                else if (e.CommandName == "ApplicationCode")
                {
                    LinkButton lnkView = (LinkButton)e.CommandSource;
                    objJSR.jsr_month = Convert.ToInt32(ddlMonth.SelectedValue);
                    objJSR.jsr_year = Convert.ToInt32(ddlYear.SelectedValue);
                    objJSR.application = Convert.ToString(lnkView.CommandArgument);
                    //ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "OpenWindow", "window.open('JSRReportByApplicationCustomer.aspx?month=" + objJSR.jsr_month + "&year=" + objJSR.jsr_year + "&subfamily=" + objJSR.subfamily + "','_newtab');", true);
                    ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "OpenWindow", "window.open('JSRReportByApplicationCustomer.aspx?month=" + objJSR.jsr_month + "&year=" + objJSR.jsr_year + "&appcode=" + objJSR.application + "&flag=" + flag + "','_newtab');", true);
                }
                ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        /// <summary>
        /// Author : Anamika
        /// Date : May 8, 2017
        /// Desc : Export in Excel
        /// ModifiedBy:K.LakshmiBindu
        /// Date:12,Dec 2018
        /// ModifiedDesc: Adding product groups columns to excel  based on selection in admin config page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void exportexcel_Click(object sender, EventArgs e)
        {
            List<string> PgList = null;
            string Flag = null;
            int count = 1;
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                reports_Click(null, null);
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "JSRReportByApplication_Family.xls"));
                Response.ContentType = "application/ms-excel";
                DataTable dtExcel = new DataTable();
              
                dtExcel = (DataTable)Session["dtApplication"];
                dtExcel.Columns["TEN_YEARS_FLAG"].SetOrdinal(12);
                DataTable dtFamilyExport;
                dtFamilyExport = dtExcel.Clone();
                DataRow drFirst = dtExcel.Rows[0];
                dtFamilyExport.Rows.Add(drFirst.ItemArray);
                DataRow[] dr = dtExcel.Select("sumFlag ='FamilySum'");
                foreach (DataRow dr1 in dr)
                {
                    dtFamilyExport.Rows.Add(dr1.ItemArray);
                }
                DataTable dtExport;
                dtExport = dtFamilyExport.Copy();

                //DataTable dtExport;
                //dtExport = dtExcel.Copy();

                dtExport.Columns.Remove("ID");
                dtExport.Columns.Remove("FAMILY_ID");
                dtExport.Columns.Remove("SUBFAMILY_ID");
                dtExport.Columns.Remove("I_T_O_FLAG");
                dtExport.Columns.Remove("sumFlag");
                dtExport.Columns.Remove("DISPLAY_VALUE");
                dtExport.Columns.Remove("Line_desc");
                if (Session["RoleId"].ToString() != "HO" || chkGP.Checked == false)
                {
                    dtExport.Columns.Remove("GP_MONTHLY");
                    dtExport.Columns.Remove("GP_YEARLY");
                    dtExport.Columns.Remove("SUBFAMILY_NAME");
                    dtExport.Columns.Remove("APPLICATION_CODE");
                }
                dtExport.Columns["TEN_YEARS_FLAG"].SetOrdinal(6);
                dtExport.Columns.Remove("GOLD_FLAG");
                dtExport.Columns.Remove("TOP_FLAG");
                dtExport.Columns.Remove("TEN_YEARS_FLAG");
                dtExport.Columns.Remove("FIVE_YEARS_FLAG");
                dtExport.Columns.Remove("BB_FLAG");
                dtExport.Columns.Remove("SPC_FLAG");

                PgList = Session["PgList"] as System.Collections.Generic.List<string>;
                for (int i = 0; i < PgList.Count; i++)
                {
                    count = count + 1;
                    int rowIdx = 0;
                    switch (PgList[i])
                    {
                        case "GOLD":
                            DataColumn dg = dtExport.Columns.Add("GOLD_FLAG");
                            dg.SetOrdinal(count);
                            dtExport.AsEnumerable().All(row => { row["GOLD_FLAG"] = dtExcel.Rows[rowIdx++]["GOLD_FLAG"]; return true; });
                            break;
                        case "TOP":
                            DataColumn dt = dtExport.Columns.Add("TOP_FLAG");
                            dt.SetOrdinal(count);
                            dtExport.AsEnumerable().All(row => { row["TOP_FLAG"] = dtExcel.Rows[rowIdx++]["TOP_FLAG"]; return true; });
                            break;
                        case "BB":
                            DataColumn dbb = dtExport.Columns.Add("BB_FLAG");
                            dbb.SetOrdinal(count);
                            dtExport.AsEnumerable().All(row => { row["BB_FLAG"] = dtExcel.Rows[rowIdx++]["BB_FLAG"]; return true; });
                            break;
                        case "FIVE YEARS":
                            DataColumn df = dtExport.Columns.Add("FIVE_YEARS_FLAG");
                            df.SetOrdinal(count);
                            dtExport.AsEnumerable().All(row => { row["FIVE_YEARS_FLAG"] = dtExcel.Rows[rowIdx++]["FIVE_YEARS_FLAG"]; return true; });
                            break;
                        case "TEN YEARS":
                            DataColumn dten = dtExport.Columns.Add("TEN_YEARS_FLAG");
                            dten.SetOrdinal(count);
                            dtExport.AsEnumerable().All(row => { row["TEN_YEARS_FLAG"] = dtExcel.Rows[rowIdx++]["TEN_YEARS_FLAG"]; return true; });
                            break;
                        case "SPC":
                            DataColumn dtspc = dtExport.Columns.Add("SPC_FLAG");
                            dtspc.SetOrdinal(count);
                            dtExport.AsEnumerable().All(row => { row["SPC_FLAG"] = dtExcel.Rows[rowIdx++]["SPC_FLAG"]; return true; });
                            break;
                    }
                }

                GridView grdExportExcel = new GridView();
                grdExportExcel.DataSource = dtExport;
                grdExportExcel.DataBind();
               
                ur = Convert.ToString(Session["ExportSalesEngineers"]);
                using (StringWriter sw = new StringWriter())
                {
                    using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                    {
                        //  Create a table to contain the grid
                        Table table = new Table();
                        table.GridLines = grdExportExcel.GridLines;

                        foreach (GridViewRow row in grdExportExcel.Rows)
                        {
                            table.Rows.Add(row);
                        }


                        table.Rows[0].Height = 30;
                        table.Rows[0].BackColor = Color.LightSeaGreen;
                        table.Rows[0].Font.Bold = true;

                        sw.WriteLine("<table><tr><td></td><td></td><td></td><td colspan=3 style='font-weight: bold; font-size:20px'>JSR Report By Application Summary</td></table>");
                        sw.WriteLine("<table style='margin-left: 200px;'>");

                        //  render the table into the htmlwriter
                        if (Session["RoleId"].ToString() == "HO")
                        {
                            string territory;
                            if (rdBtnTaegutec.Checked)
                            {
                                territory = "TAEGUTEC";
                            }
                            else
                            {
                                territory = "DURACARB";
                            }
                            sw.WriteLine("<tr><td></td><td></td><td></td><td style='font-weight: bold;'>TERRITORY :" + "</td><td colspan=8 style='font-style: italic;'>" + territory + "</td></tr>");
                        }
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>MONTH :" + "</td><td colspan=8 style='font-style: italic; text-align: left;'>" + Convert.ToString(ddlMonth.SelectedItem) + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>YEAR :" + "</td><td colspan=8 style='font-style: italic; text-align: left;'>" + Convert.ToString(ddlYear.SelectedValue) + "</td></tr>");
                       
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>BRANCH :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(Session["ExportBranchList"]) + "</td></tr>");
                        if (roleId == "SE")
                        {
                            sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>SALES ENGINEERS :" + "</td><td colspan=8 style='font-style: italic; text-align: left;'>" + strUserId + "</td></tr>");
                        }
                        else
                        {
                            sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>SALES ENGINEERS :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(Session["ExportSalesEngineers"]) + "</td></tr>");
                        }
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>CUSTOMER TYPE : " + "</td><td colspan=8 style='font-style: italic;'>" + Convert.ToString(ddlcustomertype.SelectedValue) + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>CUSTOMER NAME :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(Session["ExportCustomers"]) + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>PRODUCT GROUP :" + "</td><td colspan=8 style='font-style: italic;'>" + Convert.ToString(Session["ExportProductGroup"]) + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>PRODUCT FAMILY :" + "</td><td colspan=8 style='font-style: italic;'>" + Convert.ToString(Session["ExportFamily"]) + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>PRODUCT SUB-FAMILY :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(Session["ExportSubFamily"]) + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>APPLICATION :" + "</td><td colspan=8 style='font-style: italic;'>" + Convert.ToString(Session["ExportApp"]) + "</td></tr>");
                        sw.WriteLine("</table><br/>");


                        //  render the table into the htmlwriter
                        //if (Session["RoleId"].ToString() == "HO")
                        //{
                        //    string territory;
                        //    if (rdBtnTaegutec.Checked)
                        //    {
                        //        territory = "TAEGUTEC";
                        //    }
                        //    else
                        //    {
                        //        territory = "DURACARB";
                        //    }
                        //    sw.WriteLine("TERRITORY :" + "" + territory + "<br/>");
                        //}
                        //sw.WriteLine("MONTH :" + "" + Convert.ToString(ddlMonth.SelectedValue) + "<br/>");
                        //sw.WriteLine("YEAR :" + "" + Convert.ToString(ddlYear.SelectedValue) + "<br/>");
                        //sw.WriteLine("BRANCH :" + "" + Convert.ToString(Session["ExportBranchList"]) + "<br/>");
                        //sw.WriteLine("SALES ENGINEERS :" + "" + Convert.ToString(Session["ExportSalesEngineers"]) + "<br/>");
                        //sw.WriteLine("CUSTOMER TYPE : " + "" + Convert.ToString(ddlcustomertype.SelectedValue) + "<br/>");
                        //sw.WriteLine("CUSTOMER NAME :" + "" + Convert.ToString(Session["ExportCustomers"]) + "<br/>");
                        //sw.WriteLine("PRODUCT GROUP :" + "" + Convert.ToString(Session["ExportProductGroup"]) + "<br/>" + "<br/>");
                        //sw.WriteLine("PRODUCT FAMILY :" + "" + Convert.ToString(Session["ExportFamily"]) + "<br/>" + "<br/>");
                        //sw.WriteLine("PRODUCT SUB-FAMILY :" + "" + Convert.ToString(Session["ExportSubFamily"]) + "<br/>" + "<br/>");
                        //sw.WriteLine("APPLICATION :" + "" + Convert.ToString(Session["ExportApp"]) + "<br/>" + "<br/>");

                        table.RenderControl(htw);
                    }

                    Response.Write(sw.ToString());
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

                    Response.End();
                }


            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }

        }

        /// <summary>
        /// Author : Anamika
        /// Date : May 8, 2017
        /// Desc : Export in PDF
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void exportpdf_Click(object sender, EventArgs e)
        {
            try
            {
                GridView gvExportPDF = new GridView();
                reports_Click(null, null);
                DataTable dtTotal = new DataTable();
                dtTotal = (DataTable)Session["Total"];
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=JSRReportByApplication.pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);
                DataTable dtPDF = new DataTable();
                dtPDF = (DataTable)Session["dtApplication"];
                DataTable dtExport;
                dtExport = dtPDF.Copy();
                dtExport.Columns.Remove("ID");
                dtExport.Columns.Remove("FAMILY_ID");
                dtExport.Columns.Remove("SUBFAMILY_ID");
                dtExport.Columns.Remove("I_T_O_FLAG");
                dtExport.Columns.Remove("sumFlag");
                dtExport.Columns.Remove("DISPLAY_VALUE");
                dtExport.Columns.Remove("Line_desc");
                if (Session["RoleId"].ToString() != "HO")
                {
                    dtExport.Columns.Remove("GP_MONTHLY");
                    dtExport.Columns.Remove("GP_YEARLY");
                }
                if (Session["RoleId"].ToString() == "HO")
                {
                    string territory;
                    if (rdBtnTaegutec.Checked)
                    {
                        territory = "TAEGUTEC";
                    }
                    else
                    {
                        territory = "DURACARB";
                    }
                    sw.WriteLine("TERRITORY :" + "" + territory + "<br/>");
                }
                sw.WriteLine("MONTH :" + "" + Convert.ToString(ddlMonth.SelectedValue) + "<br/>");
                sw.WriteLine("YEAR :" + "" + Convert.ToString(ddlYear.SelectedValue) + "<br/>");
                sw.WriteLine("BRANCH :" + "" + Convert.ToString(Session["ExportBranchList"]) + "<br/>");
                sw.WriteLine("SALES ENGINEERS :" + "" + Convert.ToString(Session["ExportSalesEngineers"]) + "<br/>");
                sw.WriteLine("CUSTOMER TYPE : " + "" + Convert.ToString(ddlcustomertype.SelectedValue) + "<br/>");
                sw.WriteLine("CUSTOMER NAME :" + "" + Convert.ToString(Session["ExportCustomers"]) + "<br/>");
                sw.WriteLine("PRODUCT GROUP :" + "" + Convert.ToString(Session["ExportProductGroup"]) + "<br/>" + "<br/>");
                sw.WriteLine("PRODUCT FAMILY :" + "" + Convert.ToString(Session["ExportFamily"]) + "<br/>" + "<br/>");
                sw.WriteLine("PRODUCT SUB-FAMILY :" + "" + Convert.ToString(Session["ExportSubFamily"]) + "<br/>" + "<br/>");
                sw.WriteLine("APPLICATION :" + "" + Convert.ToString(Session["ExportApp"]) + "<br/>" + "<br/>");


                gvExportPDF.DataSource = dtExport;
                gvExportPDF.DataBind();


                gvExportPDF.RenderControl(hw);
                StringReader sr = new StringReader(sw.ToString());
                Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
                HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                pdfDoc.Open();
                htmlparser.Parse(sr);

                pdfDoc.Close();
                Response.Write(pdfDoc);
                //HttpContext.Current.Response.End();
                Response.End();
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        /// <summary>
        /// Author : Anamika 
        /// Date : May 17, 2017
        /// Desc : clearing all the data from the page
        ///  Modified by : K.LakshmiBindu
        /// Modified date :  Dec 4,2018
        /// Desc(modification) :Commented few lines of code as it is not required 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                LoadBranches();
                LoadProductFamliy();
                ddlMonth.SelectedIndex = 0;
                ddlYear.SelectedIndex = 0;
                ddlcustomertype.SelectedIndex = 0;
                LoadBranches();

                ChkBranches_SelectedIndexChanged(null, null);
                ChkSalesEng_SelectedIndexChanged(null, null);
                LoadProductGroup();
                LoadProductFamliy();
                ChkProductFamily_SelectedIndexChanged(null, null);
                ChkCustName_SelectedIndexChanged(null, null);
                ChkCustNum_SelectedIndexChanged(null, null);
                // cbBranchAll.Checked = true;
                // cbBranchAll_CheckedChanged(null, null);
                // CheckSalEngAll.Checked = true;
                //   CheckSalEngAll_CheckedChanged(null, null);
                //   ChkCustNameAll.Checked = true;
                //      ChkCustNameAll_CheckedChanged(null, null);
                //   ChkCustNumAll.Checked = true;
                //      ChkCustNumAll_CheckedChanged(null, null);
                //    ChkProductGrpAll.Checked = true;
                //      ChkProductGrpAll_CheckedChanged(null, null);
                //ChkProductFamilyAll.Checked = true;
                //        ChkProductFamilyAll_CheckedChanged(null, null);
                //ChkSubFamilyAll.Checked = true;
                //       ChkSubFamilyAll_CheckedChanged(null, null);
                //   ChkAppAll.Checked = true;
                //      ChkAppAll_CheckedChanged(null, null);





                expand.Visible = false;
                exportexcel.Visible = false;
                exportfamily.Visible = false;
                collapse.Visible = false;
                lblResult.Text = "";
                grdviewAllValues.DataSource = null;
                grdviewAllValues.DataBind();
                gridLoadedStatus = 0;
                Session["dtApplication"] = null;
                LoadMonthYear();
                chkGP.Checked = false;
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        /// <summary>
        /// Author : Anamika 
        /// Date : May 16, 2017
        /// Desc : Clicking on "000" value, all the value will be displayed in thousands
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Thousand_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (gridLoadedStatus == 1)
                {
                    Session["ValueIn"] = 1000;
                    reports_Click(null, null);
                }
                ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        /// <summary>
        /// Author : Anamika 
        /// Date : May 16, 2017
        /// Desc : Clicking on "lakh" value, all the value will be displayed in lakhs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Lakhs_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if (gridLoadedStatus == 1)
                {
                    Session["ValueIn"] = 100000;
                    reports_Click(null, null);
                }
                ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }

        /// <summary>
        /// Author : Anamika
        /// Date : July 10, 2017
        /// Desc : Export in Excel family wise
        /// ModifiedBy:K.LakshmiBindu
        /// Date:12,Dec 2018
        /// ModifiedDesc: Adding product groups columns to excel  based on selection in admin config page
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void exportfamily_Click(object sender, EventArgs e)
        {
            System.Collections.Generic.List<string> PgList = null;
            int count = 3;
          
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                reports_Click(null, null);
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "JSRReportByApplication.xls"));
                Response.ContentType = "application/ms-excel";
                DataTable dtExcel = new DataTable();
                dtExcel = (DataTable)Session["dtApplication"];
                //DataTable dtFamilyExport;
                //dtFamilyExport = dtExcel.Clone();
                //DataRow drFirst = dtExcel.Rows[0];
                //dtFamilyExport.Rows.Add(drFirst.ItemArray);
                //DataRow[] dr = dtExcel.Select("sumFlag ='FamilySum'");
                //foreach (DataRow dr1 in dr)
                //{
                //    dtFamilyExport.Rows.Add(dr1.ItemArray);
                //}
                DataTable dtExport;
                dtExport = dtExcel.Copy();
                dtExport.Columns.Remove("ID");
                dtExport.Columns.Remove("FAMILY_ID");
                dtExport.Columns.Remove("SUBFAMILY_ID");
                dtExport.Columns.Remove("I_T_O_FLAG");
                dtExport.Columns.Remove("sumFlag");
                dtExport.Columns.Remove("DISPLAY_VALUE");
                dtExport.Columns.Remove("Line_Desc");
                if (Session["RoleId"].ToString() != "HO" || chkGP.Checked == false)
                {
                    dtExport.Columns.Remove("GP_MONTHLY");
                    dtExport.Columns.Remove("GP_YEARLY");
                }

                dtExport.Columns["TEN_YEARS_FLAG"].SetOrdinal(6);
                dtExport.Columns.Remove("GOLD_FLAG");
                dtExport.Columns.Remove("TOP_FLAG");
                dtExport.Columns.Remove("TEN_YEARS_FLAG");
                dtExport.Columns.Remove("FIVE_YEARS_FLAG");
                dtExport.Columns.Remove("BB_FLAG");
                dtExport.Columns.Remove("SPC_FLAG");
                
                PgList = Session["PgList"] as System.Collections.Generic.List<string>;
                for (int i = 0; i < PgList.Count; i++)
                {
                    count=count+1;
                    int rowIdx = 0;
                    switch (PgList[i])
                    {
                        case "GOLD":
                             DataColumn dg=dtExport.Columns.Add("GOLD_FLAG");
                               dg.SetOrdinal(count);
                               dtExport.AsEnumerable().All(row => { row["GOLD_FLAG"] = dtExcel.Rows[rowIdx++]["GOLD_FLAG"]; return true; });
                            break;
                        case "TOP":
                            DataColumn dt=dtExport.Columns.Add("TOP_FLAG");
                dt.SetOrdinal(count);
              dtExport.AsEnumerable().All(row => { row["TOP_FLAG"] = dtExcel.Rows[rowIdx++]["TOP_FLAG"]; return true; });
                            break;
                        case "BB":
                             DataColumn dbb= dtExport.Columns.Add("BB_FLAG");
                               dbb.SetOrdinal(count);
                               dtExport.AsEnumerable().All(row => { row["BB_FLAG"] = dtExcel.Rows[rowIdx++]["BB_FLAG"]; return true; });
                            break;
                        case "FIVE YEARS":
                                DataColumn df= dtExport.Columns.Add("FIVE_YEARS_FLAG");
                             df.SetOrdinal(count);
                             dtExport.AsEnumerable().All(row => { row["FIVE_YEARS_FLAG"] = dtExcel.Rows[rowIdx++]["FIVE_YEARS_FLAG"]; return true; });
                            break;
                        case "TEN YEARS":
                            DataColumn dten= dtExport.Columns.Add("TEN_YEARS_FLAG");
                            dten.SetOrdinal(count);
                            dtExport.AsEnumerable().All(row => { row["TEN_YEARS_FLAG"] = dtExcel.Rows[rowIdx++]["TEN_YEARS_FLAG"]; return true; });
                            break;
                        case "SPC":
                                DataColumn dtspc=dtExport.Columns.Add("SPC_FLAG");
                                dtspc.SetOrdinal(count);
                                dtExport.AsEnumerable().All(row => { row["SPC_FLAG"] = dtExcel.Rows[rowIdx++]["SPC_FLAG"]; return true; });
                            break;
                    }
                }


                GridView grdExportExcel = new GridView();
                grdExportExcel.DataSource = dtExport;
                grdExportExcel.DataBind();

                using (StringWriter sw = new StringWriter())
                {
                    using (HtmlTextWriter htw = new HtmlTextWriter(sw))
                    {
                        //  Create a table to contain the grid
                        Table table = new Table();
                        table.GridLines = grdExportExcel.GridLines;

                        foreach (GridViewRow row in grdExportExcel.Rows)
                        {
                            row.Attributes.Add("font-weight", "bold");
                            table.Rows.Add(row);
                        }
                        table.Rows[0].Height = 30;
                        table.Rows[0].BackColor = Color.LightSeaGreen;
                        table.Rows[0].Font.Bold = true;
                        
                        for (int i = 0; i < table.Rows.Count; i++)
                        {
                            if (Convert.ToString(table.Rows[i].Cells[3].Text).Contains("CUTTING TOOLS TOTAL"))
                            {
                                table.Rows[i].BackColor = Color.LightCyan;
                                table.Rows[i].Font.Bold = true;
                            }
                            else if (Convert.ToString(table.Rows[i].Cells[3].Text).Contains("TOTAL"))
                            {
                                if (Convert.ToString(table.Rows[i].Cells[3].Text).Contains("INSERTS") || Convert.ToString(table.Rows[i].Cells[3].Text).Contains("TOOLS"))
                                {
                                    table.Rows[i].BackColor = Color.LightGray;
                                    table.Rows[i].Font.Bold = true;
                                }
                                else if (Convert.ToString(table.Rows[i].Cells[3].Text).Contains("FAMILY"))
                                {
                                    table.Rows[i].BackColor = Color.Black;
                                    table.Rows[i].ForeColor = Color.White;
                                    table.Rows[i].Font.Bold = true;
                                }
                                else
                                {
                                    table.Rows[i].BackColor = Color.Gray;
                                    // table.Rows[i].ForeColor = Color.White;
                                    table.Rows[i].Font.Bold = true;
                                }
                            }
                        }
                        sw.WriteLine("<table><tr><td></td><td></td><td></td><td colspan=3 style='font-weight: bold; font-size:20px; '>JSR Report By Application</td></table>");
                        sw.WriteLine("<table style='margin-left: 200px;'>");

                        //  render the table into the htmlwriter
                        if (Session["RoleId"].ToString() == "HO")
                        {
                            string territory;
                            if (rdBtnTaegutec.Checked)
                            {
                                territory = "TAEGUTEC";
                            }
                            else
                            {
                                territory = "DURACARB";
                            }
                            sw.WriteLine("<tr><td></td><td></td><td></td><td style='font-weight: bold;'>TERRITORY :" + "</td><td colspan=8 style='font-style: italic;'>" + territory + "</td></tr>");
                        }
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>MONTH :" + "</td><td colspan=8 style='font-style: italic; text-align: left;'>" + Convert.ToString(ddlMonth.SelectedItem) + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>YEAR :" + "</td><td colspan=8 style='font-style: italic; text-align: left;'>" + Convert.ToString(ddlYear.SelectedValue) + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>BRANCH :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(Session["ExportBranchList"]) + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>SALES ENGINEERS :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(Session["ExportSalesEngineers"]) + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>CUSTOMER TYPE : " + "</td><td colspan=8 style='font-style: italic;'>" + Convert.ToString(ddlcustomertype.SelectedValue) + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>CUSTOMER NAME :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(Session["ExportCustomers"]) + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>PRODUCT GROUP :" + "</td><td colspan=8 style='font-style: italic;'>" + Convert.ToString(Session["ExportProductGroup"]) + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>PRODUCT FAMILY :" + "</td><td colspan=8 style='font-style: italic;'>" + Convert.ToString(Session["ExportFamily"]) + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>PRODUCT SUB-FAMILY :" + "</td><td colspan=8 style='font-style: italic; '>" + Convert.ToString(Session["ExportSubFamily"]) + "</td></tr>");
                        sw.WriteLine("<tr><td></td><td></td><td></td><td border='1px' style='font-weight: bold;'>APPLICATION :" + "</td><td colspan=8 style='font-style: italic;'>" + Convert.ToString(Session["ExportApp"]) + "</td></tr>");
                        sw.WriteLine("</table><br/>");
                        table.RenderControl(htw);
                    }

                    Response.Write(sw.ToString());
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

                    Response.End();
                    //HttpContext.Current.ApplicationInstance.CompleteRequest();
                }


            }
            catch (Exception ex)
            {
                if (ex.Message.StartsWith("Thread") == false)
                {
                    //Response.Redirect("~/Error.aspx?ErrorMsg = " + ex.Message);
                }
                CommonFunctions.LogErrorStatic(ex);
            }

        }
        #endregion

        #region Methods
        /// <summary>
        /// Author : K.LakshmiBindu
        /// Created date : Dec 4,2018
        /// Description :For Loading special groups into dropdown 
        /// </summary>
        protected void LoadProductGroup()
        {
            string name_desc = "", name_code = "";
            int count = 0;
            int B_Budgetyear=0;
            try
            {
                B_Budgetyear = Convert.ToInt32(ddlYear.SelectedValue);

                DataTable ProductGroups = objRSum.GetProductGroups(B_Budgetyear);
                //Session["ProductGroups"] = ProductGroups;
                //DataTable selectedTable = ProductGroups.AsEnumerable()
                //                .Where(r => r.Field<int>("year") == B_Budgetyear)
                //                .CopyToDataTable();

                List<string> PgList = new List<string>();

                for (int i = 0; i < ProductGroups.Rows.Count; i++)
                {
                    PgList.Add(Convert.ToString(ProductGroups.Rows[i][1]));
                    if (Convert.ToString(ProductGroups.Rows[i][1]) == "FIVE YEARS")
                    {
                        ProductGroups.Rows[i][1] = "5YRS";
                    }
                    else if (Convert.ToString(ProductGroups.Rows[i][1]) == "TEN YEARS")
                    {
                        ProductGroups.Rows[i][1] = "10YRS";
                    }
                }
                Session["PgList"] = PgList;
                ProductGroupList.DataSource = ProductGroups;
                ProductGroupList.DataTextField = "splgrps";
                ProductGroupList.DataValueField = "splgrps";
                ProductGroupList.DataBind();

                foreach (System.Web.UI.WebControls.ListItem val in ProductGroupList.Items)
                {
                    val.Selected = true;
                    if (val.Selected)
                    {
                        count++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                    }
                }

                //  ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                //  ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }
        
        /// <summary>
        ///  Author :
        /// Created date :
        /// Description : For Setting Selected value to the dropDownlist of month and year
        /// </summary>
        private void LoadMonthYear()
        {
            int Month = DateTime.Now.Month;
            int year = DateTime.Now.Year;
            try
            {
                if (Month == 1)
                {
                    year = year - 1;
                    Month = 12;
                }
                else
                {
                    Month -= 1;
                }
                ddlYear.SelectedValue = Convert.ToString(year);
                ddlMonth.SelectedValue = Convert.ToString(Month);
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }


        /// <summary>
        /// Author  : 
        /// Date    : 
        /// Desc    : 
        /// Modified by : K.LakshmiBindu
        /// Modified date : Dec 4,2018
        /// Desc(modification) : added code for selecting all the list items in listbox
        ///
        /// </summary>
        /// </summary>
        private void LoadProductFamliy()
        {
            DataTable dtProductFamilyList = new DataTable();
            try
            {
                dtProductFamilyList = objBudget.LoadFamilyId();
                if (dtProductFamilyList.Rows.Count > 0)
                {
                    ProductFamList.DataSource = dtProductFamilyList;
                    ProductFamList.DataTextField = "item_family_name";
                    ProductFamList.DataValueField = "item_family_id";
                    ProductFamList.DataBind();
                }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            foreach (System.Web.UI.WebControls.ListItem val in ProductFamList.Items)
            {
                val.Selected = true;
            }
        }

        /// <summary>
        /// Author  : 
        /// Date    : 
        /// Desc    :  Load Branches into ListBox 

        /// </summary>
        /// </summary>
        protected void LoadBranches()
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                string roleId = Session["RoleId"].ToString();
                string userId = Session["UserId"].ToString();
                string branchcode = Session["BranchCode"].ToString();
                DataTable dtData = new DataTable();
                objJSR.branch = userId; // passing here territory Engineer Id  as branch code IF role is TM 
                objJSR.roleId = roleId;
                objJSR.flag = "Branch";
                objJSR.cter = cter;
                dtData = objJSRDAL.getFilterAreaValue(objJSR);
                if (dtData.Rows.Count > 0)
                {
                    BranchList.DataSource = dtData;
                    BranchList.DataTextField = "BranchDesc";
                    BranchList.DataValueField = "BranchCode";
                    BranchList.DataBind();
                }
                foreach (System.Web.UI.WebControls.ListItem val in BranchList.Items)
                    {
                        val.Selected = true;
                        
                    }
               
                ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }

        /// <summary>
        /// Author : Anamika
        /// Date : May 1, 2017
        /// Desc : fills dropdownlist of year for JSR Reports
        /// </summary>
        private void LoadYear()
        {
            try
            {
                if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
                DataTable dtData = new DataTable();
                dtData = objJSRDAL.getYear();
                if (dtData.Rows.Count > 0)
                {
                    ddlYear.DataSource = dtData;
                    ddlYear.DataTextField = "Year_no";
                    ddlYear.DataValueField = "Year_no";
                    ddlYear.DataBind();
                    ddlYear.SelectedIndex = 1;
                }
                ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }


        /// <summary>
        /// Author : 
        /// Date : 
        /// Desc : add css class to the rows of gridview 
        /// </summary>

        protected void bindgridColor()
        {
            try
            {
                if (grdviewAllValues.Rows.Count != 0)
                {
                    List<string> PgList = null;
                    int colorIndex = 0;
                    int rowIndex = 1, subRowIndex = 0, productTypeIndex = 1; int color = 0, parent_row_index = 0;
                    bool currentRowIsLast = false;
                    foreach (GridViewRow row in grdviewAllValues.Rows)
                    {

                        var check = row.FindControl("lblSumFlag") as Label;
                        PgList = Session["PgList"] as System.Collections.Generic.List<string>;
                        for (int i = 0; i < PgList.Count; i++)
                        {
                            switch (PgList[i])
                            {
                                case "GOLD" :
                                    var lblgold = row.FindControl("lblgold") as Label;
                                    lblgold.Visible = true;
                                    break;
                                case "TOP":
                                    var lbltop = row.FindControl("lbltop") as Label;
                                    lbltop.Visible = true;
                                    break;
                                case "BB":
                                    var lblBB = row.FindControl("lblbb") as Label;
                                    lblBB.Visible = true;
                                    break;
                                case "FIVE YEARS":
                                    var lbl5yrs = row.FindControl("lbl5yrs") as Label;
                                    lbl5yrs.Visible = true;
                                    break;
                                case "TEN YEARS":
                                    var lbltenyrs = row.FindControl("lbltenyrs") as Label;
                                    lbltenyrs.Visible = true;
                                    break;
                                case "SPC":
                                    var lblspc = row.FindControl("lblspc") as Label;
                                    lblspc.Visible = true;
                                    break;
                            }
                       }

                        
                      
                        string txt = check.Text;
                        if (txt == "typeSum")
                        {
                            row.CssClass = "product_row_hide subTotalRowGrid subrowindex subrowindex_" + subRowIndex + " ";

                            productTypeIndex++;
                        }
                        else if (txt == "SubFamilySum")
                        {
                            row.CssClass = "product_row_hide greendarkSubFamSum subrowindex subrowindex_" + subRowIndex;
                        }
                        else if (txt == "SubFamilyHeading")
                        {
                            row.CssClass = "product_row_hide greendark row_index row_" + rowIndex + " parent_row_index_" + parent_row_index;

                            var image = row.FindControl("Image1") as System.Web.UI.WebControls.Image;
                            image.CssClass = "row_index_image";

                            row.Attributes["data-index"] = rowIndex.ToString();
                            rowIndex++;
                            subRowIndex++;
                        }
                        else if (txt == "products")
                        {
                            color++;
                            if (color == 1) { row.CssClass = "color_Product1 "; }
                            else if (color == 2)
                            {
                                row.CssClass = "color_Product2 ";
                                color = 0;
                            }

                            row.CssClass += "product_row_hide product_type_" + productTypeIndex + " subrowindex subrowindex_" + subRowIndex;
                        }
                        else if (txt == "FamilyHeading")
                        {
                            row.CssClass = "parent_row_index";
                            row.Attributes["data-parent_row_index"] = "" + (++parent_row_index);

                            if (colorIndex <= 6)
                            {
                                for (int i = 0; i < row.Cells.Count; i++)
                                {
                                    row.Cells[0].CssClass = "TotalRowGridHead1 " + GetCssFam(colorIndex); ;
                                    row.Cells[i].CssClass = "TotalRowGridHead " + GetCssFam(colorIndex); ;
                                }
                                //row.CssClass = GetCssFam(colorIndex); 
                                colorIndex++;
                            }
                            else
                            {
                                colorIndex = 0;
                                for (int i = 0; i < row.Cells.Count; i++)
                                {
                                    row.Cells[0].CssClass = "TotalRowGridHead1 " + GetCssFam(colorIndex); ;
                                    row.Cells[i].CssClass = "TotalRowGridHead " + GetCssFam(colorIndex); ;
                                }
                            }



                        }
                        else if (txt == "BranchHeading")
                        {
                            row.CssClass = "parent_row_index";
                            row.Attributes["data-parent_row_index"] = "" + (++parent_row_index);

                            if (colorIndex <= 6)
                            {
                                for (int i = 0; i < row.Cells.Count; i++)
                                {
                                    row.Cells[0].CssClass = "TotalRowGridHead1 " + GetCssFam(colorIndex); ;
                                    row.Cells[i].CssClass = "TotalRowGridHead " + GetCssFam(colorIndex); ;
                                }
                                //row.CssClass = GetCssFam(colorIndex); 
                                colorIndex++;
                            }
                            else
                            {
                                colorIndex = 0;
                                for (int i = 0; i < row.Cells.Count; i++)
                                {
                                    row.Cells[0].CssClass = "TotalRowGridHead1 " + GetCssFam(colorIndex); ;
                                    row.Cells[i].CssClass = "TotalRowGridHead " + GetCssFam(colorIndex); ;
                                }
                            }



                        }
                        else if (txt == "SEHeading")
                        {
                            row.CssClass = "product_row_hide greendark row_index row_" + rowIndex + " parent_row_index_" + parent_row_index;

                            var image = row.FindControl("Image1") as System.Web.UI.WebControls.Image;
                            image.CssClass = "row_index_image";

                            row.Attributes["data-index"] = rowIndex.ToString();
                            rowIndex++;
                            subRowIndex++;
                        }
                        else if (txt == "FamilySum")
                        {
                            row.CssClass = "product_row_hide TotalRowGrid" + " parent_row_index_" + parent_row_index;
                            currentRowIsLast = true;

                        }
                        else if (txt == "MainSum")
                        {
                            row.CssClass = "MainTotal";
                        }

                        else if (txt == "")
                        {
                            row.CssClass = "empty_row";
                            if (!currentRowIsLast)
                            {
                                row.CssClass += " product_row_hide ";
                            }
                            currentRowIsLast = false;
                            for (int i = 0; i < row.Cells.Count; i++)
                            {
                                row.Cells[i].CssClass = "HidingHeading";
                            }
                        }
                        else if (txt == "HidingHeading")
                        {

                            for (int i = 0; i < row.Cells.Count; i++)
                            { row.Cells[i].CssClass = "greendark MainHeader"; }
                        }
                        else if (txt == "sumFlag")
                        {
                            for (int i = 0; i < row.Cells.Count; i++)
                            { row.Cells[i].CssClass = "greendark MainHeader"; }
                        }

                    }
                }


            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }

        }



        /// <summary>
        /// Author : 
        /// Date : 
        /// Desc : declaring CSS classes for different Product family in gridview
        protected void LoadCSS()
        {
            try
            {
                cssList.Add("color_3");
                cssList.Add("color_4");
                cssList.Add("color_3");

                cssListFamilyHead.Add("heading1");
                cssListFamilyHead.Add("heading2");
                cssListFamilyHead.Add("heading3");
                cssListFamilyHead.Add("heading4");
                cssListFamilyHead.Add("heading5");
                cssListFamilyHead.Add("heading6");
                cssListFamilyHead.Add("heading7");
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }

        /// <summary>
        /// Author : 
        /// Date : 
        /// Desc : add css class to the rows of gridview 
        /// </summary>
        ///<param name="colorIndex"> based on the row number we are choosing which CSS class to assaign to the row </param>
        /// <returns></returns>
        protected string GetCSS(int colorIndex)
        {
            string index = Convert.ToString(colorIndex);
            string cIndex = index[index.Length - 1].ToString();

            if (cIndex.Contains("1"))
            { return cssList.ElementAt(1); }
            else if (cIndex.Contains("2"))
            { return cssList.ElementAt(2); }
            //else if (cIndex.Contains("3"))
            //{ return cssList.ElementAt(3); }
            //else if (cIndex.Contains("4"))
            //{ return cssList.ElementAt(4); }
            //else if (cIndex.Contains("5"))
            //{ return cssList.ElementAt(5); }
            else { return cssList.ElementAt(2); }

        }

        /// <summary>
        /// Author : 
        /// Date : 
        /// Desc : Retriews CSS class bases on the row index that we pass from BindGridColor method for FamilyHeading
        /// </summary>
     /// <param name="colorIndex"></param>
     /// <returns>css class</returns>
        protected string GetCssFam(int colorIndex)
        {
            try
            {
                string index = Convert.ToString(colorIndex);
                string cIndex = index[index.Length - 1].ToString();

                if (cIndex.Contains("1"))
                { return cssListFamilyHead.ElementAt(1); }
                else if (cIndex.Contains("2"))
                { return cssListFamilyHead.ElementAt(2); }
                else if (cIndex.Contains("3"))
                { return cssListFamilyHead.ElementAt(3); }
                else if (cIndex.Contains("4"))
                { return cssListFamilyHead.ElementAt(4); }
                else if (cIndex.Contains("5"))
                { return cssListFamilyHead.ElementAt(5); }
                else if (cIndex.Contains("6"))
                { return cssListFamilyHead.ElementAt(6); }
                else { return cssListFamilyHead.ElementAt(0); }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
                return null;
            }
        }

        protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
        {
                LoadProductGroup();
        }
        /// <summary>
        /// Author:K.LakshmiBindu
        /// Date:12,dec 2018
        /// </summary>
       
    

        #endregion

        #region Commented Code

        /// <summary>
        /// Author  : Anamika
        /// Date    : April 27,2017
        /// Desc    : Click on "All" checkbox, all customer number are selected
        ///  Modified by : K.LakshmiBindu
        /// Modified date :Dec 4,2018
        /// Desc(modification) :Commented this method as it is not required 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //protected void ChkCustNumAll_CheckedChanged(object sender, EventArgs e)
        //{
        //    //if (ChkCustNumAll.Checked == true)
        //    //{

        //    //    foreach (System.Web.UI.WebControls.ListItem val in ChkCustNum.Items)
        //    //    {
        //    //        val.Selected = true;
        //    //    }
        //    //    ChkCustNum_SelectedIndexChanged(null, null);
        //    //    //if (ChkCustNameAll.Checked == false)
        //    //    //{
        //    //    //    ChkCustNameAll.Checked = true;
        //    //    //    ChkCustNameAll_CheckedChanged(null, null);
        //    //    //}
        //    //}
        //    //else
        //    //{
        //    //    foreach (System.Web.UI.WebControls.ListItem val in ChkCustNum.Items)
        //    //    {
        //    //        val.Selected = false;
        //    //    }
        //        //if (ChkCustNameAll.Checked == true)
        //        //{
        //        //    ChkCustNameAll.Checked = false;
        //        //    ChkCustNameAll_CheckedChanged(null, null);
        //        //}
        //   // }
        //}

        //public void load_to_excel_template(DataTable dataTable)
        //{
        //    Workbook book = new Workbook("Import_Table.xlsx");

        //    //Get the worksheet using index
        //    Worksheet worksheet = book.Worksheets[0];

        //    //the worksheet as a header row
        //    worksheet.Cells.ImportDataTable(dataTable, true, "A1");

        //    //Open the template worksheet
        //    Workbook templateBook = new Workbook("template.xlsx");

        //    //Combine Multiple files
        //    templateBook.Combine(book);

        //    //Save resultant files
        //    templateBook.Save("Output.xlsx", SaveFormat.Xlsx);
        //}
        /// <summary>
        /// Author  : Anamika
        /// Date    : April 27,2017
        /// Desc    : Click on "All" checkbox, all sales engineers are selected
        ///  Modified by : K.LakshmiBindu
        /// Modified date : :Dec 4,2018
        /// Desc(modification) :Commented this method as it is not required 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //protected void CheckSalEngAll_CheckedChanged(object sender, EventArgs e)
        //{




        //    //if (CheckSalEngAll.Checked == true)
        //    //{
        //    //    foreach (System.Web.UI.WebControls.ListItem val in ChkSalesEng.Items)
        //    //    {
        //    //        val.Selected = true;
        //    //    }
        //    //    ChkSalesEng_SelectedIndexChanged(null, null);
        //    //}
        //    //else
        //    //{
        //    //    foreach (System.Web.UI.WebControls.ListItem val in ChkSalesEng.Items)
        //    //    {
        //    //        val.Selected = false;
        //    //    }

        //    //}
        //}

        /// <summary>
        /// Author  : Anamika
        /// Date    : April 27,2017
        /// Desc    : Click on "All" checkbox, all branches are selected
        /// Modified by : K.LakshmiBindu
        /// Modified date : Dec 4,2018
        /// Desc(modification) :Commented this method as it is not required 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //protected void cbBranchAll_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (cbBranchAll.Checked == true)
        //    {
        //        foreach (System.Web.UI.WebControls.ListItem val in ChkBranches.Items)
        //        {
        //            val.Selected = true;
        //        }
        //        txtbranchlist.Text = "ALL";
        //        ChkBranches_SelectedIndexChanged(null, null);
        //    }
        //    else
        //    {
        //        foreach (System.Web.UI.WebControls.ListItem val in ChkBranches.Items)
        //        {
        //            val.Selected = false;
        //        }

        //    }
        //}
        /// <summary>
        /// Author  : Anamika
        /// Date    : April 27,2017
        /// Desc    : Click on "All" checkbox, all customername are selected
        /// Modified by : K.LakshmiBindu
        /// Modified date  Dec 4,2018
        /// Desc(modification) :Commented this method as it is not required 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //protected void ChkCustNameAll_CheckedChanged(object sender, EventArgs e)
        //{



        //    //if (ChkCustNameAll.Checked == true)
        //    //{
        //    //    foreach (System.Web.UI.WebControls.ListItem val in ChkCustName.Items)
        //    //    {
        //    //        val.Selected = true;
        //    //    }
        //    //    ChkCustName_SelectedIndexChanged(null, null);
        //    //    if (ChkCustNumAll.Checked == false)
        //    //    {
        //    //        ChkCustNumAll.Checked = true;
        //    //        ChkCustNumAll_CheckedChanged(null, null);

        //    //    }
        //    //}
        //    //else
        //    //{
        //    //    foreach (System.Web.UI.WebControls.ListItem val in ChkCustName.Items)
        //    //    {
        //    //        val.Selected = false;
        //    //    }
        //    //    if (ChkCustNumAll.Checked == true)
        //    //    {
        //    //        ChkCustNumAll.Checked = false;
        //    //        ChkCustNumAll_CheckedChanged(null, null);

        //    //    }
        //    //}
        //}
        /// <summary>
        /// Author  : Anamika
        /// Date    : April 27,2017
        /// Desc    : Click on "All" checkbox, all applications are selected
        ///Modified by : K.LakshmiBindu
        /// Modified date :  Dec 4,2018
        /// Desc(modification) :Commented this method as it is not required 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //protected void ChkAppAll_CheckedChanged(object sender, EventArgs e)
        //{
        //    //if (ChkAppAll.Checked == true)
        //    //{
        //    //    foreach (System.Web.UI.WebControls.ListItem val in ChkApplicationList.Items)
        //    //    {
        //    //        val.Selected = true;
        //    //    }
        //    //    ChkApplicationList_SelectedIndexChanged(null, null);
        //    //}
        //    //else
        //    //{
        //    //    foreach (System.Web.UI.WebControls.ListItem val in ChkApplicationList.Items)
        //    //    {
        //    //        val.Selected = false;
        //    //    }
        //    //    ChkApplicationList_SelectedIndexChanged(null, null);
        //    //}
        //}
        /// <summary>
        /// Author  : Anamika
        /// Date    : April 27,2017
        /// Desc    : Click on "All" checkbox, all sub families are selected
        /// Modified by : K.LakshmiBindu
        /// Modified date : Dec 4,2018
        /// Desc(modification) :Commented few lines of code  as it is not required 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //protected void ChkSubFamilyAll_CheckedChanged(object sender, EventArgs e)
        //{
        //    //if (ChkSubFamilyAll.Checked == true)
        //    //{
        //    //    foreach (System.Web.UI.WebControls.ListItem val in ChkSubFamily.Items)
        //    //    {
        //    //        val.Selected = true;
        //    //    }
        //    //    ChkSubFamily_SelectedIndexChanged(null, null);
        //    //}
        //    //else
        //    //{
        //    //    foreach (System.Web.UI.WebControls.ListItem val in ChkSubFamily.Items)
        //    //    {
        //    //        val.Selected = false;
        //    //    }
        //    //    ChkSubFamily_SelectedIndexChanged(null, null);
        //    //}
        //}

        /// <summary>
        /// Author  : Anamika
        /// Date    : April 27,2017
        /// Desc    : Click on "All" checkbox, all product group are selected
        ///  Modified by : K.LakshmiBindu
        /// Modified date : Dec 4,2018
        /// Desc(modification) :Commented this method as it is not required 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //protected void ChkProductGrpAll_CheckedChanged(object sender, EventArgs e)
        //{
        //    //if (ChkProductGrpAll.Checked == true)
        //    //{
        //    //    foreach (System.Web.UI.WebControls.ListItem val in ChkProductGroup.Items)
        //    //    {
        //    //        val.Selected = true;
        //    //    }
        //    //    ChkProductGroup_SelectedIndexChanged(null, null);
        //    //}
        //    //else
        //    //{
        //    //    foreach (System.Web.UI.WebControls.ListItem val in ChkProductGroup.Items)
        //    //    {
        //    //        val.Selected = false;
        //    //    }

        //    //}
        //}
        /// <summary>
        /// Author  : Anamika
        /// Date    : April 27,2017
        /// Desc    : Click on "All" checkbox, all families are selected
        /// Modified by : K.LakshmiBindu
        /// Modified date : Dec 4,2018
        /// Desc(modification) :Commented this method as it is not required 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //protected void ChkProductFamilyAll_CheckedChanged(object sender, EventArgs e)
        //{
        //    //if (ChkProductFamilyAll.Checked == true)
        //    //{
        //    //    foreach (System.Web.UI.WebControls.ListItem val in ChkProductFamily.Items)
        //    //    {
        //    //        val.Selected = true;
        //    //    }
        //    //    ChkProductFamily_SelectedIndexChanged(null, null);
        //    //}
        //    //else
        //    //{
        //    //    foreach (System.Web.UI.WebControls.ListItem val in ChkProductFamily.Items)
        //    //    {
        //    //        val.Selected = false;
        //    //    }
        //    //}
        //}


        #endregion

      
    }
  
        
}