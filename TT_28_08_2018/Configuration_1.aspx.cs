﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TaegutecSalesBudget
{
    public partial class Configuration_1 : System.Web.UI.Page
    {
        AdminConfiguration objAdmin = new AdminConfiguration();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserName"] == null) { Response.Redirect("Login.aspx"); }
            if (!IsPostBack)
            {
                getProfiler();
                getLinkVisibility();
               // lbl_Date.InnerText = "YEAR - MONTH - DATE ( Ex: " + DateTime.Today.ToString("yyyy-MM-dd") + " )";
            }

        }

        private void getLinkVisibility()
        {
            DataTable dt = new DataTable();
            dt = objAdmin.getVisibility();
            if (dt != null)
            {
                if (dt.Rows.Count > 0)
                {
                    if (dt.Rows[0]["TM"].ToString() == "1")
                        CheckBoxList1.Items[0].Selected = true;
                    if (dt.Rows[0]["HO"].ToString() == "1")
                        CheckBoxList1.Items[1].Selected = true;
                }
            }
        }
        protected void getProfiler()
        {
            txtBudgetYear.Text = objAdmin.GetProfile("BUDGET_YEAR");
            //txtActualYear.Text = objAdmin.GetProfile("ACTUAL_YEAR");
            //ddlActualMonth.SelectedValue = objAdmin.GetProfile("ACTUAL_MONTH");

            txtBudgetYear_B.Text = objAdmin.GetProfile("B_BUDGET_YEAR");
            //txtActualYear_B.Text = objAdmin.GetProfile("B_ACTUAL_YEAR");
           // ddlActualMonth_B.SelectedValue = objAdmin.GetProfile("B_ACTUAL_MONTH");

            decimal rate = Convert.ToDecimal(objAdmin.GetProfile("INC_RATE"));
            int val = (int)(rate * 100) - 100;

            txtIncRate.Text = val.ToString();


            txt_SE_StartDate.Text = objAdmin.GetProfile("START_DATE_SE");
            txt_SE_EndDate.Text = objAdmin.GetProfile("END_DATE_SE");

            txt_BM_StartDate.Text = objAdmin.GetProfile("START_DATE_BM");
            txt_BM_EndDate.Text = objAdmin.GetProfile("END_DATE_BM");

            txt_TM_StartDate.Text = objAdmin.GetProfile("START_DATE_TM");
            txt_TM_EndDate.Text = objAdmin.GetProfile("END_DATE_TM");

            txt_HO_StartDate.Text = objAdmin.GetProfile("START_DATE_HO");
            txt_HO_EndDate.Text = objAdmin.GetProfile("END_DATE_HO");




        }

        protected void btnBYUpdate_Click(object sender, EventArgs e)
        {
            updateProfile("BUDGET_YEAR", txtBudgetYear.Text);
        }

        protected void btn_AY_Update_Click(object sender, EventArgs e)
        {
            updateProfile("ACTUAL_YEAR", txtActualYear.Text);
        }

        protected void btn_AM_Update_Click(object sender, EventArgs e)
        {
            updateProfile("ACTUAL_MONTH", ddlActualMonth.Value);
        }

        protected void btn_IR_Update_Click(object sender, EventArgs e)
        {
            int val = Convert.ToInt32(txtIncRate.Text);
            decimal rate = (decimal)(val + 100) / 100;
            updateProfile("INC_RATE", rate.ToString());
        }

        protected void updateProfile(string PName, string PValue)
        {
            string Result = objAdmin.UpdateProfile(PName, PValue);
            if (Result == "Updated Successfully")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Updated Successfully'); triggerScript();", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Oops Something went wrong'); triggerScript();", true);
            }

        }

        protected void btn_AM_Update_B_Click(object sender, EventArgs e)
        {
            updateProfile("B_ACTUAL_MONTH", ddlActualMonth_B.Value);
        }

        protected void btn_AY_Update_B_Click(object sender, EventArgs e)
        {
            updateProfile("B_ACTUAL_YEAR", txtActualYear_B.Text);
        }

        protected void btnBYUpdate_B_Click(object sender, EventArgs e)
        {
            updateProfile("B_BUDGET_YEAR", txtBudgetYear_B.Text);
        }

        protected void btn_BVisibleUpdate_Click(object sender, EventArgs e)
        {
            updateProfile("START_DATE_SE", txt_SE_StartDate.Text);
            updateProfile("END_DATE_SE", txt_SE_EndDate.Text);

            updateProfile("START_DATE_BM", txt_BM_StartDate.Text);
            updateProfile("END_DATE_BM", txt_BM_EndDate.Text);

            updateProfile("START_DATE_TM", txt_TM_StartDate.Text);
            updateProfile("END_DATE_TM", txt_TM_EndDate.Text);

            updateProfile("START_DATE_HO", txt_HO_StartDate.Text);
            updateProfile("END_DATE_HO", txt_HO_EndDate.Text);
        }

        protected void btnlink_Click(object sender, EventArgs e)
        {
            string strLink = lbllink.Text;
            int TM = 0;
            int HO = 0;
            if (CheckBoxList1.Items[0].Selected)
            {
                TM = 1;
            }
            if (CheckBoxList1.Items[1].Selected)
            {
                HO = 1;
            }
            string Result = objAdmin.saveVisibility(strLink, TM, HO);
            if (Result == "Updated Successfully")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Updated Successfully'); triggerScript();", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Oops Something went wrong'); triggerScript();", true);
            }
        }


    }
}