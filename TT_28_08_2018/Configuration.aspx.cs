﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace TaegutecSalesBudget
{
    public partial class Configuration : System.Web.UI.Page
    { 
        #region GlobalDeclareation
        AdminConfiguration objAdmin = new AdminConfiguration();
        CommonFunctions objCom = new CommonFunctions();
        splgrpvisibility objsplgrp = new splgrpvisibility();
        #endregion

        #region Events
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["UserName"] == null) { Response.Redirect("Login.aspx"); }
            if (!IsPostBack)
            {
                getProfiler();
                getLinkVisibility();
                getBranch();
                txtYear.Text = Convert.ToString(DateTime.Now.Year);
                lbl_Date.InnerText = "YEAR - MONTH - DATE ( Ex: " + DateTime.Today.ToString("yyyy-MM-dd") + " )";

            }

        }

        protected void btnBYUpdate_Click(object sender, EventArgs e)
        {
            updateProfile("BUDGET_YEAR", txtBudgetYear.Text);
        }

        protected void btn_AY_Update_Click(object sender, EventArgs e)
        {
            updateProfile("ACTUAL_YEAR", txtActualYear.Text);
        }

        protected void btn_AM_Update_Click(object sender, EventArgs e)
        {
            updateProfile("ACTUAL_MONTH", ddlActualMonth.SelectedItem.Value);
        }

        protected void btn_IR_Update_Click(object sender, EventArgs e)
        {
            int val = Convert.ToInt32(txtIncRate.Text);
            decimal rate = (decimal)(val + 100) / 100;
            updateProfile("INC_RATE", rate.ToString());
        }

        protected void updateProfile(string PName, string PValue)
        {
            string Result = objAdmin.UpdateProfile(PName, PValue);
            if (Result == "Updated Successfully")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Updated Successfully'); triggerScript();", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Oops Something went wrong'); triggerScript();", true);
            }

        }

        protected void btn_AM_Update_B_Click(object sender, EventArgs e)
        {
            updateProfile("B_ACTUAL_MONTH", ddlActualMonth_B.SelectedItem.Value);
        }

        protected void btn_AY_Update_B_Click(object sender, EventArgs e)
        {
            updateProfile("B_ACTUAL_YEAR", txtActualYear_B.Text);
        }

        protected void btnBYUpdate_B_Click(object sender, EventArgs e)
        {
            updateProfile("B_BUDGET_YEAR", txtBudgetYear_B.Text);
        }
        /// <summary>
        /// Author:Neha
        /// Date:24th dec,2018
        /// Description: Adding popup for GALSyncronization
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnsyncGal(object sender, EventArgs e)
        {
            if (objAdmin.jobstart() == 0)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('The synchronization process with GAL system has started successfully, it will take 15 minutes to run. Please check the changes after 15 minutes.'); triggerScript();", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Oops! Something went wrong. Please contact technical team or try after sometime.'); triggerScript();", true);
            }
        }

        protected void btn_BVisibleUpdate_Click(object sender, EventArgs e)
        {
            updateProfile("START_DATE_SE", txt_SE_StartDate.Text);
            updateProfile("END_DATE_SE", txt_SE_EndDate.Text);

            updateProfile("START_DATE_BM", txt_BM_StartDate.Text);
            updateProfile("END_DATE_BM", txt_BM_EndDate.Text);

            updateProfile("START_DATE_TM", txt_TM_StartDate.Text);
            updateProfile("END_DATE_TM", txt_TM_EndDate.Text);

            updateProfile("START_DATE_HO", txt_HO_StartDate.Text);
            updateProfile("END_DATE_HO", txt_HO_EndDate.Text);
        }

        protected void btnlink_Click(object sender, EventArgs e)
        {
            string strLink = lbllink.Text;
            int TM = 0;
            int HO = 0;
            if (CheckBoxList1.Items[0].Selected)
            {
                TM = 1;
            }
            if (CheckBoxList1.Items[1].Selected)
            {
                HO = 1;
            }
            string Result = objAdmin.saveVisibility(strLink, TM, HO);
            if (Result == "Updated Successfully")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Updated Successfully'); triggerScript();", true);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Oops Something went wrong'); triggerScript();", true);
            }
        }

        protected void btnEnable_Click(object sender, EventArgs e)
        {
            try
            {
                int year = Convert.ToInt32(txtYear.Text);
                string branch = Convert.ToString(ddlBranch.SelectedValue);
                string result = objAdmin.saveEnableBudget(year, branch);
                if (result == "SUCCESS")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Updated Successfully'); triggerScript();", true);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Oops Something went wrong'); triggerScript();", true);
                }
            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
        }
      
        protected void ddlyears_SelectedIndexChanged(object sender, EventArgs e)
        {
            string year = ddlyears.SelectedValue;


           DataTable dtsplgrpvisible = objsplgrp.splgrpvisible(Convert.ToInt32(year));
           if (dtsplgrpvisible != null)
           {
               DataTable T_dtsplgrpvisible = GenerateTransposedTable(dtsplgrpvisible);
               pgl.DataSource = T_dtsplgrpvisible;
               pgl.DataTextField = "year";
               pgl.DataValueField = "year";
               pgl.DataBind();

               if (T_dtsplgrpvisible.Columns.Count > 1)
               {
                   foreach (ListItem item in pgl.Items)
                   {
                       for (int i = 0; i < T_dtsplgrpvisible.Rows.Count; i++)
                       {
                           if ((T_dtsplgrpvisible.Rows[i][0]).ToString() == item.Value)
                           {
                               if ((T_dtsplgrpvisible.Rows[i][1]).ToString() == "Y")
                               {
                                   item.Selected = true;
                               }

                           }
                       }
                   }
               }
               else
               {
                   foreach (ListItem item in pgl.Items)
                   {
                       item.Selected = false;
                   }
               }
           }
           else
           {
               foreach (ListItem item in pgl.Items)
               {
                   item.Selected = false;
               }
           }

        }

        protected void btnspcl_Click(object sender, EventArgs e)
            {
                string BB=null, SPC=null, GOLD=null, FIVEYEARS=null, TENYEARS=null,TOP=null;
                int year=Convert.ToInt32(ddlyears.SelectedValue);
                foreach (ListItem item in pgl.Items)
                {
                    if (item.Selected)
                    {
                        switch (item.Value)
                        {
                            case "BB": BB = "Y";
                                break;
                            case "SPC": SPC = "Y";
                                break;
                            case "GOLD": GOLD = "Y";
                                break;
                            case "TOP": TOP="Y";
                                break;
                            case "FIVE YEARS": FIVEYEARS = "Y";
                                break;
                            case "TEN YEARS": TENYEARS = "Y";
                                break;

                        }
                    }
                }

              string message =objsplgrp.Update_splgrpvisible(year,BB, SPC, GOLD,TOP, FIVEYEARS, TENYEARS);
              if (message == "SUCCESS")
              {
                  ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Updated Successfully'); triggerScript();", true);
              }
              else
              {
                  ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Oops Something went wrong'); triggerScript();", true);
              }
            }
        #endregion

        #region Methods
            private void getBranch()
            {
                DataTable dt = new DataTable();
                try
                {
                    if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

                    dt = objAdmin.getBranch();
                    if (dt != null && dt.Rows.Count > 0)
                    {
                        ddlBranch.DataSource = dt;
                        ddlBranch.DataTextField = "region_description";
                        ddlBranch.DataValueField = "region_code";
                        ddlBranch.DataBind();
                    }
                    else
                    {
                        ddlBranch.DataSource = null;
                        ddlBranch.DataBind();
                    }
                }
                catch (Exception ex)
                {
                    objCom.LogError(ex);
                }

            }
            private DataTable GenerateTransposedTable(DataTable inputTable)
            {
                DataTable outputTable = new DataTable();

                // Add columns by looping rows

                // Header row's first column is same as in inputTable
                outputTable.Columns.Add(inputTable.Columns[0].ColumnName.ToString());

                // Header row's second column onwards, 'inputTable's first column taken
                foreach (DataRow inRow in inputTable.Rows)
                {
                    string newColName = inRow[0].ToString();
                    outputTable.Columns.Add(newColName);
                }

                // Add rows by looping columns        
                for (int rCount = 1; rCount <= inputTable.Columns.Count - 1; rCount++)
                {
                    DataRow newRow = outputTable.NewRow();

                    // First column is inputTable's Header row's second column
                    newRow[0] = inputTable.Columns[rCount].ColumnName.ToString();
                    for (int cCount = 0; cCount <= inputTable.Rows.Count - 1; cCount++)
                    {
                        string colValue = inputTable.Rows[cCount][rCount].ToString();
                        newRow[cCount + 1] = colValue;
                    }
                    outputTable.Rows.Add(newRow);
                }

                return outputTable;
            }
            private void getLinkVisibility()
            {
                DataTable dt = new DataTable();
                dt = objAdmin.getVisibility();
                if (dt != null)
                {
                    if (dt.Rows.Count > 0)
                    {
                        if (dt.Rows[0]["TM"].ToString() == "1")
                            CheckBoxList1.Items[0].Selected = true;
                        if (dt.Rows[0]["HO"].ToString() == "1")
                            CheckBoxList1.Items[1].Selected = true;
                    }
                }
            }
            protected void getProfiler()
            {
                txtBudgetYear.Text = objAdmin.GetProfile("BUDGET_YEAR");

                txtActualYear.Text = objAdmin.GetProfile("ACTUAL_YEAR");
                ddlActualMonth.SelectedValue = objAdmin.GetProfile("ACTUAL_MONTH");

                txtBudgetYear_B.Text = objAdmin.GetProfile("B_BUDGET_YEAR");
                string B_BUDGET_YEAR = txtBudgetYear_B.Text;
                List<string> years = new List<string>();
                years.Add(B_BUDGET_YEAR);
                string By = Convert.ToString(Convert.ToInt32(B_BUDGET_YEAR) + 1);
                years.Add(By);
                ddlyears.DataSource = years;
                ddlyears.DataBind();
                DataTable dtsplgrpvisible = new DataTable();

                dtsplgrpvisible = objsplgrp.splgrpvisible(Convert.ToInt32(B_BUDGET_YEAR));
                if (dtsplgrpvisible != null)
                {
                    DataTable T_dtsplgrpvisible = GenerateTransposedTable(dtsplgrpvisible);
                    pgl.DataSource = T_dtsplgrpvisible;
                    pgl.DataTextField = "year";
                    pgl.DataValueField = "year";
                    pgl.DataBind();
                    if (T_dtsplgrpvisible.Columns.Count > 1)
                    {
                        foreach (ListItem item in pgl.Items)
                        {
                            for (int i = 0; i < T_dtsplgrpvisible.Rows.Count; i++)
                            {
                                if ((T_dtsplgrpvisible.Rows[i][0]).ToString() == item.Value)
                                {
                                    if ((T_dtsplgrpvisible.Rows[i][1]).ToString() == "Y")
                                    {
                                        item.Selected = true;
                                    }

                                }
                            }
                        }
                    }
                    else
                    {
                        foreach (ListItem item in pgl.Items)
                        {
                            item.Selected = false;
                        }

                    }
                }
                else
                {
                    foreach (ListItem item in pgl.Items)
                    {
                        item.Selected = false;
                    }
                }
                //List<string> chkboxsplgrp = new List<string>();

                //    for (int i = 0; i < T_dtsplgrpvisible.Rows.Count; i++)
                //    {
                //        if ((T_dtsplgrpvisible.Rows[i][1]).ToString() == "Y")
                //        {
                //            chkboxsplgrp.Add(Convert.ToString(T_dtsplgrpvisible.Rows[i][0]));
                //        }
                //    }






                //else
                //{
                //    pgl.DataSource = null;
                //    pgl.DataBind();
                //    pgl.Visible = false;
                //}


                ////////////////////////////////////



                txtActualYear_B.Text = objAdmin.GetProfile("B_ACTUAL_YEAR");
                ddlActualMonth_B.SelectedValue = objAdmin.GetProfile("B_ACTUAL_MONTH");

                decimal rate = Convert.ToDecimal(objAdmin.GetProfile("INC_RATE"));
                int val = (int)(rate * 100) - 100;

                txtIncRate.Text = val.ToString();


                txt_SE_StartDate.Text = objAdmin.GetProfile("START_DATE_SE");
                txt_SE_EndDate.Text = objAdmin.GetProfile("END_DATE_SE");

                txt_BM_StartDate.Text = objAdmin.GetProfile("START_DATE_BM");
                txt_BM_EndDate.Text = objAdmin.GetProfile("END_DATE_BM");

                txt_TM_StartDate.Text = objAdmin.GetProfile("START_DATE_TM");
                txt_TM_EndDate.Text = objAdmin.GetProfile("END_DATE_TM");

                txt_HO_StartDate.Text = objAdmin.GetProfile("START_DATE_HO");
                txt_HO_EndDate.Text = objAdmin.GetProfile("END_DATE_HO");




            }
            #endregion
    }
    }

