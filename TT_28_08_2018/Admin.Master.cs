﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TaegutecSalesBudget
{
    public partial class Admin : System.Web.UI.MasterPage
    {
        LoginAuthentication authObj = new LoginAuthentication();
        protected void Page_Load(object sender, EventArgs e)
        {
         
            if (!IsPostBack)
            {
                if (Session["UserName"] != null)
                {
                    SaveModules();
                    lblUserName.Text = Session["UserName"].ToString();
                }

            }
            if (Session["RoleId"] != null)
            {
                if (Session["RoleId"].ToString() == "SE" || Session["RoleId"].ToString() == "BM" || Session["RoleId"].ToString() == "HO")
                {

                    if (Request.QueryString.Get(0) == "Configuration") { Response.Redirect("ReportsDashboard.aspx?RD"); return; }
                    if (Request.QueryString.Get(0) == "UserMgt") { Response.Redirect("ReportsDashboard.aspx?RD"); return; }
                    if (Request.QueryString.Get(0) == "Configuration") { Response.Redirect("ReportsDashboard.aspx?RD"); return; }
                    if (Request.QueryString.Get(0) == "Profile") { Response.Redirect("ReportsDashboard.aspx?RD"); return; }
                }
            }
        }

        protected void lnkbtnLogout_Click(object sender, EventArgs e)
        {
            if (Session["UserName"] != null)
            {

                Session["UserName"] = null;
                Session["LoginMailId"] = null;
                Session["RoleId"] = null;
                Session["UserId"] = null;
                Session.RemoveAll();
                Response.Redirect("Login.aspx");
            }
        }
        protected void confirmpwd_Click(object sender, EventArgs e)
        {
            if (Session["UserName"] != null)
            {

                string strpassword = authObj.Encrypt(pwd.Text);
                authObj.MailPassword = strpassword;
                authObj.EngineerId = Convert.ToString(Session["UserId"]);
                string ErrorMessage = authObj.resetpwd(authObj);
                if (ErrorMessage == null)
                {
                    string scriptString = "<script type='text/javascript'> alert('Password was successfully reset');window.location='Login.aspx';</script>";
                    ClientScriptManager script = Page.ClientScript;
                    script.RegisterClientScriptBlock(GetType(), "Script", scriptString);
                }
                else
                {
                    string scriptString = "<script type='text/javascript'> alert('Failed to reset');window.location='Login.aspx';</script>";
                    ClientScriptManager script = Page.ClientScript;
                    script.RegisterClientScriptBlock(GetType(), "Script", scriptString);
                }
            }
            else
            {
                Response.Redirect("Login.aspx");
            }
        }


        /// <summary>
        /// Author:K.LakshmiBindu
        /// Desc: For Saving userlogs based on time with module info used by user
        /// Date : Feb 12, 2019
        /// </summary>
        public void SaveModules()
        {
            int i = 0;
            SqlConnection cn = null;

            try
            {
                cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
                SqlCommand cmd = new SqlCommand("sp_saveModuleLogs", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter[] sp = new SqlParameter[]{
               new SqlParameter("@EngineerId",Convert.ToString(Session["UserId"])),
               new SqlParameter("@UniqueId",Convert.ToString(Session["UserGuid"])),
               new SqlParameter("@Module",Request.FilePath)
            };
                cmd.Parameters.AddRange(sp);
                cn.Open();
                i = cmd.ExecuteNonQuery();
                if (i > 0)
                {
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
            }
        }
    }
}