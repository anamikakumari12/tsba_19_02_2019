﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CUSTReview.aspx.cs" Inherits="TaegutecSalesBudget.CUSTReview" MasterPageFile="~/Site.Master"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
     <script src="js/jquery.dataTables.min.js"></script>
    <link href="css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="css/buttons.dataTables.min.css" rel="stylesheet" />
    
    <script type="text/javascript" class="init">
        $(document).ready(function () {
            $('#collapsebtn').unbind('click').bind('click', function (e) {
                debugger;
                var attr = $('#product_image').attr('src');
                $("#dfltreportform").slideToggle();
                if (attr == "images/up_arrow.png") {
                    $("#product_image").attr("src", "images/down_arrow.png");
                } else {
                    $("#product_image").attr("src", "images/up_arrow.png");
                }
            });
        });
        function triggerPostGridLodedActions() {
            bindGridView();
            $('#collapsebtn').unbind('click').bind('click', function (e) {
                debugger;
                var attr = $('#product_image').attr('src');
                $("#dfltreportform").slideToggle();
                if (attr == "images/up_arrow.png") {
                    $("#product_image").attr("src", "images/down_arrow.png");
                } else {
                    $("#product_image").attr("src", "images/up_arrow.png");
                }
            });
        }

        function bindGridView() {
            debugger;
            var annual = $('#MainContent_hdnannual').val();
            var jan = $('#MainContent_hdnjan').val();
            var feb = $('#MainContent_hdnfeb').val();
            var mar = $('#MainContent_hdnmar').val();
            var apr = $('#MainContent_hdnapr').val();
            var may = $('#MainContent_hdnmay').val();
            var jun = $('#MainContent_hdnjun').val();
            var jul = $('#MainContent_hdnjul').val();
            var aug = $('#MainContent_hdnaug').val();
            var sep = $('#MainContent_hdnsep').val();
            var oct = $('#MainContent_hdnoct').val();
            var nov = $('#MainContent_hdnnov').val();
            var dec = $('#MainContent_hdndec').val();
            var tot = $('#MainContent_hdntot').val();
            var ach = $('#MainContent_hdnach').val();
            var head_content = $('#MainContent_custargetvssales tr:first').html();
            $('#MainContent_custargetvssales').prepend('<thead></thead>')
            $('#MainContent_custargetvssales thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_custargetvssales tbody tr:first').hide();
            $('#MainContent_custargetvssales').append('<tfoot><tr> <th colspan="4" style="text-align:right">GRAND TOTAL:</th><th style="text-align: center">' + annual + '</th><th style="text-align: center">' + jan + '</th><th style="text-align: center">' + feb + '</th><th style="text-align: center">' + mar + '</th><th style="text-align: center">' + apr + '</th><th style="text-align: right">' + may + '</th><th style="text-align: center">' + jun + '</th><th style="text-align: center">' + jul + '</th><th style="text-align: center">' + aug + '</th><th style="text-align: center">' + sep + '</th><th style="text-align: center">' + oct + '</th><th style="text-align: center">' + nov + '</th><th style="text-align: center">' + dec + '</th><th style="text-align: center">' + tot + '</th><th style="text-align: center">' + ach + '%</th></tr></tfoot>');
            $('#MainContent_custargetvssales').DataTable(
               {
                   "info": false,
               });
            $('#MainContent_custargetvssales').on('search.dt', function () {
                debugger;
                var value = $('.dataTables_filter input').val();
                console.log(value); // <-- the value
                $('#MainContent_hdnsearch').val = value;
                document.getElementById('<%=hdnsearch.ClientID%>').value = value;
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "CUSTReview.aspx/setGrandtotal",
                    data: "{'search':'" + value + "'}",
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        console.log(data.d);
                        var parsed = data.d;
                        console.log(parsed);
                        console.log(parsed[1]);
                        $('#MainContent_custargetvssales').prepend('<tfoot></tfoot>');
                        $('#MainContent_custargetvssales tfoot').hide();
                        var annual = parsed[0];
                        var jan = parsed[1];
                        var feb = parsed[2];
                        var mar = parsed[3];
                        var apr = parsed[4];
                        var may = parsed[5];
                        var jun = parsed[6];
                        var jul = parsed[7];
                        var aug = parsed[8];
                        var sep = parsed[9];
                        var oct = parsed[10];
                        var nov = parsed[11];
                        var dec = parsed[12];
                        var tot = parsed[13];
                        var ach = parsed[14];
                        $('#MainContent_custargetvssales').append('<tfoot><tr> <th colspan="4" style="text-align:right">GRAND TOTAL:</th><th style="text-align: center">' + annual + '</th><th style="text-align: center">' + jan + '</th><th style="text-align: center">' + feb + '</th><th style="text-align: center">' + mar + '</th><th style="text-align: center">' + apr + '</th><th style="text-align: center">' + may + '</th><th style="text-align: center">' + jun + '</th><th style="text-align: center">' + jul + '</th><th style="text-align: center">' + aug + '</th><th style="text-align: center">' + sep + '</th><th style="text-align: center">' + oct + '</th><th style="text-align: center">' + nov + '</th><th style="text-align: center">' + dec + '</th><th style="text-align: center">' + tot + '</th><th style="text-align: center">' + ach + '%</th></tr></tfoot>');
                        console.log("2");
                    },
                    error: function (result) {
                        console.log("No Match");
                    }
                });
            });
        }


        $('#MainContent_custargetvssales').on('search.dt', function () {
                           debugger;
                         var value = $('.dataTables_filter input').val();
                           console.log(value);
                           document.getElementById('<%=hdnsearch.ClientID%>').value = value;
                            $.ajax({
                             type: "POST",
                              contentType: "application/json; charset=utf-8",
                              url: "CUSTReview.aspx/setGrandtotal",
                               data: "{'search':'" + value + "'}",
                                dataType: "json",
                               success: function (data) {
                                  var arr = [];        
                              for (var x in data) {
                           }
                             console.log("1");
                          },
                          error: function (result) {
                              console.log("No Match");
                           }
                       });
                    });
                     
        
                             
        </script>
    <style>
      .control_dropdown {
            width: 180px;
            height: 30px;
            border-radius: 4px!important;
            position: relative;
        }

        .label {
            padding-top: 9px;
            width: 100%;
            color: black;
        }

            .control {
            padding-top: 2px;
        }

            .btn.green {
            /*color: #fff;*/
            margin-left: auto;
            margin-right: auto;
            overflow: auto;
            cursor: pointer;
            top: -9px;
            position: relative;
            width: 100px;
            font-weight: 600;
            margin-top: -1px;
        }
              .mn_popup
        {
            width: 60%;
            /*align-content: center;*/
            margin: 15%;
        }

        body
        {
            font-family: 90%/1.45em "Helvetica Neue", HelveticaNeue, Helvetica, Arial, sans-serif;
        }

        tfoot
        {
            border: solid 1px #ebeef5;
            padding: 3px 10px;
        }

            tfoot tr th
            {
                text-align: right;
                padding: 3px!important;
            }

        tbody
        {
            border-color: #fff;
        }

        .mn_margin
        {
            margin: 10px 0 0 0;
        }

        .result
        {
            margin-left: 45%;
            font-weight: bold;
        }

        #pnlData
        {
            width: 70%;
        }


        thead
        {
            border: solid 1px #ebeef5;
        }
</style>
 </asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager2" runat="server" AsyncPostBackTimeout="360" EnableHistory="true" EnablePartialRendering="true" EnablePageMethods="true"></asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="crumbs">
                <!-- Start : Breadcrumbs -->
                <ul id="Ul2" class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a>Reports</a>
                    </li>
                    <li>Reviews</li>
                    <li class="current">CUSTOMER Review</li>
                    <div>
                        <ul>
                            <li class="title_bedcrum" style="list-style: none;">CUSTOMER REVIEW</li>
                        </ul>
                    </div>
                </ul>
            </div>
            <div id="collapsebtn" class="row">
                <img id="product_image" src="images/up_arrow.png"  style="margin-left: 46%;" />
            </div>
            <div id="dfltreportform" class="row filter_panel">
                <div class="col-md-12" runat="server" id="cterDiv">
                    <ul id="divCter" runat="server" class="btn-info rbtn_panel">
                        <li><span style="margin-right: 4px; vertical-align: text-bottom;">TAEGUTEC</span>
                            <asp:RadioButton ID="rdBtnTaegutec" AutoPostBack="true" Checked="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn"  runat="server" />
                            <span style="margin-right: 4px; margin-left: 4px; vertical-align: text-bottom;">DURACARB</span>
                            <asp:RadioButton ID="rdBtnDuraCab" AutoPostBack="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                        </li>
                    </ul>
                </div>
           
                    <div class="col-md-2 control" runat="server" id="divyear">
                            <label class="label">YEAR</label>                         
                                <asp:DropDownList ID="txtYear" class="control_dropdown"  runat="server"></asp:DropDownList>                
                    </div>

                    <div class="col-md-2 control" runat="server" id="divBranch">                      
                            <label class="label">BRANCH</label>                           
                                <asp:DropDownList ID="ddlBranchList" runat="server" OnSelectedIndexChanged="ddlBranchList_SelectedIndexChanged"  CssClass="control_dropdown" AutoPostBack="true"></asp:DropDownList>        
                    </div>

                    <div class="col-md-2 control" runat="server" id="divSE">
                            <label class="label">SALES ENGINEER</label>                         
                                <asp:DropDownList ID="ddlSalesEngineerList" runat="server" OnSelectedIndexChanged="SalesEngList_SelectedIndexChanged"  CssClass="control_dropdown" AutoPostBack="true"></asp:DropDownList>                             
                    </div>
              
                    <div class="col-md-2 control" runat="server" id="divCustomer">
                            <label class="label">CUSTOMER</label>                         
                                <asp:DropDownList ID="ddlCustomerList" runat="server"  CssClass="control_dropdown" ></asp:DropDownList>                             
                    </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <asp:Label ID="lblError" runat="server"></asp:Label>
                        <br />
                        <asp:Button ID="reports" runat="server" Text="FILTER" OnClick="reports_Click" CssClass="btn green" />
                        <asp:Button ID="export" runat="server" Text="EXPORT" OnClick="export_Click"  CssClass="btn green" />
                    </div>
                </div>
            </div>
             <div>
                <br />
                <asp:Label runat="server" ID="lblResult"></asp:Label>
                <asp:GridView ID="custargetvssales" Width="98%" Style="text-align: center;" runat="server" AutoGenerateColumns="False" ShowHeaderWhenEmpty="True">
                    <Columns>                      
                        <asp:TemplateField HeaderText="BRANCH" HeaderStyle-CssClass="HeadergridAll" Visible="true" ItemStyle-Width="125px" ItemStyle-CssClass="align-left">
                            <ItemTemplate>
                                <asp:Label Style="text-align: left;" ID="Labelbranch" runat="server" Text='<%# Eval("BRANCH") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="CUSTOMER NUMBER" HeaderStyle-CssClass="HeadergridAll" Visible="true" ItemStyle-Width="125px" ItemStyle-CssClass="align-left">
                            <ItemTemplate>
                                <asp:Label Style="text-align: left;" ID="Labelcustno" runat="server" Text='<%# Eval("customer_number") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="CUSTOMER NAME" HeaderStyle-CssClass="HeadergridAll" Visible="true" ItemStyle-Width="125px" ItemStyle-CssClass="align-left">
                            <ItemTemplate>
                                <asp:Label Style="text-align: right;" ID="Labelcustname" runat="server" Text='<%# Eval("customer_name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="SE NAME" HeaderStyle-CssClass="HeadergridAll" Visible="true" ItemStyle-Width="125px" ItemStyle-CssClass="align-left">
                            <ItemTemplate>
                                <asp:Label Style="text-align: right;" ID="Labelcustname" runat="server" Text='<%# Eval("SE_NAME") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ANNUAL BUDGET" HeaderStyle-CssClass="HeadergridAll" Visible="true" ItemStyle-Width="125px" ItemStyle-CssClass="align-center">
                            <ItemTemplate>
                                <asp:Label Style="text-align: right;" ID="Labelannual" runat="server" Text='<%# Eval("Annual_Budget") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="JAN SALES" HeaderStyle-CssClass="HeadergridAll" Visible="true" ItemStyle-Width="125px" ItemStyle-CssClass="align-center">
                            <ItemTemplate>
                                <asp:Label Style="text-align: right;" ID="Labeljan" runat="server" Text='<%# Eval("Jan_Sales") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="FEB SALES" HeaderStyle-CssClass="HeadergridAll" Visible="true" ItemStyle-Width="125px" ItemStyle-CssClass="align-center">
                            <ItemTemplate>
                                <asp:Label Style="text-align: right;" ID="Labelfeb" runat="server" Text='<%# Eval("Feb_Sales") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="MAR SALES" HeaderStyle-CssClass="HeadergridAll" Visible="true" ItemStyle-Width="125px" ItemStyle-CssClass="align-center">
                            <ItemTemplate>
                                <asp:Label Style="text-align: right;" ID="Labelmar" runat="server" Text='<%# Eval("Mar_Sales") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="APR SALES" HeaderStyle-CssClass="HeadergridAll" Visible="true" ItemStyle-Width="125px" ItemStyle-CssClass="align-center">
                            <ItemTemplate>
                                <asp:Label Style="text-align: right;" ID="Labelapr" runat="server" Text='<%# Eval("Apr_Sales") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="MAY SALES" HeaderStyle-CssClass="HeadergridAll" Visible="true" ItemStyle-Width="125px" ItemStyle-CssClass="align-center">
                            <ItemTemplate>
                                <asp:Label Style="text-align: right;" ID="Labelmar" runat="server" Text='<%# Eval("May_Sales") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="JUN SALES" HeaderStyle-CssClass="HeadergridAll" Visible="true" ItemStyle-Width="125px" ItemStyle-CssClass="align-center">
                            <ItemTemplate>
                                <asp:Label Style="text-align: right;" ID="Labeljun" runat="server" Text='<%# Eval("Jun_Sales") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="JUL SALES" HeaderStyle-CssClass="HeadergridAll" Visible="true" ItemStyle-Width="125px" ItemStyle-CssClass="align-center">
                            <ItemTemplate>
                                <asp:Label Style="text-align: right;" ID="Labeljul" runat="server" Text='<%# Eval("Jul_Sales") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="AUG SALES" HeaderStyle-CssClass="HeadergridAll" Visible="true" ItemStyle-Width="125px" ItemStyle-CssClass="align-center">
                            <ItemTemplate>
                                <asp:Label Style="text-align: right;" ID="Labelaug" runat="server" Text='<%# Eval("Aug_Sales") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="SEP SALES" HeaderStyle-CssClass="HeadergridAll" Visible="true" ItemStyle-Width="125px" ItemStyle-CssClass="align-center">
                            <ItemTemplate>
                                <asp:Label Style="text-align: right;" ID="Labelsep" runat="server" Text='<%# Eval("Sep_Sales") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="OCT SALES" HeaderStyle-CssClass="HeadergridAll" Visible="true" ItemStyle-Width="125px" ItemStyle-CssClass="align-center">
                            <ItemTemplate>
                                <asp:Label Style="text-align: right;" ID="Labeloct" runat="server" Text='<%# Eval("Oct_Sales") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="NOV SALES" HeaderStyle-CssClass="HeadergridAll" Visible="true" ItemStyle-Width="125px" ItemStyle-CssClass="align-center">
                            <ItemTemplate>
                                <asp:Label Style="text-align: right;" ID="Labelnov" runat="server" Text='<%# Eval("Nov_Sales") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="DEC SALES" HeaderStyle-CssClass="HeadergridAll" Visible="true" ItemStyle-Width="125px" ItemStyle-CssClass="align-center">
                            <ItemTemplate>
                                <asp:Label Style="text-align: right;" ID="Labeldec" runat="server" Text='<%# Eval("Dec_Sales") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TOTAL SALES" HeaderStyle-CssClass="HeadergridAll" Visible="true" ItemStyle-Width="125px" ItemStyle-CssClass="align-center">
                            <ItemTemplate>
                                <asp:Label Style="text-align: right;" ID="Labeltotal" runat="server" Text='<%# Eval("Total_Sales") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ACHIEVED" HeaderStyle-CssClass="HeadergridAll" Visible="true" ItemStyle-Width="125px" ItemStyle-CssClass="align-center">
                            <ItemTemplate>
                                <asp:Label Style="text-align: right;" ID="Labelach" runat="server" Text='<%# Eval("Achieved") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            <asp:HiddenField ID="hdnannual" runat="server" />
            <asp:HiddenField ID="hdnjan" runat="server" />
            <asp:HiddenField ID="hdnfeb" runat="server" />
            <asp:HiddenField ID="hdnmar" runat="server" />
            <asp:HiddenField ID="hdnapr" runat="server" />
            <asp:HiddenField ID="hdnmay" runat="server" />
            <asp:HiddenField ID="hdnjun" runat="server" />
            <asp:HiddenField ID="hdnjul" runat="server" />
            <asp:HiddenField ID="hdnaug" runat="server" />
            <asp:HiddenField ID="hdnsep" runat="server" />
            <asp:HiddenField ID="hdnoct" runat="server" />
            <asp:HiddenField ID="hdnnov" runat="server" />
            <asp:HiddenField ID="hdndec" runat="server" />
            <asp:HiddenField ID="hdntot" runat="server" />
            <asp:HiddenField ID="hdnach" runat="server" />
            <asp:HiddenField ID="hdnsearch" runat="server"/>
            </ContentTemplate>
        <Triggers>
           <asp:AsyncPostBackTrigger ControlID="reports" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="rdBtnTaegutec" EventName="CheckedChanged" />
            <asp:AsyncPostBackTrigger ControlID="rdBtnDuraCab" EventName="CheckedChanged" />
            <asp:PostBackTrigger ControlID="export" />
            <%--<asp:PostBackTrigger ControlID="reports" />--%>
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff">Please wait</span>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
</asp:Content>
