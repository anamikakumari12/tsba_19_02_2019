﻿$(document).ready(function () {
    PageLoadScripts();

});
 
$(window).load(function () {
    var tab_number = $("#MainContent_tab_number").val();
    LoadNextStage(tab_number);
});

function HideStatus() {
    $(".tab1_hideout").hide();
    $(".tab1_monthly").hide();
    $(".tab2_hideout").hide();
    $(".tab2_monthly").hide();
    $(".tab3_hideout").hide();
    $(".tab3_monthly").hide();
    $(".tab4_hideout").hide();
    $(".tab4_monthly").hide();
    $(".tab5_hideout").hide();
    $(".tab5_monthly").hide();
    $(".tab6_hideout").hide();
    $(".tab6_monthly").hide();
    $(".tab7_hideout").hide();
    $(".tab7_monthly").hide();
    $(".tab8_hideout").hide();
    $(".tab8_monthly").hide();
    $(".tab9_hideout").hide();
    $(".tab9_monthly").hide();
    $(".tab10_hideout").hide();
    $(".tab10_monthly").hide();

    $(".tab1_hideout_every").hide();
    $(".tab2_hideout_every").hide();
    $(".tab3_hideout_every").hide();
    $(".tab4_hideout_every").hide();
    $(".tab5_hideout_every").hide();
    $(".tab6_hideout_every").hide();
    $(".tab7_hideout_every").hide();
    $(".tab8_hideout_every").hide();
    $(".tab9_hideout_every").hide();
    $(".tab10_hideout_every").hide();

}

function ShowHideStages() {
    var stageselected = $("#MainContent_Stages").val();

    $('a[href*=tab_1_1]').hide();
    $('a[href*=tab_1_2]').hide();
    $('a[href*=tab_1_3]').hide();
    $('a[href*=tab_1_4]').hide();
    $('a[href*=tab_1_5]').hide();
    $('a[href*=tab_1_6]').hide();
    $('a[href*=tab_1_7]').hide();
    $('a[href*=tab_1_8]').hide();
    $('a[href*=tab_1_9]').hide();
    $('a[href*=tab_1_10]').hide();

    if (stageselected == "1" || stageselected == "--SELECT STAGES--") {
        $('a[href*=tab_1_1]').show();
        $('a[href*=tab_1_10]').hide();
    }
    else if (stageselected == "2") {
        $('a[href*=tab_1_1]').show();
        $('a[href*=tab_1_2]').show();
        $('a[href*=tab_1_10]').hide();
    }
    else if (stageselected == "3") {
        $('a[href*=tab_1_1]').show();
        $('a[href*=tab_1_2]').show();
        $('a[href*=tab_1_3]').show();
        $('a[href*=tab_1_10]').hide();
    }
    else if (stageselected == "4") {
        $('a[href*=tab_1_1]').show();
        $('a[href*=tab_1_2]').show();
        $('a[href*=tab_1_3]').show();
        $('a[href*=tab_1_4]').show();
        $('a[href*=tab_1_10]').hide();
    }
    else if (stageselected == "5") {
        $('a[href*=tab_1_1]').show();
        $('a[href*=tab_1_2]').show();
        $('a[href*=tab_1_3]').show();
        $('a[href*=tab_1_4]').show();
        $('a[href*=tab_1_5]').show();
        $('a[href*=tab_1_10]').hide();
    }
    else if (stageselected == "6") {
        $('a[href*=tab_1_1]').show();
        $('a[href*=tab_1_2]').show();
        $('a[href*=tab_1_3]').show();
        $('a[href*=tab_1_4]').show();
        $('a[href*=tab_1_5]').show();
        $('a[href*=tab_1_6]').show();
        $('a[href*=tab_1_10]').hide();
    }
    else if (stageselected == "7") {
        $('a[href*=tab_1_1]').show();
        $('a[href*=tab_1_2]').show();
        $('a[href*=tab_1_3]').show();
        $('a[href*=tab_1_4]').show();
        $('a[href*=tab_1_5]').show();
        $('a[href*=tab_1_6]').show();
        $('a[href*=tab_1_7]').show();
        $('a[href*=tab_1_10]').hide();
    }
    else if (stageselected == "8") {
        $('a[href*=tab_1_1]').show();
        $('a[href*=tab_1_2]').show();
        $('a[href*=tab_1_3]').show();
        $('a[href*=tab_1_4]').show();
        $('a[href*=tab_1_5]').show();
        $('a[href*=tab_1_6]').show();
        $('a[href*=tab_1_7]').show();
        $('a[href*=tab_1_8]').show();
        $('a[href*=tab_1_10]').hide();
    }
    else if (stageselected == "9") {
        $('a[href*=tab_1_1]').show();
        $('a[href*=tab_1_2]').show();
        $('a[href*=tab_1_3]').show();
        $('a[href*=tab_1_4]').show();
        $('a[href*=tab_1_5]').show();
        $('a[href*=tab_1_6]').show();
        $('a[href*=tab_1_7]').show();
        $('a[href*=tab_1_8]').show();
        $('a[href*=tab_1_9]').show();
        $('a[href*=tab_1_10]').hide();
    }
    else if (stageselected == "10") {
        $('a[href*=tab_1_1]').show();
        $('a[href*=tab_1_2]').show();
        $('a[href*=tab_1_3]').show();
        $('a[href*=tab_1_4]').show();
        $('a[href*=tab_1_5]').show();
        $('a[href*=tab_1_6]').show();
        $('a[href*=tab_1_7]').show();
        $('a[href*=tab_1_8]').show();
        $('a[href*=tab_1_9]').show();
        $('a[href*=tab_1_10]').show();
    }
    DisableTabs();

}

function BindCalendar() {
    $("#MainContent_TargetDate").datepicker({
        showOn: "both",
        buttonImageOnly: true,
        buttonText: "",
        changeYear: true,
        changeMonth: true,
        yearRange: "c-60:c+60",
        minDate: new Date(),
        dateFormat: 'dd/mm/yy',
        buttonImage: "images/calander_icon.png",
    });
    $("#MainContent_tab1Target").datepicker({
        showOn: "both",
        buttonImageOnly: true,
        buttonText: "",
        changeYear: true,
        changeMonth: true,
        yearRange: "c-60:c+60",
        minDate: new Date(),
        dateFormat: 'dd/mm/yy',
        buttonImage: "images/calander_icon.png",
    });
    $("#MainContent_tab2Target").datepicker({
        showOn: "both",
        buttonImageOnly: true,
        buttonText: "",
        changeYear: true,
        changeMonth: true,
        yearRange: "c-60:c+60",
        minDate: new Date(),
        dateFormat: 'dd/mm/yy',
        buttonImage: "images/calander_icon.png",
    });
    $("#MainContent_tab3Target").datepicker({
        showOn: "both",
        buttonImageOnly: true,
        buttonText: "",
        changeYear: true,
        changeMonth: true,
        yearRange: "c-60:c+60",
        minDate: new Date(),
        dateFormat: 'dd/mm/yy',
        buttonImage: "images/calander_icon.png",
    });
    $("#MainContent_tab4Target").datepicker({
        showOn: "both",
        buttonImageOnly: true,
        buttonText: "",
        changeYear: true,
        changeMonth: true,
        yearRange: "c-60:c+60",
        minDate: new Date(),
        dateFormat: 'dd/mm/yy',
        buttonImage: "images/calander_icon.png",
    });
    $("#MainContent_tab5Target").datepicker({
        showOn: "both",
        buttonImageOnly: true,
        buttonText: "",
        changeYear: true,
        changeMonth: true,
        yearRange: "c-60:c+60",
        minDate: new Date(),
        dateFormat: 'dd/mm/yy',
        buttonImage: "images/calander_icon.png",
    });
    $("#MainContent_tab6Target").datepicker({
        showOn: "both",
        buttonImageOnly: true,
        buttonText: "",
        changeYear: true,
        changeMonth: true,
        yearRange: "c-60:c+60",
        minDate: new Date(),
        dateFormat: 'dd/mm/yy',
        buttonImage: "images/calander_icon.png",
    });
    $("#MainContent_tab7Target").datepicker({
        showOn: "both",
        buttonImageOnly: true,
        buttonText: "",
        changeYear: true,
        changeMonth: true,
        yearRange: "c-60:c+60",
        minDate: new Date(),
        dateFormat: 'dd/mm/yy',
        buttonImage: "images/calander_icon.png",
    });
    $("#MainContent_tab8Target").datepicker({
        showOn: "both",
        buttonImageOnly: true,
        buttonText: "",
        changeYear: true,
        changeMonth: true,
        yearRange: "c-60:c+60",
        minDate: new Date(),
        dateFormat: 'dd/mm/yy',
        buttonImage: "images/calander_icon.png",
    });
    $("#MainContent_tab9Target").datepicker({
        showOn: "both",
        buttonImageOnly: true,
        buttonText: "",
        changeYear: true,
        changeMonth: true,
        yearRange: "c-60:c+60",
        minDate: new Date(),
        dateFormat: 'dd/mm/yy',
        buttonImage: "images/calander_icon.png",
    });
    $("#MainContent_tab10Target").datepicker({
        showOn: "both",
        buttonImageOnly: true,
        buttonText: "",
        changeYear: true,
        changeMonth: true,
        yearRange: "c-60:c+60",
        minDate: new Date(),
        dateFormat: 'dd/mm/yy',
        buttonImage: "images/calander_icon.png",
    });

}

function LoadNextStage(tab_num) {
    if (tab_num == '2') {
        $('a[href*=tab_1_2]').click();
    }
    if (tab_num == '3') {
        $('a[href*=tab_1_3]').click();
    }
    if (tab_num == '4') {
        $('a[href*=tab_1_4]').click();
    }
    if (tab_num == '5') {
        $('a[href*=tab_1_5]').click();
    }
    if (tab_num == '6') {
        $('a[href*=tab_1_6]').click();
    }
    if (tab_num == '7') {
        $('a[href*=tab_1_7]').click();
    }
    if (tab_num == '8') {
        $('a[href*=tab_1_8]').click();
    }
    if (tab_num == '9') {
        $('a[href*=tab_1_9]').click();
    }
    if (tab_num == '10') {
        $('a[href*=tab_1_10]').click();
    }
}

function AllowNumericData() {
    $("#MainContent_OverAllPotential").keydown(function (event) {

        // Allow: backspace, delete, tab, escape, and enter
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
            // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)
            //Allow Decimal
            || (event.keyCode == 190) || (event.keyCode == 110)
            ) {

            $(".lblOverAllPotential").hide();

            if ($.trim($("#MainContent_OverAllPotential").val()) == "") {

                $(".lblOverAllPotential").show();

            }
            // let it happen, don't do anything
            return;
        }
        else {

            // Ensure that it is a number and stop the keypress
            $(".lblOverAllPotential").hide();
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {

                event.preventDefault();
                $(".lblOverAllPotential").show();
                if ($.trim($("#MainContent_OverAllPotential").val()) == "") {

                    $(".lblOverAllPotential").show();

                }
            }
        }
    });

    $("#MainContent_Potential").keydown(function (event) {

        // Allow: backspace, delete, tab, escape, and enter
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
            // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)
            //Allow Decimal
            || (event.keyCode == 190) || (event.keyCode == 110)
            ) {

            $(".lblPotentialLakhs").hide();

            if ($.trim($("#MainContent_Potential").val()) == "") {

                $(".lblPotentialLakhs").show();

            }
            // let it happen, don't do anything
            return;
        }
        else {

            // Ensure that it is a number and stop the keypress
            $(".lblPotentialLakhs").hide();
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {

                event.preventDefault();
                $(".lblPotentialLakhs").show();
                if ($.trim($("#MainContent_Potential").val()) == "") {

                    $(".lblPotentialLakhs").show();

                }
            }
        }
    });

    $("#MainContent_Business").keydown(function (event) {

        // Allow: backspace, delete, tab, escape, and enter
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
            // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)
            //Allow Decimal
            || (event.keyCode == 190) || (event.keyCode == 110)
            ) {

            $(".lblBusinessExpected").hide();

            if ($.trim($("#MainContent_Business").val()) == "") {

                $(".lblBusinessExpected").show();

            }
            // let it happen, don't do anything
            return;
        }
        else {

            // Ensure that it is a number and stop the keypress
            $(".lblBusinessExpected").hide();
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {

                event.preventDefault();
                $(".lblBusinessExpected").show();
                if ($.trim($("#MainContent_Business").val()) == "") {

                    $(".lblBusinessExpected").show();

                }
            }
        }
    });

    $("#MainContent_Business").keydown(function (event) {

        // Allow: backspace, delete, tab, escape, and enter
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
            // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)
            //Allow Decimal
            || (event.keyCode == 190) || (event.keyCode == 110)
            ) {

            $(".lblBusinessExpected").hide();

            if ($.trim($("#MainContent_Business").val()) == "") {

                $(".lblBusinessExpected").show();

            }
            // let it happen, don't do anything
            return;
        }
        else {

            // Ensure that it is a number and stop the keypress
            $(".lblBusinessExpected").hide();
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {

                event.preventDefault();
                $(".lblBusinessExpected").show();
                if ($.trim($("#MainContent_Business").val()) == "") {

                    $(".lblBusinessExpected").show();

                }
            }
        }
    });

    $("#MainContent_tab1_monthly_expected").keydown(function (event) {
        // Allow: backspace, delete, tab, escape, and enter
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
            // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)
            //Allow Decimal
            || (event.keyCode == 190) || (event.keyCode == 110)
            ) {

            $(".lbltab1_monthly").hide();

            if ($.trim($("#MainContent_tab1_monthly_expected").val()) == "") {

                $(".lbltab1_monthly").show();

            }
            // let it happen, don't do anything
            return;
        }
        else {

            // Ensure that it is a number and stop the keypress
            $(".lbltab1_monthly").hide();
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {

                event.preventDefault();
                $(".lbltab1_monthly").show();
                if ($.trim($("#MainContent_tab1_monthly_expected").val()) == "") {

                    $(".lbltab1_monthly").show();

                }
            }
        }
    });

    $("#MainContent_tab2_monthly_expected").keydown(function (event) {
        // Allow: backspace, delete, tab, escape, and enter
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
            // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)
            //Allow Decimal
            || (event.keyCode == 190) || (event.keyCode==110)
            ) {

            $(".lbltab2_monthly").hide();

            if ($.trim($("#MainContent_tab2_monthly_expected").val()) == "") {

                $(".lbltab2_monthly").show();

            }
            // let it happen, don't do anything
            return;
        }
        else {

            // Ensure that it is a number and stop the keypress
            $(".lbltab2_monthly").hide();
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {

                event.preventDefault();
                $(".lbltab1_monthly").show();
                if ($.trim($("#MainContent_tab2_monthly_expected").val()) == "") {

                    $(".lbltab2_monthly").show();

                }
            }
        }
    });

    $("#MainContent_tab3_monthly_expected").keydown(function (event) {

        // Allow: backspace, delete, tab, escape, and enter
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
            // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)
            //Allow Decimal
            || (event.keyCode == 190) || (event.keyCode==110)
            ) {

            $(".lbltab3_monthly").hide();

            if ($.trim($("#MainContent_tab3_monthly_expected").val()) == "") {

                $(".lbltab3_monthly").show();

            }
            // let it happen, don't do anything
            return;
        }
        else {

            // Ensure that it is a number and stop the keypress
            $(".lbltab3_monthly").hide();
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {

                event.preventDefault();
                $(".lbltab3_monthly").show();
                if ($.trim($("#MainContent_tab3_monthly_expected").val()) == "") {

                    $(".lbltab3_monthly").show();

                }
            }
        }
    });

    $("#MainContent_tab4_monthly_expected").keydown(function (event) {

        // Allow: backspace, delete, tab, escape, and enter
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
            // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)
            //Allow Decimal
            || (event.keyCode == 190) || (event.keyCode==110)
            ) {

            $(".lbltab4_monthly").hide();

            if ($.trim($("#MainContent_tab4_monthly_expected").val()) == "") {

                $(".lbltab4_monthly").show();

            }
            // let it happen, don't do anything
            return;
        }
        else {

            // Ensure that it is a number and stop the keypress
            $(".lbltab4_monthly").hide();
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {

                event.preventDefault();
                $(".lbltab1_monthly").show();
                if ($.trim($("#MainContent_tab4_monthly_expected").val()) == "") {

                    $(".lbltab4_monthly").show();

                }
            }
        }
    });

    $("#MainContent_tab5_monthly_expected").keydown(function (event) {

        // Allow: backspace, delete, tab, escape, and enter
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
            // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)
            //Allow Decimal
            || (event.keyCode == 190) || (event.keyCode==110)
            ) {

            $(".lbltab5_monthly").hide();

            if ($.trim($("#MainContent_tab5_monthly_expected").val()) == "") {

                $(".lbltab5_monthly").show();

            }
            // let it happen, don't do anything
            return;
        }
        else {

            // Ensure that it is a number and stop the keypress
            $(".lbltab5_monthly").hide();
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {

                event.preventDefault();
                $(".lbltab1_monthly").show();
                if ($.trim($("#MainContent_tab5_monthly_expected").val()) == "") {

                    $(".lbltab5_monthly").show();

                }
            }
        }
    });

    $("#MainContent_tab6_monthly_expected").keydown(function (event) {

        // Allow: backspace, delete, tab, escape, and enter
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
            // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)
            //Allow Decimal
            || (event.keyCode == 190) || (event.keyCode==110)
            ) {

            $(".lbltab6_monthly").hide();

            if ($.trim($("#MainContent_tab6_monthly_expected").val()) == "") {

                $(".lbltab6_monthly").show();

            }
            // let it happen, don't do anything
            return;
        }
        else {

            // Ensure that it is a number and stop the keypress
            $(".lbltab6_monthly").hide();
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {

                event.preventDefault();
                $(".lbltab6_monthly").show();
                if ($.trim($("#MainContent_tab6_monthly_expected").val()) == "") {

                    $(".lbltab6_monthly").show();

                }
            }
        }
    });

    $("#MainContent_tab7_monthly_expected").keydown(function (event) {

        // Allow: backspace, delete, tab, escape, and enter
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
            // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)
            //Allow Decimal
            || (event.keyCode == 190) || (event.keyCode==110)
            ) {

            $(".lbltab7_monthly").hide();

            if ($.trim($("#MainContent_tab7_monthly_expected").val()) == "") {

                $(".lbltab7_monthly").show();

            }
            // let it happen, don't do anything
            return;
        }
        else {

            // Ensure that it is a number and stop the keypress
            $(".lbltab7_monthly").hide();
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {

                event.preventDefault();
                $(".lbltab7_monthly").show();
                if ($.trim($("#MainContent_tab7_monthly_expected").val()) == "") {

                    $(".lbltab7_monthly").show();

                }
            }
        }
    });

    $("#MainContent_tab8_monthly_expected").keydown(function (event) {

        // Allow: backspace, delete, tab, escape, and enter
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
            // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)
            //Allow Decimal
            || (event.keyCode == 190) || (event.keyCode==110)
            ) {

            $(".lbltab8_monthly").hide();

            if ($.trim($("#MainContent_tab8_monthly_expected").val()) == "") {

                $(".lbltab8_monthly").show();

            }
            // let it happen, don't do anything
            return;
        }
        else {

            // Ensure that it is a number and stop the keypress
            $(".lbltab8_monthly").hide();
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {

                event.preventDefault();
                $(".lbltab8_monthly").show();
                if ($.trim($("#MainContent_tab8_monthly_expected").val()) == "") {

                    $(".lbltab8_monthly").show();

                }
            }
        }
    });

    $("#MainContent_tab9_monthly_expected").keydown(function (event) {

        // Allow: backspace, delete, tab, escape, and enter
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
            // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)
            //Allow Decimal
            || (event.keyCode == 190) || (event.keyCode==110)
            ) {

            $(".lbltab9_monthly").hide();

            if ($.trim($("#MainContent_tab9_monthly_expected").val()) == "") {

                $(".lbltab9_monthly").show();

            }
            // let it happen, don't do anything
            return;
        }
        else {

            // Ensure that it is a number and stop the keypress
            $(".lbltab9_monthly").hide();
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {

                event.preventDefault();
                $(".lbltab9_monthly").show();
                if ($.trim($("#MainContent_tab9_monthly_expected").val()) == "") {

                    $(".lbltab9_monthly").show();

                }
            }
        }
    });

    $("#MainContent_tab10_monthly_expected").keydown(function (event) {

        // Allow: backspace, delete, tab, escape, and enter
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 ||
            // Allow: Ctrl+A
            (event.keyCode == 65 && event.ctrlKey === true) ||
            // Allow: home, end, left, right
            (event.keyCode >= 35 && event.keyCode <= 39)
            //Allow Decimal
            || (event.keyCode == 190) || (event.keyCode==110)
            ) {

            $(".lbltab10_monthly").hide();

            if ($.trim($("#MainContent_tab10_monthly_expected").val()) == "") {

                $(".lbltab10_monthly").show();

            }
            // let it happen, don't do anything
            return;
        }
        else {

            // Ensure that it is a number and stop the keypress
            $(".lbltab10_monthly").hide();
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {

                event.preventDefault();
                $(".lbltab10_monthly").show();
                if ($.trim($("#MainContent_tab10_monthly_expected").val()) == "") {

                    $(".lbltab10_monthly").show();

                }
            }
        }
    });
}

function ValidateTab1() {
    var exist = 0;
    var returnVal = true;
    var Target_Date = $("#MainContent_tab1Target").val();
    //var Escalated_To = $("#MainContent_ddlEscalate").val();
    //var Customer_name = $("#MainContent_ddlCustomerName").val();
    //var DistCustomer_name = $("MainContent_ddlCustomerforDistributor").val();
    //var Customer_num = $("#MainContent_ddlCustomerNo").val();

    //var Product = $("#MainContent_ExistingProduct").val();
    //var Component = $("#MainContent_Component").val();
    //var Competition = $("#MainContent_CompetitionSpec").val();
    //var Stages = $("#MainContent_Stages").val();
    //var Main_Target = $("#MainContent_TargetDate").val();
    //var OverAll_Potential = $("#MainContent_OverAllPotential").val();
    //var Pot_Lakhs = $("#MainContent_Potential").val();
    //var Business = $("#MainContent_Business").val();
    //var Industry = $("#MainContent_ddlIndustry").val();
    //var Owner = $("#MainContent_ddlSalesEngName").val();
    //var Reviewer = $("#MainContent_ddlReviewer").val();
    var finalstage = $("#MainContent_Stages").val();
    //var ProjectTitle = $("#MainContent_txtTitle").val();
    //var Existing_ProjectTitle = $("#MainContent_ddlTitle").val();
    //var Project_Type = $("#MainContent_ddlProjectType").val();   
    var Tab1_Goal = $("#MainContent_tab1txtGoal").val();

    debugger;
        DropdownValidation();
        if ($('#errDistcustname').text() != null && $('#errDistcustname').text() != "")
        {
            $('#MainContent_ddlCustomerforDistributor').css('border-color', 'red');
            returnVal = false;
        }
   
    if (finalstage == 1) {
        if ($.datepicker.parseDate('dd/mm/yy',$('#MainContent_TargetDate').val()) < $.datepicker.parseDate('dd/mm/yy',$("#MainContent_tab1Target").val())) {
            alert("Final Stage Target Date can not be after Project Target Date.");
            return false;
        }
    }
    returnVal = checkNullForCommonFields();
    //if ($('#MainContent_new_project').is(':checked'))
    //{
    //    if (ProjectTitle == "" || ProjectTitle == "-- NO PROJECT --" || ProjectTitle == "-- SELECT PROJECT --") {
    //        $('#MainContent_txtTitle').css('border-color', 'red');
    //        returnVal = false;
    //    }
    //}
    //if ($('#MainContent_existing_project').is(':checked'))
    //{        
    //    if (Existing_ProjectTitle == "-- NO PROJECT --" | Existing_ProjectTitle == "-- SELECT PROJECT --")
    //    {
    //        $(".lblProduct").show();
    //        returnVal = false;
    //        alert('You are trying to create new project.For this please click on new project radio button.');
    //        return returnVal;
    //    }
    //}
    

    //if (Project_Type == "-- SELECT --") {

    //    $('#MainContent_ddlProjectType').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {

    //    $('#MainContent_ddlProjectType').css('border-color', '');

    //}

    //if (Owner == "-- SELECT --") {

    //    $('#MainContent_ddlSalesEngName').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {

    //    $('#MainContent_ddlSalesEngName').css('border-color', '');

    //}

    //if (Reviewer == "-- SELECT --") {
    //    $('#MainContent_ddlReviewer').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {

    //    $('#MainContent_ddlReviewer').css('border-color', '');

    //}
    //if (Product == "") {
    //    $(".lblProduct").show();
    //    returnVal = false;
    //    $('#MainContent_ExistingProduct').css('border-color', 'red');
    //}
    //else {
    //    $(".lblProduct").hide();
    //    $('#MainContent_ExistingProduct').css('border-color', '');
    //}

    //if (Component == "") {
    //    $(".lblComponent").show();
    //    returnVal = false;
    //    $('#MainContent_Component').css('border-color', 'red');
    //}
    //else {
    //    $(".lblComponent").hide();
    //    $('#MainContent_Component').css('border-color', '');

    //}
    //if (Competition == "") {
    //    $(".lblCompetition").show();
    //    returnVal = false;
    //    $('#MainContent_CompetitionSpec').css('border-color', 'red');
    //}
    //else {
    //    $(".lblCompetition").hide();
    //    $('#MainContent_CompetitionSpec').css('border-color', '');

    //}
    //if (Stages == "--SELECT STAGES--") {
    //    $(".lblStages").show();
    //    returnVal = false;
    //    $('#MainContent_Stages').css('border-color', 'red');
    //}
    //else {
    //    $(".lblStages").hide();
    //    $('#MainContent_Stages').css('border-color', '');

    //}
    //if (Main_Target == "") {
    //    $(".lblTarget").show();
    //    $('#MainContent_TargetDate').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {
    //    $(".lblTarget").hide();
    //    $('#MainContent_TargetDate').css('border-color', '');
    //}
    //if (OverAll_Potential == "") {
    //    $(".lblover_Pot").show();
    //    returnVal = false;
    //    $('#MainContent_OverAllPotential').css('border-color', 'red');
    //}
    //else {
    //    $(".lblover_Pot").hide();
    //    $('#MainContent_OverAllPotential').css('border-color', '');
    //}

    //if (Pot_Lakhs == "") {
    //    $(".lblPot_Lakhs").show();
    //    returnVal = false;
    //    $('#MainContent_Potential').css('border-color', 'red');
    //}
    //else {
    //    $(".lblPot_Lakhs").hide();
    //    $('#MainContent_Potential').css('border-color', '');
    //}

    //if (Business == "") {
    //    $(".lblBusiness_Exp").show();
    //    $('#MainContent_Business').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {
    //    $(".lblBusiness_Exp").hide();
    //    $('#MainContent_Business').css('border-color', '');
    //}


    //if (Escalated_To == "-- SELECT --") {
    //    $(".lblEscalate").show();
    //    $('#MainContent_ddlEscalate').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {
    //    $(".lblEscalate").hide();
    //    $('#MainContent_ddlEscalate').css('border-color', '');

    //}

    //if (Industry == "-- SELECT --") {
    //    $(".lblIndustry").show();
    //    returnVal = false;
    //    $('#MainContent_ddlIndustry').css('border-color', 'red');
    //}
    //else {
    //    $(".lblIndustry").hide();
    //    $('#MainContent_ddlIndustry').css('border-color', '');
    //}

    if (Target_Date == "") {
        $(".lbltab1Target").show();
        $('#MainContent_tab1Target').css('border-color', 'red');
        returnVal = false;
    }
    else {
        $(".lbltab1Target").hide();
        $('#MainContent_tab1Target').css('border-color', '');
    }

    if (Tab1_Goal == "") {
        $(".lbltab1goal").show();
        $('#MainContent_tab1txtGoal').css('border-color', 'red');
        returnVal = false;
    }
    else {
        $(".lbltab1goal").hide();
        $('#MainContent_tab1txtGoal').css('border-color', '');
    }

    if (returnVal == true && finalstage == 1) {
        if (confirm("Once submitted fields can not be edited by the Owner. Do you want to continue?")) {
            return returnVal;
        }
        else {

            return false;
        }

    }
 
    if (returnVal == false) {
        alert("Fields marked in red are required");
    }
  
    else 
    {
        jQuery(function ($) {
            $("#tab_home").tabs({
                active: [0, 1],
                disabled: [2, 3, 4, 5, 6, 7, 8, 9]
            });
        });
    }
    return returnVal;
}

function ValidateTab2() {
    debugger;
    var returnVal = true;
    var Target_Date = $("#MainContent_tab2Target").val();
    //var Escalated_To = $("#MainContent_ddlEscalate").val();
    //var Customer_name = $("#MainContent_ddlCustomerName").val();
    //var Customer_num = $("#MainContent_ddlCustomerNo").val();
    //var Product = $("#MainContent_ExistingProduct").val();
    //var Component = $("#MainContent_Component").val();
    //var Competition = $("#MainContent_CompetitionSpec").val();
    //var Stages = $("#MainContent_Stages").val();
    //var Main_Target = $("#MainContent_TargetDate").val();
    //var OverAll_Potential = $("#MainContent_OverAllPotential").val();
    //var Pot_Lakhs = $("#MainContent_Potential").val();
    //var Business = $("#MainContent_Business").val();
    //var Industry = $("#MainContent_ddlIndustry").val();
    //var Owner = $("#MainContent_ddlSalesEngName").val();
    //var Reviewer = $("#MainContent_ddlReviewer").val();
    var Tab2_Goal = $("#MainContent_tab2txtGoal").val();
    var finalstage = $("#MainContent_Stages").val();
    //var ProjectTitle = $("#MainContent_txtTitle").val();
    //var Existing_ProjectTitle = $("#MainContent_ddlTitle").val();
    //var Project_Type = $("#MainContent_ddlProjectType").val();

    DropdownValidation();
    if (finalstage == 2) {
        if ($.datepicker.parseDate('dd/mm/yy',$('#MainContent_TargetDate').val()) < $.datepicker.parseDate('dd/mm/yy',$("#MainContent_tab2Target").val())) {
            alert("Final Stage Target Date can not be after Project Target Date.");
            return false;
        }
    }

    returnVal = checkNullForCommonFields();
    //if ($('#MainContent_new_project').is(':checked')) {
    //    if (ProjectTitle == "" || ProjectTitle == "-- NO PROJECT --" || ProjectTitle == "-- SELECT PROJECT --") {
    //        returnVal = false;
    //        $('#MainContent_txtTitle').css('border-color', 'red');
    //    }
    //}
    //if ($('#MainContent_existing_project').is(':checked')) {
    //    if (Existing_ProjectTitle == "-- NO PROJECT --" | Existing_ProjectTitle == "-- SELECT PROJECT --") {
    //        $(".lblProduct").show();
    //        returnVal = false;
    //        alert('You are trying to create new project.For this please click on new project radio button.');
    //        return returnVal;
    //    }
    //}
    //if (Project_Type == "-- SELECT --") {

    //    $('#MainContent_ddlProjectType').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {

    //    $('#MainContent_ddlProjectType').css('border-color', '');

    //}
    //if (Owner == "-- SELECT --") {
    //    $('#MainContent_ddlSalesEngName').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {
    //    $('#MainContent_ddlSalesEngName').css('border-color', '');
    //}

    //if (Reviewer == "-- SELECT --") {

    //    $('#MainContent_ddlReviewer').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {

    //    $('#MainContent_ddlReviewer').css('border-color', '');

    //}
    //if (Product == "") {
    //    $(".lblProduct").show();
    //    returnVal = false;
    //    $('#MainContent_ExistingProduct').css('border-color', 'red');
    //}
    //else {
    //    $(".lblProduct").hide();
    //    $('#MainContent_ExistingProduct').css('border-color', '');
    //}

    //if (Component == "") {
    //    $(".lblComponent").show();
    //    returnVal = false;
    //    $('#MainContent_Component').css('border-color', 'red');
    //}
    //else {
    //    $(".lblComponent").hide();
    //    $('#MainContent_Component').css('border-color', '');

    //}
    //if (Competition == "") {
    //    $(".lblCompetition").show();
    //    returnVal = false;
    //    $('#MainContent_CompetitionSpec').css('border-color', 'red');
    //}
    //else {
    //    $(".lblCompetition").hide();
    //    $('#MainContent_CompetitionSpec').css('border-color', '');

    //}
    //if (Stages == "--SELECT STAGES--") {
    //    $(".lblStages").show();
    //    returnVal = false;
    //    $('#MainContent_Stages').css('border-color', 'red');
    //}
    //else {
    //    $(".lblStages").hide();
    //    $('#MainContent_Stages').css('border-color', '');

    //}
    //if (Main_Target == "") {
    //    $(".lblTarget").show();
    //    $('#MainContent_TargetDate').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {
    //    $(".lblTarget").hide();
    //    $('#MainContent_TargetDate').css('border-color', '');
    //}
    //if (OverAll_Potential == "") {
    //    $(".lblover_Pot").show();
    //    returnVal = false;
    //    $('#MainContent_OverAllPotential').css('border-color', 'red');
    //}
    //else {
    //    $(".lblover_Pot").hide();
    //    $('#MainContent_OverAllPotential').css('border-color', '');
    //}

    //if (Pot_Lakhs == "") {
    //    $(".lblPot_Lakhs").show();
    //    returnVal = false;
    //    $('#MainContent_Potential').css('border-color', 'red');
    //}
    //else {
    //    $(".lblPot_Lakhs").hide();
    //    $('#MainContent_Potential').css('border-color', '');
    //}

    //if (Business == "") {
    //    $(".lblBusiness_Exp").show();
    //    $('#MainContent_Business').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {
    //    $(".lblBusiness_Exp").hide();
    //    $('#MainContent_Business').css('border-color', '');
    //}


    //if (Escalated_To == "-- SELECT --") {
    //    $(".lblEscalate").show();
    //    $('#MainContent_ddlEscalate').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {
    //    $(".lblEscalate").hide();
    //    $('#MainContent_ddlEscalate').css('border-color', '');

    //}

    //if (Industry == "-- SELECT --") {
    //    $(".lblIndustry").show();
    //    returnVal = false;
    //    $('#MainContent_ddlIndustry').css('border-color', 'red');
    //}
    //else {
    //    $(".lblIndustry").hide();
    //    $('#MainContent_ddlIndustry').css('border-color', '');


    //}

    if (Target_Date == "") {
        $(".lbltab2Target").show();
        $('#MainContent_tab2Target').css('border-color', 'red');
        returnVal = false;
    }
    else {
        $(".lbltab1Target").hide();
        $('#MainContent_tab2Target').css('border-color', '');
    }

    if (Tab2_Goal == "") {
        $(".lbltab2goal").show();
        $('#MainContent_tab2txtGoal').css('border-color', 'red');
        returnVal = false;
    }
    else {
        $(".lbltab2goal").hide();
        $('#MainContent_tab2txtGoal').css('border-color', '');
    }


    if (returnVal == true && finalstage == 2) {
        if (confirm("Once submitted fields can not be edited by the Owner. Do you want to continue?")) {
            return returnVal;
        }
        else {

            return false;
        }
    }

    if (returnVal == false) {
        alert("Fields marked in red are required");
    }
    else {
        jQuery(function ($) {
            $("#tab_home").tabs({
                active: [0, 1, 2],
                disabled: [3, 4, 5, 6, 7, 8, 9]
            });
        });
    }
    return returnVal;
}
function ValidateTab3() {
    var returnVal = true;
    var Target_Date = $("#MainContent_tab3Target").val();
    //var Escalated_To = $("#MainContent_ddlEscalate").val();
    //var Customer_name = $("#MainContent_ddlCustomerName").val();
    //var Customer_num = $("#MainContent_ddlCustomerNo").val();
    //var Product = $("#MainContent_ExistingProduct").val();
    //var Component = $("#MainContent_Component").val();
    //var Competition = $("#MainContent_CompetitionSpec").val();
    //var Stages = $("#MainContent_Stages").val();
    //var Main_Target = $("#MainContent_TargetDate").val();
    //var OverAll_Potential = $("#MainContent_OverAllPotential").val();
    //var Pot_Lakhs = $("#MainContent_Potential").val();
    //var Business = $("#MainContent_Business").val();
    //var Industry = $("#MainContent_ddlIndustry").val();
    //var Owner = $("#MainContent_ddlSalesEngName").val();
    //var Reviewer = $("#MainContent_ddlReviewer").val();
    var Tab3_Goal = $("#MainContent_tab3txtGoal").val();
    var finalstage = $("#MainContent_Stages").val();
    //var ProjectTitle = $("#MainContent_txtTitle").val();
    //var Existing_ProjectTitle = $("#MainContent_ddlTitle").val();
    //var Project_Type = $("#MainContent_ddlProjectType").val();

    DropdownValidation();
    if (finalstage == 3) {

            if ($.datepicker.parseDate('dd/mm/yy',$('#MainContent_TargetDate').val()) < $.datepicker.parseDate('dd/mm/yy',$("#MainContent_tab3Target").val())) {
            alert("Final Stage Target Date can not be after Project Target Date.");
            return false;
        }
    }
    returnVal = checkNullForCommonFields();
    //if ($('#MainContent_new_project').is(':checked')) {
    //    // debugger;
    //    if (ProjectTitle == "" || ProjectTitle == "-- NO PROJECT --" || ProjectTitle == "-- SELECT PROJECT --") {
    //        returnVal = false;
    //        $('#MainContent_txtTitle').css('border-color', 'red');
    //    }
    //}
    //if ($('#MainContent_existing_project').is(':checked')) {
    //    if (Existing_ProjectTitle == "-- NO PROJECT --" | Existing_ProjectTitle == "-- SELECT PROJECT --") {
    //        $(".lblProduct").show();
    //        returnVal = false;
    //        // alert('Oops Project Title is Invalid, Please Select New Project to enter the Project Name');
    //        alert('You are trying to create new project.For this please click on new project radio button.');
    //        return returnVal;
    //    }
    //}
    //if (Project_Type == "-- SELECT --") {

    //    $('#MainContent_ddlProjectType').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {

    //    $('#MainContent_ddlProjectType').css('border-color', '');

    //}
    //if (Owner == "-- SELECT --") {

    //    $('#MainContent_ddlSalesEngName').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {

    //    $('#MainContent_ddlSalesEngName').css('border-color', '');

    //}

    //if (Reviewer == "-- SELECT --") {

    //    $('#MainContent_ddlReviewer').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {

    //    $('#MainContent_ddlReviewer').css('border-color', '');

    //}
    //if (Product == "") {
    //    $(".lblProduct").show();
    //    returnVal = false;
    //    $('#MainContent_ExistingProduct').css('border-color', 'red');
    //}
    //else {
    //    $(".lblProduct").hide();
    //    $('#MainContent_ExistingProduct').css('border-color', '');
    //}

    //if (Component == "") {
    //    $(".lblComponent").show();
    //    returnVal = false;
    //    $('#MainContent_Component').css('border-color', 'red');
    //}
    //else {
    //    $(".lblComponent").hide();
    //    $('#MainContent_Component').css('border-color', '');

    //}
    //if (Competition == "") {
    //    $(".lblCompetition").show();
    //    returnVal = false;
    //    $('#MainContent_CompetitionSpec').css('border-color', 'red');
    //}
    //else {
    //    $(".lblCompetition").hide();
    //    $('#MainContent_CompetitionSpec').css('border-color', '');

    //}
    //if (Stages == "--SELECT STAGES--") {
    //    $(".lblStages").show();
    //    returnVal = false;
    //    $('#MainContent_Stages').css('border-color', 'red');
    //}
    //else {
    //    $(".lblStages").hide();
    //    $('#MainContent_Stages').css('border-color', '');

    //}
    //if (Main_Target == "") {
    //    $(".lblTarget").show();
    //    $('#MainContent_TargetDate').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {
    //    $(".lblTarget").hide();
    //    $('#MainContent_TargetDate').css('border-color', '');
    //}
    //if (OverAll_Potential == "") {
    //    $(".lblover_Pot").show();
    //    returnVal = false;
    //    $('#MainContent_OverAllPotential').css('border-color', 'red');
    //}
    //else {
    //    $(".lblover_Pot").hide();
    //    $('#MainContent_OverAllPotential').css('border-color', '');
    //}

    //if (Pot_Lakhs == "") {
    //    $(".lblPot_Lakhs").show();
    //    returnVal = false;
    //    $('#MainContent_Potential').css('border-color', 'red');
    //}
    //else {
    //    $(".lblPot_Lakhs").hide();
    //    $('#MainContent_Potential').css('border-color', '');
    //}

    //if (Business == "") {
    //    $(".lblBusiness_Exp").show();
    //    $('#MainContent_Business').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {
    //    $(".lblBusiness_Exp").hide();
    //    $('#MainContent_Business').css('border-color', '');
    //}


    //if (Escalated_To == "-- SELECT --") {
    //    $(".lblEscalate").show();
    //    $('#MainContent_ddlEscalate').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {
    //    $(".lblEscalate").hide();
    //    $('#MainContent_ddlEscalate').css('border-color', '');

    //}

    //if (Industry == "-- SELECT --") {
    //    $(".lblIndustry").show();
    //    returnVal = false;
    //    $('#MainContent_ddlIndustry').css('border-color', 'red');
    //}
    //else {
    //    $(".lblIndustry").hide();
    //    $('#MainContent_ddlIndustry').css('border-color', '');


    //}

    if (Target_Date == "") {
        $(".lbltab3Target").show();
        $('#MainContent_tab3Target').css('border-color', 'red');
        returnVal = false;
    }
    else {
        $(".lbltab3Target").hide();
        $('#MainContent_tab3Target').css('border-color', '');
    }

    if (Tab3_Goal == "") {
        $(".lbltab3goal").show();
        $('#MainContent_tab3txtGoal').css('border-color', 'red');
        returnVal = false;
    }
    else {
        $(".lbltab3goal").hide();
        $('#MainContent_tab3txtGoal').css('border-color', '');
    }

    if (returnVal == true && finalstage == 3) {
        if (confirm("Once submitted fields can not be edited by the Owner. Do you want to continue?")) {
            return returnVal;
        }
        else {

            return false;
        }
    }

    if (returnVal == false) {
        alert("Fields marked in red are required");
    }
    else {
        jQuery(function ($) {
            $("#tab_home").tabs({
                active: [0, 1, 2, 3],
                disabled: [4, 5, 6, 7, 8, 9]
            });
        });
    }
    return returnVal;
}
function ValidateTab4() {
    var returnVal = true;
    var Target_Date = $("#MainContent_tab4Target").val();
    //var Escalated_To = $("#MainContent_ddlEscalate").val();
    //var Customer_name = $("#MainContent_ddlCustomerName").val();
    //var Customer_num = $("#MainContent_ddlCustomerNo").val();
    //var Product = $("#MainContent_ExistingProduct").val();
    //var Component = $("#MainContent_Component").val();
    //var Competition = $("#MainContent_CompetitionSpec").val();
    //var Stages = $("#MainContent_Stages").val();
    //var Main_Target = $("#MainContent_TargetDate").val();
    //var OverAll_Potential = $("#MainContent_OverAllPotential").val();
    //var Pot_Lakhs = $("#MainContent_Potential").val();
    //var Business = $("#MainContent_Business").val();
    //var Industry = $("#MainContent_ddlIndustry").val();
    //var Owner = $("#MainContent_ddlSalesEngName").val();
    //var Reviewer = $("#MainContent_ddlReviewer").val();
    var Tab4_Goal = $("#MainContent_tab4txtGoal").val();
    var finalstage = $("#MainContent_Stages").val();
    //var ProjectTitle = $("#MainContent_txtTitle").val();
    //var Existing_ProjectTitle = $("#MainContent_ddlTitle").val();
    //var Project_Type = $("#MainContent_ddlProjectType").val();
    

    DropdownValidation();
    if (finalstage == 4) {

        if ($.datepicker.parseDate('dd/mm/yy',$('#MainContent_TargetDate').val()) < $.datepicker.parseDate('dd/mm/yy',$("#MainContent_tab4Target").val())) {
            alert("Final Stage Target Date can not be after Project Target Date.");
            return false;
        }
    }
    returnVal = checkNullForCommonFields();
    //if ($('#MainContent_new_project').is(':checked')) {
    //    // debugger;
    //    if (ProjectTitle == "" || ProjectTitle == "-- NO PROJECT --" || ProjectTitle == "-- SELECT PROJECT --") {
    //        returnVal = false;
    //        $('#MainContent_txtTitle').css('border-color', 'red');
    //    }
    //}
    //if ($('#MainContent_existing_project').is(':checked')) {
    //    if (Existing_ProjectTitle == "-- NO PROJECT --" | Existing_ProjectTitle == "-- SELECT PROJECT --") {
    //        $(".lblProduct").show();
    //        returnVal = false;
    //        // alert('Oops Project Title is Invalid, Please Select New Project to enter the Project Name');
    //        alert('You are trying to create new project.For this please click on new project radio button.');
    //        return returnVal;
    //    }
    //}
    //if (Project_Type == "-- SELECT --") {

    //    $('#MainContent_ddlProjectType').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {

    //    $('#MainContent_ddlProjectType').css('border-color', '');

    //}
    //if (Owner == "-- SELECT --") {

    //    $('#MainContent_ddlSalesEngName').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {

    //    $('#MainContent_ddlSalesEngName').css('border-color', '');

    //}

    //if (Reviewer == "-- SELECT --") {

    //    $('#MainContent_ddlReviewer').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {

    //    $('#MainContent_ddlReviewer').css('border-color', '');

    //}
    //if (Product == "") {
    //    $(".lblProduct").show();
    //    returnVal = false;
    //    $('#MainContent_ExistingProduct').css('border-color', 'red');
    //}
    //else {
    //    $(".lblProduct").hide();
    //    $('#MainContent_ExistingProduct').css('border-color', '');
    //}

    //if (Component == "") {
    //    $(".lblComponent").show();
    //    returnVal = false;
    //    $('#MainContent_Component').css('border-color', 'red');
    //}
    //else {
    //    $(".lblComponent").hide();
    //    $('#MainContent_Component').css('border-color', '');

    //}
    //if (Competition == "") {
    //    $(".lblCompetition").show();
    //    returnVal = false;
    //    $('#MainContent_CompetitionSpec').css('border-color', 'red');
    //}
    //else {
    //    $(".lblCompetition").hide();
    //    $('#MainContent_CompetitionSpec').css('border-color', '');

    //}
    //if (Stages == "--SELECT STAGES--") {
    //    $(".lblStages").show();
    //    returnVal = false;
    //    $('#MainContent_Stages').css('border-color', 'red');
    //}
    //else {
    //    $(".lblStages").hide();
    //    $('#MainContent_Stages').css('border-color', '');

    //}
    //if (Main_Target == "") {
    //    $(".lblTarget").show();
    //    $('#MainContent_TargetDate').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {
    //    $(".lblTarget").hide();
    //    $('#MainContent_TargetDate').css('border-color', '');
    //}
    //if (OverAll_Potential == "") {
    //    $(".lblover_Pot").show();
    //    returnVal = false;
    //    $('#MainContent_OverAllPotential').css('border-color', 'red');
    //}
    //else {
    //    $(".lblover_Pot").hide();
    //    $('#MainContent_OverAllPotential').css('border-color', '');
    //}

    //if (Pot_Lakhs == "") {
    //    $(".lblPot_Lakhs").show();
    //    returnVal = false;
    //    $('#MainContent_Potential').css('border-color', 'red');
    //}
    //else {
    //    $(".lblPot_Lakhs").hide();
    //    $('#MainContent_Potential').css('border-color', '');
    //}

    //if (Business == "") {
    //    $(".lblBusiness_Exp").show();
    //    $('#MainContent_Business').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {
    //    $(".lblBusiness_Exp").hide();
    //    $('#MainContent_Business').css('border-color', '');
    //}


    //if (Escalated_To == "-- SELECT --") {
    //    $(".lblEscalate").show();
    //    $('#MainContent_ddlEscalate').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {
    //    $(".lblEscalate").hide();
    //    $('#MainContent_ddlEscalate').css('border-color', '');

    //}

    //if (Industry == "-- SELECT --") {
    //    $(".lblIndustry").show();
    //    returnVal = false;
    //    $('#MainContent_ddlIndustry').css('border-color', 'red');
    //}
    //else {
    //    $(".lblIndustry").hide();
    //    $('#MainContent_ddlIndustry').css('border-color', '');


    //}

    if (Target_Date == "") {
        $(".lbltab4Target").show();
        $('#MainContent_tab4Target').css('border-color', 'red');
        returnVal = false;
    }
    else {
        $(".lbltab4Target").hide();
        $('#MainContent_tab4Target').css('border-color', '');
    }

    if (Tab4_Goal == "") {
        $(".lbltab4goal").show();
        $('#MainContent_tab4txtGoal').css('border-color', 'red');
        returnVal = false;
    }
    else {
        $(".lbltab4goal").hide();
        $('#MainContent_tab4txtGoal').css('border-color', '');
    }

    if (returnVal == true && finalstage == 4) {
        if (confirm("Once submitted fields can not be edited by the Owner. Do you want to continue?")) {
            return returnVal;
        }
        else {

            return false;
        }
    }
    if (returnVal == false) {
        alert("Fields marked in red are required");
    }
    else {
        jQuery(function ($) {
            $("#tab_home").tabs({
                active: [0, 1, 2, 3, 4],
                disabled: [5, 6, 7, 8, 9]
            });
        });
    }
    return returnVal;
}
function ValidateTab5() {
    debugger;
    var returnVal = true;
    var Target_Date = $("#MainContent_tab5Target").val();
    //var Escalated_To = $("#MainContent_ddlEscalate").val();
    //var Customer_name = $("#MainContent_ddlCustomerName").val();
    //var Customer_num = $("#MainContent_ddlCustomerNo").val();
    //var Product = $("#MainContent_ExistingProduct").val();
    //var Component = $("#MainContent_Component").val();
    //var Competition = $("#MainContent_CompetitionSpec").val();
    //var Stages = $("#MainContent_Stages").val();
    //var Main_Target = $("#MainContent_TargetDate").val();
    //var OverAll_Potential = $("#MainContent_OverAllPotential").val();
    //var Pot_Lakhs = $("#MainContent_Potential").val();
    //var Business = $("#MainContent_Business").val();
    //var Industry = $("#MainContent_ddlIndustry").val();
    //var Owner = $("#MainContent_ddlSalesEngName").val();
    //var Reviewer = $("#MainContent_ddlReviewer").val();
    var Tab5_Goal = $("#MainContent_tab5txtGoal").val();
    var finalstage = $("#MainContent_Stages").val();
    //var ProjectTitle = $("#MainContent_txtTitle").val();
    //var Existing_ProjectTitle = $("#MainContent_ddlTitle").val();
    //var Project_Type = $("#MainContent_ddlProjectType").val();
    DropdownValidation();
    if (finalstage == 5) {

        if ($.datepicker.parseDate('dd/mm/yy',$('#MainContent_TargetDate').val()) < $.datepicker.parseDate('dd/mm/yy',$("#MainContent_tab5Target").val())) {
            alert("Final Stage Target Date can not be after Project Target Date.");
            return false;
        }
    }
    returnVal = checkNullForCommonFields();
    //if ($('#MainContent_new_project').is(':checked')) {
    //    // debugger;
    //    if (ProjectTitle == "" || ProjectTitle == "-- NO PROJECT --" || ProjectTitle == "-- SELECT PROJECT --") {
    //        returnVal = false;
    //        $('#MainContent_txtTitle').css('border-color', 'red');
    //    }
    //}
    //if ($('#MainContent_existing_project').is(':checked')) {
    //    if (Existing_ProjectTitle == "-- NO PROJECT --" | Existing_ProjectTitle == "-- SELECT PROJECT --") {
    //        $(".lblProduct").show();
    //        returnVal = false;
    //        // alert('Oops Project Title is Invalid, Please Select New Project to enter the Project Name');
    //        alert('You are trying to create new project.For this please click on new project radio button.');
    //        return returnVal;
    //    }
    //}
    //if (Project_Type == "-- SELECT --") {

    //    $('#MainContent_ddlProjectType').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {

    //    $('#MainContent_ddlProjectType').css('border-color', '');

    //}
    //if (Owner == "-- SELECT --") {

    //    $('#MainContent_ddlSalesEngName').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {

    //    $('#MainContent_ddlSalesEngName').css('border-color', '');

    //}

    //if (Reviewer == "-- SELECT --") {

    //    $('#MainContent_ddlReviewer').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {

    //    $('#MainContent_ddlReviewer').css('border-color', '');

    //}
    //if (Product == "") {
    //    $(".lblProduct").show();
    //    returnVal = false;
    //    $('#MainContent_ExistingProduct').css('border-color', 'red');
    //}
    //else {
    //    $(".lblProduct").hide();
    //    $('#MainContent_ExistingProduct').css('border-color', '');
    //}

    //if (Component == "") {
    //    $(".lblComponent").show();
    //    returnVal = false;
    //    $('#MainContent_Component').css('border-color', 'red');
    //}
    //else {
    //    $(".lblComponent").hide();
    //    $('#MainContent_Component').css('border-color', '');

    //}
    //if (Competition == "") {
    //    $(".lblCompetition").show();
    //    returnVal = false;
    //    $('#MainContent_CompetitionSpec').css('border-color', 'red');
    //}
    //else {
    //    $(".lblCompetition").hide();
    //    $('#MainContent_CompetitionSpec').css('border-color', '');

    //}
    //if (Stages == "--SELECT STAGES--") {
    //    $(".lblStages").show();
    //    returnVal = false;
    //    $('#MainContent_Stages').css('border-color', 'red');
    //}
    //else {
    //    $(".lblStages").hide();
    //    $('#MainContent_Stages').css('border-color', '');

    //}
    //if (Main_Target == "") {
    //    $(".lblTarget").show();
    //    $('#MainContent_TargetDate').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {
    //    $(".lblTarget").hide();
    //    $('#MainContent_TargetDate').css('border-color', '');
    //}
    //if (OverAll_Potential == "") {
    //    $(".lblover_Pot").show();
    //    returnVal = false;
    //    $('#MainContent_OverAllPotential').css('border-color', 'red');
    //}
    //else {
    //    $(".lblover_Pot").hide();
    //    $('#MainContent_OverAllPotential').css('border-color', '');
    //}

    //if (Pot_Lakhs == "") {
    //    $(".lblPot_Lakhs").show();
    //    returnVal = false;
    //    $('#MainContent_Potential').css('border-color', 'red');
    //}
    //else {
    //    $(".lblPot_Lakhs").hide();
    //    $('#MainContent_Potential').css('border-color', '');
    //}

    //if (Business == "") {
    //    $(".lblBusiness_Exp").show();
    //    $('#MainContent_Business').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {
    //    $(".lblBusiness_Exp").hide();
    //    $('#MainContent_Business').css('border-color', '');
    //}


    //if (Escalated_To == "-- SELECT --") {
    //    $(".lblEscalate").show();
    //    $('#MainContent_ddlEscalate').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {
    //    $(".lblEscalate").hide();
    //    $('#MainContent_ddlEscalate').css('border-color', '');

    //}

    //if (Industry == "-- SELECT --") {
    //    $(".lblIndustry").show();
    //    returnVal = false;
    //    $('#MainContent_ddlIndustry').css('border-color', 'red');
    //}
    //else {
    //    $(".lblIndustry").hide();
    //    $('#MainContent_ddlIndustry').css('border-color', '');


    //}

    if (Target_Date == "") {
        $(".lbltab5Target").show();
        $('#MainContent_tab5Target').css('border-color', 'red');
        returnVal = false;
    }
    else {
        $(".lbltab5Target").hide();
        $('#MainContent_tab5Target').css('border-color', '');
    }

    if (Tab5_Goal == "") {
        $(".lbltab5goal").show();
        $('#MainContent_tab5txtGoal').css('border-color', 'red');
        returnVal = false;
    }
    else {
        $(".lbltab5goal").hide();
        $('#MainContent_tab5txtGoal').css('border-color', '');
    }

    if (returnVal == true && finalstage == 5) {
        if (confirm("Once submitted fields can not be edited by the Owner. Do you want to continue?")) {
            return returnVal;
        }
        else {

            return false;
        }

    }
    if (returnVal == false) {
        alert("Fields marked in red are required");
    }
    else {
        jQuery(function ($) {
            $("#tab_home").tabs({
                active: [0, 1, 2, 3, 4, 5],
                disabled: [6, 7, 8, 9]
            });
        });
    }
    return returnVal;
}
function ValidateTab6() {
    debugger;
    var returnVal = true;
    var Target_Date = $("#MainContent_tab6Target").val();
    //var Escalated_To = $("#MainContent_ddlEscalate").val();
    //var Customer_name = $("#MainContent_ddlCustomerName").val();
    //var Customer_num = $("#MainContent_ddlCustomerNo").val();
    //var Product = $("#MainContent_ExistingProduct").val();
    //var Component = $("#MainContent_Component").val();
    //var Competition = $("#MainContent_CompetitionSpec").val();
    //var Stages = $("#MainContent_Stages").val();
    //var Main_Target = $("#MainContent_TargetDate").val();
    //var OverAll_Potential = $("#MainContent_OverAllPotential").val();
    //var Pot_Lakhs = $("#MainContent_Potential").val();
    //var Business = $("#MainContent_Business").val();
    //var Industry = $("#MainContent_ddlIndustry").val();
    //var Owner = $("#MainContent_ddlSalesEngName").val();
    //var Reviewer = $("#MainContent_ddlReviewer").val();
    var Tab6_Goal = $("#MainContent_tab6txtGoal").val();
    var finalstage = $("#MainContent_Stages").val();
    //var ProjectTitle = $("#MainContent_txtTitle").val();
    //var Existing_ProjectTitle = $("#MainContent_ddlTitle").val();
    //var Project_Type = $("#MainContent_ddlProjectType").val();

    DropdownValidation();
    if (finalstage == 6) {

        if ($.datepicker.parseDate('dd/mm/yy',$('#MainContent_TargetDate').val()) < $.datepicker.parseDate('dd/mm/yy',$("#MainContent_tab6Target").val())) {
            alert("Final Stage Target Date can not be after Project Target Date.");
            return false;
        }
    }
    returnVal = checkNullForCommonFields();
    //if ($('#MainContent_new_project').is(':checked')) {
    //    // debugger;
    //    if (ProjectTitle == "" || ProjectTitle == "-- NO PROJECT --" || ProjectTitle == "-- SELECT PROJECT --") {
    //        returnVal = false;
    //        $('#MainContent_txtTitle').css('border-color', 'red');
    //    }
    //}
    //if ($('#MainContent_existing_project').is(':checked')) {
    //    if (Existing_ProjectTitle == "-- NO PROJECT --" | Existing_ProjectTitle == "-- SELECT PROJECT --") {
    //        $(".lblProduct").show();
    //        returnVal = false;
    //        alert('You are trying to create new project.For this please click on new project radio button.');
    //        return returnVal;
    //    }
    //}
    //if (Project_Type == "-- SELECT --") {

    //    $('#MainContent_ddlProjectType').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {

    //    $('#MainContent_ddlProjectType').css('border-color', '');

    //}
    //if (Owner == "-- SELECT --") {

    //    $('#MainContent_ddlSalesEngName').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {

    //    $('#MainContent_ddlSalesEngName').css('border-color', '');

    //}

    //if (Reviewer == "-- SELECT --") {

    //    $('#MainContent_ddlReviewer').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {

    //    $('#MainContent_ddlReviewer').css('border-color', '');

    //}
    //if (Product == "") {
    //    $(".lblProduct").show();
    //    returnVal = false;
    //    $('#MainContent_ExistingProduct').css('border-color', 'red');
    //}
    //else {
    //    $(".lblProduct").hide();
    //    $('#MainContent_ExistingProduct').css('border-color', '');
    //}

    //if (Component == "") {
    //    $(".lblComponent").show();
    //    returnVal = false;
    //    $('#MainContent_Component').css('border-color', 'red');
    //}
    //else {
    //    $(".lblComponent").hide();
    //    $('#MainContent_Component').css('border-color', '');

    //}
    //if (Competition == "") {
    //    $(".lblCompetition").show();
    //    returnVal = false;
    //    $('#MainContent_CompetitionSpec').css('border-color', 'red');
    //}
    //else {
    //    $(".lblCompetition").hide();
    //    $('#MainContent_CompetitionSpec').css('border-color', '');

    //}
    //if (Stages == "--SELECT STAGES--") {
    //    $(".lblStages").show();
    //    returnVal = false;
    //    $('#MainContent_Stages').css('border-color', 'red');
    //}
    //else {
    //    $(".lblStages").hide();
    //    $('#MainContent_Stages').css('border-color', '');

    //}
    //if (Main_Target == "") {
    //    $(".lblTarget").show();
    //    $('#MainContent_TargetDate').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {
    //    $(".lblTarget").hide();
    //    $('#MainContent_TargetDate').css('border-color', '');
    //}
    //if (OverAll_Potential == "") {
    //    $(".lblover_Pot").show();
    //    returnVal = false;
    //    $('#MainContent_OverAllPotential').css('border-color', 'red');
    //}
    //else {
    //    $(".lblover_Pot").hide();
    //    $('#MainContent_OverAllPotential').css('border-color', '');
    //}

    //if (Pot_Lakhs == "") {
    //    $(".lblPot_Lakhs").show();
    //    returnVal = false;
    //    $('#MainContent_Potential').css('border-color', 'red');
    //}
    //else {
    //    $(".lblPot_Lakhs").hide();
    //    $('#MainContent_Potential').css('border-color', '');
    //}

    //if (Business == "") {
    //    $(".lblBusiness_Exp").show();
    //    $('#MainContent_Business').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {
    //    $(".lblBusiness_Exp").hide();
    //    $('#MainContent_Business').css('border-color', '');
    //}


    //if (Escalated_To == "-- SELECT --") {
    //    $(".lblEscalate").show();
    //    $('#MainContent_ddlEscalate').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {
    //    $(".lblEscalate").hide();
    //    $('#MainContent_ddlEscalate').css('border-color', '');

    //}

    //if (Industry == "-- SELECT --") {
    //    $(".lblIndustry").show();
    //    returnVal = false;
    //    $('#MainContent_ddlIndustry').css('border-color', 'red');
    //}
    //else {
    //    $(".lblIndustry").hide();
    //    $('#MainContent_ddlIndustry').css('border-color', '');


    //}

    if (Target_Date == "") {
        $(".lbltab6Target").show();
        $('#MainContent_tab6Target').css('border-color', 'red');
        returnVal = false;
    }
    else {
        $(".lbltab6Target").hide();
        $('#MainContent_tab6Target').css('border-color', '');
    }

    if (Tab6_Goal == "") {
        $(".lbltab6goal").show();
        $('#MainContent_tab6txtGoal').css('border-color', 'red');
        returnVal = false;
    }
    else {
        $(".lbltab6goal").hide();
        $('#MainContent_tab6txtGoal').css('border-color', '');
    }

    if (returnVal == true && finalstage == 6) {
        if (confirm("Once submitted fields can not be edited by the Owner. Do you want to continue?")) {
            return returnVal;
        }
        else {

            return false;
        }

    }
    if (returnVal == false) {
        alert("Fields marked in red are required");
    }
    else {
        jQuery(function ($) {
            $("#tab_home").tabs({
                active: [0, 1, 2, 3, 4, 5, 6],
                disabled: [7, 8, 9]
            });
        });
    }
    return returnVal;
}
function ValidateTab7() {
    debugger;
    var returnVal = true;
    var Target_Date = $("#MainContent_tab7Target").val();
    //var Escalated_To = $("#MainContent_ddlEscalate").val();
    //var Customer_name = $("#MainContent_ddlCustomerName").val();
    //var Customer_num = $("#MainContent_ddlCustomerNo").val();
    //var Product = $("#MainContent_ExistingProduct").val();
    //var Component = $("#MainContent_Component").val();
    //var Competition = $("#MainContent_CompetitionSpec").val();
    //var Stages = $("#MainContent_Stages").val();
    //var Main_Target = $("#MainContent_TargetDate").val();
    //var OverAll_Potential = $("#MainContent_OverAllPotential").val();
    //var Pot_Lakhs = $("#MainContent_Potential").val();
    //var Business = $("#MainContent_Business").val();
    //var Industry = $("#MainContent_ddlIndustry").val();
    //var Owner = $("#MainContent_ddlSalesEngName").val();
    //var Reviewer = $("#MainContent_ddlReviewer").val();
    var Tab7_Goal = $("#MainContent_tab7txtGoal").val();
    var finalstage = $("#MainContent_Stages").val();
    //var ProjectTitle = $("#MainContent_txtTitle").val();
    //var Existing_ProjectTitle = $("#MainContent_ddlTitle").val();
    //var Project_Type = $("#MainContent_ddlProjectType").val();

    DropdownValidation();
    if (finalstage == 7) {

        if ($.datepicker.parseDate('dd/mm/yy',$('#MainContent_TargetDate').val()) < $.datepicker.parseDate('dd/mm/yy',$("#MainContent_tab7Target").val())) {
            alert("Final Stage Target Date can not be after Project Target Date.");
            return false;
        }
    }
    returnVal = checkNullForCommonFields();
    //if ($('#MainContent_new_project').is(':checked')) {
    //    // debugger;
    //    if (ProjectTitle == "" || ProjectTitle == "-- NO PROJECT --" || ProjectTitle == "-- SELECT PROJECT --") {
    //        returnVal = false;
    //        $('#MainContent_txtTitle').css('border-color', 'red');
    //    }
    //}
    //if ($('#MainContent_existing_project').is(':checked')) {
    //    if (Existing_ProjectTitle == "-- NO PROJECT --" | Existing_ProjectTitle == "-- SELECT PROJECT --") {
    //        $(".lblProduct").show();
    //        returnVal = false;
    //        // alert('Oops Project Title is Invalid, Please Select New Project to enter the Project Name');
    //        alert('You are trying to create new project.For this please click on new project radio button.');
    //        return returnVal;
    //    }
    //}

    //if (Owner == "-- SELECT --") {

    //    $('#MainContent_ddlSalesEngName').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {

    //    $('#MainContent_ddlSalesEngName').css('border-color', '');

    //}

    //if (Reviewer == "-- SELECT --") {

    //    $('#MainContent_ddlReviewer').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {

    //    $('#MainContent_ddlReviewer').css('border-color', '');

    //}
    //if (Product == "") {
    //    $(".lblProduct").show();
    //    returnVal = false;
    //    $('#MainContent_ExistingProduct').css('border-color', 'red');
    //}
    //else {
    //    $(".lblProduct").hide();
    //    $('#MainContent_ExistingProduct').css('border-color', '');
    //}

    //if (Component == "") {
    //    $(".lblComponent").show();
    //    returnVal = false;
    //    $('#MainContent_Component').css('border-color', 'red');
    //}
    //else {
    //    $(".lblComponent").hide();
    //    $('#MainContent_Component').css('border-color', '');

    //}
    //if (Competition == "") {
    //    $(".lblCompetition").show();
    //    returnVal = false;
    //    $('#MainContent_CompetitionSpec').css('border-color', 'red');
    //}
    //else {
    //    $(".lblCompetition").hide();
    //    $('#MainContent_CompetitionSpec').css('border-color', '');

    //}
    //if (Stages == "--SELECT STAGES--") {
    //    $(".lblStages").show();
    //    returnVal = false;
    //    $('#MainContent_Stages').css('border-color', 'red');
    //}
    //else {
    //    $(".lblStages").hide();
    //    $('#MainContent_Stages').css('border-color', '');

    //}
    //if (Main_Target == "") {
    //    $(".lblTarget").show();
    //    $('#MainContent_TargetDate').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {
    //    $(".lblTarget").hide();
    //    $('#MainContent_TargetDate').css('border-color', '');
    //}
    //if (OverAll_Potential == "") {
    //    $(".lblover_Pot").show();
    //    returnVal = false;
    //    $('#MainContent_OverAllPotential').css('border-color', 'red');
    //}
    //else {
    //    $(".lblover_Pot").hide();
    //    $('#MainContent_OverAllPotential').css('border-color', '');
    //}

    //if (Pot_Lakhs == "") {
    //    $(".lblPot_Lakhs").show();
    //    returnVal = false;
    //    $('#MainContent_Potential').css('border-color', 'red');
    //}
    //else {
    //    $(".lblPot_Lakhs").hide();
    //    $('#MainContent_Potential').css('border-color', '');
    //}

    //if (Business == "") {
    //    $(".lblBusiness_Exp").show();
    //    $('#MainContent_Business').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {
    //    $(".lblBusiness_Exp").hide();
    //    $('#MainContent_Business').css('border-color', '');
    //}


    //if (Escalated_To == "-- SELECT --") {
    //    $(".lblEscalate").show();
    //    $('#MainContent_ddlEscalate').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {
    //    $(".lblEscalate").hide();
    //    $('#MainContent_ddlEscalate').css('border-color', '');

    //}

    //if (Industry == "-- SELECT --") {
    //    $(".lblIndustry").show();
    //    returnVal = false;
    //    $('#MainContent_ddlIndustry').css('border-color', 'red');
    //}
    //else {
    //    $(".lblIndustry").hide();
    //    $('#MainContent_ddlIndustry').css('border-color', '');


    //}

    if (Target_Date == "") {
        $(".lbltab7Target").show();
        $('#MainContent_tab7Target').css('border-color', 'red');
        returnVal = false;
    }
    else {
        $(".lbltab7Target").hide();
        $('#MainContent_tab7Target').css('border-color', '');
    }

    if (Tab7_Goal == "") {
        $(".lbltab7goal").show();
        $('#MainContent_tab7txtGoal').css('border-color', 'red');
        returnVal = false;
    }
    else {
        $(".lbltab7goal").hide();
        $('#MainContent_tab7txtGoal').css('border-color', '');
    }

    if (returnVal == true && finalstage == 7) {
        if (confirm("Once submitted fields can not be edited by the Owner. Do you want to continue?")) {
            return returnVal;
        }
        else {

            return false;
        }

    }
    if (returnVal == false) {
        alert("Fields marked in red are required");
    }
    else {
        jQuery(function ($) {
            $("#tab_home").tabs({
                active: [0, 1, 2, 3, 4, 5, 6, 7],
                disabled: [8, 9]
            });
        });
    }
    return returnVal;
}
function ValidateTab8() {
    debugger;
    var returnVal = true;
    var Target_Date = $("#MainContent_tab8Target").val();
    //var Escalated_To = $("#MainContent_ddlEscalate").val();
    //var Customer_name = $("#MainContent_ddlCustomerName").val();
    //var Customer_num = $("#MainContent_ddlCustomerNo").val();
    //var Product = $("#MainContent_ExistingProduct").val();
    //var Component = $("#MainContent_Component").val();
    //var Competition = $("#MainContent_CompetitionSpec").val();
    //var Stages = $("#MainContent_Stages").val();
    //var Main_Target = $("#MainContent_TargetDate").val();
    //var OverAll_Potential = $("#MainContent_OverAllPotential").val();
    //var Pot_Lakhs = $("#MainContent_Potential").val();
    //var Business = $("#MainContent_Business").val();
    //var Industry = $("#MainContent_ddlIndustry").val();
    //var Owner = $("#MainContent_ddlSalesEngName").val();
    //var Reviewer = $("#MainContent_ddlReviewer").val();
    var Tab8_Goal = $("#MainContent_tab8txtGoal").val();
    var finalstage = $("#MainContent_Stages").val();
    //var ProjectTitle = $("#MainContent_txtTitle").val();
    //var Existing_ProjectTitle = $("#MainContent_ddlTitle").val();
    //var Project_Type = $("#MainContent_ddlProjectType").val();

    DropdownValidation();
    if (finalstage == 8) {

        if ($.datepicker.parseDate('dd/mm/yy',$('#MainContent_TargetDate').val()) < $.datepicker.parseDate('dd/mm/yy',$("#MainContent_tab8Target").val())) {
            alert("Final Stage Target Date can not be after Project Target Date.");
            return false;
        }
    }
    returnVal = checkNullForCommonFields();
    //if ($('#MainContent_new_project').is(':checked')) {
    //    // debugger;
    //    if (ProjectTitle == "" || ProjectTitle == "-- NO PROJECT --" || ProjectTitle == "-- SELECT PROJECT --") {
    //        returnVal = false;
    //        $('#MainContent_txtTitle').css('border-color', 'red');
    //    }
    //}
    //if ($('#MainContent_existing_project').is(':checked')) {
    //    if (Existing_ProjectTitle == "-- NO PROJECT --" | Existing_ProjectTitle == "-- SELECT PROJECT --") {
    //        $(".lblProduct").show();
    //        returnVal = false;
    //        // alert('Oops Project Title is Invalid, Please Select New Project to enter the Project Name');
    //        alert('You are trying to create new project.For this please click on new project radio button.');
    //        return returnVal;
    //    }
    //}

    //if (Owner == "-- SELECT --") {

    //    $('#MainContent_ddlSalesEngName').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {

    //    $('#MainContent_ddlSalesEngName').css('border-color', '');

    //}

    //if (Reviewer == "-- SELECT --") {

    //    $('#MainContent_ddlReviewer').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {

    //    $('#MainContent_ddlReviewer').css('border-color', '');

    //}
    //if (Product == "") {
    //    $(".lblProduct").show();
    //    returnVal = false;
    //    $('#MainContent_ExistingProduct').css('border-color', 'red');
    //}
    //else {
    //    $(".lblProduct").hide();
    //    $('#MainContent_ExistingProduct').css('border-color', '');
    //}

    //if (Component == "") {
    //    $(".lblComponent").show();
    //    returnVal = false;
    //    $('#MainContent_Component').css('border-color', 'red');
    //}
    //else {
    //    $(".lblComponent").hide();
    //    $('#MainContent_Component').css('border-color', '');

    //}
    //if (Competition == "") {
    //    $(".lblCompetition").show();
    //    returnVal = false;
    //    $('#MainContent_CompetitionSpec').css('border-color', 'red');
    //}
    //else {
    //    $(".lblCompetition").hide();
    //    $('#MainContent_CompetitionSpec').css('border-color', '');

    //}
    //if (Stages == "--SELECT STAGES--") {
    //    $(".lblStages").show();
    //    returnVal = false;
    //    $('#MainContent_Stages').css('border-color', 'red');
    //}
    //else {
    //    $(".lblStages").hide();
    //    $('#MainContent_Stages').css('border-color', '');

    //}
    //if (Main_Target == "") {
    //    $(".lblTarget").show();
    //    $('#MainContent_TargetDate').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {
    //    $(".lblTarget").hide();
    //    $('#MainContent_TargetDate').css('border-color', '');
    //}
    //if (OverAll_Potential == "") {
    //    $(".lblover_Pot").show();
    //    returnVal = false;
    //    $('#MainContent_OverAllPotential').css('border-color', 'red');
    //}
    //else {
    //    $(".lblover_Pot").hide();
    //    $('#MainContent_OverAllPotential').css('border-color', '');
    //}

    //if (Pot_Lakhs == "") {
    //    $(".lblPot_Lakhs").show();
    //    returnVal = false;
    //    $('#MainContent_Potential').css('border-color', 'red');
    //}
    //else {
    //    $(".lblPot_Lakhs").hide();
    //    $('#MainContent_Potential').css('border-color', '');
    //}

    //if (Business == "") {
    //    $(".lblBusiness_Exp").show();
    //    $('#MainContent_Business').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {
    //    $(".lblBusiness_Exp").hide();
    //    $('#MainContent_Business').css('border-color', '');
    //}


    //if (Escalated_To == "-- SELECT --") {
    //    $(".lblEscalate").show();
    //    $('#MainContent_ddlEscalate').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {
    //    $(".lblEscalate").hide();
    //    $('#MainContent_ddlEscalate').css('border-color', '');

    //}

    //if (Industry == "-- SELECT --") {
    //    $(".lblIndustry").show();
    //    returnVal = false;
    //    $('#MainContent_ddlIndustry').css('border-color', 'red');
    //}
    //else {
    //    $(".lblIndustry").hide();
    //    $('#MainContent_ddlIndustry').css('border-color', '');


    //}

    if (Target_Date == "") {
        $(".lbltab8Target").show();
        $('#MainContent_tab8Target').css('border-color', 'red');
        returnVal = false;
    }
    else {
        $(".lbltab8Target").hide();
        $('#MainContent_tab8Target').css('border-color', '');
    }

    if (Tab8_Goal == "") {
        $(".lbltab8goal").show();
        $('#MainContent_tab8txtGoal').css('border-color', 'red');
        returnVal = false;
    }
    else {
        $(".lbltab8goal").hide();
        $('#MainContent_tab8txtGoal').css('border-color', '');
    }

    if (returnVal == true && finalstage == 8) {
        if (confirm("Once submitted fields can not be edited by the Owner. Do you want to continue?")) {
            return returnVal;
        }
        else {

            return false;
        }

    }
    if (returnVal == false) {
        alert("Fields marked in red are required");
    }
    else {
        jQuery(function ($) {
            $("#tab_home").tabs({
                active: [0, 1, 2, 3, 4, 5, 6, 7, 8],
                disabled: [9]
            });
        });
    }
    return returnVal;
}
function ValidateTab9() {
    debugger;
    var returnVal = true;
    var Target_Date = $("#MainContent_tab9Target").val();
    //var Escalated_To = $("#MainContent_ddlEscalate").val();
    //var Customer_name = $("#MainContent_ddlCustomerName").val();
    //var Customer_num = $("#MainContent_ddlCustomerNo").val();
    //var Product = $("#MainContent_ExistingProduct").val();
    //var Component = $("#MainContent_Component").val();
    //var Competition = $("#MainContent_CompetitionSpec").val();
    //var Stages = $("#MainContent_Stages").val();
    //var Main_Target = $("#MainContent_TargetDate").val();
    //var OverAll_Potential = $("#MainContent_OverAllPotential").val();
    //var Pot_Lakhs = $("#MainContent_Potential").val();
    //var Business = $("#MainContent_Business").val();
    //var Industry = $("#MainContent_ddlIndustry").val();
    //var Owner = $("#MainContent_ddlSalesEngName").val();
    //var Reviewer = $("#MainContent_ddlReviewer").val();
    var Tab9_Goal = $("#MainContent_tab9txtGoal").val();
    var finalstage = $("#MainContent_Stages").val();
    //var ProjectTitle = $("#MainContent_txtTitle").val();
    //var Existing_ProjectTitle = $("#MainContent_ddlTitle").val();
    //var Project_Type = $("#MainContent_ddlProjectType").val();

    DropdownValidation();
    if (finalstage == 9) {

        if ($.datepicker.parseDate('dd/mm/yy',$('#MainContent_TargetDate').val()) < $.datepicker.parseDate('dd/mm/yy',$("#MainContent_tab9Target").val())) {
            alert("Final Stage Target Date can not be after Project Target Date.");
            return false;
        }
    }
    returnVal = checkNullForCommonFields();
    //if ($('#MainContent_new_project').is(':checked')) {
    //    // debugger;
    //    if (ProjectTitle == "" || ProjectTitle == "-- NO PROJECT --" || ProjectTitle == "-- SELECT PROJECT --") {
    //        returnVal = false;
    //        $('#MainContent_txtTitle').css('border-color', 'red');
    //    }
    //}
    //if ($('#MainContent_existing_project').is(':checked')) {
    //    if (Existing_ProjectTitle == "-- NO PROJECT --" | Existing_ProjectTitle == "-- SELECT PROJECT --") {
    //        $(".lblProduct").show();
    //        returnVal = false;
    //        // alert('Oops Project Title is Invalid, Please Select New Project to enter the Project Name');
    //        alert('You are trying to create new project.For this please click on new project radio button.');
    //        return returnVal;
    //    }
    //}

    //if (Owner == "-- SELECT --") {

    //    $('#MainContent_ddlSalesEngName').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {

    //    $('#MainContent_ddlSalesEngName').css('border-color', '');

    //}

    //if (Reviewer == "-- SELECT --") {

    //    $('#MainContent_ddlReviewer').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {

    //    $('#MainContent_ddlReviewer').css('border-color', '');

    //}
    //if (Product == "") {
    //    $(".lblProduct").show();
    //    returnVal = false;
    //    $('#MainContent_ExistingProduct').css('border-color', 'red');
    //}
    //else {
    //    $(".lblProduct").hide();
    //    $('#MainContent_ExistingProduct').css('border-color', '');
    //}

    //if (Component == "") {
    //    $(".lblComponent").show();
    //    returnVal = false;
    //    $('#MainContent_Component').css('border-color', 'red');
    //}
    //else {
    //    $(".lblComponent").hide();
    //    $('#MainContent_Component').css('border-color', '');

    //}
    //if (Competition == "") {
    //    $(".lblCompetition").show();
    //    returnVal = false;
    //    $('#MainContent_CompetitionSpec').css('border-color', 'red');
    //}
    //else {
    //    $(".lblCompetition").hide();
    //    $('#MainContent_CompetitionSpec').css('border-color', '');

    //}
    //if (Stages == "--SELECT STAGES--") {
    //    $(".lblStages").show();
    //    returnVal = false;
    //    $('#MainContent_Stages').css('border-color', 'red');
    //}
    //else {
    //    $(".lblStages").hide();
    //    $('#MainContent_Stages').css('border-color', '');

    //}
    //if (Main_Target == "") {
    //    $(".lblTarget").show();
    //    $('#MainContent_TargetDate').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {
    //    $(".lblTarget").hide();
    //    $('#MainContent_TargetDate').css('border-color', '');
    //}
    //if (OverAll_Potential == "") {
    //    $(".lblover_Pot").show();
    //    returnVal = false;
    //    $('#MainContent_OverAllPotential').css('border-color', 'red');
    //}
    //else {
    //    $(".lblover_Pot").hide();
    //    $('#MainContent_OverAllPotential').css('border-color', '');
    //}

    //if (Pot_Lakhs == "") {
    //    $(".lblPot_Lakhs").show();
    //    returnVal = false;
    //    $('#MainContent_Potential').css('border-color', 'red');
    //}
    //else {
    //    $(".lblPot_Lakhs").hide();
    //    $('#MainContent_Potential').css('border-color', '');
    //}

    //if (Business == "") {
    //    $(".lblBusiness_Exp").show();
    //    $('#MainContent_Business').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {
    //    $(".lblBusiness_Exp").hide();
    //    $('#MainContent_Business').css('border-color', '');
    //}


    //if (Escalated_To == "-- SELECT --") {
    //    $(".lblEscalate").show();
    //    $('#MainContent_ddlEscalate').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {
    //    $(".lblEscalate").hide();
    //    $('#MainContent_ddlEscalate').css('border-color', '');

    //}

    //if (Industry == "-- SELECT --") {
    //    $(".lblIndustry").show();
    //    returnVal = false;
    //    $('#MainContent_ddlIndustry').css('border-color', 'red');
    //}
    //else {
    //    $(".lblIndustry").hide();
    //    $('#MainContent_ddlIndustry').css('border-color', '');


    //}

    if (Target_Date == "") {
        $(".lbltab9Target").show();
        $('#MainContent_tab9Target').css('border-color', 'red');
        returnVal = false;
    }
    else {
        $(".lbltab9Target").hide();
        $('#MainContent_tab9Target').css('border-color', '');
    }

    if (Tab9_Goal == "") {
        $(".lbltab9goal").show();
        $('#MainContent_tab9txtGoal').css('border-color', 'red');
        returnVal = false;
    }
    else {
        $(".lbltab9goal").hide();
        $('#MainContent_tab9txtGoal').css('border-color', '');
    }

    if (returnVal == true && finalstage == 9) {
        if (confirm("Once submitted fields can not be edited by the Owner. Do you want to continue?")) {
            return returnVal;
        }
        else {

            return false;
        }

    }
    if (returnVal == false) {
        alert("Fields marked in red are required");
    }
    else {
        jQuery(function ($) {
            $("#tab_home").tabs({
                active: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
                disabled: []
            });
        });
    }
    return returnVal;
}
function ValidateTab10() {
    debugger;
    var returnVal = true;
    var Target_Date = $("#MainContent_tab10Target").val();
    //var Escalated_To = $("#MainContent_ddlEscalate").val();
    //var Customer_name = $("#MainContent_ddlCustomerName").val();
    //var Customer_num = $("#MainContent_ddlCustomerNo").val();
    //var Product = $("#MainContent_ExistingProduct").val();
    //var Component = $("#MainContent_Component").val();
    //var Competition = $("#MainContent_CompetitionSpec").val();
    //var Stages = $("#MainContent_Stages").val();
    //var Main_Target = $("#MainContent_TargetDate").val();
    //var OverAll_Potential = $("#MainContent_OverAllPotential").val();
    //var Pot_Lakhs = $("#MainContent_Potential").val();
    //var Business = $("#MainContent_Business").val();
    //var Industry = $("#MainContent_ddlIndustry").val();
    //var Owner = $("#MainContent_ddlSalesEngName").val();
    //var Reviewer = $("#MainContent_ddlReviewer").val();
    var Tab10_Goal = $("#MainContent_tab10txtGoal").val();
    var finalstage = $("#MainContent_Stages").val();
    //var Existing_ProjectTitle = $("#MainContent_ddlTitle").val();
    //var Project_Type = $("#MainContent_ddlProjectType").val();

    DropdownValidation();
    if (finalstage == 10) {

        if ($.datepicker.parseDate('dd/mm/yy',$('#MainContent_TargetDate').val()) < $.datepicker.parseDate('dd/mm/yy',$("#MainContent_tab10Target").val())) {
            alert("Final Stage Target Date can not be after Project Target Date.");
            return false;
        }
    }
    returnVal = checkNullForCommonFields();
    //if ($('#MainContent_new_project').is(':checked')) {
    //    // debugger;
    //    if (ProjectTitle == "" || ProjectTitle == "-- NO PROJECT --" || ProjectTitle == "-- SELECT PROJECT --") {
    //        returnVal = false;
    //        $('#MainContent_txtTitle').css('border-color', 'red');
    //    }
    //}
    //if ($('#MainContent_existing_project').is(':checked')) {
    //    if (Existing_ProjectTitle == "-- NO PROJECT --" | Existing_ProjectTitle == "-- SELECT PROJECT --") {
    //        $(".lblProduct").show();
    //        returnVal = false;
    //        // alert('Oops Project Title is Invalid, Please Select New Project to enter the Project Name');
    //        alert('You are trying to create new project.For this please click on new project radio button.');
    //        return returnVal;
    //    }
    //}

    //if (Owner == "-- SELECT --") {

    //    $('#MainContent_ddlSalesEngName').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {

    //    $('#MainContent_ddlSalesEngName').css('border-color', '');

    //}

    //if (Reviewer == "-- SELECT --") {

    //    $('#MainContent_ddlReviewer').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {

    //    $('#MainContent_ddlReviewer').css('border-color', '');

    //}
    //if (Product == "") {
    //    $(".lblProduct").show();
    //    returnVal = false;
    //    $('#MainContent_ExistingProduct').css('border-color', 'red');
    //}
    //else {
    //    $(".lblProduct").hide();
    //    $('#MainContent_ExistingProduct').css('border-color', '');
    //}

    //if (Component == "") {
    //    $(".lblComponent").show();
    //    returnVal = false;
    //    $('#MainContent_Component').css('border-color', 'red');
    //}
    //else {
    //    $(".lblComponent").hide();
    //    $('#MainContent_Component').css('border-color', '');

    //}
    //if (Competition == "") {
    //    $(".lblCompetition").show();
    //    returnVal = false;
    //    $('#MainContent_CompetitionSpec').css('border-color', 'red');
    //}
    //else {
    //    $(".lblCompetition").hide();
    //    $('#MainContent_CompetitionSpec').css('border-color', '');

    //}
    //if (Stages == "--SELECT STAGES--") {
    //    $(".lblStages").show();
    //    returnVal = false;
    //    $('#MainContent_Stages').css('border-color', 'red');
    //}
    //else {
    //    $(".lblStages").hide();
    //    $('#MainContent_Stages').css('border-color', '');

    //}
    //if (Main_Target == "") {
    //    $(".lblTarget").show();
    //    $('#MainContent_TargetDate').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {
    //    $(".lblTarget").hide();
    //    $('#MainContent_TargetDate').css('border-color', '');
    //}
    //if (OverAll_Potential == "") {
    //    $(".lblover_Pot").show();
    //    returnVal = false;
    //    $('#MainContent_OverAllPotential').css('border-color', 'red');
    //}
    //else {
    //    $(".lblover_Pot").hide();
    //    $('#MainContent_OverAllPotential').css('border-color', '');
    //}

    //if (Pot_Lakhs == "") {
    //    $(".lblPot_Lakhs").show();
    //    returnVal = false;
    //    $('#MainContent_Potential').css('border-color', 'red');
    //}
    //else {
    //    $(".lblPot_Lakhs").hide();
    //    $('#MainContent_Potential').css('border-color', '');
    //}

    //if (Business == "") {
    //    $(".lblBusiness_Exp").show();
    //    $('#MainContent_Business').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {
    //    $(".lblBusiness_Exp").hide();
    //    $('#MainContent_Business').css('border-color', '');
    //}


    //if (Escalated_To == "-- SELECT --") {
    //    $(".lblEscalate").show();
    //    $('#MainContent_ddlEscalate').css('border-color', 'red');
    //    returnVal = false;
    //}
    //else {
    //    $(".lblEscalate").hide();
    //    $('#MainContent_ddlEscalate').css('border-color', '');

    //}

    //if (Industry == "-- SELECT --") {
    //    $(".lblIndustry").show();
    //    returnVal = false;
    //    $('#MainContent_ddlIndustry').css('border-color', 'red');
    //}
    //else {
    //    $(".lblIndustry").hide();
    //    $('#MainContent_ddlIndustry').css('border-color', '');


    //}

    if (Target_Date == "") {
        $(".lbltab10Target").show();
        $('#MainContent_tab10Target').css('border-color', 'red');
        returnVal = false;
    }
    else {
        $(".lbltab10Target").hide();
        $('#MainContent_tab10Target').css('border-color', '');
    }

    if (Tab10_Goal == "") {
        $(".lbltab10goal").show();
        $('#MainContent_tab10txtGoal').css('border-color', 'red');
        returnVal = false;
    }
    else {
        $(".lbltab10goal").hide();
        $('#MainContent_tab10txtGoal').css('border-color', '');
    }

    if (returnVal == true && finalstage == 10) {
        if (confirm("Once submitted fields can not be edited by the Owner. Do you want to continue?")) {
            return returnVal;
        }
        else {

            return false;
        }

    }
    if (returnVal == false) {
        alert("Fields marked in red are required");
    }
    return returnVal;
}

function ValidateSubmitTab1() {
    var returnVal = true;
    var Target_Date = $("#MainContent_tab1Target").val();
    var Escalated_To = $("#MainContent_ddlEscalate").val();
    var Completion_Date = $("#MainContent_tab1Completion").val();
    var Customer_name = $("#MainContent_ddlCustomerName").val();
    var Customer_num = $("#MainContent_ddlCustomerNo").val();
    var Product = $("#MainContent_ExistingProduct").val();
    var Component = $("#MainContent_Component").val();
    var Competition = $("#MainContent_CompetitionSpec").val();
    var Stages = $("#MainContent_Stages").val();
    var Main_Target = $("#MainContent_TargetDate").val();
    var OverAll_Potential = $("#MainContent_OverAllPotential").val();
    var Pot_Lakhs = $("#MainContent_Potential").val();
    var Business = $("#MainContent_Business").val();
    var Industry = $("#MainContent_ddlIndustry").val();
    var Monthly_Expected = $("#MainContent_tab1_monthly_expected").val();
    var comments = $("#MainContent_tab1_comments").val();
    var finalstage = $("#MainContent_Stages").val();

    if (finalstage == 1 && Completion_Date != "") {

        if ($('#MainContent_tab1_radio_success').is(':checked') || $('#MainContent_tab1_radio_failure').is(':checked')) {
            returnVal = true;
        }
        else {
            alert("Please select The Checkbox For Success or Failure");
            return false;
        }

        if ($('#MainContent_tab1_radio_success').is(':checked')) {
            if (Monthly_Expected == "") {
                $('#MainContent_tab1_monthly_expected').css('border-color', 'red');
                returnVal = false;
            }
            else {
                $('#MainContent_tab1_monthly_expected').css('border-color', '');
            }
        }

        if (comments == "") {
            $("#MainContent_tab1_comments").css('border-color', 'red');
            returnVal = false;
        }
        else {
            $("#MainContent_tab1_comments").css('border-color', '');
        }
    }
    if (Product == "") {
        $(".lblProduct").show();
        returnVal = false;
    }
    else {
        $(".lblProduct").hide();
    }

    if (Component == "") {
        $(".lblComponent").show();
        returnVal = false;
    }
    else {
        $(".lblComponent").hide();
    }

    if (Competition == "") {
        $(".lblCompetition").show();
        returnVal = false;
    }
    else {
        $(".lblCompetition").hide();
    }
    if (Stages == "--SELECT STAGES--") {
        $(".lblStages").show();
        returnVal = false;
    }
    else {
        $(".lblStages").hide();
    }
    if (Main_Target == "") {
        $(".lblTarget").show();
        returnVal = false;
    }
    else {
        $(".lblTarget").hide();
    }
    if (OverAll_Potential == "") {
        $(".lblover_Pot").show();
        returnVal = false;
    }
    else {
        $(".lblover_Pot").hide();
    }

    if (Pot_Lakhs == "") {
        $(".lblPot_Lakhs").show();
        returnVal = false;
    }
    else {
        $(".lblPot_Lakhs").hide();
    }

    if (Business == "") {
        $(".lblBusiness_Exp").show();
        returnVal = false;
    }
    else {
        $(".lblBusiness_Exp").hide();
    }

    if (Target_Date == "") {
        $(".lbltab1Target").show();
        returnVal = false;
    }
    else {
        $(".lbltab1Target").hide();
    }

    if (Completion_Date == "") {
        $(".lbltab1Complete").show();
        $('#MainContent_tab1Completion').css('background-color', 'red');
        returnVal = false;
    }
    else {
        $('#MainContent_tab1Completion').css('background-color', '');
        $(".lbltab1Complete").hide();
    }
    if (Escalated_To == "-- SELECT --") {
        $(".lbltab1Escalate").show();
        returnVal = false;
    }
    else {
        $(".lbltab1Escalate").hide();
    }
    if (Industry == "-- SELECT --") {
        $(".lblIndustry").show();
        returnVal = false;
    }
    else {
        $(".lblIndustry").hide();
    }
    if (returnVal == false) {
        alert("Fields marked in red are required");
    }
    return returnVal;
}
function ValidateSubmitTab2() {

    var returnVal = true;
    var Target_Date = $("#MainContent_tab2Target").val();
    var Escalated_To = $("#MainContent_ddlEscalate").val();
    var Completion_Date = $("#MainContent_tab2Completion").val();
    var Customer_name = $("#MainContent_ddlCustomerName").val();
    var Customer_num = $("#MainContent_ddlCustomerNo").val();
    var Product = $("#MainContent_ExistingProduct").val();
    var Component = $("#MainContent_Component").val();
    var Competition = $("#MainContent_CompetitionSpec").val();
    var Stages = $("#MainContent_Stages").val();
    var Main_Target = $("#MainContent_TargetDate").val();
    var OverAll_Potential = $("#MainContent_OverAllPotential").val();
    var Pot_Lakhs = $("#MainContent_Potential").val();
    var Business = $("#MainContent_Business").val();
    var Industry = $("#MainContent_ddlIndustry").val();
    var Monthly_Expected = $("#MainContent_tab2_monthly_expected").val();
    var comments = $("#MainContent_tab2_comments").val();  
    var finalstage = $("#MainContent_Stages").val();

    if (finalstage == 2 && Completion_Date != "") {
        if ($('#MainContent_tab2_radio_success').is(':checked') || $('#MainContent_tab2_radio_failure').is(':checked')) {
            returnVal = true;
        }
        else {
            alert("Please select The Checkbox For Success or Failure");
            return false;
        }

        if ($('#MainContent_tab2_radio_success').is(':checked')) {
            if (Monthly_Expected == "") {
                $('#MainContent_tab2_monthly_expected').css('border-color', 'red');
                returnVal = false;
            }
            else {
                $('#MainContent_tab2_monthly_expected').css('border-color', '');
            }
        }

        if (comments == "") {
            $("#MainContent_tab2_comments").css('border-color', 'red');
            returnVal = false;
        }
        else {
            $("#MainContent_tab2_comments").css('border-color', '');
            returnVal = true;
        }
    }
    if (Product == "") {
        $(".lblProduct").show();
        returnVal = false;
    }
    else {
        $(".lblProduct").hide();
    }

    if (Component == "") {
        $(".lblComponent").show();
        returnVal = false;
    }
    else {
        $(".lblComponent").hide();
    }
    if (Competition == "") {
        $(".lblCompetition").show();
        returnVal = false;
    }
    else {
        $(".lblCompetition").hide();
    }
    if (Stages == "--SELECT STAGES--") {
        $(".lblStages").show();
        returnVal = false;
    }
    else {
        $(".lblStages").hide();
    }
    if (Main_Target == "") {
        $(".lblTarget").show();
        returnVal = false;
    }
    else {
        $(".lblTarget").hide();
    }
    if (OverAll_Potential == "") {
        $(".lblover_Pot").show();
        returnVal = false;
    }
    else {
        $(".lblover_Pot").hide();
    }

    if (Pot_Lakhs == "") {
        $(".lblPot_Lakhs").show();
        returnVal = false;
    }
    else {
        $(".lblPot_Lakhs").hide();
    }

    if (Business == "") {
        $(".lblBusiness_Exp").show();
        returnVal = false;
    }
    else {
        $(".lblBusiness_Exp").hide();
    }

    if (Target_Date == "") {
        $(".lbltab2Target").show();
        returnVal = false;
    }
    else {
        $(".lbltab2Target").hide();
    }
    if (Completion_Date == "") {
        $(".lbltab2Complete").show();
        $('#MainContent_tab2Completion').css('background-color', 'red');
        returnVal = false;
    }
    else {
        $('#MainContent_tab2Completion').css('background-color', '');
        $(".lbltab2Complete").hide();
    }
    if (Escalated_To == "-- SELECT --") {
        $(".lblEscalate").show();
        returnVal = false;
    }
    else {
        $(".lblEscalate").hide();
    }
    if (Industry == "-- SELECT --") {
        $(".lblIndustry").show();
        returnVal = false;
    }
    else {
        $(".lblIndustry").hide();
    }
    if (returnVal == false) {
        alert("Fields marked in red are required");
    }
    return returnVal;
}
function ValidateSubmitTab3() {
    var returnVal = true;
    var Target_Date = $("#MainContent_tab3Target").val();
    var Escalated_To = $("#MainContent_ddlEscalate").val();
    var Completion_Date = $("#MainContent_tab3Completion").val();
    var Customer_name = $("#MainContent_ddlCustomerName").val();
    var Customer_num = $("#MainContent_ddlCustomerNo").val();
    var Product = $("#MainContent_ExistingProduct").val();
    var Component = $("#MainContent_Component").val();
    var Competition = $("#MainContent_CompetitionSpec").val();
    var Stages = $("#MainContent_Stages").val();
    var Main_Target = $("#MainContent_TargetDate").val();
    var OverAll_Potential = $("#MainContent_OverAllPotential").val();
    var Pot_Lakhs = $("#MainContent_Potential").val();
    var Business = $("#MainContent_Business").val();
    var Industry = $("#MainContent_ddlIndustry").val();
    var Monthly_Expected = $("#MainContent_tab3_monthly_expected").val();
    var comments = $("#MainContent_tab3_comments").val(); 
    var finalstage = $("#MainContent_Stages").val();   

    if (finalstage == 3 && Completion_Date != "") {
        if ($('#MainContent_tab3_radio_success').is(':checked') || $('#MainContent_tab3_radio_failure').is(':checked')) {
            returnVal = true;
         }
        else {
            alert("Please select The Checkbox For Success or Failure");
            return false;
        }

        if ($('#MainContent_tab3_radio_success').is(':checked')) {
            if (Monthly_Expected == "") {
                $('#MainContent_tab3_monthly_expected').css('border-color', 'red');
                returnVal = false;
            }
            else {
                $('#MainContent_tab3_monthly_expected').css('border-color', '');
            }
        }
     
        if (comments == "") {
            $("#MainContent_tab3_comments").css('border-color', 'red');
            returnVal = false;
        }
        else {
            $("#MainContent_tab3_comments").css('border-color', '');
        }
    }

    if (Product == "") {
        $(".lblProduct").show();
        returnVal = false;
    }
    else {
        $(".lblProduct").hide();

    }

    if (Component == "") {
        $(".lblComponent").show();
        returnVal = false;
    }
    else {
        $(".lblComponent").hide();

    }
    if (Competition == "") {
        $(".lblCompetition").show();
        returnVal = false;
    }
    else {
        $(".lblCompetition").hide();

    }
    if (Stages == "--SELECT STAGES--") {
        $(".lblStages").show();
        returnVal = false;
    }
    else {
        $(".lblStages").hide();

    }
    if (Main_Target == "") {
        $(".lblTarget").show();
        returnVal = false;
    }
    else {
        $(".lblTarget").hide();

    }
    if (OverAll_Potential == "") {
        $(".lblover_Pot").show();
        returnVal = false;
    }
    else {
        $(".lblover_Pot").hide();

    }

    if (Pot_Lakhs == "") {
        $(".lblPot_Lakhs").show();
        returnVal = false;
    }
    else {
        $(".lblPot_Lakhs").hide();

    }

    if (Business == "") {
        $(".lblBusiness_Exp").show();
        returnVal = false;
    }
    else {
        $(".lblBusiness_Exp").hide();

    }

    if (Target_Date == "") {
        $(".lbltab3Target").show();
        returnVal = false;
    }
    else {
        $(".lbltab3Target").hide();
    }

    if (Completion_Date == "") {
        $(".lbltab3Complete").show();
        $('#MainContent_tab3Completion').css('background-color', 'red');      
        returnVal = false;
    }
    else {
        $('#MainContent_tab3Completion').css('background-color', '');
        $(".lbltab3Complete").hide();
    }
    if (Escalated_To == "-- SELECT --") {
        $(".lblEscalate").show();
        returnVal = false;
    }
    else {
        $(".lblEscalate").hide();
    }
    if (Industry == "-- SELECT --") {
        $(".lblIndustry").show();
        returnVal = false;
    }
    else {
        $(".lblIndustry").hide();
    }

    if (returnVal == false) {
        alert("Fields marked in red are required");
    }
    return returnVal;
}
function ValidateSubmitTab4() {
    var returnVal = true;
    var Target_Date = $("#MainContent_tab4Target").val();
    var Escalated_To = $("#MainContent_ddlEscalate").val();
    var Completion_Date = $("#MainContent_tab4Completion").val();
    var Customer_name = $("#MainContent_ddlCustomerName").val();
    var Customer_num = $("#MainContent_ddlCustomerNo").val();
    var Product = $("#MainContent_ExistingProduct").val();
    var Component = $("#MainContent_Component").val();
    var Competition = $("#MainContent_CompetitionSpec").val();
    var Stages = $("#MainContent_Stages").val();
    var Main_Target = $("#MainContent_TargetDate").val();
    var OverAll_Potential = $("#MainContent_OverAllPotential").val();
    var Pot_Lakhs = $("#MainContent_Potential").val();
    var Business = $("#MainContent_Business").val();
    var Industry = $("#MainContent_ddlIndustry").val();
    var Monthly_Expected = $("#MainContent_tab4_monthly_expected").val();
    var comments = $("#MainContent_tab4_comments").val();   
    var finalstage = $("#MainContent_Stages").val();

    if (finalstage == 4 && Completion_Date != "") {
        if ($('#MainContent_tab4_radio_success').is(':checked') || $('#MainContent_tab4_radio_failure').is(':checked')) {
            returnVal = true;
        }
        else {
            alert("Please select The Checkbox For Success or Failure");
            return false;
        }
        if ($("#MainContent_tab4_radio_success").is(':checked')) {
            if (Monthly_Expected == "") {
                $('#MainContent_tab4_monthly_expected').css('border-color', 'red');
                returnVal = false;
            }
            else {
                $('#MainContent_tab4_monthly_expected').css('border-color', '');                
            }
        }
        if (comments == "") {
            $("#MainContent_tab4_comments").css('border-color', 'red');
            returnVal = false;
        }
        else {
            $("#MainContent_tab4_comments").css('border-color', '');
        }
    }

    if (Product == "") {
        $(".lblProduct").show();
        returnVal = false;
    }
    else {
        $(".lblProduct").hide();
    }
    if (Component == "") {
        $(".lblComponent").show();
        returnVal = false;
    }
    else {
        $(".lblComponent").hide();
    }
    if (Competition == "") {
        $(".lblCompetition").show();
        returnVal = false;
    }
    else {
        $(".lblCompetition").hide();
    }
    if (Stages == "--SELECT STAGES--") {
        $(".lblStages").show();
        returnVal = false;
    }
    else {
        $(".lblStages").hide();
    }
    if (Main_Target == "") {
        $(".lblTarget").show();
        returnVal = false;
    }
    else {
        $(".lblTarget").hide();
    }
    if (OverAll_Potential == "") {
        $(".lblover_Pot").show();
        returnVal = false;
    }
    else {
        $(".lblover_Pot").hide();
    }

    if (Pot_Lakhs == "") {
        $(".lblPot_Lakhs").show();
        returnVal = false;
    }
    else {
        $(".lblPot_Lakhs").hide();
    }

    if (Business == "") {
        $(".lblBusiness_Exp").show();
        returnVal = false;
    }
    else {
        $(".lblBusiness_Exp").hide();
    }

    if (Target_Date == "") {
        $(".lbltab4Target").show();
        returnVal = false;
    }
    else {
        $(".lbltab4Target").hide();
    }
    if (Completion_Date == "") {
        $(".lbltab4Complete").show();
        $('#MainContent_tab4Completion').css('background-color', 'red');
        returnVal = false;
    }
    else {
        $('#MainContent_tab4Completion').css('background-color', '');
        $(".lbltab4Complete").hide();
    }
    if (Escalated_To == "-- SELECT --") {
        $(".lblEscalate").show();
        returnVal = false;
    }
    else {
        $(".lblEscalate").hide();
    }
    if (Industry == "-- SELECT --") {
        $(".lblIndustry").show();
        returnVal = false;
    }
    else {
        $(".lblIndustry").hide();
    }
    if (returnVal == false) {
        alert("Fields marked in red are required");
    }
    return returnVal;
}
function ValidateSubmitTab5() {
    var returnVal = true;
    var Target_Date = $("#MainContent_tab5Target").val();
    var Escalated_To = $("#MainContent_ddlEscalate").val();
    var Completion_Date = $("#MainContent_tab5Completion").val();
    var Customer_name = $("#MainContent_ddlCustomerName").val();
    var Customer_num = $("#MainContent_ddlCustomerNo").val();
    var Product = $("#MainContent_ExistingProduct").val();
    var Component = $("#MainContent_Component").val();
    var Competition = $("#MainContent_CompetitionSpec").val();
    var Stages = $("#MainContent_Stages").val();
    var Main_Target = $("#MainContent_TargetDate").val();
    var OverAll_Potential = $("#MainContent_OverAllPotential").val();
    var Pot_Lakhs = $("#MainContent_Potential").val();
    var Business = $("#MainContent_Business").val();
    var Industry = $("#MainContent_ddlIndustry").val();
    var Monthly_Expected = $("#MainContent_tab5_monthly_expected").val();
    var comments = $("#MainContent_tab5_comments").val();   
    var finalstage = $("#MainContent_Stages").val();

    if (finalstage == 5 && Completion_Date != "") {

        if ($('#MainContent_tab5_radio_success').is(':checked') || $('#MainContent_tab5_radio_failure').is(':checked')) {
            returnVal = true;
        }
        else {
            alert('Please select The Checkbox For Success or Failure')
        }

        if ($("#MainContent_tab5_radio_success").is(':checked')) {
            if (Monthly_Expected == "") {
                $('#MainContent_tab5_monthly_expected').css('border-color', 'red');
                returnVal = false;
            }
            else {
                $('#MainContent_tab5_monthly_expected').css('border-color', '');
            }
        }

        if (comments == "") {
            $("#MainContent_tab5_comments").css('border-color', 'red');
            returnVal = false;
        }
        else {
            $("#MainContent_tab5_comments").css('border-color', '');
        }
    }

    if (Product == "") {
        $(".lblProduct").show();
        returnVal = false;
    }
    else {
        $(".lblProduct").hide();
    }

    if (Component == "") {
        $(".lblComponent").show();
        returnVal = false;
    }
    else {
        $(".lblComponent").hide();
    }
    if (Competition == "") {
        $(".lblCompetition").show();
        returnVal = false;
    }
    else {
        $(".lblCompetition").hide();
    }
    if (Stages == "--SELECT STAGES--") {
        $(".lblStages").show();
        returnVal = false;
    }
    else {
        $(".lblStages").hide();

    }
    if (Main_Target == "") {
        $(".lblTarget").show();
        returnVal = false;
    }
    else {
        $(".lblTarget").hide();
    }
    if (OverAll_Potential == "") {
        $(".lblover_Pot").show();
        returnVal = false;
    }
    else {
        $(".lblover_Pot").hide();
    }

    if (Pot_Lakhs == "") {
        $(".lblPot_Lakhs").show();
        returnVal = false;
    }
    else {
        $(".lblPot_Lakhs").hide();
    }

    if (Business == "") {
        $(".lblBusiness_Exp").show();
        returnVal = false;
    }
    else {
        $(".lblBusiness_Exp").hide();
    }

    if (Target_Date == "") {
        $(".lbltab5Target").show();
        returnVal = false;
    }
    else {
        $(".lbltab5Target").hide();
    }
    if (Completion_Date == "") {
        $(".lbltab5Complete").show();
        $('#MainContent_tab5Completion').css('background-color', 'red');
        returnVal = false;
    }
    else {
        $('#MainContent_tab5Completion').css('background-color', '');
        $(".lbltab5Complete").hide();
    }
    if (Escalated_To == "-- SELECT --") {
        $(".lblEscalate").show();
        returnVal = false;
    }
    else {
        $(".lblEscalate").hide();
    }
    if (Industry == "-- SELECT --") {
        $(".lblIndustry").show();
        returnVal = false;
    }
    else {
        $(".lblIndustry").hide();
    }
    if (returnVal == false) {
        alert("Fields marked in red are required");
    }
    return returnVal;
}
function ValidateSubmitTab6() {
    var returnVal = true;
    var Target_Date = $("#MainContent_tab6Target").val();
    var Escalated_To = $("#MainContent_ddlEscalate").val();
    var Completion_Date = $("#MainContent_tab6Completion").val();
    var Customer_name = $("#MainContent_ddlCustomerName").val();
    var Customer_num = $("#MainContent_ddlCustomerNo").val();
    var Product = $("#MainContent_ExistingProduct").val();
    var Component = $("#MainContent_Component").val();
    var Competition = $("#MainContent_CompetitionSpec").val();
    var Stages = $("#MainContent_Stages").val();
    var Main_Target = $("#MainContent_TargetDate").val();
    var OverAll_Potential = $("#MainContent_OverAllPotential").val();
    var Pot_Lakhs = $("#MainContent_Potential").val();
    var Business = $("#MainContent_Business").val();
    var Industry = $("#MainContent_ddlIndustry").val();
    var Monthly_Expected = $("#MainContent_tab6_monthly_expected").val();
    var comments = $("#MainContent_tab6_comments").val(); 
    var finalstage = $("#MainContent_Stages").val();

    if (finalstage == 6 && Completion_Date != "") {

        if ($('#MainContent_tab6_radio_success').is(':checked') || $('#MainContent_tab6_radio_failure').is(':checked')) {
            returnVal = true;
        }
        else {
            alert('Please select The Checkbox For Success or Failure')
        }

        if ($("#MainContent_tab6_radio_success").is(':checked')) {
            if (Monthly_Expected == "") {
                $('#MainContent_tab6_monthly_expected').css('border-color', 'red');
                returnVal = false;
            }
            else {
                $('#MainContent_tab6_monthly_expected').css('border-color', '');
            }
        }

        if (comments == "") {
            $("#MainContent_tab6_comments").css('border-color', 'red');
            returnVal = false;
        }
        else {
            $("#MainContent_tab6_comments").css('border-color', '');
        }

    }
    if (Product == "") {
        $(".lblProduct").show();
        returnVal = false;
    }
    else {
        $(".lblProduct").hide();

    }

    if (Component == "") {
        $(".lblComponent").show();
        returnVal = false;
    }
    else {
        $(".lblComponent").hide();

    }
    if (Competition == "") {
        $(".lblCompetition").show();
        returnVal = false;
    }
    else {
        $(".lblCompetition").hide();

    }
    if (Stages == "--SELECT STAGES--") {
        $(".lblStages").show();
        returnVal = false;
    }
    else {
        $(".lblStages").hide();

    }
    if (Main_Target == "") {
        $(".lblTarget").show();
        returnVal = false;
    }
    else {
        $(".lblTarget").hide();

    }
    if (OverAll_Potential == "") {
        $(".lblover_Pot").show();
        returnVal = false;
    }
    else {
        $(".lblover_Pot").hide();

    }

    if (Pot_Lakhs == "") {
        $(".lblPot_Lakhs").show();
        returnVal = false;
    }
    else {
        $(".lblPot_Lakhs").hide();

    }

    if (Business == "") {
        $(".lblBusiness_Exp").show();
        returnVal = false;
    }
    else {
        $(".lblBusiness_Exp").hide();

    }

    if (Target_Date == "") {
        $(".lbltab6Target").show();
        returnVal = false;
    }
    else {
        $(".lbltab6Target").hide();

    }
    if (Completion_Date == "") {
        $(".lbltab6Complete").show();
        $('#MainContent_tab6Completion').css('background-color', 'red');
        returnVal = false;
    }
    else {
        $('#MainContent_tab6Completion').css('background-color', '');
        $(".lbltab6Complete").hide();


    }
    if (Escalated_To == "-- SELECT --") {
        $(".lblEscalate").show();
        returnVal = false;
    }
    else {
        $(".lblEscalate").hide();


    }
    if (Industry == "-- SELECT --") {
        $(".lblIndustry").show();
        returnVal = false;
    }
    else {
        $(".lblIndustry").hide();


    }
    if (returnVal == false) {
        alert("Fields marked in red are required");
    }
    return returnVal;
}
function ValidateSubmitTab7() {
    var returnVal = true;
    var Target_Date = $("#MainContent_tab7Target").val();
    var Escalated_To = $("#MainContent_ddlEscalate").val();
    var Completion_Date = $("#MainContent_tab7Completion").val();
    var Customer_name = $("#MainContent_ddlCustomerName").val();
    var Customer_num = $("#MainContent_ddlCustomerNo").val();
    var Product = $("#MainContent_ExistingProduct").val();
    var Component = $("#MainContent_Component").val();
    var Competition = $("#MainContent_CompetitionSpec").val();
    var Stages = $("#MainContent_Stages").val();
    var Main_Target = $("#MainContent_TargetDate").val();
    var OverAll_Potential = $("#MainContent_OverAllPotential").val();
    var Pot_Lakhs = $("#MainContent_Potential").val();
    var Business = $("#MainContent_Business").val();
    var Industry = $("#MainContent_ddlIndustry").val();
    var Monthly_Expected = $("#MainContent_tab7_monthly_expected").val();
    var comments = $("#MainContent_tab7_comments").val();
    var finalstage = $("#MainContent_Stages").val();

    if (finalstage == 7 && Completion_Date != "") {

        if ($('#MainContent_tab7_radio_success').is(':checked') || $('#MainContent_tab7_radio_failure').is(':checked')) {
            returnVal = true;
        }
        else {
            alert('Please select The Checkbox For Success or Failure')
        }

        if ($("#MainContent_tab7_radio_success").is(':checked')) {
            if (Monthly_Expected == "") {
                $('#MainContent_tab7_monthly_expected').css('border-color', 'red');
                returnVal = false;
            }
            else {
                $('#MainContent_tab7_monthly_expected').css('border-color', '');
            }
        }

        if (comments == "") {
            $("#MainContent_tab7_comments").css('border-color', 'red');
            returnVal = false;
         }
        else {
            $("#MainContent_tab7_comments").css('border-color', '');
        }
    }

    if (Product == "") {
        $(".lblProduct").show();
        returnVal = false;
    }
    else {
        $(".lblProduct").hide();

    }

    if (Component == "") {
        $(".lblComponent").show();
        returnVal = false;
    }
    else {
        $(".lblComponent").hide();

    }
    if (Competition == "") {
        $(".lblCompetition").show();
        returnVal = false;
    }
    else {
        $(".lblCompetition").hide();

    }
    if (Stages == "--SELECT STAGES--") {
        $(".lblStages").show();
        returnVal = false;
    }
    else {
        $(".lblStages").hide();

    }
    if (Main_Target == "") {
        $(".lblTarget").show();
        returnVal = false;
    }
    else {
        $(".lblTarget").hide();

    }
    if (OverAll_Potential == "") {
        $(".lblover_Pot").show();
        returnVal = false;
    }
    else {
        $(".lblover_Pot").hide();

    }

    if (Pot_Lakhs == "") {
        $(".lblPot_Lakhs").show();
        returnVal = false;
    }
    else {
        $(".lblPot_Lakhs").hide();

    }

    if (Business == "") {
        $(".lblBusiness_Exp").show();
        returnVal = false;
    }
    else {
        $(".lblBusiness_Exp").hide();

    }

    if (Target_Date == "") {
        $(".lbltab7Target").show();
        returnVal = false;
    }
    else {
        $(".lbltab7Target").hide();

    }
    if (Completion_Date == "") {
        $(".lbltab7Complete").show();
        $('#MainContent_tab7Completion').css('background-color', 'red');
        returnVal = false;
    }
    else {
        $('#MainContent_tab7Completion').css('background-color', '');
        $(".lbltab7Complete").hide();


    }
    if (Escalated_To == "-- SELECT --") {
        $(".lblEscalate").show();
        returnVal = false;
    }
    else {
        $(".lblEscalate").hide();


    }
    if (Industry == "-- SELECT --") {
        $(".lblIndustry").show();
        returnVal = false;
    }
    else {
        $(".lblIndustry").hide();


    }
    if (returnVal == false) {
        alert("Fields marked in red are required");
    }
    return returnVal;
}
function ValidateSubmitTab8() {
    var returnVal = true;
    var Target_Date = $("#MainContent_tab8Target").val();
    var Escalated_To = $("#MainContent_ddlEscalate").val();
    var Completion_Date = $("#MainContent_tab8Completion").val();
    var Customer_name = $("#MainContent_ddlCustomerName").val();
    var Customer_num = $("#MainContent_ddlCustomerNo").val();
    var Product = $("#MainContent_ExistingProduct").val();
    var Component = $("#MainContent_Component").val();
    var Competition = $("#MainContent_CompetitionSpec").val();
    var Stages = $("#MainContent_Stages").val();
    var Main_Target = $("#MainContent_TargetDate").val();
    var OverAll_Potential = $("#MainContent_OverAllPotential").val();
    var Pot_Lakhs = $("#MainContent_Potential").val();
    var Business = $("#MainContent_Business").val();
    var Industry = $("#MainContent_ddlIndustry").val();
    var Monthly_Expected = $("#MainContent_tab8_monthly_expected").val();
    var comments = $("#MainContent_tab8_comments").val();   
    var finalstage = $("#MainContent_Stages").val();

    if (finalstage == 8 && Completion_Date != "") {
        if ($('#MainContent_tab8_radio_success').is(':checked') || $('#MainContent_tab8_radio_failure').is(':checked')) {
            returnVal = true;
        }
        else {
            alert('Please select The Checkbox For Success or Failure')
        }

        if ($("#MainContent_tab8_radio_success").is(':checked')) {
            if (Monthly_Expected == "") {
                $('#MainContent_tab8_monthly_expected').css('border-color', 'red');
                returnVal = false;
            }
            else {
                $('#MainContent_tab8_monthly_expected').css('border-color', '');
            }
        }

        if (comments == "") {
            $("#MainContent_tab8_comments").css('border-color', 'red');
            returnVal = false;
        }
        else {
            $("#MainContent_tab8_comments").css('border-color', '');
        }
    }

    if (Product == "") {
        $(".lblProduct").show();
        returnVal = false;
    }
    else {
        $(".lblProduct").hide();

    }

    if (Component == "") {
        $(".lblComponent").show();
        returnVal = false;
    }
    else {
        $(".lblComponent").hide();

    }
    if (Competition == "") {
        $(".lblCompetition").show();
        returnVal = false;
    }
    else {
        $(".lblCompetition").hide();

    }
    if (Stages == "--SELECT STAGES--") {
        $(".lblStages").show();
        returnVal = false;
    }
    else {
        $(".lblStages").hide();

    }
    if (Main_Target == "") {
        $(".lblTarget").show();
        returnVal = false;
    }
    else {
        $(".lblTarget").hide();

    }
    if (OverAll_Potential == "") {
        $(".lblover_Pot").show();
        returnVal = false;
    }
    else {
        $(".lblover_Pot").hide();

    }

    if (Pot_Lakhs == "") {
        $(".lblPot_Lakhs").show();
        returnVal = false;
    }
    else {
        $(".lblPot_Lakhs").hide();

    }

    if (Business == "") {
        $(".lblBusiness_Exp").show();
        returnVal = false;
    }
    else {
        $(".lblBusiness_Exp").hide();

    }

    if (Target_Date == "") {
        $(".lbltab8Target").show();
        returnVal = false;
    }
    else {
        $(".lbltab8Target").hide();

    }
    if (Completion_Date == "") {
        $(".lbltab8Complete").show();
        $('#MainContent_tab8Completion').css('background-color', 'red');
        returnVal = false;
    }
    else {
        $('#MainContent_tab8Completion').css('background-color', '');
        $(".lbltab8Complete").hide();


    }
    if (Escalated_To == "-- SELECT --") {
        $(".lblEscalate").show();
        returnVal = false;
    }
    else {
        $(".lblEscalate").hide();


    }
    if (Industry == "-- SELECT --") {
        $(".lblIndustry").show();
        returnVal = false;
    }
    else {
        $(".lblIndustry").hide();


    }
    if (returnVal == false) {
        alert("Fields marked in red are required");
    }
    return returnVal;
}
function ValidateSubmitTab9() {
    var returnVal = true;
    var Target_Date = $("#MainContent_tab9Target").val();
    var Escalated_To = $("#MainContent_ddlEscalate").val();
    var Completion_Date = $("#MainContent_tab9Completion").val();
    var Customer_name = $("#MainContent_ddlCustomerName").val();
    var Customer_num = $("#MainContent_ddlCustomerNo").val();
    var Product = $("#MainContent_ExistingProduct").val();
    var Component = $("#MainContent_Component").val();
    var Competition = $("#MainContent_CompetitionSpec").val();
    var Stages = $("#MainContent_Stages").val();
    var Main_Target = $("#MainContent_TargetDate").val();
    var OverAll_Potential = $("#MainContent_OverAllPotential").val();
    var Pot_Lakhs = $("#MainContent_Potential").val();
    var Business = $("#MainContent_Business").val();
    var Industry = $("#MainContent_ddlIndustry").val();
    var Monthly_Expected = $("#MainContent_tab9_monthly_expected").val();
    var comments = $("#MainContent_tab9_comments").val();   
    var finalstage = $("#MainContent_Stages").val();
    if (finalstage == 9 && Completion_Date != "") {

        if ($('#MainContent_tab9_radio_success').is(':checked') || $('#MainContent_tab9_radio_failure').is(':checked')) {
            returnVal = true;
        }
        else {
            alert('Please select The Checkbox For Success or Failure')
        }

        if ($("#MainContent_tab9_radio_success").is(':checked')) {
            if (Monthly_Expected == "") {
                $('#MainContent_tab9_monthly_expected').css('border-color', 'red');
                returnVal = false;
            }
            else {
                $('#MainContent_tab9_monthly_expected').css('border-color', '');
            }
        }

        if (comments == "") {
            $("#MainContent_tab9_comments").css('border-color', 'red');
            returnVal = false;
        }
        else {
            $("#MainContent_tab9_comments").css('border-color', '');
        }

    }
    if (Product == "") {
        $(".lblProduct").show();
        returnVal = false;
    }
    else {
        $(".lblProduct").hide();

    }

    if (Component == "") {
        $(".lblComponent").show();
        returnVal = false;
    }
    else {
        $(".lblComponent").hide();

    }
    if (Competition == "") {
        $(".lblCompetition").show();
        returnVal = false;
    }
    else {
        $(".lblCompetition").hide();

    }
    if (Stages == "--SELECT STAGES--") {
        $(".lblStages").show();
        returnVal = false;
    }
    else {
        $(".lblStages").hide();

    }
    if (Main_Target == "") {
        $(".lblTarget").show();
        returnVal = false;
    }
    else {
        $(".lblTarget").hide();

    }
    if (OverAll_Potential == "") {
        $(".lblover_Pot").show();
        returnVal = false;
    }
    else {
        $(".lblover_Pot").hide();

    }

    if (Pot_Lakhs == "") {
        $(".lblPot_Lakhs").show();
        returnVal = false;
    }
    else {
        $(".lblPot_Lakhs").hide();

    }

    if (Business == "") {
        $(".lblBusiness_Exp").show();
        returnVal = false;
    }
    else {
        $(".lblBusiness_Exp").hide();

    }

    if (Target_Date == "") {
        $(".lbltab9Target").show();
        returnVal = false;
    }
    else {
        $(".lbltab9Target").hide();

    }
    if (Completion_Date == "") {
        $(".lbltab9Complete").show();
        $('#MainContent_tab9Completion').css('background-color', 'red');
        returnVal = false;
    }
    else {
        $('#MainContent_tab9Completion').css('background-color', '');
        $(".lbltab9Complete").hide();


    }
    if (Escalated_To == "-- SELECT --") {
        $(".lblEscalate").show();
        returnVal = false;
    }
    else {
        $(".lblEscalate").hide();


    }
    if (Industry == "-- SELECT --") {
        $(".lblIndustry").show();
        returnVal = false;
    }
    else {
        $(".lblIndustry").hide();


    }
    if (returnVal == false) {
        alert("Fields marked in red are required");
    }
    return returnVal;
}
function ValidateSubmitTab10() {
    var returnVal = true;
    var Target_Date = $("#MainContent_tab10Target").val();
    var Escalated_To = $("#MainContent_ddlEscalate").val();
    var Completion_Date = $("#MainContent_tab10Completion").val();
    var Customer_name = $("#MainContent_ddlCustomerName").val();
    var Customer_num = $("#MainContent_ddlCustomerNo").val();
    var Product = $("#MainContent_ExistingProduct").val();
    var Component = $("#MainContent_Component").val();
    var Competition = $("#MainContent_CompetitionSpec").val();
    var Stages = $("#MainContent_Stages").val();
    var Main_Target = $("#MainContent_TargetDate").val();
    var OverAll_Potential = $("#MainContent_OverAllPotential").val();
    var Pot_Lakhs = $("#MainContent_Potential").val();
    var Business = $("#MainContent_Business").val();
    var Industry = $("#MainContent_ddlIndustry").val();
    var Monthly_Expected = $("#MainContent_tab10_monthly_expected").val();
    var comments = $("#MainContent_tab10_comments").val();  
    var finalstage = $("#MainContent_Stages").val();

    if (finalstage == 10 && Completion_Date != "") {

        if ($('#MainContent_tab10_radio_success').is(':checked') || $('#MainContent_tab10_radio_failure').is(':checked')) {
            returnVal = true;
        }
        else {
            alert('Please select The Checkbox For Success or Failure')
        }

        if ($("#MainContent_tab10_radio_success").is(':checked')) {
            if (Monthly_Expected == "") {
                $('#MainContent_tab10_monthly_expected').css('border-color', 'red');
                returnVal = false;
            }
            else {
                $('#MainContent_tab10_monthly_expected').css('border-color', '');
            }
        }

        if (comments == "") {
            $("#MainContent_tab10_comments").css('border-color', 'red');
            returnVal = false;
        }
        else {
            $("#MainContent_tab10_comments").css('border-color', '');
        }
    }

    if (Product == "") {
        $(".lblProduct").show();
        returnVal = false;
        $('#MainContent_ExistingProduct').css('border-color', 'red');
    }
    else {
        $(".lblProduct").hide();
        $('#MainContent_ExistingProduct').css('border-color', '');
    }

    if (Component == "") {
        $(".lblComponent").show();
        returnVal = false;
        $('#MainContent_Component').css('border-color', 'red');
    }
    else {
        $(".lblComponent").hide();
        $('#MainContent_Component').css('border-color', '');
    }
    if (Competition == "") {
        $(".lblCompetition").show();
        returnVal = false;
        $('#MainContent_CompetitionSpec').css('border-color', 'red');
    }
    else {
        $(".lblCompetition").hide();
        $('#MainContent_CompetitionSpec').css('border-color', '');

    }
    if (Stages == "--SELECT STAGES--") {
        $(".lblStages").show();
        returnVal = false;
        $('#MainContent_Stages').css('border-color', 'red');
    }
    else {
        $(".lblStages").hide();
        $('#MainContent_Stages').css('border-color', '');

    }
    if (Main_Target == "") {
        $(".lblTarget").show();
        $('#MainContent_TargetDate').css('border-color', 'red');
        returnVal = false;
    }
    else {
        $(".lblTarget").hide();
        $('#MainContent_TargetDate').css('border-color', '');
    }
    if (OverAll_Potential == "") {
        $(".lblover_Pot").show();
        returnVal = false;
        $('#MainContent_OverAllPotential').css('border-color', 'red');
    }
    else {
        $(".lblover_Pot").hide();
        $('#MainContent_OverAllPotential').css('border-color', '');
    }

    if (Pot_Lakhs == "") {
        $(".lblPot_Lakhs").show();
        returnVal = false;
        $('#MainContent_Potential').css('border-color', 'red');
    }
    else {
        $(".lblPot_Lakhs").hide();
        $('#MainContent_Potential').css('border-color', '');
    }

    if (Business == "") {
        $(".lblBusiness_Exp").show();
        $('#MainContent_Business').css('border-color', 'red');
        returnVal = false;
    }
    else {
        $(".lblBusiness_Exp").hide();
        $('#MainContent_Business').css('border-color', '');
    }

    if (Target_Date == "") {
        $(".lbltab10Target").show();
        returnVal = false;
    }
    else {
        $(".lbltab10Target").hide();

    }
    if (Completion_Date == "") {
        $(".lbltab10Complete").show();
        $('#MainContent_tab10Completion').css('background-color', 'red');
        returnVal = false;
    }
    else {
        $('#MainContent_tab10Completion').css('background-color', '');
        $(".lbltab10Complete").hide();
    }
    if (Escalated_To == "-- SELECT --") {
        $(".lblEscalate").show();
        returnVal = false;
    }
    else {
        $(".lblEscalate").hide();
    }

    if (Industry == "-- SELECT --") {
        $(".lblIndustry").show();
        returnVal = false;
    }
    else {
        $(".lblIndustry").hide();
    }
    if (returnVal == false) {
        alert("Fields marked in red are required");
    }
    return returnVal;
}

function LoadSameTab() {

    var tab_num = $("#MainContent_tab_number").val();

    if (tab_num == '2') {
        $('a[href*=tab_1_1]').parent().removeClass('active');
        $("#tab_1_1").removeClass('active');

        $("#tab_1_2").addClass('active');
        $('a[href*=tab_1_2]').parent().addClass('active');

        $("#tab_1_1").css('display', 'none');
        $("#tab_1_2").css('display', 'block');

    }

    if (tab_num == '3') {
        $('a[href*=tab_1_1]').parent().removeClass('active');
        $("#tab_1_1").removeClass('active');

        $("#tab_1_3").addClass('active');
        $('a[href*=tab_1_3]').parent().addClass('active');
        $("#tab_1_1").hide();
        $("#tab_1_3").show();
    }

    if (tab_num == '4') {
        $('a[href*=tab_1_1]').parent().removeClass('active');
        $("#tab_1_1").removeClass('active');

        $("#tab_1_4").addClass('active');
        $('a[href*=tab_1_4]').parent().addClass('active');
        $("#tab_1_1").hide();
        $("#tab_1_4").show();
    }

    if (tab_num == '5') {
        $('a[href*=tab_1_1]').parent().removeClass('active');
        $("#tab_1_1").removeClass('active');

        $("#tab_1_5").addClass('active');
        $('a[href*=tab_1_5]').parent().addClass('active');
        $("#tab_1_1").hide();
        $("#tab_1_5").show();
    }

    if (tab_num == '6') {
        $('a[href*=tab_1_1]').parent().removeClass('active');
        $("#tab_1_1").removeClass('active');

        $("#tab_1_6").addClass('active');
        $('a[href*=tab_1_6]').parent().addClass('active');
        $("#tab_1_1").hide();
        $("#tab_1_6").show();
    }

    if (tab_num == '7') {
        $('a[href*=tab_1_1]').parent().removeClass('active');
        $("#tab_1_1").removeClass('active');

        $("#tab_1_7").addClass('active');
        $('a[href*=tab_1_7]').parent().addClass('active');
        $("#tab_1_1").hide();
        $("#tab_1_7").show();
    }

    if (tab_num == '8') {
        $('a[href*=tab_1_1]').parent().removeClass('active');
        $("#tab_1_1").removeClass('active');

        $("#tab_1_8").addClass('active');
        $('a[href*=tab_1_8]').parent().addClass('active');
        $("#tab_1_1").hide();
        $("#tab_1_8").show();
    }

    if (tab_num == '9') {
        $('a[href*=tab_1_1]').parent().removeClass('active');
        $("#tab_1_1").removeClass('active');

        $("#tab_1_9").addClass('active');
        $('a[href*=tab_1_9]').parent().addClass('active');
        $("#tab_1_1").hide();
        $("#tab_1_9").show();
    }

    if (tab_num == '10') {
        $('a[href*=tab_1_1]').parent().removeClass('active');
        $("#tab_1_1").removeClass('active');

        $("#tab_1_10").addClass('active');
        $('a[href*=tab_1_10]').parent().addClass('active');
        $("#tab_1_1").hide();
        $("#tab_1_10").show();
    }

}
function AddClassWhenDisabled() {
    //code commented by suresh
    //$('#MainContent_ddlCustomerName').addClass("form-control");
   // debugger;
    $("#MainContent_ExistingProduct").addClass("form-control");
    $("#MainContent_Component").addClass("form-control");
    $("#MainContent_CompetitionSpec").addClass("form-control");
    $("#MainContent_TargetDate").addClass("form-control");
    $("#MainContent_OverAllPotential").addClass("form-control");
    $("#MainContent_Potential").addClass("form-control");
    $("#MainContent_Business").addClass("form-control");
   // $("#MainContent_ddlTitle").addClass("form-control");
    $("#MainContent_txtTitle").addClass("form-control");
    $("#MainContent_ddlProjectType").addClass("form-control");
    $("#MainContent_tab1Completion").addClass("form-control");
    $("#MainContent_tab2Completion").addClass("form-control");
    $("#MainContent_tab3Completion").addClass("form-control");
    $("#MainContent_tab4Completion").addClass("form-control");
    $("#MainContent_tab5Completion").addClass("form-control");
    $("#MainContent_tab6Completion").addClass("form-control");
    $("#MainContent_tab7Completion").addClass("form-control");
    $("#MainContent_tab8Completion").addClass("form-control");
    $("#MainContent_tab9Completion").addClass("form-control");
    $("#MainContent_tab10Completion").addClass("form-control");

    $("#MainContent_tab1Target").addClass("form-control");
    $("#MainContent_tab2Target").addClass("form-control");
    $("#MainContent_tab3Target").addClass("form-control");
    $("#MainContent_tab4Target").addClass("form-control");
    $("#MainContent_tab5Target").addClass("form-control");
    $("#MainContent_tab6Target").addClass("form-control");
    $("#MainContent_tab7Target").addClass("form-control");
    $("#MainContent_tab8Target").addClass("form-control");
    $("#MainContent_tab9Target").addClass("form-control");
    $("#MainContent_tab10Target").addClass("form-control");


    $("#MainContent_tab1Target").addClass("form-control");
    $("#MainContent_tab2_comments").addClass("form-control");
    $("#MainContent_tab3_comments").addClass("form-control");
    $("#MainContent_tab4_comments").addClass("form-control");
    $("#MainContent_tab5_comments").addClass("form-control");
    $("#MainContent_tab6_comments").addClass("form-control");
    $("#MainContent_tab7_comments").addClass("form-control");
    $("#MainContent_tab8_comments").addClass("form-control");
    $("#MainContent_tab9_comments").addClass("form-control");
    $("#MainContent_tab10_comments").addClass("form-control");

    $("#MainContent_tab1_txtApprove").addClass("form-control");
    $("#MainContent_tab2_txtApprove").addClass("form-control");
    $("#MainContent_tab3_txtApprove").addClass("form-control");
    $("#MainContent_tab4_txtApprove").addClass("form-control");
    $("#MainContent_tab5_txtApprove").addClass("form-control");
    $("#MainContent_tab6_txtApprove").addClass("form-control");
    $("#MainContent_tab7_txtApprove").addClass("form-control");
    $("#MainContent_tab8_txtApprove").addClass("form-control");
    $("#MainContent_tab9_txtApprove").addClass("form-control");
    $("#MainContent_tab10_txtApprove").addClass("form-control");

    //$("#MainContent_tab1btnRemarks").addClass("btn green");
    //$("#MainContent_tab2btnRemarks").addClass("btn green");
    //$("#MainContent_tab3btnRemarks").addClass("btn green");
    //$("#MainContent_tab4btnRemarks").addClass("btn green");
    //$("#MainContent_tab5btnRemarks").addClass("btn green");
    //$("#MainContent_tab6btnRemarks").addClass("btn green");
    //$("#MainContent_tab7btnRemarks").addClass("btn green");
    //$("#MainContent_tab8btnRemarks").addClass("btn green");
    //$("#MainContent_tab9btnRemarks").addClass("btn green");
    //$("#MainContent_tab10btnRemarks").addClass("btn green");
    $("#MainContent_tab8saveRemarks").removeClass("btn green");
    
}

function ValidateFreeze() {
    var returnVal = true;
    var Customer_name = $("#MainContent_ddlCustomerName").val();
    var Customer_num = $("#MainContent_ddlCustomerNo").val();
    var Product = $("#MainContent_ExistingProduct").val();
    var Component = $("#MainContent_Component").val();
    var Competition = $("#MainContent_CompetitionSpec").val();
    var Stages = $("#MainContent_Stages").val();
    var Main_Target = $("#MainContent_TargetDate").val();
    var OverAll_Potential = $("#MainContent_OverAllPotential").val();
    var Pot_Lakhs = $("#MainContent_Potential").val();
    var Business = $("#MainContent_Business").val();
    var Industry = $("#MainContent_ddlIndustry").val();
    var Escalated_To = $("#MainContent_ddlEscalate").val();
    var Owner = $("#MainContent_ddlSalesEngName").val();
    var Reviewer = $("#MainContent_ddlReviewer").val();
    if (Product == "") {
        $(".lblProduct").show();
        returnVal = false;
        $('#MainContent_ExistingProduct').css('border-color', 'red');
    }
    else {
        $(".lblProduct").hide();
        $('#MainContent_ExistingProduct').css('border-color', '');
    }

    if (Component == "") {
        $(".lblComponent").show();
        returnVal = false;
        $('#MainContent_Component').css('border-color', 'red');
    }
    else {
        $(".lblComponent").hide();
        $('#MainContent_Component').css('border-color', '');

    }
    if (Competition == "") {
        $(".lblCompetition").show();
        returnVal = false;
        $('#MainContent_CompetitionSpec').css('border-color', 'red');
    }
    else {
        $(".lblCompetition").hide();
        $('#MainContent_CompetitionSpec').css('border-color', '');

    }
    if (Stages == "--SELECT STAGES--") {
        $(".lblStages").show();
        returnVal = false;
        $('#MainContent_Stages').css('border-color', 'red');
    }
    else {
        $(".lblStages").hide();
        $('#MainContent_Stages').css('border-color', '');

    }
    if (Main_Target == "") {
        $(".lblTarget").show();
        $('#MainContent_TargetDate').css('border-color', 'red');
        returnVal = false;
    }
    else {
        $(".lblTarget").hide();
        $('#MainContent_TargetDate').css('border-color', '');
    }
    if (OverAll_Potential == "") {
        $(".lblover_Pot").show();
        returnVal = false;
        $('#MainContent_OverAllPotential').css('border-color', 'red');
    }
    else {
        $(".lblover_Pot").hide();
        $('#MainContent_OverAllPotential').css('border-color', '');
    }

    if (Pot_Lakhs == "") {
        $(".lblPot_Lakhs").show();
        returnVal = false;
        $('#MainContent_Potential').css('border-color', 'red');
    }
    else {
        $(".lblPot_Lakhs").hide();
        $('#MainContent_Potential').css('border-color', '');
    }

    if (Business == "") {
        $(".lblBusiness_Exp").show();
        $('#MainContent_Business').css('border-color', 'red');
        returnVal = false;
    }
    else {
        $(".lblBusiness_Exp").hide();
        $('#MainContent_Business').css('border-color', '');
    }


    if (Escalated_To == "-- SELECT --") {
        $(".lblEscalate").show();
        $('#MainContent_ddlEscalate').css('border-color', 'red');
        returnVal = false;
    }
    else {
        $(".lblEscalate").hide();
        $('#MainContent_ddlEscalate').css('border-color', '');

    }

    if (Owner == "-- SELECT --") {

        $('#MainContent_ddlSalesEngName').css('border-color', 'red');
        returnVal = false;
    }
    else {

        $('#MainContent_ddlSalesEngName').css('border-color', '');

    }

    if (Reviewer == "-- SELECT --") {

        $('#MainContent_ddlReviewer').css('border-color', 'red');
        returnVal = false;
    }
    else {

        $('#MainContent_ddlReviewer').css('border-color', '');

    }

    if (Industry == "-- SELECT --") {
        $(".lblIndustry").show();
        returnVal = false;
        $('#MainContent_ddlIndustry').css('border-color', 'red');
    }
    else {
        $(".lblIndustry").hide();
        $('#MainContent_ddlIndustry').css('border-color', '');


    }
    if (returnVal == true) {
        if (confirm("Once submitted fields can not be edited by the Owner. Do you want to continue?")) {
            return returnVal;
        }
        else {

            return false;
        }

    }
    return returnVal;
}

function DisableTabs() {
   
    if ($("#MainContent_tab1Target").val() == "") {
        jQuery(function ($) {
            $("#tab_home").tabs({
                active: [0],
                disabled: [1, 2, 3, 4, 5, 6, 7, 8, 9]

            });
        });
    }

    if ($("#MainContent_tab1Target").val() != "") {
        jQuery(function ($) {
            $("#tab_home").tabs({
                active: [0, 1],
                disabled: [2, 3, 4, 5, 6, 7, 8, 9]

            });
        });
    }

    if ($("#MainContent_tab2Target").val() != "") {
        jQuery(function ($) {
            $("#tab_home").tabs({
                active: [0, 1, 2],
                disabled: [3, 4, 5, 6, 7, 8, 9]

            });
        });
    }
    if ($("#MainContent_tab3Target").val() != "") {
        jQuery(function ($) {
            $("#tab_home").tabs({
                active: [0, 1, 2, 3],
                disabled: [4, 5, 6, 7, 8, 9]

            });
        });
    }

    if ($("#MainContent_tab4Target").val() != "") {
        jQuery(function ($) {
            $("#tab_home").tabs({
                active: [0, 1, 2, 3, 4],
                disabled: [5, 6, 7, 8, 9]

            });
        });
    }

    if ($("#MainContent_tab5Target").val() != "") {
        jQuery(function ($) {
            $("#tab_home").tabs({
                active: [0, 1, 2, 3, 4, 5],
                disabled: [6, 7, 8, 9]

            });
        });
    }

    if ($("#MainContent_tab6Target").val() != "") {
        jQuery(function ($) {
            $("#tab_home").tabs({
                active: [0, 1, 2, 3, 4, 5, 6],
                disabled: [7, 8, 9]

            });
        });
    }

    if ($("#MainContent_tab7Target").val() != "") {
        jQuery(function ($) {
            $("#tab_home").tabs({
                active: [0, 1, 2, 3, 4, 5, 6, 7],
                disabled: [8, 9]

            });
        });
    }

    if ($("#MainContent_tab8Target").val() != "") {
        jQuery(function ($) {
            $("#tab_home").tabs({
                active: [0, 1, 2, 3, 4, 5, 6, 7, 8, ],
                disabled: [9]

            });
        });
    }

    if ($("#MainContent_tab9Target").val() != "") {
        jQuery(function ($) {
            $("#tab_home").tabs({
                active: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
                disabled: []
            });
        });
    }
}


function triggerPostGridLodedActions() {
    $('#collapsebtn').unbind('click').bind('click', function (e) {
        var attr = $('#product_image').attr('src');
        $("#dvcustomer").slideToggle();
        if (attr == "images/button_minus.gif") {
            $("#product_image").attr("src", "images/button_plus.gif");
        } else {
            $("#product_image").attr("src", "images/button_minus.gif");
        }
        return false;
    });
    $('#pot_collapse').unbind('click').bind('click', function (e) {
        var attr = $('#linegrid_image').attr('src');
        $("#dvpotential").slideToggle();
        if (attr == "images/button_minus.gif") {
            $("#linegrid_image").attr("src", "images/button_plus.gif");
        } else {
            $("#linegrid_image").attr("src", "images/button_minus.gif");
        }
    });
    $('#det_collapse').unbind('click').bind('click', function (e) {
        var attr = $('#familygrid_image').attr('src');
        $("#details").slideToggle();
        if (attr == "images/button_minus.gif") {
            $("#familygrid_image").attr("src", "images/button_plus.gif");
        } else {
            $("#familygrid_image").attr("src", "images/button_minus.gif");
        }
    });
}
function triggerPostGridLodedActions1() {
    $("#dvcustomer").slideToggle();
    $("#dvpotential").slideToggle();
    $("#details").slideToggle();
}

$("#MainContent_tab1Completion").datepicker({
    showOn: "both",
    buttonImageOnly: true,
    buttonText: "",
    changeYear: true,
    changeMonth: true,
    yearRange: "c-60:c+60",
    minDate: -7, // new Date(),
    dateFormat: 'dd/mm/yy',
    buttonImage: "images/calander_icon.png",
    onSelect: function () {
        $(".tab1_hideout_every").show();
        var finalstage = $("#MainContent_Stages").val();
        if (finalstage == 1) {
            $("#MainContent_tab1_radio_success").attr('checked', false);
            $(".tab1_hideout").show();

        }
    }
});

$("#MainContent_tab2Completion").datepicker({
    showOn: "both",
    buttonImageOnly: true,
    buttonText: "",
    changeYear: true,
    changeMonth: true,
    yearRange: "c-60:c+60",
    minDate: -7, //  new Date(),
    dateFormat: 'dd/mm/yy',
    buttonImage: "images/calander_icon.png",
    onSelect: function () {
        $(".tab2_hideout_every").show();
        var finalstage = $("#MainContent_Stages").val();
        if (finalstage == 2) {
            $("#MainContent_tab2_radio_success").attr('checked', false);
            $(".tab2_hideout").show();
        }
    }
});

$("#MainContent_tab3Completion").datepicker({
    showOn: "both",
    buttonImageOnly: true,
    buttonText: "",
    changeYear: true,
    changeMonth: true,
    yearRange: "c-60:c+60",
    minDate: -7, //  new Date(),
    dateFormat: 'dd/mm/yy',
    buttonImage: "images/calander_icon.png",
    onSelect: function () {
        $(".tab3_hideout_every").show();
        var finalstage = $("#MainContent_Stages").val();
        if (finalstage == 3) {
            $("#MainContent_tab3_radio_success").attr('checked', false);
            $(".tab3_hideout").show();
        }
    }
});

$("#MainContent_tab4Completion").datepicker({
    showOn: "both",
    buttonImageOnly: true,
    buttonText: "",
    changeYear: true,
    changeMonth: true,
    yearRange: "c-60:c+60",
    minDate: -7, // new Date(),
    dateFormat: 'dd/mm/yy',
    buttonImage: "images/calander_icon.png",
    onSelect: function () {
        $(".tab4_hideout_every").show();
        var finalstage = $("#MainContent_Stages").val();
        if (finalstage == 4) {
            $("#MainContent_tab4_radio_success").attr('checked', false);
            $(".tab4_hideout").show();
        }
    }
});

$("#MainContent_tab5Completion").datepicker({
    showOn: "both",
    buttonImageOnly: true,
    buttonText: "",
    changeYear: true,
    changeMonth: true,
    yearRange: "c-60:c+60",
    minDate: -7, //  new Date(),
    dateFormat: 'dd/mm/yy',
    buttonImage: "images/calander_icon.png",
    onSelect: function () {
        $(".tab5_hideout_every").show();
        var finalstage = $("#MainContent_Stages").val();
        if (finalstage == 5) {
            $("#MainContent_tab5_radio_success").attr('checked', false);
            $(".tab5_hideout").show();
        }
    }
});

$("#MainContent_tab6Completion").datepicker({
    showOn: "both",
    buttonImageOnly: true,
    buttonText: "",
    changeYear: true,
    changeMonth: true,
    yearRange: "c-60:c+60",
    minDate: -7, // new Date(),
    dateFormat: 'dd/mm/yy',
    buttonImage: "images/calander_icon.png",
    onSelect: function () {
        $(".tab6_hideout_every").show();
        var finalstage = $("#MainContent_Stages").val();
        if (finalstage == 6) {
            $("#MainContent_tab6_radio_success").attr('checked', false);
            $(".tab6_hideout").show();
        }
    }
});

$("#MainContent_tab7Completion").datepicker({
    showOn: "both",
    buttonImageOnly: true,
    buttonText: "",
    changeYear: true,
    changeMonth: true,
    yearRange: "c-60:c+60",
    minDate: -7, // new Date(),
    dateFormat: 'dd/mm/yy',
    buttonImage: "images/calander_icon.png",
    onSelect: function () {
        $(".tab7_hideout_every").show();
        var finalstage = $("#MainContent_Stages").val();
        if (finalstage == 7) {
            $("#MainContent_tab7_radio_success").attr('checked', false);
            $(".tab7_hideout").show();
        }
    }
});

$("#MainContent_tab8Completion").datepicker({
    showOn: "both",
    buttonImageOnly: true,
    buttonText: "",
    changeYear: true,
    changeMonth: true,
    yearRange: "c-60:c+60",
    minDate: -7, // new Date(),
    dateFormat: 'dd/mm/yy',
    buttonImage: "images/calander_icon.png",
    onSelect: function () {
        $(".tab8_hideout_every").show();
        var finalstage = $("#MainContent_Stages").val();
        if (finalstage == 8) {
            $("#MainContent_tab8_radio_success").attr('checked', false);
            $(".tab8_hideout").show();
        }
    }
});

$("#MainContent_tab9Completion").datepicker({
    showOn: "both",
    buttonImageOnly: true,
    buttonText: "",
    changeYear: true,
    changeMonth: true,
    yearRange: "c-60:c+60",
    minDate: -7, // new Date(),
    dateFormat: 'dd/mm/yy',
    buttonImage: "images/calander_icon.png",
    onSelect: function () {
        $(".tab9_hideout_every").show();
        var finalstage = $("#MainContent_Stages").val();
        if (finalstage == 9) {
            $("#MainContent_tab9_radio_success").attr('checked', false);
            $(".tab9_hideout").show();
        }
    }
});

$("#MainContent_tab10Completion").datepicker({
    showOn: "both",
    buttonImageOnly: true,
    buttonText: "",
    changeYear: true,
    changeMonth: true,
    yearRange: "c-60:c+60",
    minDate: -7, // new Date(),
    dateFormat: 'dd/mm/yy',
    buttonImage: "images/calander_icon.png",
    onSelect: function () {
        $(".tab10_hideout_every").show();
        var finalstage = $("#MainContent_Stages").val();
        if (finalstage == 10) {
            $("#MainContent_tab10_radio_success").attr('checked', false);
            $(".tab10_hideout").show();
        }
    }
});
$("#MainContent_tab1_radio_success").click(function () {
    if ($("#MainContent_tab1_radio_success").is(':checked')) {
        $(".tab1_monthly").show();
    }
});
$("#MainContent_tab2_radio_success").click(function () {
    if ($("#MainContent_tab2_radio_success").is(':checked')) {
        $(".tab2_monthly").show();
    }
});
$("#MainContent_tab3_radio_success").click(function () {
    if ($("#MainContent_tab3_radio_success").is(':checked')) {
        $(".tab3_monthly").show();
    }
});
$("#MainContent_tab4_radio_success").click(function () {
    if ($("#MainContent_tab4_radio_success").is(':checked')) {
        $(".tab4_monthly").show();
    }
});
$("#MainContent_tab5_radio_success").click(function () {
    if ($("#MainContent_tab5_radio_success").is(':checked')) {
        $(".tab5_monthly").show();
    }
});
$("#MainContent_tab6_radio_success").click(function () {
    if ($("#MainContent_tab6_radio_success").is(':checked')) {
        $(".tab6_monthly").show();
    }
});
$("#MainContent_tab7_radio_success").click(function () {
    if ($("#MainContent_tab7_radio_success").is(':checked')) {
        $(".tab7_monthly").show();
    }
});
$("#MainContent_tab8_radio_success").click(function () {
    if ($("#MainContent_tab8_radio_success").is(':checked')) {
        $(".tab8_monthly").show();
    }
});
$("#MainContent_tab9_radio_success").click(function () {
    if ($("#MainContent_tab9_radio_success").is(':checked')) {
        $(".tab9_monthly").show();
    }
});
$("#MainContent_tab10_radio_success").click(function () {
    if ($("#MainContent_tab10_radio_success").is(':checked')) {
        $(".tab10_monthly").show();
    }
});

$("#MainContent_tab1_radio_failure").click(function () {
    if ($("#MainContent_tab1_radio_failure").is(':checked')) {
        $(".tab1_monthly").hide();
    }
});
$("#MainContent_tab2_radio_failure").click(function () {
    if ($("#MainContent_tab2_radio_failure").is(':checked')) {
        $(".tab2_monthly").hide();
    }
});
$("#MainContent_tab3_radio_failure").click(function () {
    if ($("#MainContent_tab3_radio_failure").is(':checked')) {
        $(".tab3_monthly").hide();
    }
});
$("#MainContent_tab4_radio_failure").click(function () {
    if ($("#MainContent_tab4_radio_failure").is(':checked')) {
        $(".tab4_monthly").hide();
    }
});
$("#MainContent_tab5_radio_failure").click(function () {
    if ($("#MainContent_tab5_radio_failure").is(':checked')) {
        $(".tab5_monthly").hide();
    }
});
$("#MainContent_tab6_radio_failure").click(function () {
    if ($("#MainContent_tab6_radio_failure").is(':checked')) {
        $(".tab6_monthly").hide();
    }
});
$("#MainContent_tab7_radio_failure").click(function () {
    if ($("#MainContent_tab7_radio_failure").is(':checked')) {
        $(".tab7_monthly").hide();
    }
});
$("#MainContent_tab8_radio_failure").click(function () {
    if ($("#MainContent_tab8_radio_failure").is(':checked')) {
        $(".tab8_monthly").hide();
    }
});
$("#MainContent_tab9_radio_failure").click(function () {
    if ($("#MainContent_tab9_radio_failure").is(':checked')) {
        $(".tab9_monthly").hide();
    }
});
$("#MainContent_tab10_radio_failure").click(function () {
    if ($("#MainContent_tab10_radio_failure").is(':checked')) {
        $(".tab10_monthly").hide();
    }
});

$("#MainContent_existing_project").change(function () {
    $("#dvnew_project").hide();
    $("#dvexisting_project").show();
});
$("#MainContent_new_project").change(function () {
    $("#dvnew_project").show();
    $("#dvexisting_project").hide();
});
$("#MainContent_Stages").change(function (e) {
    ShowHideStages();
});
$("#MainContent_direct_customer").change(function () {
    $("#dv_direct_customer").show();
    $("#dv_channel_partner_1").hide();
    $("#dv_channel_partner_2").hide();
});
$("#MainContent_channel_partner").change(function () {

    $("#dv_direct_customer").hide();
    $("#dv_channel_partner_1").show();
    $("#dv_channel_partner_2").show();
});

$("#MainContent_tab1Target").keyup(function (e) {
    e.preventDefault();
    $("#MainContent_tab1Target").val("");
    alert("Please use datepicker to enter date.");
});
$("#MainContent_tab2Target").keyup(function (e) {
    e.preventDefault();
    $("#MainContent_tab2Target").val("");
    alert("Please use datepicker to enter date.");
});
$("#MainContent_tab3Target").keyup(function (e) {
    e.preventDefault();
    $("#MainContent_tab3Target").val("");
    alert("Please use datepicker to enter date.");
});
$("#MainContent_tab4Target").keyup(function (e) {
    e.preventDefault();
    $("#MainContent_tab4Target").val("");
    alert("Please use datepicker to enter date.");
});
$("#MainContent_tab5Target").keyup(function (e) {
    e.preventDefault();
    $("#MainContent_tab5Target").val("");
    alert("Please use datepicker to enter date.");
});
$("#MainContent_tab6Target").keyup(function (e) {
    e.preventDefault();
    $("#MainContent_tab6Target").val("");
    alert("Please use datepicker to enter date.");
});
$("#MainContent_tab7Target").keyup(function (e) {
    e.preventDefault();
    $("#MainContent_tab7Target").val("");
    alert("Please use datepicker to enter date.");
});
$("#MainContent_tab8Target").keyup(function (e) {
    e.preventDefault();
    $("#MainContent_tab8Target").val("");
    alert("Please use datepicker to enter date.");
});
$("#MainContent_tab9Target").keyup(function (e) {
    e.preventDefault();
    $("#MainContent_tab9Target").val("");
    alert("Please use datepicker to enter date.");
});
$("#MainContent_tab10Target").keyup(function (e) {
    e.preventDefault();
    $("#MainContent_tab10Target").val("");
    alert("Please use datepicker to enter date.");
});

$("#MainContent_tab1Completion").keyup(function (e) {
    e.preventDefault();
    $("#MainContent_tab1Completion").val("");
    alert("Please use datepicker to enter date.");
});
$("#MainContent_tab2Completion").keyup(function (e) {
    e.preventDefault();
    $("#MainContent_tab2Completion").val("");
    alert("Please use datepicker to enter date.");
});
$("#MainContent_tab3Completion").keyup(function (e) {
    e.preventDefault();
    $("#MainContent_tab3Completion").val("");
    alert("Please use datepicker to enter date.");
});
$("#MainContent_tab4Completion").keyup(function (e) {
    e.preventDefault();
    $("#MainContent_tab4Completion").val("");
    alert("Please use datepicker to enter date.");
});
$("#MainContent_tab5Completion").keyup(function (e) {
    e.preventDefault();
    $("#MainContent_tab5Completion").val("");
    alert("Please use datepicker to enter date.");
});
$("#MainContent_tab6Completion").keyup(function (e) {
    e.preventDefault();
    $("#MainContent_tab6Completion").val("");
    alert("Please use datepicker to enter date.");
});
$("#MainContent_tab7Completion").keyup(function (e) {
    e.preventDefault();
    $("#MainContent_tab7Completion").val("");
    alert("Please use datepicker to enter date.");
});
$("#MainContent_tab8Completion").keyup(function (e) {
    e.preventDefault();
    $("#MainContent_tab8Completion").val("");
    alert("Please use datepicker to enter date.");
});
$("#MainContent_tab9Completion").keyup(function (e) {
    e.preventDefault();
    $("#MainContent_tab9Completion").val("");
    alert("Please use datepicker to enter date.");
});
$("#MainContent_tab10ompletion").keyup(function (e) {
    e.preventDefault();
    $("#MainContent_tab10Completion").val("");
    alert("Please use datepicker to enter date.");
});


$("#MainContent_TargetDate").keyup(function (e) {
    e.preventDefault();
    $("#MainContent_TargetDate").val("");
    alert("Please use datepicker to enter date.");
});

$("#MainContent_tab1_btnReject").click(function () {
    var remarks = $("#MainContent_tab1_txtApprove").val();
    if (remarks == "") {
        alert("Please add remarks to reject.");
        return false;
    }
    else {
        return true;
    }

});
$("#MainContent_tab2_btnReject").click(function () {
    var remarks = $("#MainContent_tab2_txtApprove").val();
    if (remarks == "") {
        alert("Please add remarks to reject.");
        return false;
    }
    else {
        return true;
    }

});
$("#MainContent_tab3_btnReject").click(function () {
    var remarks = $("#MainContent_tab3_txtApprove").val();
    if (remarks == "") {
        alert("Please add remarks to reject.");
        return false;
    }
    else {
        return true;
    }

});
$("#MainContent_tab4_btnReject").click(function () {
    var remarks = $("#MainContent_tab4_txtApprove").val();
    if (remarks == "") {
        alert("Please add remarks to reject.");
        return false;
    }
    else {
        return true;
    }

});
$("#MainContent_tab5_btnReject").click(function () {
    var remarks = $("#MainContent_tab5_txtApprove").val();
    if (remarks == "") {
        alert("Please add remarks to reject.");
        return false;
    }
    else {
        return true;
    }

});
$("#MainContent_tab6_btnReject").click(function () {
    var remarks = $("#MainContent_tab6_txtApprove").val();
    if (remarks == "") {
        alert("Please add remarks to reject.");
        return false;
    }
    else {
        return true;
    }

});
$("#MainContent_tab7_btnReject").click(function () {
    var remarks = $("#MainContent_tab7_txtApprove").val();
    if (remarks == "") {
        alert("Please add remarks to reject.");
        return false;
    }
    else {
        return true;
    }

});
$("#MainContent_tab8_btnReject").click(function () {
    var remarks = $("#MainContent_tab8_txtApprove").val();
    if (remarks == "") {
        alert("Please add remarks to reject.");
        return false;
    }
    else {
        return true;
    }

});
$("#MainContent_tab9_btnReject").click(function () {
    var remarks = $("#MainContent_tab9_txtApprove").val();
    if (remarks == "") {
        alert("Please add remarks to reject.");
        return false;
    }
    else {
        return true;
    }

});
$("#MainContent_tab10_btnReject").click(function () {
    var remarks = $("#MainContent_tab10_txtApprove").val();
    if (remarks == "") {
        alert("Please add remarks to reject.");
        return false;
    }
    else {
        return true;
    }

});



function PageReloadScripts() {
       
    PageLoadScripts();

    AddClassWhenDisabled();
    DisableTabs();
    $("#MainContent_tab1Completion").datepicker({
        showOn: "both",
        buttonImageOnly: true,
        buttonText: "",
        changeYear: true,
        changeMonth: true,
        yearRange: "c-60:c+60",
        minDate: -7, //   new Date(),
        dateFormat: 'dd/mm/yy',
        buttonImage: "images/calander_icon.png",
        onSelect: function () {
            $(".tab1_hideout_every").show();
            var finalstage = $("#MainContent_Stages").val();
            if (finalstage == 1) {
                $("#MainContent_tab1_radio_success").attr('checked', false);
                $(".tab1_hideout").show();

            }
        }
    });

    $("#MainContent_tab2Completion").datepicker({
        showOn: "both",
        buttonImageOnly: true,
        buttonText: "",
        changeYear: true,
        changeMonth: true,
        yearRange: "c-60:c+60",
        minDate: -7, //  new Date(),
        dateFormat: 'dd/mm/yy',
        buttonImage: "images/calander_icon.png",
        onSelect: function () {
            $(".tab2_hideout_every").show();
            var finalstage = $("#MainContent_Stages").val();
            if (finalstage == 2) {
                $("#MainContent_tab2_radio_success").attr('checked', false);
                $(".tab2_hideout").show();
            }
        }
    });

    $("#MainContent_tab3Completion").datepicker({
        showOn: "both",
        buttonImageOnly: true,
        buttonText: "",
        changeYear: true,
        changeMonth: true,
        yearRange: "c-60:c+60",
        minDate: -7, //  new Date(),
        dateFormat: 'dd/mm/yy',
        buttonImage: "images/calander_icon.png",
        onSelect: function () {
            $(".tab3_hideout_every").show();
            var finalstage = $("#MainContent_Stages").val();
            if (finalstage == 3) {
                $("#MainContent_tab3_radio_success").attr('checked', false);
                $(".tab3_hideout").show();
            }
        }
    });

    $("#MainContent_tab4Completion").datepicker({
        showOn: "both",
        buttonImageOnly: true,
        buttonText: "",
        changeYear: true,
        changeMonth: true,
        yearRange: "c-60:c+60",
        minDate: -7, //   new Date(),
        dateFormat: 'dd/mm/yy',
        buttonImage: "images/calander_icon.png",
        onSelect: function () {
            $(".tab4_hideout_every").show();
            var finalstage = $("#MainContent_Stages").val();
            if (finalstage == 4) {
                $("#MainContent_tab4_radio_success").attr('checked', false);
                $(".tab4_hideout").show();
            }
        }
    });

    $("#MainContent_tab5Completion").datepicker({
        showOn: "both",
        buttonImageOnly: true,
        buttonText: "",
        changeYear: true,
        changeMonth: true,
        yearRange: "c-60:c+60",
        minDate: -7, //  new Date(),
        dateFormat: 'dd/mm/yy',
        buttonImage: "images/calander_icon.png",
        onSelect: function () {
            $(".tab5_hideout_every").show();
            var finalstage = $("#MainContent_Stages").val();
            if (finalstage == 5) {
                $("#MainContent_tab5_radio_success").attr('checked', false);
                $(".tab5_hideout").show();
            }
        }
    });

    $("#MainContent_tab6Completion").datepicker({
        showOn: "both",
        buttonImageOnly: true,
        buttonText: "",
        changeYear: true,
        changeMonth: true,
        yearRange: "c-60:c+60",
        minDate: -7, //  new Date(),
        dateFormat: 'dd/mm/yy',
        buttonImage: "images/calander_icon.png",
        onSelect: function () {
            $(".tab6_hideout_every").show();
            var finalstage = $("#MainContent_Stages").val();
            if (finalstage == 6) {
                $("#MainContent_tab6_radio_success").attr('checked', false);
                $(".tab6_hideout").show();
            }
        }
    });

    $("#MainContent_tab7Completion").datepicker({
        showOn: "both",
        buttonImageOnly: true,
        buttonText: "",
        changeYear: true,
        changeMonth: true,
        yearRange: "c-60:c+60",
        minDate: -7, //  new Date(),
        dateFormat: 'dd/mm/yy',
        buttonImage: "images/calander_icon.png",
        onSelect: function () {
            $(".tab7_hideout_every").show();
            var finalstage = $("#MainContent_Stages").val();
            if (finalstage == 7) {
                $("#MainContent_tab7_radio_success").attr('checked', false);
                $(".tab7_hideout").show();
            }
        }
    });

    $("#MainContent_tab8Completion").datepicker({
        showOn: "both",
        buttonImageOnly: true,
        buttonText: "",
        changeYear: true,
        changeMonth: true,
        yearRange: "c-60:c+60",
        minDate: -7, //  new Date(),
        dateFormat: 'dd/mm/yy',
        buttonImage: "images/calander_icon.png",
        onSelect: function () {
            $(".tab8_hideout_every").show();
            var finalstage = $("#MainContent_Stages").val();
            if (finalstage == 8) {
                $("#MainContent_tab8_radio_success").attr('checked', false);
                $(".tab8_hideout").show();
            }
        }
    });

    $("#MainContent_tab9Completion").datepicker({
        showOn: "both",
        buttonImageOnly: true,
        buttonText: "",
        changeYear: true,
        changeMonth: true,
        yearRange: "c-60:c+60",
        minDate: -7, //  new Date(),
        dateFormat: 'dd/mm/yy',
        buttonImage: "images/calander_icon.png",
        onSelect: function () {
            $(".tab9_hideout_every").show();
            var finalstage = $("#MainContent_Stages").val();
            if (finalstage == 9) {
                $("#MainContent_tab9_radio_success").attr('checked', false);
                $(".tab9_hideout").show();
            }
        }
    });

    $("#MainContent_tab10Completion").datepicker({
        showOn: "both",
        buttonImageOnly: true,
        buttonText: "",
        changeYear: true,
        changeMonth: true,
        yearRange: "c-60:c+60",
        minDate: -7, //  new Date(),
        dateFormat: 'dd/mm/yy',
        buttonImage: "images/calander_icon.png",
        onSelect: function () {
            $(".tab10_hideout_every").show();
            var finalstage = $("#MainContent_Stages").val();
            if (finalstage == 10) {
                $("#MainContent_tab10_radio_success").attr('checked', false);
                $(".tab10_hideout").show();
            }
        }
    });


    $("#MainContent_tab1_radio_success").click(function () {
        if ($("#MainContent_tab1_radio_success").is(':checked')) {
            $(".tab1_monthly").show();
        }
    });

    $("#MainContent_tab2_radio_success").click(function () {
        if ($("#MainContent_tab2_radio_success").is(':checked')) {
            $(".tab2_monthly").show();
        }
    });

    $("#MainContent_tab3_radio_success").click(function () {
        if ($("#MainContent_tab3_radio_success").is(':checked')) {
            $(".tab3_monthly").show();
        }
    });


    $("#MainContent_tab4_radio_success").click(function () {
        if ($("#MainContent_tab4_radio_success").is(':checked')) {
            $(".tab4_monthly").show();
        }
    });

    $("#MainContent_tab5_radio_success").click(function () {
        if ($("#MainContent_tab5_radio_success").is(':checked')) {
            $(".tab5_monthly").show();
        }
    });

    $("#MainContent_tab6_radio_success").click(function () {
        if ($("#MainContent_tab6_radio_success").is(':checked')) {
            $(".tab6_monthly").show();
        }
    });

    $("#MainContent_tab7_radio_success").click(function () {
        if ($("#MainContent_tab7_radio_success").is(':checked')) {
            $(".tab7_monthly").show();
        }
    });

    $("#MainContent_tab8_radio_success").click(function () {
        if ($("#MainContent_tab8_radio_success").is(':checked')) {
            $(".tab8_monthly").show();
        }
    });

    $("#MainContent_tab9_radio_success").click(function () {
        if ($("#MainContent_tab9_radio_success").is(':checked')) {
            $(".tab9_monthly").show();
        }
    });

    $("#MainContent_tab10_radio_success").click(function () {
        if ($("#MainContent_tab10_radio_success").is(':checked')) {
            $(".tab10_monthly").show();
        }
    });

    $("#MainContent_tab1_radio_failure").click(function () {
        if ($("#MainContent_tab1_radio_failure").is(':checked')) {
            $(".tab1_monthly").hide();
        }
    });

    $("#MainContent_tab2_radio_failure").click(function () {
        if ($("#MainContent_tab2_radio_failure").is(':checked')) {
            $(".tab2_monthly").hide();
        }
    });

    $("#MainContent_tab3_radio_failure").click(function () {
        if ($("#MainContent_tab3_radio_failure").is(':checked')) {
            $(".tab3_monthly").hide();
        }
    });


    $("#MainContent_tab4_radio_failure").click(function () {
        if ($("#MainContent_tab4_radio_failure").is(':checked')) {
            $(".tab4_monthly").hide();
        }
    });

    $("#MainContent_tab5_radio_failure").click(function () {
        if ($("#MainContent_tab5_radio_failure").is(':checked')) {
            $(".tab5_monthly").hide();
        }
    });

    $("#MainContent_tab6_radio_failure").click(function () {
        if ($("#MainContent_tab6_radio_failure").is(':checked')) {
            $(".tab6_monthly").hide();
        }
    });

    $("#MainContent_tab7_radio_failure").click(function () {
        if ($("#MainContent_tab7_radio_failure").is(':checked')) {
            $(".tab7_monthly").hide();
        }
    });

    $("#MainContent_tab8_radio_failure").click(function () {
        if ($("#MainContent_tab8_radio_failure").is(':checked')) {
            $(".tab8_monthly").hide();
        }
    });

    $("#MainContent_tab9_radio_failure").click(function () {
        if ($("#MainContent_tab9_radio_failure").is(':checked')) {
            $(".tab9_monthly").hide();
        }
    });

    $("#MainContent_tab10_radio_failure").click(function () {
        if ($("#MainContent_tab10_radio_failure").is(':checked')) {
            $(".tab10_monthly").hide();
        }
    });

    $("#MainContent_tab1Target").keyup(function (e) {
        e.preventDefault();
        $("#MainContent_tab1Target").val("");
        alert("Please use datepicker to enter date.");
    });
    $("#MainContent_tab2Target").keyup(function (e) {
        e.preventDefault();
        $("#MainContent_tab2Target").val("");
        alert("Please use datepicker to enter date.");
    });
    $("#MainContent_tab3Target").keyup(function (e) {
        e.preventDefault();
        $("#MainContent_tab3Target").val("");
        alert("Please use datepicker to enter date.");
    });
    $("#MainContent_tab4Target").keyup(function (e) {
        e.preventDefault();
        $("#MainContent_tab4Target").val("");
        alert("Please use datepicker to enter date.");
    });
    $("#MainContent_tab5Target").keyup(function (e) {
        e.preventDefault();
        $("#MainContent_tab5Target").val("");
        alert("Please use datepicker to enter date.");
    });
    $("#MainContent_tab6Target").keyup(function (e) {
        e.preventDefault();
        $("#MainContent_tab6Target").val("");
        alert("Please use datepicker to enter date.");
    });
    $("#MainContent_tab7Target").keyup(function (e) {
        e.preventDefault();
        $("#MainContent_tab7Target").val("");
        alert("Please use datepicker to enter date.");
    });
    $("#MainContent_tab8Target").keyup(function (e) {
        e.preventDefault();
        $("#MainContent_tab8Target").val("");
        alert("Please use datepicker to enter date.");
    });
    $("#MainContent_tab9Target").keyup(function (e) {
        e.preventDefault();
        $("#MainContent_tab9Target").val("");
        alert("Please use datepicker to enter date.");
    });
    $("#MainContent_tab10Target").keyup(function (e) {
        e.preventDefault();
        $("#MainContent_tab10Target").val("");
        alert("Please use datepicker to enter date.");
    });

    $("#MainContent_tab1Completion").keyup(function (e) {
        e.preventDefault();
        $("#MainContent_tab1Completion").val("");
        alert("Please use datepicker to enter date.");
    });
    $("#MainContent_tab2Completion").keyup(function (e) {
        e.preventDefault();
        $("#MainContent_tab2Completion").val("");
        alert("Please use datepicker to enter date.");
    });
    $("#MainContent_tab3Completion").keyup(function (e) {
        e.preventDefault();
        $("#MainContent_tab3Completion").val("");
        alert("Please use datepicker to enter date.");
    });
    $("#MainContent_tab4Completion").keyup(function (e) {
        e.preventDefault();
        $("#MainContent_tab4Completion").val("");
        alert("Please use datepicker to enter date.");
    });
    $("#MainContent_tab5Completion").keyup(function (e) {
        e.preventDefault();
        $("#MainContent_tab5Completion").val("");
        alert("Please use datepicker to enter date.");
    });
    $("#MainContent_tab6Completion").keyup(function (e) {
        e.preventDefault();
        $("#MainContent_tab6Completion").val("");
        alert("Please use datepicker to enter date.");
    });
    $("#MainContent_tab7Completion").keyup(function (e) {
        e.preventDefault();
        $("#MainContent_tab7Completion").val("");
        alert("Please use datepicker to enter date.");
    });
    $("#MainContent_tab8Completion").keyup(function (e) {
        e.preventDefault();
        $("#MainContent_tab8Completion").val("");
        alert("Please use datepicker to enter date.");
    });
    $("#MainContent_tab9Completion").keyup(function (e) {
        e.preventDefault();
        $("#MainContent_tab9Completion").val("");
        alert("Please use datepicker to enter date.");
    });
    $("#MainContent_tab10ompletion").keyup(function (e) {
        e.preventDefault();
        $("#MainContent_tab10Completion").val("");
        alert("Please use datepicker to enter date.");
    });

    $("#MainContent_TargetDate").keyup(function (e) {
        e.preventDefault();
        $("#MainContent_TargetDate").val("");
        alert("Please use datepicker to enter date.");
    });

    $("#MainContent_tab1_btnReject").click(function () {
        var remarks = $("#MainContent_tab1_txtApprove").val();
        if (remarks == "") {
            alert("Please add remarks to reject.");
            return false;
        }
        else {
            return true;
        }

    });
    $("#MainContent_tab2_btnReject").click(function () {
        var remarks = $("#MainContent_tab2_txtApprove").val();
        if (remarks == "") {
            alert("Please add remarks to reject.");
            return false;
        }
        else {
            return true;
        }

    });
    $("#MainContent_tab3_btnReject").click(function () {
        var remarks = $("#MainContent_tab3_txtApprove").val();
        if (remarks == "") {
            alert("Please add remarks to reject.");
            return false;
        }
        else {
            return true;
        }

    });
    $("#MainContent_tab4_btnReject").click(function () {
        var remarks = $("#MainContent_tab4_txtApprove").val();
        if (remarks == "") {
            alert("Please add remarks to reject.");
            return false;
        }
        else {
            return true;
        }

    });
    $("#MainContent_tab5_btnReject").click(function () {
        var remarks = $("#MainContent_tab5_txtApprove").val();
        if (remarks == "") {
            alert("Please add remarks to reject.");
            return false;
        }
        else {
            return true;
        }

    });
    $("#MainContent_tab6_btnReject").click(function () {
        var remarks = $("#MainContent_tab6_txtApprove").val();
        if (remarks == "") {
            alert("Please add remarks to reject.");
            return false;
        }
        else {
            return true;
        }

    });
    $("#MainContent_tab7_btnReject").click(function () {
        var remarks = $("#MainContent_tab7_txtApprove").val();
        if (remarks == "") {
            alert("Please add remarks to reject.");
            return false;
        }
        else {
            return true;
        }

    });
    $("#MainContent_tab8_btnReject").click(function () {
        var remarks = $("#MainContent_tab8_txtApprove").val();
        if (remarks == "") {
            alert("Please add remarks to reject.");
            return false;
        }
        else {
            return true;
        }

    });
    $("#MainContent_tab9_btnReject").click(function () {
        var remarks = $("#MainContent_tab9_txtApprove").val();
        if (remarks == "") {
            alert("Please add remarks to reject.");
            return false;
        }
        else {
            return true;
        }

    });
    $("#MainContent_tab10_btnReject").click(function () {
        var remarks = $("#MainContent_tab10_txtApprove").val();
        if (remarks == "") {
            alert("Please add remarks to reject.");
            return false;
        }
        else {
            return true;
        }

    });

    DropdownValidation();
}
function DropdownValidation()
{
    var Customer_name = $("#MainContent_ddlCustomerName").val();
    var objcontrolName = '#MainContent_ddlCustomerName';
    var alertmsg = 'Please Select appropriate Customer Name.';
    if ($("#MainContent_direct_customer").is(':checked')) {
        if ($('#errcustName').text() == null || $('#errcustName').text() == undefined || $('#errcustName').text() == "") {
            //CustomerNameDropdownValidation(Customer_name, objcontrolName, alertmsg);
            if (Customer_name == "-- SELECT CUSTOMER --") {
                {
                    $('#errcustName').append('<p>' + alertmsg + '</p>').css('color', 'red');
                    $(objcontrolName).css('border-color', 'red');
                    return false;
                }
            }
            else {
                $(objcontrolName).css('border-color', '');
                $('#errcustName').hide();
            }
        }
    }
  
    var CustomerforDistributor = $('#MainContent_ddlCustomerforDistributor').val();
    var objcustcontrolname = '#MainContent_ddlCustomerforDistributor';
    var alertmsg = 'Please Select appropriate Distributor name.';
    if ($("#MainContent_channel_partner").is(':checked')) {
        if ($('#errDistcustname').text() == null || $('#errDistcustname').text() == undefined || $('#errDistcustname').text() == "") {
            //DistributorCustomerNameDropdownValidation(CustomerforDistributor, objcontrolName, alertmsg)
            if (CustomerforDistributor == "-- SELECT CUSTOMER --" || CustomerforDistributor == "-- NO CUSTOMER --") {
                $('#errDistcustname').append('<p>' + alertmsg + '</p>').css('color', 'red');
                $(objcontrolName).css('border-color', 'red');

                return false;
            }
            else {
                $(objcontrolName).css('border-color', '');
                $('#errDistcustname').hide();
            }
        }
    }

    var ChannelPartner = $("#MainContent_ddlDistributorList").val();
    var objcontrolName = '#MainContent_ddlDistributorList';
    var alertmsg = 'Please Select appropriate ChannelPartner.';
    if ($('#errchannelpartner').text() == null || $('#errchannelpartner').text() == undefined || $('#errchannelpartner').text() == "") {
        //ChannepertnerDropdownValidation(ChannelPartner, objcontrolName, alertmsg);
        if (ChannelPartner == "-- SELECT CHANNEL PARTNER --") {
            $('#errchannelpartner').append('<p>' + alertmsg + '</p>').css('color', 'red');
            $(objcontrolName).css('border-color', 'red');
            return false;
        }
        else {
            $(objcontrolName).css('border-color', '');
            $('#errchannelpartner').hide();
        } 
    }

    if ($('#MainContent_new_project').is(':checked')) {
        var projecttitle = $("#MainContent_ddlTitle").val();
        var objcontrolName = '#MainContent_ddlTitle';
        var alertmsg = 'Please Select appropriate ProjectTitle.';
        if ($('#errprojecttitle').text() == null || $('#errprojecttitle').text() == undefined || $('#errprojecttitle').text() == "") {
            ProjecttitleDropdownValidation(projecttitle, objcontrolName, alertmsg);
        }
    }
    else {
        var projecttitle = $("#MainContent_txtTitle").val();
        var objcontrolName = '#MainContent_txtTitle';
        var alertmsg = 'Please Select appropriate ProjectTitle.';
        if ($('#errprojecttitle').text() == null || $('#errprojecttitle').text() == undefined || $('#errprojecttitle').text() == "") {
            ProjecttitleDropdownValidation(projecttitle, objcontrolName, alertmsg);
        }
    }

    var projecttype = $("MainContent_ddlProjectType").val();
    var objcontrolName = '#MainContent_ddlProjectType';
    var alertmsg = 'Please Select ProjectType.';
    if ($('#errprojecttype').text() == null || $('#errprojecttype').text() == undefined || $('#errprojecttype').text() == "") {
        ProjecttypeDropdownValidation(projecttype, objcontrolName, alertmsg);
    }

    var noofstages = $("#MainContent_Stages").val();
    var objcontrolName = '#MainContent_Stages';
    var alertmsg = 'Please Select No of Stages.';
    if ($('#errnoofstages').text() == null || $('#errnoofstages').text() == undefined || $('#errnoofstages').text() == "") {
        NoofstagesDropdownValidation(noofstages, objcontrolName, alertmsg);
    }
    var reviewer = $("#MainContent_ddlReviewer").val();
    var objcontrolName = '#MainContent_ddlReviewer';
    var alertmsg = 'Please Select Reviewer.';
    if ($('#errReviewer').text() == null || $('#errReviewer').text() == undefined || $('#errReviewer').text() == "") {
        ReviewerDropdownValidation(reviewer, objcontrolName, alertmsg);
    }

    var escalatedto = $("#MainContent_ddlEscalate").val();
    var objcontrolName = '#MainContent_ddlEscalate';
    var alertmsg = 'Please Select Escalate To.';    
    if ($('#errEscalateto').text() == null || $('#errEscalateto').text() == undefined || $('#errEscalateto').text() == "") {
        EscalatedtoDropdownValidation(escalatedto, objcontrolName, alertmsg);
    }  
}

function LoadClosingFields() {
    var tab_num = $("#MainContent_final_stage").val();
    var finalstage = $("#MainContent_Stages").val();
    var class1 = ".tab" + tab_num + "_hideout";
    var id1 = "#MainContent_tab" + tab_num + "_radio_success";
    var class2 = ".tab" + tab_num + "_monthly";
    if (tab_num == finalstage) {
        $(class1).show();
        if ($(id1).is(':checked')) {
            $(class2).show();
        }
    }
}

function DisableCompletionDatePickers() {
    $("#MainContent_tab1Completion").datepicker("disable");
    $("#MainContent_tab2Completion").datepicker("disable");
    $("#MainContent_tab3Completion").datepicker("disable");
    $("#MainContent_tab4Completion").datepicker("disable");
    $("#MainContent_tab5Completion").datepicker("disable");
    $("#MainContent_tab6Completion").datepicker("disable");
    $("#MainContent_tab7Completion").datepicker("disable");
    $("#MainContent_tab8Completion").datepicker("disable");
    $("#MainContent_tab9Completion").datepicker("disable");
    $("#MainContent_tab10Completion").datepicker("disable");

}
function EnableCompletionDatePickers() {
    if ($("#MainContent_tab1Completion_flag").val() == "True") {
        $("#MainContent_tab1Completion").datepicker("enable");
    }
    else {
        $("#MainContent_tab1Completion").datepicker({ disabled: true });
    }

    if ($("#MainContent_tab2Completion_flag").val() == "True") {
        $("#MainContent_tab2Completion").datepicker("enable");
    }
    else {
        $("#MainContent_tab2Completion").datepicker({ disabled: true });
    }
    if ($("#MainContent_tab3Completion_flag").val() == "True") {
        $("#MainContent_tab3Completion").datepicker("enable");
    }
    else {
        $("#MainContent_tab3Completion").datepicker({ disabled: true });
    }
    if ($("#MainContent_tab4Completion_flag").val() == "True") {
        $("#MainContent_tab4Completion").datepicker("enable");
    }
    else {
        $("#MainContent_tab4Completion").datepicker({ disabled: true });
    }

    if ($("#MainContent_tab5Completion_flag").val() == "True") {
        $("#MainContent_tab5Completion").datepicker("enable");
    }
    else {
        $("#MainContent_tab5Completion").datepicker({ disabled: true });
    }
    if ($("#MainContent_tab6Completion_flag").val() == "True") {
        $("#MainContent_tab6Completion").datepicker("enable");
    }
    else {
        $("#MainContent_tab6Completion").datepicker({ disabled: true });
    }
    if ($("#MainContent_tab7Completion_flag").val() == "True") {
        $("#MainContent_tab7Completion").datepicker("enable");
    }
    else {
        $("#MainContent_tab7Completion").datepicker({ disabled: true });
    }
    if ($("#MainContent_tab8Completion_flag").val() == "True") {
        $("#MainContent_tab8Completion").datepicker("enable");
    }
    else {
        $("#MainContent_tab8Completion").datepicker({ disabled: true });
    }
    if ($("#MainContent_tab9Completion_flag").val() == "True") {
        $("#MainContent_tab9Completion").datepicker("enable");
    }
    else {
        $("#MainContent_tab9Completion").datepicker({ disabled: true });
    }
    if ($("#MainContent_tab10Completion_flag").val() == "True") {
        $("#MainContent_tab10Completion").datepicker("enable");
    }
    else {
        $("#MainContent_tab10Completion").datepicker({ disabled: true });
    }
}
function DisableTargetDatePickers() {

    if ($("#MainContent_tab1Target_flag").val() == "True") {
        $("#MainContent_tab1Target").datepicker("disable");
    }

    if ($("#MainContent_tab2Target_flag").val() == "True") {
        $("#MainContent_tab2Target").datepicker("disable");
    }
    if ($("#MainContent_tab3Target_flag").val() == "True") {
        $("#MainContent_tab3Target").datepicker("disable");
    }
    if ($("#MainContent_tab4Target_flag").val() == "True") {
        $("#MainContent_tab4Target").datepicker("disable");
    }

    if ($("#MainContent_tab5Target_flag").val() == "True") {
        $("#MainContent_tab5Target").datepicker("disable");
    }

    if ($("#MainContent_tab6Target_flag").val() == "True") {
        $("#MainContent_tab6Target").datepicker("disable");
    }

    if ($("#MainContent_tab7Target_flag").val() == "True") {
        $("#MainContent_tab7Target").datepicker("disable");
    }

    if ($("#MainContent_tab8Target_flag").val() == "True") {
        $("#MainContent_tab8Target").datepicker("disable");
    }

    if ($("#MainContent_tab9Target_flag").val() == "True") {
        $("#MainContent_tab9Target").datepicker("disable");
    }

    if ($("#MainContent_tab10Target_flag").val() == "True") {
        $("#MainContent_tab10Target").datepicker("disable");
    }
}

function ProjecttitleDropdownValidation(projecttitle, objcontrolName, alertmsg) {
    if (projecttitle == "-- SELECT PROJECT --" || projecttitle == "-- NO PROJECT --") {
        $('#errprojecttitle').append('<p>' + alertmsg + '</p>').css('color', 'red');
        $(objcontrolName).css('border-color', 'red');
        return false;
    }
    else {
        $(objcontrolName).css('border-color', '');
        $('#errprojecttitle').hide();
    }

}
function ProjecttypeDropdownValidation(projecttype, objcontrolName, alertmsg) {
    if (projecttype == "-- SELECT --") {
        $('#errprojecttype').append('<p>' + alertmsg + '</p>').css('color', 'red');
        $(objcontrolName).css('border-color', 'red');
        return false;
    }
    else {
        $(objcontrolName).css('border-color', '');
        $('#errprojecttype').hide();
    }

}
function NoofstagesDropdownValidation(noofstages, objcontrolName, alertmsg) {
    if (noofstages == "--SELECT STAGES--") {
        $('#errnoofstages').append('<p>' + alertmsg + '</p>').css('color', 'red');
        $(objcontrolName).css('border-color', 'red');
        return false;
    }
    else {
        $(objcontrolName).css('border-color', '');
        $('#errnoofstages').hide();
    }

}
function ReviewerDropdownValidation(reviewer, objcontrolName, alertmsg) {
    if (reviewer == "-- SELECT --") {
        $('#errReviewer').show();
        $('#errReviewer').append('<p>' + alertmsg + '</p>').css('color', 'red');
        $(objcontrolName).css('border-color', 'red');
        return false;
    }
    else {
        $(objcontrolName).css('border-color', '');
        $('#errReviewer').hide();
    }

}
function EscalatedtoDropdownValidation(escalatedto, objcontrolName, alertmsg) {
    if (escalatedto == "-- SELECT --") {
        $('#errEscalateto').show();
        $('#errEscalateto').append('<p>' + alertmsg + '</p>').css('color', 'red');
        $(objcontrolName).css('border-color', 'red');
        return false;
    }
    else {
        $(objcontrolName).css('border-color', '');
        $('#errEscalateto').hide();
    }

}

//Added By Anantha
function TargetdatesValidations() {
    $(document).on('change', '#MainContent_TargetDate', function () {
        debugger;
        var Project_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_TargetDate').val());
        var Stage1_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab1Target').val());
        var Stage2_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab2Target').val());
        var Stage3_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab3Target').val());
        var Stage4_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab4Target').val());
        var Stage5_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab5Target').val());
        var Stage6_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab6Target').val());
        var Stage7_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab7Target').val());
        var Stage8_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab8Target').val());
        var Stage9_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab9Target').val());
        var Stage10_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab10Target').val());
        var vaue = $('#MainContent_Target_Date').val();
        if (Stage1_Target_Date != "" && Stage1_Target_Date != undefined && Stage1_Target_Date != null)
            if (Project_Target_Date != "" && Project_Target_Date != undefined && Project_Target_Date != null)
            if (new Date(Project_Target_Date) < new Date(Stage1_Target_Date)) {
                alert("Target Date can not be less than the Stage 1 Target Date");
                //var vaue = $('#MainContent_tab1Target_date').val();
                $("#MainContent_TargetDate").val(vaue);
                return false;
            }
            else if (new Date(Project_Target_Date) < new Date(Stage2_Target_Date)) {
                alert("Target Date can not be less than the Stage 2 Target Date");
                // var vaue = "";
                $("#MainContent_TargetDate").val(vaue);
                return false;
            }
            else if (new Date(Project_Target_Date) < new Date(Stage3_Target_Date)) {
                alert("Target Date can not be less than the Stage 3 Target Date");
                //var vaue = "";
                $("#MainContent_TargetDate").val(vaue);
                return false;
            }
            else if (new Date(Project_Target_Date) < new Date(Stage4_Target_Date)) {
                alert("Target Date can not be less than the Stage 4 Target Date");
                var vaue = "";
                $("#MainContent_TargetDate").val(vaue);
                return false;
            }
            else if (new Date(Project_Target_Date) < new Date(Stage5_Target_Date)) {
                alert("Target Date can not be less than the Stage 5 Target Date");
                //var vaue = "";
                $("#MainContent_TargetDate").val(vaue);
                return false;
            }
            else if (new Date(Project_Target_Date) < new Date(Stage6_Target_Date)) {
                alert("Target Date can not be less than the Stage 6 Target Date");
                //var vaue = "";
                $("#MainContent_TargetDate").val(vaue);
                return false;
            }
            else if (new Date(Project_Target_Date) < new Date(Stage7_Target_Date)) {
                alert("Target Date can not be less than the Stage 7 Target Date");
                //var vaue = "";
                $("#MainContent_TargetDate").val(vaue);
                return false;
            }
            else if (new Date(Project_Target_Date) < new Date(Stage8_Target_Date)) {
                alert("Target Date can not be less than the Stage 8 Target Date");
                //var vaue = "";
                $("#MainContent_TargetDate").val(vaue);
                return false;
            }
            else if (new Date(Project_Target_Date) < new Date(Stage9_Target_Date)) {
                alert("Target Date can not be less than the Stage 9 Target Date");
                //var vaue = "";
                $("#MainContent_TargetDate").val(vaue);
                return false;
            }
            else if (new Date(Project_Target_Date) < new Date(Stage10_Target_Date)) {
                alert("Target Date can not be less than the Stage 10 Target Date");
                //var vaue = "";
                $("#MainContent_TargetDate").val(vaue);
                return false;
            }
        return true;
    });

    $(document).on('change', '#MainContent_tab1Target', function () {
        debugger;
        var Project_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_TargetDate').val());
        var Stage1_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab1Target').val());
        var Stage2_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab2Target').val());
        if ($('#MainContent_new_project').is(':checked')) {
            if (Project_Target_Date != "" && Project_Target_Date != undefined && Project_Target_Date != null) {
                if (Stage1_Target_Date != "" && Stage1_Target_Date != undefined && Stage1_Target_Date != null) {
                if (new Date(Project_Target_Date) < new Date(Stage1_Target_Date)) {
                    alert("Stage 1 Date can not be greater than the Project Target Date");
                    var vaue = $('#MainContent_tab1Target_date').val();
                    $("#MainContent_tab1Target").val(vaue);
                    return false;
                }
                }
            }
            $("#tab_home").tabs({
                active: [0],
                disabled: [1, 2, 3, 4, 5, 6, 7, 8, 9]

            });
            return true;
        }
        else {
            if (Stage2_Target_Date == null) {
                if (Project_Target_Date != "" && Project_Target_Date != undefined && Project_Target_Date != null) {
                    if (Stage1_Target_Date != "" && Stage1_Target_Date != undefined && Stage1_Target_Date != null) {
                        if (new Date(Project_Target_Date) < new Date(Stage1_Target_Date)) {
                            alert("Stage 1 Date can not be greater than the Project Target Date");
                            var vaue = $('#MainContent_tab1Target_date').val();
                            $("#MainContent_tab1Target").val(vaue);
                            return false;
                        }
                    }
                }
                return true;
            }
            else if (Stage2_Target_Date != null) {
                if (Project_Target_Date != "" && Project_Target_Date != undefined && Project_Target_Date != null)
                    if (Stage1_Target_Date != "" && Stage1_Target_Date != undefined && Stage1_Target_Date != null)
                    if (new Date(Project_Target_Date) < new Date(Stage1_Target_Date) || new Date(Stage2_Target_Date) < new Date(Stage1_Target_Date)) {
                        alert("Stage 1 Date can not be greater than the Project Target Date");
                        var vaue = $('#MainContent_tab1Target_date').val();
                        $("#MainContent_tab1Target").val(vaue);
                        return false;
                    }
                return true;
            }
        }
    });

    $(document).on('change', '#MainContent_tab2Target', function () {
        debugger;
        var Project_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_TargetDate').val());
        var Stage1_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab1Target').val());
        var Stage2_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab2Target').val());
        var Stage3_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab3Target').val());
        if (Stage3_Target_Date == null) {
            if (Project_Target_Date != "" && Project_Target_Date != undefined && Project_Target_Date != null) {
                if (Stage2_Target_Date != "" && Stage2_Target_Date != undefined && Stage2_Target_Date != null) {
                    if (new Date(Stage2_Target_Date) > new Date(Stage1_Target_Date) && new Date(Project_Target_Date) > new Date(Stage2_Target_Date)) {

                    }
                    else if (new Date(Project_Target_Date) < new Date(Stage2_Target_Date)) {
                        alert("Current Stage Target Date can not be Next Day(s) of the Project Target Date");
                        var vaue = $('#MainContent_tab2Target_date').val();
                        $("#MainContent_tab2Target").val(vaue);
                        return false;
                    }
                    else if (new Date(Stage2_Target_Date) < new Date(Stage1_Target_Date)) {
                        alert("Current Stage Target Date can not be Before Day(s) of the Previous Stage Target Date");
                        var vaue = $('#MainContent_tab2Target_date').val();
                        $("#MainContent_tab2Target").val(vaue);
                        return false;
                    }
                }
            }
            return true;
        }
        else if (Stage3_Target_Date != null) {
            if (Project_Target_Date != "" && Project_Target_Date != undefined && Project_Target_Date != null)
                if (Stage2_Target_Date != "" && Stage2_Target_Date != undefined && Stage2_Target_Date != null)
                if (new Date(Project_Target_Date) < new Date(Stage2_Target_Date) || new Date(Stage3_Target_Date) < new Date(Stage2_Target_Date)) {
                    alert("Stage 2 Date can not be greater than the Higher Stage TargetDates and Lesser than the Lower Target Dates  ");
                    var vaue = $('#MainContent_tab2Target_date').val();
                    $("#MainContent_tab2Target").val(vaue);
                    return false;
                }
                else if (new Date(Stage2_Target_Date) < new Date(Stage1_Target_Date)) {
                    alert("Current Stage Target Date can not be Before Day(s) of the Previous Stage Target Date");
                    var vaue = $('#MainContent_tab2Target_date').val();
                    $("#MainContent_tab2Target").val(vaue);
                    return false;
                }
            return true;
        }
    });

    $(document).on('change', '#MainContent_tab3Target', function () {
        var Project_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_TargetDate').val());
        var Stage2_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab2Target').val());
        var Stage3_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab3Target').val());
        var Stage4_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab4Target').val());
        if (Stage4_Target_Date == null) {
            if (Project_Target_Date != "" && Project_Target_Date != undefined && Project_Target_Date != null) {
                if (Stage3_Target_Date != "" && Stage3_Target_Date != undefined && Stage3_Target_Date != null) {
                    if (new Date(Stage3_Target_Date) > new Date(Stage2_Target_Date) && new Date(Project_Target_Date) > new Date(Stage3_Target_Date)) {

                    }
                    else if (new Date(Project_Target_Date) < new Date(Stage3_Target_Date)) {
                        alert("Current Stage Target Date can not be Next Day(s) of the Project Target Date");
                        var vaue = $('#MainContent_tab3Target_date').val();
                        $("#MainContent_tab3Target").val(vaue);
                        return false;
                    }
                    else if (new Date(Stage3_Target_Date) < new Date(Stage2_Target_Date)) {
                        alert("Current Stage Target Date can not be Before Day(s) of the Previous Stage Target Date");
                        var vaue = $('#MainContent_tab3Target_date').val();
                        $("#MainContent_tab3Target").val(vaue);
                        return false;
                    }
                }
            }
            return true;
        }
        else if (Stage4_Target_Date != null) {
            if (Project_Target_Date != "" && Project_Target_Date != undefined && Project_Target_Date != null)
                if (Stage3_Target_Date != "" && Stage3_Target_Date != undefined && Stage3_Target_Date != null)
                if (new Date(Project_Target_Date) < new Date(Stage3_Target_Date) || new Date(Stage4_Target_Date) < new Date(Stage3_Target_Date)) {
                    alert("Stage 3 Date can not be greater than the Higher Stage TargetDates and Lesser than the Lower Target Dates  ");
                    var vaue = $('#MainContent_tab3Target_date').val();
                    $("#MainContent_tab3Target").val(vaue);
                    return false;
                }
                else if (new Date(Stage3_Target_Date) < new Date(Stage2_Target_Date)) {
                    alert("Current Stage Target Date can not be Before Day(s) of the Previous Stage Target Date");
                    var vaue = $('#MainContent_tab3Target_date').val();
                    $("#MainContent_tab3Target").val(vaue);
                    return false;
                }

            return true;
        }
    });

    $(document).on('change', '#MainContent_tab4Target', function () {
        var Project_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_TargetDate').val());
        var Stage3_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab3Target').val());
        var Stage4_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab4Target').val());
        var Stage5_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab5Target').val());
        if (Stage5_Target_Date == null) {
            if (Project_Target_Date != "" && Project_Target_Date != undefined && Project_Target_Date != null) {
                if (Stage4_Target_Date != "" && Stage4_Target_Date != undefined && Stage4_Target_Date != null) {
                    if (new Date(Stage4_Target_Date) > new Date(Stage3_Target_Date) && new Date(Project_Target_Date) > new Date(Stage4_Target_Date)) {

                    }
                    else if (new Date(Project_Target_Date) < new Date(Stage4_Target_Date)) {
                        alert("Current Stage Target Date can not be Next Day(s) of the Project Target Date");
                        var vaue = $('#MainContent_tab4Target_date').val();
                        $("#MainContent_tab4Target").val(vaue);
                        return false;
                    }
                    else if (new Date(Stage4_Target_Date) < new Date(Stage3_Target_Date)) {
                        alert("Current Stage Target Date can not be Before Day(s) of the Previous Stage Target Date");
                        var vaue = $('#MainContent_tab4Target_date').val();
                        $("#MainContent_tab4Target").val(vaue);
                        return false;
                    }
                }
            }
            return true;
        }
        else if (Stage5_Target_Date != null) {
            if (Project_Target_Date != "" && Project_Target_Date != undefined && Project_Target_Date != null)
                if (Stage4_Target_Date != "" && Stage4_Target_Date != undefined && Stage4_Target_Date != null)
                if (new Date(Project_Target_Date) < new Date(Stage4_Target_Date) || new Date(Stage5_Target_Date) < new Date(Stage4_Target_Date)) {
                    alert("Stage 4 Date can not be greater than the Higher Stage TargetDates and Lesser than the Lower Target Dates  ");
                    var vaue = $('#MainContent_tab4Target_date').val();
                    $("#MainContent_tab5Target").val(vaue);
                    return false;
                }
                else if (new Date(Stage4_Target_Date) < new Date(Stage3_Target_Date)) {
                    alert("Current Stage Target Date can not be Before Day(s) of the Previous Stage Target Date");
                    var vaue = $('#MainContent_tab4Target_date').val();
                    $("#MainContent_tab4Target").val(vaue);
                    return false;
                }
            return true;
        }
    });

    $(document).on('change', '#MainContent_tab5Target', function () {
        var Project_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_TargetDate').val());
        var Stage4_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab4Target').val());
        var Stage5_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab5Target').val());
        var Stage6_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab6Target').val());
        if (Stage6_Target_Date == null) {
            if (Project_Target_Date != "" && Project_Target_Date != undefined && Project_Target_Date != null) {
                if (Stage5_Target_Date != "" && Stage5_Target_Date != undefined && Stage5_Target_Date != null) {
                    if (new Date(Stage5_Target_Date) > new Date(Stage4_Target_Date) && new Date(Project_Target_Date) > new Date(Stage5_Target_Date)) {

                    }
                    else if (new Date(Project_Target_Date) < new Date(Stage5_Target_Date)) {
                        alert("Current Stage Target Date can not be Next Day(s) of the Project Target Date");
                        var vaue = $('#MainContent_tab5Target_date').val();
                        $("#MainContent_tab5Target").val(vaue);
                        return false;
                    }
                    else if (new Date(Stage5_Target_Date) < new Date(Stage4_Target_Date)) {
                        alert("Current Stage Target Date can not be Before Day(s) of the Previous Stage Target Date");
                        var vaue = $('#MainContent_tab5Target_date').val();
                        $("#MainContent_tab5Target").val(vaue);
                        return false;
                    }
                }
            }
            return true;
        }
        else if (Stage6_Target_Date != null) {
            if (Project_Target_Date != "" && Project_Target_Date != undefined && Project_Target_Date != null)
                if (Stage5_Target_Date != "" && Stage5_Target_Date != undefined && Stage5_Target_Date != null)
                if (new Date(Project_Target_Date) < new Date(Stage5_Target_Date) || new Date(Stage6_Target_Date) < new Date(Stage5_Target_Date)) {
                    alert("Stage 5 Date can not be greater than the Higher Stage TargetDates and Lesser than the Lower Target Dates  ");
                    var vaue = $('#MainContent_tab5Target_date').val();
                    $("#MainContent_tab5Target").val(vaue);
                    return false;
                }
                else if (new Date(Stage5_Target_Date) < new Date(Stage4_Target_Date)) {
                    alert("Current Stage Target Date can not be Before Day(s) of the Previous Stage Target Date");
                    var vaue = $('#MainContent_tab5Target_date').val();
                    $("#MainContent_tab5Target").val(vaue);
                    return false;
                }
            return true;
        }
    });

    $(document).on('change', '#MainContent_tab6Target', function () {
        var Project_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_TargetDate').val());
        var Stage5_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab5Target').val());
        var Stage6_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab6Target').val());
        var Stage7_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab7Target').val());
        if (Stage7_Target_Date == null) {
            if (Project_Target_Date != "" && Project_Target_Date != undefined && Project_Target_Date != null) {
                if (Stage6_Target_Date != "" && Stage6_Target_Date != undefined && Stage6_Target_Date != null) {
                    if (new Date(Stage6_Target_Date) > new Date(Stage5_Target_Date) && new Date(Project_Target_Date) > new Date(Stage6_Target_Date)) {

                    }
                    else if (new Date(Project_Target_Date) < new Date(Stage6_Target_Date)) {
                        alert("Current Stage Target Date can not be Next Day(s) of the Project Target Date");
                        var vaue = $('#MainContent_tab6Target_date').val();
                        $("#MainContent_tab6Target").val(vaue);
                        return false;
                    }
                    else if (new Date(Stage6_Target_Date) < new Date(Stage5_Target_Date)) {
                        alert("Current Stage Target Date can not be Before Day(s) of the Previous Stage Target Date");
                        var vaue = $('#MainContent_tab6Target_date').val();
                        $("#MainContent_tab6Target").val(vaue);
                        return false;
                    }
                }
            }
            return true;
        }
        else if (Stage7_Target_Date != null) {
            if (Project_Target_Date != "" && Project_Target_Date != undefined && Project_Target_Date != null)
                if (Stage6_Target_Date != "" && Stage6_Target_Date != undefined && Stage6_Target_Date != null)
                if (new Date(Project_Target_Date) < new Date(Stage6_Target_Date) || new Date(Stage7_Target_Date) < new Date(Stage6_Target_Date)) {
                    alert("Stage 6 Date can not be greater than the Higher Stage TargetDates and Lesser than the Lower Target Dates  ");
                    var vaue = $('#MainContent_tab6Target_date').val();
                    $("#MainContent_tab6Target").val(vaue);
                    return false;
                }
                else if (new Date(Stage6_Target_Date) < new Date(Stage5_Target_Date)) {
                    alert("Current Stage Target Date can not be Before Day(s) of the Previous Stage Target Date");
                    var vaue = $('#MainContent_tab6Target_date').val();
                    $("#MainContent_tab6Target").val(vaue);
                    return false;
                }
            return true;
        }
    });

    $(document).on('change', '#MainContent_tab7Target', function () {
        var Project_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_TargetDate').val());
        var Stage6_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab6Target').val());
        var Stage7_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab7Target').val());
        var Stage8_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab8Target').val());
        if (Stage8_Target_Date == null) {
            if (Project_Target_Date != "" && Project_Target_Date != undefined && Project_Target_Date != null) {
                if (Stage7_Target_Date != "" && Stage7_Target_Date != undefined && Stage7_Target_Date != null) {
                    if (new Date(Stage7_Target_Date) > new Date(Stage6_Target_Date) && new Date(Project_Target_Date) > new Date(Stage7_Target_Date)) {

                    }
                    else if (new Date(Project_Target_Date) < new Date(Stage7_Target_Date)) {
                        alert("Current Stage Target Date can not be Next Day(s) of the Project Target Date");
                        var vaue = $('#MainContent_tab7Target_date').val();
                        $("#MainContent_tab7Target").val(vaue);
                        return false;
                    }
                    else if (new Date(Stage7_Target_Date) < new Date(Stage6_Target_Date)) {
                        alert("Current Stage Target Date can not be Before Day(s) of the Previous Stage Target Date");
                        var vaue = $('#MainContent_tab7Target_date').val();
                        $("#MainContent_tab7Target").val(vaue);
                        return false;
                    }
                }
            }
            return true;
        }
        else if (Stage8_Target_Date != null) {
            if (Project_Target_Date != "" && Project_Target_Date != undefined && Project_Target_Date != null)
                if (Stage7_Target_Date != "" && Stage7_Target_Date != undefined && Stage7_Target_Date != null)
                if (new Date(Project_Target_Date) < new Date(Stage7_Target_Date) || new Date(Stage8_Target_Date) < new Date(Stage7_Target_Date)) {
                    alert("Stage 7 Date can not be greater than the Higher Stage TargetDates and Lesser than the Lower Target Dates  ");
                    var vaue = $('#MainContent_tab7Target_date').val();
                    $("#MainContent_tab7Target").val(vaue);
                    return false;
                }
                else if (new Date(Stage7_Target_Date) < new Date(Stage6_Target_Date)) {
                    alert("Current Stage Target Date can not be Before Day(s) of the Previous Stage Target Date");
                    var vaue = $('#MainContent_tab7Target_date').val();
                    $("#MainContent_tab7Target").val(vaue);
                    return false;
                }
            return true;
        }
    });

    $(document).on('change', '#MainContent_tab8Target', function () {
        var Project_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_TargetDate').val());
        var Stage7_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab7Target').val());
        var Stage8_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab8Target').val());
        var Stage9_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab9Target').val());
        if (Stage9_Target_Date == null) {
            if (Project_Target_Date != "" && Project_Target_Date != undefined && Project_Target_Date != null) {
                if (Stage8_Target_Date != "" && Stage8_Target_Date != undefined && Stage8_Target_Date != null) {
                    if (new Date(Stage8_Target_Date) > new Date(Stage7_Target_Date) && new Date(Project_Target_Date) > new Date(Stage8_Target_Date)) {

                    }
                    else if (new Date(Project_Target_Date) < new Date(Stage8_Target_Date)) {
                        alert("Current Stage Target Date can not be Next Day(s) of the Project Target Date");
                        var vaue = $('#MainContent_tab8Target_date').val();
                        $("#MainContent_tab8Target").val(vaue);
                        return false;
                    }
                    else if (new Date(Stage8_Target_Date) < new Date(Stage7_Target_Date)) {
                        alert("Current Stage Target Date can not be Before Day(s) of the Previous Stage Target Date");
                        var vaue = $('#MainContent_tab8Target_date').val();
                        $("#MainContent_tab8Target").val(vaue);
                        return false;
                    }
                }
            }
            return true;
        }
        else if (Stage9_Target_Date != null) {
            if (Project_Target_Date != "" && Project_Target_Date != undefined && Project_Target_Date != null)
                if (Stage8_Target_Date != "" && Stage8_Target_Date != undefined && Stage8_Target_Date != null)
                if (new Date(Project_Target_Date) < new Date(Stage8_Target_Date) || new Date(Stage9_Target_Date) < new Date(Stage8_Target_Date)) {
                    alert("Stage 8 Date can not be greater than the Higher Stage TargetDates and Lesser than the Lower Target Dates  ");
                    var vaue = $('#MainContent_tab8Target_date').val();
                    $("#MainContent_tab8Target").val(vaue);
                    return false;
                }
                else if (new Date(Stage8_Target_Date) < new Date(Stage7_Target_Date)) {
                    alert("Current Stage Target Date can not be Before Day(s) of the Previous Stage Target Date");
                    var vaue = $('#MainContent_tab8Target_date').val();
                    $("#MainContent_tab8Target").val(vaue);
                    return false;
                }
            return true;
        }
    });

    $(document).on('change', '#MainContent_tab9Target', function () {
        var Project_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_TargetDate').val());
        var Stage8_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab8Target').val());
        var Stage9_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab9Target').val());
        var Stage10_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab10Target').val());
        if (Stage10_Target_Date == null) {
            if (Project_Target_Date != "" && Project_Target_Date != undefined && Project_Target_Date != null) {
                if (Stage9_Target_Date != "" && Stage9_Target_Date != undefined && Stage9_Target_Date != null) {
                    if (new Date(Stage9_Target_Date) > new Date(Stage8_Target_Date) && new Date(Project_Target_Date) > new Date(Stage9_Target_Date)) {

                    }
                    else if (new Date(Project_Target_Date) < new Date(Stage9_Target_Date)) {
                        alert("Current Stage Target Date can not be Next Day(s) of the Project Target Date");
                        var vaue = $('#MainContent_tab9Target_date').val();
                        $("#MainContent_tab9Target").val(vaue);
                        return false;
                    }
                    else if (new Date(Stage9_Target_Date) < new Date(Stage8_Target_Date)) {
                        alert("Current Stage Target Date can not be Before Day(s) of the Previous Stage Target Date");
                        var vaue = $('#MainContent_tab9Target_date').val();
                        $("#MainContent_tab9Target").val(vaue);
                        return false;
                    }
                }
            }
            return true;
        }
        else if (Stage10_Target_Date != null) {
            if (Project_Target_Date != "" && Project_Target_Date != undefined && Project_Target_Date != null)
                if (Stage9_Target_Date != "" && Stage9_Target_Date != undefined && Stage9_Target_Date != null)
                if (new Date(Project_Target_Date) < new Date(Stage9_Target_Date) || new Date(Stage10_Target_Date) < new Date(Stage9_Target_Date)) {
                    alert("Stage 9 Date can not be greater than the Higher Stage TargetDates and Lesser than the Lower Target Dates  ");
                    var vaue = $('#MainContent_tab9Target_date').val();
                    $("#MainContent_tab9Target").val(vaue);
                    return false;
                }
                else if (new Date(Stage9_Target_Date) < new Date(Stage8_Target_Date)) {
                    alert("Current Stage Target Date can not be Before Day(s) of the Previous Stage Target Date");
                    var vaue = $('#MainContent_tab9Target_date').val();
                    $("#MainContent_tab9Target").val(vaue);
                    return false;
                }
            return true;
        }
    });

    $(document).on('change', '#MainContent_tab10Target', function () {
        var Project_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_TargetDate').val());
        var Stage9_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab9Target').val());
        var Stage10_Target_Date = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_tab10Target').val());
        if (Project_Target_Date != "" && Project_Target_Date != undefined && Project_Target_Date != null) {
            if (Stage10_Target_Date != "" && Stage10_Target_Date != undefined && Stage10_Target_Date != null) {
                if (new Date(Stage10_Target_Date) > new Date(Stage9_Target_Date) && new Date(Project_Target_Date) > new Date(Stage10_Target_Date)) {

                }
                else if (new Date(Project_Target_Date) < new Date(Stage10_Target_Date)) {
                    alert("Current Stage Target Date can not be Next Day(s) of the Project Target Date");
                    var vaue = $('#MainContent_tab10Target_date').val();
                    $("#MainContent_tab10Target").val(vaue);
                    return false;
                }
                else if (new Date(Stage10_Target_Date) < new Date(Stage9_Target_Date)) {
                    alert("Current Stage Target Date can not be Before Day(s) of the Previous Stage Target Date");
                    var vaue = $('#MainContent_tab10Target_date').val();
                    $("#MainContent_tab10Target").val(vaue);
                    return false;
                }
            }
        }
        return true;
    });

}
function clearallpagedetails() {
    $("#MainContent_ddlCustomerName").on('change', function (e) {
        var vaue = "";
        setControlValues(vaue)
    });

    $("#MainContent_ddlDistributorList").on('change', function (e) {
        var vaue = "";
        $("#MainContent_ddlCustomerforDistributor").val(vaue);
        setControlValues(vaue)
    });

    $("#MainContent_ddlCustomerforDistributor").on('change', function (e) {
        var vaue = "";
        setControlValues(vaue)
    });
}
//End Anantha

//Anamika
//Date : Jan 1st,2017
//Desc : setting values to all the controls
function setControlValues(vaue) {
    $("#MainContent_ddlEscalate").val(vaue);
    $("#MainContent_ddlReviewer").val(vaue);
    $("#MainContent_Potential").val(vaue);
    $("#MainContent_Business").val(vaue);
    $("#MainContent_OverAllPotential").val(vaue);
    $("#MainContent_TargetDate").val(vaue);
    $("#MainContent_txtTitle").val(vaue);
    $("#MainContent_ddlProjectType").val(vaue);
    $("#MainContent_ExistingProduct").val(vaue);
    $("#MainContent_Component").val(vaue);
    $("#MainContent_CompetitionSpec").val(vaue);
    $("#MainContent_Stages").val("4");
    $("#MainContent_tab1txtGoal").val(vaue);
    $("#MainContent_tab1txtRemarks_1").val(vaue);
    $("#MainContent_tab1Target").val(vaue);


    $("#MainContent_tab2txtGoal").val(vaue);
    $("#MainContent_tab2txtRemarks_1").val(vaue);
    $("#MainContent_tab2Target").val(vaue);

    $("#MainContent_tab3txtGoal").val(vaue);
    $("#MainContent_tab3txtRemarks_1").val(vaue);
    $("#MainContent_tab3Target").val(vaue);

    $("#MainContent_tab4txtGoal").val(vaue);
    $("#MainContent_tab4txtRemarks_1").val(vaue);
    $("#MainContent_tab4Target").val(vaue);

    $("#MainContent_tab5txtGoal").val(vaue);
    $("#MainContent_tab5txtRemarks_1").val(vaue);
    $("#MainContent_tab5Target").val(vaue);

    $("#MainContent_tab6txtGoal").val(vaue);
    $("#MainContent_tab6txtRemarks_1").val(vaue);
    $("#MainContent_tab6Target").val(vaue);

    $("#MainContent_tab7txtGoal").val(vaue);
    $("#MainContent_tab7txtRemarks_1").val(vaue);
    $("#MainContent_tab7Target").val(vaue);

    $("#MainContent_tab8txtGoal").val(vaue);
    $("#MainContent_tab8txtRemarks_1").val(vaue);
    $("#MainContent_tab8Target").val(vaue);

    $("#MainContent_tab9txtGoal").val(vaue);
    $("#MainContent_tab9txtRemarks_1").val(vaue);
    $("#MainContent_tab9Target").val(vaue);

    $("#MainContent_tab10txtGoal").val(vaue);
    $("#MainContent_tab10txtRemarks_1").val(vaue);
    $("#MainContent_tab10Target").val(vaue);
}
function checkNullForCommonFields() {
    debugger;
    var returnVal = true;
    var Escalated_To = $("#MainContent_ddlEscalate").val();
    var Customer_name = $("#MainContent_ddlCustomerName").val();
    var Customer_num = $("#MainContent_ddlCustomerNo").val();
    var Product = $("#MainContent_ExistingProduct").val();
    var Component = $("#MainContent_Component").val();
    var Competition = $("#MainContent_CompetitionSpec").val();
    var Stages = $("#MainContent_Stages").val();
    var Main_Target = $("#MainContent_TargetDate").val();
    var OverAll_Potential = $("#MainContent_OverAllPotential").val();
    var Pot_Lakhs = $("#MainContent_Potential").val();
    var Business = $("#MainContent_Business").val();
    var Industry = $("#MainContent_ddlIndustry").val();
    var Owner = $("#MainContent_ddlSalesEngName").val();
    var Reviewer = $("#MainContent_ddlReviewer").val();
    var ProjectTitle = $("#MainContent_txtTitle").val();
    var Existing_ProjectTitle = $("#MainContent_ddlTitle").val();
    var Project_Type = $("#MainContent_ddlProjectType").val();

    if ($('#MainContent_new_project').is(':checked')) {
        // debugger;

        if (ProjectTitle == "" || ProjectTitle == "-- NO PROJECT --" || ProjectTitle == "-- SELECT PROJECT --") {
            returnVal = false;
            $('#MainContent_txtTitle').css('border-color', 'red');
        }
    }
    if ($('#MainContent_existing_project').is(':checked')) {
        if (Existing_ProjectTitle == "-- NO PROJECT --" | Existing_ProjectTitle == "-- SELECT PROJECT --") {
            $(".lblProduct").show();
            returnVal = false;
            // alert('Oops Project Title is Invalid, Please Select New Project to enter the Project Name');
            alert('You are trying to create new project.For this please click on new project radio button.');
            return returnVal;
        }
    }
    if (Project_Type == "-- SELECT --") {

        $('#MainContent_ddlProjectType').css('border-color', 'red');
        returnVal = false;
    }
    else {

        $('#MainContent_ddlProjectType').css('border-color', '');

    }
    if (Owner == "-- SELECT --") {

        $('#MainContent_ddlSalesEngName').css('border-color', 'red');
        returnVal = false;
    }
    else {

        $('#MainContent_ddlSalesEngName').css('border-color', '');

    }

    if (Reviewer == "-- SELECT --") {

        $('#MainContent_ddlReviewer').css('border-color', 'red');
        returnVal = false;
    }
    else {

        $('#MainContent_ddlReviewer').css('border-color', '');

    }
    if (Product == "") {
        $(".lblProduct").show();
        returnVal = false;
        $('#MainContent_ExistingProduct').css('border-color', 'red');
    }
    else {
        $(".lblProduct").hide();
        $('#MainContent_ExistingProduct').css('border-color', '');
    }

    if (Component == "") {
        $(".lblComponent").show();
        returnVal = false;
        $('#MainContent_Component').css('border-color', 'red');
    }
    else {
        $(".lblComponent").hide();
        $('#MainContent_Component').css('border-color', '');

    }
    if (Competition == "") {
        $(".lblCompetition").show();
        returnVal = false;
        $('#MainContent_CompetitionSpec').css('border-color', 'red');
    }
    else {
        $(".lblCompetition").hide();
        $('#MainContent_CompetitionSpec').css('border-color', '');

    }
    if (Stages == "--SELECT STAGES--") {
        $(".lblStages").show();
        returnVal = false;
        $('#MainContent_Stages').css('border-color', 'red');
    }
    else {
        $(".lblStages").hide();
        $('#MainContent_Stages').css('border-color', '');

    }
    if (Main_Target == "") {
        $(".lblTarget").show();
        $('#MainContent_TargetDate').css('border-color', 'red');
        returnVal = false;
    }
    else {
        $(".lblTarget").hide();
        $('#MainContent_TargetDate').css('border-color', '');
    }
    if (OverAll_Potential == "") {
        $(".lblover_Pot").show();
        returnVal = false;
        $('#MainContent_OverAllPotential').css('border-color', 'red');
    }
    else {
        $(".lblover_Pot").hide();
        $('#MainContent_OverAllPotential').css('border-color', '');
    }

    if (Pot_Lakhs == "") {
        $(".lblPot_Lakhs").show();
        returnVal = false;
        $('#MainContent_Potential').css('border-color', 'red');
    }
    else {
        $(".lblPot_Lakhs").hide();
        $('#MainContent_Potential').css('border-color', '');
    }

    if (Business == "") {
        $(".lblBusiness_Exp").show();
        $('#MainContent_Business').css('border-color', 'red');
        returnVal = false;
    }
    else {
        $(".lblBusiness_Exp").hide();
        $('#MainContent_Business').css('border-color', '');
    }


    if (Escalated_To == "-- SELECT --") {
        $(".lblEscalate").show();
        $('#MainContent_ddlEscalate').css('border-color', 'red');
        returnVal = false;
    }
    else {
        $(".lblEscalate").hide();
        $('#MainContent_ddlEscalate').css('border-color', '');

    }

    if (Industry == "-- SELECT --") {
        $(".lblIndustry").show();
        returnVal = false;
        $('#MainContent_ddlIndustry').css('border-color', 'red');
    }
    else {
        $(".lblIndustry").hide();
        $('#MainContent_ddlIndustry').css('border-color', '');


    }
    return returnVal;
}
function PageLoadScripts() {
    BindCalendar();
    ShowHideStages();
    AllowNumericData();
    AddClassWhenDisabled();
    triggerPostGridLodedActions();
    HideStatus();
    LoadClosingFields();
    DisableCompletionDatePickers();
    EnableCompletionDatePickers();
    DisableTargetDatePickers();
    clearallpagedetails();

    if (document.referrer.split('/')[4] == "ProjectsDashboard.aspx?Projects") {
        $("#crumbs2").css("display", "block");
        $("#crumbs3").css("display", "none");
        $("#crumbs1").css("display", "none");
    }
    else if (document.referrer.split('/')[4] == "PendingTasks.aspx?Approve") {
        $("#crumbs3").css("display", "block");
        $("#crumbs1").css("display", "none");
        $("#crumbs2").css("display", "none");
    }
    else {
        $("#crumbs1").css("display", "block");
        $("#crumbs2").css("display", "none");
        $("#crumbs3").css("display", "none");
    }

    if ($("#MainContent_Target_flag").val() == "True") {
        $("#MainContent_TargetDate").datepicker("disable");
    }
    else {
        $("#MainContent_TargetDate").datepicker("enable");
    }

    $("#dv_direct_customer").show();
    $("#dv_channel_partner_1").hide();
    $("#dv_channel_partner_2").hide();

    if ($("#MainContent_direct_customer").is(':checked')) {

        $("#dv_direct_customer").show();
        $("#dv_channel_partner_1").hide();
        $("#dv_channel_partner_2").hide();
    }

    if ($("#MainContent_channel_partner").is(':checked')) {

        $("#dv_direct_customer").hide();
        $("#dv_channel_partner_1").show();
        $("#dv_channel_partner_2").show();
    }


    $("#dvnew_project").hide();
    if ($('#MainContent_existing_project').is(':checked')) {
        $("#dvnew_project").hide();
        $("#dvexisting_project").show();

    }
    if ($('#MainContent_new_project').is(':checked')) {
        $("#dvnew_project").show();
        $("#dvexisting_project").hide();

    }
    var tab_number = $("#MainContent_tab_number").val();
    LoadNextStage(tab_number);
    $("#MainContent_existing_project").change(function () {
        $("#dvnew_project").hide();
        $("#dvexisting_project").show();

    });

    $("#MainContent_new_project").change(function () {
        $("#dvnew_project").show();
        $("#dvexisting_project").hide();

    });

    $("#MainContent_Stages").change(function (e) {
        ShowHideStages();
    });

    $("#MainContent_direct_customer").change(function () {
        $("#dv_direct_customer").show();
        $("#dv_channel_partner_1").hide();
        $("#dv_channel_partner_2").hide();
    });

    $("#MainContent_channel_partner").change(function () {

        $("#dv_direct_customer").hide();
        $("#dv_channel_partner_1").show();
        $("#dv_channel_partner_2").show();
    });

    TargetdatesValidations();
}



//$("#MainContent_TargetDate").keyup(function (e) {
//    e.preventDefault();
//    $("#MainContent_TargetDate").val("");
//    alert("Please use datepicker to enter date.");
//});
//$("#MainContent_tab1Target").keyup(function (e) {
//    e.preventDefault();
//    $("#MainContent_tab1Target").val("");
//    alert("Please use datepicker to enter date.");
//});
//$("#MainContent_tab2Target").keyup(function (e) {
//    e.preventDefault();
//    $("#MainContent_tab2Target").val("");
//    alert("Please use datepicker to enter date.");
//});
//$("#MainContent_tab3Target").keyup(function (e) {
//    e.preventDefault();
//    $("#MainContent_tab3Target").val("");
//    alert("Please use datepicker to enter date.");
//});
//$("#MainContent_tab4Target").keyup(function (e) {
//    e.preventDefault();
//    $("#MainContent_tab4Target").val("");
//    alert("Please use datepicker to enter date.");
//});
//$("#MainContent_tab5Target").keyup(function (e) {
//    e.preventDefault();
//    $("#MainContent_tab5Target").val("");
//    alert("Please use datepicker to enter date.");
//});
//$("#MainContent_tab6Target").keyup(function (e) {
//    e.preventDefault();
//    $("#MainContent_tab6Target").val("");
//    alert("Please use datepicker to enter date.");
//});
//$("#MainContent_tab7Target").keyup(function (e) {
//    e.preventDefault();
//    $("#MainContent_tab7Target").val("");
//    alert("Please use datepicker to enter date.");
//});
//$("#MainContent_tab8Target").keyup(function (e) {
//    e.preventDefault();
//    $("#MainContent_tab8Target").val("");
//    alert("Please use datepicker to enter date.");
//});
//$("#MainContent_tab9Target").keyup(function (e) {
//    e.preventDefault();
//    $("#MainContent_tab9Target").val("");
//    alert("Please use datepicker to enter date.");
//});
//$("#MainContent_tab10Target").keyup(function (e) {
//    e.preventDefault();
//    $("#MainContent_tab10Target").val("");
//    alert("Please use datepicker to enter date.");
//});

//$("#MainContent_tab1Completion").keyup(function (e) {
//    e.preventDefault();
//    $("#MainContent_tab1Completion").val("");
//    alert("Please use datepicker to enter date.");
//});
//$("#MainContent_tab2Completion").keyup(function (e) {
//    e.preventDefault();
//    $("#MainContent_tab2Completion").val("");
//    alert("Please use datepicker to enter date.");
//});
//$("#MainContent_tab3Completion").keyup(function (e) {
//    e.preventDefault();
//    $("#MainContent_tab3Completion").val("");
//    alert("Please use datepicker to enter date.");
//});
//$("#MainContent_tab4Completion").keyup(function (e) {
//    e.preventDefault();
//    $("#MainContent_tab4Completion").val("");
//    alert("Please use datepicker to enter date.");
//});
//$("#MainContent_tab5Completion").keyup(function (e) {
//    e.preventDefault();
//    $("#MainContent_tab5Completion").val("");
//    alert("Please use datepicker to enter date.");
//});
//$("#MainContent_tab6Completion").keyup(function (e) {
//    e.preventDefault();
//    $("#MainContent_tab6Completion").val("");
//    alert("Please use datepicker to enter date.");
//});
//$("#MainContent_tab7Completion").keyup(function (e) {
//    e.preventDefault();
//    $("#MainContent_tab7Completion").val("");
//    alert("Please use datepicker to enter date.");
//});
//$("#MainContent_tab8Completion").keyup(function (e) {
//    e.preventDefault();
//    $("#MainContent_tab8Completion").val("");
//    alert("Please use datepicker to enter date.");
//});
//$("#MainContent_tab9Completion").keyup(function (e) {
//    e.preventDefault();
//    $("#MainContent_tab9Completion").val("");
//    alert("Please use datepicker to enter date.");
//});
//$("#MainContent_tab10ompletion").keyup(function (e) {
//    e.preventDefault();
//    $("#MainContent_tab10Completion").val("");
//    alert("Please use datepicker to enter date.");
//});



//$("#MainContent_tab1_btnReject").click(function () {
//    var remarks = $("#MainContent_tab1_txtApprove").val();
//    if (remarks == "") {
//        alert("Please add remarks to reject.");
//        return false;
//    }
//    else {
//        return true;
//    }

//});
//$("#MainContent_tab2_btnReject").click(function () {
//    var remarks = $("#MainContent_tab2_txtApprove").val();
//    if (remarks == "") {
//        alert("Please add remarks to reject.");
//        return false;
//    }
//    else {
//        return true;
//    }

//});
//$("#MainContent_tab3_btnReject").click(function () {
//    var remarks = $("#MainContent_tab3_txtApprove").val();
//    if (remarks == "") {
//        alert("Please add remarks to reject.");
//        return false;
//    }
//    else {
//        return true;
//    }

//});
//$("#MainContent_tab4_btnReject").click(function () {
//    var remarks = $("#MainContent_tab4_txtApprove").val();
//    if (remarks == "") {
//        alert("Please add remarks to reject.");
//        return false;
//    }
//    else {
//        return true;
//    }

//});
//$("#MainContent_tab5_btnReject").click(function () {
//    var remarks = $("#MainContent_tab5_txtApprove").val();
//    if (remarks == "") {
//        alert("Please add remarks to reject.");
//        return false;
//    }
//    else {
//        return true;
//    }

//});
//$("#MainContent_tab6_btnReject").click(function () {
//    var remarks = $("#MainContent_tab6_txtApprove").val();
//    if (remarks == "") {
//        alert("Please add remarks to reject.");
//        return false;
//    }
//    else {
//        return true;
//    }

//});
//$("#MainContent_tab7_btnReject").click(function () {
//    var remarks = $("#MainContent_tab7_txtApprove").val();
//    if (remarks == "") {
//        alert("Please add remarks to reject.");
//        return false;
//    }
//    else {
//        return true;
//    }

//});
//$("#MainContent_tab8_btnReject").click(function () {
//    var remarks = $("#MainContent_tab8_txtApprove").val();
//    if (remarks == "") {
//        alert("Please add remarks to reject.");
//        return false;
//    }
//    else {
//        return true;
//    }

//});
//$("#MainContent_tab9_btnReject").click(function () {
//    var remarks = $("#MainContent_tab9_txtApprove").val();
//    if (remarks == "") {
//        alert("Please add remarks to reject.");
//        return false;
//    }
//    else {
//        return true;
//    }

//});
//$("#MainContent_tab10_btnReject").click(function () {
//    var remarks = $("#MainContent_tab10_txtApprove").val();
//    if (remarks == "") {
//        alert("Please add remarks to reject.");
//        return false;
//    }
//    else {
//        return true;
//    }

//});

//function CustomerNameDropdownValidation(Customer_name, objcontrolName, alertmsg) {
//    if (Customer_name == "-- SELECT CUSTOMER --") {     
//        {
//            $('#errcustName').append('<p>' + alertmsg + '</p>').css('color', 'red');
//            $(objcontrolName).css('border-color', 'red');
//            return false;
//        }
//    }
//    else {
//        $(objcontrolName).css('border-color', '');
//        $('#errcustName').hide();
//    }
//}

//function DistributorCustomerNameDropdownValidation(DistributorCustomer_name, objcontrolName, alertmsg) {
//    if (DistributorCustomer_name == "-- SELECT CUSTOMER --" || DistributorCustomer_name == "-- NO CUSTOMER --") {
//        $('#errDistcustname').append('<p>' + alertmsg + '</p>').css('color', 'red');
//        $(objcontrolName).css('border-color', 'red');
        
//        return false;
//    }
//    else {
//        $(objcontrolName).css('border-color', '');
//        $('#errDistcustname').hide();
//    }
//}


//function ChannepertnerDropdownValidation(ChannelPartner, objcontrolName, alertmsg) {
//    if (ChannelPartner == "-- SELECT CHANNEL PARTNER --") {
//        $('#errchannelpartner').append('<p>' + alertmsg + '</p>').css('color', 'red');
//        $(objcontrolName).css('border-color', 'red');
//        return false;
//    }
//    else {
//        $(objcontrolName).css('border-color', '');
//        $('#errchannelpartner').hide();
//    }

//}

