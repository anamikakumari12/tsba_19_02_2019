﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.DataVisualization.Charting;
using System.Web.UI.WebControls;

namespace TaegutecSalesBudget
{
    public partial class ProjectsDashboard : System.Web.UI.Page
    {
        #region Global Declaration
        PrjctsDashboard objprjctdashboard = new PrjctsDashboard();
        Reports objreports = new Reports();
        Review objRSum = new Review();
        public static DataTable dt_trgtd_cmpltn_prjcts, dt_pndng_prjcts, dt_overdue_stgs, dt_inustry_bsns_val, dt_cnsldt_industry_val, dt_projecttype_potential;
        public static decimal cmpltdProjectsVal;
        public static string cter;
        public string ddBranch, ddSalesEngineer, ddCustomerClass;
        #endregion

        #region Events

        /// <summary>
        /// Modified By : Anamika
        /// Date : Dec 6, 2016
        /// Desc : Loading all the charts based on default selection of Branch, Customer Class and Owner
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

            Session["FR"] = "0";
            Session["Customer_Number"] = null;
            Session["Project_Number"] = null;
            if (Session["UserName"] != null)
            {
                if (!IsPostBack)
                {
                    Session["FR"] = "1";
                    if (Session["RoleId"].ToString() == "HO")
                    {
                        if (Session["cter"] == null)
                        {
                            Session["cter"] = "TTA";
                            cter = "TTA";

                        }

                        if (Session["cter"].ToString() == "DUR")
                        {
                            rdBtnDuraCab.Checked = true;
                            rdBtnTaegutec.Checked = false;
                            cter = "DUR";
                        }
                        else
                        {
                            rdBtnTaegutec.Checked = true;
                            rdBtnDuraCab.Checked = false;
                            cter = "TTA";
                        }
                    }
                    else if (Session["RoleId"].ToString() == "BM")
                    {
                        cter = null;
                     // divBranch.Visible = false;
                        div5.Visible = false;
                    }
                    else if (Session["RoleId"].ToString() == "SE")
                    {
                        cter = null;
                       divBranch.Visible = false;
                     divSalesEngnr.Visible = false;
                     div6.Visible = false;
                     div5.Visible = false;
                    }
                    LoadBranches();
                    LoadTrgtdCmpltnPrjctsInDaysChart();
                    LoadPndngStagewisePrjctChart();
                    LoadOverdueChart();
                    cnsldtindustrybargraph();
                    industrybargraph();
                    LoadProjectTypePotential();
                }

            }
            else { Response.Redirect("Login.aspx?Login"); }
        }

        /// <summary>
        /// Modified By : Anamika
        /// Date : Dec 6, 2016
        /// Desc : Loading all the owners based on selection of Branch
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlBranchList_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

            string roleId = Session["RoleId"].ToString();
            string userId = Session["UserId"].ToString();
            string branchcode = Session["BranchCode"].ToString();
            objRSum.BranchCode = roleId == "TM" && ddlBranchList.SelectedItem.Value == "ALL" ? userId : ddlBranchList.SelectedItem.Value;
            objRSum.roleId = roleId;
            objRSum.flag = "SalesEngineer";
            objRSum.cter = cter;
            if (roleId == "BM")
            {
                objRSum.BranchCode = Session["BranchCode"].ToString();
            }
            DataTable dtData = objRSum.getFilterAreaValue(objRSum);

            if (dtData.Rows.Count != 0)
            {
                ddlSalesEngineerList.DataSource = dtData;
                ddlSalesEngineerList.DataTextField = "EngineerName";
                ddlSalesEngineerList.DataValueField = "EngineerId";
                ddlSalesEngineerList.DataBind();
                ddlSalesEngineerList.Items.Insert(0, "ALL");
            }
            else
            {
                ddlSalesEngineerList.DataSource = dtData;
                ddlSalesEngineerList.DataTextField = "EngineerName";
                ddlSalesEngineerList.DataValueField = "EngineerId";
                ddlSalesEngineerList.DataBind();
                ddlSalesEngineerList.Items.Insert(0, "NO SALES ENGINEER");
            }

            
            ddlSalesEngineerList_SelectedIndexChanged(null, null);
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "loadScript();", true);

        }


        protected void ddlSalesEngineerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "loadScript();", true);

        }

        /// <summary>
        /// Modified By : Anamika
        /// Date : Dec 6, 2016
        /// Desc : Loading all the charts based on selection of Sales Engineer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rdBtnTaegutec_CheckedChanged(object sender, EventArgs e)
        {
            if (rdBtnTaegutec.Checked)
            {
                Session["cter"] = "TTA";
                cter = "TTA";

            }
            if (rdBtnDuraCab.Checked)
            {
                Session["cter"] = "DUR";
                cter = "DUR";
            }
            LoadBranches();
            LoadTrgtdCmpltnPrjctsInDaysChart();
            LoadPndngStagewisePrjctChart();
            LoadOverdueChart();
            cnsldtindustrybargraph();
            industrybargraph();
            LoadProjectTypePotential();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "loadScript();", true);

        }

        /// <summary>
        /// Modified By : Anamika
        /// Date : Dec 6, 2016
        /// Desc : Loading all the charts based on selection of Sales Engineer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void filter_Click(object sender, EventArgs e)
        {

            ddBranch = ddlBranchList.SelectedItem.Value == "ALL" ? null : ddlBranchList.SelectedItem.Value;
            ddSalesEngineer = ddlSalesEngineerList.SelectedItem.Value;
            ddCustomerClass = ddlCustomerClass.SelectedItem.Value == "ALL" ? null : ddlCustomerClass.SelectedItem.Value;
            if (ddSalesEngineer == "ALL" || ddSalesEngineer == "NO SALES ENGINEER")
            {
                ddSalesEngineer = null;
            }
            LoadTrgtdCmpltnPrjctsInDaysChart();
            LoadPndngStagewisePrjctChart();
            LoadOverdueChart();
            cnsldtindustrybargraph();
            industrybargraph();
            LoadProjectTypePotential();
       
          
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "loadScript();", true);

        }
        #endregion

        #region Methods
        /// <summary>
        /// Desc : It will load the branch values to ddlBranchList
        /// </summary>
        protected void LoadBranches()
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string roleId = Session["RoleId"].ToString();
            string userId = Session["UserId"].ToString();
            string branchcode = Session["BranchCode"].ToString();
            DataTable dtData = new DataTable();
            objRSum.BranchCode = userId; // passing here territory Engineer Id  as branch code IF role is TM 
            objRSum.roleId = roleId;
            objRSum.flag = "Branch";
            objRSum.cter = cter;
            dtData = objRSum.getFilterAreaValue(objRSum);

            ddlBranchList.DataSource = dtData;
            ddlBranchList.DataTextField = "BranchDesc";
            ddlBranchList.DataValueField = "BranchCode";
            ddlBranchList.DataBind();
            ddlBranchList.Items.Insert(0, "ALL");
          //  ddlBranchList.SelectedValue = "ALL";
            ddlBranchList_SelectedIndexChanged(null, null);
        }

        /// <summary>
        /// Modified By : Ananmika
        /// Date : Dec 6, 2016
        /// Desc : Fetch data to display through charts from database
        /// </summary>
        protected void LoadTrgtdCmpltnPrjctsInDaysChart()
        {
            dt_trgtd_cmpltn_prjcts = new DataTable();
            dt_pndng_prjcts = new DataTable();
            dt_overdue_stgs = new DataTable();
            dt_inustry_bsns_val = new DataTable();
            dt_projecttype_potential = new DataTable();
            string prcnt;
            decimal[] vals = new decimal[2];
            
         
            if (Session["RoleId"].ToString() == "HO")
            {
                divCter.Visible = true;

                dt_trgtd_cmpltn_prjcts = objprjctdashboard.getProjectsProgress(ddSalesEngineer, ddBranch, ddCustomerClass, cter);
                dt_pndng_prjcts = objprjctdashboard.getPendingStages(ddSalesEngineer, ddBranch, ddCustomerClass, cter);
                dt_overdue_stgs = objprjctdashboard.getOverdueStages(ddSalesEngineer, ddBranch, ddCustomerClass, cter);
                vals = (objprjctdashboard.getCompletedProjectVal(ddSalesEngineer, ddBranch, ddCustomerClass, cter));//Purpose:To Calc % of ProjectsVal cmpltd
                lblprjcttotal.Text = vals[1].ToString();
                lblcmpltdval.Text = vals[0].ToString();
                Session["  dt_trgtd_cmpltn_prjcts"] = dt_trgtd_cmpltn_prjcts;
                          prcnt = vals[1] == 0 ? "0" : (vals[0] / vals[1]).ToString();
                hdnfieldprojectval.Value = prcnt;
                dt_inustry_bsns_val = objprjctdashboard.getIndustryBusinessVal(ddSalesEngineer, ddBranch, ddCustomerClass, cter);

                dt_cnsldt_industry_val = objprjctdashboard.getCnsldtdIndustryBusinessVal(ddSalesEngineer, ddBranch, ddCustomerClass, cter);
             
                dt_projecttype_potential = objprjctdashboard.getProjectTypePotential(ddSalesEngineer, ddBranch, ddCustomerClass);
                Session["ProjectPotential"] = dt_projecttype_potential;
                Session["IndustryView"] = dt_inustry_bsns_val;
                Session["ConsolidatedIndustryValue"] = dt_cnsldt_industry_val;
            }
            else if (Session["RoleId"].ToString() == "TM")
            {
                string tm_id = Session["UserId"].ToString();
                if (string.IsNullOrEmpty(ddBranch))
                {
                    ddBranch = Session["UserId"].ToString();
                }
                dt_trgtd_cmpltn_prjcts = objprjctdashboard.getProjectsProgress(ddSalesEngineer, ddBranch, ddCustomerClass);
                dt_pndng_prjcts = objprjctdashboard.getPendingStages(ddSalesEngineer, ddBranch, ddCustomerClass);
                dt_overdue_stgs = objprjctdashboard.getOverdueStages(ddSalesEngineer, ddBranch, ddCustomerClass);
                vals = (objprjctdashboard.getCompletedProjectVal(ddSalesEngineer, ddBranch, ddCustomerClass));//Purpose:To Calc % of ProjectsVal cmpltd
                lblprjcttotal.Text = vals[1].ToString();
                lblcmpltdval.Text = vals[0].ToString();
                prcnt = vals[1] == 0 ? "0" : (vals[0] / vals[1]).ToString();
                hdnfieldprojectval.Value = prcnt;
                dt_inustry_bsns_val = objprjctdashboard.getIndustryBusinessVal(ddSalesEngineer, ddBranch, ddCustomerClass);
                dt_cnsldt_industry_val = objprjctdashboard.getCnsldtdIndustryBusinessVal(ddSalesEngineer, ddBranch, ddCustomerClass);
                dt_projecttype_potential = objprjctdashboard.getProjectTypePotential(ddSalesEngineer, ddBranch, ddCustomerClass);
                Session["ProjectPotential"] = dt_projecttype_potential;
                Session["IndustryView"] = dt_inustry_bsns_val;
                Session["ConsolidatedIndustryValue"] = dt_cnsldt_industry_val;
                
            }
            else if (Session["RoleId"].ToString() == "BM")
            {
                string branch_code = Session["BranchCode"].ToString();
                dt_trgtd_cmpltn_prjcts = objprjctdashboard.getProjectsProgress(ddSalesEngineer, branch_code, ddCustomerClass);
                dt_pndng_prjcts = objprjctdashboard.getPendingStages(ddSalesEngineer, branch_code, ddCustomerClass);
                dt_overdue_stgs = objprjctdashboard.getOverdueStages(ddSalesEngineer, branch_code, ddCustomerClass);
                vals = (objprjctdashboard.getCompletedProjectVal(ddSalesEngineer, branch_code, ddCustomerClass));//Purpose:To Calc % of ProjectsVal cmpltd
                lblprjcttotal.Text = vals[1].ToString();
                lblcmpltdval.Text = vals[0].ToString();
                prcnt = vals[1] == 0 ? "0" : (vals[0] / vals[1]).ToString();//val[0](completedval)/vals[1](total project val)
                hdnfieldprojectval.Value = prcnt;
                dt_inustry_bsns_val = objprjctdashboard.getIndustryBusinessVal(ddSalesEngineer, branch_code, ddCustomerClass);
                dt_cnsldt_industry_val = objprjctdashboard.getCnsldtdIndustryBusinessVal(ddSalesEngineer, branch_code, ddCustomerClass);
                dt_projecttype_potential = objprjctdashboard.getProjectTypePotential(ddSalesEngineer, branch_code, ddCustomerClass);
                Session["ProjectPotential"] = dt_projecttype_potential;
                Session["IndustryView"] = dt_inustry_bsns_val;
                Session["ConsolidatedIndustryValue"] = dt_cnsldt_industry_val;
            }
            else if (Session["RoleId"].ToString() == "SE")
            {
                string sales_engnr_id = Session["UserId"].ToString();
                dt_trgtd_cmpltn_prjcts = objprjctdashboard.getProjectsProgress(sales_engnr_id, null, ddCustomerClass);
                dt_pndng_prjcts = objprjctdashboard.getPendingStages(sales_engnr_id, null, ddCustomerClass);
                dt_overdue_stgs = objprjctdashboard.getOverdueStages(sales_engnr_id, null, ddCustomerClass);
                vals = (objprjctdashboard.getCompletedProjectVal(sales_engnr_id, null, ddCustomerClass));//Purpose:To Calc % of ProjectsVal cmpltd
                lblprjcttotal.Text = vals[1].ToString();
                lblcmpltdval.Text = vals[0].ToString();
                prcnt = vals[1] == 0 ? "0" : (vals[0] / vals[1]).ToString();
                hdnfieldprojectval.Value = prcnt;
                dt_inustry_bsns_val = objprjctdashboard.getIndustryBusinessVal(sales_engnr_id, null, ddCustomerClass);
                dt_cnsldt_industry_val = objprjctdashboard.getCnsldtdIndustryBusinessVal(sales_engnr_id, null, ddCustomerClass);
                dt_projecttype_potential = objprjctdashboard.getProjectTypePotential(sales_engnr_id, null, ddCustomerClass);
                Session["ProjectPotential"] = dt_projecttype_potential;
                Session["IndustryView"] = dt_inustry_bsns_val;
                Session["ConsolidatedIndustryValue"] = dt_cnsldt_industry_val;
               
            }

            DataTable dt = new DataTable();
            DataTable griddt = new DataTable();
            dt.Columns.Add(new DataColumn("Value1"));
            dt.Columns.Add(new DataColumn("Value2"));
            dt.Columns.Add(new DataColumn("Value3"));
            griddt.Columns.Add(new DataColumn("Target"));
            griddt.Columns.Add(new DataColumn("Count"));
            griddt.Columns.Add(new DataColumn("Value"));
            int cnt = dt_trgtd_cmpltn_prjcts.Rows.Count - 1;
            decimal total = 0;
            for (int i = 0; i <= cnt; i++)
            {
                total += dt_trgtd_cmpltn_prjcts.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToDecimal(dt_trgtd_cmpltn_prjcts.Rows[i].ItemArray[0].ToString());
            }
            for (int i = 0; i <= cnt; i++)
            {
                decimal trmp = dt_trgtd_cmpltn_prjcts.Rows[i].ItemArray[0].ToString() == "" || dt_trgtd_cmpltn_prjcts.Rows[i].ItemArray[0].ToString() == null ? 0 : Convert.ToDecimal(dt_trgtd_cmpltn_prjcts.Rows[i].ItemArray[0].ToString());
                decimal prcntval = total == 0 ? 0 : trmp / total * 100;
                dt.Rows.Add("", dt_trgtd_cmpltn_prjcts.Rows[i].ItemArray[0].ToString(), "Value :" + " " + dt_trgtd_cmpltn_prjcts.Rows[i].ItemArray[0].ToString());
                griddt.Rows.Add("", dt_trgtd_cmpltn_prjcts.Rows[i].ItemArray[0].ToString() == "" ? "0.00" : dt_trgtd_cmpltn_prjcts.Rows[i].ItemArray[0].ToString(),
                 Convert.ToString(Math.Round(prcntval)) + '%');
            }

            dt.Rows[0][0] = griddt.Rows[0][0] = " <30 Days";
            dt.Rows[1][0] = griddt.Rows[1][0] = "31-60";
            dt.Rows[2][0] = griddt.Rows[2][0] = "61-90";
            dt.Rows[3][0] = griddt.Rows[3][0] = "91-180";
            dt.Rows[4][0] = griddt.Rows[4][0] = ">180";


            //tooltip
            dt.Rows[0][2] = "Target : <30 Days" + "\n" + "Project Potential :" + " " + dt_trgtd_cmpltn_prjcts.Rows[0].ItemArray[0].ToString();
            dt.Rows[1][2] = "Target : 31-60 " + "\n" + "Project Potential :" + " " + dt_trgtd_cmpltn_prjcts.Rows[1].ItemArray[0].ToString();
            dt.Rows[2][2] = "Target : 61-90 " + "\n" + "Project Potential :" + " " + dt_trgtd_cmpltn_prjcts.Rows[2].ItemArray[0].ToString();
            dt.Rows[3][2] = "Target : 91-180 " + "\n" + "Project Potential :" + " " + dt_trgtd_cmpltn_prjcts.Rows[3].ItemArray[0].ToString();
            dt.Rows[4][2] = "Target : >180" + "\n" + "Project Potential :" + " " + dt_trgtd_cmpltn_prjcts.Rows[4].ItemArray[0].ToString();
            //end
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i].ItemArray[1].ToString() == "0.00" || dt.Rows[i].ItemArray[1].ToString() == "" || dt.Rows[i].ItemArray[1].ToString() == "0" || dt.Rows[i].ItemArray[1].ToString() == null)
                    {
                        dt.Rows[i].Delete();
                        i = -1;
                    }
                }
            }
            //chart_target.DataSource = dt;
            //chart_target.Series[0].XValueMember = "Value1";
            //chart_target.Series[0].YValueMembers = "Value2";
            //chart_target.Series[0].Label = "#PERCENT{P0}";
            //chart_target.Series[0].LegendText = "#AXISLABEL";
            //chart_target.DataBind();
            //chart_target.Series["Series1"].Font = new System.Drawing.Font("Helvetica Neue", 10, System.Drawing.FontStyle.Bold);

            //for (int cnts = 0; cnts < chart_target.Series[0].Points.Count; cnts++)
            //{
            //    chart_target.Series[0].Points[cnts].ToolTip = dt.Rows[cnts]["Value3"].ToString();
            //}
            //foreach (Series charts in chart_target.Series)
            //{
            //    foreach (DataPoint point in charts.Points)
            //    {
            //        switch (point.AxisLabel)
            //        {
            //            case " <30 Days": point.Color = ColorTranslator.FromHtml("#399BFF"); break;
            //            case "31-60": point.Color = ColorTranslator.FromHtml("#26A65B"); break;
            //            case "61-90": point.Color = ColorTranslator.FromHtml("#EF4836"); break;
            //            case "91-180": point.Color = ColorTranslator.FromHtml("#F39C12"); break;
            //            case ">180": point.Color = ColorTranslator.FromHtml("#CB513A"); break;

            //        }
            //    }
            //}
            gridtrgtcmpltnprjcts.DataSource = griddt;
            Session["griddt"] = griddt;
            gridtrgtcmpltnprjcts.DataBind();


        }

        protected void LoadPndngStagewisePrjctChart()
        {

            DataTable dtPndngPrjcts = new DataTable();//Purpose:To bind to chart
            DataTable griddtPndngPrjcts = new DataTable();//Purpose TO bind to Gridtable
            dtPndngPrjcts.Columns.Add(new DataColumn("Value1"));
            dtPndngPrjcts.Columns.Add(new DataColumn("Value2"));
            dtPndngPrjcts.Columns.Add(new DataColumn("Value3"));
            griddtPndngPrjcts.Columns.Add(new DataColumn("Color"));
            griddtPndngPrjcts.Columns.Add(new DataColumn("Target"));
            griddtPndngPrjcts.Columns.Add(new DataColumn("Count"));
            griddtPndngPrjcts.Columns.Add(new DataColumn("Value"));
            int cnt = dt_pndng_prjcts.Rows.Count - 1;
            int j = 0;
            decimal total = 0;
            for (int i = 0; i <= cnt; i++)
            {
                total += dt_pndng_prjcts.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToDecimal(dt_pndng_prjcts.Rows[i].ItemArray[0].ToString());
            }
            for (int i = 0; i <= cnt; i++)
            {
                j++;
                if (dt_pndng_prjcts.Rows[i].ItemArray[0].ToString() != "" && dt_pndng_prjcts.Rows[i].ItemArray[0].ToString() != "0" && dt_pndng_prjcts.Rows[i].ItemArray[0].ToString() != null)
                {

                    decimal prcnt = dt_pndng_prjcts.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToDecimal(dt_pndng_prjcts.Rows[i].ItemArray[0].ToString()) / total * 100;

                    dtPndngPrjcts.Rows.Add("Stage" + " " + j, dt_pndng_prjcts.Rows[i].ItemArray[0].ToString(), "Stage :" + " " + j + "\n" + "Project Potential :" + " " + dt_pndng_prjcts.Rows[i].ItemArray[0].ToString());
                    griddtPndngPrjcts.Rows.Add("", "Stage" + " " + j, dt_pndng_prjcts.Rows[i].ItemArray[0].ToString() == "" ? "0" : dt_pndng_prjcts.Rows[i].ItemArray[0].ToString(),
                     Convert.ToString(Math.Round(prcnt)) + '%');

                }
            }
            if (dtPndngPrjcts.Rows.Count > 0)
            {
                for (int i = 0; i < dtPndngPrjcts.Rows.Count; i++)
                {
                    if (dtPndngPrjcts.Rows[i].ItemArray[0].ToString() == "0" || dtPndngPrjcts.Rows[i].ItemArray[0].ToString() == "")
                    {
                        dtPndngPrjcts.Rows[i].Delete();
                        i = -1;
                    }
                }
            }

            //chartPdngPrjcts.DataSource = dtPndngPrjcts;
            //chartPdngPrjcts.Series[0].XValueMember = "Value1";
            //chartPdngPrjcts.Series[0].YValueMembers = "Value2";
            //chartPdngPrjcts.Series[0].Label = "#PERCENT{P0}";
            //chartPdngPrjcts.Series[0].LegendText = "#AXISLABEL";
            //chartPdngPrjcts.DataBind();
            //chartPdngPrjcts.Series["Series1"].Font = new System.Drawing.Font("Helvetica Neue", 10, System.Drawing.FontStyle.Bold);

            //for (int cnts = 0; cnts < chartPdngPrjcts.Series[0].Points.Count; cnts++)
            //{
            //    chartPdngPrjcts.Series[0].Points[cnts].ToolTip = dtPndngPrjcts.Rows[cnts]["Value3"].ToString();
            //}
            //foreach (Series charts in chartPdngPrjcts.Series)
            //{
            //    foreach (DataPoint point in charts.Points)
            //    {
            //        switch (point.AxisLabel)
            //        {
            //            case "Stage 1": point.Color = ColorTranslator.FromHtml("#399BFF"); break;
            //            case "Stage 2": point.Color = ColorTranslator.FromHtml("#26A65B"); break;
            //            case "Stage 3": point.Color = ColorTranslator.FromHtml("#EF4836"); break;
            //            case "Stage 4": point.Color = ColorTranslator.FromHtml("#F39C12"); break;
            //            case "Stage 5": point.Color = ColorTranslator.FromHtml("#CB513A"); break;
            //            case "Stage 6": point.Color = ColorTranslator.FromHtml("#C7BAA7"); break;
            //            case "Stage 7": point.Color = ColorTranslator.FromHtml("#FAA43A"); break;
            //            case "Stage 8": point.Color = ColorTranslator.FromHtml("#5DA5DA"); break;
            //            case "Stage 9": point.Color = ColorTranslator.FromHtml("#4D4D4D"); break;
            //            case "Stage 10": point.Color = ColorTranslator.FromHtml("#FF00FF"); break;
            //        }

            //    }
            //}
            Session["griddtPndngPrjcts"] = griddtPndngPrjcts;

            GridPdngPrjcts.DataSource = griddtPndngPrjcts;
            GridPdngPrjcts.DataBind();
        }
        protected void LoadOverdueChart()
        {

            DataTable dtOverduePrjcts = new DataTable();
            DataTable griddtOverduePrjcts = new DataTable();
            dtOverduePrjcts.Columns.Add(new DataColumn("Value1"));
            dtOverduePrjcts.Columns.Add(new DataColumn("Value2"));
            dtOverduePrjcts.Columns.Add(new DataColumn("Value3"));
            griddtOverduePrjcts.Columns.Add(new DataColumn("Color"));
            griddtOverduePrjcts.Columns.Add(new DataColumn("Target"));
            griddtOverduePrjcts.Columns.Add(new DataColumn("Count"));
            griddtOverduePrjcts.Columns.Add(new DataColumn("Value"));
            int cnt = dt_overdue_stgs.Rows.Count - 1;
            int j = 0;
            decimal total = 0;
            for (int i = 0; i <= cnt; i++)
            {
                total += dt_overdue_stgs.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToDecimal(dt_overdue_stgs.Rows[i].ItemArray[0].ToString());
            }
            for (int i = 0; i <= cnt; i++)
            {
                j++;
                if (dt_overdue_stgs.Rows[i].ItemArray[0].ToString() != "" && dt_overdue_stgs.Rows[i].ItemArray[0].ToString() != "0" && dt_overdue_stgs.Rows[i].ItemArray[0].ToString() != null)
                {

                    decimal oprcnt = dt_overdue_stgs.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToDecimal(dt_overdue_stgs.Rows[i].ItemArray[0].ToString()) / total * 100;

                    dtOverduePrjcts.Rows.Add("Stage" + " " + j, dt_overdue_stgs.Rows[i].ItemArray[0].ToString(), "Stage :" + " " + j + "\n" + "Project Potential :" + " " + dt_overdue_stgs.Rows[i].ItemArray[0].ToString());
                    griddtOverduePrjcts.Rows.Add(" ", "Stage" + " " + j, dt_overdue_stgs.Rows[i].ItemArray[0].ToString() == "" ? "0" : dt_overdue_stgs.Rows[i].ItemArray[0].ToString(),
                     Convert.ToString(Math.Round(oprcnt)) + '%');

                }
            }


            if (dtOverduePrjcts.Rows.Count > 0)
            {
                for (int i = 0; i < dtOverduePrjcts.Rows.Count; i++)
                {
                    if (dtOverduePrjcts.Rows[i].ItemArray[0].ToString() == "0" || dtOverduePrjcts.Rows[i].ItemArray[0].ToString() == "")
                    {
                        dtOverduePrjcts.Rows[i].Delete();
                        i = -1;
                    }
                }
            }

        //    chartOverdue.DataSource = dtOverduePrjcts;
            Session["dtOverduePrjcts"] = dtOverduePrjcts;
            //chartOverdue.Series[0].XValueMember = "Value1";
            //chartOverdue.Series[0].YValueMembers = "Value2";
            //chartOverdue.Series[0].Label = "#PERCENT{P0}";
            //chartOverdue.Series[0].LegendText = "#AXISLABEL";
            //chartOverdue.DataBind();
            //chartOverdue.Series["Series1"].Font = new System.Drawing.Font("Helvetica Neue", 10, System.Drawing.FontStyle.Bold);

        //    for (int cnts = 0; cnts < chartOverdue.Series[0].Points.Count; cnts++)
        //    {
        ////        chartOverdue.Series[0].Points[cnts].ToolTip = dtOverduePrjcts.Rows[cnts]["Value3"].ToString();
        //    }
        //    foreach (Series charts in chartOverdue.Series)
        //    {
        //        foreach (DataPoint point in charts.Points)
        //        {
        //            switch (point.AxisLabel)
        //            {
        //                case "Stage 1": point.Color = ColorTranslator.FromHtml("#399BFF"); break;
        //                case "Stage 2": point.Color = ColorTranslator.FromHtml("#26A65B"); break;
        //                case "Stage 3": point.Color = ColorTranslator.FromHtml("#EF4836"); break;
        //                case "Stage 4": point.Color = ColorTranslator.FromHtml("#F39C12"); break;
        //                case "Stage 5": point.Color = ColorTranslator.FromHtml("#CB513A"); break;
        //                case "Stage 6": point.Color = ColorTranslator.FromHtml("#C7BAA7"); break;
        //                case "Stage 7": point.Color = ColorTranslator.FromHtml("#FAA43A"); break;
        //                case "Stage 8": point.Color = ColorTranslator.FromHtml("#5DA5DA"); break;
        //                case "Stage 9": point.Color = ColorTranslator.FromHtml("#4D4D4D"); break;
        //                case "Stage 10": point.Color = ColorTranslator.FromHtml("#FF00FF"); break;
        //            }

        //        }
        //    }
            GridViewOverdue.DataSource = griddtOverduePrjcts;
            Session["griddtOverduePrjcts"] = griddtOverduePrjcts;
            GridViewOverdue.DataBind();
        }

        protected void industrybargraph()
        {

         //  bargraphIndustry.DataSource = dt_inustry_bsns_val;
            //bargraphIndustry.Series["Customer Potential"].XValueMember = "IndustryName";
            //bargraphIndustry.Series["Customer Potential"].YValueMembers = "Ovrl_ptnl";
            //bargraphIndustry.Series["Project Potential"].YValueMembers = "Potential";
            //bargraphIndustry.Series["Business Expected"].YValueMembers = "Bsns_Exptd";
            //bargraphIndustry.Series["Monthly Business Expected"].YValueMembers = "ytd";
            //bargraphIndustry.ChartAreas[0].AxisY.Title = "Value";
            //bargraphIndustry.DataBind();
            //bargraphIndustry.Series["Customer Potential"].SmartLabelStyle.Enabled = false;
            //bargraphIndustry.Series["Project Potential"].SmartLabelStyle.Enabled = false;
            //bargraphIndustry.Series["Business Expected"].SmartLabelStyle.Enabled = false;
            //bargraphIndustry.Series["Monthly Business Expected"].SmartLabelStyle.Enabled = false;
         

        }

        protected void cnsldtindustrybargraph()
        {
            //bargraphconsldtd.DataSource = dt_cnsldt_industry_val;
            //bargraphconsldtd.Series["Customer Potential"].YValueMembers = "Ovrl_ptnl";
            //bargraphconsldtd.Series["Project Potential"].YValueMembers = "Potential";
            //bargraphconsldtd.Series["Business Expected"].YValueMembers = "Bsns_Exptd";
            //bargraphconsldtd.Series["Monthly Business Expected"].YValueMembers = "ytd";
            //bargraphconsldtd.ChartAreas[0].AxisX.Interval = 110;
            //bargraphconsldtd.ChartAreas[0].AxisX.TitleFont = new System.Drawing.Font("Helvetica Neue", 10, System.Drawing.FontStyle.Bold);
            //bargraphconsldtd.ChartAreas[0].AxisY.Title = "Value";
            //bargraphconsldtd.DataBind();
            //bargraphconsldtd.Series["Customer Potential"].SmartLabelStyle.Enabled = false;
            //bargraphconsldtd.Series["Project Potential"].SmartLabelStyle.Enabled = false;
            //bargraphconsldtd.Series["Business Expected"].SmartLabelStyle.Enabled = false;
            //bargraphconsldtd.Series["Monthly Business Expected"].SmartLabelStyle.Enabled = false;
            Session["ConsolidatedIndustryValue"] = dt_cnsldt_industry_val;

        }


        /// <summary>
        /// Author : Anamika Kumari
        /// Date : Dec 6,2016
        /// Desc : Loading the bar chart for project type with the data of table dt_projecttype_potential
        /// </summary>
        protected void LoadProjectTypePotential()
        {

            //bargraphProjectType.DataSource = dt_projecttype_potential;
            //bargraphProjectType.Series["Potential For Closed Projects"].XValueMember = "PType";
            //bargraphProjectType.Series["Potential For Closed Projects"].YValueMembers = "Project_Potential_Closed";
            //bargraphProjectType.Series["Potential For All Projects"].YValueMembers = "Project_Potential_All";
            //bargraphProjectType.ChartAreas[0].AxisY.Title = "Value";
            //bargraphProjectType.DataBind();
            //bargraphProjectType.Series["Potential For Closed Projects"].SmartLabelStyle.Enabled = false;
            //bargraphProjectType.Series["Potential For All Projects"].SmartLabelStyle.Enabled = false;

        }
        #endregion


        [WebMethod]
        public static string BarGraphValues(string s)
        {
            String Json = null;
                DataTable dt = null;
                int tot = 0;
                int intval = 0;

                if (s != null)
                {
                    switch (s)
                    {
                        case "dt_cnsldt_industry_val":
                            dt = dt_cnsldt_industry_val;
                            dt = HttpContext.Current.Session["ConsolidatedIndustryValue"] as DataTable;
                            break;

                        case "dt_projecttype_potential":
                            dt_projecttype_potential = HttpContext.Current.Session["ProjectPotential"] as DataTable;
                            return JsonString(dt_projecttype_potential);

                        case "dt_inustry_bsns_val":
                            dt_inustry_bsns_val = HttpContext.Current.Session["IndustryView"] as DataTable;
                            return JsonString(dt_inustry_bsns_val);

                        case "dt_pndng_prjcts":
                            dt = HttpContext.Current.Session["griddtPndngPrjcts"] as DataTable;
                            return JsonString(dt);


                        case "dtOverduePrjcts":
                            dt = HttpContext.Current.Session["griddtOverduePrjcts"] as DataTable;
                            return JsonString(dt);

                        case "dt_trgtd_cmpltn_prjcts":
                            dt = HttpContext.Current.Session["griddt"] as DataTable;
                            return JsonString(dt);

                        default:
                            dt = HttpContext.Current.Session["ConsolidatedIndustryValue"] as DataTable;
                            break;


                    }

                    Random rnd = new Random();
                    StringBuilder sb = new StringBuilder();
                    if (s == "dt_cnsldt_industry_val")
                    {
                        if (dt != null)
                        {
                            DataRow dr = dt.NewRow();
                            DataTable dtt = GenerateTransposedTable(dt);
                            if (!dtt.Columns.Contains("Color"))
                            {
                                DataColumn dc = new DataColumn("Color");
                                DataColumn dc1 = new DataColumn("flag");
                                dtt.Columns.Add(dc);
                                dtt.Columns.Add(dc1);
                                for (int i = 0; i < dtt.Rows.Count; i++)
                                {
                                    DataRow dr1 = dtt.Rows[i];

                                    switch (i)
                                    {
                                        case 0:

                                            dr1["flag"] = "CustomerPOtential";
                                            dr1[dc] = "#84b761";
                                            break;
                                        case 1:

                                            dr1["flag"] = "ProjectPotential";
                                            dr1[dc] = "#fdd400 ";
                                            break;
                                        case 2:
                                            DataRow dr4 = dtt.Rows[i];
                                            dr1["flag"] = "Business Expected";
                                            dr1[dc] = "#cc4748";
                                            break;
                                        case 3:
                                            DataRow dr5 = dtt.Rows[i];
                                            dr1["flag"] = "MonthlyBusiness Expected";
                                            dr1[dc] = "#67b7dc";
                                            break;
                                    }

                                }
                            }
                            DataTable dtfinal = new DataTable();
                            dtfinal.Columns.Add("color");
                            dtfinal.Columns.Add("startValue");
                            dtfinal.Columns.Add("endValue");
                            dtfinal.Columns.Add("radius");
                            dtfinal.Columns.Add("innerRadius");
                            dtfinal.Columns.Add("balloonText");

                            tot = Convert.ToInt32(dtt.AsEnumerable().Sum(x => Convert.ToDecimal(x["value"])));
                            if (dtt != null)
                                if (dtt.Rows.Count > 0)
                                {
                                    for (int i = 0; i < dtt.Rows.Count; i++)
                                    {
                                        DataRow datar = dtt.Rows[i];
                                        int val = Convert.ToInt32(datar["value"]);
                                        if (tot > 0)
                                        {
                                            intval = Math.Abs(Convert.ToInt32(datar["value"]) * 200 / tot);
                                        }
                                        else
                                        {
                                            intval = Math.Abs(Convert.ToInt32(datar["value"]));
                                        }
                                        for (int j = 0; j < 2; j++)
                                        {
                                            dr = dtfinal.NewRow();
                                            dr["startValue"] = 0;
                                            if (j == 1)
                                            {
                                                dr["color"] = dtt.Rows[i]["color"];
                                                dr["endValue"] = intval;
                                            }
                                            else
                                            {
                                                dr["color"] = "#eee";
                                                dr["endValue"] = 100;
                                            }
                                            if (i == 0)
                                            {
                                                dr["radius"] = "100%";
                                                dr["innerRadius"] = "85%";
                                            }
                                            else if (i == 1)
                                            {
                                                dr["radius"] = "80%";
                                                dr["innerRadius"] = "65%";
                                            }
                                            else if (i == 2)
                                            {
                                                dr["radius"] = "60%";
                                                dr["innerRadius"] = "45%";
                                            }
                                            else
                                            {
                                                dr["radius"] = "40%";
                                                dr["innerRadius"] = "25%";
                                            }
                                            if (j == 1)
                                            {
                                                dr["balloonText"] = dtt.Rows[i]["value"];
                                            }
                                            dtfinal.Rows.Add(dr);
                                        }

                                    }
                                    Json = JsonString(dtfinal);
                                }
                        }

                        return Json;
                    }
                    if (s == "dt_projecttype_potential" || s == "dt_inustry_bsns_val" || s == "dt_pndng_prjcts" || s == "dtOverduePrjcts" || s == "dt_trgtd_cmpltn_prjcts")
                    {


                    }


                    return Json;
                }
            

                return Json;
               
            
        }
        public static string JsonString(DataTable dt)
        {
            StringBuilder sb = new StringBuilder();
            String Json = null;
          
            if (dt != null)
            {
                sb.Append("[");
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    sb.Append("{");
                    for (int j = 0; j < dt.Columns.Count; j++)

                    {
                        string ColumnName = dt.Columns[j].ColumnName;


                        if (dt.Rows[i][j] is decimal || dt.Rows[i][j] is Single || dt.Rows[i][j] is double || ColumnName == "Value2")
                        {
                            decimal d = Convert.ToDecimal(dt.Rows[i][j]);
                            if (j == dt.Columns.Count - 1)
                            {
                                sb.Append("\"" + ColumnName + "\":" + d + "");
                                //     sb.Append("}");

                                break;
                            }
                            else
                            {

                                sb.Append("\"" + ColumnName + "\":" + d + ",");

                            }
                        }
                        else
                        {
                            string ColumnValue = dt.Rows[i][j].ToString();

                            if (ColumnName == "Color")
                            {
                               ColumnName=ColumnName.ToLower();
                                string stagevalue = Convert.ToString(dt.Rows[i][j + 1]);
                                switch (stagevalue)
                                {
                                    case "Stage 1":
                                        ColumnValue = "#399BFF";
                                        break;
                                    case "Stage 2":
                                        ColumnValue = "#26A65B";
                                        break;
                                    case "Stage 3":
                                        ColumnValue = "#EF4836";
                                        break;
                                    case "Stage 4":
                                        ColumnValue = "#F39C12";
                                        break;

                                }
                            }


                            if (j == dt.Columns.Count - 1)
                            {

                                sb.Append("\"" + ColumnName + "\":\"" + ColumnValue + "\"");
                                //sb.Append("}");
                                break;
                            }
                            else
                            {

                                sb.Append("\"" + ColumnName + "\":\"" + ColumnValue + "\",");

                            }

                        }

                        //  sb.Append("},");

                    }
                    if (i == dt.Rows.Count - 1)
                    {
                        sb.Append("}");
                    }
                    else
                    {
                        sb.Append("},");
                    }
                }
                sb.Append("]");

                Json = sb.ToString();
                return Json;
            }
            return Json;
        }
        private static DataTable GenerateTransposedTable(DataTable inputTable)
        {
            try
            {
                DataTable outputTable = new DataTable();
                outputTable.Columns.Add(new DataColumn("falg"));
                outputTable.Columns.Add(new DataColumn("value",typeof(decimal)));

                for (int rCount = 0; rCount <= inputTable.Columns.Count - 1; rCount++)
                {
                    DataRow newRow = outputTable.NewRow();

                    // First column is inputTable's Header row's second column
                    newRow[0] = inputTable.Columns[rCount].ColumnName.ToString();
                    for (int cCount = 0; cCount <= inputTable.Rows.Count - 1; cCount++)
                    {
                        string colValue = inputTable.Rows[cCount][rCount].ToString();
                        newRow[cCount + 1] = colValue;
                    }
                    outputTable.Rows.Add(newRow);
                }

                return outputTable;
            }

            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
                return null;
              
            }
        }

    }
}