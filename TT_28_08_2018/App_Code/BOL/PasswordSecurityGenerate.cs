﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Web;

namespace TaegutecSalesBudget
{
    public class PasswordSecurityGenerate
    {
        // Define default min and max password lengths.
        private static int DEFAULT_MIN_PASSWORD_LENGTH = 8;
        private static int DEFAULT_MAX_PASSWORD_LENGTH = 10;

        // Define supported password characters divided into groups.
        // You can add (or remove) characters to (from) these groups.
        private static string PASSWORD_CHARS_LCASE = "abcdefgijmnqrt";
        private static string PASSWORD_CHARS_UCASE = "HJKLMNPQRSTWXYZ";
        private static string PASSWORD_CHARS_NUMERIC = "3456789";
        private static string PASSWORD_CHARS_SPECIAL = "@#$_";//   *$-?_&!%{}/#^";


        public static string Generate()
        {
            return Generate(DEFAULT_MIN_PASSWORD_LENGTH, DEFAULT_MAX_PASSWORD_LENGTH);
        }


        public static string Generate(int length)
        {
            return Generate(length, length);
        }
        public static string Generate(int minLength, int maxLength)
        {

            if (minLength <= 0 || maxLength <= 0 || minLength > maxLength)
                return null;
            char[][] charGroups = new char[][]
        {
            PASSWORD_CHARS_LCASE.ToCharArray(),
            PASSWORD_CHARS_UCASE.ToCharArray(),
            PASSWORD_CHARS_NUMERIC.ToCharArray(),
            PASSWORD_CHARS_SPECIAL.ToCharArray()        };


            int[] charsLeftInGroup = new int[charGroups.Length];


            for (int i = 0; i < charsLeftInGroup.Length; i++)
                charsLeftInGroup[i] = charGroups[i].Length;

            int[] leftGroupsOrder = new int[charGroups.Length];


            for (int i = 0; i < leftGroupsOrder.Length; i++)
                leftGroupsOrder[i] = i;


            byte[] randomBytes = new byte[4];


            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            rng.GetBytes(randomBytes);


            int seed = (randomBytes[0] & 0x7f) << 24 |
            randomBytes[1] << 16 |
            randomBytes[2] << 8 |
            randomBytes[3];

            Random random = new Random(seed);

            char[] password = null;

            if (minLength < maxLength)
                password = new char[random.Next(minLength, maxLength + 1)];
            else
                password = new char[minLength];


            int nextCharIdx;
            int nextGroupIdx;
            int nextLeftGroupsOrderIdx;
            int lastCharIdx;
            int lastLeftGroupsOrderIdx = leftGroupsOrder.Length - 1;


            for (int i = 0; i < password.Length; i++)
            {

                if (lastLeftGroupsOrderIdx == 0)
                    nextLeftGroupsOrderIdx = 0;
                else
                    nextLeftGroupsOrderIdx = random.Next(0, lastLeftGroupsOrderIdx);


                nextGroupIdx = leftGroupsOrder[nextLeftGroupsOrderIdx];


                lastCharIdx = charsLeftInGroup[nextGroupIdx] - 1;


                if (lastCharIdx == 0)
                    nextCharIdx = 0;
                else
                    nextCharIdx = random.Next(0, lastCharIdx + 1);


                password[i] = charGroups[nextGroupIdx][nextCharIdx];


                if (lastCharIdx == 0)
                    charsLeftInGroup[nextGroupIdx] =
                    charGroups[nextGroupIdx].Length;

                else
                {

                    if (lastCharIdx != nextCharIdx)
                    {
                        char temp = charGroups[nextGroupIdx][lastCharIdx];
                        charGroups[nextGroupIdx][lastCharIdx] =
                        charGroups[nextGroupIdx][nextCharIdx];
                        charGroups[nextGroupIdx][nextCharIdx] = temp;
                    }

                    charsLeftInGroup[nextGroupIdx]--;
                }


                if (lastLeftGroupsOrderIdx == 0)
                    lastLeftGroupsOrderIdx = leftGroupsOrder.Length - 1;

                else
                {

                    if (lastLeftGroupsOrderIdx != nextLeftGroupsOrderIdx)
                    {
                        int temp = leftGroupsOrder[lastLeftGroupsOrderIdx];
                        leftGroupsOrder[lastLeftGroupsOrderIdx] =
                        leftGroupsOrder[nextLeftGroupsOrderIdx];
                        leftGroupsOrder[nextLeftGroupsOrderIdx] = temp;
                    }

                    lastLeftGroupsOrderIdx--;
                }
            }


            return new string(password);
        }

        //SecurityCode Generate
        public static string CreateSecurityCode(int codeLength)
        {
            string _allowedChars = "1234567890";
            Random randNum = new Random();
            char[] Code = new char[codeLength];
            int allowedCharCount = _allowedChars.Length;
            for (int i = 0; i < codeLength; i++)
            {
                Code[i] = _allowedChars[(int)((_allowedChars.Length) * randNum.NextDouble())];
            }
            return new string(Code);
        }

        //SecurityCode Generate



        /// <summary>
 
        /// Author :K.lakshmiBindu
        /// Desc: For Fetching LoginDevice Details
        /// Date: Jan 23, 2019
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        public DataTable GetLoginDeviceDeatials(LoginAuthentication authObj)
        {
            DataTable dtLoginDeviceDetails = new DataTable(); ;
            SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("sp_getLoginDevicedetails", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@emailid", SqlDbType.VarChar, 500000, null).Value = authObj.LoginMailID;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                command.CommandTimeout = 100000;
                sqlDa.Fill(dtLoginDeviceDetails);
                return dtLoginDeviceDetails;
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
                return null;
            }


        }

        /// <summary>
      
        /// Author By:K.lakshmiBindu
        ///  Desc: For Updating defaultdevice1
        ///  Date: Jan 23, 2019
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        public bool UpdateDefaultdevice1(LoginAuthentication authObj, string ipAddress)
        {
            SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("sp_updateLoginDevicedetails", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@email", SqlDbType.VarChar, 500000, null).Value = authObj.LoginMailID;
                command.Parameters.Add("@defaultdevice1", SqlDbType.VarChar, 500, null).Value = ipAddress;
                command.Parameters.Add("@flag", SqlDbType.VarChar, 500, null).Value = "UpdateDefaultDevice1";
                int count =command.ExecuteNonQuery();
                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
                return false;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

          
        }

        /// <summary>
        
        /// Author :K.lakshmiBindu
        ///  Desc:For Updating defaultdevice2
        /// Modified Date: Jan 23, 2019
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>


        public bool UpdateDefaultdevice2(LoginAuthentication authObj, string defaultdevice2, string defaultdevice1)
        {
            SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("sp_updateLoginDevicedetails", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@email", SqlDbType.VarChar, 500000, null).Value = authObj.LoginMailID;
                command.Parameters.Add("@defaultdevice1", SqlDbType.VarChar, 500, null).Value = defaultdevice1;
                command.Parameters.Add("@defaultdevice2", SqlDbType.VarChar, 500, null).Value = defaultdevice2;

                command.Parameters.Add("@flag", SqlDbType.VarChar, 500, null).Value = "UpdateDefaultDevice2";
                int count = command.ExecuteNonQuery();
                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
                return false;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

        }


        /// <summary>
        /// Author:
        /// Desc:
        /// Author :K.lakshmiBindu
        ///  Desc:For Updating Requested device
        /// Date: Jan 23, 2019
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public bool RequestForNewDevice(LoginAuthentication authObj, string RequestedDevice, string defaultdevice1)
        {
            SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("sp_updateLoginDevicedetails", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@email", SqlDbType.VarChar, 500000, null).Value = authObj.LoginMailID;
                command.Parameters.Add("@defaultdevice1", SqlDbType.VarChar, 500, null).Value = defaultdevice1;

                command.Parameters.Add("@requesteddevice", SqlDbType.VarChar, 50, null).Value = RequestedDevice;

                command.Parameters.Add("@flag", SqlDbType.VarChar, 500, null).Value = "RequestedDevice";
                int count = command.ExecuteNonQuery();
                if (count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
                return false;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
        }

        /// <summary>
     
        /// Author :K.lakshmiBindu
        ///  Desc:if the login request is coming for first time we are inserting record  
        ///  Date: Jan 23, 2019
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public bool InsertRecordForFirstTime(LoginAuthentication authObj, string ipAddress)
        {
            SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("sp_updateLoginDevicedetails", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@email", SqlDbType.VarChar, 500000, null).Value = authObj.LoginMailID;
                command.Parameters.Add("@defaultdevice1", SqlDbType.VarChar, 500, null).Value = ipAddress;
                command.Parameters.Add("@flag", SqlDbType.VarChar, 500, null).Value = "intial";
                SqlParameter spstatus = command.Parameters.Add("@errornum", SqlDbType.Int);
                spstatus.Direction = ParameterDirection.Output;
             
                int count = command.ExecuteNonQuery();
               if (count > 0)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
              
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
                return false;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            
        }


        /// <summary>
      
        /// Author :K.lakshmiBindu
        ///  Desc:For checking the approval status of requested device
        ///  Date: Jan 23, 2019
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public bool CheckForApprovalofRequestedDevice(LoginAuthentication authObj, string RequestedDeviceIP)
        {
            DataTable dtLoginDeviceDetails = new DataTable(); ;
            SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("Sp_getApprovalstatusforRequestedDevice", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@emailid", SqlDbType.VarChar,40, null).Value = authObj.LoginMailID;
                command.Parameters.Add("@requesteddevice", SqlDbType.VarChar, 40, null).Value = RequestedDeviceIP;
                SqlParameter spstatus = command.Parameters.Add("@status", SqlDbType.Int);
             
                spstatus.Direction = ParameterDirection.Output;

             
                command.CommandTimeout = 100000;
                //SqlDataAdapter da = new SqlDataAdapter(command);
                command.ExecuteNonQuery();
              //  da.Fill(dtLoginDeviceDetails);

                if (Convert.ToInt32(spstatus.Value) == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }

              
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
                return false;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }


        }



        public string  ChangeMailId(LoginAuthentication authObj)
        {
            string Loginmailid = null;
            DataTable dtLoginDeviceDetails = new DataTable(); ;
            SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("sp_ChangeMailId", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@loginmail", SqlDbType.VarChar, 40, null).Value = authObj.LoginMailID;
                SqlParameter retloginmail = command.Parameters.Add("@retloginmail ", SqlDbType.VarChar,50);

                retloginmail.Direction = ParameterDirection.Output;


                command.CommandTimeout = 100000;
                //SqlDataAdapter da = new SqlDataAdapter(command);
                command.ExecuteScalar();
                //  da.Fill(dtLoginDeviceDetails);
                Loginmailid=Convert.ToString(retloginmail.Value);
                 return Loginmailid;
     

            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
                return null;
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

          
        }


        /// <summary>
        /// Author : K.LakshmiBindu
        /// Desc   : For logging in the user logins
        /// Date   : Feb 11,2019
        /// </summary>
        /// <param name="authObj"></param>
        /// <param name="DeviceIP"></param>
        /// <returns></returns>
        public int LogUsers(LoginAuthentication authObj, string DeviceIP)
        {
            int i = 0;
              SqlConnection connection = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
              try
              {
                  connection.Open();
                  SqlCommand command = new SqlCommand("sp_saveLoginUserLogs", connection);
                  command.CommandType = CommandType.StoredProcedure;
                  command.Parameters.Add("@EngineerId", SqlDbType.VarChar, 40, null).Value = authObj.EngineerId;
                  command.Parameters.Add("@EngineerName", SqlDbType.VarChar, 40, null).Value = authObj.UserName;
                  command.Parameters.Add("@DeviceIP", SqlDbType.VarChar, 40, null).Value = DeviceIP;
                 i=command.ExecuteNonQuery();
              }
              catch (Exception ex)
              {
                  CommonFunctions.LogErrorStatic(ex);
                  return i;
              }
              finally
              {
                  if (connection.State == ConnectionState.Open)
                  {
                      connection.Close();
                  }
              }

            return i;
        }
    }
}