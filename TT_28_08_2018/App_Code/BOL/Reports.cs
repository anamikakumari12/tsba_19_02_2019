﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.IO.Compression;
using System.Globalization;

namespace TaegutecSalesBudget
{
    public class Reports
    {
        public static DataTable dtfamily;
        AdminConfiguration objConfig = new AdminConfiguration();

        public DataTable getSpecialGroupValueSum(string BranchCode, string Branch_Manager_Id, string salesengineer_id = null, string CustomerNumber = null, string flag = null, string familyname = null, string custtype = null, string cter = null)
        {

            DataTable dsgoldprod = new DataTable();

            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());

            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getValueSum", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@BranchCode", SqlDbType.VarChar, 50).Value = BranchCode;
                command.Parameters.Add("@Branch_Manager_Id", SqlDbType.VarChar, 50).Value = Branch_Manager_Id;
                command.Parameters.Add("@salesengineer_id", SqlDbType.VarChar, 50).Value = salesengineer_id;
                command.Parameters.Add("@flag", SqlDbType.VarChar, 50).Value = flag;
                command.Parameters.Add("@item_family_name", SqlDbType.VarChar, 50).Value = familyname;
                command.Parameters.Add("@customer_type", SqlDbType.VarChar, 50).Value = custtype;
                command.Parameters.Add("@customer_number", SqlDbType.VarChar, 50).Value = CustomerNumber;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 50).Value = cter;

                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dsgoldprod);

                return dsgoldprod;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dsgoldprod;
        }

        public DataTable getValueSum(string BranchCode, string Branch_Manager_Id, string salesengineer_id = null, string CustomerNumber = null, string flag = null, string familyname = null, string custtype = null, string cter = null)
        {
            DataTable dstotals = new DataTable();

            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getValueSum", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@BranchCode", SqlDbType.VarChar, 50).Value = BranchCode;
                command.Parameters.Add("@Branch_Manager_Id", SqlDbType.VarChar, 50).Value = Branch_Manager_Id;
                command.Parameters.Add("@salesengineer_id", SqlDbType.VarChar, 50).Value = salesengineer_id;
                command.Parameters.Add("@flag", SqlDbType.VarChar, 50).Value = flag;
                command.Parameters.Add("@item_family_name", SqlDbType.VarChar, 50).Value = familyname;
                command.Parameters.Add("@customer_type", SqlDbType.VarChar, 50).Value = custtype;
                command.Parameters.Add("@customer_number", SqlDbType.VarChar, 50).Value = CustomerNumber;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 50).Value = cter;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dstotals);

                return dstotals;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dstotals;
        }

        ////salesbyline


        //salesbyline

        public DataTable getValueSum_SF(string flag, string BranchCode = null, string salesengineer_id = null, string customer_type = null, string customer_number = null, string cter = null)
        {
            DataTable dssalesbyline = new DataTable();

            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getValueSum_SF", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@BranchCode", SqlDbType.VarChar, 10).Value = BranchCode;
                command.Parameters.Add("@salesengineer_id", SqlDbType.VarChar, 10).Value = salesengineer_id;
                command.Parameters.Add("@customer_type", SqlDbType.VarChar, 10).Value = customer_type;
                command.Parameters.Add("@customer_number", SqlDbType.VarChar, 10).Value = customer_number;
                command.Parameters.Add("@flag", SqlDbType.VarChar, 10).Value = flag;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 50).Value = cter;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dssalesbyline);

                return dssalesbyline;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dssalesbyline;
        }
        public DataTable salesbyfamily(string BranchCode = null, string salesengineer_id = null, string customer_type = null, string customer_number = null, string cter = null)
        {
            dtfamily = getValueSum_SF("F", BranchCode, salesengineer_id, customer_type, customer_number, cter);
            return dtfamily;
        }
        public DataSet getSalesByLineSum(string flag, float byValueIn, string BranchCode = null, string salesengineer_id = null, string customer_type = null, string customer_number = null, string cter = null)
        {
            DataSet dsTables = new DataSet();

            DataTable dtsales = new DataTable();
            dtsales.Columns.Add("FixedRow", typeof(string));
            dtsales.Columns.Add("sales_value_year_1", typeof(string));
            dtsales.Columns.Add("sales_value_year_0", typeof(string));
            dtsales.Columns.Add("estimate_value_next_year", typeof(string));
            dtsales.Columns.Add("p_sales_value_year_1", typeof(string));
            dtsales.Columns.Add("ytd", typeof(string));
            dtsales.Columns.Add("ach", typeof(string));
            dtsales.Columns.Add("arate", typeof(string));

            DataTable dtsalesReports = new DataTable();
            dtsalesReports.Columns.Add("FixedRow", typeof(string));
            dtsalesReports.Columns.Add("sales_value_year_1", typeof(string));
            dtsalesReports.Columns.Add("sales_value_year_0", typeof(string));
            dtsalesReports.Columns.Add("estimate_value_next_year", typeof(string));
            dtsalesReports.Columns.Add("p_sales_value_year_1", typeof(string));
            dtsalesReports.Columns.Add("ytd", typeof(string));
            dtsalesReports.Columns.Add("ach", typeof(string));
            dtsalesReports.Columns.Add("arate", typeof(string));

            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };
            DataTable dtSalesbyLine = new DataTable();
            int actual_mnth = 12 - (objConfig.getActualMonth());
            if (flag == "F")
            {
                dtSalesbyLine = dtfamily;
            }
            else
            {
                dtSalesbyLine = getValueSum_SF(flag, BranchCode, salesengineer_id, customer_type, customer_number, cter);
            }
            for (int i = 0; i < dtSalesbyLine.Rows.Count; i++)
            {
                string subfmname = dtSalesbyLine.Rows[i].ItemArray[1].ToString();
                float top0 = dtSalesbyLine.Rows[i].ItemArray[2].ToString() == "" || dtSalesbyLine.Rows[i].ItemArray[2].ToString() == null ? 0    //2013
                  : Convert.ToInt64(dtSalesbyLine.Rows[i].ItemArray[2].ToString());


                float top1 = dtSalesbyLine.Rows[i].ItemArray[3].ToString() == "" || dtSalesbyLine.Rows[i].ItemArray[3].ToString() == null ? 0    //2014
                    : Convert.ToInt64(dtSalesbyLine.Rows[i].ItemArray[3].ToString());


                float top2 = dtSalesbyLine.Rows[i].ItemArray[4].ToString() == "" || dtSalesbyLine.Rows[i].ItemArray[4].ToString() == null ? 0   //2015 budget
                   : Convert.ToInt64(dtSalesbyLine.Rows[i].ItemArray[4].ToString());



                float top3 = top1 == 0 ? 0 : (top2 / top1 - 1) * 100; //15/14

                float top4 = dtSalesbyLine.Rows[i].ItemArray[5].ToString() == "" || dtSalesbyLine.Rows[i].ItemArray[5].ToString() == null ? 0   //ytd
                    : Convert.ToInt64(dtSalesbyLine.Rows[i].ItemArray[5].ToString());



                //ytd/2015
                // decimal top5 = top2 == 0 ? 0 : (top4 / top2 - 1) * 100;
                float top5 = top2 == 0 ? 0 : (top4 / top2) * 100;
                float top6 = actual_mnth == 0 ? 0 : (top2 - top4) / actual_mnth;


                top0 = top0 == 0 ? 0 : (top0 / byValueIn);
                top1 = top1 == 0 ? 0 : (top1 / byValueIn);
                top2 = top2 == 0 ? 0 : (top2 / byValueIn);
                top4 = top4 == 0 ? 0 : (top4 / byValueIn);
                top6 = top6 == 0 ? 0 : (top6 / byValueIn);
                bool ckforZeros = DeleteEmptyRows(top0, top1, top2, top3, top4, top5, top6);
                if (ckforZeros == false)
                {
                    dtsales.Rows.Add(subfmname, (Math.Round(top0, 0)).ToString("N0", culture), (Math.Round(top1, 0)).ToString("N0", culture), (Math.Round(top2, 0)).ToString("N0", culture), Convert.ToString(Math.Round(top3, 0)) + '%', (Math.Round(top4, 0)).ToString("N0", culture), Convert.ToString(Math.Round(top5, 0)) + '%', (Math.Round(top6, 0)).ToString("N0", culture));
                    dtsalesReports.Rows.Add(subfmname, (Math.Round(top0, 0)).ToString(), (Math.Round(top1, 0)).ToString(), (Math.Round(top2, 0)).ToString(), Convert.ToString(Math.Round(top3, 0)) + '%', (Math.Round(top4, 0)).ToString(), Convert.ToString(Math.Round(top5, 0)) + '%', (Math.Round(top6, 0)).ToString());
                }
            }
            dsTables.Tables.Add(dtsales);
            dsTables.Tables.Add(dtsalesReports);

            return dsTables;
        }



        public DataTable getSalesbyApplication(string CustomerNumber, string salesengineer, string custtype, string branchcode, string bmid, string cter = null)
        {
            DataTable dssalesbyapp = new DataTable();

            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getsalesbyapp", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@customer_number", SqlDbType.VarChar, 50).Value = CustomerNumber;
                command.Parameters.Add("@salesengineer_id", SqlDbType.VarChar, 50).Value = salesengineer;
                command.Parameters.Add("@Branch_Manager_Id", SqlDbType.VarChar, 50).Value = bmid;
                command.Parameters.Add("@BranchCode", SqlDbType.VarChar, 50).Value = branchcode;
                command.Parameters.Add("@customer_type", SqlDbType.VarChar, 50).Value = custtype;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 50).Value = cter;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dssalesbyapp);
                command.CommandTimeout = 100000;
                return dssalesbyapp;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dssalesbyapp;
        }
        public DataSet getsalesbyapp(string CustomerNumber, string salesengineer, string custtype, string branchcode, string bmid, float byValueIn, string cter = null)
        {
            DataSet dsTables = new DataSet();
            DataTable dtapp = new DataTable();
            dtapp.Columns.Add("app_Desc", typeof(string));
            dtapp.Columns.Add("app_Code", typeof(string));
            dtapp.Columns.Add("sales_value_year_1", typeof(string));
            dtapp.Columns.Add("sales_value_year_0", typeof(string));
            dtapp.Columns.Add("estimate_value_next_year", typeof(string));
            dtapp.Columns.Add("change", typeof(string));
            dtapp.Columns.Add("ytd", typeof(string));
            dtapp.Columns.Add("acvmnt", typeof(string));
            dtapp.Columns.Add("askrate", typeof(string));

            DataTable dtappReport = new DataTable();
            dtappReport.Columns.Add("app_Desc", typeof(string));
            dtappReport.Columns.Add("app_Code", typeof(string));
            dtappReport.Columns.Add("sales_value_year_1", typeof(string));
            dtappReport.Columns.Add("sales_value_year_0", typeof(string));
            dtappReport.Columns.Add("estimate_value_next_year", typeof(string));
            dtappReport.Columns.Add("change", typeof(string));
            dtappReport.Columns.Add("ytd", typeof(string));
            dtappReport.Columns.Add("acvmnt", typeof(string));
            dtappReport.Columns.Add("askrate", typeof(string));

            int actual_mnth = 12 - (objConfig.getActualMonth());
            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };
            DataTable dtsalesbyapp = getSalesbyApplication(CustomerNumber, salesengineer, custtype, branchcode, bmid, cter);
            for (int i = 0; i < dtsalesbyapp.Rows.Count; i++)
            {
                float pre_actlyear = dtsalesbyapp.Rows[i].ItemArray[2].ToString() == "" || dtsalesbyapp.Rows[i].ItemArray[2].ToString() == null ? 0    //13
                            : Convert.ToInt64(dtsalesbyapp.Rows[i].ItemArray[2].ToString());
                float actlyear = dtsalesbyapp.Rows[i].ItemArray[3].ToString() == "" || dtsalesbyapp.Rows[i].ItemArray[3].ToString() == null ? 0      //14
                            : Convert.ToInt64(dtsalesbyapp.Rows[i].ItemArray[3].ToString());
                float actlyear_next = dtsalesbyapp.Rows[i].ItemArray[4].ToString() == "" || dtsalesbyapp.Rows[i].ItemArray[4].ToString() == null ? 0     //15
                           : Convert.ToInt64(dtsalesbyapp.Rows[i].ItemArray[4].ToString());
                float ytd = dtsalesbyapp.Rows[i].ItemArray[5].ToString() == "" || dtsalesbyapp.Rows[i].ItemArray[5].ToString() == null ? 0   //ytd
                               : Convert.ToInt64(dtsalesbyapp.Rows[i].ItemArray[5].ToString());

                pre_actlyear = pre_actlyear == 0 ? 0 : (pre_actlyear / byValueIn);
                actlyear = actlyear == 0 ? 0 : (actlyear / byValueIn);
                actlyear_next = actlyear_next == 0 ? 0 : (actlyear_next / byValueIn);
                string app_desc = dtsalesbyapp.Rows[i].ItemArray[0].ToString();
                string app_code = dtsalesbyapp.Rows[i].ItemArray[1].ToString();

                //15/14

                ytd = ytd == 0 ? 0 : (ytd / byValueIn);
                //ytd/2015B
                float acvmnt = actlyear_next == 0 ? 0 : (ytd / actlyear_next) * 100;
                float change = actlyear == 0 ? 0 : (actlyear_next / actlyear - 1) * 100;
                float askrate = actual_mnth == 0 ? 0 : (actlyear_next - ytd) / actual_mnth;
                bool chkforzeroes = DeleteEmptyRows(pre_actlyear, actlyear, actlyear_next, change, ytd, acvmnt, askrate);
                if (chkforzeroes == false)
                {
                    dtapp.Rows.Add(app_desc, app_code, (Math.Round(pre_actlyear, 0).ToString("N0", culture)), (Math.Round(actlyear, 0)).ToString("N0", culture), (Math.Round(actlyear_next, 0)).ToString("N0", culture), Convert.ToString(Math.Round(change, 0)) + "%", (Math.Round(ytd, 0)).ToString("N0", culture), Convert.ToString(Math.Round(acvmnt, 0)) + "%", (Math.Round(askrate, 0)).ToString("N0", culture));
                    dtappReport.Rows.Add(app_desc, app_code, (Math.Round(pre_actlyear, 0).ToString()), (Math.Round(actlyear, 0)).ToString(), (Math.Round(actlyear_next, 0)).ToString(), Convert.ToString(Math.Round(change, 0)) + "%", (Math.Round(ytd, 0)).ToString(), Convert.ToString(Math.Round(acvmnt, 0)) + "%", (Math.Round(askrate, 0)).ToString());
                }


            }
            dsTables.Tables.Add(dtapp);
            dsTables.Tables.Add(dtappReport);
            return dsTables;
        }
        #region Anantha
        //Added by Anantha   7-8-2016
        public DataTable getSalesbyApplicationQty(string CustomerNumber, string salesengineer, string custtype, string branchcode, string bmid, string cter = null)
        {
            DataTable dssalesbyapp = new DataTable();

            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getsalesbyappQty", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@customer_number", SqlDbType.VarChar, 50).Value = CustomerNumber;
                command.Parameters.Add("@salesengineer_id", SqlDbType.VarChar, 50).Value = salesengineer;
                command.Parameters.Add("@Branch_Manager_Id", SqlDbType.VarChar, 50).Value = bmid;
                command.Parameters.Add("@BranchCode", SqlDbType.VarChar, 50).Value = branchcode;
                command.Parameters.Add("@customer_type", SqlDbType.VarChar, 50).Value = custtype;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 50).Value = cter;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dssalesbyapp);

                return dssalesbyapp;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dssalesbyapp;
        }

        public DataSet getsalesbyappQty(string CustomerNumber, string salesengineer, string custtype, string branchcode, string bmid, float byValueIn, string cter = null)
        {
            DataSet dsTables = new DataSet();
            DataTable dtapp = new DataTable();

            dtapp.Columns.Add("app_Desc", typeof(string));
            dtapp.Columns.Add("app_Code", typeof(string));
            dtapp.Columns.Add("sales_value_year_1", typeof(string));
            dtapp.Columns.Add("sales_value_year_0", typeof(string));
            dtapp.Columns.Add("estimate_value_next_year", typeof(string));

            dtapp.Columns.Add("change", typeof(string));
            dtapp.Columns.Add("ytd", typeof(string));
            dtapp.Columns.Add("acvmnt", typeof(string));
            dtapp.Columns.Add("askrate", typeof(string));


            DataTable dtappReport = new DataTable();

            dtappReport.Columns.Add("app_Desc", typeof(string));
            dtappReport.Columns.Add("app_Code", typeof(string));
            dtappReport.Columns.Add("sales_value_year_1", typeof(string));
            dtappReport.Columns.Add("sales_value_year_0", typeof(string));
            dtappReport.Columns.Add("estimate_value_next_year", typeof(string));
            dtappReport.Columns.Add("change", typeof(string));
            dtappReport.Columns.Add("ytd", typeof(string));
            dtappReport.Columns.Add("acvmnt", typeof(string));
            dtappReport.Columns.Add("askrate", typeof(string));


            int actual_mnth = 12 - (objConfig.getActualMonth());
            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };
            DataTable dtsalesbyapp = getSalesbyApplicationQty(CustomerNumber, salesengineer, custtype, branchcode, bmid, cter);
            for (int i = 0; i < dtsalesbyapp.Rows.Count; i++)
            {
                float pre_actlyear = dtsalesbyapp.Rows[i].ItemArray[2].ToString() == "" || dtsalesbyapp.Rows[i].ItemArray[2].ToString() == null ? 0    //13
                            : Convert.ToInt64(dtsalesbyapp.Rows[i].ItemArray[2].ToString());
                float actlyear = dtsalesbyapp.Rows[i].ItemArray[3].ToString() == "" || dtsalesbyapp.Rows[i].ItemArray[3].ToString() == null ? 0      //14
                            : Convert.ToInt64(dtsalesbyapp.Rows[i].ItemArray[3].ToString());
                float actlyear_next = dtsalesbyapp.Rows[i].ItemArray[4].ToString() == "" || dtsalesbyapp.Rows[i].ItemArray[4].ToString() == null ? 0     //15
                           : Convert.ToInt64(dtsalesbyapp.Rows[i].ItemArray[4].ToString());
                float ytd = dtsalesbyapp.Rows[i].ItemArray[5].ToString() == "" || dtsalesbyapp.Rows[i].ItemArray[5].ToString() == null ? 0   //ytd
                               : Convert.ToInt64(dtsalesbyapp.Rows[i].ItemArray[5].ToString());

                pre_actlyear = pre_actlyear == 0 ? 0 : (pre_actlyear / byValueIn);
                actlyear = actlyear == 0 ? 0 : (actlyear / byValueIn);
                actlyear_next = actlyear_next == 0 ? 0 : (actlyear_next / byValueIn);
                string app_desc = dtsalesbyapp.Rows[i].ItemArray[0].ToString();
                string app_code = dtsalesbyapp.Rows[i].ItemArray[1].ToString();

                //15/14

                ytd = ytd == 0 ? 0 : (ytd / byValueIn);
                //ytd/2015B
                float acvmnt = actlyear_next == 0 ? 0 : (ytd / actlyear_next) * 100;
                float change = actlyear == 0 ? 0 : (actlyear_next / actlyear - 1) * 100;
                float askrate = actual_mnth == 0 ? 0 : (actlyear_next - ytd) / actual_mnth;
                bool chkforzeroes = DeleteEmptyRows(pre_actlyear, actlyear, actlyear_next, change, ytd, acvmnt, askrate);
                if (chkforzeroes == false)
                {
                    dtapp.Rows.Add(app_desc, app_code, (Math.Round(pre_actlyear, 0).ToString("N0", culture)), (Math.Round(actlyear, 0)).ToString("N0", culture), (Math.Round(actlyear_next, 0)).ToString("N0", culture), Convert.ToString(Math.Round(change, 0)) + "%", (Math.Round(ytd, 0)).ToString("N0", culture), Convert.ToString(Math.Round(acvmnt, 0)) + "%", (Math.Round(askrate, 0)).ToString("N0", culture));
                    dtappReport.Rows.Add(app_desc, app_code, (Math.Round(pre_actlyear, 0).ToString()), (Math.Round(actlyear, 0)).ToString(), (Math.Round(actlyear_next, 0)).ToString(), Convert.ToString(Math.Round(change, 0)) + "%", (Math.Round(ytd, 0)).ToString(), Convert.ToString(Math.Round(acvmnt, 0)) + "%", (Math.Round(askrate, 0)).ToString());
                }


            }
            dsTables.Tables.Add(dtapp);
            dsTables.Tables.Add(dtappReport);
            return dsTables;
        }

        // End 

        #endregion


        public DataTable getSalesbycustomers(string branch_Code, string se_id, string cust_type, string cust_num, string cter = null)
        {
            DataTable dssalesbycust = new DataTable();

            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getsalesbycustomer", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@salesengineer_id", SqlDbType.VarChar, 50).Value = se_id;
                //command.Parameters.Add("@Branch_Manager_Id", SqlDbType.VarChar, 50).Value = bmid;
                command.Parameters.Add("@BranchCode", SqlDbType.VarChar, 50).Value = branch_Code;
                command.Parameters.Add("@customer_type", SqlDbType.VarChar, 50).Value = cust_type;
                command.Parameters.Add("@customer_number", SqlDbType.VarChar, 50).Value = cust_num;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 50).Value = cter;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dssalesbycust);

                return dssalesbycust;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dssalesbycust;
        }
        //public DataTable LoadSubFamilyName()
        //{
        //    DataTable dtSubFam = new DataTable();
        //    SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
        //    try
        //    {
        //        connection.Open();

        //        SqlCommand command = new SqlCommand("getSubFamilyNames", connection);
        //        command.CommandType = CommandType.StoredProcedure;
        //        SqlDataAdapter sqlDa = new SqlDataAdapter(command);
        //        sqlDa.Fill(dtSubFam);
        //        return dtSubFam;
        //    }

        //    catch (Exception ex) { }
        //    finally
        //    {
        //        if (connection.State == ConnectionState.Open)
        //        {
        //            connection.Close();
        //        }
        //    }


        //    return dtSubFam;
        //}

        //salesbyfamily

        //salesengineerdetails
        public DataTable LoadSalesEngDetails(string BranchEngineerId)
        {
            DataTable dtSalesEngDetails = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getSalesEngineerDetails", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@engineer_id", SqlDbType.VarChar, 10).Value = BranchEngineerId;
                //command.Parameters.Add("@roleId", SqlDbType.VarChar, 50).Value = RoleId;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtSalesEngDetails);
                //return dtCustomerDetails;
            }

            catch (Exception ex)
            {

            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            // getSalesEngDetails
            return dtSalesEngDetails;

        }

        public DataSet LoadCustomerDetails(string SalesEngineerId, string RoleId)
        {
            DataSet dtCustomerDetails = new DataSet();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getCustomerDetails", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@assigned_salesengineer_id", SqlDbType.VarChar, 10).Value = SalesEngineerId;
                command.Parameters.Add("@roleId", SqlDbType.VarChar, 50).Value = RoleId;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                DataSet ds = new DataSet();
                sqlDa.Fill(dtCustomerDetails);

                //return dtCustomerDetails;
            }

            catch (Exception ex)
            {

            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            // getCustomerDetails
            return dtCustomerDetails;

        }


        //getlogininfo
        public DataTable LoadUserInfo(string branchCode = null, string branchselected = null, string tm_branchcode = null, string territory_engineer_id = null, string tm_branchselected = null)
        {
            DataTable dtUserInfo = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getBranchCode", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@branchCode", SqlDbType.VarChar, 10).Value = branchCode;
                command.Parameters.Add("@branchselected", SqlDbType.VarChar, 10).Value = branchselected;
                command.Parameters.Add("@tm_branchcode", SqlDbType.VarChar, 10).Value = tm_branchcode;
                command.Parameters.Add("@territory_engineer_id", SqlDbType.VarChar, 10).Value = territory_engineer_id;
                command.Parameters.Add("@tm_branchselected", SqlDbType.VarChar, 10).Value = tm_branchselected;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtUserInfo);
                return dtUserInfo;
            }

            catch (Exception ex) { }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }


            return dtUserInfo;
        }



        public DataTable LoadCustomerDetailstype(string SalesEngineerId, string RoleId, string custtype = null, string branchcode = null)
        {
            DataTable dtCustomerDetails = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("getCustomerDetailsontype", connection);
                command.CommandType = CommandType.StoredProcedure;
                if (SalesEngineerId == "ALL")
                {

                    // command.Parameters.Add("@assigned_salesengineer_id", SqlDbType.VarChar, 10).Value = SalesEngineerId;
                    command.Parameters.Add("@roleId", SqlDbType.VarChar, 50).Value = RoleId;
                    command.Parameters.Add("@custtype", SqlDbType.VarChar, 50).Value = custtype;
                    command.Parameters.Add("@branchcode", SqlDbType.VarChar, 50).Value = branchcode;
                }
                else if (SalesEngineerId != "ALL")
                {
                    command.Parameters.Add("@assigned_salesengineer_id", SqlDbType.VarChar, 10).Value = SalesEngineerId;
                    command.Parameters.Add("@roleId", SqlDbType.VarChar, 50).Value = RoleId;
                    command.Parameters.Add("@custtype", SqlDbType.VarChar, 50).Value = custtype;
                    command.Parameters.Add("@branchcode", SqlDbType.VarChar, 50).Value = branchcode;
                }
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtCustomerDetails);
                //return dtCustomerDetails;
            }

            catch (Exception ex)
            {

            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            // getCustomerDetails
            return dtCustomerDetails;

        }



        /* ------------------------------------SALES BY CUSTOMERS--START-----------------------------------------------------------------------*/

        public DataTable custtype_total(string id, string roleid, string cust_type)
        {
            int actual_mnth = 12 - (objConfig.getActualMonth());
            string totalof;
            DataTable dt = new DataTable();
            dt.Columns.Add("cust_name");
            dt.Columns.Add("sales_value_year_1");
            dt.Columns.Add("sales_value_year_0");
            dt.Columns.Add("estimate_value_next_year");
            dt.Columns.Add("change", typeof(string));
            dt.Columns.Add("ytd", typeof(string));
            dt.Columns.Add("acvmnt", typeof(string));
            dt.Columns.Add("askrate", typeof(string));
            DataTable dtCTotals;

            if (roleid == "SE")
            {
                dtCTotals = getValueSum(id, "SE", null, null, cust_type);

            }
            else if (roleid == "SE_T")
            {
                dtCTotals = getValueSum(id, "SE", null, null);
            }
            else if (roleid == "BM")
            {
                dtCTotals = getValueSum(id, "BM", null, null, cust_type);
            }
            else if (roleid == "BM_T")
            {
                dtCTotals = getValueSum(id, "BM", null, null);
            }
            else if (roleid == "HO")
            {
                dtCTotals = getValueSum(null, "ALLBR", null, null, cust_type);
            }
            else if (roleid == "HO_T")
            {
                dtCTotals = getValueSum(null, "ALLBR", null, null);
            }
            else
            {
                dtCTotals = getValueSum(id, "SE", null, null, cust_type);
            }
            for (int i = 0; i < dtCTotals.Rows.Count; i++)
            {
                float pre_actlyear = dtCTotals.Rows[i].ItemArray[0].ToString() == "" || dtCTotals.Rows[i].ItemArray[0].ToString() == null ? 0  //13
                            : Convert.ToInt64(dtCTotals.Rows[i].ItemArray[0].ToString());

                float actlyear = dtCTotals.Rows[i].ItemArray[1].ToString() == "" || dtCTotals.Rows[i].ItemArray[1].ToString() == null ? 0  //14
                            : Convert.ToInt64(dtCTotals.Rows[i].ItemArray[1].ToString());

                float actlyear_next = dtCTotals.Rows[i].ItemArray[2].ToString() == "" || dtCTotals.Rows[i].ItemArray[2].ToString() == null ? 0  //15
                           : Convert.ToInt64(dtCTotals.Rows[i].ItemArray[2].ToString());

                pre_actlyear = pre_actlyear == 0 ? 0 : (pre_actlyear / 1000);
                actlyear = actlyear == 0 ? 0 : (actlyear / 1000);
                actlyear_next = actlyear_next == 0 ? 0 : (actlyear_next / 1000);

                float change = actlyear == 0 ? 0 : (actlyear_next / actlyear - 1) * 100; //15/14
                float ytd = dtCTotals.Rows[i].ItemArray[3].ToString() == "" || dtCTotals.Rows[i].ItemArray[3].ToString() == null ? 0   //ytd
                               : Convert.ToInt64(dtCTotals.Rows[i].ItemArray[3].ToString());
                ytd = ytd == 0 ? 0 : (ytd / 1000);
                float acvmnt = actlyear_next == 0 ? 0 : (ytd / actlyear_next) * 100; //ytd/2015B

                float askrate = actual_mnth == 0 ? 0 : (actlyear_next - ytd) / actual_mnth;
                if (cust_type == "C")
                {
                    totalof = "CUSTOMER TOTAL";
                    //dt.Rows.Add(totalof, Convert.ToString(Math.Round(pre_actlyear, 0)), Convert.ToString(Math.Round(actlyear, 0)), Convert.ToString(Math.Round(actlyear_next)));
                    dt.Rows.Add(totalof, Convert.ToString(Math.Round(pre_actlyear, 0)), Convert.ToString(Math.Round(actlyear, 0)), Convert.ToString(Math.Round(actlyear_next)), Convert.ToString(Math.Round(change)) + "%", Convert.ToString(Math.Round(ytd)), Convert.ToString(Math.Round(acvmnt)) + "%", Convert.ToString(Math.Round(askrate)));

                }
                else if (cust_type == "D")
                {
                    totalof = "CHANNEL PARTNER TOTAL";
                    //dt.Rows.Add(totalof, Convert.ToString(Math.Round(pre_actlyear, 0)), Convert.ToString(Math.Round(actlyear, 0)), Convert.ToString(Math.Round(actlyear_next)));
                    dt.Rows.Add(totalof, Convert.ToString(Math.Round(pre_actlyear, 0)), Convert.ToString(Math.Round(actlyear, 0)), Convert.ToString(Math.Round(actlyear_next)), Convert.ToString(Math.Round(change)) + "%", Convert.ToString(Math.Round(ytd)), Convert.ToString(Math.Round(acvmnt)) + "%", Convert.ToString(Math.Round(askrate)));

                }
                else
                {

                    totalof = "GRAND TOTAL";
                    //dt.Rows.Add(totalof, Convert.ToString(Math.Round(pre_actlyear, 0)), Convert.ToString(Math.Round(actlyear, 0)), Convert.ToString(Math.Round(actlyear_next)));
                    dt.Rows.Add(totalof, Convert.ToString(Math.Round(pre_actlyear, 0)), Convert.ToString(Math.Round(actlyear, 0)), Convert.ToString(Math.Round(actlyear_next)), Convert.ToString(Math.Round(change)) + "%", Convert.ToString(Math.Round(ytd)), Convert.ToString(Math.Round(acvmnt)) + "%", Convert.ToString(Math.Round(askrate)));

                }

                // dt.Rows.Add(totalof,Convert.ToString(Math.Round(pre_actlyear, 0)), Convert.ToString(Math.Round(actlyear, 0)), Convert.ToString(Math.Round(actlyear_next)));

            }
            return dt;
        }

        public DataSet cust_total(string branch_Code, string se_id, string cust_type, string cust_num, float byValueIn, string cter = null)
        {
            int actual_mnth = 12 - (objConfig.getActualMonth());
            DataSet dsTables = new DataSet();
            DataTable dt = new DataTable();
            dt.Columns.Add("cust_name");
            dt.Columns.Add("cust_number");
            dt.Columns.Add("sales_value_year_1");
            dt.Columns.Add("sales_value_year_0");
            dt.Columns.Add("estimate_value_next_year");

            dt.Columns.Add("change", typeof(string));
            dt.Columns.Add("ytd", typeof(string));
            dt.Columns.Add("acvmnt", typeof(string));
            dt.Columns.Add("askrate", typeof(string));

            DataTable dtReport = new DataTable();
            dtReport.Columns.Add("cust_name");
            dtReport.Columns.Add("cust_number");
            dtReport.Columns.Add("sales_value_year_1");
            dtReport.Columns.Add("sales_value_year_0");
            dtReport.Columns.Add("estimate_value_next_year");
            dtReport.Columns.Add("change", typeof(string));
            dtReport.Columns.Add("ytd", typeof(string));
            dtReport.Columns.Add("acvmnt", typeof(string));
            dtReport.Columns.Add("askrate", typeof(string));

            DataTable dtCTotals;
            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };

            dtCTotals = getSalesbycustomers(branch_Code, se_id, cust_type, cust_num, cter);

            for (int i = 0; i < dtCTotals.Rows.Count; i++)
            {
                float pre_actlyear = dtCTotals.Rows[i].ItemArray[2].ToString() == "" || dtCTotals.Rows[i].ItemArray[2].ToString() == null ? 0 //13
                            : Convert.ToInt64(dtCTotals.Rows[i].ItemArray[2].ToString());
                float actlyear = dtCTotals.Rows[i].ItemArray[3].ToString() == "" || dtCTotals.Rows[i].ItemArray[3].ToString() == null ? 0   //14
                            : Convert.ToInt64(dtCTotals.Rows[i].ItemArray[3].ToString());
                float actlyear_next = dtCTotals.Rows[i].ItemArray[4].ToString() == "" || dtCTotals.Rows[i].ItemArray[4].ToString() == null ? 0   //15
                           : Convert.ToInt64(dtCTotals.Rows[i].ItemArray[4].ToString());
                pre_actlyear = pre_actlyear == 0 ? 0 : (pre_actlyear / byValueIn);
                actlyear = actlyear == 0 ? 0 : (actlyear / byValueIn);
                actlyear_next = actlyear_next == 0 ? 0 : (actlyear_next / byValueIn);


                string cust_name = dtCTotals.Rows[i].ItemArray[0].ToString();
                string cust_number = dtCTotals.Rows[i].ItemArray[1].ToString();
                float change = actlyear == 0 ? 0 : (actlyear_next / actlyear - 1) * 100; //15/14
                float ytd = dtCTotals.Rows[i].ItemArray[5].ToString() == "" || dtCTotals.Rows[i].ItemArray[5].ToString() == null ? 0   //ytd
                               : Convert.ToInt64(dtCTotals.Rows[i].ItemArray[5].ToString());
                ytd = ytd == 0 ? 0 : (ytd / byValueIn);
                float acvmnt = actlyear_next == 0 ? 0 : (ytd / actlyear_next) * 100; //ytd/2015B

                float askrate = actual_mnth == 0 ? 0 : (actlyear_next - ytd) / actual_mnth;
                bool chkforzeroes = DeleteEmptyRows(pre_actlyear, actlyear, actlyear_next, change, ytd, acvmnt, askrate);
                if (chkforzeroes == false)
                {
                    dt.Rows.Add(cust_name, cust_number, (Math.Round(pre_actlyear, 0)).ToString("N0", culture), (Math.Round(actlyear, 0)).ToString("N0", culture), (Math.Round(actlyear_next)).ToString("N0", culture), Convert.ToString(Math.Round(change)) + "%", (Math.Round(ytd)).ToString("N0", culture), Convert.ToString(Math.Round(acvmnt)) + "%", (Math.Round(askrate)).ToString("N0", culture));
                    dtReport.Rows.Add(cust_name, cust_number, (Math.Round(pre_actlyear, 0)).ToString(), (Math.Round(actlyear, 0)).ToString(), (Math.Round(actlyear_next)).ToString(), Convert.ToString(Math.Round(change)) + "%", (Math.Round(ytd)).ToString(), Convert.ToString(Math.Round(acvmnt)) + "%", (Math.Round(askrate)).ToString());
                }
            }
            dsTables.Tables.Add(dt);
            dsTables.Tables.Add(dtReport);
            return dsTables;
        }
        /* ------------------------------------SALES BY CUSTOMERS--END-----------------------------------------------------------------------*/
        /*---------------------------------------SALES BY CUSTOMERS _OVERALL----------------------------------------------------------------*/


        /*-----------------------------------------SALES BY CUSTOMERS _END-------------------------------------------------------*/

        /* ------------------------------------SALES BY CUSTOMERS--REVIEWS VARIATION-----------------------------------------------------------------------*/
        public DataTable getSalesbycustomers_variation(string branchcode, string salesEngineerID, int? negative, int? positive, string custtype, string cter)
        {
            DataTable dssalesbycust_variation = new DataTable();

            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getsalesbycustomer_variation", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@BranchCode", SqlDbType.VarChar, 50).Value = branchcode;
                command.Parameters.Add("@salesengineer_id", SqlDbType.VarChar, 50).Value = salesEngineerID;
                command.Parameters.Add("@negative", SqlDbType.VarChar, 50).Value = negative;
                command.Parameters.Add("@positive", SqlDbType.VarChar, 50).Value = positive;
                command.Parameters.Add("@customer_type", SqlDbType.VarChar, 50).Value = custtype;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 50).Value = cter;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dssalesbycust_variation);

                return dssalesbycust_variation;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dssalesbycust_variation;
        }
        public DataTable customer_variation(string branchcode, string salesEngineerID, int? negative, int? positive, string custtype, float byValueIn, string cter = null)
        {
            //int actual_mnth = 12 - (objConfig.getActualMonth());
            DataTable dt = new DataTable();
            
            dt.Columns.Add("cust_name");
            dt.Columns.Add("cust_number");
            dt.Columns.Add("customer_type");
            dt.Columns.Add("Branch");
            dt.Columns.Add("EngineerName");
            dt.Columns.Add("sales_year_0");
            dt.Columns.Add("budget");
            dt.Columns.Add("ytdplan");
            dt.Columns.Add("ytd");
            dt.Columns.Add("variance");
            dt.Columns.Add("variancePct", typeof(int));
            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };
            DataTable dtCTotals;

            dtCTotals = getSalesbycustomers_variation(branchcode, salesEngineerID, negative, positive, custtype, cter);


            for (int i = 0; i < dtCTotals.Rows.Count; i++)
            {
                string cust_name = dtCTotals.Rows[i].ItemArray[0].ToString();
                string cust_numer = dtCTotals.Rows[i].ItemArray[1].ToString();
                string cust_type = dtCTotals.Rows[i].ItemArray[2].ToString();
                string branch = dtCTotals.Rows[i].ItemArray[3].ToString() == null ? ""
                                      : (dtCTotals.Rows[i]["Branch"].ToString());
                string se = dtCTotals.Rows[i].ItemArray[4].ToString() == null ? ""
                               : (dtCTotals.Rows[i]["EngineerName"].ToString());
     
             
                float sales_year_0 = dtCTotals.Rows[i].ItemArray[5].ToString() == "" || dtCTotals.Rows[i].ItemArray[5].ToString() == null ? 0   //15
                           : Convert.ToInt64(dtCTotals.Rows[i].ItemArray[5].ToString());

                float budgetval = dtCTotals.Rows[i].ItemArray[6].ToString() == "" || dtCTotals.Rows[i].ItemArray[6].ToString() == null ? 0   //15
                            : Convert.ToInt64(dtCTotals.Rows[i].ItemArray[6].ToString());

                float ytdplan = dtCTotals.Rows[i].ItemArray[7].ToString() == "" || dtCTotals.Rows[i].ItemArray[7].ToString() == null ? 0   //mtd
                            : Convert.ToInt64(dtCTotals.Rows[i].ItemArray[7].ToString());

                float ytdval = dtCTotals.Rows[i].ItemArray[8].ToString() == "" || dtCTotals.Rows[i].ItemArray[8].ToString() == null ? 0   //mtd
                           : Convert.ToInt64(dtCTotals.Rows[i].ItemArray[8].ToString());
                
                float variance = dtCTotals.Rows[i].ItemArray[9].ToString() == "" || dtCTotals.Rows[i].ItemArray[9].ToString() == null ? 0   //mtd
                        : Convert.ToInt64(dtCTotals.Rows[i].ItemArray[9].ToString());
                
                float variancePrcnt = dtCTotals.Rows[i].ItemArray[10].ToString() == "" || dtCTotals.Rows[i].ItemArray[10].ToString() == null ? 0   //mtd
                        : Convert.ToInt64(dtCTotals.Rows[i].ItemArray[10].ToString());

                sales_year_0 = sales_year_0 == 0 ? 0 : (sales_year_0 / byValueIn);
                budgetval = budgetval == 0 ? 0 : (budgetval / byValueIn);
                ytdplan = ytdplan == 0 ? 0 : (ytdplan / byValueIn);
                ytdval = ytdval == 0 ? 0 : (ytdval / byValueIn);
                variance = variance == 0 ? 0 : (variance / byValueIn);
                bool allCoulmnszero = DeleteEmptyRows(0, sales_year_0, budgetval, ytdplan, ytdval, variance, variancePrcnt);
                if (allCoulmnszero == false)
                {

                    dt.Rows.Add(cust_name, cust_numer, cust_type, branch, se, (Math.Round(sales_year_0, 0).ToString("N0", culture)), (Math.Round(budgetval).ToString("N0", culture)), (Math.Round(ytdplan).ToString("N0", culture)), (Math.Round(ytdval).ToString("N0", culture)), (Math.Round(variance).ToString("N0", culture)), (Math.Round(variancePrcnt)));
                }
            }

            return dt;
        }



        /* ------------------------------------SALES BY CUSTOMERS-END-REVIEWS VARIATION-----------------------------------------------------------------------*/
        /*--------------------------------------Product Review-Variation--START--------------------------------------------------------------------------------*/
        public DataTable getProductVariation(string branchcode, string salesEngineerID, int? negative, int? positive, string cter)
        {
            DataTable dssalesbyprdct_variation = new DataTable();

            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getApplicationVariation", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@BranchCode", SqlDbType.VarChar, 50).Value = branchcode;
                command.Parameters.Add("@salesengineer_id", SqlDbType.VarChar, 50).Value = salesEngineerID;
                command.Parameters.Add("@negative", SqlDbType.VarChar, 50).Value = negative;
                command.Parameters.Add("@positive", SqlDbType.VarChar, 50).Value = positive;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 50).Value = cter;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dssalesbyprdct_variation);

                return dssalesbyprdct_variation;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dssalesbyprdct_variation;
        }
        public DataTable product_variation(string branchcode, string salesEngineerID, int? negative, int? positive, float byValueIn, string cter = null)
        {
            //int actual_mnth = 12 - (objConfig.getActualMonth());
            DataTable dt = new DataTable();
            dt.Columns.Add("prdct_name");
            //dt.Columns.Add("Branch");
            //dt.Columns.Add("sales engineer");
            dt.Columns.Add("sales_year_0");
            dt.Columns.Add("budget");
            dt.Columns.Add("ytdplan");
            dt.Columns.Add("ytd");
            dt.Columns.Add("variance", typeof(string));
            dt.Columns.Add("pvariancePct");
            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };
            DataTable dtCTotals;

            dtCTotals = getProductVariation(branchcode, salesEngineerID, negative, positive, cter);


            for (int i = 0; i < dtCTotals.Rows.Count; i++)
            {
                string prdct_name = dtCTotals.Rows[i].ItemArray[1].ToString();

                //string branch = dt.Rows[i].ItemArray[0].ToString() == null ? ""
                //                        : (dt.Rows[i]["Branch"].ToString());
                //string se = dt.Rows[i].ItemArray[0].ToString() == null ? ""
                //               : (dt.Rows[i]["EngineerName"].ToString());

                float sales_year_0 = dtCTotals.Rows[i].ItemArray[2].ToString() == "" || dtCTotals.Rows[i].ItemArray[2].ToString() == null ? 0   //14
                            : Convert.ToInt64(dtCTotals.Rows[i].ItemArray[2].ToString());
                float budgetval = dtCTotals.Rows[i].ItemArray[3].ToString() == "" || dtCTotals.Rows[i].ItemArray[3].ToString() == null ? 0   //15
                            : Convert.ToInt64(dtCTotals.Rows[i].ItemArray[3].ToString());

                float ytdplan = dtCTotals.Rows[i].ItemArray[4].ToString() == "" || dtCTotals.Rows[i].ItemArray[4].ToString() == null ? 0   //mtd
                            : Convert.ToInt64(dtCTotals.Rows[i].ItemArray[4].ToString());

                float ytdval = dtCTotals.Rows[i].ItemArray[5].ToString() == "" || dtCTotals.Rows[i].ItemArray[5].ToString() == null ? 0   //mtd
                           : Convert.ToInt64(dtCTotals.Rows[i].ItemArray[5].ToString());

                float variance = dtCTotals.Rows[i].ItemArray[6].ToString() == "" || dtCTotals.Rows[i].ItemArray[6].ToString() == null ? 0   //mtd
                           : Convert.ToInt64(dtCTotals.Rows[i].ItemArray[6].ToString());
                float variancePrcnt = dtCTotals.Rows[i].ItemArray[7].ToString() == "" || dtCTotals.Rows[i].ItemArray[7].ToString() == null ? 0   //mtd
                           : Convert.ToInt64(dtCTotals.Rows[i].ItemArray[7].ToString());

                sales_year_0 = sales_year_0 == 0 ? 0 : (sales_year_0 / byValueIn);
                budgetval = budgetval == 0 ? 0 : (budgetval / byValueIn);
                ytdval = ytdval == 0 ? 0 : (ytdval / byValueIn);
                ytdplan = ytdplan == 0 ? 0 : (ytdplan / byValueIn);
                variance = variance == 0 ? 0 : (variance / byValueIn);

                //float variancePrcnt = budgetval == 0 ? 0 : (variance / budgetval) * 100;
                dt.Rows.Add(prdct_name, (Math.Round(sales_year_0, 0).ToString("N0", culture)), (Math.Round(budgetval).ToString("N0", culture)), (Math.Round(ytdplan).ToString("N0", culture)), (Math.Round(ytdval).ToString("N0", culture)), (Math.Round(variance).ToString("N0", culture)), Convert.ToString(Math.Round(variancePrcnt)));

            }

            return dt;
        }


        /*--------------------------------------Product Review-Variation--END--------------------------------------------------------------------------------*/

        /*Created by Anoj for MonthlyTarget*/
        /*Created by Anoj for MonthlyTarget*/

        //Anoj5-1-2015//
        public DataTable LoadBranchCode(string strUserId)
        {
            DataTable dtBracnchCode = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand Cmd = new SqlCommand();

            }
            catch (Exception ex)
            {
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtBracnchCode;
        }



        // Anoj7-1-2015//
        public DataTable getMOnthlyTotal(string BranchCode, int month, string type)
        {
            DataTable dssalesbycust = new DataTable();

            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("[getsales_overall_a]", connection);
                command.CommandType = CommandType.StoredProcedure;

                command.Parameters.Add("@month", SqlDbType.Int).Value = month;
                command.Parameters.Add("@customer_type ", SqlDbType.VarChar, 50).Value = type;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dssalesbycust);

                return dssalesbycust;
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dssalesbycust;
        }

        public bool DeleteEmptyRows(float col1, float col2, float col3, float col4, float col5, float col6, float col7)
        {

            DataTable dt1 = new DataTable();

            dt1.Columns.Add("sales_value_year_1", typeof(string));
            dt1.Columns.Add("sales_value_year_0", typeof(string));
            dt1.Columns.Add("estimate_value_next_year", typeof(string));
            dt1.Columns.Add("change", typeof(string));
            dt1.Columns.Add("ytd", typeof(string));
            dt1.Columns.Add("ach", typeof(string));
            dt1.Columns.Add("arate", typeof(string));
            dt1.Rows.Add(col1, col2, col3, col4, col5, col6, col7);
            DataRow dr = dt1.Rows[0];
            bool flag = AreAllColumnsEmpty(dr);


            return flag;
        }
        public bool DeleteEmptyRows(decimal col1, decimal col2, decimal col3, decimal col4, decimal col5, decimal col6, decimal col7)
        {

            DataTable dt1 = new DataTable();
            dt1.Columns.Add("sales_value_year_1", typeof(string));
            dt1.Columns.Add("sales_value_year_0", typeof(string));
            dt1.Columns.Add("estimate_value_next_year", typeof(string));
            dt1.Columns.Add("change", typeof(string));
            dt1.Columns.Add("ytd", typeof(string));
            dt1.Columns.Add("ach", typeof(string));
            dt1.Columns.Add("arate", typeof(string));
            dt1.Rows.Add(col1, col2, col3, col4, col5, col6, col7);
            DataRow dr = dt1.Rows[0];
            bool flag = AreAllColumnsEmpty(dr);
            return flag;
        }

        bool AreAllColumnsEmpty(DataRow dr)
        {
            dr.ItemArray = dr.ItemArray.Select(i => i == null ? string.Empty : i.ToString()).ToArray();
            if (dr == null)
            {
                return true;
            }
            else
            {
                foreach (string value in dr.ItemArray)
                {
                    if (value != "0" && value != "" && value != "0.00000")
                    {
                        return false;
                    }
                }
                return true;
            }
        }
    }
}