﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TaegutecSalesBudget.App_Code.BOL
{
    public class ImproperCalendarEvent
    {
        public int id { get; set; }
        public string agenda { get; set; }
        public string customerNumber { get; set; }
        public string customer_short_name { get; set; }
        public string customer_class { get; set; }
        public string project_id { get; set; }
        //public string visit_date_start { get; set; }       
        //public string visit_date_end { get; set; }
        public string remarks { get; set; }
        public string modified_date { get; set; }
        public string distributor_number { get; set; }
        public string start { get; set; }
        public string end { get; set; }
        public int isSubmit { get; set; }
        public string salesEngineer { get; set; }
        public string visited_salesengineer_id { get; set; }
        public string visited_salesengineer_name { get; set; }
        public string created_date { get; set; }
      

    }
}