﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.IO;


namespace TaegutecSalesBudget
{
    public class Budget
    {
        public string customer_number, salesengineer_id, item_code, status_sales_engineer, status_branch_manager, status_ho, Review_flag;
        public int budget_year, estimated_qty, budget_id;
        public decimal estimated_rate, estimated_value;


        public DataTable LoadBudget(string CustomerNumber)
        {
            DataTable dsBudget = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getBudgetDetailsbyCustNum", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@CustomerNumber", SqlDbType.VarChar, 50).Value = CustomerNumber;
               
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dsBudget);
               
                return dsBudget;
            }
            
            catch(Exception ex){}            
            finally{
               if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dsBudget;
        }

        public DataTable LoadSubFamilyId()
        {
            DataTable dtSubFam = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getSubFamily", connection);
                command.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtSubFam);
                return dtSubFam;
            }
            
            catch(Exception ex){}            
            finally{
               if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            

            return dtSubFam;
        }

        public DataTable LoadFamilyId()
        {
            DataTable dtSubFam = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getFamily", connection);
                command.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtSubFam);
                return dtSubFam;
            }

            catch (Exception ex) { }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }


            return dtSubFam;
        }


        public DataTable LoadCustomerDetails(string SalesEngineerId, string RoleId, string territory_engineer_id = null, string cterType = null,string custtype=null)
        {
            DataTable dtCustomerDetails = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getCustomerDetails", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@assigned_salesengineer_id", SqlDbType.VarChar, 10).Value = SalesEngineerId;
                command.Parameters.Add("@roleId", SqlDbType.VarChar, 50).Value = RoleId;
                command.Parameters.Add("@territory_engineer_id", SqlDbType.VarChar, 50).Value = territory_engineer_id;
                command.Parameters.Add("@custtype", SqlDbType.VarChar, 50).Value = custtype;
                command.Parameters.Add("@cter_type", SqlDbType.VarChar, 50).Value = cterType;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtCustomerDetails);
                //return dtCustomerDetails;
            }

            catch (Exception ex)
            {

            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            // getCustomerDetails
            return dtCustomerDetails;

        }


        //Main Basic
        public DataTable LoadBudgetbySubFamilyId_CustNum(int subfamId, string CutNum, string type)
        {
            DataTable dtBudget = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("getBudgetbySubFam_CustNum", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@CustomerNumber", SqlDbType.VarChar, 50).Value = CutNum;
                command.Parameters.Add("@subFamId", SqlDbType.Int).Value = subfamId;
                command.Parameters.Add("@type", SqlDbType.VarChar, 20).Value = type;

                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtBudget);

                return dtBudget;
            }

            catch (Exception ex) { }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtBudget;
        }

      
        public List<DataTable> LoadBudgetbySubFamily_old(string CustomerNumber, DataTable dtBudget)
        {

            List<DataTable> listbudgetdata = new List<DataTable>();
            List<string> lsType = new List<string>();
            lsType.Add("I");
            lsType.Add("P");
            lsType.Add("S");
            lsType.Add("T");
            lsType.Add("X");
            lsType.Add("");

            DataTable dtSubFamily = LoadSubFamilyId();
            for (int i = 0; i < dtSubFamily.Rows.Count; i++)
            {
                int subfamId = Convert.ToInt32(dtSubFamily.Rows[i].ItemArray[0].ToString());
                if (subfamId != 0)
                {
                    //dtBudget = LoadBudgetbySubFamilyId_CustNum(subfamId, CustomerNumber);
                    foreach (string strType in lsType)
                    {
                        listbudgetdata.Add(LoadBudgetbySubFamilyId_CustNum(subfamId, CustomerNumber, strType));
                        //listbudgetdata.Add(getBudgetbySubFamily(subfamId, CustomerNumber, dtBudget, strType));
                    }
                 
                }
            }

            return listbudgetdata;



            //DataTable dt = new DataTable();
            ////DataTable dtSubFamily = LoadSubFamilyId();
            //List<KeyValuePair<int, DataTable>> listbudgetdata = new List<KeyValuePair<int, DataTable>>();
            //DataTable dtSubFamily = LoadSubFamilyId();
            //for (int i = 0; i < dtSubFamily.Rows.Count; i++)
            //{
            //    int subfamId = Convert.ToInt32(dtSubFamily.Rows[i].ItemArray[0].ToString());
            //    if (subfamId != 0)
            //    {
            //        listbudgetdata.Insert(i,new KeyValuePair<int, DataTable>(subfamId, getBudgetbySubFamily(subfamId, CustomerNumber)));
            //    }
            //}


            return listbudgetdata;
        }

        public List<DataTable> LoadBudgetbySubFamily_old_withoutType(string CustomerNumber)
        {
            List<string> lsType = new List<string>();
            lsType.Add("I");
            lsType.Add("P");
            lsType.Add("S");
            lsType.Add("T");
            lsType.Add("X");
            lsType.Add("");
            List<DataTable> listbudgetdata = new List<DataTable>();

            DataTable dtSubFamily = LoadSubFamilyId();
            for (int i = 0; i < dtSubFamily.Rows.Count; i++)
            {
                foreach (string type in lsType)
                {
                    int subfamId = Convert.ToInt32(dtSubFamily.Rows[i].ItemArray[0].ToString());
                    if (subfamId != 0)
                    {
                        listbudgetdata.Add(LoadBudgetbySubFamilyId_CustNum(subfamId, CustomerNumber, type));
                    }
                }
            }

            return listbudgetdata;


        }

        public DataTable getBudgetbySubFamily(int SubfamId, string CustomerNumber, DataTable dtTemnp, string type)
        {
          
            //DataTable dtTemnp = LoadBudget(CustomerNumber);
            DataTable dtbudget = new DataTable();
            dtbudget = dtTemnp.Clone();
          
                for (int i = 0; i < dtTemnp.Rows.Count; i++)
                {
                    int tempId = Convert.ToInt32(dtTemnp.Rows[i].ItemArray[7].ToString());
                    if (SubfamId == tempId && type == dtTemnp.Rows[i].ItemArray[9].ToString())
                    {
                        dtbudget.Rows.Add(
                            dtTemnp.Rows[i].ItemArray[0].ToString(),
                            dtTemnp.Rows[i].ItemArray[1].ToString(),
                            dtTemnp.Rows[i].ItemArray[2].ToString(),
                            dtTemnp.Rows[i].ItemArray[3].ToString(),
                            dtTemnp.Rows[i].ItemArray[4].ToString(),
                            Convert.ToInt32(dtTemnp.Rows[i].ItemArray[5].ToString()),
                            Convert.ToInt32(dtTemnp.Rows[i].ItemArray[6].ToString() != "" ? dtTemnp.Rows[i].ItemArray[6].ToString() : "0"),
                            Convert.ToInt32(dtTemnp.Rows[i].ItemArray[7].ToString()),
                            dtTemnp.Rows[i].ItemArray[8].ToString(),
                            dtTemnp.Rows[i].ItemArray[9].ToString(),
                            dtTemnp.Rows[i].ItemArray[10].ToString(),
                            dtTemnp.Rows[i].ItemArray[11].ToString(),
                            dtTemnp.Rows[i].ItemArray[12].ToString(),
                            dtTemnp.Rows[i].ItemArray[13].ToString(),
                            Convert.ToInt32(dtTemnp.Rows[i].ItemArray[14].ToString() != "" ? dtTemnp.Rows[i].ItemArray[14].ToString() : "0"),
                            Convert.ToInt32(dtTemnp.Rows[i].ItemArray[15].ToString() != "" ? dtTemnp.Rows[i].ItemArray[15].ToString() : "0"),
                            Convert.ToInt32(dtTemnp.Rows[i].ItemArray[16].ToString() != "" ? dtTemnp.Rows[i].ItemArray[16].ToString() : "0"),
                            Convert.ToInt32(dtTemnp.Rows[i].ItemArray[17].ToString() != "" ? dtTemnp.Rows[i].ItemArray[17].ToString() : "0"),
                            Convert.ToInt32(dtTemnp.Rows[i].ItemArray[18].ToString() != "" ? dtTemnp.Rows[i].ItemArray[18].ToString() : "0"),
                            Convert.ToInt32(dtTemnp.Rows[i].ItemArray[19].ToString() != "" ? dtTemnp.Rows[i].ItemArray[19].ToString() : "0"),
                            Convert.ToInt32(dtTemnp.Rows[i].ItemArray[20].ToString())
                            );



                    }
                }
           
            return dtbudget;
        }

        public string insertProBudget(Budget objBudget)
        {

            string strResult;
            string errorNum;

            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection connection = new SqlConnection(connstring);
            connection.Open();
            SqlCommand command = new SqlCommand("insertProBudget", connection);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter[] parameters = {
                new SqlParameter("@budget_year", SqlDbType.VarChar,50), //1
                new SqlParameter("@customer_number", SqlDbType.VarChar,50), //2
                new SqlParameter("@salesengineer_id", SqlDbType.VarChar,50), //3
               
                new SqlParameter("@item_code", SqlDbType.VarChar,50), //4
                new SqlParameter("@estimated_qty", SqlDbType.Int,50, null), //5
                new SqlParameter("@estimated_rate", SqlDbType.Decimal,23,null), //6
                new SqlParameter("@estimated_value", SqlDbType.Decimal), //7

                new SqlParameter("@status_sales_engineer", SqlDbType.VarChar,50,null), //8
                new SqlParameter("@status_branch_manager", SqlDbType.VarChar,50,null), //9
                new SqlParameter("@status_ho", SqlDbType.VarChar,50,null), //10
                new SqlParameter("@Review_flag", SqlDbType.VarChar,50,null), //11  
              
                new SqlParameter("@Error", SqlDbType.VarChar,50),//12
                new SqlParameter("@ErrNum", SqlDbType.Int),//13
                new SqlParameter("@budget_id", SqlDbType.Int,100,null),//14 
                new SqlParameter("@ExistID", SqlDbType.Int,100,null)};    
            
            //commdan.Parameters["@estimated_rate"].Precision = 22;
            //commdan.Parameters["@estimated_rate"].Scale = 5;
            //commdan.Parameters["@estimated_value"].Precision = 22;
            //commdan.Parameters["@estimated_value"].Scale = 5;
    

            parameters[0].Value = objBudget.budget_year;
            parameters[1].Value = (objBudget.customer_number);
            parameters[2].Value = (objBudget.salesengineer_id);

            parameters[3].Value = (objBudget.item_code);
            parameters[4].Value = (objBudget.estimated_qty);
            parameters[5].Value = (objBudget.estimated_rate);
            parameters[6].Value = (objBudget.estimated_value);

            parameters[7].Value = (objBudget.status_sales_engineer);
            parameters[8].Value = (objBudget.status_branch_manager);
            parameters[9].Value = (objBudget.status_ho);
            parameters[10].Value = (objBudget.Review_flag);
           
            parameters[11].Direction = ParameterDirection.Output;
            parameters[12].Direction = ParameterDirection.Output;
            parameters[13].Value = (objBudget.budget_id);
            parameters[14].Direction = ParameterDirection.Output;


            

            Int32 j;
            for (j = 0; j <= parameters.Length - 1; j++)
            {
                command.Parameters.Add(parameters[j]);
            }


            //string a = “1234.34″;
            //decimal b = decimal.Parse(a);
           
            try
            {
                command.ExecuteNonQuery();

                strResult = parameters[11].Value.ToString();
                errorNum = parameters[12].Value.ToString();
                string temp = command.Parameters["@ExistID"].Value.ToString();
                budget_id = temp != null && temp != "" ? Convert.ToInt32(command.Parameters["@ExistID"].Value.ToString()) : budget_id;

                return strResult;
            }

            catch (Exception ex)
            {
                strResult = ex.Message;
                return strResult;
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }


        }

        public string getCustomerBudgetStatus(string customer_number, string role) 
        {
            string status = "";

            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection connection = new SqlConnection(connstring);
            connection.Open();
            SqlCommand command = new SqlCommand("getCustomerBudgetStatus", connection);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter[] parameters = {
                new SqlParameter("@customer_number", SqlDbType.VarChar,50), //1
                new SqlParameter("@role", SqlDbType.VarChar,50), //2
                new SqlParameter("@Status", SqlDbType.VarChar,50) //3
                                        };
               
            parameters[0].Value = customer_number;
            parameters[1].Value = role;            
            parameters[2].Direction = ParameterDirection.Output;

            Int32 j;
            for (j = 0; j <= parameters.Length - 1; j++)
            {
                command.Parameters.Add(parameters[j]);
            }

            try
            {
                command.ExecuteNonQuery();
                status = command.Parameters["@Status"].Value.ToString();
                return status;
            }

            catch (Exception ex)
            {
                status = ex.Message;
                return status;
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

           
        }

        public string insertPwd(string id, string Pwd)
        {

            string strResult;
            string errorNum;

            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection connection = new SqlConnection(connstring);
            connection.Open();
            SqlCommand command = new SqlCommand("insertPwd", connection);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter[] parameters = {
                new SqlParameter("@id", SqlDbType.VarChar,50), //1
                new SqlParameter("@pwd", SqlDbType.VarChar,1000)};//2

            parameters[0].Value = id;
            parameters[1].Value = Pwd;


            Int32 j;
            for (j = 0; j <= parameters.Length - 1; j++)
            {
                command.Parameters.Add(parameters[j]);
            }

            try
            {
                command.ExecuteNonQuery();
                return "";
            }

            catch (Exception ex)
            {
                strResult = ex.Message;
                return strResult;
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }


        }

        public void callPwdMeth()
        {
            LoginAuthentication logauth = new LoginAuthentication();
            DataTable dtId = SalesEngineer();
            for (int i = 0; i < dtId.Rows.Count; i++)
            {
                string pwd = PasswordSecurityGenerate.Generate(8, 8);
                pwd = logauth.Encrypt(pwd);
                insertPwd(dtId.Rows[i].ItemArray[0].ToString(), pwd);
            }



        }

        public DataTable SalesEngineer()
        {
            DataTable dtSubFam = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("getSalesEng", connection);
                command.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtSubFam);
                return dtSubFam;
            }

            catch (Exception ex) { }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return dtSubFam;
        }

        public string DeleteBudgetNumbers(int BudgetId, string item_code) 
        {
            string strResult;
            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection connection = new SqlConnection(connstring);
            connection.Open();
            SqlCommand command = new SqlCommand("DeleteBudgetNum", connection);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter[] parameters = {
                new SqlParameter("@budId", SqlDbType.Int), //1
                new SqlParameter("@item_code", SqlDbType.VarChar,50),
                new SqlParameter("@Error", SqlDbType.VarChar,50)};//2

            parameters[0].Value = BudgetId;
            parameters[1].Value = item_code;
            parameters[2].Value = ParameterDirection.Output; 


            Int32 j;
            for (j = 0; j <= parameters.Length - 1; j++)
            {
                command.Parameters.Add(parameters[j]);
                
            }

            try
            {
                command.ExecuteNonQuery();
                string temp = command.Parameters["@Error"].Value.ToString();
                if (temp == "Deleted successfully")
                { return ""; }
                else if (temp == "given itemcode / budget id not exist in table")
                { return "error"; }
                else { return ""; }
            }

            catch (Exception ex)
            {
                strResult = ex.Message;
                return strResult;
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

        }

        public List<DataTable> LoadBudgetbySubFamily_Region(string Region, string SalesEngineer, string CustomerType, string CustomerId, string cter = null)
        {
            List<string> lsType = new List<string>();
            lsType.Add("I");
            lsType.Add("P");
            lsType.Add("S");
            lsType.Add("T");
            lsType.Add("X");
            lsType.Add("");
            List<DataTable> listbudgetdata = new List<DataTable>();

            DataTable dtSubFamily = LoadSubFamilyId();
            for (int i = 0; i < dtSubFamily.Rows.Count; i++)
            {
                foreach (string type in lsType)
                {
                    int subfamId = Convert.ToInt32(dtSubFamily.Rows[i].ItemArray[0].ToString());
                    if (subfamId != 0)
                    {
                        listbudgetdata.Add(LoadBudgetbySubFamilyId_Region(subfamId, Region, type, SalesEngineer, CustomerType, CustomerId,cter));
                    }
                }
            }

            return listbudgetdata;


        }

        public List<DataTable> LoadBudgetbySummery(string Region, string SalesEngineer, string CustomerType, string CustomerId, string cter = null)
        {
            DataTable dt = LoadBudgetbySummery_Get( Region, SalesEngineer, CustomerType, CustomerId, cter);

            List<DataTable> listbudgetdata = new List<DataTable>();
            //listbudgetdata.Add(dt);

            List<string> lsType = new List<string>();
            lsType.Add("I");
            lsType.Add("P");
            lsType.Add("S");
            lsType.Add("T");
            lsType.Add("X");
            lsType.Add("");            
            DataTable dtSubFamily = LoadSubFamilyId();


            for (int i = 0; i < dtSubFamily.Rows.Count; i++)
            {
                int subfamId = Convert.ToInt32(dtSubFamily.Rows[i].ItemArray[0].ToString());                   
                foreach (string type in lsType)
                {
                    DataTable dtTemp = new DataTable();
                    dtTemp = dt.Clone();                
                    for (int j = 0; j < dt.Rows.Count; j++) 
                    {
                        int tempsubFamId = dt.Rows[j].Field<int>("item_sub_family_id");
                        string tempInsFalg = dt.Rows[j].Field<string>("insert_or_tool_flag");
                        if (subfamId == tempsubFamId && tempInsFalg == type) 
                        {                            
                            dtTemp.Rows.Add(dt.Rows[j].ItemArray);
                        }
                    }
                    listbudgetdata.Add(dtTemp);
                }
               
            }
            return listbudgetdata;

        }

        public DataTable LoadBudgetbySubFamilyId_Region(int subfamId, string Region, string type, string SalesEngineer, string CustomerType, string CustomerId,string cter = null)
        {
            DataTable dtBudget = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("sp_getBrachBudget", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@p_branch_code", SqlDbType.VarChar, 50).Value = Region;
                command.Parameters.Add("@p_salesengineer_id", SqlDbType.VarChar, 50).Value = SalesEngineer;
                command.Parameters.Add("@p_cust_dist_flag", SqlDbType.VarChar, 50).Value = CustomerType;
                command.Parameters.Add("@p_customer_number", SqlDbType.VarChar, 50).Value = CustomerId;

                command.Parameters.Add("@p_sub_family_id", SqlDbType.Int).Value = subfamId;
                command.Parameters.Add("@p_ins_tool_flag", SqlDbType.VarChar, 20).Value = type;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 20).Value = cter;

                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtBudget);
                //if (dtBudget == null)
                //{
                //    LogFile("No data NULL","Adapter not filling any data","",""); ;
                //}
                //else if (dtBudget.Rows.Count == 0) { LogFile("No data : count Zero", "Adapter not filling any data", "", ""); ;}
                return dtBudget;
            }

            catch (Exception ex) 
            {
                //LogFile("ERROR ", ex.Message, ex.InnerException.Message,  "=============================================================================================================");
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtBudget;
        }

        public DataTable LoadBudgetbySummery_Get( string Region, string SalesEngineer, string CustomerType, string CustomerId, string cter = null)
        {
            DataTable dtBudget = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("sp_getSummery", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@p_branch_code", SqlDbType.VarChar, 50).Value = Region;
                command.Parameters.Add("@p_salesengineer_id", SqlDbType.VarChar, 50).Value = SalesEngineer;
                command.Parameters.Add("@p_cust_dist_flag", SqlDbType.VarChar, 50).Value = CustomerType;
                command.Parameters.Add("@p_customer_number", SqlDbType.VarChar, 50).Value = CustomerId;

                command.Parameters.Add("@cter", SqlDbType.VarChar, 20).Value = cter;

                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtBudget);
                //if (dtBudget == null)
                //{
                //    LogFile("No data NULL","Adapter not filling any data","",""); ;
                //}
                //else if (dtBudget.Rows.Count == 0) { LogFile("No data : count Zero", "Adapter not filling any data", "", ""); ;}
                return dtBudget;
            }

            catch (Exception ex)
            {
                //LogFile("ERROR ", ex.Message, ex.InnerException.Message,  "=============================================================================================================");
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtBudget;
        }


        public void LogFile(string use1, string count, string names, string use)
        {
            StreamWriter log;

            if (!File.Exists("logfileBudgetBranch.txt"))
            {
                log = new StreamWriter(("C:\\TSBA_Log\\logfileBudgetBranch.txt"), true);
            }
            else
            {
                log = File.AppendText(("C:\\TSBA_Log\\logfileBudgetBranch.txt"));
            }

            // Write to the file:
            log.WriteLine("Date Time:" + DateTime.Now.ToString());
            log.WriteLine(use1);
            log.WriteLine(count);
            log.WriteLine(names);
            log.WriteLine(use + "\n");
            // Close the stream:
            log.Close();

        }

        public DataTable getBranch_by_managerApproval(string cter_type, string roleId=null, string teId = null)
        {
            DataTable dtBranchesList = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("sp_getBranch_by_managerApproval", connection);
                command.Parameters.Add("@cter_type", SqlDbType.VarChar, 10).Value = cter_type;
                command.Parameters.Add("@role", SqlDbType.VarChar, 10).Value = roleId;
                command.Parameters.Add("@teId", SqlDbType.VarChar, 10).Value = teId;
                

                command.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtBranchesList);
                return dtBranchesList;
            }

            catch (Exception ex) { }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }


            return dtBranchesList;
        }

        public string Approve_by_Branch(string brnach)
        {

            string strResult;
            string errorNum;

            string connstring = ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString();
            SqlConnection connection = new SqlConnection(connstring);
            connection.Open();
            SqlCommand command = new SqlCommand("Approve_by_Branch", connection);
            command.CommandType = CommandType.StoredProcedure;

            SqlParameter[] parameters = {
                new SqlParameter("@branch", SqlDbType.VarChar,50), //1
                new SqlParameter("@Error", SqlDbType.VarChar,50), //2                
                new SqlParameter("@ErrNum", SqlDbType.Int,50, null) //3
                                        };


            parameters[0].Value = brnach;           
            parameters[1].Direction = ParameterDirection.Output;
            parameters[2].Direction = ParameterDirection.Output;
            
            Int32 j;
            for (j = 0; j <= parameters.Length - 1; j++)
            {
                command.Parameters.Add(parameters[j]);
            }
            try
            {
                command.ExecuteNonQuery();
                strResult = parameters[1].Value.ToString();
                errorNum = parameters[2].Value.ToString();                
                return strResult;
            }

            catch (Exception ex)
            {
                strResult = ex.Message;
                return strResult;
            }

            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }


        }


        public DataTable RemoveEmptyRows(DataTable source)
        {
           int colCount = source.Columns.Count;
           int count = 0;
           DataRow currentRow = source.Rows[0];
           foreach( var colValue in currentRow.ItemArray)
           {
               if (!string.IsNullOrEmpty(colValue.ToString()))
               {
                   count++;             
               }
           }
           if (count == 0)
           {
               // If we get here, all the columns are empty
               source.Rows[0].Delete();
               source.AcceptChanges();
           }
           return source;
          

        }

        /// <summary>
        /// Author : Anamika
        /// </summary>
        /// <param name="Region"></param>
        /// <param name="SalesEngineer"></param>
        /// <param name="CustomerType"></param>
        /// <param name="CustomerId"></param>
        /// <param name="cter"></param>
        /// <returns></returns>
        internal DataTable getGrandMain(string Region, string SalesEngineer, string CustomerType, string CustomerId, string cter, int valueF)
        {
            DataTable dtBudget = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("sp_get_summary_final", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@p_branch_code", SqlDbType.VarChar, 50).Value = Region;
                command.Parameters.Add("@p_salesengineer_id", SqlDbType.VarChar, 50).Value = SalesEngineer;
                command.Parameters.Add("@p_cust_dist_flag", SqlDbType.VarChar, 50).Value = CustomerType;
                command.Parameters.Add("@p_customer_number", SqlDbType.VarChar, 50).Value = CustomerId;

                command.Parameters.Add("@cter", SqlDbType.VarChar, 20).Value = cter;
                command.Parameters.Add("@valueF", SqlDbType.Int).Value = valueF;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtBudget);
                return dtBudget;
            }

            catch (Exception ex)
            {
                
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtBudget;
        }

        /// <summary>
        /// Author : Anamika
        /// Date : Nov 14,2016
        /// Desc : Called a stored procedure to add three columns for Inserts/tool
        /// </summary>
        /// <param name="Dtbudget"></param>
        /// <returns></returns>
        internal DataTable AddInsertPerTool(DataTable Dtbudget)
        {
            DataTable dtBudget = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("sp_AddInsertPerTool", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@tblBudget", Dtbudget);

                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtBudget);
                //return dtBudget;
            }

            catch (Exception ex)
            {
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtBudget;
        }

        internal DataTable getEntryMain(string customerNumber)
        {
            DataTable dtBudget = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("sp_get_entry_final", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@p_customer_number", SqlDbType.VarChar, 50).Value = customerNumber;
                command.CommandTimeout = 1000000;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtBudget);
                return dtBudget;
            }

            catch (Exception ex)
            {

            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtBudget;
        }

        /// <summary>
        /// Author : Anamika 
        /// Date : May 05, 2017
        /// Desc : Passing all the filters to database and fetch the penetration report
        /// </summary>
        /// <param name="From_date"></param>
        /// <param name="To_date"></param>
        /// <param name="Contribution"></param>
        /// <param name="filter"></param>
        /// <param name="cter"></param>
        /// <param name="byValueIn"></param>
        /// <returns></returns>
        internal DataSet GetPenetrationReport(string From_date, string To_date, int Contribution, string filter, string cter, float byValueIn)
        {
            DataSet dtReport = new DataSet();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("sp_PenetrationReport", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@FROMDATE", SqlDbType.VarChar, 50).Value = From_date.ToString();
                command.Parameters.Add("@TODATE", SqlDbType.VarChar, 50).Value = To_date.ToString();
                command.Parameters.Add("@Contribution", SqlDbType.Int).Value = Contribution;
                command.Parameters.Add("@BY", SqlDbType.VarChar, 100).Value = filter;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 50).Value = cter;
                command.Parameters.Add("@byValueIn", SqlDbType.Float).Value = byValueIn;

                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                command.CommandTimeout = 1000000;
                sqlDa.Fill(dtReport);
                return dtReport;
            }

            catch (Exception ex)
            {

            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtReport;
        }
       
        /// <summary>
        ///  Author:K.LakshmiBindu
        /// Date: 8 Dec,2018
        /// Desc:Gets Budget details familywise,channel patner wise, customer wise
        /// </summary>
        /// <param name="BranchCode">we are passing branch code</param>
        /// <param name="Valuein">whether in thousands or units or laksh</param>
        /// <returns>dataset contains 3 tables </returns>
        public DataTable GetBudgetDetailsByFamilyCustomerChannelPatner(Review objrsum, int Valuein)
        {
             DataTable dtBudget = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("sp_getBudgetDetails", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@SE", SqlDbType.VarChar, 2000).Value = objrsum.salesengineer_id;
                command.Parameters.Add("@BRANCH", SqlDbType.VarChar, 50).Value = objrsum.BranchCode;
                command.Parameters.Add("@valueIn", SqlDbType.Int).Value = Valuein;
                command.CommandTimeout = 1000000;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                command.CommandTimeout = 100000;
                sqlDa.Fill(dtBudget);
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtBudget;
        }
        /// <summary>
        ///  Author:K.LakshmiBindu
        /// Date: 11 Dec,2018
        /// Desc:Gets Budget details familywise,channel patner wise, customer wise
        /// </summary>
        /// <returns></returns>
        public string AprroveBudget(Review objrsum, string level,out int resultnumber)
        {
            DataSet dtBudget = new DataSet();
            string strResult = null;
            resultnumber = 1;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("Approve_by_Branch_SE", connection);
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter[] parameters = {
                new SqlParameter("@SE", SqlDbType.VarChar,2000), //1
                new SqlParameter("@branch", SqlDbType.VarChar,50), //2
                new SqlParameter("@ROLE", SqlDbType.VarChar,50), //3
                  new SqlParameter("@LEVEL", SqlDbType.VarChar,50), //3
                new SqlParameter("@Error", SqlDbType.VarChar, -1),
                new SqlParameter("@ErrNum",SqlDbType.Int)
               };
                parameters[0].Value = objrsum.salesengineer_id;
                parameters[1].Value = objrsum.BranchCode;
                parameters[2].Value = objrsum.roleId;
                parameters[3].Value = level;
                parameters[4].Direction = ParameterDirection.Output;
                parameters[5].Direction = ParameterDirection.Output;
                for (int j = 0; j <= parameters.Length - 1; j++)
                {
                    command.Parameters.Add(parameters[j]);
                }
                try
                {
                    command.CommandTimeout = 100000;
                    command.ExecuteNonQuery();
                    resultnumber = Convert.ToInt32(command.Parameters["@ErrNum"].Value);
                    string profile_value = command.Parameters["@Error"].Value.ToString();
                    return strResult = profile_value;
                }
                catch (Exception ex)
                {
                    resultnumber = 1;
                    strResult = ex.Message;
                    CommonFunctions.LogErrorStatic(ex);
                   
                }
                finally
                {
                    if (connection.State == ConnectionState.Open)
                    {
                        connection.Close();
                    }
                }
                return strResult;
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return strResult;
        }

        public object ApproveFalgForBudget(Review objrsum)
        {
             object ApproveFalg=null;
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();

                SqlCommand command = new SqlCommand("sp_getApprovalFlag", connection);
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter[] parameters = {
               new SqlParameter("@Branch", SqlDbType.VarChar,50), //2
                new SqlParameter("@ROLE", SqlDbType.VarChar,50), //3
                new SqlParameter("@SE", SqlDbType.VarChar,2000), //1
                                            };
                
                for (int j = 0; j <= parameters.Length - 1; j++)
                {
                    command.Parameters.Add(parameters[j]);
                }
                   parameters[0].Value =objrsum.BranchCode;
                parameters[1].Value = objrsum.roleId; ;
                parameters[2].Value = objrsum.salesengineer_id;
                command.CommandTimeout = 100000;
                 ApproveFalg=Convert.ToInt32(command.ExecuteScalar());

                return ApproveFalg;
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return ApproveFalg ;
        }

        public DataSet GetExceptionCustForBudget(Review objrsum)
        {
            DataSet dsGetExceptionCustForBudget = new DataSet();
              SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
              try
              {
                  connection.Open();
                  SqlCommand command = new SqlCommand("sp_getExceptionCustForBudget", connection);
                  command.CommandType = CommandType.StoredProcedure;
                  command.Parameters.Add("@Branch", SqlDbType.VarChar, 50).Value = objrsum.BranchCode;
                  command.Parameters.Add("@SE", SqlDbType.VarChar,2000).Value = objrsum.salesengineer_id;
                  command.Parameters.Add("@CTR", SqlDbType.VarChar, 10).Value = objrsum.cter;
                  command.Parameters.Add("@ROLE", SqlDbType.VarChar, 10).Value = objrsum.roleId;
                  SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                  command.CommandTimeout = 1000000;
                  sqlDa.Fill(dsGetExceptionCustForBudget);
                  return dsGetExceptionCustForBudget;

              }
              catch (Exception ex)
              {
                  CommonFunctions.LogErrorStatic(ex);
              }
              finally
              {
                  if (connection.State == ConnectionState.Open)
                  {
                      connection.Close();
                  }
              }
              return dsGetExceptionCustForBudget;
        }
        }
    }