﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace TaegutecSalesBudget
{
    public class Review
    {
        public string BranchCode, salesengineer_id, customer_number, flag, item_family_name, item_sub_family_name, customer_type, product_group, item_code, roleId, cter , userid;
        public int Year, valuein;
        CommonFunctions objFunc = new CommonFunctions();
        public DataTable getMonthlyVal(Review objRSum)
        {
            DataTable dtmonthlyval = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("getMonthlyValueSum", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@BranchCode", SqlDbType.VarChar, 500000, null).Value = objRSum.BranchCode;
                command.Parameters.Add("@salesengineer_id", SqlDbType.VarChar, 500000, null).Value = objRSum.salesengineer_id;
                command.Parameters.Add("@customer_number", SqlDbType.VarChar, 500000, null).Value = objRSum.customer_number;
                command.Parameters.Add("@flag", SqlDbType.VarChar, 50, null).Value = objRSum.flag;
                command.Parameters.Add("@item_family_name", SqlDbType.VarChar, 500000, null).Value = objRSum.item_family_name;
                command.Parameters.Add("@item_sub_family_name", SqlDbType.VarChar, 500000, null).Value = objRSum.item_sub_family_name;
                command.Parameters.Add("@customer_type", SqlDbType.VarChar, 50, null).Value = objRSum.customer_type;
                command.Parameters.Add("@Year", SqlDbType.Int, 50).Value = Year;
                command.Parameters.Add("@product_group", SqlDbType.VarChar, 500000).Value = objRSum.product_group;
                command.Parameters.Add("@valuein", SqlDbType.Int, 50).Value = valuein;
                command.Parameters.Add("@item_code", SqlDbType.VarChar, 500000).Value = objRSum.item_code;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 10).Value = objRSum.cter;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                command.CommandTimeout = 100000;
                sqlDa.Fill(dtmonthlyval);
              
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }
            return dtmonthlyval;
        }
        public DataTable getMonthlyQty(Review objRSum)
        {
            DataTable dtmonthlyqty = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("getMonthlyQuantitySum", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@BranchCode", SqlDbType.VarChar, 50000, null).Value = objRSum.BranchCode;
                command.Parameters.Add("@salesengineer_id", SqlDbType.VarChar, 50000, null).Value = objRSum.salesengineer_id;
                command.Parameters.Add("@customer_number", SqlDbType.VarChar, 50000, null).Value = objRSum.customer_number;
                command.Parameters.Add("@flag", SqlDbType.VarChar, 100, null).Value = objRSum.flag;
                command.Parameters.Add("@item_family_name", SqlDbType.VarChar, 50000, null).Value = objRSum.item_family_name;
                command.Parameters.Add("@item_sub_family_name", SqlDbType.VarChar, 50000, null).Value = objRSum.item_sub_family_name;
                command.Parameters.Add("@customer_type", SqlDbType.VarChar, 50, null).Value = objRSum.customer_type;
                command.Parameters.Add("@Year", SqlDbType.Int, 50).Value = Year;
                command.Parameters.Add("@product_group", SqlDbType.VarChar, 50000).Value = objRSum.product_group;
                command.Parameters.Add("@valuein", SqlDbType.Int, 50).Value = valuein;
                command.Parameters.Add("@item_code", SqlDbType.VarChar, 50000).Value = objRSum.item_code;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 10).Value = objRSum.cter;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                command.CommandTimeout = 100000;
                sqlDa.Fill(dtmonthlyqty);
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

            }

            return dtmonthlyqty;
        }

        public DataTable getProductLine(int familyId)
        {
            DataTable dtPL = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("getProductLinebyFam", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@familyId", SqlDbType.Int).Value = familyId;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtPL);
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

            }
            return dtPL;
        }

        public DataTable getProducts(string familyId, string group)
        {
            //sp_getApplicationByFam

            DataTable dtPL = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("sp_getApplicationByFam", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@familyId", SqlDbType.VarChar, 50000, null).Value = familyId;
                command.Parameters.Add("@product_group", SqlDbType.VarChar, 50000, null).Value = group;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtPL);

            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

            }
            return dtPL;
        }

        public DataTable getFilterAreaValue(Review objRSum)
        {
            DataTable dtData = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("sp_getFilterData", connection);
                command.CommandType = CommandType.StoredProcedure;  
                command.Parameters.Add("@p_branch_code", SqlDbType.VarChar, 20000, null).Value = objRSum.BranchCode;
                command.Parameters.Add("@p_salesengineer_id", SqlDbType.VarChar, 20000, null).Value = objRSum.salesengineer_id;
                command.Parameters.Add("@p_cust_dist_flag", SqlDbType.VarChar, 50, null).Value = objRSum.customer_type;
                command.Parameters.Add("@p_role_id", SqlDbType.VarChar, 50, null).Value = objRSum.roleId;
                command.Parameters.Add("@p_flag", SqlDbType.VarChar, 50, null).Value = objRSum.flag;
                command.Parameters.Add("@p_cter", SqlDbType.VarChar, 50, null).Value = objRSum.cter;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtData);
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

            }

            return dtData;
        }

        public DataTable getQuarterlyValueSum(Review objRSum)
        {
            DataTable dtmonthlyval = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("getQuarterlyValueSum", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@BranchCode", SqlDbType.VarChar, 50000, null).Value = objRSum.BranchCode;
                command.Parameters.Add("@salesengineer_id", SqlDbType.VarChar, 50000, null).Value = objRSum.salesengineer_id;
                command.Parameters.Add("@customer_number", SqlDbType.VarChar, 50000, null).Value = objRSum.customer_number;
                command.Parameters.Add("@flag", SqlDbType.VarChar, 50, null).Value = objRSum.flag;
                command.Parameters.Add("@item_family_name", SqlDbType.VarChar, 50000, null).Value = objRSum.item_family_name;
                command.Parameters.Add("@item_sub_family_name", SqlDbType.VarChar, 50000, null).Value = objRSum.item_sub_family_name;
                command.Parameters.Add("@customer_type", SqlDbType.VarChar, 50, null).Value = objRSum.customer_type;
                command.Parameters.Add("@Year", SqlDbType.Int, 50).Value = Year;
                command.Parameters.Add("@product_group", SqlDbType.VarChar, 50000).Value = objRSum.product_group;
                command.Parameters.Add("@valuein", SqlDbType.Int, 50).Value = valuein;
                command.Parameters.Add("@item_code", SqlDbType.VarChar, 50000).Value = objRSum.item_code;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 10).Value = objRSum.cter;

                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtmonthlyval);
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

            }

            return dtmonthlyval;
        }

        public DataTable getTargets(Review objRSum, int yearmonth)
        {
            DataTable dtTargets = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("sp_getTartgets", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@branch", SqlDbType.VarChar, 50000, null).Value = objRSum.BranchCode;
                command.Parameters.Add("@seid", SqlDbType.VarChar, 50000, null).Value = objRSum.salesengineer_id;
                command.Parameters.Add("@ctype", SqlDbType.VarChar, 50, null).Value = objRSum.customer_type;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 50, null).Value = objRSum.cter;
                command.Parameters.Add("@yearmonth", SqlDbType.Int, 20000).Value = yearmonth;
                command.Parameters.Add("@valuein", SqlDbType.Int, 50).Value = objRSum.valuein;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtTargets);
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

            }

            return dtTargets;
        }
        public DataTable getTargets_total(Review objRSum, int yearmonth)
        {
            DataTable dtTargets = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("sp_getTartgets_total", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@branch", SqlDbType.VarChar, 20000, null).Value = objRSum.BranchCode;
                command.Parameters.Add("@seid", SqlDbType.VarChar, 20000, null).Value = objRSum.salesengineer_id;
                command.Parameters.Add("@ctype", SqlDbType.VarChar, 50, null).Value = objRSum.customer_type;
                command.Parameters.Add("@cter", SqlDbType.VarChar, 50, null).Value = objRSum.cter;
                command.Parameters.Add("@yearmonth", SqlDbType.Int, 20000).Value = yearmonth;
                command.Parameters.Add("@valuein", SqlDbType.Int, 50).Value = objRSum.valuein;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtTargets);
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }

            }

            return dtTargets;
        }

        public List<string> Getusernameforautocomplete(string term)
        {

            List<string> lstusernames = new List<string>();
            DataTable dtOutput = new DataTable(); ;
            SqlConnection sqlConn = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            sqlConn.Open();
            SqlCommand com = new SqlCommand("TestGetnamesforautocomplete", sqlConn);
            com.CommandType = CommandType.StoredProcedure;
            com.Parameters.Add("TERM", SqlDbType.NVarChar, 200).Value = term;
            //com.ExecuteNonQuery();
            SqlDataReader rdr = com.ExecuteReader();

            while (rdr.Read())
            {
                lstusernames.Add(rdr["region_description"].ToString());
            }
            return lstusernames;
        }

        internal DataTable getTargetvsSales(Review objRSum)
        {
            DataTable dtTargets = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("tt_se_target_vs_sales", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@p_company_code", SqlDbType.VarChar, 100).Value = objRSum.cter;
                command.Parameters.Add("@p_year", SqlDbType.Int).Value = objRSum.Year;
                command.Parameters.Add("@p_branch_code", SqlDbType.VarChar, 100).Value = objRSum.BranchCode;
                command.Parameters.Add("@p_se_id", SqlDbType.VarChar, 100).Value = objRSum.salesengineer_id;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtTargets);
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return dtTargets;
        }

        internal DataTable cust_target_vs_sales(Review objRSum)
        {
            DataTable dtTargets = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("tt_cust_target_vs_sales", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@p_company_code", SqlDbType.VarChar, 100).Value = objRSum.cter;
                command.Parameters.Add("@p_branch_code", SqlDbType.VarChar, 100).Value = objRSum.BranchCode;
                command.Parameters.Add("@p_se_id", SqlDbType.VarChar, 100).Value = objRSum.salesengineer_id;
                command.Parameters.Add("@p_customer_number", SqlDbType.VarChar, 100).Value = objRSum.customer_number;
                command.Parameters.Add("@p_year", SqlDbType.Int).Value = objRSum.Year;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtTargets);
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return dtTargets;
        }


        internal DataTable budgetstatus(Review objRSum)
        {
            DataTable dtTargets = new DataTable();
            SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("sp_budgetstatus", connection);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@CTR", SqlDbType.VarChar, 100,null).Value = objRSum.cter;
                command.Parameters.Add("@BRANCH", SqlDbType.VarChar, 100,null).Value = objRSum.BranchCode;
                command.Parameters.Add("@CUSTNUMBER", SqlDbType.VarChar, 100,null).Value = objRSum.customer_number;
                command.Parameters.Add("@SEID", SqlDbType.VarChar, 100,null).Value = objRSum.salesengineer_id;
                command.Parameters.Add("@USERID", SqlDbType.VarChar, 100,null).Value = objRSum.userid;
                SqlDataAdapter sqlDa = new SqlDataAdapter(command);
                sqlDa.Fill(dtTargets);
            }

            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                {
                    connection.Close();
                }
            }

            return dtTargets;
        }

        public DataTable GetProductGroups(int B_BUDGET_YEAR)
        {
            DataTable ProductGroups = new DataTable();
            SqlConnection connection = new SqlConnection(Convert.ToString(ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"]));
            try
            {
                connection.Open();
                SqlCommand cmd = new SqlCommand("LoadSplGroups", connection);
                cmd.CommandType = CommandType.StoredProcedure;

                cmd.Parameters.Add("@year", SqlDbType.Int).Value = B_BUDGET_YEAR;
                SqlDataAdapter da = new SqlDataAdapter(cmd);
               // da.SelectCommand = cmd;
                da.Fill(ProductGroups);
                
            }
            catch (Exception ex)
            {
                objFunc.LogError(ex);
            }
            finally
            {
                if (connection.State == ConnectionState.Open)
                    connection.Close();
            }
            return ProductGroups;
        }

    }


}
