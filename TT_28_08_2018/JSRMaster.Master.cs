﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TaegutecSalesBudget
{
    public partial class JSRMaster : System.Web.UI.MasterPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                SaveModules();
            }
        }

        /// <summary>
        /// Author:K.LakshmiBindu
        /// Desc: For Saving userlogs based on time with module info used by user
        /// Date : Feb 12, 2019
        /// </summary>
        public void SaveModules()
        {
            int i = 0;
            SqlConnection cn = null;

            try
            {
                cn = new SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings["csTaegutecSalesBudget"].ToString());
                SqlCommand cmd = new SqlCommand("sp_saveModuleLogs", cn);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                SqlParameter[] sp = new SqlParameter[]{
               new SqlParameter("@EngineerId",Convert.ToString(Session["UserId"])),
               new SqlParameter("@UniqueId",Convert.ToString(Session["UserGuid"])),
               new SqlParameter("@Module",Request.FilePath)
            };
                cmd.Parameters.AddRange(sp);
                cn.Open();
                i = cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
            }
            finally
            {
                if (cn.State == ConnectionState.Open)
                {
                    cn.Close();
                }
            }
        }
    }
}