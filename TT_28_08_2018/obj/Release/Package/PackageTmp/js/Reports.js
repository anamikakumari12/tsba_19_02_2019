﻿

$("body").delegate("'#MainContent_MonthlyTarget", "click", function () {

    $(this).find("td:first").each(function () {
        if ($(this).find('span').text() == "CUSTOMER TOTAL") {

            $(this).closest("tr").find("td").addClass("sum_total");
        }
    });
    $(this).find("td:first").each(function () {
        if ($(this).find('span').text() == "CHANNEL PARTNER TOTAL") {
            $(this).closest("tr").find("td").addClass("sum_total");
        }
    });
});

$(document).ready(function () {

    triggerPostGridLodedActions();
    $(window).resize(function () {
        
        var $fixedHeader = $("#header_fixed");
        $fixedHeader.html("");
        var $header = $('#MainContent_fiveyrsproducts > tbody >tr:first').clone();
        var $header1 = $('#MainContent_salesbylinegrid > tbody >tr:first').clone();
        var $header2 = $('#MainContent_salesbyfamilygrid > tbody >tr:first').clone();
        var $header3 = $('#MainContent_salesbyapp > tbody >tr:first').clone();
        var $header4 = $('#MainContent_salesbycustomer > tbody >tr:first').clone();



        //var width1 = $('#MainContent_fiveyrsproducts').width();
        //var width2 = $('#MainContent_salesbylinegrid').width();
        //var width3 = $('#MainContent_salesbyfamilygrid').width();
        //var width4 = $('#MainContent_salesbyapp').width();
        //var width5 = $('#MainContent_salesbycustomer').width();
        //var width1 = null;
        //if ($('#MainContent_fiveyrsproducts').is(':visible')) {
        //    width1 = $('#MainContent_fiveyrsproducts').width();
        //}
        //else {
        //    var tempele = $("#MainContent_fiveyrsproducts").closest("#prdgroup");
        //    $(tempele).show()
        //    width1 = $('#MainContent_fiveyrsproducts').width();
        //    $(tempele).hide();
        //}
        var width1 = resizewidth('#MainContent_fiveyrsproducts', '#prdgroup');
        var width2 = resizewidth('#MainContent_salesbylinegrid', '#linegrid');
        var width3 = resizewidth('#MainContent_salesbyfamilygrid', '#familygrid');
        var width4 = resizewidth('#MainContent_salesbyapp', '#salesappgrid');
        var width5 = resizewidth('#MainContent_salesbycustomer', '#slsbycustomergrid');
        $fixedHeader.append($header);
        $fixedHeader.append($header1);
        $fixedHeader.append($header2);
        $fixedHeader.append($header3);
        $fixedHeader.append($header4);

        $fixedHeader.show();
        $header.hide();
        $header1.hide();
        $header2.hide();
        $header3.hide();
        $header4.hide();
        $(window).bind("scroll", function () {
            var tableOffset = 150;

            var tab2ht = $("#tab2").offset().top;
            var tab3ht = $("#tab3").offset().top;
            var tab4ht = $("#tab4").offset().top;
            var tab5ht = $("#tab5").offset().top;

            var offset = $(this).scrollTop() + 1;
            // alert(width2);

            if (offset >= tableOffset && offset < tab2ht) {

                $('#header_fixed').width(width1);
                $header.show();
            }
            else {
                $header.hide();
            }
            if (offset >= tab2ht && offset < tab3ht) {
                $('#header_fixed').width(width2);
                $header1.show();
            }
            else {
                $header1.hide();
            }
            if (offset >= tab3ht && offset < tab4ht) {
                $('#header_fixed').width(width3);
                $header2.show();
            }
            else {
                $header2.hide();
            }
            if (offset > tab4ht && offset < tab5ht) {
                $('#header_fixed').width(width4);
                $header3.show();
            }
            else {
                $header3.hide();
            }
            if (offset > tab5ht) {
                $('#header_fixed').width(width5);
                $header4.show();
            }
            else {
                $header4.hide();
            }


        });
    });

});

/**
*
*/
function resizewidth(elementId, parentGroupId) {
    
    var width1 = 0;
    if ($(elementId).is(':visible')) {
        width1 = $(elementId).width();
    }
    else {
        var tempele = $(elementId).closest(parentGroupId);
        $(tempele).show()
        width1 = $(elementId).width();
        $(tempele).hide();
    }
    return width1;
}
function triggerPostGridLodedActions() {
    $('#collapsebtn').click(function () {

        var attr = $('#product_image').attr('src');
        //var imgsrc=images/button_plus.gif;
        $("#prdgroup").slideToggle();
        if (attr == "images/button_minus.gif") {
            $("#product_image").attr("src", "images/button_plus.gif");
        } else {
            $("#product_image").attr("src", "images/button_minus.gif");
        }


    });
    $('#collapseline').click(function () {
        var attr = $('#linegrid_image').attr('src');
        $("#linegrid").slideToggle();
        if (attr == "images/button_minus.gif") {
            $("#linegrid_image").attr("src", "images/button_plus.gif");
        } else {
            $("#linegrid_image").attr("src", "images/button_minus.gif");
        }

    });
    $('#collapsefamily').click(function () {
        var attr = $('#familygrid_image').attr('src');
        $("#familygrid").slideToggle();
        if (attr == "images/button_minus.gif") {
            $("#familygrid_image").attr("src", "images/button_plus.gif");
        } else {
            $("#familygrid_image").attr("src", "images/button_minus.gif");
        }

    });
    $('#collapse_app').click(function () {
        var attr = $('#salesapp_img').attr('src');
        $("#salesappgrid").slideToggle();
        if (attr == "images/button_minus.gif") {
            $("#salesapp_img").attr("src", "images/button_plus.gif");
        } else {
            $("#salesapp_img").attr("src", "images/button_minus.gif");
        }

    });
    $('#cust_app').click(function () {

        var attr = $('#slsbycust_image').attr('src');
        $("#slsbycustomergrid").slideToggle();
        if (attr == "images/button_minus.gif") {
            $("#slsbycust_image").attr("src", "images/button_plus.gif");
        } else {
            $("#slsbycust_image").attr("src", "images/button_minus.gif");
        }

    });
    var $fixedHeader = $("#header_fixed");
    $fixedHeader.html("");
    var $header = $('#MainContent_fiveyrsproducts > tbody >tr:first').clone();
    var $header1 = $('#MainContent_salesbylinegrid > tbody >tr:first').clone();
    var $header2 = $('#MainContent_salesbyfamilygrid > tbody >tr:first').clone();
    var $header3 = $('#MainContent_salesbyapp > tbody >tr:first').clone();
    var $header4 = $('#MainContent_salesbycustomer > tbody >tr:first').clone();
    var width1 = $('#MainContent_fiveyrsproducts').width();
    var width2 = $('#MainContent_salesbylinegrid').width();
    var width3 = $('#MainContent_salesbyfamilygrid').width();
    var width4 = $('#MainContent_salesbyapp').width();
    var width5 = $('#MainContent_salesbycustomer').width();
    $fixedHeader.append($header);
    $fixedHeader.append($header1);
    $fixedHeader.append($header2);
    $fixedHeader.append($header3);
    $fixedHeader.append($header4);

    $fixedHeader.show();
    $header.hide();
    $header1.hide();
    $header2.hide();
    $header3.hide();
    $header4.hide();
    $(window).bind("scroll", function () {
        var tableOffset = 150;

        var tab2ht = $("#tab2").offset().top;
        var tab3ht = $("#tab3").offset().top;
        var tab4ht = $("#tab4").offset().top;
        var tab5ht = $("#tab5").offset().top;

        var offset = $(this).scrollTop() + 1;

        //alert(offset);
        //alert(tableOffset);
        if (offset >= tableOffset && offset < tab2ht) {
            $('#header_fixed').width(width1);
            $header.show();
        }
        else {
            $header.hide();
        }
        if (offset >= tab2ht && offset < tab3ht) {
            $('#header_fixed').width(width2);
            $header1.show();
        }
        else {
            $header1.hide();
        }
        if (offset >= tab3ht && offset < tab4ht) {
            $('#header_fixed').width(width3);
            $header2.show();
        }
        else {
            $header2.hide();
        }
        if (offset > tab4ht && offset < tab5ht) {
            $('#header_fixed').width(width4);
            $header3.show();
        }
        else {
            $header3.hide();
        }
        if (offset > tab5ht) {
            $('#header_fixed').width(width5);
            $header4.show();
        }
        else {
            $header4.hide();
        }


    });

    $('#MainContent_ddlCustomerList').change(function () {

        var ddlslectedText = $("#MainContent_ddlCustomerList option:selected").val();
        $("#MainContent_ddlCustomerNumber").val(ddlslectedText);
    });
    $('#MainContent_ddlCustomerNumber').change(function () {

        var ddlslectedText = $("#MainContent_ddlCustomerNumber").val();
        $("#MainContent_ddlCustomerList").val(ddlslectedText);
    });
    $("#familygrid table tr:last td").each(function () { $(this).removeClass(); $(this).addClass("color_total") });
    $("#linegrid table tr:last td").each(function () { $(this).removeClass(); $(this).addClass("color_total") });
    $("#MainContent_salesbyapp tr:last td").each(function () { $(this).removeClass(); $(this).addClass("sum_total") });
    $("#linegrid table tr:last td").each(function () { $(this).removeClass(); $(this).addClass("color_total") });

    $("#slsbycustomergrid tr:last td").each(function () { $(this).removeClass(); $(this).addClass("color_total") })
    $("#MainContent_salesbyapp tr:last td").each(function () { $(this).removeClass(); $(this).addClass("color_total"); })

    $('#MainContent_salesbycustomer tr').each(function () {

        $(this).find("td:first").each(function () {
            if ($(this).find('span').text() == "CUSTOMER TOTAL") {
                $(this).closest("tr").find("td").addClass("sum_total");
            }
        });
        $(this).find("td:first").each(function () {
            if ($(this).find('span').text() == "CHANNEL PARTNER TOTAL") {
                $(this).closest("tr").find("td").addClass("sum_total");
            }
        });
    });
    $('#MainContent_salesbycustomer tr').each(function () {

        $(this).find("td:first").each(function () {
            if ($(this).find('span').text() == "CUSTOMER TOTAL") {
                $(this).closest("tr").find("td").addClass("sum_total");
            }
        });
        $(this).find("td:first").each(function () {
            if ($(this).find('span').text() == "CHANNEL PARTNER TOTAL") {
                $(this).closest("tr").find("td").addClass("sum_total");
            }
        });
        $(this).find("td:first").each(function () {
            if ($(this).find('span').text() == "BRANCH TOTAL") {
                $(this).closest("tr").find("td").addClass("grand_total");
            }
        });
        $(this).find("td:first").each(function () {
            if ($(this).find('span').text() == "GRAND TOTAL") {
                $(this).closest("tr").find("td").addClass("grand_total");
            }
        });
    });

    $('#MainContent_MonthlyTarget tr').each(function () {

        $(this).find("td:first").each(function () {
            if ($(this).find('span').text() == "CUSTOMER TOTAL") {

                $(this).closest("tr").find("td").addClass("sum_total");
            }
        });
        $(this).find("td:first").each(function () {
            if ($(this).find('span').text() == "CHANNEL PARTNER TOTAL") {
                $(this).closest("tr").find("td").addClass("sum_total");
            }
        });
    });

    function headergrid() {

        var $fixedHeader = $("#header_fixed");
        $fixedHeader.html("");
        var $header = $('#MainContent_fiveyrsproducts > tbody >tr:first').clone();
        var $header2 = $('#MainContent_salesbyapp > tbody >tr:first').clone();
        var $header3 = $('#MainContent_salesbycustomer > tbody >tr:first').clone();
        var width1 = $('#MainContent_fiveyrsproducts').width();
        var width2 = $('#MainContent_salesbyapp').width();
        var width3 = $('#MainContent_salesbycustomer').width();
        $fixedHeader.append($header);
        $fixedHeader.append($header2);
        $fixedHeader.append($header3);
        $fixedHeader.show();
        $header.hide();
        $header2.hide();
        $header3.hide();

        $(window).bind("scroll", function () {
            var tableOffset = 150;

            var tab4ht = $("#tab4").offset().top;
            var tab5ht = $("#tab5").offset().top;

            var offset = $(this).scrollTop() + 1;

            //alert(offset);
            //alert(tableOffset);
            if (offset >= tableOffset && offset < tab4ht) {
                $('#header_fixed').width(width1);
                $header.show();
            }
            else {
                $header.hide();
            }
            if (offset > tab4ht && offset < tab5ht) {
                $('#header_fixed').width(width2);
                $header2.show();
            }
            else {
                $header2.hide();
            }
            if (offset > tab5ht) {
                $('#header_fixed').width(width3);
                $header3.show();
            }
            else {
                $header3.hide();
            }


        });
    }



}

function alertClickFilter() {
    alert("Please click on Filter to view the result data");
}


