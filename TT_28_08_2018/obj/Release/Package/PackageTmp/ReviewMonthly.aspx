﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReviewMonthly.aspx.cs" Inherits="TaegutecSalesBudget.ReviewMonthly" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
      <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <%--  <link href="css/sol.css" rel="stylesheet" />
    <script src="js/sol.js"></script>--%>
    <script src="js/charts.js"></script>
    <script src="js/jquery.dataTables.min.js"></script>
    <link href="css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="css/buttons.dataTables.min.css" rel="stylesheet" />
    <script src="js/amcharts/amcharts.js"></script>
    <script src="js/amcharts/serial.js"></script>
    <script src="js/amcharts/plugins/dataloader/dataloader.min.js"></script>
    <script src="js/amcharts/plugins/exports/export.min.js"></script>
    <link rel="stylesheet" href="js/amcharts/plugins/exports/export.css" type="text/css" media="all"/>
    <script src="js/amcharts/plugins/exports/libs/FileSaver.min.js"></script>
    <script src="js/amcharts/plugins/exports/libs/fabric.min.js"></script>
    <script src="js/amcharts/plugins/exports/libs/jszip.min.js"></script>
        <script src="js/jquery.sumoselect.min.js"></script>
    <link href="css/sumoselect.css" rel="stylesheet" />
    <script type="text/javascript">
            function bindGridView() {
                var head_content = $('#MainContent_grdviewAllValues tr:first').html();
                $('#MainContent_grdviewAllValues').prepend('<thead></thead>')
                $('#MainContent_grdviewAllValues thead').html('<tr>' + head_content + '</tr>');
                $('#MainContent_grdviewAllValues tbody tr:first').hide();
                $('#MainContent_grdviewAllValues').DataTable(
                         {
                             "info": false,
                             "order": [],
                             "columnDefs": [{ orderable: false, targets: [0] }]
                         });
            }
         function triggerPostGridLodedActions() {
                bindGridView();
                var RoleID = '<%=Session["RoleId"].ToString()%>';
                console.log("Roleid=" + RoleID);
                if (RoleID == "HO" || RoleID == "TM") {
                    $(<%=BranchList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true,search:true});
                  
                    $(<%=SalesEngList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true, search: true });
                 
                    $(<%=CustNameList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true, search: true });
                  
                 
                    $(<%=CustNumList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true, search: true });
                  
                }
                    if (RoleID == "BM") {
                        $(<%=SalesEngList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true,search:true});
                        $(<%=CustNameList.ClientID%>).SumoSelect({  okCancelInMulti: true, selectAll: true,search:true});
                        $(<%=CustNumList.ClientID%>).SumoSelect({  okCancelInMulti: true, selectAll: true,search:true});
                    }

                    if (RoleID == "SE") {
                        $(<%=CustNameList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true, search: true });
                        $(<%=CustNumList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true, search: true });
                    }
                    else {
                    }

             $(<%=ProductGrpList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true});
             $(<%=ProductFamilyList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true, search: true });
             $(<%=ApplicationList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true, search: true });
               
                amchartvalsave();
                amchartqtysave();
                    Value_or_Qty_Change();

                    $('#product_image').unbind('click').bind('click', function (e) {
                        var attr = $('#product_image').attr('src');
                        $("#MainContent_reportdrpdwns").slideToggle();
                        if (attr == "images/up_arrow.png") {
                            $("#product_image").attr("src", "images/down_arrow.png");
                        } else {
                            $("#product_image").attr("src", "images/up_arrow.png");
                        }
                    });
                }
            
           
   
            function Value_or_Qty_Change() {
                if ($("#MainContent_rbtn_value").is(':checked')) {
                    $("#MainContent_divgridchart").show();
                    $("#MainContent_divgridchartqty").hide();
                    //  var hdn = '<%=Session["hdnClick"]%>'
                    debugger;
                    var cd = document.getElementById("MainContent_chartdisplay");
                    var hdn = cd.value;

                    console.log("hidden filed vale "+hdn)
                  if (hdn == "1") {

                        //document.getElementById("AmchartVAL").style.display = "block";
                        document.getElementById("amchartvalsave").style.display = "block";
                        document.getElementById("amchartqtysave").style.display = "none";
                        //document.getElementById("AmchartQTY").style.display = "none";

                       
                   
              //      amchartvalsave()
                        //AmChartGraphVal()
                    }

                }
                if ($("#MainContent_rbtn_quantity").is(':checked')) {
                    $("#MainContent_divgridchart").hide();
                    $("#MainContent_divgridchartqty").show();
                    //var hdn = '<%=Session["hdnClick"]%>'
               //     console.log(hdn)
                    var cd = document.getElementById("MainContent_chartdisplay");
                    var hdn = cd.value;

                    if (hdn == "1") {
                        //document.getElementById("AmchartQTY").style.display = "block";
                        //document.getElementById("AmchartVAL").style.display = "none";
                        document.getElementById("amchartvalsave").style.display = "none";
                        document.getElementById("amchartqtysave").style.display = "block";
                   
                    
                    //   amchartqtysave();
                        //AmChartGraphQty()
                    }
                }
            }


        function amchartqtysave() {
            debugger;
            var year = $('#MainContent_hdnYear').val();
                var value = "Qty";
                $.ajax({
                    type: "POST",
                    url: "ReviewMonthly.aspx/AmchartGraphQty",
                    data: "",
                    contentType: "Application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) { 
                        tmp = msg.d;
                        var chart = AmCharts.makeChart("amchartqtysave",
                            {
                                "type": "serial",
                                "theme": "light",
                                "dataProvider": tmp,
                                "categoryField": "Heading",
                                "legend": {
                                    "horizontalGap": 10,
                                    "maxColumns": 3,
                                    "position": "top",
                                    "useGraphSettings": true,
                                    "markerSize": 16,
                                    "valueAlign": "left",
                                    "align": "center"
                                },
                                "chartCursor": {
                                    "categoryBalloonEnabled": false,
                                    "cursorAlpha": 0,
                                    "zoomable": false
                                },
                                "categoryAxis": {
                                    "gridPosition": "start",
                                    "axisAlpha": 0.8,
                                    "gridAlpha": 0
                                },
                                "valueAxes": [{
                                    "position": "left",
                                    "axisAlpha": 0.8,
                                    "gridAlpha": 0,
                                    "titleBold":true
                                }],

                                "graphs": [{
                                   
                                    "valueField": "YTDSALE CurrentYear",
                                    "title": "YTDSALE "+year,
                                    "fillAlphas": 1,
                                    "lineAlpha": 0.1,
                                    "type": "column",
                                    "topRadius": 0.8,
                                    "labelText": "[[value]]",
                                    "labelPosition": "top"
                                },
                                {
                                    "fillAlphas": 1,
                                    "lineAlpha": 0.1,
                                    "type": "column",
                                    "topRadius": 0.8,
                                    "colorField": "#002a37",
                                    "valueField": "YTDPLAN CurrentYear",
                                    "title": "YTDPLAN "+year,
                                    "labelText": "[[value]]"
                                },
                                  {
                                      "fillAlphas": 1,
                                      "lineAlpha": 0.1,
                                      "type": "column",
                                      "topRadius": 0.8,
                                      "valueField": "YTDSALE ActualYear",
                                      "labelText": "[[value]]",
                                      "title": "YTDSALE "+(parseInt(year)-1),
                                      "labelPosition": "bottom",
                                  }
                                ],
                                "depth3D": 40,
                                "angle": 30,
                                "export": {
                                    "enabled": true,
                                 
                                }
                                , "listeners": [{
                                    "event": "drawn",
                                    "method": addLegendLabel
                                }, {
                                    "event": "rendered",
                                    "method": handleRender
                                }]

                            });
                        function addLegendLabel(e) {
                            var title = document.createElement("div");
                            title.innerHTML = "Summary of Sales Budget Monthly";
                            title.className = "legend-title";
                            e.chart.legendDiv.appendChild(title)
                        }
                        function handleRender(e) {
                            debugger;
                            e.chart["export"].capture({}, function () {

                                // SAVE TO PNG
                                this.toPNG({}, function (base64) {

                                    // We have now a Base64-encoded image data
                                    // which we can now transfer to server via AJAX
                                    // i.e. jQuery.post( "saveimage.php", { "data": base64 } )
                                    console.log(base64);
                                    debugger;
                                    $.ajax({
                                        type: "POST",
                                        url: "ReviewMonthly.aspx/ToPNG",
                                        data: "{'base64String':'" + base64 + "','s':'Qty'}",
                                        contentType: "Application/json; charset=utf-8",
                                        dataType: "json",
                                        success: function (msg) {},
                                        error: function (e) {
                                            
                                        }
                                    });



                                });
                            });
                        }
                        //   chart.fontSize = 17;
                        chart.dataProvider = AmCharts.parseJSON(tmp);
                        chart.validateData();
                    },
                    error: function (e) {
                        //console(e);
                    }
                });
            }
      
        function amchartvalsave() {
            debugger;
                var value = "val";
                var year = $('#MainContent_hdnYear').val();
              
                $.ajax({
                    type: "POST",
                    url: "ReviewMonthly.aspx/AmchartGraphVal",
                    data: "",
                    contentType: "Application/json; charset=utf-8",
                    dataType: "json",
                    success: function (msg) {
                        tmp = msg.d;
                        console.log(tmp);
                        var chart = AmCharts.makeChart("amchartvalsave",
                            {
                               
                                "type": "serial",
                                "theme": "light",
                                "dataProvider": tmp,
                                "categoryField": "Heading",
                                "legend": {

                                    "horizontalGap": 10,
                                    "maxColumns": 3,
                                    "position": "top",
                                    "useGraphSettings": true,
                                    "markerSize": 16,
                                    "valueAlign": "left",
                                    "align": "center"

                                    
                                },
                                "chartCursor": {
                                    "categoryBalloonEnabled": false,
                                    "cursorAlpha": 0,
                                    "zoomable": false
                                },
                                "categoryAxis": {
                                    "gridPosition": "start",
                                    "axisAlpha": 0.8,
                                    "gridAlpha": 0
                                },
                                "valueAxes": [{
                                    "position": "left",
                                    "axisAlpha": 0.8,
                                    "gridAlpha": 0,
                                    "titleBold": true
                                }],

                                "graphs": [{
                                    "valueField": "YTDSALE CurrentYear",
                                    "title": "YTDSALE "+year,
                                    "fillAlphas": 1,
                                    "lineAlpha": 0.1,
                                    "type": "column",
                                    "topRadius": 0.8,
                                    "labelText": "[[value]]",
                                    "labelPosition": "top"
                                   
                                },
                                {
                                    "fillAlphas": 1,
                                    "lineAlpha": 0.1,
                                    "type": "column",
                                    "topRadius": 0.8,
                                    "valueField": "YTDPLAN CurrentYear",
                                    "title": "YTDPLAN "+year,
                                    "labelText": "[[value]]"
                                },
                                  {
                                     
                                      "fillAlphas": 1,
                                      "lineAlpha": 0.1,
                                      "type": "column",
                                      "topRadius": 0.8,
                                      "valueField": "YTDSALE ActualYear",
                                      "labelText": "[[value]]",
                                      "title": "YTDSALE " + (parseInt(year) - 1),
                                      "labelPosition": "bottom"
                                  }],
                                "depth3D": 40,
                                "angle": 30,
                                "export":{ "enabled": true},
                                "listeners": [{
                                    "event": "drawn",
                                    "method": addLegendLabel
                                }, {
                                    "event": "rendered",
                                    "method": handleRender
                                }
                                ]

                    });
                      function addLegendLabel(e) {
                          var title = document.createElement("div");
                          title.innerHTML = "Summary Of Sales Budget Monthly";
                          title.className = "legend-title";
                          e.chart.legendDiv.appendChild(title)
                      }
                     
                      function handleRender(e) {
                          debugger;
                          e.chart["export"].capture({}, function () {

                              // SAVE TO PNG
                              this.toPNG({}, function (base64) {

                                  // We have now a Base64-encoded image data
                                  // which we can now transfer to server via AJAX
                                  // i.e. jQuery.post( "saveimage.php", { "data": base64 } )
                                  console.log(base64);
                                  debugger;
                                  $.ajax({
                                      type: "POST",
                                      url: "ReviewMonthly.aspx/ToPNG",
                                      data: "{'base64String':'" + base64 + "','s':'value'}",
                                      contentType: "Application/json; charset=utf-8",
                                      dataType: "json",
                                      success: function (msg) {  },
                                      error: function (e) {
                                         
                                      }
                                  });



                              });
                          });
                      }
                        chart.dataProvider = AmCharts.parseJSON(tmp);
                        chart.validateData();
                    },
                    error: function (e) {
                        //console(e);
                    }



                });


        }

            function hideTable() {
                debugger;
                //document.getElementById("AmchartVAL").style.display = "none";
                var id = document.getElementById("amchartvalsave");
                document.getElementById("amchartvalsave").style.display = "none";
                document.getElementById("amchartqtysave").style.display = "none";

                }
            
    </script>

    <style type="text/css">


.SumoSelect p {
  margin: 0;
    width: 200px;
}

.SumoSelect {
  width: 252px;
  
}

.SelectBox {
  padding: 5px 0px;
 /* margin-bottom:5px;*/
}
        
/* Filtering style */

.SumoSelect .hidden {
  display: none;
}

.SumoSelect .search-txt {
  display: none;
  outline: none;
}

.SumoSelect .no-match {
  display: none;
  padding: 6px;
}

.SumoSelect.open .search-txt {
  display: inline-block;
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  margin: 0;
  padding: 5px 8px;
  border: none;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  border-radius: 5px;
}

.SumoSelect.open>.search>span,
.SumoSelect.open>.search>label {
  visibility: hidden;
}


/*this is applied on that hidden select. DO NOT USE display:none; or visiblity:hidden; and Do not override any of these properties. */

.SelectClass,
.SumoUnder {
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  height: 100%;
  width: 100%;
  border: none;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
  filter: alpha(opacity=0);
  -moz-opacity: 0;
  -khtml-opacity: 0;
  opacity: 0;
}

.SelectClass {
  z-index: 1;
}

.SumoSelect > .optWrapper > .options li.opt label,
.SumoSelect > .CaptionCont,
.SumoSelect .select-all > label {
  user-select: none;
  -o-user-select: none;
  -moz-user-select: none;
  -khtml-user-select: none;
  -webkit-user-select: none;
  overflow-wrap:normal;
}

.SumoSelect {
  display:inline-block;
  position: relative;
  outline: none;
}

.SumoSelect:focus > .CaptionCont,
.SumoSelect:hover > .CaptionCont,
.SumoSelect.open > .CaptionCont {
  box-shadow: 0 0 2px #7799D0;
  border-color: #7799D0;
}

.SumoSelect > .CaptionCont {
  position: relative;
  border: 1px solid #A4A4A4;
  min-height: 14px;
  background-color: #fff;
  border-radius: 2px;
  margin: 0;
}

.SumoSelect > .CaptionCont > span {
  display: block;
  padding-right: 30px;
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
  cursor: default;
margin-left: 5px;
}


/*placeholder style*/

.SumoSelect > .CaptionCont > span.placeholder {
  color: #ccc;
  font-style: italic;
}

.SumoSelect > .CaptionCont > label {
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  width: 30px;
}

.SumoSelect > .CaptionCont > label > i {
  background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAANCAYAAABy6+R8AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3wMdBhAJ/fwnjwAAAGFJREFUKM9jYBh+gBFKuzEwMKQwMDB8xaOWlYGB4T4DA0MrsuapDAwM//HgNwwMDDbYTJuGQ8MHBgYGJ1xOYGNgYJiBpuEpAwODHSF/siDZ+ISBgcGClEDqZ2Bg8B6CkQsAPRga0cpRtDEAAAAASUVORK5CYII=');
  background-position: center center;
  width: 16px;
  height: 16px;
  display: block;
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  margin: auto;
  background-repeat: no-repeat;
  opacity: 0.8;
}

.SumoSelect > .optWrapper {
  display: none;
  z-index: 1000;
  top: 30px;
  width: 100%;
 
  position: absolute;
  left: 0;
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
  background: #fff;
  border: 1px solid #ddd;
  box-shadow: 2px 3px 3px rgba(0, 0, 0, 0.11);
  border-radius: 3px;
  overflow: visible;
  padding-bottom:0px;
  padding-right:0px;
  padding-left:0px;
  padding-top:0px;
}

.SumoSelect.open > .optWrapper {
  top: 35px;
  display: block;

}

.SumoSelect.open > .optWrapper.up {
  top: auto;
  bottom: 100%;
  margin-bottom: 5px;
}

.SumoSelect > .optWrapper ul {
  list-style: none;
  display: block;
  padding: 0;
  margin: 0;
  overflow: auto;
}

.SumoSelect > .optWrapper > .options {
  border-radius: 2px;
  position: relative;
  /*Set the height of pop up here (only for desktop mode)*/
 max-height: 250px;
  /*height*/
}

.SumoSelect > .optWrapper > .options li.group.disabled > label {
  opacity: 0.5;
}

.SumoSelect > .optWrapper > .options li ul li.opt {
  padding-left: 22px;
}

.SumoSelect > .optWrapper.multiple > .options li ul li.opt {
  padding-left: 50px;
}

.SumoSelect > .optWrapper.isFloating > .options {
  max-height: 100%;
  box-shadow: 0 0 100px #595959;
}

.SumoSelect > .optWrapper > .options li.opt {
  padding: 6px 6px;
  position: relative;
  border-bottom: 1px solid #f5f5f5;
}

.SumoSelect > .optWrapper > .options > li.opt:first-child {
  border-radius: 2px 2px 0 0;
}

.SumoSelect > .optWrapper > .options > li.opt:last-child {
  border-radius: 0 0 2px 2px;
  border-bottom: none;
}

.SumoSelect > .optWrapper > .options li.opt:hover {
  background-color: #E4E4E4;
}

.SumoSelect > .optWrapper > .options li.opt.sel {
  background-color: #a1c0e4;
  border-bottom: 1px solid #a1c0e4;
}

.SumoSelect > .optWrapper > .options li label {
  text-overflow: ellipsis;
  white-space: nowrap;
  overflow: hidden;
  display: block;
  cursor: pointer;
}

.SumoSelect > .optWrapper > .options li span {
  display: none;
}

.SumoSelect > .optWrapper > .options li.group > label {
  cursor: default;
  padding: 8px 6px;
  font-weight: bold;
}


/*Floating styles*/

.SumoSelect > .optWrapper.isFloating {
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  width: 90%;
  bottom: 0;
  margin: auto;
  max-height: 90%;
}


/*disabled state*/

.SumoSelect > .optWrapper > .options li.opt.disabled {
  background-color: inherit;
  pointer-events: none;
}

.SumoSelect > .optWrapper > .options li.opt.disabled * {
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=50)";
  /* IE 5-7 */
  filter: alpha(opacity=50);
  /* Netscape */
  -moz-opacity: 0.5;
  /* Safari 1.x */
  -khtml-opacity: 0.5;
  /* Good browsers */
  opacity: 0.5;
}


/*styling for multiple select*/

.SumoSelect > .optWrapper.multiple > .options li.opt {
  padding-left: 35px;
  cursor: pointer;
}
        
.multiple {
    padding: 8px 10px;
  /*  height: 300px !important;*/
    font-size: 12px;
    border: 1px solid #dadada;
}
.SumoSelect > .optWrapper.multiple > .options li.opt span,
.SumoSelect .select-all > span {
  position: absolute;
  display: block;
  width: 30px;
  top: 0;
  bottom: 0;
  margin-top:5px;

}

.SumoSelect > .optWrapper.multiple > .options li.opt span i,
.SumoSelect .select-all > span i {
  position: absolute;
  margin: auto;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
  width: 14px;
  height: 14px;
  border: 1px solid #AEAEAE;
  border-radius: 2px;
  box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.15);
  background-color: #fff;
}

.SumoSelect > .optWrapper > .MultiControls {
  display: inline;
  border-top: 1px solid #ddd;
  background-color: #fff;
  box-shadow: 0 0 2px rgba(0, 0, 0, 0.13);
  border-radius: 0 0 3px 3px;
}

.SumoSelect > .optWrapper.multiple.isFloating > .MultiControls {
  display: block;
  margin-top: 5px;
  position: absolute;
  bottom: 0;
  width: 100%;
  
}


.SumoSelect > .optWrapper.multiple.okCancelInMulti > .MultiControls {
  display: block;

 
}

.SumoSelect > .optWrapper.multiple.okCancelInMulti > .MultiControls > p {
 /* padding: 6px;*/
}

.SumoSelect > .optWrapper.multiple > .MultiControls > p {
  display: inline-block;
  cursor: pointer;
  /*padding: 12px;*/
  width: 50%;
  box-sizing: border-box;
  text-align: center;
}

.SumoSelect > .optWrapper.multiple > .MultiControls > p:hover {
  background-color: #f1f1f1;
  margin-bottom:10px;
}

.SumoSelect > .optWrapper.multiple > .MultiControls > p.btnOk {
  border-right: 1px solid #DBDBDB;
  border-radius: 0 0 0 3px;
}

.SumoSelect > .optWrapper.multiple > .MultiControls > p.btnCancel {
  border-radius: 0 0 3px 0;
 /* margin-bottom:5px;*/
}


/*styling for select on popup mode*/

.SumoSelect > .optWrapper.isFloating > .options li.opt {
  padding: 12px 6px;
}


/*styling for only multiple select on popup mode*/

.SumoSelect > .optWrapper.multiple.isFloating > .options li.opt {
  padding-left: 35px;
}

.SumoSelect > .optWrapper.multiple.isFloating {
/*padding-bottom: 43px;*/
}

.SumoSelect > .optWrapper.multiple > .options li.opt.selected span i,
.SumoSelect .select-all.selected > span i,
.SumoSelect .select-all.partial > span i {
  background-color: rgb(5, 130, 183);
  box-shadow: none;
  border-color: transparent;
  background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAGCAYAAAD+Bd/7AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABx0RVh0U29mdHdhcmUAQWRvYmUgRmlyZXdvcmtzIENTNXG14zYAAABMSURBVAiZfc0xDkAAFIPhd2Kr1WRjcAExuIgzGUTIZ/AkImjSofnbNBAfHvzAHjOKNzhiQ42IDFXCDivaaxAJd0xYshT3QqBxqnxeHvhunpu23xnmAAAAAElFTkSuQmCC');
  background-repeat: no-repeat;
  background-position: center center;
  padding:0px;
  margin-top:0px;
}


/*disabled state*/

.SumoSelect.disabled {
  opacity: 0.7;
  cursor: not-allowed;
}

.SumoSelect.disabled > .CaptionCont {
  border-color: #ccc;
  box-shadow: none;
}


/**Select all button**/

.SumoSelect .select-all {
  border-radius: 3px 3px 0 0;
  position: relative;
  border-bottom: 1px solid #ddd;
  background-color: #fff;
  padding: 8px 0 3px 35px;
 height: 30px;
  cursor: pointer;
}

.SumoSelect .select-all > label,
.SumoSelect .select-all > span i {
  cursor: pointer;
}

.SumoSelect .select-all.partial > span i {
  background-color: #ccc;
}


/*styling for optgroups*/

.SumoSelect > .optWrapper > .options li.optGroup {
  padding-left: 5px;
  text-decoration: underline;
}


        .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
            border: 1px solid #ddd;
            padding: 5px;
        }

        .height {
            text-space-collapse: collapse;
        }

        .HeadergridAll {
            background: #ebeef5;
            color: black;
            font-weight: 600;
            text-align: center !important;
            /*border-color:#ebeef5;*/
        }


        #AmchartQTY {
            width: 100%;
            height: 500px;
          /*  background-image: url('images/Back.png');*/
            background-repeat: no-repeat;
            background-size: cover;
        }
        .legend-title {
  font-family: Verdana;
  font-weight: bold;
   font-size: 15px;
  margin-left: 550px;

  margin-bottom:5px;
}
        #AmchartVAL {
            width: 100%;
            height: 500px;
        /*   background-image: url('images/Back.png');*/ 
            background-repeat: no-repeat;
            background-size: cover;
        }

        td {
            border-color: #ebeef5;
            background: #fff;
            text-align: right;
        }

        #MainContent_grdviewAllValues td:first-child {
            text-align: left;
        }

        #MainContent_grdviewAllQuantites td:first-child {
            text-align: left;
        }

        /*.HeadergridAll {
            background: #ebeef5;
            color: black;
            font-weight: 600;
            text-align: center !important;
        }*/

        .control_dropdown {
            width: 180px;
          height: 30px;
            border-radius: 4px!important;
        }

        .label {
            padding-top: 9px;
            width: 100%;
            color: black;
        }

        .control {
            padding-top: 2px;
        }
        #amchartqtysave, #amchartvalsave {
            width: 100%;
            height: 300px;
           background-image: url('images/Back.png');
            background-repeat: no-repeat;
            background-size: cover;
        }
        .SelectBox {
            padding-top: 9px;
            width: 180px!important;
            border-radius: 4px!important;
        }
        

        .btn.green {
            margin-top: 22px;
        }

        .amcharts-chart-div a {
            display: none !important;
        }
    </style>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    <asp:HiddenField ID="hdnSearch" runat="server" />
<asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360" EnableHistory="true" EnablePartialRendering="true" EnablePageMethods="true" EnableCdn="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1"  runat="server" UpdateMode="Conditional">
    
        <ContentTemplate>

    
            <div class="crumbs">
                <!-- Start : Breadcrumbs -->
                <ul id="breadcrumbs" class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a>Reports</a>
                    </li>
                    <li>Reviews</li>
                    <li class="current">Monthly Review</li>
                    <div>
                        <ul>
                            <li class="title_bedcrum" style="list-style: none;">MONTHLY REVIEW</li>
                        </ul>
                    </div>
                    <div>
                        <ul style="float: right; list-style: none; margin-top: -4px; width: 265px; margin-right: -5px;" class="alert alert-danger fade in">
                            <li>
                                <span style="margin-right: -1px; margin-left: -35px; vertical-align: text-bottom;">Val In Units</span>
                                <asp:RadioButton ID="rbtn_Units" OnCheckedChanged="Units_CheckedChanged" GroupName="customer" runat="server" AutoPostBack="true" />
                                <span style="margin-right: -1px; margin-left: 5px; vertical-align: text-bottom;">Val In '000</span>
                                <asp:RadioButton ID="rbtn_Thousand" OnCheckedChanged="Thousand_CheckedChanged"  GroupName="customer" runat="server" Checked="True" AutoPostBack="true"/>
                                <span style="margin-right: 0px; margin-left: 6px; vertical-align: text-bottom;">Val In Lakhs</span>
                                <asp:RadioButton ID="rbtn_Lakhs" OnCheckedChanged="Lakhs_CheckedChanged"  GroupName="customer" runat="server" AutoPostBack="true" />
                            </li>
                        </ul>
                    </div>
                </ul>
                <!-- End : Breadcrumbs -->
            </div>



            <div id="collapsebtn" class="row">
                <img id="product_image" src="images/up_arrow.png" style="margin-left: 46%;" />
            </div>

            <div class="row filter_panel" id="reportdrpdwns" runat="server">
                <div runat="server" id="cterDiv" visible="false">
                    <ul id="divCter" runat="server" class="btn-info rbtn_panel">
                        <li><span style="margin-right: 4px; vertical-align: text-bottom;">TAEGUTEC</span>
                            <asp:RadioButton ID="rdBtnTaegutec" Checked="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" AutoPostBack="true" onChange="hideTable();"/>
                            <span style="margin-right: 4px; margin-left: 4px; vertical-align: text-bottom;">DURACARB</span>
                            <asp:RadioButton ID="rdBtnDuraCab"  OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" AutoPostBack="true" onChange="hideTable();"/>
                        </li>
                    </ul>
                </div>

                <div class="col-md-2 control" runat="server" id="divBranch">
                    <label class="label">BRANCH</label>
                   
                  <asp:ListBox runat="server" CssClass="control_dropdown" ID="BranchList" SelectionMode="Multiple" OnSelectedIndexChanged="BranchList_SelectedIndexChanged" AutoPostBack="true">
                        
                    </asp:ListBox>

                    <%--  <asp:DropDownList ID="ddlBranchList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlBranchList_SelectedIndexChanged"
                                CssClass="form-control select2" Width="230px">
                            </asp:DropDownList>--%>
                  
                </div>
                <input type="hidden" id="blist" runat="server"/>
                <div class="col-md-2 control" runat="server" id="divSE">
                    <label class="label">SALES ENGINEER </label>
                    <asp:ListBox runat="server" CssClass="control_dropdown" ID="SalesEngList" SelectionMode="Multiple" OnSelectedIndexChanged="SalesEngList_SelectedIndexChanged" AutoPostBack="true">
                       
                    </asp:ListBox>
                      <input type="hidden" id="slist" runat="server"/>
                    <%--<asp:DropDownList ID="ddlSalesEngineerList" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlSalesEngineerList_SelectedIndexChanged"
                                CssClass="form-control" Width="230px">
                                <asp:ListItem>--SELECT SALES ENGINEER--</asp:ListItem>
                            </asp:DropDownList>--%>
                </div>

                <div class="col-md-2 control">
                    <label class="label">CUSTOMER TYPE </label>
                    <asp:DropDownList ID="ddlcustomertype" runat="server" CssClass="control_dropdown" AutoPostBack="true" OnSelectedIndexChanged="ddlcustomertype_SelectedIndexChanged">
                        <asp:ListItem Text="ALL" Value="ALL" />
                        <asp:ListItem Text="CUSTOMER" Value="C" />
                        <asp:ListItem Text="CHANNEL PARTNER" Value="D" />
                    </asp:DropDownList>
                </div>
                 <asp:Label  ID="Sctype" runat="server" ></asp:Label>

                <div class="col-md-2 control">
                    <label class="label">CUSTOMER NAME </label>
                    <%-- <asp:DropDownList ID="ddlCustomerList" runat="server"
                                CssClass="form-control" Width="230px">
                                <asp:ListItem>--SELECT CUSTOMER --</asp:ListItem>
                            </asp:DropDownList>--%>
                    <asp:ListBox runat="server" CssClass="control_dropdown" ID="CustNameList" SelectionMode="Multiple"  class="search-txt" OnSelectedIndexChanged="CustNameList_SelectedIndexChanged" AutoPostBack="true"></asp:ListBox>
                    <input type="hidden" id="lcna" runat="server"/>
                </div>

                <div class="col-md-2 control">
                    <label class="label ">CUSTOMER NUMBER</label>
                    <%-- <asp:DropDownList ID="ddlCustomerNumber" runat="server" CssClass="form-control" Width="230px">
                                <asp:ListItem>SELECT CUSTOMER NUMBER</asp:ListItem>
                            </asp:DropDownList>--%>
                    <asp:ListBox runat="server" CssClass="control_dropdown" ID="CustNumList" SelectionMode="Multiple" class="search-txt" OnSelectedIndexChanged="CustNumList_SelectedIndexChanged" AutoPostBack="true"></asp:ListBox>
                </div>
                <input type="hidden" id="lcnu" runat="server"/>
                <div class="col-md-2 control">
                    <label class="label ">PRODUCT GROUP</label>
                    <%-- <asp:DropDownList ID="ddlProductGroup" runat="server" CssClass="form-control" Width="230px" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="ALL" Text="ALL"></asp:ListItem>
                                <asp:ListItem Value="GOLD" Text="GOLD"></asp:ListItem>
                                <asp:ListItem Value="BB" Text="BB"></asp:ListItem>
                                <asp:ListItem Value="5YRS" Text="5YRS"></asp:ListItem>
                                <asp:ListItem Value="SPC" Text="SPC"></asp:ListItem>
                                <asp:ListItem Value="TOP" Text="TOP"></asp:ListItem>
                            </asp:DropDownList>--%>
                    <input type="hidden" id="flag" runat="server" />
                    <asp:ListBox runat="server" CssClass="control_dropdown" ID="ProductGrpList" SelectionMode="Multiple" OnSelectedIndexChanged="ProductGrpList_SelectedIndexChanged" AutoPostBack="true">
                      <%-- <asp:ListItem Value="GOLD" Text="GOLD"></asp:ListItem>
                        <asp:ListItem Value="BB" Text="BB"></asp:ListItem>
                        <asp:ListItem Value="5YRS" Text="5YRS"></asp:ListItem>
                        <asp:ListItem Value="SPC" Text="SPC"></asp:ListItem>
                        <asp:ListItem Value="TOP" Text="TOP"></asp:ListItem>--%>
                    </asp:ListBox>
               
                </div>

                <div class="col-md-2 control">
                    <label class="label ">PRODUCT FAMILY</label>
                    <%-- <asp:DropDownList ID="ddlProductFamliy" runat="server" CssClass="form-control" Width="230px" AutoPostBack="True" OnSelectedIndexChanged="ddlProductFamliy_SelectedIndexChanged">
                                <asp:ListItem>SELECT FAMILY</asp:ListItem>
                            </asp:DropDownList>--%>
                    <asp:ListBox runat="server" CssClass="control_dropdown" ID="ProductFamilyList" SelectionMode="Multiple" AutoPostBack="true" OnSelectedIndexChanged="ProductFamilyList_SelectedIndexChanged"></asp:ListBox>
                </div>
              <input type="hidden" id="pfl" runat="server"/>
                <div class="col-md-2 control">
                    <label class="label">APPLICATION</label>
                    <%--  <asp:DropDownList ID="ddlApplication" runat="server" CssClass="form-control" Width="230px">
                                <asp:ListItem>SELECT Application</asp:ListItem>
                            </asp:DropDownList>--%>
                    <asp:ListBox runat="server" CssClass="control_dropdown" ID="ApplicationList" SelectionMode="Multiple" AutoPostBack="true" OnSelectedIndexChanged="ApplicationList_SelectedIndexChanged"></asp:ListBox>
                </div>
                <input type="hidden" runat="server" id="apl"/>
                <div class="col-md-4 ">
                    <asp:Button ID="reports" runat="server" Text="FILTER" CssClass="btn green"  Style="top: -5px !important;" OnClick="reports_Click"/>
                    <asp:Button ID="btnExport" Text="Export" runat="server" Visible="false" CssClass="btn green" Style="top: -5px !important;" OnClick="ExportToImage"/>
                    <label id="alertmsg" style="display: none; font-weight: bold; color: #0582b7;">Now click on Filter to view results</label>
                </div>
            </div>

            <br />
            <div id="div1" runat="server">
                <ul style="margin-left: -20px; width: 150px" class="btn-info rbtn_panel">
                    <li><span style="margin-right: 4px; vertical-align: text-bottom;">VALUE</span>
                        <asp:RadioButton ID="rbtn_value" Checked="true" GroupName="ByValueorQty" runat="server" onclick="Value_or_Qty_Change('value');" />
                        <span style="margin-right: 4px; margin-left: 4px; vertical-align: text-bottom;">QTY</span>
                        <asp:RadioButton ID="rbtn_quantity" GroupName="ByValueorQty" runat="server" onclick="Value_or_Qty_Change('Qty');"/>
                    </li>
                </ul>
            </div>
            <br />

            <div class="row" runat="server" id="divgridchart" visible="false">
                <div class="row">
                    <div style="float: left; padding-right: 10px; padding-left: 10px; width: 100%;">
                        <asp:GridView ID="grdviewAllValues" runat="server" ViewStateMode="Enabled" class="table table-bordered " AutoGenerateColumns="False" ShowHeader="false" Width="100%">
                            <Columns>
                                <asp:TemplateField HeaderStyle-CssClass="HeadergridAll">
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblTitle" Text='<%# Eval("title") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-CssClass="HeadergridAll">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lbljan" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("jan").ToString() == "0")? "NA" : Eval("jan") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-CssClass="HeadergridAll">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblfeb" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("feb").ToString() == "0")? "NA" : Eval("feb") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-CssClass="HeadergridAll">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblmar" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("mar").ToString() == "0")? "NA" : Eval("mar") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-CssClass="HeadergridAll">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblapr" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("apr").ToString() == "0")? "NA" : Eval("apr") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-CssClass="HeadergridAll">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblmay" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("may").ToString() == "0")? "NA" :  Eval("may") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-CssClass="HeadergridAll">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lbljun" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("jun").ToString() == "0")? "NA" : Eval("jun") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-CssClass="HeadergridAll">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lbljul" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("jul").ToString() == "0")? "NA" : Eval("jul") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-CssClass="HeadergridAll">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblAug" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("aug").ToString() == "0")? "NA" : Eval("aug") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-CssClass="HeadergridAll">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblsep" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("sep").ToString() == "0")? "NA" : Eval("sep") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-CssClass="HeadergridAll">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lbloct" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("oct").ToString() == "0")? "NA" : Eval("oct") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-CssClass="HeadergridAll">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblnov" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("nov").ToString() == "0")? "NA" : Eval("nov") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-CssClass="HeadergridAll">
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lbldec" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("dec").ToString() == "0")? "NA" : Eval("dec") %>'></asp:Label>
                                        <asp:Label runat="server" ID="lblFlag" Text='<%# Eval("flag") %>' Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
                
                        <asp:Chart ID="Chart2" runat="server" Width="1307px" Visible="False" ViewStateMode="Enabled" Style="max-width: 100% !important" BackColor="#E1E1E1">
                        <Titles>
                                <asp:Title Text="Summary of Sales Budget Monthly " Font="Times New Roman, 18pt, style=Bold, Italic"></asp:Title>

                             
       
                            </Titles>
                            <Legends>
                                <asp:Legend Alignment="Center" Docking="Top" IsTextAutoFit="true" Name="Legend2" LegendStyle="Row"/>
                            </Legends>
                            <Series>
                                <asp:Series Name="YTD SALE" ShadowOffset="1" Enabled="True" LabelForeColor="White" LabelAngle="-90"
                                    CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A"  ChartType="Column">
                                </asp:Series>
                                <asp:Series Name="YTD PLAN" ShadowOffset="1" ChartType="Column" LabelBorderWidth="1"  ></asp:Series>
                                <asp:Series Name="Series4"  ShadowOffset="1" ChartType="Point"></asp:Series>
                                <asp:Series Name="YTD SALE PREVIOUS YEAR" ShadowOffset="1" ChartType="Column"></asp:Series>
                                <asp:Series Name="Series5"  ShadowOffset="1" ChartType="Point"></asp:Series>
                            </Series>
                            <ChartAreas>
                                <asp:ChartArea Name="ChartArea1" BackColor="#ACD1E9">
                                    <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap">
                                        <MajorGrid LineWidth="0" />
                                        <LabelStyle Font="Verdana, 8.25pt" />
                                    </AxisX>
                                    <AxisY>
                                        <MajorGrid LineWidth="0" />
                                    </AxisY>
                                </asp:ChartArea>
                            </ChartAreas>
                            <BorderSkin BackColor="Transparent" PageColor="Transparent"
                                SkinStyle="Emboss" />
                        </asp:Chart>
                   
            
            </div>

            <div class="row" runat="server" id="divgridchartqty" visible="false">
                <div class="row">
                    <div style="float: left; padding-right: 10px; padding-left: 10px; width: 100%;">
                        <asp:GridView ID="grdviewAllQuantites" runat="server" ViewStateMode="Enabled" class="table table-bordered " AutoGenerateColumns="False" ShowHeader="false" Width="100%">
                            <Columns>
                                <asp:TemplateField>
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblTitle" Text='<%# Eval("title") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lbljan" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("jan").ToString() == "0")? "NA" : Eval("jan") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblfeb" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("feb").ToString() == "0")? "NA" : Eval("feb") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblmar" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("mar").ToString() == "0")? "NA" : Eval("mar") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblapr" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("apr").ToString() == "0")? "NA" : Eval("apr") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblmay" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("may").ToString() == "0")? "NA" :  Eval("may") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lbljun" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("jun").ToString() == "0")? "NA" : Eval("jun") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lbljul" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("jul").ToString() == "0")? "NA" : Eval("jul") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblAug" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("aug").ToString() == "0")? "NA" : Eval("aug") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblsep" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("sep").ToString() == "0")? "NA" : Eval("sep") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lbloct" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("oct").ToString() == "0")? "NA" : Eval("oct") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lblnov" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("nov").ToString() == "0")? "NA" : Eval("nov") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lbldec" Text='<%# (Eval("flag").ToString()=="YTDSALE CurrentYear" && Eval("dec").ToString() == "0")? "NA" : Eval("dec") %>'></asp:Label>
                                        <asp:Label runat="server" ID="lblFlag" Text='<%# Eval("flag") %>' Visible="false"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
              <asp:Chart ID="Chart3" runat="server" Width="1307px" ViewStateMode="Enabled" Style="max-width: 100% !important" BackColor="#E1E1E1">
                            <Titles>
                                <asp:Title Text="Summary of Sales Budget Monthly " Font="Times New Roman, 18pt, style=Bold, Italic"></asp:Title>
                            </Titles>
                            <Legends>
                                <asp:Legend Alignment="Center" Docking="Top" IsTextAutoFit="true" Name="Legend2" LegendStyle="Row" />
                            </Legends>
                            <Series>
                                <asp:Series Name="YTD SALE" ShadowOffset="1" Enabled="True" LabelForeColor="White" LabelAngle="-90"
                                    CustomProperties="LabelStyle= Bottom, DrawingStyle=Cylinder" Color="#FAA43A" ChartType="Column">
                                </asp:Series>
                                <asp:Series Name="YTD PLAN" ShadowOffset="1" ChartType="Line" LabelBorderWidth="1"></asp:Series>
                                <asp:Series Name="Series4"  ShadowOffset="1" ChartType="Point"></asp:Series>
                                <asp:Series Name="YTD SALE PREVIOUS YEAR" ShadowOffset="1" ChartType="Line"></asp:Series>
                                <asp:Series Name="Series5"  ShadowOffset="1" ChartType="Point"></asp:Series>
                            </Series>
                            <ChartAreas>
                                <asp:ChartArea Name="ChartArea1" BackColor="#ACD1E9">
                                    <AxisX LabelAutoFitStyle="LabelsAngleStep45,LabelsAngleStep90,WordWrap">
                                        <MajorGrid LineWidth="0" />
                                        <LabelStyle Font="Verdana, 8.25pt" />
                                    </AxisX>
                                    <AxisY>
                                        <MajorGrid LineWidth="0" />
                                    </AxisY>
                                </asp:ChartArea>
                            </ChartAreas>
                            <BorderSkin BackColor="Transparent" PageColor="Transparent"
                                SkinStyle="Emboss" />
                        </asp:Chart>
            </div>

            <asp:HiddenField id="hdnYear" runat="server"/>
            <br />
            <br />
            
            <input type="hidden" runat="server" id="chartdisplay" />
            <div id="amchartqtysave" style="display: none;"></div>
            <div id="amchartvalsave"style="display: none;"></div>
 
         <%--<asp:Button runat="server" OnClientClick="return  handleRender()" />--%>
       </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="BranchList" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="SalesEngList" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlcustomertype" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ProductFamilyList" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="reports" EventName="Click" />
            <asp:PostBackTrigger ControlID="btnExport" />
           

 </Triggers>
    </asp:UpdatePanel>
   <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">

                <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff">Please wait</span>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
 


</asp:Content>
