﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReviewWeekly.aspx.cs" Inherits="TaegutecSalesBudget.ReviewWeekly" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script src="js/jquery.dataTables.min.js"></script>
    <link href="css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="css/buttons.dataTables.min.css" rel="stylesheet" />
    <script>
        $(document).ready(function () {
            $('#MainContent_ddlBranchList').change(function () { $('#MainContent_export').css("display", "none") });
            $('#product_image').unbind('click').bind('click', function (e) {
                var attr = $('#product_image').attr('src');
                $("#MainContent_MainContent_dfltreportform").slideToggle();
                if (attr == "images/up_arrow.png") {
                    $("#product_image").attr("src", "images/down_arrow.png");
                } else {
                    $("#product_image").attr("src", "images/up_arrow.png");
                }
            });
            var month = $("#MainContent_ddlMonth").val();
            var d = new Date();
            var n = d.getFullYear();
        });

        function bindGridView() {
            debugger;
            var sales_current_year = $('#MainContent_hdnsales_current_year').val();
            var open_order = $('#MainContent_hdnopen_order').val();
            var supply_now = $('#MainContent_hdnsupply_now').val();
            var target_month = $('#MainContent_hdntarget_month').val();
            var actual_month = $('#MainContent_hdnactual_month').val();
            var variance = $('#MainContent_hdnvariance').val();
            var pro_rate = $('#MainContent_hdnpro_rate').val();
            var sales_current_year_cp = $('#MainContent_hdnsales_current_year_cp').val();
            var open_order_cp = $('#MainContent_hdnopen_order_cp').val();
            var supply_now_cp = $('#MainContent_hdnsupply_now_cp').val();
            var target_month_cp = $('#MainContent_hdntarget_month_cp').val();
            var actual_month_cp = $('#MainContent_hdnactual_month_cp').val();
            var variance_cp = $('#MainContent_hdnvariance_cp').val();
            var pro_rate_cp = $('#MainContent_hdnpro_rate_cp').val();
            var sales_current_year_total = $('#MainContent_hdnsales_current_year_total').val();
            var open_order_total = $('#MainContent_hdnopen_order_total').val();
            var supply_now_total = $('#MainContent_hdnsupply_now_total').val();
            var target_month_total = $('#MainContent_hdntarget_month_total').val();
            var actual_month_total = $('#MainContent_hdnactual_month_total').val();
            var variance_total = $('#MainContent_hdnvariance_total').val();
            var pro_rate_total = $('#MainContent_hdnpro_rate_total').val();
            var head_content = $('#MainContent_salesbycustomer tr:first').html();
            $('#MainContent_salesbycustomer').prepend('<thead></thead>')
            $('#MainContent_salesbycustomer thead').html('<tr>' + head_content + '</tr>');
            $('#MainContent_salesbycustomer tbody tr:first').hide();
            // $('#MainContent_salesbycustomer').append('<tfoot><tr> <th colspan="4" style="text-align:right">GRAND TOTAL:</th><th style="text-align: right">' + open_order + '</th><th style="text-align: right">' + supply_now + '</th><th style="text-align: right">' + target_month + '</th><th style="text-align: right">' + actual_month + '</th><th style="text-align: right">' + variance + '</th><th style="text-align: right">' + pro_rate + '</th></tr></tfoot>');
            $('#MainContent_salesbycustomer').append('<tfoot><tr> <th colspan="6" style="text-align:right">CUSTOMER TOTAL:</th><th style="text-align: center">' + sales_current_year + '</th><th style="text-align: center">' + open_order + '</th><th style="text-align: center">' + supply_now + '</th><th style="text-align: center">' + target_month + '</th><th style="text-align: center">' + actual_month + '</th><th style="text-align: center">' + variance + '</th><th style="text-align: center">' + pro_rate +
                '</th></tr> <tr> <th colspan="6" style="text-align:right">CHANNEL PARTNER TOTAL:</th><th style="text-align: center">' + sales_current_year_cp + '</th><th style="text-align: center">' + open_order_cp + '</th><th style="text-align: center">' + supply_now_cp + '</th><th style="text-align: center">' + target_month_cp + '</th><th style="text-align: center">' + actual_month_cp + '</th><th style="text-align: center">' + variance_cp + '</th><th style="text-align: center">' + pro_rate_cp +
                '</th></tr><tr> <th colspan="6" style="text-align:right">GRAND TOTAL:</th><th style="text-align: center">' + sales_current_year_total + '</th><th style="text-align: center">' + open_order_total + '</th><th style="text-align: center">' + supply_now_total + '</th><th style="text-align: center">' + target_month_total + '</th><th style="text-align: center">' + actual_month_total + '</th><th style="text-align: center">' + variance_total + '</th><th style="text-align: center">' + pro_rate_total + '</th></tr></tfoot>');

            $('#MainContent_salesbycustomer').DataTable(
                {                 
                    "info": false
                });

            $('#MainContent_salesbycustomer').on('search.dt', function () {
                debugger;
                var value = $('.dataTables_filter input').val();
                console.log(value); // <-- the value
                $('#MainContent_hdnsearch').val = value;
                document.getElementById('<%=hdnsearch.ClientID%>').value = value;
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "ReviewWeekly.aspx/setGrandtotal",
                    data: "{'search':'" + value + "'}",
                    dataType: "json",
                    success: function (data) {
                        console.log(data);
                        console.log(data.d);
                        var parsed = data.d;
                        console.log(parsed);
                        console.log(parsed[1]);
                        $('#MainContent_targetvssales').prepend('<tfoot></tfoot>');
                        $('#MainContent_salesbycustomer tfoot').hide();
                        var sales_current_year=parsed[0];
                        var open_order = parsed[1];
                        var supply_now = parsed[2];
                        var target_month = parsed[3];
                        var actual_month = parsed[4];
                        var variance = parsed[5];
                        var pro_rate = parsed[6];
                        var sales_current_year_cp=parsed[7];
                        var open_order_cp = parsed[8];
                        var supply_now_cp = parsed[9];
                        var target_month_cp = parsed[10];
                        var actual_month_cp = parsed[11];
                        var variance_cp = parsed[12];
                        var pro_rate_cp = parsed[13];
                        var sales_current_year_total=parsed[14];
                        var open_order_total = parsed[15];
                        var supply_now_total = parsed[16];
                        var target_month_total = parsed[17];
                        var actual_month_total = parsed[18];
                        var variance_total = parsed[19];
                        var pro_rate_total = parsed[20];
                      //  $('#MainContent_salesbycustomer').append('<tfoot><tr> <th colspan="4" style="text-align:right">CUSTOMER TOTAL:</th><th style="text-align: right">' + open_order + '</th><th style="text-align: right">' + supply_now + '</th><th style="text-align: right">' + target_month + '</th><th style="text-align: right">' + actual_month + '</th><th style="text-align: right">' + variance + '</th><th style="text-align: right">' + pro_rate +
                      //'</th></tr> <tr> <th colspan="4" style="text-align:right">CHANNEL PARTNER TOTAL:</th><th style="text-align: right">' + open_order_cp + '</th><th style="text-align: right">' + supply_now_cp + '</th><th style="text-align: right">' + target_month_cp + '</th><th style="text-align: right">' + actual_month_cp + '</th><th style="text-align: right">' + variance_cp + '</th><th style="text-align: right">' + pro_rate_cp +
                        //'</th></tr><tr> <th colspan="4" style="text-align:right">GRAND TOTAL:</th><th style="text-align: right">' + open_order_total + '</th><th style="text-align: right">' + supply_now_total + '</th><th style="text-align: right">' + target_month_total + '</th><th style="text-align: right">' + actual_month_total + '</th><th style="text-align: right">' + variance_total + '</th><th style="text-align: right">' + pro_rate_total + '</th></tr></tfoot>');
                        $('#MainContent_salesbycustomer').append('<tfoot><tr> <th colspan="6" style="text-align:right">CUSTOMER TOTAL:</th><th style="text-align: center">' + sales_current_year + '</th><th style="text-align: center">' + open_order + '</th><th style="text-align: center">' + supply_now + '</th><th style="text-align: center">' + target_month + '</th><th style="text-align: center">' + actual_month + '</th><th style="text-align: center">' + variance + '</th><th style="text-align: center">' + pro_rate +
                '</th></tr> <tr> <th colspan="6" style="text-align:right">CHANNEL PARTNER TOTAL:</th><th style="text-align: center">' + sales_current_year_cp + '</th><th style="text-align: center">' + open_order_cp + '</th><th style="text-align: center">' + supply_now_cp + '</th><th style="text-align: center">' + target_month_cp + '</th><th style="text-align: center">' + actual_month_cp + '</th><th style="text-align: center">' + variance_cp + '</th><th style="text-align: center">' + pro_rate_cp +
                '</th></tr><tr> <th colspan="6" style="text-align:right">GRAND TOTAL:</th><th style="text-align: center">' + sales_current_year_total + '</th><th style="text-align: center">' + open_order_total + '</th><th style="text-align: center">' + supply_now_total + '</th><th style="text-align: center">' + target_month_total + '</th><th style="text-align: center">' + actual_month_total + '</th><th style="text-align: center">' + variance_total + '</th><th style="text-align: center">' + pro_rate_total + '</th></tr></tfoot>');

                        console.log("2");
                    },
                    error: function (result) {
                        console.log("No Match");
                    }
                });
            });
        }
        $('#MainContent_salesbycustomer').on('search.dt', function () {
            debugger;
            var value = $('.dataTables_filter input').val();
            console.log(value); // <-- the value
            //$('#MainContent_hdnsearch').val = value;
            document.getElementById('<%=hdnsearch.ClientID%>').value = value;

        });


        function triggerPostGridLodedActions() {
            bindGridView();
            //$('#MainContent_salesbycustomer tr:last').hide();
            //gridviewScrollTrigger();
            footerwidth1();

            //$('#MainContent_salesbycustomer tr').each(function () {

            //    $(this).find("td:nth-child(2)").each(function () {
            //        if ($(this).find('span').text() == "CUSTOMER TOTAL") {
            //            $(this).closest("tr").find("td").addClass("sum_total");
            //        }
            //    });
            //    $(this).find("td:nth-child(2)").each(function () {
            //        if ($(this).find('span').text() == "CHANNEL PARTNER TOTAL") {
            //            $(this).closest("tr").find("td").addClass("sum_total");
            //        }
            //    });
            //    $(this).find("td:nth-child(2)").each(function () {
            //        if ($(this).find('span').text() == "BRANCH TOTAL") {
            //            $(this).closest("tr").find("td").addClass("grand_total");
            //        }
            //    });
            //    $(this).find("td:nth-child(2)").each(function () {
            //        if ($(this).find('span').text() == "GRAND TOTAL") {
            //            $(this).closest("tr").find("td").addClass("grand_total");
            //        }
            //    });
            //});

            $('#MainContent_ddlBranchList').change(function () { $('#MainContent_export').css("display", "none") });
            $('#product_image').unbind('click').bind('click', function (e) {
                var attr = $('#product_image').attr('src');
                $("#MainContent_MainContent_dfltreportform").slideToggle();
                if (attr == "images/up_arrow.png") {
                    $("#product_image").attr("src", "images/down_arrow.png");
                } else {
                    $("#product_image").attr("src", "images/up_arrow.png");
                }
            });
        }
    </script>
    <%--<script type="text/javascript" src="js/Reports.js"></script>--%>
    <%-- <link href="GridviewScroll.css" rel="stylesheet" />
       <script type="text/javascript" src="gridscroll.js"></script>
       <script type="text/javascript" src="js/app.js"></script>--%>

    <style type="text/css">

     .text-center{ 
    text-align: center !important;
      }

     .text-right{    
    text-align: right !important;
     }

     .text-left
     {
    text-align: left !important;
     }


        .width {
            width:58px !important;
        }
     
        .HeadergridAll {
            background: #ebeef5;
            color: #fff;
            font-weight: 600;
            text-align: center;
            border-color: #ebeef5;
            font-size: 12px;
          
        }

        .sum_total {
            font-size: 14px;
            background-color: #999;
            color: #fff;
        }

        .grand_total {
            font-size: 14px;
            background-color: #52646C !important;
            color: #fff;
        }

        #salesbycustomer table tr:last-child {
            background-color: #999;
        }

        .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
            border: 1px solid #ddd;
            padding: 10px;
        }

        /*td:nth-child(2) {
            text-align: left !important;
        }*/

        td {
            background: #fff;
            /*text-align: right;*/
            padding: 10px;
            border-color: white;
        }

        th {
            padding: 10px;
            background: #ebeef5;
            color: #111;
            font-weight: 600;
            font-size: 13px;
            border-color: #ebeef5;
            /*height: 40px;*/
            padding: 10px;
            text-align: center;
        }

        #MainContent_salesbycustomer td:first-child {
            text-align: left;
        }

        #MainContent_salesbycustomer_total td:first-child {
            text-align: left;
        }

        #MainContent_salesbycustomer_total td {
            font-size: 14px;
            background-color: #52646C !important;
            color: #fff;
        }

        .control_dropdown {
            width: 180px;
            height: 30px;
            border-radius: 4px!important;
            position: relative;
        }

        .label {
            padding-top: 9px;
            width: 100%;
            color: black;
        }

        .btn.green {
            color: #fff;
            margin-left: auto;
            margin-right: auto;
            overflow: auto;
            cursor: pointer;
            top: -9px;
            position: relative;
            width: 100px;
            font-weight: 600;
            margin-top: 12px;
        }

        .control {
            padding-top: 2px;
        }

        input {
            margin-bottom: 5px;
        }
        /*.btn.green {
            color: #fff;
            margin-left: auto;
            margin-right: auto;
            overflow: auto;
            cursor: pointer;
            top: -9px;
        }*/
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableHistory="true" EnablePartialRendering="true" EnablePageMethods="true" AsyncPostBackTimeout="360">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <div class="crumbs">
                <!-- Start : Breadcrumbs -->
                <ul id="Ul2" class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a>Reports</a>
                    </li>
                    <li>Reviews</li>
                    <li class="current">Weekly Review</li>
                    <div>
                        <ul>
                            <li class="title_bedcrum" style="list-style: none;">WEEKLY REVIEW</li>
                        </ul>
                    </div>

                    <div>
                        <ul style="float: right; list-style: none; margin-top: -4px; width: 247px; margin-right: -5px;" class="alert alert-danger fade in">
                            <li>
                                <span style="margin-right: 4px; vertical-align: text-bottom;">Val In '000</span>
                                <asp:RadioButton ID="Thousand" OnCheckedChanged="Thousand_CheckedChanged" AutoPostBack="true" Checked="true" GroupName="byValueInradiobtn" runat="server" />
                                <span style="margin-right: 4px; margin-left: 4px; vertical-align: text-bottom;">Val In Lakhs</span>
                                <asp:RadioButton ID="Lakhs" OnCheckedChanged="Lakhs_CheckedChanged" AutoPostBack="true" GroupName="byValueInradiobtn" runat="server" />
                            </li>
                        </ul>
                    </div>
                </ul>
            </div>
            <div id="collapsebtn" class="row">
                <img id="product_image" src="images/up_arrow.png" align="left" style="margin-left: 46%;" />
            </div>
            <div id="MainContent_dfltreportform" runat="server" class="row filter_panel">
                <div runat="server" id="cterDiv" visible="false">
                    <ul class="btn-info rbtn_panel">
                        <li><span style="margin-right: 4px; vertical-align: text-bottom;">TAEGUTEC</span>
                            <asp:RadioButton ID="rdBtnTaegutec" AutoPostBack="true" Checked="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                            <span style="margin-right: 4px; margin-left: 4px; vertical-align: text-bottom;">DURACARB</span>
                            <asp:RadioButton ID="rdBtnDuraCab" AutoPostBack="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                        </li>
                    </ul>
                </div>

                <div class="col-md-2 control" runat="server" id="divBranch">
                    <label class="label">BRANCH</label>
                    <asp:DropDownList ID="ddlBranchList" runat="server" CssClass="control_dropdown" AutoPostBack="True" OnSelectedIndexChanged="ddlBranchList_SelectedIndexChanged"></asp:DropDownList>
                </div>

                <div class="col-md-2 control" runat="server" id="divSE">
                    <label class="label ">SALES ENGINEER </label>
                    <asp:DropDownList ID="ddlSalesEngineerList" runat="server" CssClass="control_dropdown">
                       <%-- <asp:ListItem>--SELECT SALES ENGINEER--</asp:ListItem>--%>
                    </asp:DropDownList>
                </div>

                <div class="col-md-2 control">
                    <label class="label">REVIEW MONTH</label>
                    <asp:DropDownList ID="ddlMonth" runat="server" CssClass="control_dropdown"></asp:DropDownList>
                </div>

                <div class="col-md-2 control ">
                    <label class="label">DATE</label>
                    <asp:TextBox ID="datepicker" type="text" class="form-control" runat="server" placeholder="SELECT THE DATE" Enabled="False"></asp:TextBox>
                </div>

                <div class="col-md-4 " style="margin-top: 2px;">
                    <div class="form-group">
                        <asp:Button ID="reports" runat="server" Text="FILTER" OnClick="reports_Click" CssClass="btn green  " />
                        <asp:Button ID="export" runat="server" Text="EXPORT" OnClick="export_Click" CssClass="btn green " />
                        <label id="alertmsg" style="display: none; font-weight: bold; color: #0582b7;">Now click on Filter  to view results</label>
                    </div>
                </div>
            </div>

            <%-- <%--OVERALL GRID-- START%>--%>
            <div>
                <br />
                <asp:GridView ID="salesbycustomer" Width="98%" Style="text-align: center;" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowDataBound="salesbycustomer_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="TYPE" ItemStyle-Width="10px" HeaderStyle-CssClass="HeadergridAll" Visible="true" ItemStyle-CssClass="align-left">
                            <ItemTemplate>
                                <asp:Label Style="text-align: left;" ID="lblCustType" runat="server" Text='<%# Eval("customer_type") %>' ></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="CUST/CP NO" ItemStyle-Width="10px" HeaderStyle-CssClass="HeadergridAll" Visible="true" ItemStyle-CssClass="center" >
                            <ItemTemplate>
                                <asp:Label Style="text-align: center;" ID="Label1" runat="server" Text='<%# Eval("cust_number") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="BRANCH"  HeaderStyle-CssClass="HeadergridAll" Visible="true"  ItemStyle-CssClass="align-left">
                           <ItemTemplate>
                               <asp:Label Style="text-align: left;" ID="lblbranch" runat="server" Text='<%# Eval("branch_code") %>'></asp:Label>
                           </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="SALES ENGINEER"  HeaderStyle-CssClass="HeadergridAll" Visible="true"  ItemStyle-CssClass="align-left" >
                            <ItemTemplate>
                                <asp:Label Style="text-align: left;" ID="lblsalesengg" runat="server" Text='<%# Eval("salesengineer_name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="CUST/CP NAME" HeaderStyle-CssClass="HeadergridAll text-left" Visible="true"  ItemStyle-Width="170px" ItemStyle-CssClass="align-left">
                            <ItemTemplate>
                                <asp:Label Style="text-align: left;" ID="Labelcust_name"  runat="server" Text='<%# Eval("cust_name") %>'></asp:Label>                                
                                <asp:HiddenField ID="hdn_custtype" runat="server" Value='<%# Eval("customer_type") %>' />
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="CREDIT CODE" HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="align-center" >
                            <ItemTemplate>
                                <asp:Label Style="text-align: center;" ID="Label9" runat="server" Text='<%# Eval("cred_code") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="SALES CURRENT YEAR" HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="align-center" ItemStyle-Width="110px">
                            <ItemTemplate>
                                <asp:Label Style="text-align: center;" ID="Label8" runat="server" Text='<%# Eval("sales_current_year") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="OPEN ORDER" HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="align-center">
                            <ItemTemplate>
                                <asp:Label Style="text-align: center;" ID="Label6" runat="server" Text='<%# Eval("open_order") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="CAN SUPPLY NOW" HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="align-center" ItemStyle-Width="98px">
                            <ItemTemplate>
                                <asp:Label Style="text-align: center;" ID="Label7" runat="server" Text='<%# Eval("can_supply_now") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="TARGET FOR THE MONTH" HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="align-center" ItemStyle-Width="98px">
                            <ItemTemplate>
                                <asp:Label Style="text-align: center;" ID="lbl_Targetvalue" runat="server" Text='<%# Eval("targetval") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ACTUAL FOR THE MONTH" HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="align-center" ItemStyle-Width="98px">
                            <ItemTemplate>
                                <asp:Label Style="text-align: center;" ID="lbl_Actualsale" runat="server" Text='<%# Eval("ActualValue") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="VARIANCE" HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="align-center">
                            <ItemTemplate>
                                <asp:Label Style="text-align: center;" ID="Label3" runat="server" Text='<%# Eval("variance") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="PRO- RATA ACH%" HeaderStyle-CssClass="HeadergridAll" ItemStyle-CssClass="align-center" ItemStyle-Width="98px" >
                            <%-- <HeaderTemplate>
                                                       <asp:Label ID="Label4" runat="server" Text="PRO-RATA ACH%"></asp:Label>
                                                  <%-- <asp:ImageButton   ToolTip="Sorting" Width="15%" runat="server" ImageUrl="~/images/sort_neutral.png" ID="ibtn_ProRataPerSort" OnClick="ibtn_ProRataPerSort_Click" />
                        
                                                           </HeaderTemplate> --%>
                            <ItemTemplate>
                                <asp:Label Style="text-align: center;" ID="Labe5" runat="server" Text='<%# Eval("prorata") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            <asp:HiddenField ID="hdnsales_current_year" runat="server" />
            <asp:HiddenField ID="hdnopen_order" runat="server" />
            <asp:HiddenField ID="hdnsupply_now" runat="server" />
            <asp:HiddenField ID="hdntarget_month" runat="server" />
            <asp:HiddenField ID="hdnactual_month" runat="server" />
            <asp:HiddenField ID="hdnvariance" runat="server" />
            <asp:HiddenField ID="hdnpro_rate" runat="server" />
            <asp:HiddenField ID="hdnsales_current_year_cp" runat="server" />
            <asp:HiddenField ID="hdnopen_order_cp" runat="server" />
            <asp:HiddenField ID="hdnsupply_now_cp" runat="server" />
            <asp:HiddenField ID="hdntarget_month_cp" runat="server" />
            <asp:HiddenField ID="hdnactual_month_cp" runat="server" />
            <asp:HiddenField ID="hdnvariance_cp" runat="server" />
            <asp:HiddenField ID="hdnpro_rate_cp" runat="server" />
            <asp:HiddenField ID="hdnsales_current_year_total" runat="server" />
            <asp:HiddenField ID="hdnopen_order_total" runat="server" />
            <asp:HiddenField ID="hdnsupply_now_total" runat="server" />
            <asp:HiddenField ID="hdntarget_month_total" runat="server" />
            <asp:HiddenField ID="hdnactual_month_total" runat="server" />
            <asp:HiddenField ID="hdnvariance_total" runat="server" />
            <asp:HiddenField ID="hdnpro_rate_total" runat="server" />
            <asp:HiddenField ID="hdnsearch" runat="server" />

    
            <%--<asp:GridView ID="salesbycustomer_total"  Width="100%" Style="text-align: center;" runat="server" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" showHeader="false">
                 
                  <columns>  
                                <asp:TemplateField HeaderText="CUSTOMER/CHANNEL PARTNER NUMBER" HeaderStyle-CssClass="HeadergridAll" visible="true" ItemStyle-Width="125px"   >  
                                                     <ItemTemplate>  
                                                         <asp:Label  ID="Label1" runat="server" Text='<%# Eval("cust_number") %>'></asp:Label>  
                                                     </ItemTemplate>  
                                                 </asp:TemplateField>  
                               <asp:TemplateField HeaderText="CUSTOMER/CHANNEL PARTNER" HeaderStyle-CssClass="HeadergridAll" visible="true" ItemStyle-Width="176px"   >  
                                                     <ItemTemplate>  
                                                         <asp:Label  ID="Labelcust_name" runat="server" Text='<%# Eval("cust_name") %>'></asp:Label>  
                                                     </ItemTemplate>  
                                                 </asp:TemplateField>   
                              <asp:TemplateField HeaderText="CREDIT CODE" HeaderStyle-CssClass="HeadergridAll"    >  
                                                     <ItemTemplate>  
                                                         <asp:Label  ID="Label9" runat="server" Text='<%# Eval("cred_code") %>'></asp:Label>  
                                                     </ItemTemplate>  
                                                 </asp:TemplateField>  
                               <asp:TemplateField HeaderText="SALES CURRENT YEAR" HeaderStyle-CssClass="HeadergridAll"    >  
                                                     <ItemTemplate>  
                                                         <asp:Label  ID="Label8" runat="server" Text='<%# Eval("sales_current_year") %>'></asp:Label>  
                                                     </ItemTemplate>  
                                                 </asp:TemplateField>  
                                <asp:TemplateField HeaderText="OPEN ORDER" HeaderStyle-CssClass="HeadergridAll"    >  
                                                     <ItemTemplate>  
                                                         <asp:Label  ID="Label6" runat="server" Text='<%# Eval("open_order") %>'></asp:Label>  
                                                     </ItemTemplate>  
                                                 </asp:TemplateField>  
                                 <asp:TemplateField HeaderText="CAN SUPPLY NOW" HeaderStyle-CssClass="HeadergridAll"    >  
                                                     <ItemTemplate>  
                                                         <asp:Label  ID="Label7" runat="server" Text='<%# Eval("can_supply_now") %>'></asp:Label>  
                                                     </ItemTemplate>  
                                                 </asp:TemplateField>  
                                                                                  
                                 <asp:TemplateField HeaderText="TARGET FOR THE MONTH" HeaderStyle-CssClass="HeadergridAll"    >  
                                                     <ItemTemplate>  
                                                         <asp:Label  ID="lbl_Targetvalue" runat="server" Text='<%# Eval("targetval") %>'></asp:Label>  
                                                     </ItemTemplate>  
                                                 </asp:TemplateField> 
                                 <asp:TemplateField HeaderText="ACTUAL FOR THE MONTH" HeaderStyle-CssClass="HeadergridAll"    >  
                                                     <ItemTemplate>  
                                                         <asp:Label  ID="lbl_Actualsale" runat="server" Text='<%# Eval("ActualValue") %>'></asp:Label>  
                                                     </ItemTemplate>  
                                                 </asp:TemplateField>  
                                 <asp:TemplateField HeaderText="VARIANCE" HeaderStyle-CssClass="HeadergridAll"    >  
                                                     <ItemTemplate>  
                                                         <asp:Label  ID="Label3" runat="server" Text='<%# Eval("variance") %>'></asp:Label>  
                                                     </ItemTemplate>  
                                                 </asp:TemplateField>  
                                 <asp:TemplateField HeaderText="PRO-RATA ACH%" HeaderStyle-CssClass="HeadergridAll"    >  
                                                 <HeaderTemplate>
                                                       <asp:Label ID="Label4" runat="server" Text="PRO-RATA ACH%"></asp:Label>
                                                   <asp:ImageButton   ToolTip="Sorting" Width="8%" runat="server" ImageUrl="~/images/sort_neutral.png" ID="ibtn_ProRataPerSort" OnClick="ibtn_ProRataPerSort_Click" />
                        
                                                           </HeaderTemplate>
                                                     <ItemTemplate>  
                                                         <asp:Label  ID="Labe5" runat="server" Text='<%# Eval("prorata") %>'></asp:Label>  
                                                        
                                                     </ItemTemplate>  
                                                 </asp:TemplateField> 
                                            
                      
                                         </columns>

               </asp:GridView>--%>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="reports" EventName="Click" />

            <asp:PostBackTrigger ControlID="export" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
                <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff">Please wait</span>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <script>

        $(document).ready(function () {
            // gridviewScrollTrigger();
            //footerwidth1();

            $('#MainContent_salesbycustomer tr').each(function () {

                $(this).find("td:first").each(function () {
                    if ($(this).find('span').text() == "CUSTOMER TOTAL") {
                        $(this).closest("tr").find("td").addClass("sum_total");
                    }
                });
                $(this).find("td:first").each(function () {
                    if ($(this).find('span').text() == "CHANNEL PARTNER TOTAL") {
                        $(this).closest("tr").find("td").addClass("sum_total");
                    }
                });
                $(this).find("td:first").each(function () {
                    if ($(this).find('span').text() == "BRANCH TOTAL") {
                        $(this).closest("tr").find("td").addClass("grand_total");
                    }
                });
                $(this).find("td:first").each(function () {
                    if ($(this).find('span').text() == "GRAND TOTAL") {
                        $(this).closest("tr").find("td").addClass("grand_total");
                    }
                });
            });

            //$(window).resize(function () {
            //    function gridviewScrollTrigger() {
            //        gridView1 = $('#MainContent_salesbycustomer').gridviewScroll({
            //            width: $(window).width() - 60,
            //            height: 600,
            //            railcolor: "#F0F0F0",
            //            barcolor: "#606060",
            //            barhovercolor: "#606060",
            //            bgcolor: "#F0F0F0",
            //            freezesize: 0,
            //            arrowsize: 30,
            //            varrowtopimg: "Images/arrowvt.png",
            //            varrowbottomimg: "Images/arrowvb.png",
            //            harrowleftimg: "Images/arrowhl.png",
            //            harrowrightimg: "Images/arrowhr.png",
            //            headerrowcount: 1,
            //            railsize: 16,
            //            barsize: 14,
            //            verticalbar: "auto",
            //            horizontalbar: "auto",
            //            wheelstep: 1,
            //        });
            //    }
            //    //footerwidth1();
            //});
        });

        function gridviewScrollTrigger() {
            gridView1 = $('#MainContent_salesbycustomer').gridviewScroll({
                width: $(window).width() - 60,
                height: 450,
                railcolor: "#F0F0F0",
                barcolor: "#606060",
                barhovercolor: "#606060",
                bgcolor: "#F0F0F0",
                freezesize: 0,
                arrowsize: 30,
                varrowtopimg: "Images/arrowvt.png",
                varrowbottomimg: "Images/arrowvb.png",
                harrowleftimg: "Images/arrowhl.png",
                harrowrightimg: "Images/arrowhr.png",
                headerrowcount: 1,
                railsize: 16,
                barsize: 14,
                verticalbar: "auto",
                horizontalbar: "auto",
                wheelstep: 1,
            });
        }

        function findminofscreenheight(a, b) {
            a = a - $("#MainContent_ddlBranchList").offset().top;
            return a < b ? a : b;
        }

        function footerwidth1() {
            var vartoptablewidth1 = $('#MainContent_salesbycustomer').innerWidth();
            $('#MainContent_salesbycustomer_total ').innerWidth(vartoptablewidth1);
            var arrOfTable1 = [],
            i = 0;
            $('#MainContent_salesbycustomer td').each(function () {
                mWid = $(this).innerWidth();
                arrOfTable1.push(mWid);
            });
            $('#MainContent_salesbycustomer_total td').each(function () {
                $(this).css("width", arrOfTable1[i] + "px");
                i++;
            });
        }

    </script>
</asp:Content>
