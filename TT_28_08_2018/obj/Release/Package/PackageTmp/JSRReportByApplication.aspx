﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JSRReportByApplication.aspx.cs" Inherits="TaegutecSalesBudget.JSRReportByApplication" MasterPageFile="~/Site.Master" %>

<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="act" %>

<asp:Content ContentPlaceHolderID="HeadContent" ID="content1" runat="server">
    <link href="GridviewScroll.css" rel="stylesheet" />
    <script type="text/javascript" src="gridscroll.js"></script>
    <script type="text/javascript" src="js/app.js"></script>

    <script src="js/jquery.sumoselect.min.js"></script>
    <link href="css/sumoselect.css" rel="stylesheet" />

     <script>
         $(document).ready(function () {

             triggerPostGridLodedActions();
             $(window).resize(function () {
                 triggerPostGridLodedActions();
             });

             $('#product_image').unbind('click').bind('click', function (e) {
                 var attr = $('#product_image').attr('src');
                 $("#MainContent_reportdrpdwns").slideToggle();
                 if (attr == "images/up_arrow.png") {
                     $("#product_image").attr("src", "images/down_arrow.png");
                 } else {
                     $("#product_image").attr("src", "images/up_arrow.png");
                 }
             });
         });

         var divPopUp;
         function DivPopUpOpen() {
             dclg = $("#dvResultPopUp").dialog(
                        {
                            resizable: false,
                            draggable: true,
                            modal: true,
                            title: "Select to Proceed",
                            width: "500",
                            //height: "150",
                            closeOnEscape: false,
                            //beforeClose: function (event, ui) { return false; },
                            dialogClass: "noclose",

                        });
             divPopUp = dclg;
             dclg.parent().appendTo(jQuery("form:first"));
         }

         function closePopUpLogoff() {
             divPopUp.dialog('close');

         }
         function closePopUpNewCust() {

             divPopUp.dialog('close');
             triggerPostGridLodedActions();

         }
         function closePopUpContinue() {

             divPopUp.dialog('close');
             triggerPostGridLodedActions();

         }

         function triggerPostGridLodedActions() {
             //debugger;
             var RoleID = '<%=Session["RoleId"].ToString()%>';
            if (RoleID == "HO" || RoleID == "TM") {
                $(<%=BranchList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true, search: true });

                $(<%=SalesEngList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true, search: true });

                $(<%=CustNameList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true, search: true });


                $(<%=CustNumList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true, search: true });

            }
            if (RoleID == "BM") {
                $(<%=SalesEngList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true, search: true });
                   $(<%=CustNameList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true, search: true });
                   $(<%=CustNumList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true, search: true });
               }

               if (RoleID == "SE") {
                   $(<%=CustNameList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true, search: true });
                   $(<%=CustNumList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true, search: true });
               }
               $(<%=ProductGroupList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true });
            $(<%=ProductFamList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true, search: true });
            $(<%=ProdSubFamilyList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true, search: true });
            $(<%=AppList.ClientID%>).SumoSelect({ okCancelInMulti: true, selectAll: true, search: true });

            $('#product_image').unbind('click').bind('click', function (e) {
                var attr = $('#product_image').attr('src');
                $("#MainContent_reportdrpdwns").slideToggle();
                if (attr == "images/up_arrow.png") {
                    $("#product_image").attr("src", "images/down_arrow.png");
                } else {
                    $("#product_image").attr("src", "images/up_arrow.png");
                }
            });

            $('#txtbranchlist').attr('readonly', true);

            gridviewScroll();
            //bindToogleForRowIndex();
            //bindToogleForParentRowIndex();

            //Quantity changes Start
            jQuery(".product_type").each(function () {
                jQuery(this).change(function () {
                    var currentIndex = jQuery(this).data("product_type");
                    var grandTotal = 0;
                    jQuery(".product_type_" + currentIndex).each(function () {
                        var val = jQuery(this).val();
                        if (val != "") {
                            grandTotal += parseInt(val);
                        }
                    });
                    jQuery(".product_type_sub_total_" + currentIndex).val(grandTotal).change();
                });
            });

            jQuery(".product_type_sub_total").each(function () {
                jQuery(this).change(function () {
                    var currentIndex = jQuery(this).data("product_type_sub_total_index");
                    var grandTotal = 0;
                    jQuery(".product_type_sub_total_row_" + currentIndex).each(function () {
                        var val = jQuery(this).val();
                        if (val != "") {
                            grandTotal += parseInt(val);
                        }
                    });
                    jQuery(".product_type_SubFamilySum_" + currentIndex).val(grandTotal).change();
                });
            });
            //Quantity Changes End

            var td = $("#MainContent_grdviewAllValuesCopy").find("tr:first").find("td:first");
            $(td).find('div').attr("style", "min-width:25px");
            td = $("#MainContent_grdviewAllValuesCopyFreeze").find("tr:first").find("td:first");
            $(td).find('div').attr("style", "min-width:25px");
        }


        function gridviewScroll() {

            gridView1 = $('#MainContent_grdviewAllValues').gridviewScroll({
                width: $(window).width() - 70,
                height: findminofscreenheight($(window).height(), 450),
                railcolor: "#F0F0F0",
                barcolor: "#606060",
                barhovercolor: "#606060",
                bgcolor: "#F0F0F0",
                freezesize: 0,
                arrowsize: 30,
                varrowtopimg: "Images/arrowvt.png",
                varrowbottomimg: "Images/arrowvb.png",
                harrowleftimg: "Images/arrowhl.png",
                harrowrightimg: "Images/arrowhr.png",
                headerrowcount: 2,
                railsize: 16,
                barsize: 14,
                verticalbar: "inherit",
                horizontalbar: "inherit",
                wheelstep: 1,
            });
        }
        function findminofscreenheight(a, b) {
            a = a - $("#MainContent_CustNumList").offset().top;
            return a < b ? a : b;
        }
        $('#MainContent_grdView_T_Clamp_Partingoff tr:last').addClass("last_row");

        jQuery(".row_index").click(function () {

            var currentIndex = jQuery(this).data("index");

            jQuery(".subrowindex_" + currentIndex).each(function () {
                if (jQuery(this).css("display") != "none")
                    jQuery(this).slideUp();
                else
                    jQuery(this).slideDown();
            });
        });

        //function collapseAll()
        //{
        //    triggerPostGridLodedActions();
        //    jQuery(".row_index").click(function () {

        //        var currentIndex = jQuery(this).data("index");

        //        jQuery(".subrowindex_" + currentIndex).each(function () {
        //            //if (jQuery(this).css("display") != "none")
        //                jQuery(this).slideUp();
        //            //else
        //            //    jQuery(this).slideDown();
        //        });
        //    });
        //}

        jQuery(document).on('click', '.parent_row_index td div img', function () {
            //debugger;
            var currentIndex = jQuery(this).closest(".parent_row_index").data("parent_row_index");
            var minimised = false;
            jQuery(".parent_row_index_" + currentIndex).each(function () {
                if (jQuery(this).data("toogle_status") == "MAXIMISED")
                    jQuery(this).find("td div img").click();
                var nextElement = jQuery(this).prev();
                if (jQuery(this).css("display") != "none") {
                    jQuery(this).slideUp();
                    if (jQuery(nextElement).hasClass("empty_row"))
                        jQuery(nextElement).slideUp();
                }
                else {
                    jQuery(this).slideDown();
                    if (jQuery(nextElement).hasClass("empty_row"))
                        jQuery(nextElement).slideDown();
                    minimised = true;
                }
            });


            if (minimised == true) {

                console.log(jQuery(this).attr("src"));

                console.log(jQuery(this).attr('id'));
                console.log(jQuery(this).attr("src"));
                jQuery(this).attr("src", "");
                jQuery(this).attr("src", "images/button_minus.gif");
                console.log(jQuery(this).attr("src"));
                //console.log("minimising");
                var anamika = jQuery(this).attr('id');
                gridviewScroll();
                //setTimeout(function () { console.log($('#' + anamika).attr('src')); gridviewScroll(9, anamika); }, 3000);
                console.log(jQuery(this).attr("src"));
            }
            else {
                /* $('#MainContent_grdviewAllValues').gridviewScroll({
                     enabled: false
                 });
                 console.log(jQuery(this).attr("src"));*/
                jQuery(this).attr("src", "images/button_plus.gif");
                gridviewScroll();
                //var anamika = jQuery(this).attr('id');
                //setTimeout(function () { console.log($('#' + anamika).attr('src')); gridviewScroll(9, anamika); }, 3000);*/
            }
            //if (localStorage.getItem("freezeSize")==9) {
            //    gridviewScroll(0);
            //}
            //else {
            //    gridviewScroll(0);
            //}
            //gridviewScroll(0);

        });
        jQuery(document).on('click', '.row_index_image', function () {
            debugger;
            var currentRowElement = jQuery(this).closest(".row_index");
            var currentIndex = jQuery(currentRowElement).data("index");
            var minimised = false;
            jQuery(".subrowindex_" + currentIndex).each(function () {
                if (jQuery(this).css("display") != "none") {
                    jQuery(this).slideUp();
                    minimised = true;
                }
                else {
                    jQuery(this).slideDown();
                    minimised = false;
                }
            });
            if (minimised) {
                jQuery(this).attr("src", "images/button_plus.gif");
                jQuery(currentRowElement).data("toogle_status", "MINIMISED");
            }
            else {
                jQuery(this).attr("src", "images/button_minus.gif");
                jQuery(currentRowElement).data("toogle_status", "MAXIMISED");
            }
            gridviewScroll();
        });
        jQuery(document).on('click', '#MainContent_expand', function () {
            debugger;
            console.log('outside');
            $("#MainContent_grdviewAllValues").find("tr.parent_row_index td div img").each(function () {
                console.log('here 3');
                var currentIndex = jQuery(this).closest(".parent_row_index").data("parent_row_index");
                var minimised = false;
                jQuery(".parent_row_index_" + currentIndex).each(function () {
                    if (jQuery(this).data("toogle_status") == "MAXIMISED")
                        jQuery(this).find("td div img").click();
                    var nextElement = jQuery(this).prev();
                    if (jQuery(this).css("display") != "none") {
                    }
                    else {
                        jQuery(this).slideDown();
                        if (jQuery(nextElement).hasClass("empty_row"))
                            jQuery(nextElement).slideDown();
                        minimised = true;
                    }
                });
                jQuery(this).attr("src", "images/button_minus.gif");
                console.log('here 4');
            });
            $("#MainContent_grdviewAllValues").find("tr td .row_index_image").each(function () {
                console.log('here 5');
                var currentRowElement = jQuery(this).closest(".row_index");
                var currentIndex = jQuery(currentRowElement).data("index");
                var minimised = false;
                jQuery(".subrowindex_" + currentIndex).each(function () {
                    if (jQuery(this).css("display") != "none") {
                        minimised = true;
                    }
                    else {
                        jQuery(this).slideDown();
                        minimised = false;
                    }
                });
                if (minimised) {
                }
                else {
                    jQuery(this).attr("src", "images/button_minus.gif");
                    jQuery(currentRowElement).data("toogle_status", "MAXIMISED");
                }
                console.log('here 6');
            });
            triggerPostGridLodedActions();
        });
        jQuery(document).on('click', '#MainContent_collapse', function () {
            debugger;
            $("#MainContent_grdviewAllValues").find("tr.parent_row_index td div img").each(function () {
                console.log('here 3');
                var currentIndex = jQuery(this).closest(".parent_row_index").data("parent_row_index");
                var minimised = false;
                jQuery(".parent_row_index_" + currentIndex).each(function () {
                    if (jQuery(this).data("toogle_status") == "MAXIMISED")
                        jQuery(this).find("td div img").click();
                    var nextElement = jQuery(this).prev();
                    if (jQuery(this).css("display") != "none") {
                        jQuery(this).slideUp();
                        if (jQuery(nextElement).hasClass("empty_row"))
                            jQuery(nextElement).slideUp();
                    }

                });
                if (minimised) {
                    //jQuery(this).attr("src", "images/button_minus.gif");
                }
                else
                    jQuery(this).attr("src", "images/button_plus.gif");
                console.log('here 4');

            });
            $("#MainContent_grdviewAllValues").find("tr td .row_index_image").each(function () {
                console.log('here 5');
                var currentRowElement = jQuery(this).closest(".row_index");
                var currentIndex = jQuery(currentRowElement).data("index");
                var minimised = false;
                jQuery(".subrowindex_" + currentIndex).each(function () {
                    if (jQuery(this).css("display") != "none") {
                        jQuery(this).slideUp();
                        minimised = true;
                    }
                    else {
                        //jQuery(this).slideDown();
                        minimised = false;
                    }
                });
                if (minimised) {
                    jQuery(this).attr("src", "images/button_plus.gif");
                    jQuery(currentRowElement).data("toogle_status", "MINIMISED");
                }
                else {
                    jQuery(this).attr("src", "images/button_plus.gif");
                    jQuery(currentRowElement).data("toogle_status", "MINIMISED");
                    //jQuery(this).attr("src", "images/button_minus.gif");
                    //jQuery(currentRowElement).data("toogle_status", "MAXIMISED");
                }
                console.log('here 6');
            });
            //triggerPostGridLodedActions();
        });

    </script>
    <style>
          .form-group table {
            border-top: solid 1px #ddd;
            width: 100%;
        }


           .SelectBox {
            padding-top: 9px;
            width: 180px!important;
            border-radius: 4px!important;
        }
             .control_dropdown {
            width: 180px;
          height: 30px;
            border-radius: 4px!important;
        }

        .label {
            padding-top: 9px;
            width: 100%;
            color: black;
        }

        .control {
            padding-top: 2px;
        }

        .control_dropdown {
            width: 180px;
            height: 30px;
            border-radius: 4px!important;
        }

        .multiple {
            padding: 8px 10px;
            /* height: 300px !important;*/
            font-size: 12px;
            border: 1px solid #dadada;
        }
        td {
            height: 55px !important;
        }
        /*multiselect*/
        .form-group td {
            background: #fff;
            text-align: left;
            padding: 5px;
            border-bottom: solid 1px #ddd;
            height: auto !important;
            width: 100%;
        }

        .col-md-4 table {
            width: 100%;
        }

        th {
            padding: 10px;
        }

        .form-group input[type="radio"], .form-group input[type="checkbox"] {
            margin: 4px 4px 0;
        }

        .form-group label {
            vertical-align: text-bottom;
            display: inline;
        }

        .mn_all {
            padding: 5px;
        }

      
        /*multiselect-end*/
        .noclose .ui-dialog-titlebar-close {
            display: none;
        }

        .ui-dialog .ui-dialog-titlebar {
            padding-left: 45px;
            text-align: center !important;
        }

        #MainContent_grdviewAllValues_Image1_0 {
            display: none;
        }

        #MainContent_grdviewAllValues td {
            text-align: center;
        }


        #MainContent_rbtnlistComp td {
            height: 0px !important;
            padding: 0px !important;
        }

        .btn.sidelight {
            /*padding: 7px;
    width: 120px;
    display: block;
    margin: 10px;
    float: left;
    text-align: center;*/
            /*color: #fff;*/
            /*background: radial-gradient(circle at 100px 100px, #025995, #047fb4, #025995);*/
            color: #fff;
            margin-left: auto;
            margin-right: auto;
            overflow: auto;
            cursor: pointer;
            position: relative;
            background: -webkit-linear-gradient(rgba(2, 89, 149, 1), rgba(2, 89, 149, 0) 0%), -webkit-linear-gradient(-45deg, rgba(4, 128, 181, 0.9) 45%, rgba(4, 128, 181, 1) 0%);
            behavior: url(/common/assets/css/pie/PIE.htc);
            padding: 7px;
            width: 100px;
            font-weight: 600;
            margin-top: 5px;
        }

        .btn.green {
            top: 0!important;
        }

        .SumoSelect p {
            margin: 0;
            width: 200px;
        }

        .SumoSelect {
            width: 252px;
        }

        .SelectBox {
            padding: 5px 0px;
            /* margin-bottom:5px;*/
        }

        /* Filtering style */

        .SumoSelect .hidden {
            display: none;
        }

        .SumoSelect .search-txt {
            display: none;
            outline: none;
        }

        .SumoSelect .no-match {
            display: none;
            padding: 6px;
        }

        .SumoSelect.open .search-txt {
            display: inline-block;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
            margin: 0;
            padding: 5px 8px;
            border: none;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            border-radius: 5px;
        }

        .SumoSelect.open > .search > span,
        .SumoSelect.open > .search > label {
            visibility: hidden;
        }


        /*this is applied on that hidden select. DO NOT USE display:none; or visiblity:hidden; and Do not override any of these properties. */

        .SelectClass,
        .SumoUnder {
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            height: 100%;
            width: 100%;
            border: none;
            -webkit-box-sizing: border-box;
            -moz-box-sizing: border-box;
            box-sizing: border-box;
            -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
            filter: alpha(opacity=0);
            -moz-opacity: 0;
            -khtml-opacity: 0;
            opacity: 0;
        }

        .SelectClass {
            z-index: 1;
        }

        .SumoSelect > .optWrapper > .options li.opt label,
        .SumoSelect > .CaptionCont,
        .SumoSelect .select-all > label {
            user-select: none;
            -o-user-select: none;
            -moz-user-select: none;
            -khtml-user-select: none;
            -webkit-user-select: none;
            overflow-wrap: normal;
        }

        .SumoSelect {
            display: inline-block;
            position: relative;
            outline: none;
        }

            .SumoSelect:focus > .CaptionCont,
            .SumoSelect:hover > .CaptionCont,
            .SumoSelect.open > .CaptionCont {
                box-shadow: 0 0 2px #7799D0;
                border-color: #7799D0;
            }

            .SumoSelect > .CaptionCont {
                position: relative;
                border: 1px solid #A4A4A4;
                min-height: 14px;
                background-color: #fff;
                border-radius: 2px;
                margin: 0;
            }

                .SumoSelect > .CaptionCont > span {
                    display: block;
                    padding-right: 30px;
                    text-overflow: ellipsis;
                    white-space: nowrap;
                    overflow: hidden;
                    cursor: default;
                    margin-left: 5px;
                }


                    /*placeholder style*/

                    .SumoSelect > .CaptionCont > span.placeholder {
                        color: #ccc;
                        font-style: italic;
                    }

                .SumoSelect > .CaptionCont > label {
                    position: absolute;
                    top: 0;
                    right: 0;
                    bottom: 0;
                    width: 30px;
                }

                    .SumoSelect > .CaptionCont > label > i {
                        background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAA0AAAANCAYAAABy6+R8AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH3wMdBhAJ/fwnjwAAAGFJREFUKM9jYBh+gBFKuzEwMKQwMDB8xaOWlYGB4T4DA0MrsuapDAwM//HgNwwMDDbYTJuGQ8MHBgYGJ1xOYGNgYJiBpuEpAwODHSF/siDZ+ISBgcGClEDqZ2Bg8B6CkQsAPRga0cpRtDEAAAAASUVORK5CYII=');
                        background-position: center center;
                        width: 16px;
                        height: 16px;
                        display: block;
                        position: absolute;
                        top: 0;
                        left: 0;
                        right: 0;
                        bottom: 0;
                        margin: auto;
                        background-repeat: no-repeat;
                        opacity: 0.8;
                    }

            .SumoSelect > .optWrapper {
                display: none;
                z-index: 1000;
                top: 30px;
                width: 100%;
                position: absolute;
                left: 0;
                -webkit-box-sizing: border-box;
                -moz-box-sizing: border-box;
                box-sizing: border-box;
                background: #fff;
                border: 1px solid #ddd;
                box-shadow: 2px 3px 3px rgba(0, 0, 0, 0.11);
                border-radius: 3px;
                overflow: visible;
                padding-bottom: 0px;
                padding-right: 0px;
                padding-left: 0px;
                padding-top: 0px;
            }

            .SumoSelect.open > .optWrapper {
                top: 35px;
                display: block;
            }

                .SumoSelect.open > .optWrapper.up {
                    top: auto;
                    bottom: 100%;
                    margin-bottom: 5px;
                }

            .SumoSelect > .optWrapper ul {
                list-style: none;
                display: block;
                padding: 0;
                margin: 0;
                overflow: auto;
            }

            .SumoSelect > .optWrapper > .options {
                border-radius: 2px;
                position: relative;
                /*Set the height of pop up here (only for desktop mode)*/
                max-height: 250px;
                /*height*/
            }

                .SumoSelect > .optWrapper > .options li.group.disabled > label {
                    opacity: 0.5;
                }

                .SumoSelect > .optWrapper > .options li ul li.opt {
                    padding-left: 22px;
                }

            .SumoSelect > .optWrapper.multiple > .options li ul li.opt {
                padding-left: 50px;
            }

            .SumoSelect > .optWrapper.isFloating > .options {
                max-height: 100%;
                box-shadow: 0 0 100px #595959;
            }

            .SumoSelect > .optWrapper > .options li.opt {
                padding: 6px 6px;
                position: relative;
                border-bottom: 1px solid #f5f5f5;
            }

            .SumoSelect > .optWrapper > .options > li.opt:first-child {
                border-radius: 2px 2px 0 0;
            }

            .SumoSelect > .optWrapper > .options > li.opt:last-child {
                border-radius: 0 0 2px 2px;
                border-bottom: none;
            }

            .SumoSelect > .optWrapper > .options li.opt:hover {
                background-color: #E4E4E4;
            }

            .SumoSelect > .optWrapper > .options li.opt.sel {
                background-color: #a1c0e4;
                border-bottom: 1px solid #a1c0e4;
            }

            .SumoSelect > .optWrapper > .options li label {
                text-overflow: ellipsis;
                white-space: nowrap;
                overflow: hidden;
                display: block;
                cursor: pointer;
            }

            .SumoSelect > .optWrapper > .options li span {
                display: none;
            }

            .SumoSelect > .optWrapper > .options li.group > label {
                cursor: default;
                padding: 8px 6px;
                font-weight: bold;
            }


            /*Floating styles*/

            .SumoSelect > .optWrapper.isFloating {
                position: fixed;
                top: 0;
                left: 0;
                right: 0;
                width: 90%;
                bottom: 0;
                margin: auto;
                max-height: 90%;
            }


            /*disabled state*/

            .SumoSelect > .optWrapper > .options li.opt.disabled {
                background-color: inherit;
                pointer-events: none;
            }

                .SumoSelect > .optWrapper > .options li.opt.disabled * {
                    -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=50)";
                    /* IE 5-7 */
                    filter: alpha(opacity=50);
                    /* Netscape */
                    -moz-opacity: 0.5;
                    /* Safari 1.x */
                    -khtml-opacity: 0.5;
                    /* Good browsers */
                    opacity: 0.5;
                }


            /*styling for multiple select*/

            .SumoSelect > .optWrapper.multiple > .options li.opt {
                padding-left: 35px;
                cursor: pointer;
            }

      

        .SumoSelect > .optWrapper.multiple > .options li.opt span,
        .SumoSelect .select-all > span {
            position: absolute;
            display: block;
            width: 30px;
            top: 0;
            bottom: 0;
            margin-top: 5px;
        }

            .SumoSelect > .optWrapper.multiple > .options li.opt span i,
            .SumoSelect .select-all > span i {
                position: absolute;
                margin: auto;
                left: 0;
                right: 0;
                top: 0;
                bottom: 0;
                width: 14px;
                height: 14px;
                border: 1px solid #AEAEAE;
                border-radius: 2px;
                box-shadow: inset 0 1px 3px rgba(0, 0, 0, 0.15);
                background-color: #fff;
            }

        .SumoSelect > .optWrapper > .MultiControls {
            display: inline;
            border-top: 1px solid #ddd;
            background-color: #fff;
            box-shadow: 0 0 2px rgba(0, 0, 0, 0.13);
            border-radius: 0 0 3px 3px;
        }

        .SumoSelect > .optWrapper.multiple.isFloating > .MultiControls {
            display: block;
            margin-top: 5px;
            position: absolute;
            bottom: 0;
            width: 100%;
        }


        .SumoSelect > .optWrapper.multiple.okCancelInMulti > .MultiControls {
            display: block;
        }

            .SumoSelect > .optWrapper.multiple.okCancelInMulti > .MultiControls > p {
                /* padding: 6px;*/
            }

        .SumoSelect > .optWrapper.multiple > .MultiControls > p {
            display: inline-block;
            cursor: pointer;
            /*padding: 12px;*/
            width: 50%;
            box-sizing: border-box;
            text-align: center;
        }

            .SumoSelect > .optWrapper.multiple > .MultiControls > p:hover {
                background-color: #f1f1f1;
                margin-bottom: 10px;
            }

            .SumoSelect > .optWrapper.multiple > .MultiControls > p.btnOk {
                border-right: 1px solid #DBDBDB;
                border-radius: 0 0 0 3px;
            }

            .SumoSelect > .optWrapper.multiple > .MultiControls > p.btnCancel {
                border-radius: 0 0 3px 0;
                /* margin-bottom:5px;*/
            }


        /*styling for select on popup mode*/

        .SumoSelect > .optWrapper.isFloating > .options li.opt {
            padding: 12px 6px;
        }


        /*styling for only multiple select on popup mode*/

        .SumoSelect > .optWrapper.multiple.isFloating > .options li.opt {
            padding-left: 35px;
        }

        .SumoSelect > .optWrapper.multiple.isFloating {
            /*padding-bottom: 43px;*/
        }

        .SumoSelect > .optWrapper.multiple > .options li.opt.selected span i,
        .SumoSelect .select-all.selected > span i,
        .SumoSelect .select-all.partial > span i {
            background-color: rgb(5, 130, 183);
            box-shadow: none;
            border-color: transparent;
            background-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAGCAYAAAD+Bd/7AAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABx0RVh0U29mdHdhcmUAQWRvYmUgRmlyZXdvcmtzIENTNXG14zYAAABMSURBVAiZfc0xDkAAFIPhd2Kr1WRjcAExuIgzGUTIZ/AkImjSofnbNBAfHvzAHjOKNzhiQ42IDFXCDivaaxAJd0xYshT3QqBxqnxeHvhunpu23xnmAAAAAElFTkSuQmCC');
            background-repeat: no-repeat;
            background-position: center center;
            padding: 0px;
            margin-top: 0px;
        }


        /*disabled state*/

        .SumoSelect.disabled {
            opacity: 0.7;
            cursor: not-allowed;
        }

            .SumoSelect.disabled > .CaptionCont {
                border-color: #ccc;
                box-shadow: none;
            }


        /**Select all button**/

        .SumoSelect .select-all {
            border-radius: 3px 3px 0 0;
            position: relative;
            border-bottom: 1px solid #ddd;
            background-color: #fff;
            padding: 8px 0 3px 35px;
            height: 30px;
            cursor: pointer;
        }

            .SumoSelect .select-all > label,
            .SumoSelect .select-all > span i {
                cursor: pointer;
            }

            .SumoSelect .select-all.partial > span i {
                background-color: #ccc;
            }


        /*styling for optgroups*/

        .SumoSelect > .optWrapper > .options li.optGroup {
            padding-left: 5px;
            text-decoration: underline;
        }      
    </style>
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" ID="content2" runat="server">
          <%-- <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360" EnableHistory="true" EnablePartialRendering="true" EnablePageMethods="true">
   </asp:ScriptManager>--%>
    <act:ToolkitScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360" EnableHistory="true" EnablePartialRendering="true" EnablePageMethods="true">
    </act:ToolkitScriptManager>


    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>

            <div class="crumbs">
                <!-- Start : Breadcrumbs -->
                <ul id="breadcrumbs" class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a>Reports</a>
                    </li>
                    <li class="current">JSR Report By Application</li>
                    <div>
                        <ul>
                            <li class="title_bedcrum" style="list-style: none;">JSR REPORT BY APPLICATION</li>
                        </ul>
                    </div>
                    <div>
                        <ul style="float: right; list-style: none; margin-top: -4px; width: 265px; margin-right: -5px;" class="alert alert-danger fade in">
                            <li>
                                <span style="margin-right: -1px; margin-left: 5px; vertical-align: text-bottom;">Val In '000</span>
                                <asp:RadioButton ID="rbtn_Thousand" OnCheckedChanged="Thousand_CheckedChanged" AutoPostBack="true" GroupName="customer" runat="server" Checked="True" />
                                <span style="margin-right: 0px; margin-left: 6px; vertical-align: text-bottom;">Val In Lakhs</span>
                                <asp:RadioButton ID="rbtn_Lakhs" OnCheckedChanged="Lakhs_CheckedChanged" AutoPostBack="true" GroupName="customer" runat="server" />
                            </li>
                        </ul>
                    </div>
                </ul>
                <!-- End : Breadcrumbs -->
            </div>

            <div id="collapsebtn" class="row">
                <img id="product_image" src="images/up_arrow.png" align="left" style="margin-left: 46%;" />

            </div>

            <div class="row filter_panel" id="reportdrpdwns" runat="server">
                <div runat="server" id="cterDiv" visible="false">
                    <ul id="divCter" runat="server" class="btn-info rbtn_panel">
                        <li><span style="margin-right: 4px; vertical-align: text-bottom;">TAEGUTEC</span>

                            <asp:RadioButton ID="rdBtnTaegutec" AutoPostBack="true" Checked="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                            <span style="margin-right: 4px; margin-left: 4px; vertical-align: text-bottom;">DURACARB</span>
                            <asp:RadioButton ID="rdBtnDuraCab" AutoPostBack="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                        </li>
                    </ul>
                </div>
           
               <div class="row" style="margin-left:2px">
                    <div class="col-md-2 control" runat="server" id="div2">
                       
                            <label class="label">MONTH</label>

                            <asp:DropDownList ID="ddlMonth" runat="server" CssClass="control_dropdown" Width="180px">
                                <asp:ListItem Text="January" Value="1" />
                                <asp:ListItem Text="February" Value="2" />
                                <asp:ListItem Text="March" Value="3" />
                                <asp:ListItem Text="April" Value="4" />
                                <asp:ListItem Text="May" Value="5" />
                                <asp:ListItem Text="June" Value="6" />
                                <asp:ListItem Text="July" Value="7" />
                                <asp:ListItem Text="August" Value="8" />
                                <asp:ListItem Text="September" Value="9" />
                                <asp:ListItem Text="October" Value="10" />
                                <asp:ListItem Text="November" Value="11" />
                                <asp:ListItem Text="December" Value="12" />
                            </asp:DropDownList>

                    </div>
                    <div class="col-md-2 control" runat="server" id="divYear">
                      
                            <label class="label">YEAR</label>

                            <asp:DropDownList ID="ddlYear" runat="server" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" AutoPostBack="true" CssClass="control_dropdown" Width="180px">
                            </asp:DropDownList>

                       
                    </div>

                    <div class="col-md-2 control" runat="server" id="divBranch">
                     
                            <label class="label">BRANCH</label>

                            <asp:ListBox runat="server" CssClass="control_dropdown" ID="BranchList" SelectionMode="Multiple" AutoPostBack="true" OnSelectedIndexChanged="ChkBranches_SelectedIndexChanged" Width="180px"></asp:ListBox>


                            <%--  <asp:TextBox ID="txtbranchlist" runat="server" AutoPostBack="True" class="form-control" Style="width: 230px; background-color:white;" onkeydown="return false;"></asp:TextBox>
                            <act:PopupControlExtender ID="PopupControlExtender1" runat="server"
                                Enabled="True" ExtenderControlID="" TargetControlID="txtbranchlist" PopupControlID="panelbranch"
                                OffsetY="32">
                            </act:PopupControlExtender>
                            <asp:Panel ID="panelbranch" runat="server" Height="300px" Width="230px" BorderStyle="Solid" BorderColor="gray"
                                BorderWidth="1px" Direction="NotSet" ScrollBars="Auto" BackColor="white"
                                Style="display: none">
                                <asp:CheckBox ID="cbBranchAll" CssClass="mn_all" runat="server" AutoPostBack="true" Checked="true" Text="ALL" OnCheckedChanged="cbBranchAll_CheckedChanged"/>
                                <asp:CheckBoxList ID="ChkBranches" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ChkBranches_SelectedIndexChanged">
                                </asp:CheckBoxList>
                            </asp:Panel>--%>
                     
                    </div>


                    <div class="col-md-2 control" runat="server" id="divSE">
                       
                            <label class="label">SALES ENGINEER </label>

                            <asp:ListBox runat="server" CssClass="control_dropdown" ID="SalesEngList" SelectionMode="Multiple" OnSelectedIndexChanged="ChkSalesEng_SelectedIndexChanged" AutoPostBack="true" Width="180px"></asp:ListBox>


                            <%-- <asp:TextBox ID="TxtSalesengList" runat="server" AutoPostBack="True" class="form-control" Style="width: 230px;"  onkeydown="return false;"></asp:TextBox>
                            <act:PopupControlExtender ID="PopupControlExtender2" runat="server"
                                Enabled="True" ExtenderControlID="" TargetControlID="TxtSalesengList" PopupControlID="panelsaleseng"
                                OffsetY="32">
                            </act:PopupControlExtender>
                            <asp:Panel ID="panelsaleseng" runat="server" Height="300px" Width="230px" BorderStyle="Solid" BorderColor="gray"
                                BorderWidth="1px" Direction="NotSet" ScrollBars="Auto" BackColor="white"
                                Style="display: none;">
                                <asp:CheckBox ID="CheckSalEngAll" CssClass="mn_all" runat="server" AutoPostBack="true" Checked="true" Text="ALL" OnCheckedChanged="CheckSalEngAll_CheckedChanged"/>
                                <asp:CheckBoxList ID="ChkSalesEng" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ChkSalesEng_SelectedIndexChanged">
                                </asp:CheckBoxList>
                            </asp:Panel>--%>
                      

                    </div>
                    <div class="col-md-2 control" runat="server" id="divCT">

                            <label class="label">CUSTOMER TYPE </label>

                            <asp:DropDownList ID="ddlcustomertype" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlcustomertype_SelectedIndexChanged"
                                CssClass="control_dropdown" Width="180px">
                                <asp:ListItem Text="ALL" Selected="True" Value="ALL" />
                                <asp:ListItem Text="CUSTOMER" Value="C" />
                                <asp:ListItem Text="CHANNEL PARTNER" Value="D" />
                            </asp:DropDownList>

                     

                    </div>


                    <div class="col-md-2 control" runat="server" id="divcustomer">
                     
                            <label class="label">CUSTOMER NAME </label>

                            <asp:ListBox runat="server" CssClass="control_dropdown" ID="CustNameList" SelectionMode="Multiple" class="search-txt" OnSelectedIndexChanged="ChkCustName_SelectedIndexChanged" AutoPostBack="true" Width="180px"></asp:ListBox>

                            <%--<asp:TextBox ID="TxtCustomerName" runat="server" AutoPostBack="True" class="form-control" Style="width: 230px;"  onkeydown="return false;"></asp:TextBox>
                            <act:PopupControlExtender ID="PopupControlExtender3" runat="server"
                                Enabled="True" ExtenderControlID="" TargetControlID="TxtCustomerName" PopupControlID="PanelCustomerName"
                                OffsetY="32">
                            </act:PopupControlExtender>
                            <asp:Panel ID="PanelCustomerName" runat="server" Height="300px" Width="230px" BorderStyle="Solid" BorderColor="gray"
                                BorderWidth="1px" Direction="NotSet" ScrollBars="Auto" BackColor="white"
                                Style="display: none;">
                                <asp:CheckBox ID="ChkCustNameAll" CssClass="mn_all" runat="server" AutoPostBack="true" Checked="true" Text="ALL" OnCheckedChanged="ChkCustNameAll_CheckedChanged"/>
                                <asp:CheckBoxList ID="ChkCustName" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ChkCustName_SelectedIndexChanged">
                                </asp:CheckBoxList>
                            </asp:Panel>--%>
                    

                    </div>
           
              </div>
               
                <div class="row" style="margin-left:2px">
                    <div class="col-md-2 control">
                    
                            <label class="label">CUSTOMER NUMBER</label>

                            <asp:ListBox runat="server" CssClass="control_dropdown" ID="CustNumList" SelectionMode="Multiple" class="search-txt" OnSelectedIndexChanged="ChkCustNum_SelectedIndexChanged" AutoPostBack="true" Width="180px"></asp:ListBox>

                            <%--<asp:TextBox ID="TxtCustomerNum" runat="server" AutoPostBack="True" class="form-control" Style="width: 230px;"  onkeydown="return false;"></asp:TextBox>
                            <act:PopupControlExtender ID="PopupControlExtender4" runat="server"
                                Enabled="True" ExtenderControlID="" TargetControlID="TxtCustomerNum" PopupControlID="PanelCustomerNum"
                                OffsetY="32">
                            </act:PopupControlExtender>
                            <asp:Panel ID="PanelCustomerNum" runat="server" Height="300px" Width="230px" BorderStyle="Solid" BorderColor="gray"
                                BorderWidth="1px" Direction="NotSet" ScrollBars="Auto" BackColor="white"
                                Style="display: none;">
                                <asp:CheckBox ID="ChkCustNumAll" CssClass="mn_all" runat="server" AutoPostBack="true" Checked="true" Text="ALL" OnCheckedChanged="ChkCustNumAll_CheckedChanged"/>
                                <asp:CheckBoxList ID="ChkCustNum" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ChkCustNum_SelectedIndexChanged">
                                </asp:CheckBoxList>
                            </asp:Panel>--%>
                     
                    </div>

                    <div class="col-md-2 control">
                      
                            <label class="label">PRODUCT GROUP</label>

                            <asp:ListBox runat="server" CssClass="control_dropdown" ID="ProductGroupList" SelectionMode="Multiple" OnSelectedIndexChanged="ChkProductGroup_SelectedIndexChanged" AutoPostBack="true" Width="180px" style="margin-left: 2px;"
></asp:ListBox>

                            <%--  <asp:TextBox ID="TxtProductGrp" runat="server" AutoPostBack="True" class="form-control" Style="width: 230px;"  onkeydown="return false;"></asp:TextBox>
                            <act:PopupControlExtender ID="PopupControlExtender5" runat="server"
                                Enabled="True" ExtenderControlID="" TargetControlID="TxtProductGrp" PopupControlID="PanelProductGrp"
                                OffsetY="32">
                            </act:PopupControlExtender>
                            <asp:Panel ID="PanelProductGrp" runat="server" Height="150px" Width="230px" BorderStyle="Solid" BorderColor="gray"
                                BorderWidth="1px" Direction="NotSet" ScrollBars="Auto" BackColor="white"
                                Style="display: none;">
                                <asp:CheckBox ID="ChkProductGrpAll" CssClass="mn_all" runat="server" AutoPostBack="true" Checked="true" Text="ALL" OnCheckedChanged="ChkProductGrpAll_CheckedChanged"/>
                                <asp:CheckBoxList ID="ChkProductGroup" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ChkProductGroup_SelectedIndexChanged">
                                <asp:ListItem Value="GOLD" Text="GOLD"></asp:ListItem>
                                <asp:ListItem Value="BB" Text="BB"></asp:ListItem>
                                <asp:ListItem Value="5YRS" Text="5YRS"></asp:ListItem>
                                <asp:ListItem Value="SPC" Text="SPC"></asp:ListItem>
                                <asp:ListItem Value="TOP" Text="TOP"></asp:ListItem>
                                </asp:CheckBoxList>
                            </asp:Panel>--%>
                    
                    </div>
                    <div class="col-md-2 control ">
              
                            <label class="label">PRODUCT FAMILY</label>

                            <%-- <asp:TextBox ID="TxtProductfamily" runat="server" AutoPostBack="True" class="form-control" Style="width: 230px;"  onkeydown="return false;"></asp:TextBox>
                            <act:PopupControlExtender ID="PopupControlExtender6" runat="server"
                                Enabled="True" ExtenderControlID="" TargetControlID="TxtProductfamily" PopupControlID="PanelProductFamily"
                                OffsetY="32">
                            </act:PopupControlExtender>
                            <asp:Panel ID="PanelProductFamily" runat="server" Height="300px" Width="230px" BorderStyle="Solid" BorderColor="gray"
                                BorderWidth="1px" Direction="NotSet" ScrollBars="Auto" BackColor="white"
                                Style="display: none;">
                                <asp:CheckBox ID="ChkProductFamilyAll" CssClass="mn_all" runat="server" AutoPostBack="true" Checked="true" Text="ALL" OnCheckedChanged="ChkProductFamilyAll_CheckedChanged"/>
                                <asp:CheckBoxList ID="ChkProductFamily" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ChkProductFamily_SelectedIndexChanged">
                                </asp:CheckBoxList>
                            </asp:Panel>--%>
                            <asp:ListBox runat="server" CssClass="control_dropdown" ID="ProductFamList" SelectionMode="Multiple" AutoPostBack="true" OnSelectedIndexChanged="ChkProductFamily_SelectedIndexChanged" Width="180px"></asp:ListBox>


                  
                    </div>
                    <div class="col-md-2 control">
             
                            <label class="label">PRODUCT SUB-GROUP</label>

                            <asp:ListBox runat="server" CssClass="control_dropdown" ID="ProdSubFamilyList" SelectionMode="Multiple" AutoPostBack="true" OnSelectedIndexChanged="ChkSubFamily_SelectedIndexChanged" Width="180px"></asp:ListBox>
                            <%--     <asp:TextBox ID="txtSubFamily" runat="server" AutoPostBack="True" class="form-control" Style="width: 230px;"  onkeydown="return false;"></asp:TextBox>
                            <act:PopupControlExtender ID="PopupControlExtender8" runat="server"
                                Enabled="True" ExtenderControlID="" TargetControlID="txtSubFamily" PopupControlID="PanelSubFamily"
                                OffsetY="32">
                            </act:PopupControlExtender>
                            <asp:Panel ID="PanelSubFamily" runat="server" Height="300px" Width="230px" BorderStyle="Solid" BorderColor="gray"
                                BorderWidth="1px" Direction="NotSet" ScrollBars="Auto" BackColor="white"
                                Style="display: none;">
                                <asp:CheckBox ID="ChkSubFamilyAll" CssClass="mn_all" runat="server" AutoPostBack="true" Checked="true" Text="ALL" OnCheckedChanged="ChkSubFamilyAll_CheckedChanged"/>
                                <asp:CheckBoxList ID="ChkSubFamily" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ChkSubFamily_SelectedIndexChanged">
                                </asp:CheckBoxList>
                            </asp:Panel>--%>
                    
                    </div>
                    <div class="col-md-2 control">
                      
                            <label class="label">APPLICATION</label>


                            <asp:ListBox runat="server" CssClass="control_dropdown" ID="AppList" SelectionMode="Multiple" AutoPostBack="true" OnSelectedIndexChanged="ChkApplicationList_SelectedIndexChanged" Width="180px" ></asp:ListBox>

                            <%-- <asp:TextBox ID="TxtApplication" runat="server" AutoPostBack="True" class="form-control" Style="width: 230px;"  onkeydown="return false;"></asp:TextBox>
                            <act:PopupControlExtender ID="PopupControlExtender7" runat="server"
                                Enabled="True" ExtenderControlID="" TargetControlID="TxtApplication" PopupControlID="PanelApplication"
                                OffsetY="32">
                            </act:PopupControlExtender>
                            <asp:Panel ID="PanelApplication" runat="server" Height="300px" Width="300px" BorderStyle="Solid" BorderColor="gray"
                                BorderWidth="1px" Direction="NotSet" ScrollBars="Auto" BackColor="white"
                                Style="display: none;">
                                <asp:CheckBox ID="ChkAppAll" CssClass="mn_all" runat="server" AutoPostBack="true" Checked="true" Text="ALL" OnCheckedChanged="ChkAppAll_CheckedChanged" />
                                <asp:CheckBoxList ID="ChkApplicationList" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ChkApplicationList_SelectedIndexChanged" >
                                </asp:CheckBoxList>
                            </asp:Panel>--%>
                        
                    </div>
                    <div class="col-md-2 control " id="divGP" runat="server" visible="false">
                     
                            <label class="control-gp " runat="server">GP% Visibility</label>

                            <asp:CheckBox runat="server" Checked="false" ID="chkGP" />

                    </div>
                
         </div>




                   <div class="col-md-12" runat="server" style="margin-top: 14px;">
                    <div class="form-group">

                        <div>
                            <asp:Button ID="reports" runat="server" Text="SUBMIT" CssClass="btn green" OnClick="reports_Click"  />
                            <asp:Button ID="collapse" runat="server" Text="COLLAPSE ALL" CssClass="btn sidelight" OnClientClick="return true;" Visible="false" />
                            <asp:Button ID="expand" runat="server" Text="EXPAND ALL" CssClass="btn sidelight" OnClientClick="return false;" Visible="false" />
                            <asp:Button ID="exportexcel" runat="server" Text="EXPORT SUMMARY" Style="width: 130px;" CssClass="btn sidelight" OnClick="exportexcel_Click" Visible="false" />
                            <asp:Button ID="exportfamily" runat="server" Text="EXPORT" CssClass="btn sidelight" OnClick="exportfamily_Click" Visible="false" />
                            <asp:Button ID="btnClear" runat="server" Text="CLEAR" CssClass="btn sidelight" OnClick="btnClear_Click" />
                            <%--<asp:Button ID="exportpdf" runat="server" Text="Export In PDF" CssClass="btn green" OnClick="exportpdf_Click" />--%>
                            <label id="alertmsg" style="display: none; font-weight: bold; color: #0582b7;">Now click on Filter to view results</label>

                        </div>
                    </div>
                </div>
                        </div>
            <br />

            <div style="float: left; padding-right: 10px; padding-left: 10px; width: 100%;">
                <asp:Label ID="lblResult" runat="server" Style="color: red; font-size: 12;"></asp:Label>

                <asp:GridView ID="grdviewAllValues" runat="server" AutoGenerateColumns="False" ShowHeader="false" Width="100%" OnRowCommand="grdviewAllValues_RowCommand">
                    <Columns>
                        <asp:TemplateField>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <div>
                                    <asp:Image ID="Image1" runat="server" ImageUrl="~/images/button_plus.gif" Visible='<%# (Eval("sumFlag").ToString() == "SubFamilyHeading") || (Eval("sumFlag").ToString() == "FamilyHeading") || (Eval("sumFlag").ToString() == "HidingHeading") %>' ImageAlign="Left" />
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="GOLD">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblgold" Visible="false" Text='<%#( Eval("GOLD_FLAG").ToString()=="" ?"" :"GOLD") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="TOP">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbltop" visible="false" Text='<%# (Eval("TOP_FLAG").ToString()=="" ?"" :"TOP") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="5yrs">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl5yrs" visible="false" Text='<%# (Eval("FIVE_YEARS_FLAG").ToString()=="" ?"" :"5yrs") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="BB">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblbb" visible="false" Text='<%# (Eval("BB_FLAG").ToString()=="" ?"" :"BB") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="SPC">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblspc" visible="false" Text='<%#( Eval("SPC_FLAG").ToString()=="" ?"" :"SPC") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="10yrs">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbltenyrs" visible="false" Text='<%#( Eval("TEN_YEARS_FLAG").ToString()=="" ?"" :"10yrs") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                       
                        <asp:TemplateField HeaderText="1" ItemStyle-Width="35px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_ins" Text="Ins." Visible='<%# (Eval("I_T_O_FLAG").ToString() =="I" ) %>'></asp:Label>
                                <asp:Label runat="server" ID="lbl_tool" Text="P" Visible='<%# (Eval("I_T_O_FLAG").ToString() =="P" ) %>'></asp:Label>
                                <asp:Label runat="server" ID="Label1" Text="S" Visible='<%# (Eval("I_T_O_FLAG").ToString() =="S" ) %>'></asp:Label>
                                <asp:Label runat="server" ID="Label2" Text="Tools" Visible='<%# (Eval("I_T_O_FLAG").ToString() =="T" ) %>'></asp:Label>
                                <asp:Label runat="server" ID="Label3" Text="X" Visible='<%# (Eval("I_T_O_FLAG").ToString() =="X" ) %>'></asp:Label>
                                <asp:Label runat="server" ID="Label4" Text="" Visible='<%# (Eval("I_T_O_FLAG").ToString() =="" ) %>'></asp:Label>
                                <%--<asp:Label runat="server" ID="Label5" Text='<%# (Eval("SUBFAMILY_NAME").ToString()) %>' Visible='<%# (Eval("I_T_O_FLAG").ToString() =="SubFamHeading") %>'></asp:Label>  --%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblproductcode" Text='<%# Eval("APPLICATION_CODE".ToString()) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="T_CLAMP_PARTING_OFF ffffffffffffffffffffffff" ItemStyle-Width="140px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <%--<asp:LinkButton runat="server">--%>
                                <asp:LinkButton Font-Underline="true" ForeColor="#29AAe1" runat="server" CommandArgument='<%# (Eval("FAMILY_ID").ToString()) %>' CommandName="FamilyId" ID="lblFmilyName" Text='<%# (Eval("FAMILY_NAME").ToString()) %>' Visible='<%#Eval("sumFlag").ToString() == "FamilyHeading" || Eval("sumFlag").ToString() == "HidingHeading"  %>' CssClass="productLabel"></asp:LinkButton>
                                <asp:LinkButton Font-Underline="true" ForeColor="White" runat="server" CommandArgument='<%# (Eval("SUBFAMILY_ID").ToString()) %>' CommandName="SubFamilyId" ID="lblSubFamily" Text='<%# (Eval("SUBFAMILY_NAME").ToString()) %>' Visible='<%#Eval("sumFlag").ToString() == "SubFamilyHeading"   %>' CssClass="productLabel"></asp:LinkButton>

                                <asp:LinkButton Font-Underline="true" runat="server" ID="lblProductName" CommandArgument='<%# (Eval("APPLICATION_CODE").ToString()) %>' CommandName="ApplicationCode" Text='<%# (Eval("APPLICATION_DESC").ToString()) %>' Visible='<%#Eval("sumFlag").ToString() == "products"   %>'></asp:LinkButton>
                                <asp:Label runat="server" ID="lblOthers" Text='<%# (Eval("APPLICATION_DESC").ToString()) %>' Visible='<%#Eval("sumFlag").ToString() == "sumFlag" || Eval("sumFlag").ToString() == "typeSum"  %>'></asp:Label>
                                <%--</asp:LinkButton>--%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblSumFlag" runat="server" Text='<%# Eval("sumFlag").ToString()%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="false">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="color_3" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_item_code" Text='<%# (Eval("APPLICATION_CODE").ToString()) %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="100px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_SALES_MTD_VALUE" Text='<%# (Eval("SALES_MTD_VALUE").ToString()) %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("SALES_MTD_VALUE")))? Convert.ToString(Eval("SALES_MTD_VALUE")).Substring(0,1)=="-"? System.Drawing.Color.Red : (Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum"|| Eval("sumFlag").ToString()=="MainSum")?System.Drawing.Color.Black:System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="100px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_SALES_MTD_VALUE_LY" Text='<%# (Eval("SALES_MTD_VALUE_LY").ToString()) %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("SALES_MTD_VALUE_LY")))? Convert.ToString(Eval("SALES_MTD_VALUE_LY")).Substring(0,1)=="-"? System.Drawing.Color.Red : (Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum"|| Eval("sumFlag").ToString()=="MainSum")?System.Drawing.Color.Black:System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="100px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_GROWTH_MONTHLY" Text='<%# (Eval("GROWTH_MONTHLY").ToString()) %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("GROWTH_MONTHLY")))? Convert.ToString(Eval("GROWTH_MONTHLY")).Substring(0,1)=="-"? System.Drawing.Color.Red : (Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum"|| Eval("sumFlag").ToString()=="MainSum")?System.Drawing.Color.Black:System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="100px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_BUDGET_MTD" Text='<%# (Eval("BUDGET_MTD").ToString()) %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("BUDGET_MTD")))? Convert.ToString(Eval("BUDGET_MTD")).Substring(0,1)=="-"? System.Drawing.Color.Red : (Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum"|| Eval("sumFlag").ToString()=="MainSum")?System.Drawing.Color.Black:System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="100px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_BUDGET_MONTHLY" Text='<%# (Eval("BUDGET_MONTHLY").ToString()) %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("BUDGET_MONTHLY")))? Convert.ToString(Eval("BUDGET_MONTHLY")).Substring(0,1)=="-"? System.Drawing.Color.Red : (Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum"|| Eval("sumFlag").ToString()=="MainSum")?System.Drawing.Color.Black:System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label Width="100px" runat="server" ID="lbl_GP_MONTHLY" Text='<%# (Eval("GP_MONTHLY").ToString()) %>' Visible='<%#(Eval("Line_desc").ToString() == "HO")?chkGP.Checked:false   %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("GP_MONTHLY")))? Convert.ToString(Eval("GP_MONTHLY")).Substring(0,1)=="-"? System.Drawing.Color.Red : (Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum"|| Eval("sumFlag").ToString()=="MainSum")?System.Drawing.Color.Black:System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="100px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_SALES_YTD_VALUE" Text='<%# (Eval("SALES_YTD_VALUE").ToString()) %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("SALES_YTD_VALUE")))? Convert.ToString(Eval("SALES_YTD_VALUE")).Substring(0,1)=="-"? System.Drawing.Color.Red : (Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum"|| Eval("sumFlag").ToString()=="MainSum")?System.Drawing.Color.Black:System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="100px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_SALES_YTD_VALUE_LY" Text='<%# (Eval("SALES_YTD_VALUE_LY").ToString()) %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("SALES_YTD_VALUE_LY")))? Convert.ToString(Eval("SALES_YTD_VALUE_LY")).Substring(0,1)=="-"? System.Drawing.Color.Red : (Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum"|| Eval("sumFlag").ToString()=="MainSum")?System.Drawing.Color.Black:System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="100px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_GROWTH_YEARLY" Text='<%# (Eval("GROWTH_YEARLY").ToString()) %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("GROWTH_YEARLY")))? Convert.ToString(Eval("GROWTH_YEARLY")).Substring(0,1)=="-"? System.Drawing.Color.Red : (Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum"|| Eval("sumFlag").ToString()=="MainSum")?System.Drawing.Color.Black:System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="100px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_BUDGET_YTD" Text='<%# (Eval("BUDGET_YTD").ToString()) %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("BUDGET_YTD")))? Convert.ToString(Eval("BUDGET_YTD")).Substring(0,1)=="-"? System.Drawing.Color.Red :(Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum"|| Eval("sumFlag").ToString()=="MainSum")?System.Drawing.Color.Black: System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="100px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_BUDGET_YEARLY" Text='<%# (Eval("BUDGET_YEARLY").ToString()) %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("BUDGET_YEARLY")))? Convert.ToString(Eval("BUDGET_YEARLY")).Substring(0,1)=="-"? System.Drawing.Color.Red :(Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum"|| Eval("sumFlag").ToString()=="MainSum")?System.Drawing.Color.Black: System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label Width="100px" runat="server" ID="lbl_GP_YEARLY" Text='<%# (Eval("GP_YEARLY").ToString()) %>' Visible='<%#(Eval("Line_desc").ToString() == "HO")?chkGP.Checked:false   %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("GP_YEARLY")))? Convert.ToString(Eval("GP_YEARLY")).Substring(0,1)=="-"? System.Drawing.Color.Red :(Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum"|| Eval("sumFlag").ToString()=="MainSum")?System.Drawing.Color.Black: System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="100px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_SALES_MTD_QTY" Text='<%# (Eval("SALES_MTD_QTY").ToString()) %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("SALES_MTD_QTY")))? Convert.ToString(Eval("SALES_MTD_QTY")).Substring(0,1)=="-"? System.Drawing.Color.Red :(Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum"|| Eval("sumFlag").ToString()=="MainSum")?System.Drawing.Color.Black: System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="100px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_SALES_MTD_QTY_LY" Text='<%# (Eval("SALES_MTD_QTY_LY").ToString()) %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("SALES_MTD_QTY_LY")))? Convert.ToString(Eval("SALES_MTD_QTY_LY")).Substring(0,1)=="-"? System.Drawing.Color.Red :(Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum"|| Eval("sumFlag").ToString()=="MainSum")?System.Drawing.Color.Black: System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="100px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_SALES_YTD_QTY" Text='<%# (Eval("SALES_YTD_QTY").ToString()) %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("SALES_YTD_QTY")))? Convert.ToString(Eval("SALES_YTD_QTY")).Substring(0,1)=="-"? System.Drawing.Color.Red :(Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum"|| Eval("sumFlag").ToString()=="MainSum")?System.Drawing.Color.Black: System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="100px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_SALES_YTD_QTY_LY" Text='<%# (Eval("SALES_YTD_QTY_LY").ToString()) %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("SALES_YTD_QTY_LY")))? Convert.ToString(Eval("SALES_YTD_QTY_LY")).Substring(0,1)=="-"? System.Drawing.Color.Red :(Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum"|| Eval("sumFlag").ToString()=="MainSum")?System.Drawing.Color.Black: System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="100px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_GROWTH_YEARLY_QTY" Text='<%# (Eval("GROWTH_YEARLY_QTY").ToString()) %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("GROWTH_YEARLY_QTY")))? Convert.ToString(Eval("GROWTH_YEARLY_QTY")).Substring(0,1)=="-"? System.Drawing.Color.Red :(Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum"|| Eval("sumFlag").ToString()=="MainSum")?System.Drawing.Color.Black: System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_2">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>

                                <asp:Label runat="server" ID="lbl_Family_Id" Text='<%# Eval("FAMILY_ID").ToString() %>' Style="display: none"></asp:Label>
                                <asp:Label runat="server" ID="lbl_Sub_Family_Id" Text='<%# Eval("SUBFAMILY_ID").ToString() %>' Style="display: none"></asp:Label>
                                <asp:Label runat="server" ID="lblItemCode" Text='<%# (Eval("APPLICATION_CODE").ToString()) %>' Style="display: none"> </asp:Label>
                                <asp:Label runat="server" ID="lbl_SumFlag" Text='<%# Eval("sumFlag").ToString()%>' Style="display: none" />
                                <asp:HiddenField runat="server" ID="hdn_SumFlag" Value='<%# Eval("sumFlag").ToString()%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                <%--<asp:GridView ID="grdExportExcel" runat="server" Visible="false"></asp:GridView>--%>
            </div>

        </ContentTemplate>
        <Triggers>
            <%--<asp:AsyncPostBackTrigger ControlID="grdviewAllValues" EventName="RowCommand" />--%>
             <asp:AsyncPostBackTrigger ControlID="BranchList" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="SalesEngList" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ddlcustomertype" EventName="SelectedIndexChanged" />
            <asp:AsyncPostBackTrigger ControlID="ProductFamList" EventName="SelectedIndexChanged" />
             <asp:AsyncPostBackTrigger ControlID="reports"/>
            <asp:PostBackTrigger ControlID="exportexcel" />
            <asp:PostBackTrigger ControlID="exportfamily" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updateProgress" runat="server" AssociatedUpdatePanelID="UpdatePanel1">
        <ProgressTemplate>
            <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7; visibility:visible" >
                <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff" >Please wait</span>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    
</asp:Content>
