﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Admin.Master" AutoEventWireup="true" CodeBehind="AdminProfile.aspx.cs" Inherits="TaegutecSalesBudget.AdminProfile" ViewStateMode="Enabled" EnableEventValidation="false" %>

<%--<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>--%>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <%-- <link href="GridviewScroll.css" rel="stylesheet" />
       <script type="text/javascript" src="gridscroll.js"></script>  --%>
   
    <script src="js/jquery.dataTables.min.js"></script>
    <link href="css/jquery.dataTables.min.css" rel="stylesheet" />
    <link href="css/buttons.dataTables.min.css" rel="stylesheet" />
     <script type="text/javascript" src="js/app.js"></script>

     <script type="text/javascript">
         $(document).ready(function () {
           
          
             //  var engId = jEngineerId;
             //  console.log(jEngineerId);
             debugger;
             $("#MainContent_txteditempusertype").change(function () {
                 debugger;
                 if ($(this).val() == 'TM') {
                     $('#Edituser #EditBranchmultiple').show();
                     $('#Edituser  #lblEditbrnachmultiple').show();

                     $("#Edituser #MainContent_editddlBranch").hide();
                     $('#Edituser #lbleditbranch').hide();
                 }
                 else if ($(this).val() == 'BM') {

                     $("#Edituser #editbranch").show();
                     $('#Edituser #lbleditbranch').show();

                     $('#Edituser #MainContent_editddlBranch').show();


                     $('#Edituser #EditBranchmultiple').hide();
                     $('#Edituser  #lblEditbrnachmultiple').hide();
                 }
                 else if ($(this).val() == 'HO' || $(this).val() == 'Admin' || $(this).val() == 'RO' || $(this).val() == 'SE') {
                     $("#Edituser #MainContent_editddlBranch").hide();
                     $('#Edituser #lbleditbranch').hide();
                     $('#Edituser #EditBranchmultiple').hide();
                     $('#Edituser  #lblEditbrnachmultiple').hide();
                 }





             });
             //$(document).on('change', '#MainContent_ddlUserType', function() {
        

             $("#Edituser #MainContent_Cblterritorybranch td input[type='checkbox']").on('change', function () {
                 var currentId = $(this).attr('id');

                 if (this.checked) {
                     $('#' + currentId).attr('checked', 'checked');
                 } else {
                     $('#' + currentId).attr('checked', '');
                 }

             });

             $("#MainContent_txteditempnumber").attr("readonly", true);

             $('.control-label2').css({ " font-weight": "normal", "padding-top": "9px" });
          

             //$('#MainContent_lnkbtnCreateuser').bind("click", function (e) {
             //    debugger;
             //    $('#MainContent_txtEmpcode').val("");
             //    $('#MainContent_txtEmpName').val("");
             //    $('#MainContent_txtEmail').val("");
             //    if ($('#MainContent_rdBtnDuraCab').prop("checked")) {
             //        $('#MainContent_txtEmail').attr('placeholder', 'Username@duracarb-india.com');
             //    }
             //    else {
             //        $('#MainContent_txtEmail').attr('placeholder', 'Username@taegutec-india.com');
             //    }
             //    $('#MainContent_txtEmpContact').val("");
             //    dclg = $("#Createuser").dialog(
             //                {
             //                    resizable: false,
             //                    draggable: true,
             //                    modal: false,
             //                    title: "CREATE NEW USER",
             //                    width: "450",
             //                    height: "250",
             //                    open: function () {
             //                        $(".ui-widget-overlay").css("display", 'none');
             //                    },
             //                    close: function () {
             //                        $("#overlay").removeClass("ui-widget-overlay");
             //                        clearTextvalue();
             //                    }
             //                });
             //    dclg.parent().appendTo(jQuery("form:first"));

             //    e.preventDefault();
             //});

         
             //$('.editvalue').on('click',function (e) {
             //    debugger;
             //    var testid = $(this).prop('id');

             //    $('#MainContent_notexits').hide();
             //    $('#MainContent_exits').hide();
             //    $('#MainContent_mailnotexits').hide();
             //    $('#MainContent_mailexits').hide();

             //    popactive(testid);
             //    GeneratePopUp(e);
             //})

            


             $('#MainContent_txtEmail').on('change', function () {
                 $('#MainContent_mailnotexits').hide();
                 $('#MainContent_mailexits').hide();
                 var currentTextValue = $(this).val();
                 var empcodeval = $('#MainContent_txtEmpcode').val();
                 var email = currentTextValue.split('@');
                 var checkStatus = checkDuplicate(currentTextValue, 5);
                 var checkempcode = checkDuplicate(empcodeval, 0);
                 if ($('#MainContent_rdBtnTaegutec').prop("checked")) {

                     if (email[1] != 'taegutec-india.com' || checkStatus == false || checkempcode == false) {
                         $('#MainContent_mailnotexits').show();
                         $('#MainContent_btnAdd').hide();
                         $('#MainContent_notexits').show();

                     }
                     else {
                         $('#MainContent_mailexits').show();
                         $('#MainContent_btnAdd').show();
                         $('#MainContent_lblempemail').remove();
                     }
                 }
                 else if ($('#MainContent_rdBtnDuraCab').prop("checked")) {

                     if (email[1] != 'duracarb-india.com' || checkStatus == false || checkempcode == false) {
                         $('#MainContent_mailnotexits').show();
                         $('#MainContent_btnAdd').hide();
                         $('#MainContent_notexits').show();
                     }
                     else {
                         $('#MainContent_mailexits').show();
                         $('#MainContent_btnAdd').show();
                         $('#MainContent_lblempemail').remove();
                     }
                 }


             });


             $('#MainContent_txteditempmail').on('change', function () {
                 $('#MainContent_mailnotexits').hide();
                 $('#MainContent_mailexits').hide();
                 var currentTextValue = $(this).val();
                 var empcodeval = $('#MainContent_txteditEmpcode').val();
                 var email = currentTextValue.split('@');
                 var checkStatus = checkDuplicate(currentTextValue, 5);
                 var checkempcode = checkDuplicate(empcodeval, 0);
                 if ($('#MainContent_rdBtnTaegutec').prop("checked")) {

                     if (email[1] != 'taegutec-india.com' || checkStatus == false || checkempcode == false) {
                         $('#MainContent_mailnotexits').show();
                         $('#MainContent_btnAdd').hide();
                         $('#MainContent_notexits').show();

                     }
                     else {
                         $('#MainContent_mailexits').show();
                         $('#MainContent_btnAdd').show();
                         $('#MainContent_lblempemail').remove();
                     }
                 }
                 else if ($('#MainContent_rdBtnDuraCab').prop("checked")) {

                     if (email[1] != 'duracarb-india.com' || checkStatus == false || checkempcode == false) {
                         $('#MainContent_mailnotexits').show();
                         $('#MainContent_btnAdd').hide();
                         $('#MainContent_notexits').show();
                     }
                     else {
                         $('#MainContent_mailexits').show();
                         $('#MainContent_btnAdd').show();
                         $('#MainContent_lblempemail').remove();
                     }
                 }


             });
            

             $('#MainContent_ddlUserType').on('change', function () {
                 debugger;
                 if ($(this).val() == 'TM') {
                     $('#Branchmultiple').show();
                     $('#Branch').hide();
                 }
                 else if ($(this).val() == 'BM') {
                     $('#Branch').show();
                     $('#Branchmultiple').hide();
                 }
                 else if ($(this).val() == 'HO' || $(this).val() == 'Admin' || $(this).val() == 'RO') {
                     $('#Branch').hide();
                     $('#Branchmultiple').hide();
                 }
             });


         });

         function addbtn(id) {
           //  $('#MainContent_txtEmpcode').on('change', function () {
                 debugger;
                 $('#MainContent_notexits').hide();
                 $('#MainContent_exits').hide();
                 var currentTextValue = $('#' + id).val();
                 var checkStatus;
                 // var checkStatus = checkDuplicate(currentTextValue, 0);
                 if ($.inArray(currentTextValue, jEngineerId) != -1) {
                     checkStatus = false;
                     // found it
                 }
                 else {
                     checkStatus = true;
                 }
                 if (checkStatus == false) {
                     $('#MainContent_notexits').show();
                     $('#MainContent_btnAdd').hide();

                 }
                 else {
                     $('#MainContent_exits').show();
                     $('#MainContent_btnAdd').show();
                 }
            // });
         }

         function ddlchange(id) {
            
                 debugger;
                 if ($('#' + id).val() == 'TM') {
                     $('#Branchmultiple').show();
                     $('#Branch').hide();
                 }
                 else if ($('#' + id).val() == 'BM') {
                     $('#Branch').show();
                     $('#Branchmultiple').hide();
                 }
                 else if ($('#' + id).val() == 'HO' || $('#' + id).val() == 'Admin' || $('#' + id).val() == 'RO') {
                     $('#Branch').hide();
                     $('#Branchmultiple').hide();
                 }

           
         }

         function ShowCreateUser(e) {
             $('#MainContent_txtEmpcode').val("");
             $('#MainContent_txtEmpName').val("");
             $('#MainContent_txtEmail').val("");
             if ($('#MainContent_rdBtnDuraCab').prop("checked")) {
                 $('#MainContent_txtEmail').attr('placeholder', 'Username@duracarb-india.com');
             }
             else {
                 $('#MainContent_txtEmail').attr('placeholder', 'Username@taegutec-india.com');
             }
             $('#MainContent_txtEmpContact').val("");
             dclg = $("#Createuser").dialog(
                         {
                             resizable: false,
                             draggable: true,
                             modal: false,
                             title: "CREATE NEW USER",
                             width: "450",
                             height: "250",
                             open: function () {
                                 $(".ui-widget-overlay").css("display", 'none');
                             },
                             close: function () {
                                 $("#overlay").removeClass("ui-widget-overlay");
                                 clearTextvalue();
                             }
                         });
             dclg.parent().appendTo(jQuery("form:first"));
             e.preventDefault();
         }

         function ShowEditUser(e,id) {
             debugger;
             //var testid = $(this).prop('id');
             var testid = id;
             $('#MainContent_notexits').hide();
             $('#MainContent_exits').hide();
             $('#MainContent_mailnotexits').hide();
             $('#MainContent_mailexits').hide();

             popactive(testid);
             GeneratePopUp(e,testid);
         }

         function GeneratePopUp(e,testid) {
             debugger;
             dclg = $("#Edituser").dialog(
                            {
                                resizable: false,
                                draggable: true,
                                modal: true,
                                title: "Edit User Info",
                                width: "450",
                                height: "250",

                                open: function () {
                                    debugger;
                                    $(this).dialog('open');
                                    var empbranchcode = checkDuplicateEditbranchcode(3, testid);
                                    var empbranch = empbranchcode.split(',');
                                    $.each(empbranch, function (index, value) {
                                        $("#MainContent_Cblterritorybranch td input").each(function () {
                                            if ($(this).val() == value) {
                                                $(this).attr('checked', 'checked');

                                            }

                                        });

                                    });
                                },
                                close: function () {
                                    $(this).dialog('close');
                                    $(".ui-widget-overlay").css('background', 'none');
                                    window.location.reload(true);

                                },

                            });

             dclg.parent().appendTo(jQuery("form:first"));
             debugger;
             e.preventDefault();

             dclg.find("#MainContent_EditAdd").on('click', function (event) {

                 debugger;
                 dclg.find("#MainContent_Cblterritorybranch td input[type=checkbox]").each(function () {

                     var currentId = $(this).attr('id');
                     if ($(this).is(':checked')) {

                     } else {
                         $("#Edituser #MainContent_Cblterritorybranch td #" + currentId).attr('checked', false);
                     }

                 });
             });

         }

         function bindGridView() {
             debugger;
             var head_content = $('#MainContent_GridEngInfo tr:first').html();
             $('#MainContent_GridEngInfo').prepend('<thead></thead>')
             $('#MainContent_GridEngInfo thead').html('<tr>' + head_content + '</tr>');
             $('#MainContent_GridEngInfo tbody tr:first').hide();
             $('#MainContent_GridEngInfo').DataTable(
                {
                    "info": false,
                });
         }

         function checkDuplicateEditBranch(columnId, testid) {
             debugger;
             var table = $('#MainContent_GridEngInfo').DataTable();
             var split = testid.split('_');
             var sleng = split.length - 1;
             var spliid = parseInt(split[sleng]) + 1;
             //var value = $.trim($('#MainContent_GridEngInfo tr:eq(' + spliid + ') td:eq(' + columnId + ')').text());
             var data = table.row(spliid).data();
             var value = data[columnId].substring(data[columnId].indexOf("<span>") + 6, data[columnId].lastIndexOf("</span>"));
             return value;
         }

         function checkDuplicateEdit(columnId, testid) {
             
             var table = $('#MainContent_GridEngInfo').DataTable();
             var split = testid.split('_');
             var sleng = split.length - 1;
             var spliid = parseInt(split[sleng]) + 1;
             //var value = $.trim($('#MainContent_GridEngInfo tr:eq(' + spliid + ') td:eq(' + columnId + ')').text());
             var data = table.row(spliid).data();
             var value = data[columnId].substring(data[columnId].indexOf(">") + 1, data[columnId].lastIndexOf("<"));
             return value;
         }

         function checkDuplicateEditbranchcode(columnId, testid) {

             var split = testid.split('_');
             var sleng = split.length - 1;
             var spliid = parseInt(split[sleng]) + 1;
             var value = $.trim($('#MainContent_GridEngInfo tr:eq(' + spliid + ') td:eq(' + columnId + ') input').attr('value'));
             return value;
         }

         function checkDuplicateEditstatus(columnId, testid) {
             
             var table = $('#MainContent_GridEngInfo').DataTable();
             var split = testid.split('_');
             var sleng = split.length - 1;
             var spliid = parseInt(split[sleng]) + 1;
             var value = $.trim($('#MainContent_GridEngInfo tr:eq(' + spliid + ') td:eq(' + columnId + ') input').attr('id'));
             var data = table.row(spliid).data();
             var value = data[columnId];
             if (value.indexOf("Active") > 0) {
                 return true;
             }
             else {
                 return false;
             }
         }

         function popactive(testid) {
           //  debugger;
             var empnumber = checkDuplicateEdit(0, testid);
             var empbranchcode = checkDuplicateEditbranchcode(3, testid);
             var empname = checkDuplicateEdit(1, testid);
             var empmail = checkDuplicateEdit(5, testid);
             var empusertype = checkDuplicateEdit(2, testid);
             var empcontact = checkDuplicateEdit(4, testid);
             var empstatus = checkDuplicateEditstatus(6, testid);
             var  empbranch = checkDuplicateEditBranch(3, testid);

             if (empstatus == true) {
                 $('#MainContent_txteditempstatus').val("1");
             }
             else {
                 $('#MainContent_txteditempstatus').val("0");
             }
             if (empusertype == "HEAD OFFICE") {
                 $('#MainContent_txteditempusertype').val("HO");
             }
             else if (empusertype == "Admin") {
                 $('#MainContent_txteditempusertype').val("ADMIN");
             }
             else if (empusertype == "TERRITORY MANAGER") {
                 debugger;
                 $('#MainContent_txteditempusertype').val("TM");
                 $('#Edituser #EditBranchmultiple').show();
                 $('#Edituser  #lblEditbrnachmultiple').show();

             }
             else if (empusertype == "BRANCH MANGER") {
                 $('#MainContent_txteditempusertype').val("BM");
                 $('#editbranch').show();
             }

             else if (empusertype == "SALES ENGINEER") {
                 $('#MainContent_txteditempusertype').val("SE");
                 $('#editbranch').show();
             }
             $('#MainContent_txteditempnumber').val(empnumber);
             $('#MainContent_txteditempname').val(empname);
             $('#MainContent_txteditempmail').val(empmail);
             $('#MainContent_txteditempcontact').val(empcontact);
             var newOption = $('<option value="' + empbranchcode + '">' + empbranch + '</option>');
             $("#MainContent_editddlBranch").append(newOption);
             $('#MainContent_editddlBranch option:contains(' + empbranch + ')').prop('selected', true);
         }

         function findminofscreenheight(a, b) {
             a = a - $("#divfilter").offset().top;
             return a < b ? a : b;
         }

         function Confirm() {
             debugger;
             var confirm_value = document.createElement("INPUT");
             confirm_value.type = "hidden";
             confirm_value.name = "confirm_value";
             if (confirm("Do you want to delete the  Record?")) {
                 confirm_value.value = "Yes";
             } else {
                 confirm_value.value = "No";      
             }
             window.location.reload(true);
             document.forms[0].appendChild(confirm_value);
         //    e.preventDefault();
         }

         function ConfirmBranch() {
             var confirm_valueBranch = document.createElement("INPUT");
             confirm_valueBranch.type = "hidden";
             confirm_valueBranch.name = "confirm_valueBranch";
             if (confirm("Do you want to Deallocate this Branch?")) {
                 confirm_valueBranch.value = "Yes";
             } else {
                 confirm_valueBranch.value = "No";
             }
             document.forms[0].appendChild(confirm_valueBranch);
             e.preventDefault();
         }

         function confirmEmailNotification() {
             debugger;
             var confirm_emailNotification = document.createElement("INPUT");
             confirm_emailNotification.type = "hidden";
             confirm_emailNotification.name = "confirm_emailNotification";
             if (confirm("Do you want to send login  credentials to all active users?")) {
                 confirm_emailNotification.value = "Yes";
             } else {
                 confirm_emailNotification.value = "No";
               
                // return false;

             }
             document.forms[0].appendChild(confirm_emailNotification);
           //  e.preventDefault();
         }

         function isNumberKey(e) {
             if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) { return false; }
             return true;
         }

         function Reset(e) {
             clearTextvalue();
             return false;
         }

         function EditReset(e) {
             var value = "";
             $('#MainContent_txteditempname').val(value);
             $('#MainContent_txteditempstatus').val("1");
             $('#MainContent_txteditempusertype').val("RO");
             $("#Edituser #MainContent_editddlBranch").hide();
             $('#Edituser #lbleditbranch').hide();
             $('#Edituser #EditBranchmultiple').hide();
             $('#Edituser  #lblEditbrnachmultiple').hide();
             $("#Edituser #editbranch").hide();
             return false;
         }

         $('#MainContent_txtEmail').on('change', function () {
             $('#MainContent_mailnotexits').hide();
             $('#MainContent_mailexits').hide();
             var currentTextValue = $(this).val();
             var empcodeval = $('#MainContent_txtEmpcode').val();
             var email = currentTextValue.split('@');
             var checkStatus = checkDuplicate(currentTextValue, 5);
             var checkempcode = checkDuplicate(empcodeval, 0);
             if ($('#MainContent_rdBtnTaegutec').prop("checked")) {

                 if (email[1] != 'taegutec-india.com' || checkStatus == false || checkempcode == false) {
                     $('#MainContent_mailnotexits').show();
                     $('#MainContent_btnAdd').hide();
                     $('#MainContent_notexits').show();

                 }
                 else {
                     $('#MainContent_mailexits').show();
                     $('#MainContent_btnAdd').show();
                     $('#MainContent_lblempemail').remove();
                 }
             }
             else if ($('#MainContent_rdBtnDuraCab').prop("checked")) {

                 if (email[1] != 'duracarb-india.com' || checkStatus == false || checkempcode == false) {
                     $('#MainContent_mailnotexits').show();
                     $('#MainContent_btnAdd').hide();
                     $('#MainContent_notexits').show();
                 }
                 else {
                     $('#MainContent_mailexits').show();
                     $('#MainContent_btnAdd').show();
                     $('#MainContent_lblempemail').remove();
                 }
             }


         });


         $('#MainContent_txteditempmail').on('change', function () {
             $('#MainContent_mailnotexits').hide();
             $('#MainContent_mailexits').hide();
             var currentTextValue = $(this).val();
             var empcodeval = $('#MainContent_txteditEmpcode').val();
             var email = currentTextValue.split('@');
             var checkStatus = checkDuplicate(currentTextValue, 5);
             var checkempcode = checkDuplicate(empcodeval, 0);
             if ($('#MainContent_rdBtnTaegutec').prop("checked")) {

                 if (email[1] != 'taegutec-india.com' || checkStatus == false || checkempcode == false) {
                     $('#MainContent_mailnotexits').show();
                     $('#MainContent_btnAdd').hide();
                     $('#MainContent_notexits').show();

                 }
                 else {
                     $('#MainContent_mailexits').show();
                     $('#MainContent_btnAdd').show();
                     $('#MainContent_lblempemail').remove();
                 }
             }
             else if ($('#MainContent_rdBtnDuraCab').prop("checked")) {

                 if (email[1] != 'duracarb-india.com' || checkStatus == false || checkempcode == false) {
                     $('#MainContent_mailnotexits').show();
                     $('#MainContent_btnAdd').hide();
                     $('#MainContent_notexits').show();
                 }
                 else {
                     $('#MainContent_mailexits').show();
                     $('#MainContent_btnAdd').show();
                     $('#MainContent_lblempemail').remove();
                 }
             }


         });

         function checkDuplicate(textValue, columnId) {
             var rowCount = $('#MainContent_GridEngInfo tr').length;
             for (i = 1; i < rowCount; i++) {
                 var columnOne = $.trim($('#MainContent_GridEngInfo tr:eq(' + i + ') td:eq(' + columnId + ')').text());


                 if (columnOne == textValue) {
                     return false;
                 }
             }
             return true;
         }

         function clearTextvalue() {
             var value = "";
             $('#MainContent_txtEmpContact').val(value);
             $('#MainContent_txtEmpcode').val(value);
             $('#MainContent_txtEmpName').val(value);
             $('#MainContent_txtEmail').val(value);
             $('#MainContent_ddlUserType').val("RO");
             $('#MainContent_ddlstatus').val("1");
             $('#Branch').hide();
             $('#Branchmultiple').hide();
             $('#MainContent_notexits').hide();
             $('#MainContent_exits').hide();
             $('#MainContent_mailnotexits').hide();
             $('#MainContent_mailexits').hide();
         }

         $("#MainContent_txteditempusertype").change(function () {
             debugger;
             if ($(this).val() == 'TM') {
                 $('#Edituser #EditBranchmultiple').show();
                 $('#Edituser  #lblEditbrnachmultiple').show();

                 $("#Edituser #MainContent_editddlBranch").hide();
                 $('#Edituser #lbleditbranch').hide();
             }
             else if ($(this).val() == 'BM') {

                 $("#Edituser #editbranch").show();
                 $('#Edituser #lbleditbranch').show();

                 $('#Edituser #MainContent_editddlBranch').show();


                 $('#Edituser #EditBranchmultiple').hide();
                 $('#Edituser  #lblEditbrnachmultiple').hide();
             }
             else if ($(this).val() == 'HO' || $(this).val() == 'Admin' || $(this).val() == 'RO' || $(this).val() == 'SE') {
                 $("#Edituser #MainContent_editddlBranch").hide();
                 $('#Edituser #lbleditbranch').hide();
                 $('#Edituser #EditBranchmultiple').hide();
                 $('#Edituser  #lblEditbrnachmultiple').hide();
             }
         });

         function submit() {
             debugger;
             bindGridView();
             //$('#form1').submit();
         }

         function Cancel() {
            // window.location.reload(true);
         }

         function Disable() {
             return false;
         }

         function Validation() {
             debugger;
             var id = $('#MainContent_txtEmpcode').val();
             var name = $('#MainContent_txtEmpName').val();
             var contact = $('#MainContent_txtEmpContact').val();
             var emial = $('#MainContent_txtEmail').val();
             var usertype = $('#MainContent_ddlUserType').val();
             if (id == "") {
                 $('#MainContent_lblempnumberid').text('Enter the Employee number');
                 return false;
             }
             else {
                 $('#MainContent_lblempnumberid').remove();
             }

             if (name == "") {
                 $('#MainContent_lblempname').text('Enter the Employee name');
                 return false;
             }
             else {
                 $('#MainContent_lblempname').remove();
             }

             if (emial == "") {
                 $('#MainContent_lblempemail').text('Enter the Employee Mail');
                 return false;
             }
             else {
                 $('#MainContent_lblempemail').remove();
             }

             emial = emial.split('@');
             if (!ValidateEmail($("#MainContent_txtEmail").val())) {
                 $('#MainContent_lblempemail').text('Enter the valid email');
                 return false;
             }
             else {
                 $('#MainContent_lblempemail').remove();
                 if (emial[1] != 'taegutec-india.com' && $('#MainContent_rdBtnTaegutec').prop("checked")) {
                     $('#MainContent_lblempemail').text('Enter the  valid email');
                     return false;
                 }
                 else if (emial[1] != 'duracarb-india.com' && $('#MainContent_rdBtnDuraCab').prop("checked")) {
                     $('#MainContent_lblempemail').text('Enter the  valid email');
                     return false;
                 }
                 else {
                     $('#MainContent_lblempemail').remove();
                 }
             }
             if (contact == "") {
                 $('#MainContent_contactvalidation').text('Enter the contact number');
                 return false;
             }
             else {
                 $('#MainContent_contactvalidation').remove();

             }
             if (usertype == "RO") {
                 $('#MainContent_Usertypevalidation').text('Select the User Type');
                 return false;
             }
             else {
                 $('#MainContent_Usertypevalidation').remove();
                 return true;
             }

         }

         function EditValidation() {

             debugger;
             var name = $('#MainContent_txteditempname').val();
             //var contact = $('#Edituser #MainContent_txteditempcontact').val();
             var emial = $('#MainContent_txteditempmail').val();
             var usertype = $('#MainContent_txteditempusertype').val();


      

                 var branch =    $('#Edituser #MainContent_Cblterritorybranch td input[type="checkbox"]:checked').serialize();
         
             if (name == "") {
                 $('#Edituser #MainContent_lbltxteditempname').text('Enter the Employee name');
                 return false;
             }
             else {
                 $('#Edituser #MainContent_lbltxteditempname').remove();
             }
             emial = emial.split('@');
             if (!ValidateEmail($("#MainContent_txteditempmail").val())) {
                 $('#Edituser #MainContent_lbltxteditempmail').text('Enter the valid email');
                 return false;
             }
             else {
                 $('#Edituser #MainContent_lbltxteditempmail').remove();
                 if (emial[1] != 'taegutec-india.com' && $('#MainContent_rdBtnTaegutec').prop("checked")) {
                     $('#Edituser #MainContent_lbltxteditempmail').text('Enter the  valid email');
                     return false;
                 }
                 else if (emial[1] != 'duracarb-india.com' && $('#MainContent_rdBtnDuraCab').prop("checked")) {
                     $('#Edituser #MainContent_lbltxteditempmail').text('Enter the  valid email');
                     return false;
                 }
                 else {
                     $('#Edituser #MainContent_lbltxteditempmail').remove();
                 }
             }
             //if (contact == "") {
             //    $('#Edituser #MainContent_lbltxteditempcontact').text('Enter the contact number');
             //    return false;
             //}
             //else {
             //    $('#Edituser #MainContent_lbltxteditempcontact').remove();

             //}

             if ($('#MainContent_Cblterritorybranch').is(':visible')) {
                 if (branch == "") {
                     $('#Edituser #MainContent_EditUsertypevalidation').text('Please select atleast one branch');
                     return false
                 }
                 else {
                     $('#Edituser #MainContent_EditUsertypevalidation').remove();
                    // return true;
                 }
             }


             if (usertype == "RO") {
                 $('#Edituser #MainContent_EditUsertypevalidation').text('Select the User Type');
                 return false;
             }
             else {
                 $('#Edituser #MainContent_EditUsertypevalidation').remove();
                 return true;
             }

            

           
         }

         function ValidateEmail(email) {
             var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
             debugger;
             var output = re.test(email);
             console.log(output);
            // console.log(test(email));
             //  alert(email);
             debugger;
             //  return re.test(email);
             return output;


         }

         $('#MainContent_txtEmpcode').change(function () {
             if ($(this).val() == "") {
                 $('#MainContent_lblempnumberid').text('Enter the Employee number');
                 return false;
             }
             else {
                 $('#MainContent_lblempnumberid').remove();
             }

         });

         $('#MainContent_txtEmpName').change(function () {
             if ($(this).val() == "") {
                 $('#MainContent_lblempname').text('Enter the Employee name');
                 return false;
             }
             else {
                 $('#MainContent_lblempname').remove();
             }

         });

         $('#MainContent_txtEmail').change(function () {
             if ($(this).val() == "") {
                 $('#MainContent_lblempemail').text('Enter the Employee Mail');
                 return false;
             }
             else {
                 $('#MainContent_lblempemail').remove();
             }

         });

         $('#MainContent_txtEmpContact').change(function () {
             debugger;
             var contact = $(this).val();
             con = contact.length;
             if (contact == "" || contact == "0000000000" || contact == "00000000000" || contact == "000000000000") {

                 $('#MainContent_contactvalidation').text('Enter a valid Contact Number');
                 return false;
             }
             else if (con < 10) {
                 $('#MainContent_contactvalidation').text('Enter a valid Contact Number');
                 return false;
             }
             else {
                 $('#MainContent_contactvalidation').text('');
             }

         });
       
         $('#MainContent_ddlUserType').change(function () {
             debugger;
             if ($(this).val() == "RO") {
                 $('#MainContent_Usertypevalidation').text('Select the User Type');
                 return false;
             }
             else {
                 $('#MainContent_Usertypevalidation').remove();
             }
 
         }); 

         $('#MainContent_txteditempname').change(function () {
             debugger;
             if ($(this).val() == "") {
                 $('#Edituser #MainContent_lbltxteditempname').text('Enter the Employee name');
                 return false;
             }
             else {
                 $('#Edituser #MainContent_lbltxteditempname').remove();
             }

         });

         $('#MainContent_txteditempcontact').change(function () {

             var contact = $(this).val();
             con = contact.length;
             if (contact == "" || contact == "0000000000" || contact == "00000000000" || contact == "000000000000") {

                 $('#Edituser #MainContent_lbltxteditempcontact').text('Enter a valid Contact Number');
                 return false;
             }
             else if (con < 10) {
                 $('#Edituser #MainContent_lbltxteditempcontact').text('Enter a valid Contact Number');
                 return false;
             }
             else {
                 $('#Edituser #MainContent_lbltxteditempcontact').text('');
             }
         });

         $('#MainContent_txteditempusertype').change(function () {
             if ($(this).val() == "") {
                 $('#Edituser MainContent_EditUsertypevalidation').text('Select the User Type');
                 return false;
             }
             else {
                 $('#Edituser MainContent_EditUsertypevalidation').remove();
             }

         });

         $('#MainContent_txteditempmail').change(function () {
             var currentTextValue = $(this).val();
             var checkStatus = checkDuplicate(currentTextValue, 5);
             var email = $(this).val();
             email = email.split('@');
             if ($('#MainContent_rdBtnTaegutec').prop("checked")) {
                 if ((email[1] != 'taegutec-india.com') || checkStatus == false) {
                     $('#Edituser #MainContent_lbltxteditempmail').text('Enter the Valid  Email');
                     $('#Edituser #MainContent_Editmailexists').show();
                     $('#Edituser #MainContent_Editmailnotexits').hide();
                     return false;
                 }
                 else {
                     $('#Edituser #MainContent_Editmailnotexits').show();
                     $('#Edituser #MainContent_lbltxteditempmail').remove();
                     $('#Edituser #MainContent_Editmailexists').hide();
                 }
             }
             else if ($('#MainContent_rdBtnDuraCab').prop("checked")) {
                 if ((email[1] != 'duracarb-india.com') || checkStatus == false) {
                     $('#Edituser #MainContent_lbltxteditempmail').text('Enter the Valid  Email');
                     $('#Edituser #MainContent_Editmailexists').show();
                     $('#Edituser #MainContent_Editmailnotexits').hide();
                     return false;
                 }
                 else {
                     $('#Edituser #MainContent_Editmailnotexits').show();
                     $('#Edituser #MainContent_lbltxteditempmail').remove();
                     $('#Edituser #MainContent_Editmailexists').hide();
                 }
             }

         });

         function engineerId() {
             console.log(jEngineerId);
         }
    </script>
    <style>
        img#MainContent_notexits {
            margin-top: -24px;
            margin-right: 3px;
        }

        img#MainContent_mailexits {
            margin-top: -24px;
            margin-right: 3px;
        }

        img#MainContent_exits {
            margin-top: -24px;
            margin-right: 3px;
        }

        img#MainContent_mailnotexits {
            margin-top: -24px;
            margin-right: 3px;
        }

        #MainContent_GridEngInfo tr {
            vertical-align: top;
        }

        #MainContent_GridEngInfo td {
            padding-top: 10px;
        }

        .lblDisplay {
            display: none;
        }

        #MainContent_ddlBranchterritary input[type="checkbox"] {
            line-height: normal !important;
            margin: 4px 6px 0 !important;
        }

        #Edituser #MainContent_Cblterritorybranch input[type="checkbox"] {
            line-height: normal !important;
            margin: 4px 6px 0 !important;
        }
    </style>
    <style type="text/css">
        .hideelement {
            display: none;
        }

        .showelement {
            display: block;
        }

        td {
            height: 8px !important;
            padding-left: 5px !important;
            text-align: left !important;
        }


        th {
            /*background: #006780;
            color: #fff;
            font-weight: 600;
            font-size: 13px;
            border-color: #fff;
            height: 40px;
            padding-left: 10px;
            text-align: left;*/
            background: #ebeef5;
            color: #fff;
            font-weight: 600;
            text-align: left;
            border-color: #ebeef5;
            font-size: 12px;
        }

        .title_new {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
            font-weight: bold;
            /* text-decoration: underline; */
            text-align: center;
            padding: 10px;
            background: #999;
            color: #fff;
            text-transform: uppercase;
        }

        .control-label2 {
            font-weight: normal;
            padding-top: 9px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:ScriptManager ID="ScriptManager11" runat="server" EnableHistory="true" EnablePartialRendering="true" EnablePageMethods="true" AsyncPostBackTimeout="360">
    </asp:ScriptManager>

    <asp:UpdatePanel ID="UpdatePanel11" runat="server">
        <ContentTemplate>
            <div class="crumbs">
                <!-- Start : Breadcrumbs -->
                <ul id="breadcrumbs" class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a>Admin</a>
                    </li>


                    <li class="current">User Profile</li>
                    <div>

                        <li class="title_bedcrum" id="tittle" runat="server" style="list-style: none;">USER PROFILE</li>

                    </div>

                    <div class="col-md-2 " style="width: 160px !important; float: right">

                        <div class="form-group">
                            <asp:LinkButton runat="server" ID="lnkbtnCreateuser" Text="CREATE NEW USER" ForeColor="#0066FF" OnClientClick="ShowCreateUser(event);"
                                Font-Underline="True" Style="padding-left: 20px"></asp:LinkButton>
                        </div>
                    </div>


                </ul>

            </div>
            <!-- End : Breadcrumbs -->
            <div class="col-lg-12" style="padding-top: 8px;">
                <div style="float: left;">

                    <ul class="btn-info rbtn_panel">
                        <li><span style="margin-right: 4px;">TAEGUTEC</span>

                            <asp:RadioButton ID="rdBtnTaegutec" AutoPostBack="true" Checked="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                            <span style="margin-right: 4px; margin-left: 4px;">DURACARB</span>
                            <asp:RadioButton ID="rdBtnDuraCab" AutoPostBack="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged" GroupName="byCmpnyCodeInradiobtn" runat="server" />
                        </li>
                    </ul>
                </div>
                <div style="float: right;">
                    <asp:Button ID="btnNotifctn" Style="float: right; width: 200px" runat="server" Text="SEND EMAIL NOTIFICATION" OnClientClick="confirmEmailNotification()" OnClick="btnNotifctn_Click" CssClass="btn green" /></div>


            </div>
            <div class="clearfix"></div>
            <div id="divfilter" class="row " style="padding-left: 15px">


                <div class="col-md-1" style="width: 152px !important">
                    <div class="form-group">

                        <asp:CheckBox ID="CheckBoxAdmin" CssClass="col-md-2" Style="margin-top: 6px" runat="server" />

                        <label class="col-md-10 control-label2  ">ADMIN</label>

                    </div>
                </div>

                <div class="col-md-2" style="width: 162px !important">
                    <div class="form-group">

                        <asp:CheckBox ID="CheckBoxHo" CssClass="col-md-1" Style="margin-top: 6px" runat="server" />

                        <label class="col-md-10 control-label2  " style="padding-left: 7px">HEAD OFFICE</label>

                    </div>
                </div>
                <div class="col-md-2 " style="width: 212px !important">
                    <div class="form-group">

                        <asp:CheckBox ID="CheckBoxTM" CssClass="col-md-1" Style="margin-top: 6px" runat="server" />

                        <label class="col-md-10 control-label2  " style="padding-left: 7px">TERRITORY MANAGER</label>

                    </div>
                </div>
                <div class="col-md-2 ">
                    <div class="form-group">

                        <asp:CheckBox ID="CheckBoxBm" CssClass="col-md-1" Style="margin-top: 6px" runat="server" />

                        <label class="col-md-11 control-label2 ">BRANCH MANAGER</label>

                    </div>
                </div>
                <div class="col-md-2" style="width: 200px !important">
                    <div class="form-group">

                        <asp:CheckBox ID="CheckBoxSe" class="col-md-1" Style="margin-top: 6px" runat="server" />

                        <label class="col-md-8 control-label2 ">SALES ENGINEER</label>

                    </div>
                </div>

                <div class="col-md-2" style="width: 240px !important">
                    <div class="form-group">
                        <label class="col-md-3 control-label2 " style="width: 84px;">BRANCH</label>
                        <div class="col-md-7">

                            <asp:DropDownList runat="server" ID="ddlregiontype"
                                CssClass="form-control" Width="140px">
                            </asp:DropDownList>

                        </div>
                    </div>
                </div>
                <div class="col-md-4 " style="margin-top: 4px; width: 258px !important">
                    <div class="form-group">
                        <div>
                            <asp:Button ID="Okbtn" runat="server" Text="FILTER" OnClick="Ok_Click" CssClass="btn green" />
                            <label id="alertmsg" style="display: none; font-weight: bold; color: #0582b7;">Now click on Filter  to view results</label>
                        </div>
                    </div>
                </div>


            </div>
            <%-- GRID VIEW STARTS--%>
            <div>
                <asp:GridView ID="GridEngInfo" runat="server" Width="100%" Style="text-align: center;" AutoGenerateColumns="false" ShowHeaderWhenEmpty="true" OnRowDataBound="GridEngInfo_RowDataBound">
                    <Columns>
                        <asp:TemplateField HeaderText="ENGINEER ID">
                            <ItemTemplate>
                                <asp:Label ID="lbl_engid" runat="server" Text='<%# Eval("EngineerId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="ENGINEER NAME">
                            <ItemTemplate>
                                <asp:Label ID="lbl_name" runat="server" Text='<%# Eval("EngineerName") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="ROLE">
                            <ItemTemplate>
                                <asp:Label ID="lbl_role" runat="server" Text='<%# Eval("RoleId").ToString()=="BM" ? "BRANCH MANGER": Eval("RoleId").ToString()=="SE" ? "SALES ENGINEER": Eval("RoleId").ToString()=="HO" ? "HEAD OFFICE":Eval("RoleId").ToString()=="Admin" ? "ADMIN": Eval("RoleId").ToString()=="TM" ? "TERRITORY MANAGER":""%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="BRANCH">
                            <ItemTemplate>
                                <asp:HiddenField ID="regioncode" runat="server" Value='<%# Eval("region_code") %>' />
                                <asp:Label ID="lbl_branch" runat="server" Text='<%# Eval("region_description") %>' CssClass="lblDisplay" Visible="false"></asp:Label>
                                <asp:PlaceHolder runat="server" ID="ph_Region"></asp:PlaceHolder>
                                <asp:ImageButton ID="Deletbranch" ImageAlign="Right" ImageUrl="~/images/undelete.png" runat="server" RowIndex='<%# Container.DisplayIndex %>' Visible='<%# Eval("region_description").ToString() !=""  %>' OnClientClick="ConfirmBranch()" OnClick="Deletbranch_Click" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="CONTACT NUMBER">
                            <ItemTemplate>
                                <asp:Label ID="lbl_cntctnum" runat="server" Text='<%# Eval("PhoneNumber") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EMAIL ID">
                            <ItemTemplate>
                                <asp:Label ID="lbl_email" runat="server" Text='<%# Eval("EmailId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="LOGIN STATUS">
                            <ItemTemplate>
                                <asp:ImageButton ID="Activestatus" runat="server" ImageAlign="AbsMiddle" OnClientClick="if(!Disable()){return false;}" ImageUrl="~/images/active .png" Visible='<%# Eval("LoginStatus").ToString() =="1"  %>' />
                                <asp:ImageButton ID="Deactivestatus" runat="server" ImageUrl="~/images/deactive .png" OnClientClick="if(!Disable()){return false;}" Visible='<%# Eval("LoginStatus").ToString() =="0"  %>' />
                                <asp:ImageButton ID="Deactivestatus1" runat="server" ImageUrl="~/images/deactive .png" OnClientClick="if(!Disable()){return false;}" Visible='<%# Eval("LoginStatus").ToString() ==""  %>' />

                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EDIT/DELETE">
                            <ItemTemplate>
                                <asp:ImageButton ID="Edit" runat="server" ImageAlign="Left" ImageUrl="~/images/edit_small.png" class="editvalue" ForeColor="#0066FF" Font-Underline="True" OnClientClick="ShowEditUser(event,this.id);" />
                                <asp:ImageButton ID="Delete" runat="server" ImageAlign="Right" ImageUrl="~/images/delete_icon.png" RowIndex='<%# Container.DisplayIndex %>' OnClientClick="Confirm()" OnClick="Delete_Click1" />
                            </ItemTemplate>
                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>
            </div>

            <div id="Createuser" style="width: 100%; display: none; text-align: left !important; height: auto !important">
                <div class="form-horizontal row-border1" id="DivCreateUser">
                    <div class="form-group">
                        <label class="col-md-5 control-label">
                            Employee Number
                                      
                        </label>

                        <div class="col-md-7">
                            <asp:TextBox ID="txtEmpcode" runat="server" CssClass="form-control required email" onchange="addbtn(this.id)"></asp:TextBox>
                            <asp:Image ID="notexits" ImageUrl="~/images/x.png" runat="server" Style="display: none" ImageAlign="Right" />
                            <asp:Image ID="exits" ImageUrl="~/images/tick.png" runat="server" Style="display: none" ImageAlign="Right" />
                            <asp:Label ID="lblempnumberid" runat="server" ForeColor="Red" Style="float: right"></asp:Label>
                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">
                            Employee Name 

                        </label>

                        <div class="col-md-7">
                            <asp:TextBox runat="server" ID="txtEmpName" CssClass="form-control required"></asp:TextBox>
                            <asp:Label ID="lblempname" runat="server" ForeColor="Red" Style="float: right"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">
                            Employee Email
                                          
                        </label>

                        <div class="col-md-7">
                            <asp:TextBox runat="server" MaxLength="50" ID="txtEmail" CssClass="form-control required email" placeholder="Username@taegutec-india.com"></asp:TextBox>
                            <asp:Image ID="mailnotexits" ImageUrl="~/images/x.png" runat="server" Style="display: none" ImageAlign="Right" />
                            <asp:Image ID="mailexits" ImageUrl="~/images/tick.png" runat="server" Style="display: none" ImageAlign="Right" />
                            <asp:Label ID="lblempemail" runat="server" ForeColor="Red" Style="float: right"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">
                            Status                                 
                        </label>
                        <div class="col-md-7">
                            <asp:DropDownList ID="ddlstatus" runat="server" class="form-control">

                                <asp:ListItem Value="1">Active</asp:ListItem>
                                <asp:ListItem Value="0">Deactive</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">
                            Contact Number   
                        </label>
                        <div class="col-md-7">
                            <asp:TextBox runat="server" ID="txtEmpContact" onkeypress="return isNumberKey(event)" MaxLength="12" CssClass="form-control required email"></asp:TextBox>
                            <asp:Label ID="contactvalidation" runat="server" ForeColor="Red" Style="float: right"></asp:Label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-5 control-label">
                            User Type
                        </label>
                        <div class="col-md-7">
                            <asp:DropDownList runat="server" ID="ddlUserType" class="form-control" onchange="ddlchange(this.id)">
                                <asp:ListItem Value="RO">--Select the Role--</asp:ListItem>
                                <asp:ListItem Value="HO">Head Office</asp:ListItem>
                                <asp:ListItem Value="Admin">Admin</asp:ListItem>
                                <asp:ListItem Value="TM">Territory Manager</asp:ListItem>
                                <asp:ListItem Value="BM">Branch Manager</asp:ListItem>
                            </asp:DropDownList>
                            <asp:Label ID="Usertypevalidation" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group" style="display: none;" id="Branch">
                        <label class="col-md-5 control-label">
                            Branch
                        </label>
                        <div class="col-md-7">
                            <asp:DropDownList ID="ddlBranch" runat="server" class="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group" id="Branchmultiple" style="display: none;">
                        <label class="col-md-5 control-label">
                            Branch
                        </label>
                        <div class="col-md-7" style="height: 121px; border: 1px solid #ccc; overflow-y: scroll">
                            <asp:CheckBoxList ID="ddlBranchterritary"  runat="server" Checked="true"></asp:CheckBoxList>
                        </div>
                    </div>

                    <div class="form-actions" style="margin-bottom: -23px !important">
                        <asp:Button runat="server" ID="btnUReset" Text="Reset" CssClass="btn blue pull-right" OnClientClick="if(!Reset()){return false;}" />
                        <asp:Button runat="server" ID="btnAdd" Text="Add" Style="display: none" CssClass="btn blue pull-right" OnClick="btnAdd_Click" Autopostback="true" OnClientClick="if(!Validation()){return false;}" />
                    </div>

                </div>
            </div>

            <div id="Edituser" style="width: 100%; display: none; text-align: center !important; height: auto !important">
                <div class="form-horizontal row-border1" id="Div2">
                    <div class="form-group">
                        <label class="col-md-5 control-label">
                            Employee Number
                                       
                        </label>

                        <div class="col-md-7">
                            <asp:TextBox ID="txteditempnumber" runat="server" CssClass="form-control required email"></asp:TextBox>

                        </div>

                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">
                            Employee Name 

                        </label>

                        <div class="col-md-7">
                            <asp:TextBox runat="server" ID="txteditempname" CssClass="form-control required"></asp:TextBox>
                            <asp:Label ID="lbltxteditempname" runat="server" ForeColor="Red" Style="float: right"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">
                            Employee Email
                                          
                        </label>

                        <div class="col-md-7">
                            <asp:TextBox runat="server" MaxLength="50" ID="txteditempmail" CssClass="form-control required email" placeholder="Username@taegutec-india.com"></asp:TextBox>

                            <asp:Image ID="Editmailexists" ImageUrl="~/images/x.png" runat="server" Style="display: none" ImageAlign="Right" />
                            <asp:Image ID="Editmailnotexits" ImageUrl="~/images/tick.png" runat="server" Style="display: none" ImageAlign="Right" />
                            <asp:Label ID="lbltxteditempmail" runat="server" ForeColor="Red" Style="float: right"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">
                            Status                                 
                        </label>
                        <div class="col-md-7">
                            <asp:DropDownList ID="txteditempstatus" runat="server" class="form-control">
                                <asp:ListItem Value="1">Active</asp:ListItem>
                                <asp:ListItem Value="0">Deactive</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-5 control-label">
                            Contact Number   
                        </label>
                        <div class="col-md-7">
                            <asp:TextBox runat="server" ID="txteditempcontact" MaxLength="12" CssClass="form-control required email" onkeypress="return isNumberKey(event)"></asp:TextBox>
                            <asp:Label ID="lbltxteditempcontact" runat="server" ForeColor="Red" Style="float: right"></asp:Label>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-5 control-label">
                            User Type
                        </label>
                        <div class="col-md-7">
                            <asp:DropDownList runat="server" ID="txteditempusertype" class="form-control">
                                <asp:ListItem Value="RO">--Select the Role--</asp:ListItem>
                                <asp:ListItem Value="HO">Head Office</asp:ListItem>
                                <asp:ListItem Value="SE">Sales Engineer</asp:ListItem>
                                <asp:ListItem Value="Admin">Admin</asp:ListItem>
                                <asp:ListItem Value="TM">Territory Manager</asp:ListItem>
                                <asp:ListItem Value="BM">Branch Manager</asp:ListItem>
                            </asp:DropDownList>
                            <asp:Label ID="EditUsertypevalidation" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                    </div>
                    <div class="form-group" id="editbranch" style="display: none">
                        <label class="col-md-5 control-label" id="lbleditbranch">
                            Branch
                        </label>
                        <div class="col-md-7">
                            <asp:DropDownList ID="editddlBranch" runat="server" class="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="form-group" id="EditBranchmultiple" style="display: none">
                        <label class="col-md-5 control-label" id="lblEditbrnachmultiple">
                            Branch
                        </label>
                        <div class="col-md-7" style="height: 121px; border: 1px solid #ccc; overflow-y: scroll">
                            <asp:CheckBoxList ID="Cblterritorybranch" runat="server" Style="font-size: 12px !important;"></asp:CheckBoxList>
                        </div>
                    </div>

                    <div class="form-actions" style="margin-bottom: -23px !important">
                        <asp:Button runat="server" ID="Cancel" Text="Cancel" CssClass="btn blue pull-right" OnClick="Cancels" />
                        <asp:Button runat="server" ID="EditAdd" Text="Update" CssClass="btn blue pull-right" OnClick="EditAdd_Click" OnClientClick="if(!EditValidation()){return false;}" />
                    </div>

                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:AsyncPostBackTrigger ControlID="Okbtn" EventName="Click" />
            <asp:AsyncPostBackTrigger ControlID="btnNotifctn" EventName="Click" />

            <%--<asp:AsyncPostBackTrigger ControlID="EditAdd" EventName="Click" />--%>
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdateProgress ID="updateProgress" runat="server">
        <ProgressTemplate>
            <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">

                <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff">Please wait</span>
            </div>
        </ProgressTemplate>
    </asp:UpdateProgress>
 
    <%-- GRID VIEW END--%>
</asp:Content>
