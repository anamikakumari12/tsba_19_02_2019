﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PenetrationReport.aspx.cs" Inherits="TaegutecSalesBudget.PenetrationReport" MasterPageFile="~/Site.Master" %>

<asp:Content ContentPlaceHolderID="HeadContent" ID="Content1" runat="server">
     <style type="text/css">
        .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
            border: 1px solid #ddd;
        }

        .HeadergridAll {
            background: #006780;
            color: #fff;
            font-weight: 600;
            text-align: center;
            line-height: 40px;
        }
          .ui-datepicker-trigger {
            float: right;
            margin-top: -31px;
            padding: 8px;
            border-left: 1px solid #ccc;
            cursor: pointer;
        }
          .form-control {
    height: 32px;
    font-size: 13px;
    -webkit-border-radius: 0;
    -moz-border-radius: 0;
    border-radius: 0;
}
          .HeaderTotal {
            background: #006780;
            color: #fff;
            font-weight: 800;
            text-align: center;
            line-height: 60px;
        }
          .ValueTotal {
           color: #006780;
            font-weight: 800;
            text-align: center;
            line-height: 60px;
        }
    </style>
        <%--<link href="css/Projectsmain.css" rel="stylesheet" type="text/css" />--%>
    <%--<link href="GridviewScroll.css" rel="stylesheet" />--%>
    <%--<script type="text/javascript" src="gridscroll.js"></script>--%>
    <script type="text/javascript" src="js/app.js"></script>
</asp:Content>
<asp:Content ContentPlaceHolderID="MainContent" ID="Content2" runat="server">
	   <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360" EnableHistory="true" EnablePartialRendering="true" EnablePageMethods="true" >
   </asp:ScriptManager>
	<asp:UpdatePanel ID="UpdatePanel1" runat="server">
	  <ContentTemplate>
                      <div class="crumbs">
      <!-- Start : Breadcrumbs -->
      <ul id="breadcrumbs" class="breadcrumb">
         <li>
            <i class="fa fa-home"></i>
            <a>Reports</a>
         </li>
         <li class="current">Penetration Report</li>  
        
             <div>
                  <ul  style="float:right;list-style:none;   margin-top: -4px; width: 247px; margin-right: -5px; "  class="alert alert-danger in">
                     <li> <span style="margin-right:4px; vertical-align:text-bottom; ">Val In '000</span>                      
                        <asp:RadioButton ID="ValInThsnd" OnCheckedChanged="byValueIn_CheckedChanged" AutoPostBack="true"  Checked="true"   GroupName="byValueInradiobtn" runat="server" />
                         <span style="margin-right:4px; margin-left:4px;vertical-align:text-bottom;">Val In Lakhs</span>
                        <asp:RadioButton ID="ValInLakhs" OnCheckedChanged="byValueIn_CheckedChanged" AutoPostBack="true"   GroupName="byValueInradiobtn" runat="server" />
                     </li>
                  </ul>
               </div>
      </ul>                               
   </div>
	 <div id="collapsebtn" class="row">
                <img id="product_image" src="images/up_arrow.png" align="left" style="margin-left: 46%;" />

            </div>
		 <div class="row filter_panel" id="reportdrpdwns" runat="server">

                              <div id="divCter" runat="server" visible="false">
				  <ul class="btn-info rbtn_panel" >
					  <li> <span style="margin-right:4px;vertical-align:text-bottom;  ">TAEGUTEC</span>
					   
						<asp:RadioButton ID="rdBtnTaegutec"  AutoPostBack="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged"  Checked="true"  GroupName="byCmpnyCodeInradiobtn" runat="server" />
						 <span style="margin-right:4px; margin-left:4px;vertical-align:text-bottom;">DURACARB</span>
						<asp:RadioButton ID="rdBtnDuraCab"  AutoPostBack="true" OnCheckedChanged="rdBtnTaegutec_CheckedChanged"  GroupName="byCmpnyCodeInradiobtn" runat="server" />
					 </li>
				  </ul>
			   </div>
			 
             

		   <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-md-4 control-label"><span id="fdstar" style="color:red;">*</span>FROM DATE</label>
                        <div class="col-md-6">
                            <asp:TextBox ID="txtFromDate" runat="server" CssClass="form-control" onkeypress="return allowOnlyNumber(event);"></asp:TextBox>
                        </div>
                    </div>

                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-md-4 control-label"><span id="tostar" style="color:red;">*</span>TO DATE</label>
                        <div class="col-md-6">
                            <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control" onkeypress="return allowOnlyNumber(event);"></asp:TextBox>
                        </div>
                    </div>

                </div>

			<div class="col-md-4">
			   <div class="form-group">
				  <label class="col-md-4 control-label">FILTER TYPE </label>
				  <div class="col-md-4">
					 <asp:DropDownList ID="ddlcustomertype" runat="server" CssClass="form-control" Width="230px">
						
						<asp:ListItem Text="CUSTOMER" Value="Customer" />
						<asp:ListItem Text="CHANNEL PARTNER" Value="CP" />
						 <asp:ListItem Text="BRANCH" Value="Branches" />
						<asp:ListItem Text="PRODUCT FAMILY" Value="Product" />
						 <asp:ListItem Text="SALES ENGINEER" Value="SE" />
						
					 </asp:DropDownList>
				  </div>
			   </div>
			</div>
			<div class="col-md-4" style="margin-top:10px;">
			   <div class="form-group">
				  <label class="col-md-4 control-label"><span id="cnstar" style="color:red;">*</span>CONTRIBUTION(%) : </label>
				  <div class="col-md-4">
					 <asp:TextBox runat="server" ID="txtContribution" onkeypress="return isNumberKey(event,this);"  MaxLength="3" CssClass="form-control" Width="230px">

					 </asp:TextBox>
                     <%-- <span style="color: Red; font-size: 12px; width:230px">
                     <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ControlToValidate="txtContribution" runat="server" ErrorMessage="Only Numbers allowed" ValidationExpression="\d+"></asp:RegularExpressionValidator>--%>
                      <%--<asp:RangeValidator Width="230px" ID="RangeValidator1" runat="server" Type="Integer" MinimumValue="0" MaximumValue="100" ControlToValidate="txtContribution" ErrorMessage="Value must be a whole number between 0 and 100" />
                          </span>--%>
				  </div>
				  
			   </div>
			</div>
			
		   <div  class="col-md-4 " style="margin-top:14px;">
			   <div class="form-group">
				  <div >
					 <asp:Button ID="reports" runat="server" CausesValidation="true" OnClientClick="return checkMandatory();" Text="Fetch Report" CssClass="btn green" OnClick="reports_Click" />
					   <asp:Button ID="exportexlbtn" runat="server" OnClientClick="return checkMandatory();" Text="Export In Excel" CssClass="btn green" OnClick="exportexlbtn_Click"  />
                      <asp:Button ID="exportpdfbtn" runat="server" OnClientClick="return checkMandatory();" Text="Export In PDF" CssClass="btn green" OnClick="exportpdfbtn_Click"  />
                      <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn green" OnClick="btnClear_Click"  />
					  <label id="alertmsg" style="display:none; font-weight: bold; color: #0582b7;">Now click on Filter  to view results</label>
				  </div>
			   </div>
			</div>
			  
		 </div>
		 <div runat="server">
<%--			<div class="col-md-12">
			   <div class="portlet-body">
				  <div id="accordion" class="panel-group">
					 <div class="panel panel-default">
						 <div id="prdgroup">--%>
						   <div>
                               <br />
                               <br />
                               <asp:Label style="color: red; font-size: 14" runat="server" ID="lblResult"></asp:Label>
                               <br />
                               <table>
                                   <tr>
                                       <td>
                                           &nbsp<asp:Label runat="server" ID="lblCount" CssClass="HeaderTotal"></asp:Label>&nbsp
                               
                                       </td>
                                       <td>
                                           <asp:Label runat="server" ID="txtCount" CssClass="ValueTotal"></asp:Label>&nbsp &nbsp
                               
                                       </td>
                                       <td>
                                           &nbsp<asp:Label runat="server" ID="lblSale" CssClass="HeaderTotal"></asp:Label>&nbsp
                               
                                       </td>
                                       <td>
                                           &nbsp<asp:Label runat="server" ID="txtSale" CssClass="ValueTotal"></asp:Label>&nbsp &nbsp                               
                                       </td>
                                       <td>
                                           &nbsp<asp:Label runat="server" ID="lblContr" CssClass="HeaderTotal"></asp:Label>&nbsp
                               
                                       </td>
                                       <td>
                                           &nbsp<asp:Label runat="server" ID="txtContr" CssClass="ValueTotal"></asp:Label>&nbsp &nbsp
                               
                                       </td>
                                   </tr>
                               </table>
									<asp:GridView ID="grdCustomer" AutoGenerateColumns="false" style="float:left;margin-top:30px;margin-left: 15px;width: 70%;" runat="server" ShowHeaderWhenEmpty="true">
									     <Columns>
                                        <asp:TemplateField HeaderText="CUSTOMER NUMBER" ItemStyle-Height="30" ItemStyle-Width="20%" HeaderStyle-CssClass="HeadergridAll" SortExpression="CustNumber" >

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_custno" runat="server" Text="CUSTOMER NUMBER"></asp:Label>
                                        </HeaderTemplate>                                
                                        <ItemTemplate>
                                            <asp:Label ID="custno_lbl" runat="server" Text='<%# Eval("Customer Number") %>'></asp:Label>
                                        </ItemTemplate>

                                        </asp:TemplateField>
                                              <asp:TemplateField HeaderText="CUSTOMER NAME" ItemStyle-Height="30" ItemStyle-Width="30%" HeaderStyle-CssClass="HeadergridAll" SortExpression="CustomerName" >

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_custname" runat="server" Text="CUSTOMER NAME"></asp:Label>
                                        </HeaderTemplate>                                
                                        <ItemTemplate>
                                            <asp:Label ID="custname_lbl" runat="server" Text='<%# Eval("Customer Name") %>'></asp:Label>
                                        </ItemTemplate>

                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="SALE" ItemStyle-Width="25%" ItemStyle-Height="30" HeaderStyle-CssClass="HeadergridAll" SortExpression="Value" >

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_value_cn" runat="server" Text="SALE"></asp:Label>
                                        </HeaderTemplate>                                
                                        <ItemTemplate>
                                            <asp:Label ID="value_cn_lbl" runat="server" Text='<%# Eval("Sale") %>'></asp:Label>
                                        </ItemTemplate>

                                        </asp:TemplateField>

                                         <asp:TemplateField ItemStyle-Height="30" ItemStyle-Width="25%" HeaderText="CONTRIBUTION" HeaderStyle-CssClass="HeadergridAll" SortExpression="Contribution" >

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_contribution_cn" runat="server" Text="CONTRIBUTION"></asp:Label>
                                        </HeaderTemplate>                                
                                        <ItemTemplate>
                                            <asp:Label ID="contribution_cn_lbl" runat="server" Text='<%# Eval("Contribution") %>'></asp:Label>
                                        </ItemTemplate>

                                        </asp:TemplateField>
                                         </Columns>
									</asp:GridView>

									<asp:GridView AutoGenerateColumns="false" ID="grdChannelPartner" style="float:left;margin-top:30px;margin-left: 15px;width: 90%;" runat="server" ShowHeaderWhenEmpty="true">
									     <Columns>
                                        <asp:TemplateField ItemStyle-Height="30" ItemStyle-Width="20%" HeaderText="CHANNEL PARTNER NUMBER" HeaderStyle-CssClass="HeadergridAll" SortExpression="Distributor_number" >

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_distno" runat="server" Text="CHANNEL PARTNER NUMBER"></asp:Label>
                                        </HeaderTemplate>                                
                                        <ItemTemplate>
                                            <asp:Label ID="distno_lbl" runat="server" Text='<%# Eval("Channel Partner Number") %>'></asp:Label>
                                        </ItemTemplate>

                                        </asp:TemplateField>
                                              <asp:TemplateField ItemStyle-Height="30" ItemStyle-Width="30%" HeaderText="CHANNEL PARTNER NAME" HeaderStyle-CssClass="HeadergridAll" SortExpression="Distributor" >

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_distributor" runat="server" Text="CHANNEL PARTNER NAME"></asp:Label>
                                        </HeaderTemplate>                                
                                        <ItemTemplate>
                                            <asp:Label ID="distributor_lbl" runat="server" Text='<%# Eval("Channel Partner Name") %>'></asp:Label>
                                        </ItemTemplate>

                                        </asp:TemplateField>
                                         <asp:TemplateField ItemStyle-Height="30" ItemStyle-Width="25%" HeaderText="SALE" HeaderStyle-CssClass="HeadergridAll" SortExpression="Value" >

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_value_dis" runat="server" Text="SALE"></asp:Label>
                                        </HeaderTemplate>                                
                                        <ItemTemplate>
                                            <asp:Label ID="value_dis_lbl" runat="server" Text='<%# Eval("Sale") %>'></asp:Label>
                                        </ItemTemplate>

                                        </asp:TemplateField>

                                         <asp:TemplateField HeaderText="CONTRIBUTION" ItemStyle-Height="30" ItemStyle-Width="25%" HeaderStyle-CssClass="HeadergridAll" SortExpression="Contribution" >

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_contribution_dis" runat="server" Text="CONTRIBUTION"></asp:Label>
                                        </HeaderTemplate>                                
                                        <ItemTemplate>
                                            <asp:Label ID="contribution_dis_lbl" runat="server" Text='<%# Eval("Contribution") %>'></asp:Label>
                                        </ItemTemplate>

                                        </asp:TemplateField>
                                         </Columns>
									</asp:GridView>

									<asp:GridView ID="grdBranch" style="float:left;margin-top:30px;margin-left: 15px;width: 70%;" runat="server" ShowHeaderWhenEmpty="true" AutoGenerateColumns="false">
									  <Columns>
                                        <asp:TemplateField HeaderText="BRANCH" ItemStyle-Height="30" ItemStyle-Width="40%" HeaderStyle-CssClass="HeadergridAll" SortExpression="BRANCH" >

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_Branch" runat="server" Text="BRANCH"></asp:Label>
                                        </HeaderTemplate>                                
                                        <ItemTemplate>
                                            <asp:Label ID="branch_lbl" runat="server" Text='<%# Eval("BRANCH") %>'></asp:Label>
                                        </ItemTemplate>

                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="SALE" ItemStyle-Height="30" ItemStyle-Width="30%" HeaderStyle-CssClass="HeadergridAll" SortExpression="Value" >

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_value_b" runat="server" Text="SALE"></asp:Label>
                                        </HeaderTemplate>                                
                                        <ItemTemplate>
                                            <asp:Label ID="value_b_lbl" runat="server" Text='<%# Eval("Sale") %>'></asp:Label>
                                        </ItemTemplate>

                                        </asp:TemplateField>

                                         <asp:TemplateField ItemStyle-Height="30" ItemStyle-Width="30%" HeaderText="CONTRIBUTION" HeaderStyle-CssClass="HeadergridAll" SortExpression="Contribution" >

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_contribution_b" runat="server" Text="CONTRIBUTION"></asp:Label>
                                        </HeaderTemplate>                                
                                        <ItemTemplate>
                                            <asp:Label ID="contribution_b_lbl" runat="server" Text='<%# Eval("Contribution") %>'></asp:Label>
                                        </ItemTemplate>

                                        </asp:TemplateField>
                                         </Columns>
									</asp:GridView>
						
							
									<asp:GridView ID="grdProduct" style="float:left;margin-top:30px;margin-left: 15px;width: 90%;" runat="server" ShowHeaderWhenEmpty="true" AutoGenerateColumns="false">
									<Columns>
                                        <asp:TemplateField ItemStyle-Height="30" ItemStyle-Width="40%" HeaderText="PRODUCT FAMILY" HeaderStyle-CssClass="HeadergridAll" SortExpression="Product_Family" >

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_prodfamily" runat="server" Text="PRODUCT FAMILY"></asp:Label>
                                        </HeaderTemplate>                                
                                        <ItemTemplate>
                                            <asp:Label ID="prodfamily_lbl" runat="server" Text='<%# Eval("Product Family") %>'></asp:Label>
                                        </ItemTemplate>

                                        </asp:TemplateField>
                                         <asp:TemplateField ItemStyle-Height="30" ItemStyle-Width="30%" HeaderText="SALE" HeaderStyle-CssClass="HeadergridAll" SortExpression="Value" >

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_value_pf" runat="server" Text="SALE"></asp:Label>
                                        </HeaderTemplate>                                
                                        <ItemTemplate>
                                            <asp:Label ID="value_pf_lbl" runat="server" Text='<%# Eval("Sale") %>'></asp:Label>
                                        </ItemTemplate>

                                        </asp:TemplateField>

                                         <asp:TemplateField ItemStyle-Height="30" ItemStyle-Width="30%" HeaderText="CONTRIBUTION" HeaderStyle-CssClass="HeadergridAll" SortExpression="Contribution" >

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_contribution_pf" runat="server" Text="CONTRIBUTION"></asp:Label>
                                        </HeaderTemplate>                                
                                        <ItemTemplate>
                                            <asp:Label ID="contribution_pf_lbl" runat="server" Text='<%# Eval("Contribution") %>'></asp:Label>
                                        </ItemTemplate>

                                        </asp:TemplateField>
                                         </Columns>
									</asp:GridView>
							
									<asp:GridView AutoGenerateColumns="false" style="float:left;margin-top:30px;margin-left: 15px;width: 90%;" ID="grdSE" runat="server" ShowHeaderWhenEmpty="true">
									 <Columns>
                                        <asp:TemplateField ItemStyle-Height="30" ItemStyle-Width="40%" HeaderText="SALES ENGINEER" HeaderStyle-CssClass="HeadergridAll" SortExpression="SalesEngineer" >

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderTemplate>
                                            <asp:Label ID="Engineer_Name" runat="server" Text="SALES ENGINEER"></asp:Label>
                                        </HeaderTemplate>                                
                                        <ItemTemplate>
                                            <asp:Label ID="salesengineer_lbl" runat="server" Text='<%# Eval("SalesEngineer") %>'></asp:Label>
                                        </ItemTemplate>

                                        </asp:TemplateField>
                                         <asp:TemplateField ItemStyle-Height="30" ItemStyle-Width="30%" HeaderText="SALE" HeaderStyle-CssClass="HeadergridAll" SortExpression="Value" >

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_value" runat="server" Text="SALE"></asp:Label>
                                        </HeaderTemplate>                                
                                        <ItemTemplate>
                                            <asp:Label ID="value_lbl" runat="server" Text='<%# Eval("Sale") %>'></asp:Label>
                                        </ItemTemplate>

                                        </asp:TemplateField>

                                         <asp:TemplateField ItemStyle-Height="30" ItemStyle-Width="30%" HeaderText="CONTRIBUTION" HeaderStyle-CssClass="HeadergridAll" SortExpression="Contribution" >

                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                        <HeaderTemplate>
                                            <asp:Label ID="lbl_contribution" runat="server" Text="CONTRIBUTION"></asp:Label>
                                        </HeaderTemplate>                                
                                        <ItemTemplate>
                                            <asp:Label ID="contribution_lbl" runat="server" Text='<%# Eval("Contribution") %>'></asp:Label>
                                        </ItemTemplate>

                                        </asp:TemplateField>
                                         </Columns>
									</asp:GridView>
							<div class="clearfix"></div>
						   </div>
						<%--</div>
					 </div>--%>
					 
					 <table id="header_fixed"   ></table>
				  <%--</div>
			   </div>
			</div>
		  --%>
			<a style="display: none;" class="scrollup" href="javascript:void(0);">Scroll</a>
		 </div>
		  </ContentTemplate>
		<Triggers>
            <asp:AsyncPostBackTrigger ControlID="reports" EventName="Click" />
        <asp:AsyncPostBackTrigger ControlID="ValInThsnd" EventName="CheckedChanged" />
        <asp:AsyncPostBackTrigger ControlID="ValInLakhs" EventName="CheckedChanged" />
            <asp:AsyncPostBackTrigger ControlID="rdBtnTaegutec" EventName="CheckedChanged" />
        <asp:AsyncPostBackTrigger ControlID="rdBtnDuraCab" EventName="CheckedChanged" />
          <asp:PostBackTrigger ControlID="exportexlbtn" />
            <asp:PostBackTrigger ControlID="exportpdfbtn" />
		</Triggers>
		</asp:UpdatePanel>
    <asp:UpdateProgress ID="updateProgress" runat="server">
      <ProgressTemplate>
         <div style="position: fixed; text-align: left; height: 100%; width: 100%; top: 0; right: 0; left: 0; z-index: 9999999; background-color: #000000; opacity: 0.7;">
            <span style="border-width: 0px; position: fixed; padding: 50px; font-size: 30px; left: 40%; top: 40%; color: #fff">Please wait</span>
         </div>
      </ProgressTemplate>
   </asp:UpdateProgress>
        <script type="text/javascript">

            $(document).ready(function () {
                debugger;
                //gridviewScrollTrigger();
                BindCalendar();
                $('#product_image').unbind('click').bind('click', function (e) {
                    var attr = $('#product_image').attr('src');
                    $("#MainContent_reportdrpdwns").slideToggle();
                    if (attr == "images/up_arrow.png") {
                        $("#product_image").attr("src", "images/down_arrow.png");
                    } else {
                        $("#product_image").attr("src", "images/up_arrow.png");
                    }
                });
                

                $(window).resize(function () {

                    function gridviewScrollTrigger() {
                        if ($("#MainContent_grdCustomer").height() >= 450) {
                            gridView1 = $('#MainContent_grdCustomer').gridviewScroll({
                                width: $(window).width() - 50,
                                height: 350,
                                railcolor: "#F0F0F0",
                                barcolor: "#606060",
                                barhovercolor: "#606060",
                                bgcolor: "#F0F0F0",
                                freezesize: 0,
                                arrowsize: 30,
                                varrowtopimg: "Images/arrowvt.png",
                                varrowbottomimg: "Images/arrowvb.png",
                                harrowleftimg: "Images/arrowhl.png",
                                harrowrightimg: "Images/arrowhr.png",
                                headerrowcount: 2,
                                railsize: 16,
                                barsize: 14,
                                verticalbar: "auto",
                                horizontalbar: "auto",
                                overflow: 'none',
                                wheelstep: 1,
                            });
                        }
                    
                        if ($("#MainContent_grdChannelPartner").height() >= 450) {
                            gridView1 = $('#MainContent_grdChannelPartner').gridviewScroll({
                            width: $(window).width() - 50,
                            height: 350,
                            railcolor: "#F0F0F0",
                            barcolor: "#606060",
                            barhovercolor: "#606060",
                            bgcolor: "#F0F0F0",
                            freezesize: 0,
                            arrowsize: 30,
                            varrowtopimg: "Images/arrowvt.png",
                            varrowbottomimg: "Images/arrowvb.png",
                            harrowleftimg: "Images/arrowhl.png",
                            harrowrightimg: "Images/arrowhr.png",
                            headerrowcount: 2,
                            railsize: 16,
                            barsize: 14,
                            verticalbar: "auto",
                            horizontalbar: "auto",
                            overflow: 'none',
                            wheelstep: 1,
                        });
                    }
                
                        if ($("#MainContent_grdBranch").height() >= 450) {
                            gridView1 = $('#MainContent_grdBranch').gridviewScroll({
                        width: $(window).width() - 50,
                        height: 350,
                        railcolor: "#F0F0F0",
                        barcolor: "#606060",
                        barhovercolor: "#606060",
                        bgcolor: "#F0F0F0",
                        freezesize: 0,
                        arrowsize: 30,
                        varrowtopimg: "Images/arrowvt.png",
                        varrowbottomimg: "Images/arrowvb.png",
                        harrowleftimg: "Images/arrowhl.png",
                        harrowrightimg: "Images/arrowhr.png",
                        headerrowcount: 2,
                        railsize: 16,
                        barsize: 14,
                        verticalbar: "auto",
                        horizontalbar: "auto",
                        overflow: 'none',
                        wheelstep: 1,
                    });
                }
            
            if ($("#MainContent_grdSE").height() >= 450) {
                gridView1 = $('#MainContent_grdSE').gridviewScroll({
                    width: $(window).width() - 50,
                    height: 350,
                    railcolor: "#F0F0F0",
                    barcolor: "#606060",
                    barhovercolor: "#606060",
                    bgcolor: "#F0F0F0",
                    freezesize: 0,
                    arrowsize: 30,
                    varrowtopimg: "Images/arrowvt.png",
                    varrowbottomimg: "Images/arrowvb.png",
                    harrowleftimg: "Images/arrowhl.png",
                    harrowrightimg: "Images/arrowhr.png",
                    headerrowcount: 2,
                    railsize: 16,
                    barsize: 14,
                    verticalbar: "auto",
                    horizontalbar: "auto",
                    overflow: 'none',
                    wheelstep: 1,
                });
            }
            
                    if ($("#MainContent_grdProduct").height() >= 450) {
                        gridView1 = $('#MainContent_grdProduct').gridviewScroll({
                    width: $(window).width() - 50,
                    height: 350,
                    railcolor: "#F0F0F0",
                    barcolor: "#606060",
                    barhovercolor: "#606060",
                    bgcolor: "#F0F0F0",
                    freezesize: 0,
                    arrowsize: 30,
                    varrowtopimg: "Images/arrowvt.png",
                    varrowbottomimg: "Images/arrowvb.png",
                    harrowleftimg: "Images/arrowhl.png",
                    harrowrightimg: "Images/arrowhr.png",
                    headerrowcount: 2,
                    railsize: 16,
                    barsize: 14,
                    verticalbar: "auto",
                    horizontalbar: "auto",
                    overflow: 'none',
                    wheelstep: 1,
                });
            }
            }
                });

                $(document).on('change', '#MainContent_txtToDate', function () {
                    var Target_from = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_txtFromDate').val());
                    var Target_To = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_txtToDate').val());
                    if (Target_from != "" && Target_from != undefined && Target_from != null)
                        if (new Date(Target_To) < new Date(Target_from)) {
                            alert("To Date can not be less than the From Date");
                            var vaue = "";
                            $("#MainContent_txtToDate").val(vaue);
                            return false;
                        }
                    return true;
                });

                $(document).on('change', '#MainContent_txtFromDate', function () {
                    debugger;
                    var Target_from = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_txtFromDate').val());
                    var Target_To = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_txtToDate').val());
                    if (Target_To != "" && Target_To != undefined && Target_To != null)
                        if (new Date(Target_To) < new Date(Target_from)) {
                            alert("From Date can not be greater than the To Date");
                            var vaue = "";
                            $("#MainContent_txtFromDate").val(vaue);
                            return false;
                        }
                    return true;
                });
            });

            //function funNumber(a) {
            //    debugger;
            //    if (a.value > 100) {
            //        a.value = a.value.substring(0, 3);
            //        return true;
            //    }
            //    else {
            //        if (a.keyCode >= 48 && a.keyCode <= 57) {
            //            return true;
            //        }
            //        else if (a.keyCode >= 45 && a.keyCode <= 46) {
            //        }
            //        else {
            //            a.returnValue = false;
            //            return false;
            //        }
            //    }
            //}


            function allowOnlyNumber(evt) {
                debugger;
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return true;
                return false;
                var value = $("#MainContent_txtContribution").val;
                if (value > 100) {
                    alert("Please enter value upto 100.");
                    $("#MainContent_txtContribution").val("");
                }
            }

            //function isNumberKey(evt, obj) {

            //    debugger;
            //    var charCode = (evt.which) ? evt.which : event.keyCode
            //    var value = obj.value;
            //    //var dotcontains = value.indexOf(".") != -1;
            //    //if (dotcontains) {
            //    //    var match = ('' + value).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
            //    //    if (!match) { return 0; }
            //    //    var decCount = Math.max(0,
            //    //         // Number of digits right of decimal point.
            //    //         (match[1] ? match[1].length : 0)
            //    //         // Adjust for scientific notation.
            //    //         - (match[2] ? +match[2] : 0));
            //    //    if (decCount > 1) return false;

            //    //    if (charCode == 46) return false;
            //    //}
            //    //else {

            //    if (value.val > 100) return false;

            //    if (value.length > 2) {
                   
            //            if (charCode == 46) return true;
            //            else return false;
            //        }

            //    //}
            //    if (charCode == 46) return true;
            //    if (charCode > 31 && (charCode < 48 || charCode > 57))
            //        return false;
            //    return true;
            //}

            function BindCalendar() {
                debugger;
                var start = new Date();
                start.setYear(start.getYear() - 2);
                var end = new Date();
                end.setYear(end.getYear());

                $("#MainContent_txtFromDate").datepicker({
                    showOn: "both",
                    buttonImageOnly: true,
                    buttonText: "",
                    changeYear: true,
                    changeMonth: true,
                    yearRange: '-2:+0',
                    dateFormat: 'dd/mm/yy',
                    buttonImage: "images/calander_icon.png",
                });
                $("#MainContent_txtToDate").datepicker({
                    showOn: "both",
                    buttonImageOnly: true,
                    buttonText: "",
                    changeYear: true,
                    changeMonth: true,
                    yearRange: '-2:+0',
                    dateFormat: 'dd/mm/yy',
                    buttonImage: "images/calander_icon.png",
                });
            }

            function gridviewScrollTrigger() {

                gridView1 = $('#MainContent_grdCustomer').gridviewScroll({
                    width: $(window).width() - 30,
                    height: 450,
                    railcolor: "#F0F0F0",
                    barcolor: "#606060",
                    barhovercolor: "#606060",
                    bgcolor: "#F0F0F0",
                    freezesize: 0,
                    arrowsize: 30,
                    varrowtopimg: "Images/arrowvt.png",
                    varrowbottomimg: "Images/arrowvb.png",
                    harrowleftimg: "Images/arrowhl.png",
                    harrowrightimg: "Images/arrowhr.png",
                    headerrowcount: 2,
                    railsize: 16,
                    barsize: 14,
                    verticalbar: "auto",
                    horizontalbar: "auto",
                    wheelstep: 1,
                });

                gridView1 = $('#MainContent_grdChannelPartner').gridviewScroll({
                    width: $(window).width() - 30,
                    height: 450,
                    railcolor: "#F0F0F0",
                    barcolor: "#606060",
                    barhovercolor: "#606060",
                    bgcolor: "#F0F0F0",
                    freezesize: 0,
                    arrowsize: 30,
                    varrowtopimg: "Images/arrowvt.png",
                    varrowbottomimg: "Images/arrowvb.png",
                    harrowleftimg: "Images/arrowhl.png",
                    harrowrightimg: "Images/arrowhr.png",
                    headerrowcount: 2,
                    railsize: 16,
                    barsize: 14,
                    verticalbar: "auto",
                    horizontalbar: "auto",
                    wheelstep: 1,
                });

                gridView1 = $('#MainContent_grdBranch').gridviewScroll({
                    width: $(window).width() - 30,
                    height: 450,
                    railcolor: "#F0F0F0",
                    barcolor: "#606060",
                    barhovercolor: "#606060",
                    bgcolor: "#F0F0F0",
                    freezesize: 0,
                    arrowsize: 30,
                    varrowtopimg: "Images/arrowvt.png",
                    varrowbottomimg: "Images/arrowvb.png",
                    harrowleftimg: "Images/arrowhl.png",
                    harrowrightimg: "Images/arrowhr.png",
                    headerrowcount: 2,
                    railsize: 16,
                    barsize: 14,
                    verticalbar: "auto",
                    horizontalbar: "auto",
                    wheelstep: 1,
                });

                gridView1 = $('#MainContent_grdProduct').gridviewScroll({
                    width: $(window).width() - 30,
                    height: 450,
                    railcolor: "#F0F0F0",
                    barcolor: "#606060",
                    barhovercolor: "#606060",
                    bgcolor: "#F0F0F0",
                    freezesize: 0,
                    arrowsize: 30,
                    varrowtopimg: "Images/arrowvt.png",
                    varrowbottomimg: "Images/arrowvb.png",
                    harrowleftimg: "Images/arrowhl.png",
                    harrowrightimg: "Images/arrowhr.png",
                    headerrowcount: 2,
                    railsize: 16,
                    barsize: 14,
                    verticalbar: "auto",
                    horizontalbar: "auto",
                    wheelstep: 1,
                });


                gridView1 = $('#MainContent_grdSE').gridviewScroll({
                    width: $(window).width() - 30,
                    height: 450,
                    railcolor: "#F0F0F0",
                    barcolor: "#606060",
                    barhovercolor: "#606060",
                    bgcolor: "#F0F0F0",
                    freezesize: 0,
                    arrowsize: 30,
                    varrowtopimg: "Images/arrowvt.png",
                    varrowbottomimg: "Images/arrowvb.png",
                    harrowleftimg: "Images/arrowhl.png",
                    harrowrightimg: "Images/arrowhr.png",
                    headerrowcount: 2,
                    railsize: 16,
                    barsize: 14,
                    verticalbar: "auto",
                    horizontalbar: "auto",
                    wheelstep: 1,
                });


            }

            function triggerPostGridLodedActions() {

                //gridviewScrollTrigger();
                BindCalendar();
                $('#product_image').unbind('click').bind('click', function (e) {
                    var attr = $('#product_image').attr('src');
                    $("#MainContent_reportdrpdwns").slideToggle();
                    if (attr == "images/up_arrow.png") {
                        $("#product_image").attr("src", "images/down_arrow.png");
                    } else {
                        $("#product_image").attr("src", "images/up_arrow.png");
                    }
                });
            }
            function checkMandatory() {
                debugger;
                var returnVal = true;
                var Target_From = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_txtFromDate').val());
                var Target_To = $.datepicker.parseDate('dd/mm/yy', $('#MainContent_txtToDate').val());
                var contribution = $('#MainContent_txtContribution').val();
                
                if (Target_From == "" || Target_From == undefined || Target_From == null) {
                    //alert("Please enter From date.");
                    $('#MainContent_txtFromDate').css('border-color', 'red');
                    returnVal = false;
                }
                else {
                    $('#MainContent_txtFromDate').css('border-color', '');
                    returnVal = true;
                }

                if (Target_To == "" || Target_To == undefined || Target_To == null) {
                    //alert("Please enter To date.");
                    $('#MainContent_txtToDate').css('border-color', 'red');
                    returnVal = false;
                }
                else {
                    $('#MainContent_txtToDate').css('border-color', '');
                    returnVal = true;
                }
                if (contribution == "" || contribution == undefined || contribution == null) {
                    //alert("Please enter Contribution.");
                    $('#MainContent_txtContribution').css('border-color', 'red');
                    returnVal = false;
                }
                else if (contribution > 100) {
                    alert("Contribution can not be greater than 100%. Please change the value and continue.");
                    $('#MainContent_txtContribution').css('border-color', 'red');
                    returnVal = false;
                }
                else if (contribution.indexOf(".") >= 0) {
                    alert("Contribution value should be a whole number between 0 and 100.");
                    $('#MainContent_txtContribution').css('border-color', 'red');
                    returnVal = false;
                }
                else {
                    $('#MainContent_txtContribution').css('border-color', '');
                    returnVal = true;
                }
                if (returnVal == false) {
                    alert("Fields marked in red are required")
                }
                return returnVal
            }
            function isNumberKey(evt, obj) {

                debugger;
                var charCode = (evt.which) ? evt.which : event.keyCode
                var value = obj.value;
                var dotcontains = value.indexOf(".") != -1;
                if (dotcontains) {
                    var match = ('' + value).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
                    if (!match) { return 0; }
                    var decCount = Math.max(0,
                         // Number of digits right of decimal point.
                         (match[1] ? match[1].length : 0)
                         // Adjust for scientific notation.
                         - (match[2] ? +match[2] : 0));
                    if (decCount > 1) return false;

                    if (charCode == 46) return false;
                }
                else {
                    if (value.length > 2) {
                        if (charCode == 46) return true;
                        else return false;
                    }

                }
                if (charCode == 46) return true;
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            }
    </script>

</asp:Content>