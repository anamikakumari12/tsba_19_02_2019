﻿using AjaxControlToolkit;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace TaegutecSalesBudget
{
    public partial class ReviewQuarterly : System.Web.UI.Page
    {


        #region globaldeclarations
        Budget objBudget = new Budget();
        AdminConfiguration objConfig = new AdminConfiguration();
        Reports objReports = new Reports();
        Review objRSum = new Review();
        public static int gridLoadedStatus;
        public static string cter;
        CommonFunctions objCom = new CommonFunctions();
        AdminConfiguration objAdmin = new AdminConfiguration();
        #endregion


     

        #region Events
        /// <summary>
        /// Author       :
        /// Created date :
        /// Description  :Loading dropdownlist of branch,sales engineer list, customer names, customer numbers, application list, product groups based on user roles

        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

            if (!IsPostBack)
            {
                cter = null;
                gridLoadedStatus = 0;
                string name_desc = "", name_code = "";
                int count = 0;
                string strUserId = Session["UserId"].ToString();
                string roleId = Session["RoleId"].ToString();
                //load product family details

                if (Session["RoleId"].ToString() == "HO" || Session["RoleId"].ToString() == "TM")
                {
                    if (Session["RoleId"].ToString() == "HO")
                    {
                        if (Session["cter"] == null && roleId == "HO")
                        {
                            Session["cter"] = "TTA";
                            cter = "TTA";
                        }
                        if (Session["cter"].ToString() == "DUR")
                        {
                            rdBtnDuraCab.Checked = true;
                            rdBtnTaegutec.Checked = false;
                            cter = "DUR";
                        }
                        else
                        {
                            rdBtnTaegutec.Checked = true;
                            rdBtnDuraCab.Checked = false;
                            cter = "TTA";
                        }
                        cterDiv.Visible = true;
                    }
                    LoadBranches();
                    //cbAll_CheckedChanged(null, null);
                    BranchList_SelectedIndexChanged(null, null);
                }

                else if (Session["RoleId"].ToString() == "BM")
                {
                    string username = Session["UserName"].ToString();
                    string branchcode = Session["BranchCode"].ToString();
                    string branchDec = Session["BranchDesc"].ToString();
                    Session["SelectedBranchList"] = "'" + branchcode + "'";

                    //bind branch
                    BranchList.Items.Insert(0, branchcode);
                    divBranch.Visible = false;

                    // sales engineers loading
                    DataTable dtSalesEngDetails = objReports.LoadUserInfo(null, branchcode);
                    if (dtSalesEngDetails != null)
                    {
                        SalesEngList.DataSource = dtSalesEngDetails;
                        SalesEngList.DataTextField = "EngineerName";
                        SalesEngList.DataValueField = "EngineerId";
                        SalesEngList.DataBind();
                        //ddlSalesEngineerList.Items.Insert(0, "SELECT SALES ENGINEER");
                        // ChkSalesEng.Items.Insert(0, "ALL");
                    }

                    foreach (ListItem val in SalesEngList.Items)
                    {
                        val.Selected = true;
                        if (val.Selected)
                        {
                            count++;
                            name_desc += val.Text + " , ";
                            name_code += val.Value + "','";
                        }
                    }

                    SalesEngList_SelectedIndexChanged(null, null);

                }

                else if (Session["RoleId"].ToString() == "SE")
                {

                    string username = Session["UserName"].ToString();
                    string branchcode = Session["BranchCode"].ToString();
                    string branchDec = Session["BranchDesc"].ToString();
                    Session["SelectedBranchList"] = "'" + branchcode + "'";
                    Session["SelectedSalesEngineers"] = "'" + strUserId + "'";

                    //bind branch
                    BranchList.Items.Insert(0, branchcode);
                    divBranch.Visible = false;
                    //bind Sales engineer
                    SalesEngList.Items.Insert(0, strUserId);
                    divSE.Visible = false;

                    // customers loading
                    DataTable dtCutomerDetails = objBudget.LoadCustomerDetails(strUserId, "SE"); ;
                    if (dtCutomerDetails != null)
                    {
                        DataTable dtDeatils = new DataTable();
                        dtDeatils.Columns.Add("customer_number", typeof(string));
                        dtDeatils.Columns.Add("customer_name", typeof(string));
                        for (int i = 0; i < dtCutomerDetails.Rows.Count; i++)
                        {
                            dtDeatils.Rows.Add(dtCutomerDetails.Rows[i].ItemArray[1].ToString(), dtCutomerDetails.Rows[i].ItemArray[2].ToString() + "(" + dtCutomerDetails.Rows[i].ItemArray[1].ToString() + ")");
                        }
                        CustNameList.DataSource = dtDeatils;
                        CustNameList.DataTextField = "customer_name";
                        CustNameList.DataValueField = "customer_number";
                        CustNameList.DataBind();
                        //ddlCustomerList.Items.Insert(0, "-- SELECT CUSTOMER --");
                        //ChkCustName.Items.Insert(0, "ALL");

                        CustNumList.DataSource = dtCutomerDetails;
                        CustNumList.DataTextField = "customer_number";
                        CustNumList.DataValueField = "customer_number";
                        CustNumList.DataBind();
                        //ddlCustomerNumber.Items.Insert(0, "-- SELECT CUSTOMER NUMBER --");
                        //ChkCustNum.Items.Insert(0, "ALL");

                        foreach (ListItem val in CustNameList.Items)
                        {
                            val.Selected = true;
                            if (val.Selected)
                            {
                                count++;
                                name_desc += val.Text + " , ";
                                name_code += val.Value + "','";
                            }
                        }

                        foreach (ListItem val in CustNumList.Items)
                        {
                            val.Selected = true;
                            if (val.Selected)
                            {
                                count++;
                                name_desc += val.Text + " , ";
                                name_code += val.Value + "','";
                            }
                        }

                        CustNameList_SelectedIndexChanged(null, null);
                        CustNumList_SelectedIndexChanged(null, null);
                    }

                }

                LoadProductFamliy();
                LoadProductGroup();
                //ChkProductFamilyAll_CheckedChanged(null, null);
                //ChkProductGrpAll_CheckedChanged(null, null);
                ProductGrpList_SelectedIndexChanged(null, null);
                //ChkAppAll_CheckedChanged(null, null);

                //txtbranchlist.Text = "ALL";
                //TxtSalesengList.Text = "ALL";
                //TxtCustomerName.Text = "ALL";
                //TxtCustomerNum.Text = "ALL";
                //TxtProductGrp.Text = "ALL";
                //TxtProductfamily.Text = "ALL";
                //TxtApplication.Text = "ALL";
            }
        }
        /// <summary>
        /// Author  : 
        /// Date    : 
        /// Desc    : Select the company type and store in session
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void rdBtnTaegutec_CheckedChanged(object sender, EventArgs e)
        {
            ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "alert('Please click on FILTER to view results');triggerPostGridLodedActions();", true);

            if (rdBtnTaegutec.Checked)
            {
                Session["cter"] = "TTA";
                cter = "TTA";
            }
            if (rdBtnDuraCab.Checked)
            {
                Session["cter"] = "DUR";
                cter = "DUR";
            }
            grdviewAllValues.DataSource = null;
            grdviewAllValues.DataBind();
            //Chart2.DataSource = null;
            grdviewAllValues.DataBind();
            divGridGraph.Visible = false;


            LoadBranches();
            BranchList_SelectedIndexChanged(null, null);
            SalesEngList_SelectedIndexChanged(null, null);

        }

      


        /// <summary>
        /// Author :
        /// Created date :
        /// Description :  click on branches dropdownlist, se should be loaded accordingly and selected value should be stored in session for further use
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
       
        protected void BranchList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name_desc = "", name_code = "";
            int count = 0;
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

            foreach (ListItem val in BranchList.Items)
            {
                if (val.Selected)
                {
                    count++;
                    name_desc += val.Text + " , ";
                    name_code += val.Value + "','";
                }
            }

            name_code = "'" + name_code;
            // txtbranchlist.Text = name_desc;

            string branchlist = name_code.Substring(0, Math.Max(0, name_code.Length - 2));

            if (count == BranchList.Items.Count)
            {
                Session["SelectedBranchList"] = "ALL";
            }
            else
            {
                Session["SelectedBranchList"] = branchlist;
            }

            string roleId = Session["RoleId"].ToString();
            string userId = Session["UserId"].ToString();
            //string branchcode = Session["BranchCode"].ToString();
            string branchcode = Convert.ToString(Session["SelectedBranchList"]);
            objRSum.BranchCode = roleId == "TM" && branchcode == "ALL" ? userId : branchcode;
            objRSum.roleId = roleId;
            objRSum.flag = "SalesEngineer";
            objRSum.cter = cter;
            DataTable dtData = objRSum.getFilterAreaValue(objRSum);

            if (dtData.Rows.Count != 0)
            {
                SalesEngList.DataSource = dtData;
                SalesEngList.DataTextField = "EngineerName";
                SalesEngList.DataValueField = "EngineerId";
                SalesEngList.DataBind();
                // ChkSalesEng.Items.Insert(0, "ALL");
            }
            else
            {
                SalesEngList.DataSource = dtData;
                SalesEngList.DataTextField = "EngineerName";
                SalesEngList.DataValueField = "EngineerId";
                SalesEngList.DataBind();
                //ChkSalesEng.Items.Insert(0, "NO SALES ENGINEER");
            }

            //if (cbAll.Checked == true || count >= 1)
            //{
            //    CheckSalEngAll.Checked = true;
            //    CheckSalEngAll_CheckedChanged(null, null);

            //}
            foreach (ListItem val in SalesEngList.Items)
            {
                val.Selected = true;
                if (val.Selected)
                {
                    count++;
                    name_desc += val.Text + " , ";
                    name_code += val.Value + "','";
                }
            }

            SalesEngList_SelectedIndexChanged(null, null);

            //DataTable dt = ViewState["Chart"] as DataTable;
            //if (dt != null) { if (dt.Rows.Count != 0) bindchart(dt); }
            ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        }

        /// <summary>
        /// Author       :
        /// Created date :
        /// Description  : click on se Dropdownlist, customers should be loaded accordingly and selected value should be stored in session for further use
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SalesEngList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name_desc = "", name_code = "";
            int count = 0;
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            foreach (ListItem val in SalesEngList.Items)
            {
                if (val.Selected)
                {
                    count++;
                    name_desc += val.Text + " , ";
                    name_code += val.Value + "','";
                }
            }

            name_code = "'" + name_code;  

            string SalesengList = name_code.Substring(0, Math.Max(0, name_code.Length - 2));
            string SalesengnameList = name_desc.Substring(0, Math.Max(0, name_desc.Length - 2));

            if (count == SalesEngList.Items.Count)
            {
                Session["SelectedSalesEngineers"] = "ALL";
            }
            else
            {
                Session["SelectedSalesEngineers"] = SalesengList;
            }          

            string roleId = Session["RoleId"].ToString();
            string userId = Session["UserId"].ToString();
            //string branchcode = Session["BranchCode"].ToString();
            string branchcode = Convert.ToString(Session["SelectedBranchList"]);
            objRSum.BranchCode = (roleId == "TM" && BranchList.SelectedItem.Value == "ALL") ? userId : branchcode;
            objRSum.salesengineer_id = SalesengList.ToString() == "ALL" || SalesengList.ToString() == "" ? null : SalesengList;
            objRSum.customer_type = ddlcustomertype.SelectedItem.Value;
            objRSum.roleId = roleId;
            objRSum.flag = "CustomerType";
            objRSum.cter = cter;
            DataTable dtData = objRSum.getFilterAreaValue(objRSum);

            if (dtData.Rows.Count != 0)
            {
                CustNameList.DataSource = dtData;
                CustNameList.DataTextField = "customer_short_name";
                CustNameList.DataValueField = "customer_number";
                CustNameList.DataBind();
                // ChkCustName.Items.Insert(0, "ALL");
            }
            else
            {
                CustNameList.DataSource = dtData;
                CustNameList.DataTextField = "customer_short_name";
                CustNameList.DataValueField = "customer_number";
                CustNameList.DataBind();
                // ChkCustName.Items.Insert(0, "NO CUSTOMER");
            }
            if (dtData.Rows.Count != 0)
            {
                CustNumList.DataSource = dtData;
                CustNumList.DataTextField = "customer_number";
                CustNumList.DataValueField = "customer_number";
                CustNumList.DataBind();
                // ChkCustNum.Items.Insert(0, "ALL");
            }
            else
            {
                CustNumList.DataSource = dtData;
                CustNumList.DataTextField = "customer_number";
                CustNumList.DataValueField = "customer_number";
                CustNumList.DataBind();
                // ChkCustNum.Items.Insert(0, "NO CUSTOMER");
            }
            foreach (ListItem val in CustNameList.Items)
            {
                val.Selected = true;
                if (val.Selected)
                {
                    count++;
                    name_desc += val.Text + " , ";
                    name_code += val.Value + "','";
                }
            }

            foreach (ListItem val in CustNumList.Items)
            {
                val.Selected = true;
                if (val.Selected)
                {
                    count++;
                    name_desc += val.Text + " , ";
                    name_code += val.Value + "','";
                }
            }
            CustNameList_SelectedIndexChanged(null, null);
            CustNumList_SelectedIndexChanged(null, null);
            //DataTable dt = ViewState["Chart"] as DataTable;
            //if (dt != null) { if (dt.Rows.Count != 0) bindchart(dt); }
            ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        /// <summary>
        /// Author       :
        /// Created date :
        /// Description  : click on cust_name checkbox, cust_no should be checked accordingly and selected value should be stored in session for further use
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CustNameList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name_desc = "", name_code = "";
            string c_name_desc = "", c_name_code = "";
            int counter = 0, icounter = 0;
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            try
            {
                foreach (ListItem val in CustNameList.Items)
                {
                    if (val.Selected)
                    {
                        counter++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                        foreach (ListItem val1 in CustNumList.Items)
                        {
                            if (val1.Value == val.Value)
                            {
                                icounter++;
                                val1.Selected = true;
                                c_name_desc += val1.Text + " , ";
                                c_name_code += val1.Value + "','";
                            }
                        }
                    }
                    else
                    {
                        foreach (ListItem val1 in CustNumList.Items)
                        {
                            if (val1.Value == val.Value)
                            {
                                val1.Selected = false;
                            }
                        }
                    }
                }

                name_code = "'" + name_code;
                string CustomerNamelist = name_code.Substring(0, Math.Max(0, name_code.Length - 2));


                if (CustNameList.Items.Count == counter)
                {
                    Session["SelectedCustomerNames"] = "ALL";
                }
                else
                {
                    Session["SelectedCustomerNames"] = CustomerNamelist;
                }

                c_name_code = "'" + c_name_code;
                string CustomerNumlist = c_name_code.Substring(0, Math.Max(0, c_name_code.Length - 2));
                if (CustNumList.Items.Count == icounter)
                {

                    Session["SelectedCustomerNumbers"] = "ALL";
                }
                else
                {
                    Session["SelectedCustomerNumbers"] = CustomerNumlist;

                }

            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        }

        /// <summary>
        /// Author       :
        /// Created date :
        /// Description  : click on cust_no checkbox, cust_name should be checked accordingly and selected value should be stored in session for further use
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void CustNumList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name_desc = "", name_code = "";
            string c_name_desc = "", c_name_code = "";
            int counter = 0, icounter = 0;
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            try
            {
                foreach (ListItem val in CustNumList.Items)
                {
                    if (val.Selected)
                    {
                        counter++;
                        name_desc += val.Text + " , ";
                        name_code += val.Value + "','";
                        foreach (ListItem val1 in CustNameList.Items)
                        {
                            if (val1.Value == val.Value)
                            {
                                icounter++;
                                val1.Selected = true;
                                c_name_desc += val1.Text + " , ";
                                c_name_code += val1.Value + "','";
                            }
                        }
                    }
                    else
                    {
                        foreach (ListItem val1 in CustNameList.Items)
                        {
                            if (val1.Value == val.Value)
                            {
                                val1.Selected = false;
                            }
                        }
                    }
                }

                name_code = "'" + name_code;
                string CustomerNumlist = name_code.Substring(0, Math.Max(0, name_code.Length - 2));


                if (CustNameList.Items.Count == counter)
                {
                    Session["SelectedCustomerNumbers"] = "ALL";
                }
                else
                {
                    Session["SelectedCustomerNumbers"] = CustomerNumlist;
                }

                c_name_code = "'" + c_name_code;
                string CustomerNamelist = c_name_code.Substring(0, Math.Max(0, c_name_code.Length - 2));
                if (CustNumList.Items.Count == icounter)
                {

                    Session["SelectedCustomerNames"] = "ALL";
                }
                else
                {
                    Session["SelectedCustomerNames"] = CustomerNamelist;

                }


            }
            catch (Exception ex)
            {
                objCom.LogError(ex);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        }
        /// <summary>
        /// Author       :
        /// Created date :
        /// Description  :click on customer type, customers should be loaded accordingly and selected value should be stored in session for further use
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void customertype_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string name_desc = "", name_code = "";
            int count = 0;
            string roleId = Session["RoleId"].ToString();
            string userId = Session["UserId"].ToString();
            // string branchcode = Session["BranchCode"].ToString();
            string branchcode = Convert.ToString(Session["SelectedBranchList"]);
            string SalesengList = Convert.ToString(Session["SelectedSalesEngineers"]);
            objRSum.BranchCode = (roleId == "TM" && BranchList.SelectedItem.Value == "ALL") ? userId : branchcode;
            objRSum.salesengineer_id = SalesengList;
            objRSum.customer_type = ddlcustomertype.SelectedItem.Value;
            objRSum.roleId = roleId;
            objRSum.flag = "CustomerType";
            DataTable dtData = objRSum.getFilterAreaValue(objRSum);

            if (dtData.Rows.Count != 0)
            {
                CustNameList.DataSource = dtData;
                CustNameList.DataTextField = "customer_short_name";
                CustNameList.DataValueField = "customer_number";
                CustNameList.DataBind();
                //ChkCustName.Items.Insert(0, "ALL");
            }
            else
            {
                CustNameList.DataSource = dtData;
                //CustNameList.DataTextField = "customer_short_name";
                //CustNameList.DataValueField = "customer_number";
                CustNameList.DataBind();
                //ChkCustName.Items.Insert(0, "NO CUSTOMER");
            }
            if (dtData.Rows.Count != 0)
            {
                CustNumList.DataSource = dtData;
                CustNumList.DataTextField = "customer_number";
                CustNumList.DataValueField = "customer_number";
                CustNumList.DataBind();
                // ChkCustNum.Items.Insert(0, "ALL");
            }
            else
            {
                CustNumList.DataSource = dtData;
                //CustNumList.DataTextField = "customer_number";
                //CustNumList.DataValueField = "customer_number";
                CustNumList.DataBind();
                // ChkCustNum.Items.Insert(0, "NO CUSTOMER");
            }
            foreach (ListItem val in CustNameList.Items)
            {
                val.Selected = true;
                if (val.Selected)
                {
                    count++;
                    name_desc += val.Text + " , ";
                    name_code += val.Value + "','";
                }
            }

            foreach (ListItem val in CustNumList.Items)
            {
                val.Selected = true;
                if (val.Selected)
                {
                    count++;
                    name_desc += val.Text + " , ";
                    name_code += val.Value + "','";
                }
            }

            CustNameList_SelectedIndexChanged(null, null);
            CustNumList_SelectedIndexChanged(null, null);

            //DataTable dt = ViewState["Chart"] as DataTable;
            //if (dt != null) { if (dt.Rows.Count != 0) bindchart(dt); }
            ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        /// <summary>
        /// Author :
        /// Created date :
        /// Description :click on family checkbox, subfamily should be loaded accordingly and selected value should be stored in session for further use
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ProductFamilyList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name_desc = "", name_code = "", chakgrp_desc = "", chkgrp_code = "";
            int count = 0, count1 = 0;
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            // For PRODUCT FAMILY
            foreach (ListItem val in ProductFamilyList.Items)
            {
                if (val.Selected)
                {
                    count++;
                    name_desc += val.Text + " , ";
                    name_code += val.Value + ",";
                }
            }

            name_code = "" + name_code;

            string ProductFamilyListVal = name_code.Substring(0, Math.Max(0, name_code.Length - 1));
            string ProductFamilyNameList = name_desc.Substring(0, Math.Max(0, name_desc.Length - 2));

            if (count == ProductFamilyList.Items.Count)
            {
                Session["SelectedProductFamily"] = "ALL";
            }
            else
            {
                Session["SelectedProductFamily"] = ProductFamilyListVal;
            }
            
            // For PRODUCT GROUP
            foreach (ListItem val in ProductGrpList.Items)
            {
                if (val.Selected)
                {
                    count1++;
                    chakgrp_desc += val.Text + " , ";
                    chkgrp_code += val.Value + "','";
                }
            }

            chkgrp_code = "'" + chkgrp_code;

            string ProductGrpListVal = chkgrp_code.Substring(0, Math.Max(0, chkgrp_code.Length - 2));
            string ProductGrpNameList = chakgrp_desc.Substring(0, Math.Max(0, chakgrp_desc.Length - 2));

            if (count1 == ProductGrpList.Items.Count)
            {
                Session["SelectedProductGroup"] = "ALL";
            }
            else
            {
                Session["SelectedProductGroup"] = ProductGrpListVal;
            }


            string ProductGroup = Convert.ToString(Session["SelectedProductGroup"]);
            string ProductFamily = Convert.ToString(Session["SelectedProductFamily"]);

            string famId = ProductFamily == "ALL" || ProductFamily == "" ? "0" : ProductFamily;
            // int Id = Convert.ToInt32(famId);
            DataTable dtPL = new DataTable();
            string group = ProductGroup == "ALL" || ProductGroup == "" ? null : ProductGroup;
            dtPL = objRSum.getProducts(famId, group);
            DataTable dtTemp = dtPL.Clone();
            for (int i = 0; i < dtPL.Rows.Count; i++)
            {
                dtTemp.Rows.Add(dtPL.Rows[i].ItemArray[0], dtPL.Rows[i].ItemArray[0].ToString() + "_" + dtPL.Rows[i].ItemArray[2].ToString(), dtPL.Rows[i].ItemArray[2]);
            }
            if (dtTemp.Rows.Count != 0)
            {
                ApplicationList.DataSource = dtTemp;
                ApplicationList.DataValueField = "item_code";
                ApplicationList.DataTextField = "item_short_name";
                ApplicationList.DataBind();
                //ChkApplicationList.Items.Insert(0, "ALL");
            }
            else
            {

                ApplicationList.DataSource = dtTemp;
                ApplicationList.DataValueField = "item_code";
                ApplicationList.DataTextField = "item_short_name";
                ApplicationList.DataBind();
                // ChkApplicationList.Items.Insert(0, "NO APPLICATION FOR SELECTION");
            }
            foreach (ListItem val in ApplicationList.Items)
            {
                val.Selected = true;
                if (val.Selected)
                {
                    count++;
                    name_desc += val.Text + " , ";
                    name_code += val.Value + "','";
                }
            }

            ApplicationList_SelectedIndexChanged(null, null);
            //DataTable dt = ViewState["Chart"] as DataTable;
            //if (dt != null) { if (dt.Rows.Count != 0) bindchart(dt); }
            ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);


        }


        /// <summary>
        /// Author  : 
        /// Date    : 
        /// Desc    : click on group checkbox,  selected value should be stored in session for further use
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ProductGrpList_SelectedIndexChanged(object sender, EventArgs e)
        {
            ProductFamilyList_SelectedIndexChanged(null, null);
            //DataTable dt = ViewState["Chart"] as DataTable;
            //if (dt != null) { if (dt.Rows.Count != 0) bindchart(dt); }
            ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        /// <summary>
        /// Author  : 
        /// Date    : 
        /// Desc    : click on application checkbox, selected value should be stored in session for further use
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ApplicationList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string name_desc = "", name_code = "";
            int count = 0;
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }

            foreach (ListItem val in ApplicationList.Items)
            {
                if (val.Selected)
                {
                    count++;
                    name_desc += val.Text + " , ";
                    name_code += val.Value + "','";
                }
            }

            name_code = "'" + name_code;

            // TxtApplication.Text = name_desc;

            string ApplicationListVal = name_code.Substring(0, Math.Max(0, name_code.Length - 2));
            string ApplicationNameList = name_desc.Substring(0, Math.Max(0, name_desc.Length - 2));

            if (count == ApplicationList.Items.Count)
            {
                Session["SelectedApplications"] = "ALL";
            }
            else
            {
                Session["SelectedApplications"] = ApplicationListVal;
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }
        /// <summary>
        ///  Author : 
        /// Date :
        /// Desc : Fetch report from database using all the filters
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void reports_Click(object sender, EventArgs e)
        {
            DataTable dtYtdSales = new DataTable();
            gridLoadedStatus = 1;
            Session["FC"] = "1";
            if (rbtn_Thousand.Checked) { dtYtdSales = loadGrid(); }
            if (rbtn_Lakhs.Checked) { dtYtdSales = loadGrid_lakh(); }


            if (dtYtdSales.Rows.Count != 0)
            {
                grdviewAllValues.DataSource = dtYtdSales;
                grdviewAllValues.DataBind();
            }
            bindgridColor();

            //chart pre
            DataTable dtChart = GenerateTransposedTable(dtYtdSales);
            DataTable dtoriginal = dtChart.Copy();
            Session["dtoriginal"] = dtoriginal;
            ViewState["Chart"] = dtChart;
            foreach (DataRow row in dtChart.Rows)
            {
                if (row.ItemArray[0].ToString() == "flag")
                {
                    row.Delete(); dtChart.AcceptChanges();
                    //LoadChartData(dtChart);
                    string column1 = dtChart.Columns[1].ColumnName.ToString(); // Months 
                    string column2 = dtChart.Columns[5].ColumnName.ToString(); // YTD SALE
                    string column3 = dtChart.Columns[4].ColumnName.ToString(); // YTD PLAN
                    string column4 = dtChart.Columns[6].ColumnName.ToString(); // YTD SALE PREVIOUS YEAR

                    //Chart2.DataSource = dtChart;
                    //Chart2.Series.Clear();
                    //Chart2.Series.Add(column2);
                    //Chart2.Series[column2].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Column;

                    //Chart2.Series.Add(column3);
                    //Chart2.Series[column3].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Line;
                    //Chart2.Series[column3].IsValueShownAsLabel = true;
                    //Chart2.Series.Add("Series4");
                    //Chart2.Series["Series4"].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Point;
                    //Chart2.Series.Add(column4);
                    //Chart2.Series[column4].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Line;
                    //Chart2.Series[column4].IsValueShownAsLabel = true;
                    //Chart2.Series.Add("Series5");
                    //Chart2.Series["Series5"].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Point;
                    //Chart2.Series[column2].Color = ColorTranslator.FromHtml("#EE4394");
                    //Chart2.Series[column3].Color = ColorTranslator.FromHtml("#0A438C");
                    //Chart2.Series["Series4"].Color = ColorTranslator.FromHtml("#097054");
                    //Chart2.Series[column4].Color = ColorTranslator.FromHtml("#FF9900");
                    //Chart2.Series["Series5"].Color = ColorTranslator.FromHtml("#097054");
                    ////Bar chart
                    //Chart2.Series[column2].XValueMember = column1;
                    //Chart2.Series[column2].YValueMembers = column2;
                    ////Line chart YTD PLAN 2015
                    //Chart2.Series[column3].XValueMember = column1;
                    //Chart2.Series[column3].YValueMembers = column3;
                    //Chart2.Series["Series4"].XValueMember = column1;
                    //Chart2.Series["Series4"].YValueMembers = column3;
                    //Chart2.Series["Series4"].IsVisibleInLegend = false;
                    //// Line chaert YTD SALE 2014
                    //Chart2.Series[column4].XValueMember = column1;
                    //Chart2.Series[column4].YValueMembers = column4;
                    //Chart2.Series["Series5"].XValueMember = column1;
                    //Chart2.Series["Series5"].YValueMembers = column4;

                    //Chart2.Series["Series5"].IsVisibleInLegend = false;
                    //Chart2.Series[column2].SetCustomProperty("DrawingStyle", "Cylinder");
                    //Chart2.DataBind();

                    //// making user friendly

                    //Chart2.ChartAreas["ChartArea1"].AxisX.Interval = 1;
                    //Chart2.Series[column2].ToolTip = column2 + " " + ":" + " " + "#VALY"; ;
                    //Chart2.Series[column3].ToolTip = column3 + " " + ":" + " " + "#VALY"; ;
                    //Chart2.Series[column4].ToolTip = column4 + " " + ":" + " " + "#VALY";
                    //Chart2.Visible = true;
                    //Chart2.Series[column3].BorderWidth = 2;
                    //Chart2.Series[column4].BorderWidth = 2;
                    //Chart2.Legends["Legend2"].CellColumns.Add(new LegendCellColumn(column2, LegendCellColumnType.Text, "MTD SALE"));
                    //Random random = new Random();
                    //foreach (var item in Chart2.Series["YTD SALE"].Points)
                    //{
                    //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
                    //    item.Color = c;
                    //}

                    ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
                    break;
                }
            }
            divGridGraph.Visible = true;
            ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);


        }



        #region Value in Thousand/Lakhs

      
        protected void rbtn_Value_SelectedIndexChanged(object sender, EventArgs e)
        {

            reports_Click(null, null);

            ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);


        }
        /// <summary>
        /// loads gridview in thousands
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Thousand_CheckedChanged(object sender, EventArgs e)
        {
            if (gridLoadedStatus == 1)
            {
                reports_Click(null, null);
            }
            ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);


        }
        /// <summary>
        /// loads gridview in Laksh
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Lakhs_CheckedChanged(object sender, EventArgs e)
        {
            if (gridLoadedStatus == 1)
            {

                reports_Click(null, null);
            }
            ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);


        }
        #endregion


        #region check all Items Anantha
        //protected void ChkCustNameAll_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (ChkCustNameAll.Checked == true)
        //    {
        //        foreach (ListItem val in ChkCustName.Items)
        //        {
        //            val.Selected = true;
        //        }
        //    }
        //    else
        //    {
        //        foreach (ListItem val in ChkCustName.Items)
        //        {
        //            val.Selected = false;
        //        }
        //    }
        //}
        //protected void ChkCustNumAll_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (ChkCustNumAll.Checked == true)
        //    {
        //        foreach (ListItem val in ChkCustNum.Items)
        //        {
        //            val.Selected = true;
        //        }
        //    }
        //    else
        //    {
        //        foreach (ListItem val in ChkCustNum.Items)
        //        {
        //            val.Selected = false;
        //        }
        //    }
        //}
        //protected void cbAll_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (cbAll.Checked == true)
        //    {
        //        foreach (ListItem val in ChkBranches.Items)
        //        {
        //            val.Selected = true;
        //        }

        //    }
        //    else
        //    {
        //        foreach (ListItem val in ChkBranches.Items)
        //        {
        //            val.Selected = false;
        //        }

        //    }
        //    ddlBranchList_SelectedIndexChanged(null, null);
        //}
        //protected void CheckSalEngAll_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (CheckSalEngAll.Checked == true)
        //    {
        //        foreach (ListItem val in ChkSalesEng.Items)
        //        {
        //            val.Selected = true;
        //        }

        //    }
        //    else
        //    {
        //        foreach (ListItem val in ChkSalesEng.Items)
        //        {
        //            val.Selected = false;
        //        }

        //    }
        //    ddlSalesEngineerList_SelectedIndexChanged(null, null);
        //}
        //protected void ChkProductFamilyAll_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (ChkProductFamilyAll.Checked == true)
        //    {
        //        foreach (ListItem val in ChkProductFamily.Items)
        //        {
        //            val.Selected = true;
        //        }
        //    }
        //    else
        //    {
        //        foreach (ListItem val in ChkProductFamily.Items)
        //        {
        //            val.Selected = false;
        //        }
        //    }
        //}
        //protected void ChkProductGrpAll_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (ChkProductGrpAll.Checked == true)
        //    {
        //        foreach (ListItem val in ChkProductGroup.Items)
        //        {
        //            val.Selected = true;
        //        }
        //    }
        //    else
        //    {
        //        foreach (ListItem val in ChkProductGroup.Items)
        //        {
        //            val.Selected = false;
        //        }

        //    }
        //    ddlProductGroup_SelectedIndexChanged(null, null);
        //}
        //protected void ChkAppAll_CheckedChanged(object sender, EventArgs e)
        //{
        //    if (ChkAppAll.Checked == true)
        //    {
        //        foreach (ListItem val in ChkApplicationList.Items)
        //        {
        //            val.Selected = true;
        //        }
        //    }
        //    else
        //    {
        //        foreach (ListItem val in ChkApplicationList.Items)
        //        {
        //            val.Selected = false;
        //        }
        //    }
        //}
        #endregion
        #endregion


        #region Methods
        protected void LoadProductFamliy()
        {
            string name_desc = "", name_code = "";
            int count = 0;
            DataTable dtProductFamilyList = new DataTable();
            dtProductFamilyList = objBudget.LoadFamilyId();
            ProductFamilyList.DataSource = dtProductFamilyList;
            ProductFamilyList.DataTextField = "item_family_name";
            ProductFamilyList.DataValueField = "item_family_id";

            ProductFamilyList.DataBind();
            //ddlProductFamliy.Items.Insert(0, "ALL");

            foreach (ListItem val in ProductFamilyList.Items)
            {
                val.Selected = true;
                if (val.Selected)
                {
                    count++;
                    name_desc += val.Text + " , ";
                    name_code += val.Value + "','";
                }
            }
        }
        /// <summary>
        /// Author  : 
        /// Date    : 
        /// Desc    :  Load Branches into ListBox 
        /// </summary>
        protected void LoadBranches()
        {
            string name_desc = "", name_code = "";
            int count = 0;
            if (Session["UserId"] == null) { Response.Redirect("Login.aspx"); return; }
            string roleId = Session["RoleId"].ToString();
            string userId = Session["UserId"].ToString();
            string branchcode = Session["BranchCode"].ToString();
            DataTable dtData = new DataTable();
            objRSum.BranchCode = userId; // passing here territory Engineer Id  as branch code IF role is TM 
            objRSum.roleId = roleId;
            objRSum.flag = "Branch";
            objRSum.cter = cter;
            dtData = objRSum.getFilterAreaValue(objRSum);

            BranchList.DataSource = dtData;
            BranchList.DataTextField = "BranchDesc";
            BranchList.DataValueField = "BranchCode";
            BranchList.DataBind();
            // ChkBranches.Items.Insert(0, "ALL");
            foreach (ListItem val in BranchList.Items)
            {
                val.Selected = true;
                if (val.Selected)
                {
                    count++;
                    name_desc += val.Text + " , ";
                    name_code += val.Value + "','";
                }
            }


            ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        }

        /// <summary>
        /// Author  : 
        /// Date    : 
        /// Desc    :  Load ProductGroup into ListBox 
        /// </summary>
        protected void LoadProductGroup()
        {
            string name_desc = "", name_code = "";
            int count = 0;
            int B_Budgetyear = Convert.ToInt32(objAdmin.GetProfile("B_BUDGET_YEAR"));
            DataTable ProductGroups = objRSum.GetProductGroups(B_Budgetyear);
            for (int i = 0; i < ProductGroups.Rows.Count; i++)
            {

                if (Convert.ToString(ProductGroups.Rows[i][1]) == "FIVE YEARS")
                {
                    ProductGroups.Rows[i][1] = "5YRS";
                }
                else if (Convert.ToString(ProductGroups.Rows[i][1]) == "TEN YEARS")
                {
                    ProductGroups.Rows[i][1] = "10YRS";
                }
            }
            ProductGrpList.DataSource = ProductGroups;
            ProductGrpList.DataTextField = "splgrps";
            ProductGrpList.DataValueField = "splgrps";
            ProductGrpList.DataBind();

            foreach (ListItem val in ProductGrpList.Items)
            {
                val.Selected = true;
                if (val.Selected)
                {
                    count++;
                    name_desc += val.Text + " , ";
                    name_code += val.Value + "','";
                }
            }

            //  ScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
            //  ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);

        }

        /// <summary>
        /// Author : 
        /// Date : 
        /// Desc : For Transposing Datatable for providing it as datasource to chart
        /// </summary>
        /// <param name="dt"></param>
        public static void Transpose(DataTable dt)
        {
            if (dt != null)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                }
            }
        }
        /// <summary>
        /// Author : K.LakshmiBindu
        /// Date   : 
        /// Desc   : Converts DataTable to Json String
        /// </summary>
        /// <param name="table"> Data table that we want to convert to json string</param>
        /// <returns></returns>
        public static string DataTableToJSONWithStringBuilder(DataTable table)
        {
            var JSONString = new System.Text.StringBuilder();
            try
            {
                if (table != null)
                {
                    if (table.Rows.Count > 0)
                    {
                        JSONString.Append("[");
                        for (int i = 0; i < table.Rows.Count - 1; i++)
                        {
                            JSONString.Append("{");
                            for (int j = 0; j < table.Columns.Count; j++)
                            {
                                if (j < table.Columns.Count - 1)
                                {
                                    int charindex = table.Rows[i][j].ToString().IndexOf(',');
                                    if (charindex == -1)
                                    {
                                        JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\",");
                                    }
                                    else
                                    {
                                        string s = table.Rows[i][j].ToString();
                                        var chars = s.ToCharArray().ToList();
                                        var builder = new StringBuilder();
                                        foreach (var character in chars)
                                        {
                                            if (char.IsNumber(character))
                                                builder.Append(character);
                                        }
                                        JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + builder + "\",");
                                    }
                                }
                                else if (j == table.Columns.Count - 1)
                                {
                                    JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\"");
                                }
                            }
                            if (i == table.Rows.Count - 2)
                            {
                                JSONString.Append("}");
                            }
                            else
                            {
                                JSONString.Append("},");
                            }
                        }
                        JSONString.Append("]");
                    }

                }
                return JSONString.ToString();
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
                return null;
            }
        }

        #region Bind Grid
        /// <summary>
        /// Author : 
        /// Date   : 
        /// Desc   : assigning css class to the heading of gridview 
        /// </summary>
        protected void bindgridColor()
        {
            if (grdviewAllValues.Rows.Count != 0)
            {
                int color = 0;

                foreach (GridViewRow row in grdviewAllValues.Rows)
                {
                    var Flag = row.FindControl("lblFlag") as Label;

                    if (Flag.Text == "Heading")
                    {
                        for (int i = 0; i < row.Cells.Count; i++) { row.Cells[i].CssClass = "HeadergridAll"; }
                        row.CssClass = "HeadergridAll";
                    }

                }
            }
        }
        /// <summary>
        /// Author : 
        /// Date   : 
        /// Desc   : Loads Gridviews Quaterly Values
        /// </summary>
        /// <param name="Year"></param>
        /// <param name="flag"></param>
        /// <param name="valuein"></param>
        /// <returns></returns>

        protected DataTable getQuarterlyValueSum(int Year, string flag, int valuein = 1000)
        {
            DataTable dtmonthval = new DataTable();
            string roleId = Session["RoleId"].ToString();
            string userId = Session["UserId"].ToString();
            string branchcode = Convert.ToString(Session["SelectedBranchList"]);
            string SalesengList = Convert.ToString(Session["SelectedSalesEngineers"]);
            string CustomerNamelist = Convert.ToString(Session["SelectedCustomerNames"]);
            string CustomerNumlist = Convert.ToString(Session["SelectedCustomerNumbers"]);
            string ProductFamilyList = Convert.ToString(Session["SelectedProductFamily"]);
            string ProductGrpList = Convert.ToString(Session["SelectedProductGroup"]);
            string ApplicationList = Convert.ToString(Session["SelectedApplications"]);
            if (roleId == "TM")
            {
                objRSum.BranchCode = branchcode == "ALL" ? userId : branchcode;
            }
            else { objRSum.BranchCode = branchcode == "ALL" ? null : branchcode; }
            objRSum.salesengineer_id = SalesengList == "ALL" ? null : SalesengList;
            objRSum.customer_type = ddlcustomertype.SelectedItem.Value == "ALL" ? null : ddlcustomertype.SelectedItem.Value;
            objRSum.customer_number = CustomerNumlist == "ALL" ? null : CustomerNumlist;
            objRSum.item_family_name = ProductFamilyList == "ALL" ? null : ProductFamilyList;
            objRSum.item_sub_family_name = null;
            objRSum.Year = Year;
            objRSum.flag = flag;
            objRSum.product_group = ProductGrpList == "ALL" ? null : ProductGrpList;
            objRSum.item_code = ApplicationList == "ALL" ? null : ApplicationList;
            objRSum.valuein = valuein;
            objRSum.cter = cter;
            dtmonthval = objRSum.getQuarterlyValueSum(objRSum);

            return dtmonthval;
        }
        /// <summary>
        /// Author : 
        /// Date   : 
        /// Desc   : Loads Gridview in Thousands
        /// </summary>
        /// <returns></returns>
        protected DataTable loadGrid()
        {

            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };
            DataTable dtYtdSales = new DataTable();
            dtYtdSales.Columns.Add("title", typeof(string));
            dtYtdSales.Columns.Add("quarter1", typeof(string));
            dtYtdSales.Columns.Add("quarter2", typeof(string));
            dtYtdSales.Columns.Add("quarter3", typeof(string));
            dtYtdSales.Columns.Add("quarter4", typeof(string));
            dtYtdSales.Columns.Add("flag", typeof(string));
            DataTable temp = new DataTable();
            int ActualYear = objConfig.getActualYear() - 1;
            ay.Value = Convert.ToString(ActualYear);
            int YTDYear = ActualYear + 1;
            int MTDYear = ActualYear + 1;
            int currentmonth = System.DateTime.Now.Month;
            int quarter = 0;
            if (currentmonth > 0 && currentmonth < 4) { quarter = 1; }
            else if (currentmonth > 3 && currentmonth < 7) { quarter = 2; }
            else if (currentmonth > 6 && currentmonth < 10) { quarter = 3; }
            else { quarter = 4; }
            currentmonth = quarter;

            int Remain_months = 4 - currentmonth;

            /// Adding Header to Table
            ///  
            dtYtdSales.Rows.Add("", "Quarter1", "Quarter2", "Quarter3", "Quarter4", "Heading");



            /// 
            /// Step:1 Mtd PLAN 2015
            /// 
            /// --------------------------------------------------------------------------------------------------------------------
            /// MTD PLAN 2015	

            temp = getQuarterlyValueSum(MTDYear, "QTD Plan");
            decimal YTDBudget = 0; ;
            ///
            /// MTD PLAN 2015 formula = ((BUDGET 2015)-(YTD SALE 2015))/(No. of months remaining) 
            /// Work in progress
            ///               

            DataTable dtSale = new DataTable();
            decimal val1 = 0, val2 = 0, val3 = 0, val4 = 0;
            dtSale = getQuarterlyValueSum(YTDYear, "YTD Sale");
            for (int i = 0; i < temp.Rows.Count; i++)
            {
                YTDBudget = temp.Rows[i].ItemArray[0].ToString() != "" ? Convert.ToDecimal(temp.Rows[i].ItemArray[0].ToString()) : 0;
                val1 = YTDBudget / 4; // jan
                for (int j = 0; j < dtSale.Rows.Count; j++)
                {
                    if (i == j)
                    {
                        val2 = currentmonth > 1 ? (YTDBudget - (dtSale.Rows[j].ItemArray[0].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[0].ToString()) : 0)) / 11 : val1; //feb
                        val3 = currentmonth > 2 ? (YTDBudget - (dtSale.Rows[j].ItemArray[1].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[1].ToString()) : 0)) / 10 : val2; // mar
                        val4 = currentmonth > 3 ? (YTDBudget - (dtSale.Rows[j].ItemArray[2].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[2].ToString()) : 0)) / 9 : val3; // apr

                        try
                        {
                            dtYtdSales.Rows.Add("QTD PLAN " + MTDYear,
                                               Math.Round(val1).ToString("N0", culture), //1
                                               Math.Round(val2).ToString("N0", culture), //2
                                               Math.Round(val3).ToString("N0", culture), //3
                                               Math.Round(val4).ToString("N0", culture),//4

                                               "QTDPLAN CurrentYear"
                                               );
                        }
                        catch (Exception ex) { }
                    }
                }
            }


            /// Step:2 MTD SALE 2015
            ///---------------------------------------------------------------------------------------------------------------------------
            ///MTD SALE 2015	
            ///MTD SALE 2015 = Actual sale for that month in 2015 ( so if we have logged in June’15,
            ///then Jun-Dec fields would show 0 value; 
            ///only after end of month will data show for that month because we would get monthly dumps only)
            ///
            temp = null;
            temp = getQuarterlyValueSum(MTDYear, "QTD Sale");
            for (int i = 0; i < temp.Rows.Count; i++)
            {
                int loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString());
                int loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString());
                int loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString());
                int loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString());
                dtYtdSales.Rows.Add("QTD SALE " + MTDYear,
                                   loc1.ToString("N0", culture),
                                   loc2.ToString("N0", culture),
                                   loc3.ToString("N0", culture),
                                   loc4.ToString("N0", culture),

                                    "QTDSALE CurrentYear");

            }



            /// STEP: 3 YTD PLAN 2015
            /// --------------------------------------------------------------------------------------------------------------------
            /// YTD PLAN 2015
            /// 
            ///If  Cell_month != Actual_month THEN   YTD_Plan(Cell_month) = YTD_Plan(Cell_month -1) + MTD_Plan(Cell_Month) 
            ///If  Cell_month = Actual_month THEN   YTD_Plan(Cell_month) = YTD_actuals(Cell_month -1) + MTD_Plan(Cell_Month) 
            ///
            ///
            temp = null;


            decimal YTD_val1 = 0, YTD_val2 = 0, YTD_val3 = 0, YTD_val4 = 0;

            for (int j = 0; j < dtSale.Rows.Count; j++)
            {
                YTD_val1 = YTDBudget / 4; // jan
                YTD_val2 = currentmonth != 2 ? YTD_val1 + val2 : (val2 + (dtSale.Rows[j].ItemArray[0].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[0].ToString()) : 0)); //feb
                YTD_val3 = currentmonth != 3 ? YTD_val2 + val3 : (val3 + (dtSale.Rows[j].ItemArray[1].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[1].ToString()) : 0));  // mar
                YTD_val4 = currentmonth != 4 ? YTD_val3 + val4 : (val4 + (dtSale.Rows[j].ItemArray[2].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[2].ToString()) : 0)); // apr

                try
                {
                    dtYtdSales.Rows.Add("YTD PLAN " + YTDYear,
                                       Math.Round(YTD_val1).ToString("N0", culture), //1
                                       Math.Round(YTD_val2).ToString("N0", culture), //2
                                       Math.Round(YTD_val3).ToString("N0", culture), //3
                                       Math.Round(YTD_val4).ToString("N0", culture),//4
                                       "YTDPLAN CurrentYear"
                                       );
                }
                catch (Exception ex) { }

            }



            /// STEP: 4 YTD SALE 2015
            /// 
            ///-----------------------------------------------------------------------------------------------------------------------------------
            ///YTD SALE 2015	
            ///YTD SALE 2015 = Cumulative total of MTD sales, as in d); 
            ///however, if logged in June’15, then Jun-Dec fields would show same data as NA not applicable)
            ///
            temp = null;

            temp = getQuarterlyValueSum(YTDYear, "YTD Sale");
            for (int i = 0; i < temp.Rows.Count; i++)
            {

                int loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString());
                int loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString());
                int loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString());
                int loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString());

                dtYtdSales.Rows.Add("YTD SALE " + YTDYear,
                                   currentmonth < 1 ? "0" : Convert.ToInt32(loc1).ToString("N0", culture),
                                   currentmonth < 2 ? "0" : Convert.ToInt32(loc2).ToString("N0", culture),
                                   currentmonth < 3 ? "0" : Convert.ToInt32(loc3).ToString("N0", culture),
                                   currentmonth < 4 ? "0" : Convert.ToInt32(loc4).ToString("N0", culture),
                                   "YTDSALE CurrentYear");
            }

            /// --------------------------------------------------------------------------------------------------------------------
            /// STEP: 5 YTD SALE 2014
            temp = null;
            temp = getQuarterlyValueSum(ActualYear, "YTD Sale");

            for (int i = 0; i < temp.Rows.Count; i++)
            {
                int loc1 = temp.Rows[i].ItemArray[0].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString());
                int loc2 = temp.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString());
                int loc3 = temp.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString());
                int loc4 = temp.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString());

                dtYtdSales.Rows.Add("YTD SALE " + ActualYear,
                                   loc1.ToString("N0", culture),
                                   loc2.ToString("N0", culture),
                                   loc3.ToString("N0", culture),
                                   loc4.ToString("N0", culture),
                                   "YTDSALE ActualYear"
                                   );

            }




            decimal temp1 = 0, temp2 = 0, temp3 = 0, temp4 = 0;
            decimal ys1 = 0, ys2 = 0, ys3 = 0, ys4 = 0;
            decimal ys_a1 = 0, ys_a2 = 0, ys_a3 = 0, ys_a4 = 0;
            for (int i = 0; i < dtYtdSales.Rows.Count; i++)
            {
                string flag = dtYtdSales.Rows[i].ItemArray[5].ToString();
                //execute one time
                if (flag == "YTDSALE ActualYear")
                {
                    ys_a1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                    ys_a2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                    ys_a3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                    ys_a4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                }
                if (flag == "YTDPLAN CurrentYear")
                {
                    temp1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                    temp2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                    temp3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                    temp4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                }
                if (flag == "YTDSALE CurrentYear")
                {
                    ys1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                    ys2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                    ys3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                    ys4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                }

            }

            ///
            ///PRO-RATE ACH% = ((YTD SALE 2015)/(YTD PLAN 2015))x100
            ///GROWTH% = ((YTD SALES 2015)/(YTD SALE 2014))x100             
            decimal result1 = 0, result2 = 0, result3 = 0, result4 = 0;
            result1 = temp1 != 0 ? ((decimal)ys1 / (decimal)temp1) * 100 : 0;
            result2 = temp2 != 0 ? (((decimal)ys2 / (decimal)temp2) * 100) : 0;
            result3 = temp3 != 0 ? (((decimal)ys3 / (decimal)temp3) * 100) : 0;
            result4 = temp4 != 0 ? (((decimal)ys4 / (decimal)temp4) * 100) : 0;

            dtYtdSales.Rows.Add("PRO-RATA ACH%",
                                 Math.Round(result1),
                                 Math.Round(result2),
                                 Math.Round(result3),
                                 Math.Round(result4),
                                 "ProRate"
                                );

            ///changed Growth% = ((YTD SALE 2015)-(YTD SALE 2014))*100/YTD PLAN 2014
            result1 = 0; result2 = 0; result3 = 0; result4 = 0;
            result1 = ys_a1 != 0 ? (((decimal)ys1 - (decimal)ys_a1) * 100) / (decimal)ys_a1 : 0;
            result2 = ys_a1 != 0 ? (((decimal)ys2 - (decimal)ys_a2) * 100) / (decimal)ys_a2 : 0;
            result3 = ys_a1 != 0 ? (((decimal)ys3 - (decimal)ys_a3) * 100) / (decimal)ys_a3 : 0;
            result4 = ys_a1 != 0 ? (((decimal)ys4 - (decimal)ys_a4) * 100) / (decimal)ys_a4 : 0;
            dtYtdSales.Rows.Add("GROWTH%",
                                 Math.Round(result1),
                                 Math.Round(result2),
                                 Math.Round(result3),
                                 Math.Round(result4),
                                 "Growth"
                                );


            return dtYtdSales;
        }
        /// <summary>
        /// Author : 
        /// Date   : 
        /// Desc   : Loads Gridview in Laksh
        /// </summary>
        /// <returns></returns>
        protected DataTable loadGrid_lakh()
        {

            var culture = new CultureInfo("en-us", true)
            {
                NumberFormat =
                {
                    NumberGroupSizes = new int[] { 2, 2 }
                }
            };
            DataTable dtYtdSales = new DataTable();
            dtYtdSales.Columns.Add("title", typeof(string));
            dtYtdSales.Columns.Add("quarter1", typeof(string));
            dtYtdSales.Columns.Add("quarter2", typeof(string));
            dtYtdSales.Columns.Add("quarter3", typeof(string));
            dtYtdSales.Columns.Add("quarter4", typeof(string));
            dtYtdSales.Columns.Add("flag", typeof(string));
            DataTable temp = new DataTable();
            int ActualYear = objConfig.getActualYear() - 1;
            int YTDYear = ActualYear + 1;
            int MTDYear = ActualYear + 1;
            int currentmonth = System.DateTime.Now.Month;
            int quarter = 0;
            if (currentmonth > 0 && currentmonth < 4) { quarter = 1; }
            else if (currentmonth > 3 && currentmonth < 7) { quarter = 2; }
            else if (currentmonth > 6 && currentmonth < 10) { quarter = 3; }
            else { quarter = 4; }
            currentmonth = quarter;

            int Remain_months = 4 - currentmonth;

            /// Adding Header to Table
            ///  
            dtYtdSales.Rows.Add("", "Quarter1", "Quarter2", "Quarter3", "Quarter4", "Heading");


            /// 
            /// Step:1 Mtd PLAN 2015
            /// 
            /// --------------------------------------------------------------------------------------------------------------------
            /// MTD PLAN 2015	

            temp = getQuarterlyValueSum(MTDYear, "QTD Plan", 100000);
            decimal YTDBudget = 0; ;
            ///
            /// MTD PLAN 2015 formula = ((BUDGET 2015)-(YTD SALE 2015))/(No. of months remaining) 
            /// Work in progress
            ///               

            DataTable dtSale = new DataTable();
            decimal val1 = 0, val2 = 0, val3 = 0, val4 = 0;
            dtSale = getQuarterlyValueSum(YTDYear, "YTD Sale", 100000);
            for (int i = 0; i < temp.Rows.Count; i++)
            {
                YTDBudget = temp.Rows[i].ItemArray[0].ToString() != "" ? Convert.ToDecimal(temp.Rows[i].ItemArray[0].ToString()) : 0;
                val1 = YTDBudget / 12; // jan
                for (int j = 0; j < dtSale.Rows.Count; j++)
                {
                    if (i == j)
                    {
                        val2 = currentmonth > 1 ? (YTDBudget - (dtSale.Rows[j].ItemArray[0].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[0].ToString()) : 0)) / 11 : val1; //feb
                        val3 = currentmonth > 2 ? (YTDBudget - (dtSale.Rows[j].ItemArray[1].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[1].ToString()) : 0)) / 10 : val2; // mar
                        val4 = currentmonth > 3 ? (YTDBudget - (dtSale.Rows[j].ItemArray[2].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[2].ToString()) : 0)) / 9 : val3; // apr

                        try
                        {
                            dtYtdSales.Rows.Add("QTD PLAN " + MTDYear,
                                               Math.Round(val1).ToString("N0", culture), //1
                                               Math.Round(val2).ToString("N0", culture), //2
                                               Math.Round(val3).ToString("N0", culture), //3
                                               Math.Round(val4).ToString("N0", culture),//4                                              
                                               "QTDPLAN CurrentYear"
                                               );
                        }
                        catch (Exception ex) { }
                    }
                }
            }


            /// Step:2 MTD SALE 2015
            ///---------------------------------------------------------------------------------------------------------------------------
            ///MTD SALE 2015	
            ///MTD SALE 2015 = Actual sale for that month in 2015 ( so if we have logged in June’15,
            ///then Jun-Dec fields would show 0 value; 
            ///only after end of month will data show for that month because we would get monthly dumps only)
            ///
            temp = null;
            temp = getQuarterlyValueSum(MTDYear, "QTD Sale", 100000);
            for (int i = 0; i < temp.Rows.Count; i++)
            {
                dtYtdSales.Rows.Add("QTD SALE " + MTDYear,
                    Convert.ToInt32((temp.Rows[i].ItemArray[0].ToString() == "") ? "0" : (temp.Rows[i].ItemArray[0].ToString())).ToString("N0", culture),
                    Convert.ToInt32((temp.Rows[i].ItemArray[1].ToString() == "") ? "0" : (temp.Rows[i].ItemArray[1].ToString())).ToString("N0", culture),
                    Convert.ToInt32((temp.Rows[i].ItemArray[2].ToString() == "") ? "0" : (temp.Rows[i].ItemArray[2].ToString())).ToString("N0", culture),
                    Convert.ToInt32((temp.Rows[i].ItemArray[3].ToString() == "") ? "0" : (temp.Rows[i].ItemArray[3].ToString())).ToString("N0", culture),
                    // Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString()).ToString("N0", culture),
                    //Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString()).ToString("N0", culture),
                    // Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString()).ToString("N0", culture),
                                   "QTDSALE CurrentYear");

            }


            /// STEP: 3 YTD PLAN 2015
            /// --------------------------------------------------------------------------------------------------------------------
            /// YTD PLAN 2015
            /// 
            ///If  Cell_month != Actual_month THEN   YTD_Plan(Cell_month) = YTD_Plan(Cell_month -1) + MTD_Plan(Cell_Month) 
            ///If  Cell_month = Actual_month THEN   YTD_Plan(Cell_month) = YTD_actuals(Cell_month -1) + MTD_Plan(Cell_Month) 
            ///
            ///
            temp = null;


            decimal YTD_val1 = 0, YTD_val2 = 0, YTD_val3 = 0, YTD_val4 = 0;
            for (int j = 0; j < dtSale.Rows.Count; j++)
            {
                YTD_val1 = YTDBudget / 12; // jan
                YTD_val2 = currentmonth != 2 ? YTD_val1 + val2 : (val2 + (dtSale.Rows[j].ItemArray[0].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[0].ToString()) : 0)); //feb
                YTD_val3 = currentmonth != 3 ? YTD_val2 + val3 : (val3 + (dtSale.Rows[j].ItemArray[1].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[1].ToString()) : 0));  // mar
                YTD_val4 = currentmonth != 4 ? YTD_val3 + val4 : (val4 + (dtSale.Rows[j].ItemArray[2].ToString() != "" ? Convert.ToDecimal(dtSale.Rows[j].ItemArray[2].ToString()) : 0)); // apr

                try
                {
                    dtYtdSales.Rows.Add("YTD PLAN " + YTDYear,
                                       Math.Round(YTD_val1).ToString("N0", culture), //1
                                       Math.Round(YTD_val2).ToString("N0", culture), //2
                                       Math.Round(YTD_val3).ToString("N0", culture), //3
                                       Math.Round(YTD_val4).ToString("N0", culture),//4
                                       "YTDPLAN CurrentYear"
                                       );
                }
                catch (Exception ex) { }

            }


            /// STEP: 4 YTD SALE 2015
            /// 
            ///-----------------------------------------------------------------------------------------------------------------------------------
            ///YTD SALE 2015	
            ///YTD SALE 2015 = Cumulative total of MTD sales, as in d); 
            ///however, if logged in June’15, then Jun-Dec fields would show same data as NA not applicable)
            ///
            temp = null;

            temp = getQuarterlyValueSum(YTDYear, "YTD Sale", 100000);
            for (int i = 0; i < temp.Rows.Count; i++)
            {
                dtYtdSales.Rows.Add("YTD SALE " + YTDYear,
                                   currentmonth < 1 ? "0" : Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString()).ToString("N0", culture),
                                   currentmonth < 2 ? "0" : Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString()).ToString("N0", culture),
                                   currentmonth < 3 ? "0" : Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString()).ToString("N0", culture),
                                   currentmonth < 4 ? "0" : Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString()).ToString("N0", culture),
                                   "YTDSALE CurrentYear");
            }

            /// --------------------------------------------------------------------------------------------------------------------
            /// STEP: 5 YTD SALE 2014
            temp = null;
            temp = getQuarterlyValueSum(ActualYear, "YTD Sale", 100000);

            for (int i = 0; i < temp.Rows.Count; i++)
            {

                //pending to add all values
                dtYtdSales.Rows.Add("YTD SALE " + ActualYear,
                                   Convert.ToInt32(temp.Rows[i].ItemArray[0].ToString()).ToString("N0", culture),
                                   Convert.ToInt32(temp.Rows[i].ItemArray[1].ToString()).ToString("N0", culture),
                                   Convert.ToInt32(temp.Rows[i].ItemArray[2].ToString()).ToString("N0", culture),
                                   Convert.ToInt32(temp.Rows[i].ItemArray[3].ToString()).ToString("N0", culture),
                                   "YTDSALE ActualYear"
                                   );

            }




            decimal temp1 = 0, temp2 = 0, temp3 = 0, temp4 = 0;
            decimal ys1 = 0, ys2 = 0, ys3 = 0, ys4 = 0;
            decimal ys_a1 = 0, ys_a2 = 0, ys_a3 = 0, ys_a4 = 0;
            for (int i = 0; i < dtYtdSales.Rows.Count; i++)
            {
                string flag = dtYtdSales.Rows[i].ItemArray[5].ToString();
                //execute one time
                if (flag == "YTDSALE ActualYear")
                {
                    ys_a1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                    ys_a2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                    ys_a3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                    ys_a4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());

                }
                if (flag == "YTDPLAN CurrentYear")
                {
                    temp1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                    temp2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                    temp3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                    temp4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());

                }
                if (flag == "YTDSALE CurrentYear")
                {
                    ys1 = dtYtdSales.Rows[i].ItemArray[1].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[1].ToString());
                    ys2 = dtYtdSales.Rows[i].ItemArray[2].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[2].ToString());
                    ys3 = dtYtdSales.Rows[i].ItemArray[3].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[3].ToString());
                    ys4 = dtYtdSales.Rows[i].ItemArray[4].ToString() == "" ? 0 : Convert.ToDecimal(dtYtdSales.Rows[i].ItemArray[4].ToString());
                }

            }

            ///
            ///PRO-RATE ACH% = ((YTD SALE 2015)/(YTD PLAN 2015))x100
            ///GROWTH% = ((YTD SALES 2015)/(YTD SALE 2014))x100             
            decimal result1 = 0, result2 = 0, result3 = 0, result4 = 0;
            result1 = temp1 != 0 ? ((decimal)ys1 / (decimal)temp1) * 100 : 0;
            result2 = temp2 != 0 ? (((decimal)ys2 / (decimal)temp2) * 100) : 0;
            result3 = temp3 != 0 ? (((decimal)ys3 / (decimal)temp3) * 100) : 0;
            result4 = temp4 != 0 ? (((decimal)ys4 / (decimal)temp4) * 100) : 0;
            dtYtdSales.Rows.Add("PRO-RATA ACH%",
                                 Math.Round(result1),
                                 Math.Round(result2),
                                 Math.Round(result3),
                                 Math.Round(result4),

                                 "ProRate"
                                );

            ///changed Growth% = ((YTD SALE 2015)-(YTD SALE 2014))*100/YTD SALE 2014
            result1 = 0; result2 = 0; result3 = 0; result4 = 0;
            result1 = ys_a1 != 0 ? (((decimal)ys1 - (decimal)ys_a1) * 100) / (decimal)ys_a1 : 0;
            result2 = ys_a1 != 0 ? (((decimal)ys2 - (decimal)ys_a2) * 100) / (decimal)ys_a2 : 0;
            result3 = ys_a1 != 0 ? (((decimal)ys3 - (decimal)ys_a3) * 100) / (decimal)ys_a3 : 0;
            result4 = ys_a1 != 0 ? (((decimal)ys4 - (decimal)ys_a4) * 100) / (decimal)ys_a4 : 0;
            dtYtdSales.Rows.Add("GROWTH%",
                                 Math.Round(result1),
                                 Math.Round(result2),
                                 Math.Round(result3),
                                 Math.Round(result4),

                                 "Growth"
                                );


            return dtYtdSales;
        }
        /// <summary>
        /// Author : 
        /// Date   : 
        /// Desc   : we are tranposing the given input table by converting rows to columns and columns to rows 
        /// </summary>
        /// <param name="inputTable"></param>
        /// <returns></returns>
        private DataTable GenerateTransposedTable(DataTable inputTable)
        {
            DataTable outputTable = new DataTable();

            // Add columns by looping rows

            // Header row's first column is same as in inputTable
            outputTable.Columns.Add(inputTable.Columns[0].ColumnName.ToString());

            // Header row's second column onwards, 'inputTable's first column taken
            foreach (DataRow inRow in inputTable.Rows)
            {
                string newColName = inRow[0].ToString();
                outputTable.Columns.Add(newColName);
            }

            // Add rows by looping columns        
            for (int rCount = 1; rCount <= inputTable.Columns.Count - 1; rCount++)
            {
                DataRow newRow = outputTable.NewRow();

                // First column is inputTable's Header row's second column
                newRow[0] = inputTable.Columns[rCount].ColumnName.ToString();
                for (int cCount = 0; cCount <= inputTable.Rows.Count - 1; cCount++)
                {
                    string colValue = inputTable.Rows[cCount][rCount].ToString();
                    newRow[cCount + 1] = colValue;
                }
                outputTable.Rows.Add(newRow);
            }

            return outputTable;
        }
        #endregion

        #region bind chart
        protected void bindchart(DataTable dtChart)
        {


            //LoadChartData(dtChart);
            string column1 = dtChart.Columns[1].ColumnName.ToString(); // Months 
            string column2 = dtChart.Columns[5].ColumnName.ToString(); // YTD SALE
            string column3 = dtChart.Columns[4].ColumnName.ToString(); // YTD PLAN
            string column4 = dtChart.Columns[6].ColumnName.ToString(); // YTD SALE PREVIOUS YEAR

            //Chart2.DataSource = dtChart;
            //Chart2.Series.Clear();
            //Chart2.Series.Add(column2);
            //Chart2.Series[column2].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Column;

            //Chart2.Series.Add(column3);
            //Chart2.Series[column3].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Line;
            //Chart2.Series[column3].IsValueShownAsLabel = true;
            //Chart2.Series.Add("Series4");
            //Chart2.Series["Series4"].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Point;
            //Chart2.Series.Add(column4);
            //Chart2.Series[column4].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Line;
            //Chart2.Series[column4].IsValueShownAsLabel = true;
            //Chart2.Series.Add("Series5");
            //Chart2.Series["Series5"].ChartType = System.Web.UI.DataVisualization.Charting.SeriesChartType.Point;
            //Chart2.Series[column2].Color = ColorTranslator.FromHtml("#EE4394");
            //Chart2.Series[column3].Color = ColorTranslator.FromHtml("#0A438C");
            //Chart2.Series["Series4"].Color = ColorTranslator.FromHtml("#097054");
            //Chart2.Series[column4].Color = ColorTranslator.FromHtml("#FF9900");
            //Chart2.Series["Series5"].Color = ColorTranslator.FromHtml("#097054");
            ////Bar chart
            //Chart2.Series[column2].XValueMember = column1;
            //Chart2.Series[column2].YValueMembers = column2;
            ////Line chart YTD PLAN 2015
            //Chart2.Series[column3].XValueMember = column1;
            //Chart2.Series[column3].YValueMembers = column3;
            //Chart2.Series["Series4"].XValueMember = column1;
            //Chart2.Series["Series4"].YValueMembers = column3;
            //Chart2.Series["Series4"].IsVisibleInLegend = false;
            // Line chaert YTD SALE 2014
            //Chart2.Series[column4].XValueMember = column1;
            //Chart2.Series[column4].YValueMembers = column4;
            //Chart2.Series["Series5"].XValueMember = column1;
            //Chart2.Series["Series5"].YValueMembers = column4;

            //Chart2.Series["Series5"].IsVisibleInLegend = false;
            //Chart2.Series["column3"].BorderWidth = 4;
            //Chart2.Series[column2].SetCustomProperty("DrawingStyle", "Cylinder");

            //Chart2.DataBind();

            //// making user friendly

            //Chart2.ChartAreas["ChartArea1"].AxisX.Interval = 1;
            //Chart2.Series[column2].ToolTip = column2 + " " + ":" + " " + "#VALY"; ;
            //Chart2.Series[column3].ToolTip = column3 + " " + ":" + " " + "#VALY"; ;
            //Chart2.Series[column4].ToolTip = column4 + " " + ":" + " " + "#VALY";
            //Chart2.Visible = true;
            //Chart2.Legends["Legend2"].CellColumns.Add(new LegendCellColumn(column2, LegendCellColumnType.Text, "MTD SALE"));
            //Random random = new Random();
            //foreach (var item in Chart2.Series["YTD SALE"].Points)
            //{
            //    Color c = Color.FromArgb(random.Next(0, 255), random.Next(0, 255), random.Next(0, 255));
            //    item.Color = c;
            //}


            ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "ShowPopup", "triggerPostGridLodedActions();", true);
        }

        #endregion

        #endregion

   

        #region Compression
        private byte[] Compress(byte[] b)
        {
            MemoryStream ms = new MemoryStream();
            GZipStream zs = new GZipStream(ms, CompressionMode.Compress, true);
            zs.Write(b, 0, b.Length);
            zs.Close();
            return ms.ToArray();
        }

        /// This method takes the compressed byte stream as parameter
        /// and return a decompressed bytestream.

        private byte[] Decompress(byte[] b)
        {
            MemoryStream ms = new MemoryStream();
            GZipStream zs = new GZipStream(new MemoryStream(b),
                                           CompressionMode.Decompress, true);
            byte[] buffer = new byte[4096];
            int size;
            while (true)
            {
                size = zs.Read(buffer, 0, buffer.Length);
                if (size > 0)
                    ms.Write(buffer, 0, size);
                else break;
            }
            zs.Close();
            return ms.ToArray();
        }

        protected override object LoadPageStateFromPersistenceMedium()
        {
            System.Web.UI.PageStatePersister pageStatePersister1 = this.PageStatePersister;
            pageStatePersister1.Load();
            String vState = pageStatePersister1.ViewState.ToString();
            byte[] pBytes = System.Convert.FromBase64String(vState);
            pBytes = Decompress(pBytes);
            LosFormatter mFormat = new LosFormatter();
            Object ViewState = mFormat.Deserialize(System.Convert.ToBase64String(pBytes));
            return new Pair(pageStatePersister1.ControlState, ViewState);
        }

        protected override void SavePageStateToPersistenceMedium(Object pViewState)
        {
            Pair pair1;
            System.Web.UI.PageStatePersister pageStatePersister1 = this.PageStatePersister;
            Object ViewState;
            if (pViewState is Pair)
            {
                pair1 = ((Pair)pViewState);
                pageStatePersister1.ControlState = pair1.First;
                ViewState = pair1.Second;
            }
            else
            {
                ViewState = pViewState;
            }
            LosFormatter mFormat = new LosFormatter();
            StringWriter mWriter = new StringWriter();
            mFormat.Serialize(mWriter, ViewState);
            String mViewStateStr = mWriter.ToString();
            byte[] pBytes = System.Convert.FromBase64String(mViewStateStr);
            pBytes = Compress(pBytes);
            String vStateStr = System.Convert.ToBase64String(pBytes);
            pageStatePersister1.ViewState = vStateStr;
            pageStatePersister1.Save();
        }
        #endregion


        #region webmethods
        /// <summary>
     ///  Author:K.LakshmiBindu
     /// Date:
     /// Desc: For providing data to graph 
     /// </summary>
     /// <returns></returns>

        [WebMethod]
        public static string AmchartGraphQuaterly()
        {
            try
            {
                DataTable dtoriginal_qty = new DataTable();
                dtoriginal_qty = HttpContext.Current.Session["dtoriginal"] as DataTable;

                string fc = Convert.ToString(HttpContext.Current.Session["FC"]);
                if (fc == "1")
                {
                    for (int i = dtoriginal_qty.Rows.Count; i > 0; i--)
                    {
                        string fl = dtoriginal_qty.Rows[0][0].ToString();
                        if (Convert.ToString(dtoriginal_qty.Rows[0][0]) == "flag")
                        {
                        }
                        else
                        {
                            for (int j = 0; j < dtoriginal_qty.Rows[i - 1].ItemArray.Length; j++)
                            {
                                string columnname = Convert.ToString(dtoriginal_qty.Rows[i - 1].ItemArray[j]);
                                for (int k = j; k < dtoriginal_qty.Columns.Count; k++)
                                {
                                    dtoriginal_qty.Columns[k].ColumnName = columnname;
                                    break;
                                }

                            }
                        }
                        DataRow row = dtoriginal_qty.Rows[i - 1];
                       // row.Delete();

                        break;
                    }
                }
                String jsonString = null;
                if (dtoriginal_qty != null)
                {
                    jsonString = DataTableToJSONWithStringBuilder(dtoriginal_qty);
                //    dtoriginal_qty.Clear();
                }
                return jsonString;
            }
            catch (Exception ex)
            {
                CommonFunctions.LogErrorStatic(ex);
                return null;
            }
        }

        #endregion
    }
    }
