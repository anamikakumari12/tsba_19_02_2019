﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JSRReportByApplicationCustomer.aspx.cs" Inherits="TaegutecSalesBudget.JSRReportByApplicationCustomer" MasterPageFile="~/JSRMaster.Master" %>

<asp:Content ContentPlaceHolderID="HeadContent" ID="content1" runat="server">
        <link href="GridviewScroll.css" rel="stylesheet" />
    <script type="text/javascript" src="gridscroll.js"></script>
    <script type="text/javascript" src="js/app.js"></script>

    <style type="text/css">
        td
        {
            height: 55px !important;
        }
        /*body{
        overflow:hidden !important;
        }*/
        .noclose .ui-dialog-titlebar-close
        {
            display: none;
        }

        .ui-dialog .ui-dialog-titlebar
        {
            padding-left: 45px;
            text-align: center !important;
        }
        /*.col-md-4 {
         width: 33.3333% !important;
        }*/

        #MainContent_grdviewAllValues_Image1_0
        {
            display: none;
        }

        #MainContent_grdviewAllValues td
        {
            text-align: center;
        }

        #MainContent_rbtnlistComp td
        {
            height: 0px !important;
            padding: 0px !important;
        }
        .btn.sidelight {
    /*padding: 7px;
    width: 120px;
    display: block;
    margin: 10px;
    float: left;
    text-align: center;*/
    /*color: #fff;*/
    /*background: radial-gradient(circle at 100px 100px, #025995, #047fb4, #025995);*/


        color: #fff;
    margin-left: auto;
    margin-right: auto;

    overflow: auto;
    cursor: pointer;
   
    position: relative;
    background: -webkit-linear-gradient(rgba(2, 89, 149, 1), rgba(2, 89, 149, 0) 0%), -webkit-linear-gradient(-45deg, rgba(4, 128, 181, 0.9) 45%, rgba(4, 128, 181, 1) 0%);
    behavior: url(/common/assets/css/pie/PIE.htc);
    padding: 7px;
    width: 100px;
    font-weight: 600;
    margin-top: 5px;
}
    </style>
</asp:Content>

<asp:Content ContentPlaceHolderID="MainContent" ID="content2" runat="server">
           <asp:ScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360" EnableHistory="true" EnablePartialRendering="true" EnablePageMethods="true" >
   </asp:ScriptManager>
   
     
   <asp:UpdatePanel ID="UpdatePanel1" runat="server"  >
       <ContentTemplate>  
            <div class="crumbs">
                <!-- Start : Breadcrumbs -->
                <ul id="Ul1" class="breadcrumb">
                    <li>
                        <i class="fa fa-home"></i>
                        <a>Reports</a>
                    </li>
                    <li class="current">JSR Report By Application</li>
                    <li class="current">JSR Report By Customer</li>
                    <div>
                        <ul>
                            <li class="title_bedcrum" style="list-style: none;">JSR REPORT BY CUSTOMER</li>
                        </ul>
                    </div>
                    <div>
                        <ul style="float: right; list-style: none; margin-top: -4px; width: 265px; margin-right: -5px;" class="alert alert-danger fade in">
                            <li>
                                <span style="margin-right: -1px; margin-left: 5px; vertical-align: text-bottom;">Val In '000</span>
                                <asp:RadioButton ID="rbtn_Thousand" OnCheckedChanged="Thousand_CheckedChanged" AutoPostBack="true" GroupName="customer" runat="server" Checked="True" />
                                <span style="margin-right: 0px; margin-left: 6px; vertical-align: text-bottom;">Val In Lakhs</span>
                                <asp:RadioButton ID="rbtn_Lakhs" OnCheckedChanged="Lakhs_CheckedChanged" AutoPostBack="true" GroupName="customer" runat="server" />
                            </li>
                        </ul>
                    </div>
                </ul>
                <!-- End : Breadcrumbs -->
            </div>
<%--           <div>

     <div class="crumbs" style="float: left; padding-right: 10px; padding-left: 10px; width: 100%;">
                <!-- Start : Breadcrumbs -->
                <ul id="breadcrumbs" class="breadcrumb">
                    <li>
                        <ul>
                            <li class="title_bedcrum" style="list-style: none; text-align: center">JSR REPORT BY CUSTOMER</li>
                        </ul>
                        </li>
                </ul>
                <!-- End : Breadcrumbs -->
            </div>
               </div>--%>
           <br />
            <div class="row filter_panel" id="reportdrpdwns" runat="server">
           <div class="col-md-12" style="margin-top:14px;">
                    <div class="form-group">

                        <div>
                            <asp:Button ID="collapse" runat="server" Text="COLLAPSE ALL" CssClass="btn sidelight"  OnClientClick="return true;" />
                            <asp:Button ID="expand" runat="server" Text="EXPAND ALL" CssClass="btn sidelight"  OnClientClick="return false;" />
                            <asp:Button ID="exportexcel" runat="server" Text="EXPORT" CssClass="btn sidelight" OnClick="exportexcel_Click" />
                            <asp:Button ID="exportfamily" runat="server" Text="EXPORT SUMMARY" style="    width: 130px;" CssClass="btn sidelight" OnClick="exportfamily_Click"/>
                        </div>
                    </div>
                </div>
                </div>
           <br />
     <div style="float: left; padding-right: 10px; padding-left: 10px; width: 100%;">
              <asp:Label ID="lblResult" runat="server"  style="color: red; font-size: 12;"></asp:Label>
                <asp:GridView ID="grdviewAllValues" runat="server" ViewStateMode="Enabled" AutoGenerateColumns="False" ShowHeader="false" Width="100%">
                    <Columns>
                        <asp:TemplateField>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <div>
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/button_plus.gif" Visible='<%# (Eval("sumFlag").ToString() == "BranchHeading") || (Eval("sumFlag").ToString() == "SEHeading") || (Eval("sumFlag").ToString() == "HidingHeading") %>' ImageAlign="Left" />
                                    </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        
                        <asp:TemplateField HeaderText="">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="Label1" Text='<%# Eval("CUSTOMER_NUMBER".ToString()) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="T_CLAMP_PARTING_OFF ffffffffffffffffffffffff" ItemStyle-Width="160px"><%--HeaderStyle-CssClass="greendark">--%>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="Label2" Text='<%# (Eval("BRANCH").ToString()) %>' Visible='<%#Eval("sumFlag").ToString() == "BranchHeading" || Eval("sumFlag").ToString() == "HidingHeading"  %>' CssClass="productLabel"></asp:Label>
                                <asp:Label runat="server" ID="Label4" Text='<%# (Eval("SE_NAME").ToString()) %>' Visible='<%#Eval("sumFlag").ToString() == "SEHeading"  %>' CssClass="productLabel"></asp:Label>
                                
                                <asp:Label runat="server" ID="Label3" Text='<%# (Eval("CUSTOMER_NAME").ToString()) %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblSumFlag" runat="server" Text='<%# Eval("sumFlag").ToString()%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField Visible="false">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" CssClass="color_3" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_customer_num" Text='<%# (Eval("CUSTOMER_NUMBER").ToString()) %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="100px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_SALES_MTD_VALUE" Text='<%# (Eval("SALES_MTD_VALUE").ToString()) %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("SALES_MTD_VALUE")))? Convert.ToString(Eval("SALES_MTD_VALUE")).Substring(0,1)=="-"? System.Drawing.Color.Red :(Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum")?System.Drawing.Color.Black: System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="100px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_SALES_MTD_VALUE_LY" Text='<%# (Eval("SALES_MTD_VALUE_LY").ToString()) %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("SALES_MTD_VALUE_LY")))? Convert.ToString(Eval("SALES_MTD_VALUE_LY")).Substring(0,1)=="-"? System.Drawing.Color.Red :(Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum")?System.Drawing.Color.Black: System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="100px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_GROWTH_MONTHLY" Text='<%# (Eval("GROWTH_MONTHLY").ToString()) %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("GROWTH_MONTHLY")))? Convert.ToString(Eval("GROWTH_MONTHLY")).Substring(0,1)=="-"? System.Drawing.Color.Red :(Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum")?System.Drawing.Color.Black: System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="100px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_BUDGET_MTD" Text='<%# (Eval("BUDGET_MTD").ToString()) %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("BUDGET_MTD")))? Convert.ToString(Eval("BUDGET_MTD")).Substring(0,1)=="-"? System.Drawing.Color.Red :(Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum")?System.Drawing.Color.Black: System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="100px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_BUDGET_MONTHLY" Text='<%# (Eval("BUDGET_MONTHLY").ToString()) %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("BUDGET_MONTHLY")))? Convert.ToString(Eval("BUDGET_MONTHLY")).Substring(0,1)=="-"? System.Drawing.Color.Red : (Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum")?System.Drawing.Color.Black:System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label Width="100" runat="server" ID="lbl_GP_MONTHLY" Text='<%# (Eval("GP_MONTHLY").ToString()) %>' Visible='<%#Eval("ShowGP").ToString() == "1"   %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("GP_MONTHLY")))? Convert.ToString(Eval("GP_MONTHLY")).Substring(0,1)=="-"? System.Drawing.Color.Red : System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="100px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_SALES_YTD_VALUE" Text='<%# (Eval("SALES_YTD_VALUE").ToString()) %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("SALES_YTD_VALUE")))? Convert.ToString(Eval("SALES_YTD_VALUE")).Substring(0,1)=="-"? System.Drawing.Color.Red :(Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum")?System.Drawing.Color.Black: System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="100px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_SALES_YTD_VALUE_LY" Text='<%# (Eval("SALES_YTD_VALUE_LY").ToString()) %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("SALES_YTD_VALUE_LY")))? Convert.ToString(Eval("SALES_YTD_VALUE_LY")).Substring(0,1)=="-"? System.Drawing.Color.Red :(Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum")?System.Drawing.Color.Black: System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                       <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="100px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_GROWTH_YEARLY" Text='<%# (Eval("GROWTH_YEARLY").ToString()) %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("GROWTH_YEARLY")))? Convert.ToString(Eval("GROWTH_YEARLY")).Substring(0,1)=="-"? System.Drawing.Color.Red :(Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum")?System.Drawing.Color.Black: System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="100px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_BUDGET_YTD" Text='<%# (Eval("BUDGET_YTD").ToString()) %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("BUDGET_YTD")))? Convert.ToString(Eval("BUDGET_YTD")).Substring(0,1)=="-"? System.Drawing.Color.Red :(Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum")?System.Drawing.Color.Black: System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="100px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_BUDGET_YEARLY" Text='<%# (Eval("BUDGET_YEARLY").ToString()) %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("BUDGET_YEARLY")))? Convert.ToString(Eval("BUDGET_YEARLY")).Substring(0,1)=="-"? System.Drawing.Color.Red :(Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum")?System.Drawing.Color.Black: System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label Width="100px" runat="server" ID="lbl_GP_YEARLY" Text='<%# (Eval("GP_YEARLY").ToString()) %>' Visible='<%#Eval("ShowGP").ToString() == "1"   %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("GP_YEARLY")))? Convert.ToString(Eval("GP_YEARLY")).Substring(0,1)=="-"? System.Drawing.Color.Red :(Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum")?System.Drawing.Color.Black: System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="100px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_SALES_MTD_QTY" Text='<%# (Eval("SALES_MTD_QTY").ToString()) %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("SALES_MTD_QTY")))? Convert.ToString(Eval("SALES_MTD_QTY")).Substring(0,1)=="-"? System.Drawing.Color.Red :(Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum")?System.Drawing.Color.Black: System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="100px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_SALES_MTD_QTY_LY" Text='<%# (Eval("SALES_MTD_QTY_LY").ToString()) %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("SALES_MTD_QTY_LY")))? Convert.ToString(Eval("SALES_MTD_QTY_LY")).Substring(0,1)=="-"? System.Drawing.Color.Red :(Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum")?System.Drawing.Color.Black: System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="100px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_SALES_YTD_QTY" Text='<%# (Eval("SALES_YTD_QTY").ToString()) %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("SALES_YTD_QTY")))? Convert.ToString(Eval("SALES_YTD_QTY")).Substring(0,1)=="-"? System.Drawing.Color.Red :(Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum")?System.Drawing.Color.Black: System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="100px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_SALES_YTD_QTY_LY" Text='<%# (Eval("SALES_YTD_QTY_LY").ToString()) %>'  ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("SALES_YTD_QTY_LY")))? Convert.ToString(Eval("SALES_YTD_QTY_LY")).Substring(0,1)=="-"? System.Drawing.Color.Red :(Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum")?System.Drawing.Color.Black: System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="" HeaderStyle-CssClass="color_3" ItemStyle-Width="100px">
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lbl_GROWTH_YEARLY_QTY" Text='<%# (Eval("GROWTH_YEARLY_QTY").ToString()) %>' ForeColor='<%# !string.IsNullOrEmpty(Convert.ToString(Eval("GROWTH_YEARLY_QTY")))? Convert.ToString(Eval("GROWTH_YEARLY_QTY")).Substring(0,1)=="-"? System.Drawing.Color.Red : (Eval("sumFlag").ToString()=="products" || Eval("sumFlag").ToString()=="typeSum")?System.Drawing.Color.Black:System.Drawing.Color.White : System.Drawing.Color.White%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField> 
                    </Columns>
                </asp:GridView>
            </div>
           
           </ContentTemplate>
       <Triggers>
           <asp:PostBackTrigger ControlID="exportexcel" />
           <asp:PostBackTrigger ControlID="exportfamily" />
           <asp:AsyncPostBackTrigger ControlID="rbtn_Thousand" EventName="CheckedChanged" />
           <asp:AsyncPostBackTrigger ControlID="rbtn_Lakhs" EventName="CheckedChanged" />
       </Triggers>
       </asp:UpdatePanel>
           <script>
               $(document).ready(function () {
                   triggerPostGridLodedActions();
                   $(window).resize(function () {
                       triggerPostGridLodedActions();
                   });
               });

               function triggerPostGridLodedActions() {
                   gridviewScroll();
                   //bindToogleForRowIndex();
                   //bindToogleForParentRowIndex();

                   //Quantity changes Start
                   jQuery(".product_type").each(function () {
                       jQuery(this).change(function () {
                           var currentIndex = jQuery(this).data("product_type");
                           var grandTotal = 0;
                           jQuery(".product_type_" + currentIndex).each(function () {
                               var val = jQuery(this).val();
                               if (val != "") {
                                   grandTotal += parseInt(val);
                               }
                           });
                           jQuery(".product_type_sub_total_" + currentIndex).val(grandTotal).change();
                       });
                   });

                   jQuery(".product_type_sub_total").each(function () {
                       jQuery(this).change(function () {
                           var currentIndex = jQuery(this).data("product_type_sub_total_index");
                           var grandTotal = 0;
                           jQuery(".product_type_sub_total_row_" + currentIndex).each(function () {
                               var val = jQuery(this).val();
                               if (val != "") {
                                   grandTotal += parseInt(val);
                               }
                           });
                           jQuery(".product_type_SubFamilySum_" + currentIndex).val(grandTotal).change();
                       });
                   });
                   //Quantity Changes End

                   var td = $("#MainContent_grdviewAllValuesCopy").find("tr:first").find("td:first");
                   $(td).find('div').attr("style", "min-width:25px");
                   td = $("#MainContent_grdviewAllValuesCopyFreeze").find("tr:first").find("td:first");
                   $(td).find('div').attr("style", "min-width:25px");
               }

               function gridviewScroll() {

                   gridView1 = $('#MainContent_grdviewAllValues').gridviewScroll({
                       width: $(window).width() - 70,
                       height: findminofscreenheight($(window).height(), 450),
                       railcolor: "#F0F0F0",
                       barcolor: "#606060",
                       barhovercolor: "#606060",
                       bgcolor: "#F0F0F0",
                       freezesize: 0,
                       arrowsize: 30,
                       varrowtopimg: "Images/arrowvt.png",
                       varrowbottomimg: "Images/arrowvb.png",
                       harrowleftimg: "Images/arrowhl.png",
                       harrowrightimg: "Images/arrowhr.png",
                       headerrowcount: 2,
                       railsize: 16,
                       barsize: 14,
                       verticalbar: "auto",
                       horizontalbar: "auto",
                       wheelstep: 1,
                   });
               }
               function findminofscreenheight(a, b) {
                   a = a - $("#MainContent_collapse").offset().top;
                   return a < b ? a : b;
               }

               $('#MainContent_grdView_T_Clamp_Partingoff tr:last').addClass("last_row");


               
               function collapseAll() {
                   jQuery(".row_index").click(function () {

                       var currentIndex = jQuery(this).data("index");

                       jQuery(".subrowindex_" + currentIndex).each(function () {
                           //if (jQuery(this).css("display") != "none")
                           jQuery(this).slideUp();
                           //else
                           //    jQuery(this).slideDown();
                       });
                   });
               }
               function expandAll() {
                   debugger;
                   jQuery(".row_index").click(function () {

                       var currentIndex = jQuery(this).data("index");

                       jQuery(".subrowindex_" + currentIndex).each(function () {
                           //if (jQuery(this).css("display") != "none")
                           //jQuery(this).slideUp();
                           //else
                           jQuery(this).slideDown();
                           jQuery(this).attr("src", "images/button_minus.gif");
                           jQuery(this).data("toogle_status") = "MINIMISED";
                       });
                   });
               }
               jQuery(document).on('click', '.parent_row_index td div img', function () {
                   console.log('here 2');
                   var currentIndex = jQuery(this).closest(".parent_row_index").data("parent_row_index");
                   var minimised = false;
                   jQuery(".parent_row_index_" + currentIndex).each(function () {
                       if (jQuery(this).data("toogle_status") == "MAXIMISED")
                           jQuery(this).find("td div img").click();
                       var nextElement = jQuery(this).prev();
                       if (jQuery(this).css("display") != "none") {
                           jQuery(this).slideUp();
                           if (jQuery(nextElement).hasClass("empty_row"))
                               jQuery(nextElement).slideUp();
                       }
                       else {
                           jQuery(this).slideDown();
                           if (jQuery(nextElement).hasClass("empty_row"))
                               jQuery(nextElement).slideDown();
                           minimised = true;
                       }
                   });
                   if (minimised)
                       jQuery(this).attr("src", "images/button_minus.gif");
                   else
                       jQuery(this).attr("src", "images/button_plus.gif");
                   gridviewScroll();
               });
               jQuery(document).on('click', '.row_index_image', function () {
                   console.log('here 1');
                   var currentRowElement = jQuery(this).closest(".row_index");
                   var currentIndex = jQuery(currentRowElement).data("index");
                   var minimised = false;
                   jQuery(".subrowindex_" + currentIndex).each(function () {
                       if (jQuery(this).css("display") != "none") {
                           jQuery(this).slideUp();
                           minimised = true;
                       }
                       else {
                           jQuery(this).slideDown();
                           minimised = false;
                       }
                   });
                   if (minimised) {
                       jQuery(this).attr("src", "images/button_plus.gif");
                       jQuery(currentRowElement).data("toogle_status", "MINIMISED");
                   }
                   else {
                       jQuery(this).attr("src", "images/button_minus.gif");
                       jQuery(currentRowElement).data("toogle_status", "MAXIMISED");
                   }
                   gridviewScroll();
               });
               jQuery(document).on('click', '#MainContent_expand', function () {
                   debugger;
                   console.log('outside');
                   $("#MainContent_grdviewAllValues").find("tr.parent_row_index td div img").each(function () {
                       console.log('here 3');
                       var currentIndex = jQuery(this).closest(".parent_row_index").data("parent_row_index");
                       var minimised = false;
                       jQuery(".parent_row_index_" + currentIndex).each(function () {
                           if (jQuery(this).data("toogle_status") == "MAXIMISED")
                               jQuery(this).find("td div img").click();
                           var nextElement = jQuery(this).prev();
                           if (jQuery(this).css("display") != "none") {
                           }
                           else {
                               jQuery(this).slideDown();
                               if (jQuery(nextElement).hasClass("empty_row"))
                                   jQuery(nextElement).slideDown();
                               minimised = true;
                           }
                       });
                       jQuery(this).attr("src", "images/button_minus.gif");
                       console.log('here 4');

                   });
                   $("#MainContent_grdviewAllValues").find("tr td .row_index_image").each(function () {
                       console.log('here 5');
                       var currentRowElement = jQuery(this).closest(".row_index");
                       var currentIndex = jQuery(currentRowElement).data("index");
                       var minimised = false;
                       jQuery(".subrowindex_" + currentIndex).each(function () {
                           if (jQuery(this).css("display") != "none") {
                               minimised = true;
                           }
                           else {
                               jQuery(this).slideDown();
                               minimised = false;
                           }
                       });
                       if (minimised) {
                       }
                       else {
                           jQuery(this).attr("src", "images/button_minus.gif");
                           jQuery(currentRowElement).data("toogle_status", "MAXIMISED");
                       }
                       console.log('here 6');
                   });
                   triggerPostGridLodedActions();
               });
               jQuery(document).on('click', '#MainContent_collapse', function () {
                   debugger;
                   $("#MainContent_grdviewAllValues").find("tr.parent_row_index td div img").each(function () {
                       console.log('here 3');
                       var currentIndex = jQuery(this).closest(".parent_row_index").data("parent_row_index");
                       var minimised = false;
                       jQuery(".parent_row_index_" + currentIndex).each(function () {
                           if (jQuery(this).data("toogle_status") == "MAXIMISED")
                               jQuery(this).find("td div img").click();
                           var nextElement = jQuery(this).prev();
                           if (jQuery(this).css("display") != "none") {
                               jQuery(this).slideUp();
                               if (jQuery(nextElement).hasClass("empty_row"))
                                   jQuery(nextElement).slideUp();
                           }

                       });
                       if (minimised) {
                           //jQuery(this).attr("src", "images/button_minus.gif");
                       }
                       else
                           jQuery(this).attr("src", "images/button_plus.gif");
                       console.log('here 4');

                   });
                   $("#MainContent_grdviewAllValues").find("tr td .row_index_image").each(function () {
                       console.log('here 5');
                       var currentRowElement = jQuery(this).closest(".row_index");
                       var currentIndex = jQuery(currentRowElement).data("index");
                       var minimised = false;
                       jQuery(".subrowindex_" + currentIndex).each(function () {
                           if (jQuery(this).css("display") != "none") {
                               jQuery(this).slideUp();
                               minimised = true;
                           }
                           else {
                               //jQuery(this).slideDown();
                               minimised = false;
                           }
                       });
                       if (minimised) {
                           jQuery(this).attr("src", "images/button_plus.gif");
                           jQuery(currentRowElement).data("toogle_status", "MINIMISED");
                       }
                       else {
                           jQuery(this).attr("src", "images/button_plus.gif");
                           jQuery(currentRowElement).data("toogle_status", "MINIMISED");
                           //jQuery(this).attr("src", "images/button_minus.gif");
                           //jQuery(currentRowElement).data("toogle_status", "MAXIMISED");
                       }
                       console.log('here 6');
                   });
                   //triggerPostGridLodedActions();
               });
    </script>
    </asp:Content>


