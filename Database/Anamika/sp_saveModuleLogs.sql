-- =============================================
-- Author:		Anamika Kumari
-- Create date: Feb 12, 2019
-- Description:	A table to store time with module info used by user
-- =============================================
CREATE TABLE tt_ModuleLogs
(
ID INT IDENTITY(1,1),
EngineerId VARCHAR(100),
UniqueId VARCHAR(100),
Module	VARCHAR(MAX),
TimeOfUse DateTime
)
GO
-- =============================================
-- Author:		Anamika Kumari
-- Create date: Feb 12, 2019
-- Description:	A stored procedure to store time with module info used by user
-- =============================================
CREATE PROCEDURE sp_saveModuleLogs
(
@EngineerId VARCHAR(100),
@UniqueId VARCHAR(100),
@Module	VARCHAR(MAX)
)
AS
BEGIN

INSERT INTO tt_ModuleLogs(EngineerId,UniqueId,Module,TimeOfUse)
VALUES(@EngineerId,@UniqueId,@Module,GETDATE())

END