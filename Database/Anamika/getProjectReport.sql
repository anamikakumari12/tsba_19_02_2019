USE [Taegutec_Sales_Budget]
GO
/****** Object:  StoredProcedure [dbo].[getProjectReport]    Script Date: 2/8/2019 11:00:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--exec getProjectReport 'HO',null,null,null, null,null,null,null,null,null,null, null,null,null,null,null,'Knuckle'
ALTER Procedure [dbo].[getProjectReport]
(
@RoleId VARCHAR(10),
@Owner varchar(255)=null,
@Reviewer varchar(255)=null,
@Escalate varchar(255)=null,
@Industry varchar(255)=null,
@SubIndustry varchar(255)=null,
@Customer varchar(255)=null,
@customer_class varchar(255)=null,
@Branch varchar(255)=null,
@Status varchar(255)=null,
@from_target_date varchar(255)=null,
@to_target_date varchar(255)=null,
@from_created_date varchar(255)=null,
@to_created_date varchar(255)=null,
@cter varchar(10) = null,
@projtype varchar(255)=null,
@search VARCHAR(MAX)=null
)
AS
declare @query varchar(max)
declare @query1 varchar(max)
declare @query2 varchar(max)
Declare  @frmDate_t datetime , @frmDate_c datetime, @toDate_t datetime, @toDate_c datetime


BEGIN

-- Adding/Subtracting one extra day to existing day (Created Date and Target Date)
-- this date considering as date and time and while comparing time not dispaying exact values particular date
-- because of time including. thts y adding1 day extra to all dates

IF @to_target_date IS NOT NULL 
BEGIN
set @toDate_t = (select dateadd(DD, 1, CONVERT(datetime, REPLACE(@to_target_date, '''', ''), 103)))
set @to_target_date = ''''+ Convert(varchar, @toDate_t) +'''' 
END

IF @to_created_date IS NOT NULL 
BEGIN
set @toDate_c = (select dateadd(DD, 1, CONVERT(datetime, REPLACE(@to_created_date, '''',''), 103)))
set @to_created_date = ''''+ Convert(varchar, @toDate_c) +'''' 
END

Set @query1='Select m.customer_number customer_num,
m.customer_name,
I.IndustryName industry_lbl, 
OverAll_Potential overall_pot_lbl,
Potential_Lakhs potential_lakhs_lbl, 
Existing_Product existing_product_lbl,

Component component_lbl,
Stage_Completed,
NoOfStages,
Remarks txt_remarks,
IsCanceled ,
m.project_title projects,
 m.project_type,
project_number project_num,
project_status,
monthly_expected monthly_exp_val, 
m.assigned_salesengineer_id,
Reviewer,
Escalated_To,
s_i.sub_IndustryName sub_industry_lbl,
m.distributor distributor_num, 
Date_Created creation_date_lbl 
, l.EngineerName Engineer_Name, m.Business_Expected Business_Expected
, m.customer_name + ''(''+ m.customer_number +'')'' customer_lbl
,CASE 
	WHEN (m.distributor IS NULL OR m.distributor='''') THEN ''''
		ELSE ((Select customer_short_name from tt_customer_master where customer_number=m.distributor)+''(''+m.distributor+'')'') 
	END distributor_lbl
,CASE 
	WHEN Stage_Completed=NoOfStages THEN
		CASE
			WHEN project_status=''failure'' THEN ''FAILURE''
			WHEN project_status=''success'' THEN ''SUCCESS''
			ELSE ''Stg''+ CAST(Stage_Completed AS VARCHAR(10))+ ''/''+ ''Stg''+ CAST(NoOfStages AS VARCHAR(10)) 
		END
	ELSE
		CASE	
			WHEN Stage_Completed <>'''' THEN ''Stg''+ CAST(Stage_Completed AS VARCHAR(10))+ ''/''+ ''Stg''+ CAST(NoOfStages AS VARCHAR(10)) 
			ELSE ''Stg0/Stg'' + CAST(NoOfStages AS VARCHAR(10)) 
		END
	END status_lbl
,ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2) Percentage
,CASE
	WHEN (ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2)<>0 AND ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2) <=33.33) THEN ''yes''
	WHEN (ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2)<>0 AND ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2) >33.33 AND ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2)<=66.66) THEN ''no''
	WHEN (ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2)<>0 AND ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2) >66.66) THEN ''no''
	ELSE ''no''
	END red
,CASE
	WHEN (ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2)<>0 AND ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2) <=33.33) THEN ''no''
	WHEN (ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2)<>0 AND ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2) >33.33 AND ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2)<=66.66) THEN ''yes''
	WHEN (ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2)<>0 AND ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2) >66.66) THEN ''no''
	ELSE ''no''														 				
	END yellow															 				
,CASE																	 				
	WHEN (ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2)<>0 AND ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2) <=33.33) THEN ''no''
	WHEN (ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2)<>0 AND ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2) >33.33 AND ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2)<=66.66) THEN ''no''
	WHEN (ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2)<>0 AND ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2) >66.66) THEN ''yes''
	ELSE ''no''
	END green
,m.project_title+''(''+m.project_number+'')'' project_title
,CASE
	WHEN IsCanceled=''1'' THEN ''no''
	WHEN (IsCanceled <>''1'' OR IsCanceled IS NULL) AND ('''+@RoleID+'''=''HO'' OR '''+@RoleID+'''=''TM'') THEN ''yes''
	END cancel

 from tt_mdpinfo M 
left outer join tt_industry I on I.IndustryId=M.main_industry 
left outer join tt_sub_industry s_i on s_i.Sub_IndustryId=m.sub_industry
 join view_Customers c on c.customer_number = m.customer_number
join LoginInfo l on l.EngineerId = m.assigned_salesengineer_id
 Where ( m.IsApproved=1 and IsCanceled is null) 
 '

 if @customer_class IS NOT NULL
 set @query1=@query1+' AND  (( c.customer_class='+@customer_class+' ))'

 IF @Branch is not null
 set @query1=@query1+' AND  ((m.Branch_Code='+@Branch+' )'+'OR (m.territory_engineer_id='+@Branch+'))'


 IF @Owner is not null
 set @query1=@query1+' AND  (m.assigned_salesengineer_id='+@Owner+' )'

 IF @Reviewer is not null
 set @query1=@query1+' AND  (m.Reviewer='+@Reviewer+' )'

 IF @Escalate is not null
 set @query1=@query1+' AND  (m.Escalated_To='+@Escalate+' )'

 IF @Industry is not null
 set @query1=@query1+' AND  (m.main_industry='+@Industry+' )'

 IF @Customer is not null
 set @query1=@query1+' AND  ((m.customer_number='+@Customer+' ) OR (m.distributor='+@Customer+'))'

 if @cter is not null
set @query1=@query1+' AND  ( c.cter='''+@cter+''' )'

 IF @SubIndustry is not null
 set @query1=@query1+' AND  (m.sub_industry='+@SubIndustry+' )'

IF @Status='''OPEN'''
set @query1=@query1+' AND (m.Stage_Completed<>m.NoOfStages AND m.project_status NOT IN (''success'',''failure'') )'
--set @query1=@query1+' AND (m.project_status NOT IN (''success'',''failure'') )'
IF @Status='''FAILURE''' or @Status='''SUCCESS'''
set @query1=@query1+' AND  (m.project_status='+@Status+' )'
  
IF @to_target_date  is not null
set @query1=@query1+' AND (CONVERT(datetime,m.Main_Target_Date,103)<=CONVERT(datetime,'+@to_target_date+',103))'

IF @from_target_date  is not null
set @query1=@query1+' AND  (CONVERT(datetime,m.Main_Target_Date,103)>=CONVERT(datetime,'+@from_target_date+',103))'

IF
@to_created_date  is not null
set @query1=@query1+' AND (CONVERT(datetime,m.Date_Created,103)<=CONVERT(datetime,'+@to_created_date+',103))'
IF
@from_created_date  is not null
set @query1=@query1+' AND  (CONVERT(datetime,m.Date_Created,103)>=CONVERT(datetime,'+@from_created_date+',103))'

IF (@projtype is not null OR @projtype<>0)
set @query1=@query1+' AND  (m.project_type='+@projtype+' )'

Set @query2='Select m.customer_number customer_num,m.customer_name,I.IndustryName industry_lbl,OverAll_Potential overall_pot_lbl
,Potential_Lakhs potential_lakhs_lbl,Existing_Product existing_product_lbl, Component component_lbl, Stage_Completed,NoOfStages,
Remarks,IsCanceled ,m.project_title projects, m.project_type,
project_number project_num,project_status,monthly_expected monthly_exp_val, m.assigned_salesengineer_id,Reviewer,Escalated_To,
s_i.sub_IndustryName sub_industry_lbl, m.distributor distributor_num, Date_Created creation_date_lbl, 
l.EngineerName Engineer_Name, m.Business_Expected Business_Expected
,m.customer_name+''(''+m.customer_number+'')'' customer_lbl
,CASE 
	WHEN (m.distributor IS NULL OR m.distributor='''') THEN ''''
		ELSE ((Select customer_short_name from tt_customer_master where customer_number=m.distributor)+''(''+m.distributor+'')'') 
	END distributor_lbl
,CASE 
	WHEN Stage_Completed=NoOfStages THEN
		CASE
			WHEN project_status=''failure'' THEN ''FAILURE''
			WHEN project_status=''success'' THEN ''SUCCESS''
		END
	ELSE
		CASE	
			WHEN Stage_Completed <>'''' THEN ''Stg''+ CAST(Stage_Completed AS VARCHAR(10))+ ''/''+ ''Stg''+ CAST(NoOfStages AS VARCHAR(10)) 
			ELSE ''Stg0/Stg'' + CAST(NoOfStages AS VARCHAR(10)) 
		END
	END status_lbl
,ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2) Percentage
,CASE
	WHEN (ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2)<>0 AND ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2) <=33.33) THEN ''yes''
	WHEN (ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2)<>0 AND ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2) >33.33 AND ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2)<=66.66) THEN ''no''
	WHEN (ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2)<>0 AND ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2) >66.66) THEN ''no''
	ELSE ''no''
	END red
,CASE
	WHEN (ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2)<>0 AND ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2) <=33.33) THEN ''no''
	WHEN (ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2)<>0 AND ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2) >33.33 AND ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2)<=66.66) THEN ''yes''
	WHEN (ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2)<>0 AND ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2) >66.66) THEN ''no''
	ELSE ''no''														 				
	END yellow															 				
,CASE																	 				
	WHEN (ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2)<>0 AND ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2) <=33.33) THEN ''no''
	WHEN (ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2)<>0 AND ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2) >33.33 AND ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2)<=66.66) THEN ''no''
	WHEN (ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2)<>0 AND ROUND((CAST(Stage_Completed AS FLOAT)/CAST(NoOfStages AS FLOAT))*100, 2) >66.66) THEN ''yes''
	ELSE ''no''
	END green
,m.project_title+''(''+m.project_number+'')'' project_title
,CASE
	WHEN IsCanceled=''1'' THEN ''no''
	WHEN (IsCanceled <>''1'' OR IsCanceled IS NULL) AND ('''+@RoleID+'''=''HO'' OR '''+@RoleID+'''=''TM'') THEN ''yes''
	END cancel

 from tt_mdpinfo M 
 left outer join tt_industry I on I.IndustryId=M.main_industry 
 join tt_sub_industry s_i on s_i.Sub_IndustryId=m.sub_industry
 join view_Customers c on c.customer_number = M.customer_number
 join LoginInfo l on l.EngineerId = m.assigned_salesengineer_id
 Where (m.IsApproved=1 and IsCanceled is null) 
'



if @customer_class IS NOT NULL
 set @query2=@query2+' AND  (( c.customer_class='+@customer_class+' ))'

 IF @Branch is not null
 set @query2=@query2+' AND  ((m.Branch_Code='+@Branch+' )'+'OR (m.territory_engineer_id='+@Branch+'))'

 IF @Owner is not null
 set @query2=@query2+' AND  (m.assigned_salesengineer_id='+@Owner+' )'

 IF @Reviewer is not null
 set @query2=@query2+' AND  (m.Reviewer='+@Reviewer+' )'

 IF @Escalate is not null
 set @query2=@query2+' AND  (m.Escalated_To='+@Escalate+' )'

 IF @Industry is not null
 set @query2=@query2+' AND  (m.main_industry='+@Industry+' )'

 IF @Customer is not null
 set @query2=@query2+' AND  ((m.customer_number='+@Customer+' )  OR (m.distributor='+@Customer+'))'

 if @cter is not null
set @query2=@query2+' AND  ( c.cter='''+@cter+''' )'


 IF @SubIndustry is not null
 set @query2=@query2+' AND  (m.sub_industry='+@SubIndustry+' )'

IF @Status='''OPEN'''
set @query2=@query2+' AND (m.Stage_Completed<>m.NoOfStages AND m.project_status NOT IN (''success'',''failure''))'
--set @query2=@query2+' AND (m.project_status NOT IN (''success'',''failure''))'

IF @Status='''FAILURE''' or @Status='''SUCCESS'''
set @query2=@query2+' AND  (m.project_status='+@Status+' )'
  
IF @to_target_date  is not null
set @query2=@query2+' AND (CONVERT(datetime,m.Main_Target_Date,103)<=CONVERT(datetime,'+@to_target_date+',103))'

IF @from_target_date  is not null
set @query2=@query2+' AND  (CONVERT(datetime,m.Main_Target_Date,103)>=CONVERT(datetime,'+@from_target_date+',103))'

IF
@to_created_date  is not null
set @query2=@query2+' AND (CONVERT(datetime,m.Date_Created,103)<=CONVERT(datetime,'+@to_created_date+',103))'
IF
@from_created_date  is not null
set @query2=@query2+' AND  (CONVERT(datetime,m.Date_Created,103)>=CONVERT(datetime,'+@from_created_date+',103))'

IF (@projtype is not null OR @projtype<>0)
set @query2=@query2+' AND  (m.project_type='+@projtype+' )'

SET @search= LTRIM(RTRIM(@search))

IF (@search<>'' AND @search IS NOT NULL)
BEGIN
set @query1= @query1 +' AND (m.customer_number like ''%'+@search+'%'' 
 OR m.customer_name like ''%'+@search+'%''
 OR m.distributor like ''%'+@search+'%''
 OR m.project_title like ''%'+@search+'%''
 OR m.project_number like ''%'+@search+'%''
 OR m.project_type like ''%'+@search+'%''
 OR l.EngineerName like ''%'+@search+'%''
 OR Component like ''%'+@search+'%''
 OR Existing_Product like ''%'+@search+'%'') '

 set @query2= @query2 +' AND (m.customer_number like ''%'+@search+'%'' 
 OR m.customer_name like ''%'+@search+'%''
 OR m.distributor like ''%'+@search+'%''
 OR m.project_title like ''%'+@search+'%''
 OR m.project_number like ''%'+@search+'%''
  OR m.project_type like ''%'+@search+'%''
 OR l.EngineerName like ''%'+@search+'%''
 OR Component like ''%'+@search+'%''
 OR Existing_Product like ''%'+@search+'%'') '
 END


 set @query=@query1+''+' union '+' '+@query2
--SELECT @Status
Print (@Query)



TRUNCATE TABLE TEMP_MDPREPORTS
INSERT INTO TEMP_MDPREPORTS
Exec(@query)

DECLARE @COUNT INT
Select @COUNT=COUNT(*) from TEMP_MDPREPORTS

IF(@COUNT>0)
INSERT INTO TEMP_MDPREPORTS(Engineer_Name, overall_pot_lbl, potential_lakhs_lbl, monthly_exp_val, Business_Expected)
SELECT 'Grand Total', sum(overall_pot_lbl) over_pot,sum(potential_lakhs_lbl) pot_lakh, sum(monthly_exp_val) total_monthly_expected,sum(Business_Expected) total_business_expected 
FROM
(SELECT  distinct overall_pot_lbl, potential_lakhs_lbl, monthly_exp_val, Business_Expected,project_num
FROM TEMP_MDPREPORTS)t

SELECT * FROM TEMP_MDPREPORTS

END
RETURN

