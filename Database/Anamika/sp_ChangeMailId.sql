USE [Taegutec_Sales_Budget]
GO
/****** Object:  StoredProcedure [dbo].[sp_ChangeMailId]    Script Date: 2/7/2019 2:57:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Modified By:		Anamika Kumari
-- Modfied date:	Jan 30, 2019
-- Description:		To change the mail id for admin and guest user
-- =============================================
ALTER proc [dbo].[sp_ChangeMailId]
@loginmail varchar(50),
@retloginmail varchar(50) out
as
begin 

if @loginmail='Administrator@taegutec-india.com'
	set @retloginmail='bindu@knstek.com'
else if @loginmail='guestuser@knstek.com'
	set @retloginmail='anamika@knstek.com'
else
	set @retloginmail='bindu@knstek.com'
end