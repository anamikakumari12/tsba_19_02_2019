
-- =============================================
-- Author:		Anamika Kumari
-- Create date: Feb 11, 2019
-- Description:	A table to store login time with user info
-- =============================================
CREATE TABLE tt_LoginLogs
(
ID INT IDENTITY(1,1),
EngineerId VARCHAR(100),
EngineerName VARCHAR(MAX),
DeviceIP VARCHAR(100),
LoginTime DateTime
)

GO
-- =============================================
-- Author:		Anamika Kumari
-- Create date: Feb 11, 2019
-- Description:	A stored procedure to store login time with user info
-- =============================================
CREATE PROCEDURE sp_saveLoginUserLogs
(
@EngineerId VARCHAR(100),
@EngineerName VARCHAR(MAX),
@DeviceIP VARCHAR(100)
)
AS
BEGIN

INSERT INTO tt_LoginLogs(EngineerId,EngineerName,DeviceIP,LoginTime)
VALUES(@EngineerId,@EngineerName,@DeviceIP,GETDATE())
END