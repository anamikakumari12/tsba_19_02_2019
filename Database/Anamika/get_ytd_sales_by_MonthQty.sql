USE [Taegutec_Sales_Budget]
GO
/****** Object:  UserDefinedFunction [dbo].[get_ytd_sales_by_MonthQty]    Script Date: 2/1/2019 5:05:44 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Modified By:		Anamika Kumari
-- Modfied date:	Dec 07, 2018
-- Description:		To fetch ytd quantity value monthly with different inputs, also product group added (10years flag)
-- =============================================
ALTER  FUNCTION [dbo].[get_ytd_sales_by_MonthQty] 
			(@p_year	int,
			@p_current_month	int,
			@p_flag	varchar(50),
			@p_family_name   VARCHAR(MAX),
			@p_sub_family_name   VARCHAR(MAX), 
			@p_customer_type varchar(10),
			@p_customer_number	VARCHAR(MAX),
			@p_sales_engineer_id	VARCHAR(MAX),
			@p_customer_region VARCHAR(MAX),
			@p_item_code VARCHAR(MAX),
			@p_cter varchar(10)
			)
RETURNS numeric(22,5) AS
BEGIN
	DECLARE @ret numeric(22,5) ;
		Declare @currentdate int
	Select @currentdate=DAY(GETDATE())
	IF(@currentdate=1 AND @p_current_month<>1)
	SET @p_current_month=@p_current_month-1
	-- Case 0
	-- IF ITEM CODE AND SPECIAL GROUPs NOT NULL
	IF ((@p_sub_family_name IS NULL  AND  @p_family_name IS NULL)  AND 
	    (@p_customer_type IS NULL AND  @p_customer_number IS NULL AND @p_sales_engineer_id IS NULL AND @p_customer_region IS NULL)
	    AND (@p_item_code IS NOT NULL OR @p_flag IS NULL))
		AND @p_cter IS NOT NULL
		BEGIN
			SELECT   @ret = SUM(quantity)
			FROM   [dbo].[tt_salesnumbers] s, dbo.tt_item_master itm, tt_customer_master c, tt_item_group tg
			WHERE  s.inv_year = @p_year
			AND    s.inv_month <= @p_current_month
			AND    s.item_code = itm.item_code
			AND		itm.item_code=tg.item_code
		AND		tg.budget_year=@p_year
			AND (('GOLD'IN (SELECT item from dbo.SplitString(REPLACE(@p_flag,'''',''),','))  AND tg.gold_flag = 'Y') OR
				('BB'  IN (SELECT item from dbo.SplitString(REPLACE(@p_flag,'''',''),','))  AND tg.bb_flag = 'Y') OR
				('5YRS' IN (SELECT item from dbo.SplitString(REPLACE(@p_flag,'''',''),','))  AND tg.five_years_flag = 'Y') OR
				('SPC' IN (SELECT item from dbo.SplitString(REPLACE(@p_flag,'''',''),','))  AND tg.SPC_flag = 'Y') OR
				('TOP' IN (SELECT item from dbo.SplitString(REPLACE(@p_flag,'''',''),','))  AND tg.top_flag = 'Y') OR
				('10YRS' IN (SELECT item from dbo.SplitString(REPLACE(@p_flag,'''',''),','))  AND tg.ten_years_flag = 'Y') OR
				  @p_flag IS NULL)
			AND   (@p_item_code IS NULL OR itm.item_code in (select Item from SplitString(@p_item_code,''',''')))
			AND    c.customer_number = s.customer_number
			AND    (@p_cter IS NULL OR c.cter in (select Item from SplitString(@p_cter,''',''')))
		RETURN @ret
		END


	--Case1	
	-- IF ITEM CODE AND SPECIAL GROUPs NOT NULL
	ELSE IF ((@p_sub_family_name IS NULL  AND  @p_family_name IS NULL)  AND 
	    (@p_customer_type IS NULL AND  @p_customer_number IS NULL AND @p_sales_engineer_id IS NULL AND @p_customer_region IS NULL)
	    AND (@p_item_code IS NOT NULL OR @p_flag IS NULL))
		BEGIN
			SELECT   @ret = SUM(quantity)
			FROM  [dbo].[tt_salesnumbers] s, dbo.tt_item_master itm, tt_item_group tg
			WHERE  s.inv_year = @p_year
			AND    s.inv_month <= @p_current_month
			AND    s.item_code = itm.item_code
			AND		itm.item_code=tg.item_code
		AND		tg.budget_year=@p_year
			 AND (('GOLD'IN (SELECT item from dbo.SplitString(REPLACE(@p_flag,'''',''),','))  AND tg.gold_flag = 'Y') OR
				('BB'  IN (SELECT item from dbo.SplitString(REPLACE(@p_flag,'''',''),','))  AND tg.bb_flag = 'Y') OR
				('5YRS' IN (SELECT item from dbo.SplitString(REPLACE(@p_flag,'''',''),','))  AND tg.five_years_flag = 'Y') OR
				('SPC' IN (SELECT item from dbo.SplitString(REPLACE(@p_flag,'''',''),','))  AND tg.SPC_flag = 'Y') OR
				('TOP' IN (SELECT item from dbo.SplitString(REPLACE(@p_flag,'''',''),','))  AND tg.top_flag = 'Y') OR
				('10YRS' IN (SELECT item from dbo.SplitString(REPLACE(@p_flag,'''',''),','))  AND tg.ten_years_flag = 'Y') OR
				  @p_flag IS NULL)
			AND   (@p_item_code IS NULL OR itm.item_code in (select Item from SplitString(@p_item_code,''',''')))
		RETURN @ret
		END

	--Case2	
	-- For ALL condition (@p_sub_family_name, @p_flag, @p_family_name NULL Values)
	IF (@p_sub_family_name IS  NULL AND @p_flag IS  NULL AND @p_family_name IS NULL AND @p_item_code IS NULL)
		SELECT   @ret = SUM(quantity)
			FROM   [dbo].[tt_salesnumbers] s, dbo.tt_customer_master c
			WHERE  inv_year = @p_year
			AND    s.inv_month <= @p_current_month
			and    s.customer_number = c.customer_number
			and	   (@p_customer_type IS NULL OR (c.customer_type in (select Item from SplitString(@p_customer_type,''','''))))
			AND	   (@p_customer_number IS NULL OR (c.customer_number in (select Item from SplitString(@p_customer_number,''','''))))
			AND	   (@p_sales_engineer_id IS NULL OR (c.assigned_salesengineer_id in (select Item from SplitString(@p_sales_engineer_id,''','''))))
			AND	   (@p_customer_region IS NULL OR (c.Customer_region in (select Item from SplitString(@p_customer_region,''',''')) OR c.territory_engineer_id in (select Item from SplitString(@p_customer_region,''','''))))	
			AND    (@p_cter IS NULL OR c.cter  in (select Item from SplitString(@p_cter,''',''')))
	
	--Case3
	-- For ALL condition (@p_sub_family_name, @p_flag, @p_family_name, @p_item_code NOT NULL Values)
	IF (@p_sub_family_name IS NOT  NULL OR  @p_flag IS NOT NULL OR  @p_family_name IS NOT NULL OR @p_item_code IS NOT NULL)
	SELECT   @ret = SUM(quantity)
		FROM   [dbo].[tt_salesnumbers] s, dbo.tt_item_master itm, dbo.tt_item_family fly, dbo.tt_customer_master c, dbo.tt_item_sub_family sub, tt_item_group tg
		WHERE  s.inv_year = @p_year
		AND    s.inv_month <= @p_current_month
		AND    s.item_code = itm.item_code
		and    s.customer_number = c.customer_number
		AND		itm.item_code=tg.item_code
		AND		tg.budget_year=@p_year
		and	   (@p_customer_type IS NULL OR (c.customer_type in (select Item from SplitString(@p_customer_type,''','''))))
			AND	   (@p_customer_number IS NULL OR (c.customer_number in (select Item from SplitString(@p_customer_number,''','''))))
			AND	   (@p_sales_engineer_id IS NULL OR (c.assigned_salesengineer_id in (select Item from SplitString(@p_sales_engineer_id,''','''))))
			AND	   (@p_customer_region IS NULL OR (c.Customer_region in (select Item from SplitString(@p_customer_region,''',''')) OR c.territory_engineer_id in (select Item from SplitString(@p_customer_region,''','''))))	
		AND    itm.item_family_id = fly.item_family_id	
		AND    itm.item_sub_family_id = sub.item_sub_family_id
		AND    (@p_sub_family_name IS NULL OR sub.item_sub_family_name in (select Item from SplitString(@p_sub_family_name,''',''')))
		AND    (@p_family_name IS NULL OR fly.item_family_id in (select Item from SplitString(@p_family_name,',')))
		AND (('GOLD'IN (SELECT item from dbo.SplitString(REPLACE(@p_flag,'''',''),','))  AND tg.gold_flag = 'Y') OR
				('BB'  IN (SELECT item from dbo.SplitString(REPLACE(@p_flag,'''',''),','))  AND tg.bb_flag = 'Y') OR
				('5YRS' IN (SELECT item from dbo.SplitString(REPLACE(@p_flag,'''',''),','))  AND tg.five_years_flag = 'Y') OR
				('SPC' IN (SELECT item from dbo.SplitString(REPLACE(@p_flag,'''',''),','))  AND tg.SPC_flag = 'Y') OR
				('TOP' IN (SELECT item from dbo.SplitString(REPLACE(@p_flag,'''',''),','))  AND tg.top_flag = 'Y') OR
				('10YRS' IN (SELECT item from dbo.SplitString(REPLACE(@p_flag,'''',''),','))  AND tg.ten_years_flag = 'Y') OR
				  @p_flag IS NULL)
		AND   (@p_item_code IS NULL OR itm.item_code in (select Item from SplitString(@p_item_code,''',''')))
		AND    (@p_cter IS NULL OR c.cter  in (select Item from SplitString(@p_cter,''',''')))

	RETURN @ret
END


