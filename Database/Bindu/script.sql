USE [Taegutec_Sales_Budget]
GO
/****** Object:  Table [dbo].[tt_LoginDeviceInfo]    Script Date: 2/7/2019 2:24:23 PM ******/
DROP TABLE [dbo].[tt_LoginDeviceInfo]
GO
/****** Object:  StoredProcedure [dbo].[sp_updateLoginDevicedetails]    Script Date: 2/7/2019 2:24:23 PM ******/
DROP PROCEDURE [dbo].[sp_updateLoginDevicedetails]
GO
/****** Object:  StoredProcedure [dbo].[sp_getLoginDevicedetails]    Script Date: 2/7/2019 2:24:23 PM ******/
DROP PROCEDURE [dbo].[sp_getLoginDevicedetails]
GO
/****** Object:  StoredProcedure [dbo].[LoginAuthentication]    Script Date: 2/7/2019 2:24:23 PM ******/
DROP PROCEDURE [dbo].[LoginAuthentication]
GO
/****** Object:  StoredProcedure [dbo].[LoginAuthentication]    Script Date: 2/7/2019 2:24:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Aswini

-- Description:	Login Authentication
--Modified By:K.LakshmiBindu
-- ALTER date: 2/2/2019
--Modified Desc: Added PhoneNumber 
-- =============================================
CREATE Procedure[dbo].[LoginAuthentication]
(@LoginMailId varchar(500),
 @MailPassword varchar(1000),
 @EngineerName varchar(500) OUTPUT,
 @EngineerId nvarchar(255)OUTPUT,
 @RoleID  varchar(500) OUTPUT,
 @ErrorMessege Varchar(100) OUTPUT,
 @ErrorNum int OUTPUT,
 @Branchcode Varchar(100) OUTPUT,
 @BranchDesc varchar(100)OUTPUT,
 @Territory varchar(100) OUTPUT,
 @PhoneNumber varchar(15) OUTPUT)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	IF EXISTS (SELECT * FROM view_UserData Where LoginStatus = 1 AND EmailId = @LoginMailId)
	BEGIN
		IF EXISTS (SELECT EmailId, EngineerName, RoleId, LoginStatus from view_UserData WHERE EmailId = @LoginMailId AND Password = @MailPassword)
		BEGIN 
		SET @EngineerName =  (SELECT  EngineerName from view_UserData WHERE EmailId = @LoginMailId) 
		SET @RoleID = (SELECT RoleId from view_UserData WHERE EmailId = @LoginMailId )
		SET @EngineerId = (SELECT EngineerId from view_UserData WHERE EmailId = @LoginMailId )
		SET @ErrorNum = 0
		SET @Branchcode  = (SELECT BranchCode from view_UserData WHERE EmailId = @LoginMailId )
		SET @BranchDesc  = (SELECT region_description from tt_region WHERE region_code = @Branchcode )
		SET @Territory=(select cter from tt_region where region_code=@Branchcode)
		set @PhoneNumber=(select PhoneNumber from view_UserData where EmailId=@LoginMailId)
		RETURN
		END

		ELSE 
		BEGIN SET @ErrorMessege = 'Enter Valid UserName and PassWord' SET @ErrorNum = 1
		RETURN
		END
		
	RETURN
	END

	ELSE IF EXISTS (SELECT * FROM view_UserData Where LoginStatus = 0 AND EmailId = @LoginMailId)
	BEGIN SET @ErrorMessege = 'Your account is blocked' SET @ErrorNum = 2
	RETURN
	END
	
END





GO
/****** Object:  StoredProcedure [dbo].[sp_getLoginDevicedetails]    Script Date: 2/7/2019 2:24:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[sp_getLoginDevicedetails]
@emailid varchar(40)=null
as
begin 
update tt_LoginDeviceInfo set StausOfRequestedDevice=(case when ApprovedTillTimePeriod>cast(GETDATE() as date)
															then 1
															else 0 end)
if(@emailid is not null)
begin
 select * from tt_LoginDeviceInfo where EmailId=@emailid
end
else
begin
 select l.EngineerName,l.EmailId,l.RoleId,ld.defaultDevice1,ld.defaultDevice2,ld.RequestedDevice,
 case when  ld.RequestedDevice<> null and ld.StausOfRequestedDevice=0 then'pending for approval' 
         when ld.RequestedDevice is null or ld.RequestedDevice= ''  then 'No Requested Device' 
		 when ld.stausofRequesteddevice =1 then 'Approved'
     else 'pending for approval'
end as statusofdevice, DATEDIFF(day,approveddate,approvedtilltimeperiod) as noofdays
  from tt_LoginDeviceInfo ld,LoginInfo l where ld.emailid= l.EmailId and l.LoginStatus=1
end
end
GO
/****** Object:  StoredProcedure [dbo].[sp_updateLoginDevicedetails]    Script Date: 2/7/2019 2:24:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE proc [dbo].[sp_updateLoginDevicedetails]
@email varchar(40),
@defaultdevice1 varchar(40),
@defaultdevice2 varchar(40)=null,
@requesteddevice varchar(40)=null,
@requesteddate date=null,
@statusofrequesteddevice int=null,
@RequestedTilldate date=null,
@timeperiod int=0,
@approveddate date=null,
@flag varchar(40),
@errornum int =0 out
as
begin 

if(@flag='intial')
begin 

declare @useremail varchar(40)
select @useremail= EmailId from LoginInfo where LoginStatus=1 and EmailId=@email
if(@useremail is null)
begin 
set @errornum=1;
end
else
begin 
insert into tt_LoginDeviceInfo values(@email,@defaultdevice1,@defaultdevice2,@requesteddevice,@requestedDate,@statusofrequesteddevice,@RequestedTilldate,@approveddate)
end 

end 
 
if(@flag='UpdateDefaultDevice2')
begin
 update tt_LoginDeviceInfo set DefaultDevice2=@defaultdevice2 where EmailId=@email and @email= (select Emailid from LoginInfo where EmailId=@email and LoginStatus=1)

end
if(@flag='RequestedDevice')
begin

 select @requesteddate=cast(GETDATE() as date)
 update tt_LoginDeviceInfo set RequestedDate=@requesteddate,RequestedDevice=@requesteddevice,StausOfRequestedDevice=0,ApprovedTillTimePeriod=null,ApprovedDate=null where EmailId=@email and @email= (select Emailid from LoginInfo where EmailId=@email and LoginStatus=1)
end 
if(@flag='UpdateDefaultDevice1')
begin
 update tt_LoginDeviceInfo set DefaultDevice1=@defaultdevice1 where EmailId=@email and @email= (select Emailid from LoginInfo where EmailId=@email and LoginStatus=1)
end

if(@flag='UpdateStatusOfRequestedDevice')
begin
if(@statusofrequesteddevice>0)
begin
declare @approvedtilldate date
select @approvedtilldate =cast(DATEADD(D,@timeperiod,GETDATE()) as date)
 update tt_LoginDeviceInfo set DefaultDevice1=@defaultdevice1,DefaultDevice2=@defaultdevice2, RequestedDevice=@requesteddevice,ApprovedTillTimePeriod=@approvedtilldate, StausOfRequestedDevice=@statusofrequesteddevice,ApprovedDate=@approveddate where EmailId=@email and @email= (select Emailid from LoginInfo where EmailId=@email and LoginStatus=1)
end  
else
begin
 update tt_LoginDeviceInfo set DefaultDevice1=@defaultdevice1,DefaultDevice2=@defaultdevice2, RequestedDevice=@requesteddevice,StausOfRequestedDevice=@statusofrequesteddevice,ApprovedTillTimePeriod=null,ApprovedDate=null where EmailId=@email and @email= (select Emailid from LoginInfo where EmailId=@email and LoginStatus=1)
end
end
end



GO
/****** Object:  Table [dbo].[tt_LoginDeviceInfo]    Script Date: 2/7/2019 2:24:23 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tt_LoginDeviceInfo](
	[EmailId] [varchar](50) NOT NULL,
	[DefaultDevice1] [varchar](50) NULL,
	[DefaultDevice2] [varchar](50) NULL,
	[RequestedDevice] [varchar](50) NULL,
	[Requesteddate] [date] NULL,
	[StausOfRequestedDevice] [int] NULL,
	[ApprovedTillTimePeriod] [date] NULL,
	[ApprovedDate] [date] NULL,
PRIMARY KEY CLUSTERED 
(
	[EmailId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
